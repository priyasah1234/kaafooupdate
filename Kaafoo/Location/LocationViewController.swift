//
//  LocationViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 28/03/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class LocationViewController: GlobalViewController,UIPickerViewDelegate,UIPickerViewDataSource {

    var clickedIndex : Int!
    var DataArray : NSMutableArray! = NSMutableArray()
    let dispatchGroup = DispatchGroup()

    @IBOutlet weak var DataPickerView: UIPickerView!
    @IBOutlet weak var LocalHeaderView: UIView!
    @IBOutlet weak var tapOnBackOutlet: UIButton!
    @IBAction func tapOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var ResetOutlet: UIButton!
    @IBAction func ResetBtn(_ sender: UIButton) {
    }
    @IBAction func StateBtn(_ sender: UIButton) {
        clickedIndex = 0
        self.DataPickerView.reloadAllComponents()
        self.DataPickerView.isHidden = false
    }
    @IBOutlet weak var StateBtnOutlet: UIButton!
    
    @IBOutlet weak var SuburbLbl: UILabel!
    @IBOutlet weak var CityLbl: UILabel!
    @IBOutlet weak var StateLbl: UILabel!
    @IBOutlet weak var SuburbBtnOutlet: UIButton!
    @IBAction func SuburbBtn(_ sender: UIButton) {
        clickedIndex = 2
        self.DataPickerView.reloadAllComponents()
    }
    @IBAction func CityBtn(_ sender: UIButton) {
        clickedIndex = 1
        self.DataPickerView.reloadAllComponents()
        self.DataPickerView.isHidden = false
    }
    @IBOutlet weak var CityBtnOutlet: UIButton!
    
    @IBAction func tapOnApply(_ sender: UIButton) {
    }
    @IBOutlet weak var ApplyBtnOutlet: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetStatesList()

        // Do any additional setup after loading the view.
    }

    //MArk:- //PickerView delegate and datasource methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if clickedIndex == 0
        {
          return self.DataArray.count
        }
        else if clickedIndex == 1
        {
          return 0
        }
        else if clickedIndex == 2
        {
          return 0
        }
        else
        {
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if clickedIndex == 0
        {
           return "\((self.DataArray[row] as! NSDictionary)["region_name"]!)"
        }
        else if clickedIndex == 1
        {

        }
        else if clickedIndex == 2
        {

        }
        else
        {
            print("Nothing")
        }
        return "Something"
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if clickedIndex == 0
        {
          self.StateLbl.text = "\((self.DataArray[row] as! NSDictionary)["region_name"]!)"
          self.DataPickerView.isHidden = true
        }
        else if clickedIndex == 1
        {
          //do nothing
        }
        else if clickedIndex == 2
        {
          //do nothing
        }
    }

    //Mark:- //Get States From API
    func GetStatesList()
    {
        let langID = UserDefaults.standard.string(forKey: "langID")
        let CountryId = UserDefaults.standard.string(forKey: "countryID")
        let parameters = "lang_id=\(langID!)&mycountry_id=\(CountryId!)"
        self.CallAPI(urlString: "app_country_region_city", param: parameters, completion: {
            let infoarray = self.globalJson["info_array"] as! NSMutableArray
            for i in 0..<infoarray.count
            {
                let proDetail = infoarray[i] as! NSDictionary
                self.DataArray.add(proDetail )
            }
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.DataPickerView.delegate = self
                self.DataPickerView.dataSource = self
                self.DataPickerView.reloadAllComponents()
                SVProgressHUD.dismiss()
            }
        })
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
