//
//  DailyDealsListingViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 02/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage



class DailyDealsListingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var CategoryLbl: UILabel!
    
    var SelectedPickerRow : Int! = 0
    
    var sortSearch : String! = ""
    
    @IBOutlet weak var ShowSortView: UIView!
    
    @IBOutlet weak var SortPickerView: UIPickerView!
    
    
    @IBAction func SortBtnAction(_ sender: UIButton) {
        
        self.ShowSortView.isHidden = false
        
        self.SortPickerView.selectRow(0, inComponent: 0, animated: false)
    }
    
    var userTypeArray = [["key" : "" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: "")],["key" : "LP" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "lowest_price", comment: "")] , ["key" : "HP" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "highest_price", comment: "")] , ["key" : "T" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "title", comment: "")],["key" : "N" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "newest_listed", comment: "")],["key" : "C" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "closing_soon", comment: "")]]

    @IBAction func ListGridBtn(_ sender: UIButton) {
        
      //Changes done
    }
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    @IBOutlet weak var ListGridBtnOutlet: UIButton!
    
    @IBOutlet weak var LocalHeaderView: UIView!

    @IBOutlet weak var DailyDealsListingCollectionView: UICollectionView!
    
    @IBOutlet weak var DailyDealsListingTableView: UITableView!
    
    let dispatchGroup = DispatchGroup()
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    var StartValue : Int! = 0
    
    var PerLoad : String! = "10"
    
    var nextStart : String! = ""
    
    var InfoArray : NSDictionary!
    
    var ProductInfoArray : NSMutableArray!
    
    var ScrollBegin : CGFloat!
    
    var ScrollEnd : CGFloat!
    
    var watchListProductID : String! = ""
    
    //MARK:- //ViewDidLoad Method
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.ConfigureDesign()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateWatchlist), name: NSNotification.Name(rawValue: "UpdateWatchlist"), object: nil)
        
        self.SetLanguage()
        
        self.ListGridBtnOutlet.setImage(UIImage(named : "Group 18"), for: .normal)
        
        self.CreateFavouriteView(inview: self.view)
        
        self.ShowSortView.isHidden = true
        
        self.TapONSortView()
        
        self.StartDataListing()
        
        self.SortPickerView.delegate = self
        
        self.SortPickerView.dataSource = self
        
        self.SortPickerView.reloadAllComponents()
        
        self.DailyDealsListingTableView.isHidden = true
        
        self.DailyDealsListingCollectionView.isHidden = false
        
        self.DailyDealsListingCollectionView.alwaysBounceVertical = false
        
        self.DailyDealsListingCollectionView.alwaysBounceHorizontal = false

        // Do any additional setup after loading the view.
    }
    
    //MARK:- // Updation of Watchlist Status Using NSNotification
    
    @objc func updateWatchlist()
    {
        self.recordsArray.removeAllObjects()
        
        self.StartValue = 0
        
        self.StartDataListing()
    }
    
    //MARK:- //Set Layer For PickerView
    func ConfigureDesign()
    {
        self.SortPickerView.backgroundColor = .white
        
        self.ShowSortView.isUserInteractionEnabled = true
        
        self.CreateToolbar()
    }
    
    //MARK:- //Create Toolbar for Pickerview in Swift
    func CreateToolbar()
    {
        // ToolBar
        let toolBar = UIToolbar()
        
        toolBar.barStyle = .default
        
        toolBar.isTranslucent = true
        
        toolBar.tintColor = .white
        
        toolBar.backgroundColor = .black
        
        toolBar.barTintColor = .black
        
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "done", comment: ""), style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneClick))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: ""), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelClick))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        self.ShowSortView.addSubview(toolBar)
        
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        
        toolBar.leadingAnchor.constraint(equalTo: self.SortPickerView.leadingAnchor, constant: 0).isActive = true
        
        toolBar.trailingAnchor.constraint(equalTo: self.SortPickerView.trailingAnchor, constant: 0).isActive = true
        
        toolBar.topAnchor.constraint(equalTo: self.SortPickerView.topAnchor, constant: 0).isActive = true
        
        toolBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
    }
    
    @objc func doneClick()
    {
        sortSearch = "\((self.userTypeArray[SelectedPickerRow] as NSDictionary)["key"]!)"
        
        self.ShowSortView.isHidden = true
        
        self.StartValue = 0
        
        self.recordsArray.removeAllObjects()
        
        print("SortSearch",sortSearch!)
        
        self.StartDataListing()
        
        print("Done")
    }
    
    @objc func cancelClick()
    {
        self.ShowSortView.isHidden = true
        
        print("Cancel")
    }
    

    
    //MARK:- Global API Call for Product Listing
    
    func StartDataListing()
    {
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let parameters = "user_id=\(userID!)&mycountry_name=\(myCountryName!)&lang_id=\(langID!)&start_value=\(StartValue!)&per_value=\(PerLoad!)&sort_search=\(sortSearch!)"
        
        self.CallAPI(urlString: "app_daily_deals", param: parameters, completion: {
            
            self.InfoArray = self.globalJson["info_array"] as? NSDictionary

            if "\(self.globalJson["response"] as! Bool)".elementsEqual("true")
            {
                self.ProductInfoArray = self.InfoArray["product_info"] as? NSMutableArray
                
                for i in (0..<self.ProductInfoArray.count)
                {
                    self.recordsArray.add(self.ProductInfoArray[i] as! NSDictionary)
                }
                self.nextStart = "\(self.globalJson["next_start"]!)"
                
                self.StartValue = self.StartValue + self.ProductInfoArray.count

            }
            else
            {

            }
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                SVProgressHUD.dismiss()
                
                self.DailyDealsListingTableView.delegate = self
                
                self.DailyDealsListingTableView.dataSource = self
                
                self.DailyDealsListingTableView.reloadData()
                
                self.DailyDealsListingTableView.rowHeight = UITableView.automaticDimension
                
                self.DailyDealsListingTableView.estimatedRowHeight = 300

                self.DailyDealsListingCollectionView.delegate = self
                
                self.DailyDealsListingCollectionView.dataSource = self
                
                self.DailyDealsListingCollectionView.reloadData()
            }
        })
    }
    //MARK:- //UItbaleview Delegate and Datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return self.recordsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = tableView.dequeueReusableCell(withIdentifier: "dailydeals") as! DailyDealsTableViewCell
        
        obj.ProductName.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        
        obj.ListingTime.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_time_ago"]!)"
        
        obj.ClosingTime.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["expire_time_nw"]!)"
        
        obj.ProductAddress.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["address"]!)"

        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\((recordsArray[indexPath.row] as! NSDictionary)["currrency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)")

        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))

        obj.StartPrice.attributedText = attributeString

        obj.ReservePrice.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currrency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"

        obj.ProductType.text = ""
        
        let path = "\((self.recordsArray[indexPath.row] as! NSDictionary)["image"]!)"
        
        obj.ProductImage.sd_setImage(with: URL(string: path))
        
        obj.selectionStyle = .none
        
        obj.WatchlistBtn.tag = indexPath.row
        
        obj.WatchlistedBtn.tag = indexPath.row
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["wishlist_status"]!)".elementsEqual("1")
        {
//            obj.WatchlistBtn.setImage(UIImage(named: "heart_red"), for: .normal)
            obj.WatchlistedBtn.isHidden = false
            
            obj.WatchlistBtn.isHidden = true
            
            obj.WatchlistedBtn.addTarget(self, action: #selector(remove(sender:)), for: .touchUpInside)
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["wishlist_status"]!)".elementsEqual("0")
        {
            obj.WatchlistBtn.isHidden = false
            
            obj.WatchlistedBtn.isHidden = true
            
            obj.WatchlistBtn.addTarget(self, action: #selector(add(sender:)), for: .touchUpInside)
        }
        else
        {
            obj.WatchlistedBtn.isHidden = true
            
            obj.WatchlistBtn.isHidden = false
        }

        let progress = Float(Float("\((recordsArray[indexPath.row] as! NSDictionary)["percentage_sell"]!)")! / Float(100.0))
        obj.ProgressBar.setProgress(Float(progress), animated: true)

        return obj
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
        
        nav.ProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
        
        self.navigationController?.pushViewController(nav, animated: true)

    }

    //MARK:- //CollectionView Delegate and Datasource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.recordsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "dailydeals", for: indexPath) as! DailyDealsListingCollectionViewCell
        
        let path = "\((self.recordsArray[indexPath.row] as! NSDictionary)["image"]!)"
        
        obj.ProductImage.sd_setImage(with: URL(string: path))
        
        obj.ProductName.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
//        obj.ProductAddress.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["address"]!)"
//        obj.StartPrice.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currrency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"



        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\((recordsArray[indexPath.row] as! NSDictionary)["currrency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)")

        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))

        obj.StartPrice.attributedText = attributeString


//        obj.StartPrice.textColor = .green

        obj.ReservePrice.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currrency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
//        obj.ReservePrice.textColor = .red
        obj.ClosingTime.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["expire_time_nw"]!)"
        
        obj.ListingTime.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_time_ago"]!)"
        
        obj.biCount.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["bid_count"] ?? "")" + " bids"
        
        obj.WatchlistBtn.tag = indexPath.row
        
        obj.watchlistedBtn.tag = indexPath.row
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["wishlist_status"]!)".elementsEqual("1")
        {
            obj.WatchlistBtn.isHidden = true
            
            obj.watchlistedBtn.isHidden = false
            
            obj.watchlistedBtn.addTarget(self, action: #selector(remove(sender:)), for: .touchUpInside)
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["wishlist_status"]!)".elementsEqual("0")
        {
            obj.WatchlistBtn.isHidden = false
            
            obj.watchlistedBtn.isHidden = true
            
            obj.WatchlistBtn.addTarget(self, action: #selector(add(sender:)), for: .touchUpInside)
        }
        else
        {
            obj.WatchlistBtn.isHidden = true
            
            obj.watchlistedBtn.isHidden = false
        }
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type"]!)".elementsEqual("B")
        {
            obj.BuyNowView.isHidden = false
            
            obj.AuctionView.isHidden = true
        }
        else
        {
            obj.BuyNowView.isHidden = true
            
            obj.AuctionView.isHidden = false
        }

        let progress = Float(Float("\((recordsArray[indexPath.row] as! NSDictionary)["percentage_sell"]!)")! / Float(100.0))
        
        obj.ProgressBar.setProgress(Float(progress), animated: true)
        
        return obj
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }



    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let padding : CGFloat! = 4

        let collectionViewSize = DailyDealsListingCollectionView.frame.size.width - padding

        return CGSize(width: collectionViewSize/2, height: 245)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
        
        nav.ProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
        
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    
    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        ScrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        ScrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if ScrollBegin > ScrollEnd
        {

        }
        else
        {
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    
                SVProgressHUD.dismiss()
            }
            }
            else
            {
                self.StartDataListing()
            }
        }
    }

    @objc func add(sender: UIButton)
    {

        if userID?.isEmpty == true
        {
            let alert = UIAlertController.init(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""), preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.GlobalAddtoWatchlist(ProductID:  "\((recordsArray[sender.tag] as! NSDictionary)["product_id"]!)", completion: {
                
                self.recordsArray.removeAllObjects()
                
                self.StartValue = 0
                
                self.StartDataListing()
            })
        }


    }

    @objc func remove(sender: UIButton)
    {
        if userID?.isEmpty == true
        {
            let alert = UIAlertController.init(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""), preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.GlobalRemoveFromWatchlist(ProductID:  "\((recordsArray[sender.tag] as! NSDictionary)["product_id"]!)", completion: {
                
                self.recordsArray.removeAllObjects()
                
                self.StartValue = 0
                
                self.StartDataListing()
            })
        }
    }

    func TapONSortView()
    {
        self.ShowSortView.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)) )
        
        self.ShowSortView.addGestureRecognizer(tap)
    }
    @objc func handleTap(sender: UITapGestureRecognizer)
    {
        self.ShowSortView.isHidden = true
    }

    //MARK:- //PickerView Delegate and Datasource Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.userTypeArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "\((self.userTypeArray[row] as NSDictionary)["value"]!)"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.SelectedPickerRow = row
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView
    {
        var pickerLabel = UILabel()
        
        pickerLabel.textColor = UIColor.black
        
        pickerLabel.text = "\((self.userTypeArray[row] as NSDictionary)["value"]!)"
        
        pickerLabel.font = UIFont(name: "Verdana", size: 18)
        // In this use your custom font
        pickerLabel.textAlignment = NSTextAlignment.center
        
        return pickerLabel
    }

    //MARK:- //Set Data
    func SetLanguage()
    {
        self.CategoryLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Daily Deals", comment: "")
    }

}
//MARK:- //DAily Deals TableView Cell Class

class DailyDealsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var ProductImage: UIImageView!
    
    @IBOutlet weak var WatchlistBtn: UIButton!
    
    @IBOutlet weak var ProductName: UILabel!
    
    @IBOutlet weak var ListingTime: UILabel!
    
    @IBOutlet weak var ClosingTime: UILabel!
    
    @IBOutlet weak var ProductAddress: UILabel!
    
    @IBOutlet weak var ProductType: UILabel!
    
    @IBOutlet weak var StartPrice: UILabel!
    
    @IBOutlet weak var ReservePrice: UILabel!
    
    @IBOutlet weak var ProgressBar: UIProgressView!
    
    @IBOutlet weak var WatchlistedBtn: UIButton!

    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
//MARK:- //Daily Deals Listing TableviewCell
class DailyDealsListingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var ProductImage: UIImageView!
    
    @IBOutlet weak var ProductName: UILabel!
    
    @IBOutlet weak var ListingTime: UILabel!
    
    @IBOutlet weak var ClosingTime: UILabel!
    
    @IBOutlet weak var WatchlistBtn: UIButton!
    
    @IBOutlet weak var ProductAddress: UILabel!
    
    @IBOutlet weak var ProgressBar: UIProgressView!
    
    @IBOutlet weak var StartPrice: UILabel!
    
    @IBOutlet weak var ReservePrice: UILabel!
    
    @IBOutlet weak var watchlistedBtn: UIButton!
    
    @IBOutlet weak var ProductType: UILabel!
    
    @IBOutlet weak var BuyNowView: UIView!
    
    @IBOutlet weak var AuctionView: UIView!
    
    @IBOutlet weak var biCount: UILabel!
}


