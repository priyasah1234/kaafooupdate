//
//  ChatUserListViewController.swift
//  Kaafoo
//
//  Created by esolz on 22/10/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class ChatUserListViewController: GlobalViewController {
    var StartFrom : Int! = 0
    var PerPage : String! = "100"
    
    var recordsArray : NSMutableArray! = NSMutableArray()

    @IBOutlet weak var UsersListTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getListing()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- //Get Listing From API
    
    func getListing()
    {
        let UserID = UserDefaults.standard.string(forKey: "userID")
        let LangID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(UserID!)&lang_id=\(LangID!)&start_from=\(StartFrom!)&per_page=\(PerPage!)"
        self.CallAPI(urlString: "app_user_chat_listing", param: parameters, completion: {
            self.globalDispatchgroup.leave()
            let infoArray  = self.globalJson["info_array"] as! NSArray
            for i in 0..<infoArray.count
            {
                let tempdict = infoArray[i] as! NSDictionary
                self.recordsArray.add(tempdict)
            }
            self.globalDispatchgroup.notify(queue: .main, execute: {
                DispatchQueue.main.async {
                    self.UsersListTableView.delegate = self
                    self.UsersListTableView.dataSource = self
                    self.UsersListTableView.reloadData()
                    SVProgressHUD.dismiss()
                }
            })
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class UsersListTVC : UITableViewCell
{
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var UserStatusImageView: UIImageView!
    @IBOutlet weak var UserImage: UIImageView!
    
}

extension ChatUserListViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "users", for: indexPath) as! UsersListTVC
        cell.UserName.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["receiver_name"] ?? "")"
        let imageURL = "\((self.recordsArray[indexPath.row] as! NSDictionary)["receiver_image"] ?? "")"
        cell.UserImage.sd_setImage(with: URL(string: imageURL))
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["online_status"] ?? "")".elementsEqual("0")
        {
            cell.UserStatusImageView.image = UIImage(named: "")
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["online_status"] ?? "")".elementsEqual("1")
        {
           cell.UserStatusImageView.image = UIImage(named: "whatsapp")
        }
        else
        {
            //do nothing
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected")
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["block_status"] ?? "")".elementsEqual("1")
        {
            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "chatdetails") as! ChatDetailsViewController
            navigate.ShowTextInput = false
            navigate.receiverName = "\((self.recordsArray[indexPath.row] as! NSDictionary)["receiver_name"] ?? "")"
            navigate.SenderId = "\((self.recordsArray[indexPath.row] as! NSDictionary)["receiverid"] ?? "")"
            self.navigationController?.pushViewController(navigate, animated: true)
//            DispatchQueue.main.async {
//                self.ShowAlertMessage(title: "Warning", message: "This user is blocked")
//            }
        }
        else
        {
            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "chatdetails") as! ChatDetailsViewController
            navigate.ShowTextInput = true
            navigate.receiverName = "\((self.recordsArray[indexPath.row] as! NSDictionary)["receiver_name"] ?? "")"
            navigate.SenderId = "\((self.recordsArray[indexPath.row] as! NSDictionary)["receiverid"] ?? "")"
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
}
