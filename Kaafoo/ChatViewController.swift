//
//  ChatViewController.swift
//  Kaafoo
//
//  Created by esolz on 22/10/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import PusherSwift

class ChatViewController: GlobalViewController {

    var pusher: Pusher!
    @IBOutlet weak var MessageTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //push notifications
        
        let options = PusherClientOptions(
               host: .cluster("ap2")
             )

             pusher = Pusher(
               key: "e53a9c4a375968423d66",
               options: options
             )

             pusher.delegate = self

             // subscribe to channel
        
        //MARK:- //Friends who are Online Event CallBack
        
             let channel = pusher.subscribe("myonline-channel")
        let channelOne = pusher.subscribe("my-channel")

             // bind a callback to handle an event
        let _ = channel.bind(eventName: "myonline_event", callback: { (data: Any?) -> Void in
                   if let data = data as? [String : AnyObject] {
                       
                       print("Data==",data)
                    
                   }
               })
        
        //MARK:- //One Person to One Person Chat Section
        
        let _ = channelOne.bind(eventName: "my_event", callback: { (data: Any?) -> Void in
                           if let data = data as? [String : AnyObject] {
                               
                               print("DataOne==",data)
                            if let message = data["message"] as? String {
                                print("ChatMessage",message)
                                }
                           }
                       })

             pusher.connect()

        // Do any additional setup after loading the view.
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class MessageCell : UITableViewCell
{
    @IBOutlet weak var MessageAuthor: UILabel!
    @IBOutlet weak var MessageText: UILabel!
    @IBOutlet weak var MessageImageView: UIImageView!
}

struct Message {

    let author: String
    let message: String

    init(author: String, message: String) {
        self.author = author
        self.message = message
    }
}
