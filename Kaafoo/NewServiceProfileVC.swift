//
//  NewServiceProfileVC.swift
//  Kaafoo
//
//  Created by esolz on 30/09/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class NewServiceProfileVC: GlobalViewController {
    
    @IBOutlet weak var ReportViewBox: Report!
    
    @IBAction func HideReportViewBtn(_ sender: UIButton) {
        
        self.ReportBoxView.isHidden = true
        
    }
    @IBOutlet weak var HideReportViewBtnOutlet: UIButton!
    
    @IBOutlet weak var ReportBoxView: UIView!
    
    @IBAction func HideMessageViewBtn(_ sender: UIButton) {
        
        self.MessageViewBOX.isHidden = true
        
    }
    @IBOutlet weak var HideMessageViewBtnOutlet: UIButton!
    
    @IBOutlet weak var MessageViewBOX: UIView!
    
    @IBOutlet weak var MessageBoxView: MessageBox!
    
    @IBOutlet weak var totalRating: UILabel!
    
    @IBOutlet weak var totalFeedback: UILabel!
    
    @IBOutlet weak var SelectedServiceName: UILabel!
    
    @IBOutlet weak var MainScroll: UIScrollView!
    
    @IBOutlet weak var ScrollContentView: UIView!
    
    @IBOutlet weak var BannerImageView: UIImageView!
    
    @IBOutlet weak var ProfileImageView: UIImageView!
    
    @IBOutlet weak var DetailsOneView: UIView!
    
    @IBOutlet weak var tapView: UIView!
    
    @IBOutlet weak var QRView: UIView!
    
    @IBOutlet weak var RatingDetailsView: UIView!
    
    @IBOutlet weak var FeedBackDetailsView: UIView!
    
    @IBOutlet weak var QRImageView: UIImageView!
    
    @IBOutlet weak var FeedbackView: UIView!
    
    @IBOutlet weak var BusinessImagesView: UIView!
    
    @IBOutlet weak var AccountDetailsView: UIView!
    
    @IBOutlet weak var SendMessageBtnOutlet: UIButton!
    
    @IBAction func SendMessageBtn(_ sender: UIButton) {

        self.MessageBoxView.MessageTextView.text = ""
        
        self.MessageViewBOX.isHidden = false
        
    }
    
    @IBOutlet weak var PickerDetailsView: UIView!
    
    @IBOutlet weak var SearchBtnOutlet: UIButton!
    
    @IBAction func SearchButton(_ sender: UIButton) {
        
    }
    
    @IBOutlet weak var MapButtonOutlet: UIButton!
    
    @IBAction func MapButton(_ sender: UIButton) {
        
        let contact = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "contactVC") as! ContactViewController
        
         self.navigationController?.pushViewController(contact, animated: true)
    }
    
    @IBOutlet weak var dropDownImageView: UIImageView!
    
    @IBOutlet weak var PickerShowButtonOutlet: UIButton!
    
    @IBAction func PickerShowButton(_ sender: UIButton) {
        
    }
    
    @IBOutlet weak var ServiceName: UILabel!
    
    @IBOutlet weak var tapCollectionView: UICollectionView!
    
    
    
    
    let UserID = UserDefaults.standard.string(forKey: "userID")
    
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    var selleRID : String! = ""
    
    
    
    let catID = UserDefaults.standard.string(forKey: "catID")
    
    
    var SellerId : String! = ""
    
    var SellerInfoDict : NSDictionary!
    
    var CategoryListArray : NSMutableArray! = NSMutableArray()
    
    var ServiceListArray : NSMutableArray! = NSMutableArray()
    
    var listingTypeArray : NSArray! = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "company_profile", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "listing", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "reviews", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "contact", comment: "")]
    
    @IBOutlet weak var AccountNumber: UILabel!
    
    @IBOutlet weak var MemberSince: UILabel!
    
    @IBOutlet weak var ReportBtnOutlet: UIButton!
    
    @IBAction func ReportBtn(_ sender: UIButton) {
//        self.ShowAlertMessage(title: "Warning", message: "You can not report your own profile")
        self.ReportViewBox.ReportTxtView.text = ""
        
        self.ReportBoxView.isHidden = false
    }
    @IBOutlet weak var TotalViews: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
//        self.view.isHidden = true
        
        self.MessageViewBOX.isHidden = true
        
        self.ReportBoxView.isHidden = true
        
        self.setLayer()
        
        self.SetMessageBOX()
        
        self.SetReportBox()
        
        self.getData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        self.selleRID = UserDefaults.standard.string(forKey: "sellerID")
        
        self.SelectedServiceName.text = ServiceDetailsParameters.shared.SelectedService ?? ""
    }
    
    func setData()
    {
        headerView.headerViewTitle.text = "\(self.SellerInfoDict["businessname"] ?? "")"
        
        let bannerImageURL = "\(self.SellerInfoDict["banner"] ?? "")"
        
        let logoImageURL = "\(self.SellerInfoDict["logo"] ?? "")"
        
        let QRImageURL = "\(self.SellerInfoDict["qrcode_img"] ?? "")"
        
        self.BannerImageView.sd_setImage(with: URL(string: bannerImageURL))
        
        self.ProfileImageView.sd_setImage(with: URL(string: logoImageURL))
        
        self.QRImageView.sd_setImage(with: URL(string: QRImageURL))
        
        self.AccountNumber.text = "Account Number : " + "\(self.SellerInfoDict["account_no"] ?? "")"
        
        self.MemberSince.text = "Member Since : " + "\(self.SellerInfoDict["member_since"] ?? "")"
        
        self.TotalViews.text = "Views : " + "\(self.SellerInfoDict["total_view"] ?? "")"
        
        self.ServiceName.text = "\(self.SellerInfoDict["businessname"] ?? "")"
        
        self.totalFeedback.text = "Total Feedback : " + "\(self.SellerInfoDict["total_feedback"] ?? "")"
        
        self.totalRating.text = "\(self.SellerInfoDict["total_review"] ?? "")" + " Out Of 5 Stars"
    }
    
    func setLayer()
    {
        self.FeedBackDetailsView.layer.borderWidth = 1.0
        
        self.FeedBackDetailsView.layer.borderColor = UIColor.gray.cgColor
        
        self.RatingDetailsView.layer.borderWidth = 1.0
        
        self.RatingDetailsView.layer.borderColor = UIColor.gray.cgColor
        
        self.tapCollectionView.delegate = self
        
        self.tapCollectionView.dataSource = self
        
        self.tapCollectionView.reloadData()
        
        self.SendMessageBtnOutlet.titleLabel?.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 16)))
        
        headerView.headerViewTitle.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 20)))
        
        self.tapCollectionView.showsVerticalScrollIndicator = false
        
        self.tapCollectionView.showsHorizontalScrollIndicator = false
        
        self.MessageBoxView.layer.borderWidth = 1.0
        
        self.MessageBoxView.layer.borderColor = UIColor.black.cgColor
        
        self.MessageBoxView.clipsToBounds = true
        
        self.MessageBoxView.layer.cornerRadius = 3.0
        
        self.ReportViewBox.layer.borderWidth = 1.0
        
        self.ReportViewBox.layer.borderColor = UIColor.black.cgColor
        
        self.ReportViewBox.clipsToBounds = true
        
        self.ReportViewBox.layer.cornerRadius = 3.0
    }
    
    //MARK:- //SET MESSAGE BOX Functions
    
    func SetMessageBOX()
    {
        self.MessageBoxView.MessageTextView.text = ""
        
        self.MessageBoxView.CancelBtnOutlet.addTarget(self, action: #selector(SetMessageBoxHidden(sender:)), for: .touchUpInside)
        
        self.MessageBoxView.SendBtnOutlet.addTarget(self, action: #selector(SetSendmessageToUser(sender:)), for: .touchUpInside)
    }
    
    //MARK:- //SET REPORT BOX Functions
    
    func SetReportBox()
    {
        self.ReportViewBox.ReportTxtView.text = ""
        
        self.ReportViewBox.CrossBtnOutlet.addTarget(self, action: #selector(SetReportBoxHidden(sender:)), for: .touchUpInside)
        
        self.ReportViewBox.SendBtnOutlet.addTarget(self, action: #selector(SetReportSendToOwner(sender:)), for: .touchUpInside)
    }
    
    @objc func SetReportBoxHidden(sender : UIButton)
    {
        self.ReportBoxView.isHidden = true
    }
    
    @objc func SetReportSendToOwner(sender : UIButton)
    {
        if "\(UserID!)".elementsEqual("\(selleRID!)")
        {
            self.ShowAlertMessage(title: "Warning", message: "You can not send report to yourself")
        }
        else
        {
            if self.ReportViewBox.ReportTxtView.text.isEmpty == true
            {
                self.ShowAlertMessage(title: "Warning", message: "You can not report blank text message")
            }
            else
            {
                self.reportUser()
            }
        }
    }
    
    @objc func SetMessageBoxHidden(sender : UIButton)
    {
        self.MessageViewBOX.isHidden = true
    }
    
    @objc func SetSendmessageToUser(sender : UIButton)
    {
        if "\(UserID!)".elementsEqual("\(selleRID!)")
        {
            self.ShowAlertMessage(title: "Warning", message: "You Can not send message to yourself")
        }
        else
        {
            if self.MessageBoxView.MessageTextView.text.isEmpty == true
            {
                self.ShowAlertMessage(title: "Warning", message: "Text message can not be blank")
            }
            else
            {
                self.sendMessage()
            }
        }
    }
    
    //MARK:- // JSon Post To Send Report Message Against User
    
    func reportUser()
    {
        let parameters = "lang_id=\(LangID!)&reportuser_id=\(selleRID!)&login_id=\(UserID!)&report_msg=\(self.ReportViewBox.ReportTxtView.text ?? "")"
        
        self.CallAPI(urlString: "app_report_user_send", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    
                    self.ReportBoxView.isHidden = true
                    
                    self.ShowAlertMessage(title: "Warning", message: "\(self.globalJson["message"] ?? "")")
                }
            })
        })
    }

    
    // MARK:- // JSON Post Method to Send Message to User

      func sendMessage() {
        
          var parameters : String = ""
        
          let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&seller_id=\(selleRID!)&message=\(self.MessageBoxView.MessageTextView.text!)"

          self.CallAPI(urlString: "app_friend_send_message", param: parameters) {
            
              DispatchQueue.main.async {
                
                  self.globalDispatchgroup.leave()
                
                  SVProgressHUD.dismiss()
                
                  self.MessageBoxView.MessageTextView.text = ""
              }
          }
          self.globalDispatchgroup.notify(queue: .main) {

              self.customAlert(title: "Submit Success", message: "\(self.globalJson["message"] ?? "")", completion: {

                  UIView.animate(withDuration: 0.3, animations: {

                      self.MessageViewBOX.isHidden = true
                  })
              })
          }
      }
    
    func getData()
    {
        print("SellerID",selleRID ?? "")
        
        let parameters = "login_id=\(UserID!)&lang_id=\(LangID!)&seller_id=\(selleRID!)"
        
        self.CallAPI(urlString: "app_service_user_details_main", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            SVProgressHUD.dismiss()
            
            let infoArrayDict = self.globalJson["info_array"] as! NSDictionary
            
            self.SellerInfoDict = (infoArrayDict["seller_info"] as! NSDictionary)
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                DispatchQueue.main.async {
                    
                    self.setData()
                }
                
                self.view.isHidden = false
            })
        })
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class tapCollectionCVC : UICollectionViewCell
{
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var tapLabelName: UILabel!
    
    @IBOutlet weak var SeparatorView: UIView!
    
    override var isSelected: Bool {
        
        didSet {
            
            if self.isSelected {
                
                tapLabelName.textColor = .black
            }
            else
            {
                tapLabelName.textColor = .gray
            }
        }
    }
}

extension NewServiceProfileVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.listingTypeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tap", for: indexPath) as! tapCollectionCVC
        
        cell.tapLabelName.text = "\(self.listingTypeArray[indexPath.item])"
//        let attriString = NSAttributedString(string:"\(self.listingTypeArray[indexPath.item])", attributes:
//            [NSAttributedString.Key.foregroundColor: UIColor.lightGray,
//             NSAttributedString.Key.font: "Lato-Regular"])
//        cell.tapLabelName.attributedText = attriString
        cell.tapLabelName.textColor = .gray
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size: CGSize = "\(self.listingTypeArray[indexPath.row])".size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0)])
        
        return CGSize(width: size.width + 60, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tap", for: indexPath) as! tapCollectionCVC
        
        cell.isSelected = true
        
        if indexPath.row == 1
        {
            let bookATime = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "servicelisting") as! ServiceListingViewController
            
            self.navigationController?.pushViewController(bookATime, animated: true)
        }
        else if indexPath.row == 2
        {
             let reviews = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "reviewsVC") as! ReviewsViewController
            
            self.navigationController?.pushViewController(reviews, animated: true)
        }
        else if indexPath.row == 3
        {
            let contact = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "contactVC") as! ContactViewController
            
            self.navigationController?.pushViewController(contact, animated: true)
        }
    }
}
