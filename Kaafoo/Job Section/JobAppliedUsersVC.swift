//
//  JobAppliedUsersVC.swift
//  Kaafoo
//
//  Created by esolz on 02/01/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class JobAppliedUsersVC: GlobalViewController {
    
    var ListingArray : NSMutableArray! = NSMutableArray()
    
    @IBOutlet weak var AppliedUsersTV: UITableView!
    
    var JobID : String! = ""
    
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    let UserID = UserDefaults.standard.string(forKey: "userID")
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
    
        self.setLayerConfiguration()
        
        self.AppliedUsersTV.delegate = self
        
        self.AppliedUsersTV.dataSource = self
        
        self.AppliedUsersTV.reloadData()
        
        self.getListing()
        
        // Do any additional setup after loading the view.
    }
    
    func setLayerConfiguration()
    {
        self.AppliedUsersTV.separatorStyle = .none
    }
    
    
    func getListing()
    {
        let parameters = "user_id=\(UserID ?? "")&lang_id=\(LangID!)&job_id=\(self.JobID ?? "")"
            
        self.CallAPI(urlString: "job-user-list", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            self.ListingArray = (self.globalJson["list_info"] as! NSMutableArray)
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    
                    self.AppliedUsersTV.delegate = self
                    
                    self.AppliedUsersTV.dataSource = self
                    
                    self.AppliedUsersTV.reloadData()
                    
                }
            })
            
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class JobAppliedTVC : UITableViewCell
{
    @IBOutlet weak var ViewContent: UIView!
    
    @IBOutlet weak var appliedName: UILabel!
    
    @IBOutlet weak var applierMessage: UILabel!
    
    @IBOutlet weak var applierPhoneNumber: UILabel!
    
    @IBOutlet weak var applierEmail: UILabel!
    
}

extension JobAppliedUsersVC : UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.ListingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "jobapplied") as! JobAppliedTVC
        
        cell.appliedName.text = "\((self.ListingArray[indexPath.row] as! NSDictionary)["Name"] ?? "")"
        
        cell.applierMessage.text = "\((self.ListingArray[indexPath.row] as! NSDictionary)["Message"] ?? "")"
        
        cell.applierPhoneNumber.text = "\((self.ListingArray[indexPath.row] as! NSDictionary)["Phone"] ?? "")"
        
        cell.applierEmail.text = "\((self.ListingArray[indexPath.row] as! NSDictionary)["Email"] ?? "")"
        
        cell.ViewContent.layer.cornerRadius = 8.0
        
        cell.ViewContent.clipsToBounds = true
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
}
