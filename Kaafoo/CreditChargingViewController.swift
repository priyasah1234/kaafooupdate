//
//  CreditChargingViewController.swift
//  Kaafoo
//
//  Created by esolz on 17/10/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD


class CreditChargingViewController: GlobalViewController {

    //Outlet Connections
    @IBOutlet weak var RechargeDetailsView: UIView!
    @IBOutlet weak var RechargeListTableView: UITableView!
    @IBOutlet weak var WalletBalance: UILabel!
    @IBOutlet weak var rechargeBtnOutlet: UIButton!
    @IBAction func rechargeBtn(_ sender: UIButton) {
       
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "recharge") as! RechargeWalletViewController
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    var StartValue : Int! = 0
    var perLoad : Int! = 20
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLayer()
        self.getListings()
        // Do any additional setup after loading the view.
    }
    
    func setLayer()
    {
        self.rechargeBtnOutlet.layer.cornerRadius = 15.0
        self.rechargeBtnOutlet.clipsToBounds = true
    }
    
    func getListings()
    {
        let UserID = UserDefaults.standard.string(forKey: "userID")
        let LangID = UserDefaults.standard.string(forKey: "langID")
        let parameter = "user_id=\(UserID!)&start_value=\(StartValue!)&per_load=\(perLoad!)&lang_id=\(LangID!)"
        self.CallAPI(urlString: "wallet_list", param: parameter, completion: {
            self.globalDispatchgroup.leave()
            self.globalDispatchgroup.notify(queue: .main, execute: {
                let infoArray = self.globalJson["info_array"] as! NSArray
                for i in 0..<infoArray.count
                {
                    let tempDict = infoArray[i] as! NSDictionary
                    self.recordsArray.add(tempDict)
                }
                
                self.RechargeListTableView.delegate = self
                self.RechargeListTableView.dataSource = self
                self.RechargeListTableView.reloadData()
                self.WalletBalance.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "amount", comment: "") + " : " + "\(self.globalJson["amount"] ?? "")"
                SVProgressHUD.dismiss()
            })
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class CreditListingTVC : UITableViewCell
{
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var transactionIDLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var transactionID: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var transactionTypeLbl: UILabel!
    @IBOutlet weak var transactionType: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var creditAmount: UILabel!
}

extension CreditChargingViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "credit", for: indexPath) as! CreditListingTVC
        cell.ContentView.layer.cornerRadius = 8.0
        cell.ContentView.clipsToBounds = true
        cell.transactionIDLbl.textColor = .yellow2()
        cell.transactionIDLbl.textColor = .darkGray
        cell.amountLbl.textColor = .darkGray
        cell.dateLbl.textColor = .darkGray
        cell.transactionID.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["payment_id"] ?? "")"
        cell.date.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["payment_date"] ?? "")"
        cell.transactionType.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["message"] ?? "")"
        cell.creditAmount.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency"] ?? "")" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["amount"] ?? "")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
