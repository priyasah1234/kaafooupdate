//
//  ForgotPasswordViewController.swift
//  Kaafoo
//
//  Created by admin on 24/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ForgotPasswordViewController: GlobalViewController,UITextFieldDelegate {
    
    // Global Font Applied
    @IBOutlet weak var SubmitBtnOutlet: UIButton!
    @IBOutlet weak var UpdateBtnOutlet: UIButton!
    
    
    @IBOutlet weak var NewPasswordTxt: UITextField!
    @IBOutlet weak var ConfirmPasswordTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    
    
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var verificationTxt: UITextField!
    
    
    @IBOutlet weak var forgotLbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var emailTXT: UITextField!
    @IBOutlet weak var verificationCodeLbl: UILabel!
    @IBOutlet weak var verificationCodeTXT: UITextField!
    @IBOutlet weak var newPasswordLbl: UILabel!
    @IBOutlet weak var newPasswordTXT: UITextField!
    @IBOutlet weak var confirmPasswordLbl: UILabel!
    @IBOutlet weak var confirmPasswordTXT: UITextField!
    
    @IBOutlet weak var resetPasswordView: UIView!
    
    let dispatchGroup = DispatchGroup()
    
    
    @IBAction func SubmitBtn(_ sender: UIButton) {
        if "\(self.emailTxt.text!)".elementsEqual("")
        {
            self.ShowAlertMessage(title: "Alert", message: "Email address can not be blank")
        }
        else
        {
            self.forgotPassword()
            
        }
        
    }
    
    @IBAction func UpdateBtn(_ sender: UIButton) {
        if self.verificationTxt.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Alert", message: "Enter Your Verification Code")
        }
        else if self.NewPasswordTxt.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Alert", message: "Enter Your new password")
        }
        else if self.ConfirmPasswordTxt.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Alert", message: "Confirm Your new password")
        }
        else if "\(self.NewPasswordTxt.text)".elementsEqual("\(self.ConfirmPasswordTxt.text)")
        {
            self.resetPassword()
        }
        else
        {
            self.ShowAlertMessage(title: "Alert", message: "Your Password does not match")
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetLayer()
//        setAttributedTitle(toLabel: forgotLbl, boldText: "FORGOT", boldTextFont: UIFont(name: forgotLbl.font.fontName, size: CGFloat(Get_fontSize(size: 28)))!, normalTextFont: UIFont(name: (emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 28)))!, normalText: " PASSWORD")
//        
//        set_font()
//        
//        emailTXT.delegate = self
//        verificationCodeTXT.delegate = self
//        newPasswordTXT.delegate = self
//        confirmPasswordTXT.delegate = self
//
//        submitOutlet.layer.cornerRadius = submitOutlet.frame.size.height / 2
//        
//        updateOutlet.layer.cornerRadius = updateOutlet.frame.size.height / 2
        
    }
    
    //MARK:- //Set Text
    
    func SetLayer()
    {
        self.SubmitBtnOutlet.layer.cornerRadius = self.SubmitBtnOutlet.frame.size.height / 2
        self.UpdateBtnOutlet.layer.cornerRadius = self.UpdateBtnOutlet.frame.size.height / 2
        
        self.verificationTxt.isHidden = true
        self.NewPasswordTxt.isHidden = true
        self.ConfirmPasswordTxt.isHidden = true
        self.UpdateBtnOutlet.isHidden = true
    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Click on Email
    
    
    @IBOutlet weak var clickOnEmailOutlet: UIButton!
    @IBAction func clickOnEmail(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.emailLbl.frame.origin.y = (self.emailLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.emailLbl.font = UIFont(name: (self.emailLbl.font?.fontName)!,size: 11)
            self.emailLbl.font = UIFont(name: self.emailLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnEmailOutlet.isUserInteractionEnabled = false
            self.emailTXT.isUserInteractionEnabled = true
            self.emailTXT.becomeFirstResponder()
            
        }
        
    }
    
    
    // MARK:- // Click on Verification Code
    
    @IBOutlet weak var clickOnVerificationCodeOutlet: UIButton!
    @IBAction func clickOnVerificationCode(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.verificationCodeLbl.frame.origin.y = (self.verificationCodeLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.verificationCodeLbl.font = UIFont(name: (self.verificationCodeLbl.font?.fontName)!,size: 11)
            self.verificationCodeLbl.font = UIFont(name: self.verificationCodeLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnVerificationCodeOutlet.isUserInteractionEnabled = false
            self.verificationCodeTXT.isUserInteractionEnabled = true
            self.verificationCodeTXT.becomeFirstResponder()
            if (self.newPasswordTXT.text?.isEmpty)!
            {
                self.newPasswordGetBackInPosition()
                if (self.confirmPasswordTXT.text?.isEmpty)!
                {
                    self.confirmPasswordGetBackInPosition()
                }
            }
            else if (self.confirmPasswordTXT.text?.isEmpty)!
            {
                self.confirmPasswordGetBackInPosition()
            }
            
        }
        
    }
    
    
    // MARK:- // Click on Password
    
    
    @IBOutlet weak var clickOnNewPasswordOutlet: UIButton!
    @IBAction func clickOnNewPassword(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.newPasswordLbl.frame.origin.y = (self.newPasswordLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.newPasswordLbl.font = UIFont(name: (self.newPasswordLbl.font?.fontName)!,size: 11)
            self.newPasswordLbl.font = UIFont(name: self.newPasswordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnNewPasswordOutlet.isUserInteractionEnabled = false
            self.newPasswordTXT.isUserInteractionEnabled = true
            self.newPasswordTXT.becomeFirstResponder()
            if (self.verificationCodeTXT.text?.isEmpty)!
            {
                self.verificationCodeGetBackInPosition()
                if (self.confirmPasswordTXT.text?.isEmpty)!
                {
                    self.confirmPasswordGetBackInPosition()
                }
            }
            else if (self.confirmPasswordTXT.text?.isEmpty)!
            {
                self.confirmPasswordGetBackInPosition()
            }
            
        }
        
    }
    
    
    // MARK:- // Click on Confirm New Password
    
    
    @IBOutlet weak var clickOnConfirmPasswordOutlet: UIButton!
    @IBAction func clickOnConfirmPassword(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.confirmPasswordLbl.frame.origin.y = (self.confirmPasswordLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.confirmPasswordLbl.font = UIFont(name: (self.confirmPasswordLbl.font?.fontName)!,size: 11)
            self.confirmPasswordLbl.font = UIFont(name: self.confirmPasswordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnConfirmPasswordOutlet.isUserInteractionEnabled = false
            self.confirmPasswordTXT.isUserInteractionEnabled = true
            self.confirmPasswordTXT.becomeFirstResponder()
            if (self.verificationCodeTXT.text?.isEmpty)!
            {
                self.verificationCodeGetBackInPosition()
                if (self.newPasswordTXT.text?.isEmpty)!
                {
                    self.newPasswordGetBackInPosition()
                }
            }
            else if (self.newPasswordTXT.text?.isEmpty)!
            {
                self.newPasswordGetBackInPosition()
            }
            
        }
        
    }
    
    
    // MARK:- // Submit Button
    
    
    @IBOutlet weak var submitOutlet: UIButton!
    @IBAction func submit(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.forgotPassword()
            
            self.dispatchGroup.notify(queue: .main, execute: {
                self.resetPasswordView.isHidden = false
            })
            
        }
        
    }
    
    
    
    // MARK:- // Update Button
    
    
    
    @IBOutlet weak var updateOutlet: UIButton!
    @IBAction func update(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.resetPassword()
            
            self.dispatchGroup.notify(queue: .main, execute: {
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController
                self.navigationController?.pushViewController(navigate, animated: true)
            })
            
        }
        
    }
    
    
    // MARK:- // Defined Functions
    
    // MARK:- // Verification Code get back in position
    
    
    func verificationCodeGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.verificationCodeLbl.frame.origin.y = self.verificationCodeTXT.frame.origin.y
            self.verificationCodeLbl.font = UIFont(name: (self.verificationCodeLbl.font?.fontName)!,size: 16)
            self.verificationCodeLbl.font = UIFont(name: self.verificationCodeLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnVerificationCodeOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    // MARK:- // Email get back in position
    
    func emailGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.emailLbl.frame.origin.y = self.emailTXT.frame.origin.y
            //self.emailLbl.font = UIFont(name: (self.emailTXT.font?.fontName)!,size: 16)
            self.emailLbl.font = UIFont(name: self.emailLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnEmailOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    // MARK:- // New password get back in position
    
    func newPasswordGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.newPasswordLbl.frame.origin.y = self.newPasswordTXT.frame.origin.y
            //self.newPasswordLbl.font = UIFont(name: (self.newPasswordLbl.font?.fontName)!,size: 16)
            self.newPasswordLbl.font = UIFont(name: self.newPasswordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnNewPasswordOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    // MARK:- // Confirm Password get back in position
    
    func confirmPasswordGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.confirmPasswordLbl.frame.origin.y = self.confirmPasswordTXT.frame.origin.y
            self.confirmPasswordLbl.font = UIFont(name: (self.confirmPasswordLbl.font?.fontName)!,size: 16)
            self.confirmPasswordLbl.font = UIFont(name: self.confirmPasswordLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnConfirmPasswordOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    
    
    // MARK: - // JSON POST Method to submit Forgot Password Request
    
    func forgotPassword()
        
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_forgot_password")!   //change the url
        
        var parameters : String = ""
        
        parameters = "login_email=\(emailTxt.text!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Forgot Password Response: " , json)
                    
                    if "\(json["response"] as! Bool)".elementsEqual("true")
                    {
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }
                        
                        self.dispatchGroup.leave()
                        
                        self.dispatchGroup.notify(queue: .main, execute: {
                            DispatchQueue.main.async {
                                self.verificationTxt.isHidden = false
                                self.NewPasswordTxt.isHidden = false
                                self.ConfirmPasswordTxt.isHidden = false
                                self.UpdateBtnOutlet.isHidden = false
                            }
                        })
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.ShowAlertMessage(title: "Alert", message: "\(json["message"] ?? "")")
                        }
                        
                        self.dispatchGroup.leave()
                    }
                    
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK: - // JSON POST Method to submit Reset Password Request
    
    func resetPassword()
        
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_password_reset")!   //change the url
        
        var parameters : String = ""
        
        parameters = "user_reset_code=\(verificationTxt.text!)&new_password=\(NewPasswordTxt.text!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Reset Password Response: " , json)
                    
                    if "\(json["response"] as! Bool)".elementsEqual("true")
                    {
                        DispatchQueue.main.async {
                                               SVProgressHUD.dismiss()
                                           }
                                           
                                           self.dispatchGroup.leave()
                                           
                                           self.dispatchGroup.notify(queue: .main, execute: {
                                               DispatchQueue.main.async {
                                           let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController
                                           self.navigationController?.pushViewController(navigate, animated: true)
                                               }
                                           })
                                           
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.ShowAlertMessage(title: "Alert", message: "\(json["message"] ?? "")")
                        }
                        
                        self.dispatchGroup.leave()
                    }
                    
                   
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    
    // MARK:- // Textfield Delegates
    
    // For pressing return on the keyboard to dismiss keyboard
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        emailTXT.resignFirstResponder()
        verificationCodeTXT.resignFirstResponder()
        newPasswordTXT.resignFirstResponder()
        confirmPasswordTXT.resignFirstResponder()
        mainScroll.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        
        if (self.emailTXT.text?.isEmpty)!
        {
            self.emailGetBackInPosition()
        }
        
        if (self.verificationCodeTXT.text?.isEmpty)!
        {
            self.verificationCodeGetBackInPosition()
        }
        
        if (self.newPasswordTXT.text?.isEmpty)!
        {
            self.newPasswordGetBackInPosition()
        }
        
        if (self.confirmPasswordTXT.text?.isEmpty)!
        {
            self.confirmPasswordGetBackInPosition()
        }
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == verificationCodeTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (10/568)*self.FullHeight), animated: true)
            
        }
        else if textField == newPasswordTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (95/568)*self.FullHeight), animated: true)
            
        }
        else if textField == confirmPasswordTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (170/568)*self.FullHeight), animated: true)
            
        }
        
    }
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
        //        signLbl.font = UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        //        upLbl.font = UIFont(name: upLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        emailLbl.font = UIFont(name: emailLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        emailTXT.font = UIFont(name: (emailTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        verificationCodeLbl.font = UIFont(name: verificationCodeLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        verificationCodeTXT.font = UIFont(name: (verificationCodeTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        newPasswordLbl.font = UIFont(name: newPasswordLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        newPasswordTXT.font = UIFont(name: (newPasswordTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        confirmPasswordLbl.font = UIFont(name: confirmPasswordLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        confirmPasswordTXT.font = UIFont(name: (confirmPasswordTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        
        submitOutlet.titleLabel?.font = UIFont(name: (submitOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!
        updateOutlet.titleLabel?.font = UIFont(name: (updateOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!
        
        
    }
    

    
}
