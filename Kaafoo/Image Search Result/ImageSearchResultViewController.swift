//
//  ImageSearchResultViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 15/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class ImageSearchResultViewController: GlobalViewController {

    
    @IBOutlet weak var SortPickerView: UIPickerView!
    
    @IBOutlet weak var SortView: UIView!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var listView: UIView!
    
    @IBAction func SortBtn(_ sender: UIButton) {
        
        self.SortView.isHidden = false
    }
    
    @IBOutlet weak var SortBtnOutlet: UIButton!
    
    @IBOutlet weak var listTableview: UITableView!
    
    @IBOutlet weak var gridView: UIView!
    
    @IBOutlet weak var gridCollectionview: UICollectionView!
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    var imageData = Data()
    
    var recordsArray = NSArray()
    
    let dispatchGroup = DispatchGroup()
    
    var watchListProductID = String()

    var SortDict = [["key" : "" , "value" : "All"],["key" : "HP" , "value" : "High Priced"],["key" : "LP" , "value" : "Low Priced"],["key" : "C" , "value" : "Closing Soon"],["key" : "N" , "value" : "Newly Listed"],["key" : "B" , "value" : "Most Bids"],["key" : "T" , "value" : "Title"]]
    
    
    // MARK:- // ViewDidload
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.mainView.isHidden = true
        
        self.searchByImage()
        
        self.dispatchGroup.notify(queue: .main) {
            
            if self.recordsArray.count > 0 {
                
                self.mainView.isHidden = false
            }
            else {
                
                self.mainView.isHidden = true
            }
            
        }

    }
    
    
    // MARK:- // ViewWillAppear
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.listTableview.estimatedRowHeight = 400
        
        self.listTableview.rowHeight = UITableView.automaticDimension
        
    }
    
    
    // MARK:- // Buttons
    
    // MARK:- // Change between List and Grid Button
    
    @IBOutlet weak var changeBetweenListAndGridOutlet: UIButton!
    
    @IBAction func changeBetweenListAndGrid(_ sender: UIButton) {
        
        if self.listView.isHidden == false
        {
            self.gridView.isHidden = false
            
            self.listView.isHidden = true
            
            self.changeBetweenListAndGridOutlet.setImage(UIImage(named: "Group 18"), for: .normal)
        }
        else
        {
            self.gridView.isHidden = true
            
            self.listView.isHidden = false
            
            self.changeBetweenListAndGridOutlet.setImage(UIImage(named: "list-1"), for: .normal)
        }
        
    }
    
    
    
    //MARK:- // WatchListBtn Selector Method
    
    @objc func addToWatchlist(sender : UIButton)
    {
        if userID?.isEmpty == true
        {
            let alert = UIAlertController(title: "Warning", message: "Please login and try gain later", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Warning", message: "Product is added to watchlist", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: {
                
                self.watchListProductID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"
                
                self.addToWatchList {
                    
                    self.searchByImage()
                    
                }
            })
        }
    }
    
    
    
    // MARK:- // Remove From Watchlist Selector Method
    
    @objc func Removefromwatchlist(sender : UIButton)
    {
        if userID?.isEmpty == true
        {
            let alert = UIAlertController.init(title: "Warning", message: "Please login and try gain later", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "warning", message: "Product is removed from Watchlist", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: {
                
                self.watchListProductID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"
                
                self.removeFromWatchList()
                    {
                        self.searchByImage()
                        
//                        self.dispatchGroup.notify(queue: .main, execute: {
//
//                            self.listTableview.reloadData()
//                            self.gridCollectionview.reloadData()
//
//                        })
                }
            })
        }
    }
    
    
    
    
    
    // MARK: - // JSON POST Method to Search By Image
    
    
    func searchByImage() {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let parameters = [
            "lang_id" : "\(langID!)" , "user_id" : "\(self.userID!)"]
        
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                
                for (key, value) in parameters {
                    MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
                MultipartFormData.append(self.imageData, withName: "image_encoded", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                
        }, to: (GLOBALAPI + "ios_search_product_image"))
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
//
                    print(response.request!)  // original URL request
                    
                    print(response.response!) // URL response
                    
                    print(response.data!)     // server data
                    
                    print(response.result)   // result of response serialization

                    if let JSON = response.result.value {
                        
                        print("Image Search Response-----: \(JSON)")
                        
                        self.recordsArray = ((JSON as! NSDictionary)["info_array"] as! NSDictionary)["product_listing"] as! NSArray
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.listTableview.delegate = self
                            
                            self.listTableview.dataSource = self
                            
                            self.listTableview.reloadData()
                            
                            self.gridCollectionview.delegate = self
                            
                            self.gridCollectionview.dataSource = self
                            
                            self.gridCollectionview.reloadData()
                            
                            SVProgressHUD.dismiss()
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            SVProgressHUD.dismiss()
                        }
                    }
                    
                    
                }
                
            case .failure(let encodingError):
                
                print(encodingError)
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
            }
            
        }
        
        
    }
    
    
    
    
    // MARK:- // Json Post Method for Adding in Watchlist
    
    func addToWatchList(completion : @escaping () -> ())
        
    {
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_add_watchlist")!   //change the url
        
        print("add from watchlist URL : ", url)
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "user_id=\(userID!)&add_product_id=\(watchListProductID)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                DispatchQueue.main.async {
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
                return
            }
            
            guard let data = data else {
                
                DispatchQueue.main.async {
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("add to watchlist Response: " , json)
                    
                    if (json["response"] as! Bool) == true
                    {
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                             completion()
                            
                            SVProgressHUD.dismiss()
                            
                        }
                       
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            
                            SVProgressHUD.dismiss()
                        }
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                
                DispatchQueue.main.async {
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    // MARK:- // JSON Post Method to Remove Item from Watchlist
    
    func removeFromWatchList(completion : @escaping () -> ())
        
    {
        dispatchGroup.enter()
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_remove_watchlist")!
        
        print("add from watchlist URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&remove_product_id=\(watchListProductID)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
                return
            }
            
            guard let data = data else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("remove from watchlist Response: " , json)
//                    let alertmsg = "\(json["message"]!)"

                    if (json["response"] as! Bool) == true
                    {
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            completion()
                            
                            SVProgressHUD.dismiss()
                            
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            
                            SVProgressHUD.dismiss()
                        }
                    }
                    
                    
//                    DispatchQueue.main.async {
//
//                        self.dispatchGroup.leave()
//
//                        let alert = UIAlertController(title: "Warning", message: alertmsg, preferredStyle: .alert)
//                        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
//                        alert.addAction(ok)
//                        self.present(alert, animated: true, completion: nil)
//
//                        SVProgressHUD.dismiss()
//                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
            }
        })
        
        task.resume()
        
        
        
    }
    
    
}



// MARK:- // Tableview Delegates

extension ImageSearchResultViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj = tableView.dequeueReusableCell(withIdentifier: "marketplacetblcell") as! MarketPlaceListingTableViewCell
        let path = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_photo"]!)"
        obj.Product_imageview.sd_setImage(with: URL(string: path))
        obj.Product_name.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        obj.Product_type.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type"]!)"
        obj.Product_address.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["address"]!)"
        obj.discounted_price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" +  "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
        obj.Closing_time.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["expire_time_nw"]!)"
        obj.Listing_time.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_time"]!)"
        obj.Product_sellerName.text = "Business name:" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"
        obj.Final_price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
        if "\((((self.recordsArray[indexPath.row] as! NSDictionary)["user_details"]) as! NSDictionary)["user_id"]!)".isEmpty == true
        {
            obj.markView.isHidden = false
            //            obj.Product_SellerView.isHidden = true
            obj.Product_sellerName.isHidden = true
        }
        else
        {
            obj.markView.isHidden = true
            obj.Product_sellerName.isHidden = false
            //            obj.Product_SellerView.isHidden = false
        }
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["special_offer_status"]!)".elementsEqual("1")
        {
            obj.discounted_price.isHidden = false
            let txt = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            obj.discounted_price.attributedText = txt.strikeThrough()
            obj.Final_price.text =  "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
        }
        else
        {
            obj.discounted_price.isHidden = true
            obj.Final_price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
        }
        if "\((((self.recordsArray[indexPath.row] as! NSDictionary)["user_details"]) as! NSDictionary)["user_verified_status"]!)".elementsEqual("V")
        {
            obj.Verified_image.isHidden = false
        }
        else
        {
            obj.Verified_image.isHidden = true
        }
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("1")
        {
            obj.Watchlist_btn.setImage(UIImage(named: "heart_red"), for: .normal)
        }
        else
        {
            obj.Watchlist_btn.setImage(UIImage(named: "heart"), for: .normal)
        }
        obj.Watchlist_btn.tag = indexPath.row
        
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("1")
        {
            obj.Watchlist_btn.setImage(UIImage(named: "heart_red"), for: .normal)
            obj.Watchlist_btn.addTarget(self, action: #selector(Removefromwatchlist(sender:)), for: .touchUpInside)
            
        }
        else
        {
            obj.Watchlist_btn.setImage(UIImage(named: "heart"), for: .normal)
            obj.Watchlist_btn.addTarget(self, action: #selector(addToWatchlist(sender:)), for: .touchUpInside)
        }
        
//        obj.Watchlist_btn.addTarget(self, action: #selector(addToWatchlist(sender:)), for: .touchUpInside)
        obj.selectionStyle = .none;
        
        return obj
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let obj = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "productDetailsVC") as! ProductDetailsViewController
//        obj.productID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
//        self.navigationController?.pushViewController(obj, animated: true)

        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
        navigate.ProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }

    //MARK:- //PickerView Delegate and DataSource Methods
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return self.SortDict.count
//    }
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return "\((self.SortDict[row] as! NSDictionary)["value"]!)"
//    }
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        <#code#>
//    }
//

    
    
    
}




// MARK:- // Collectionview Delegates

extension ImageSearchResultViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.recordsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "marketplacelisting", for: indexPath) as! MarketPlaceListingCollectionViewCell
        
        let path = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_photo"]!)"
        
        cell.Product_image.sd_setImage(with: URL(string: path))
        
        cell.Product_name.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        
        cell.Listing_time.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_time"]!)"
        
        cell.Expire_time.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["expire_time_nw"]!)"
        
        cell.ProductSellerName.text = "Business name:" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"
        
        cell.Product_address.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["address"]!)"
        
        cell.Product_Price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        
        cell.Product_discounted_price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
        
        cell.Product_type.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type"]!)"
        
        
        
        //        cell.CellView.layer.borderWidth = 0.5
        //        cell.CellView.layer.borderColor = UIColor.lightGray.cgColor
        
        
        if "\((((self.recordsArray[indexPath.row] as! NSDictionary)["user_details"]) as! NSDictionary)["user_id"]!)".isEmpty == true
        {
            cell.Product_StatusView.isHidden = false
            
            cell.Product_SellerView.isHidden = true
        }
        else
        {
            cell.Product_StatusView.isHidden = true
            
            cell.Product_SellerView.isHidden = false
            
        }
        if "\((((self.recordsArray[indexPath.row] as! NSDictionary)["user_details"]) as! NSDictionary)["user_verified_status"]!)".elementsEqual("V")
        {
            cell.Verified_image.isHidden = false
        }
        else
        {
            cell.Verified_image.isHidden = true
        }
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["special_offer_status"]!)".elementsEqual("1")
        {
            cell.Product_discounted_price.isHidden = false
            
            let txt = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            
            cell.Product_discounted_price.attributedText = txt.strikeThrough()
            
            cell.Product_Price.text =  "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
        }
        else
        {
            cell.Product_discounted_price.isHidden = true
            
            cell.Product_Price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
        }
        cell.watchlistBtnOutlet.tag = indexPath.row
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("1")
        {
            cell.watchlistBtnOutlet.setImage(UIImage(named: "heart_red"), for: .normal)
            
            cell.watchlistBtnOutlet.addTarget(self, action: #selector(Removefromwatchlist(sender:)), for: .touchUpInside)
            
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("0")
        {
            cell.watchlistBtnOutlet.setImage(UIImage(named: "heart"), for: .normal)
            
            cell.watchlistBtnOutlet.addTarget(self, action: #selector(addToWatchlist(sender:)), for: .touchUpInside)
        }
        else
        {
            cell.watchlistBtnOutlet.setImage(UIImage(named: "heart"), for: .normal)
//            cell.watchlistBtnOutlet.addTarget(self, action: #selector(addToWatchlist(sender:)), for: .touchUpInside)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 4
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding : CGFloat! = 4
        
        let collectionViewSize = self.gridCollectionview.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: 254 )
    }
    
}
