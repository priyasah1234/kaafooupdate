//
//  ProductQueryListingViewController.swift
//  Kaafoo
//
//  Created by esolz on 13/12/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ProductQueryListingViewController: GlobalViewController {
    
    @IBOutlet weak var ProductQueryListingtableView: UITableView!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    var sidemenuView : SideMenuView = {
        
        let sideView = SideMenuView()
        
        sideView.translatesAutoresizingMaskIntoConstraints = false
        
        return sideView
        
    }()
    

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
//        self.getListing()
        
        self.SetLayer()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- // View will appear method  ---- To Update data
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        self.recordsArray.removeAllObjects()
        
        self.getListing()
    }
    
    //MARK:- //Set Layers
    
    func SetLayer()
    {
        self.ProductQueryListingtableView.bounces = false
        
        /*
        self.view.addSubview(self.sidemenuView)

        self.sidemenuView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        
        self.sidemenuView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        self.sidemenuView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: -(320*(self.FullWidth))).isActive = true
        
        self.sidemenuView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -(320*(self.FullWidth))).isActive = true

        self.sidemenuView.isHidden = true
 
        */
    }
    
    //MARK:- // API Call Method
    
    func getListing()
    {
        let parameters = "lang_id=\(langID!)&user_id=\(userID!)"
        
        self.CallAPI(urlString: "product-ques-ans-list", param: parameters, completion: {
            
          self.globalDispatchgroup.leave()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                let tempdict = self.globalJson["info_array"] as! NSArray
                
                for i in 0..<tempdict.count
                {
                    let dict = tempdict[i] as! NSDictionary
                    
                    self.recordsArray.add(dict)
                }
                
                self.globalDispatchgroup.notify(queue: .main, execute: {
                    DispatchQueue.main.async {
                        
                       self.ProductQueryListingtableView.delegate = self
                        
                        self.ProductQueryListingtableView.dataSource = self
                        
                        self.ProductQueryListingtableView.reloadData()
                        
                         SVProgressHUD.dismiss()
                    }
                })
            })
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class ProductQueryTVC : UITableViewCell
{
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var ProductName: UILabel!
    
    @IBOutlet weak var NotificationImage: UIImageView!
    
    @IBOutlet weak var QuestionsNumber: UILabel!
    
    @IBOutlet weak var Separator: UIView!
    
    @IBOutlet weak var ViewContent: UIView!
    
}
extension ProductQueryListingViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "query") as! ProductQueryTVC
        
        cell.ViewContent.layer.borderWidth = 1.0
        
        cell.ViewContent.layer.borderColor = UIColor.black.cgColor
        
        cell.ViewContent.layer.cornerRadius = 4.0
        
        cell.ProductName.text = "Product Name : " + "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"] ?? "")"
        cell.NotificationImage.image = UIImage(named: "notice")
        
        cell.QuestionsNumber.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_count"] ?? "")"
        
        //Font
        
        cell.ProductName.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        cell.QuestionsNumber.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
        
        //
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "qnavc") as! ProductQueryQNAVC
        
        navigate.FaqArray = ((self.recordsArray[indexPath.row] as! NSDictionary)["faq"] as! NSMutableArray)
        
        navigate.reportID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"] ?? "")"
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }
}
