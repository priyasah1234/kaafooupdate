//
//  SignUpOneViewController.swift
//  Kaafoo
//
//  Created by admin on 14/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift

class SignUpOneViewController: GlobalViewController,UITextFieldDelegate {
    
    // Global Font Applied
    
    
    @IBOutlet weak var signLbl: UILabel!
    
    @IBOutlet weak var privateLbl: UILabel!
    @IBOutlet weak var privateBottomView: UIView!
    @IBOutlet weak var BusinessLbl: UILabel!
    @IBOutlet weak var businessBottomView: UIView!
    
    
    @IBOutlet weak var firstNameTXT: ACFloatingTextfield!
    @IBOutlet weak var lastNameTXT: ACFloatingTextfield!
    @IBOutlet weak var displayNameTXT: ACFloatingTextfield!
    
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    
    
    var signUpInfoArray : NSMutableArray = NSMutableArray()
    
    var signUpType : String! = "Private"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        setAttributedTitle(toLabel: signLbl, boldText: "SIGN", boldTextFont: UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (firstNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: " UP")
        
        if signUpType.elementsEqual("Business")
        {
            self.firstNameTXT.placeholder = "Business name"
            self.lastNameTXT.placeholder = "Website url"
            self.displayNameTXT.placeholder = "Display Name"
            
            self.signUpType = "Business"
            
            self.clickOnPrivateOutlet.isUserInteractionEnabled = true
            self.clickOnBusinessOutlet.isUserInteractionEnabled = false
            
            self.businessBottomView.backgroundColor = UIColor(red: 255/255, green: 202/255, blue: 0/255, alpha: 1)
            self.privateBottomView.backgroundColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
        }
        else
        {
            
        }
        
        set_font()
        
        firstNameTXT.delegate = self
        lastNameTXT.delegate = self
        displayNameTXT.delegate = self

    }
    
    //MARK:- //Dismiss Keyboard
       
       @objc func dismissKeyboard() {
           view.endEditing(true)
       }
       
    
    // MARK:- // Buttons

    /*
    // MARK:- // Click on First Name
    
    
    @IBOutlet weak var clickOnFirstNameOutlet: UIButton!
    @IBAction func clickOnFirstName(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.firstNameLbl.frame.origin.y = (self.firstNameLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.firstNameLbl.font = UIFont(name: (self.firstNameLbl.font?.fontName)!,size: 11)
            self.firstNameLbl.font = UIFont(name: self.firstNameLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnFirstNameOutlet.isUserInteractionEnabled = false
            self.firstNameTXT.isUserInteractionEnabled = true
            self.firstNameTXT.becomeFirstResponder()
            
            if (self.lastNameTXT.text?.isEmpty)!
            {
                self.lastNameGetBackInPosition()
                if (self.displayNameTXT.text?.isEmpty)!
                {
                    self.displayNameGetBackInPosition()
                }
            }
            else if (self.displayNameTXT.text?.isEmpty)!
            {
                self.displayNameGetBackInPosition()
            }
            
        }
        
        
    }
 */
    
    /*
    // MARK:- // Click on Last Name
    
    @IBOutlet weak var clickOnLastNameOutlet: UIButton!
    @IBAction func clickOnLastName(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.lastNameLbl.frame.origin.y = (self.lastNameLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.lastNameLbl.font = UIFont(name: (self.lastNameLbl.font?.fontName)!,size: 11)
            self.lastNameLbl.font = UIFont(name: self.lastNameLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnLastNameOutlet.isUserInteractionEnabled = false
            self.lastNameTXT.isUserInteractionEnabled = true
            self.lastNameTXT.becomeFirstResponder()
            if (self.firstNameTXT.text?.isEmpty)!
            {
                self.firstNameGetBackInPosition()
                if (self.displayNameTXT.text?.isEmpty)!
                {
                    self.displayNameGetBackInPosition()
                }
            }
            else if (self.displayNameTXT.text?.isEmpty)!
            {
                self.displayNameGetBackInPosition()
            }
            
        }
        
    }
 */
    
    /*
    // MARK:- // Click on Display Name
    
    
    @IBOutlet weak var clickOnDisplayNameOutlet: UIButton!
    @IBAction func clickOnDisplayName(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.displayNameLbl.frame.origin.y = (self.displayNameLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.displayNameLbl.font = UIFont(name: (self.displayNameLbl.font?.fontName)!,size: 11)
            self.displayNameLbl.font = UIFont(name: self.displayNameLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnDisplayNameOutlet.isUserInteractionEnabled = false
            self.displayNameTXT.isUserInteractionEnabled = true
            self.displayNameTXT.becomeFirstResponder()
            if (self.firstNameTXT.text?.isEmpty)!
            {
                self.firstNameGetBackInPosition()
                if (self.lastNameTXT.text?.isEmpty)!
                {
                    self.lastNameGetBackInPosition()
                }
            }
            else if (self.lastNameTXT.text?.isEmpty)!
            {
                self.lastNameGetBackInPosition()
            }
            
        }
        
    }
 */
    
    
    // MARK:- // Click on Private Section
    
    
    @IBOutlet weak var clickOnPrivateOutlet: UIButton!
    @IBAction func clickOnPrivate(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.firstNameTXT.placeholder = "First Name"
            self.lastNameTXT.placeholder = "Last Name"
            self.displayNameTXT.placeholder = "Display Name"
            
            self.signUpType = "Private"
            
            self.clickOnPrivateOutlet.isUserInteractionEnabled = false
            self.clickOnBusinessOutlet.isUserInteractionEnabled = true
            
            self.privateBottomView.backgroundColor = UIColor(red: 255/255, green: 202/255, blue: 0/255, alpha: 1)
            self.businessBottomView.backgroundColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
            
        }
        
    }
    
    
    // MARK:- // Click on Business Section
    
    
    @IBOutlet weak var clickOnBusinessOutlet: UIButton!
    @IBAction func clickOnBusiness(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.firstNameTXT.placeholder = "Business name"
            self.lastNameTXT.placeholder = "Website url"
            self.displayNameTXT.placeholder = "Display Name"
            
            self.signUpType = "Business"
            
            self.clickOnPrivateOutlet.isUserInteractionEnabled = true
            self.clickOnBusinessOutlet.isUserInteractionEnabled = false
            
            self.businessBottomView.backgroundColor = UIColor(red: 255/255, green: 202/255, blue: 0/255, alpha: 1)
            self.privateBottomView.backgroundColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
            
        }
        
    }
    
    
    // MARK:- // Cross Button
    
    
    @IBOutlet weak var crossImageView: UIImageView!
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBAction func tapOnCross(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
//        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController
//
//        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    // MARK:- // Next Button
    

    @IBAction func nextButton(_ sender: UIButton) {
        
        if signUpType.elementsEqual("Business")
        {
            
            businessValidateWithAlerts()
           
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpBusinessTwoVC") as! SignUpBusinessTwoViewController
            
            addDataInArray()
            
            print("Sign UP Info Array : ", signUpInfoArray)
            
            navigate.signUpInfoArray = signUpInfoArray.mutableCopy() as! NSMutableArray
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
            
        }
        else
        {
            
            validateWithAlerts()
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpTwoVC") as! SignUpTwoViewController
            
            addDataInArray()
            
            print("Sign UP Info Array : ", signUpInfoArray)
            
            navigate.signUpInfoArray = signUpInfoArray.mutableCopy() as! NSMutableArray
            
            
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        
    }
    
    
    // MARK:- // Defined Functions
    
    /*
    // MARK:- // First name get back in position
    
    
    
    func firstNameGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.firstNameLbl.frame.origin.y = self.firstNameTXT.frame.origin.y
            self.firstNameLbl.font = UIFont(name: (self.firstNameLbl.font?.fontName)!,size: 16)
            self.firstNameLbl.font = UIFont(name: self.firstNameLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnFirstNameOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    // MARK:- // Last name get back in Position
    
    func lastNameGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.lastNameLbl.frame.origin.y = self.lastNameTXT.frame.origin.y
            self.lastNameLbl.font = UIFont(name: (self.lastNameLbl.font?.fontName)!,size: 16)
            self.lastNameLbl.font = UIFont(name: self.lastNameLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnLastNameOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    // MARK:- // Display name get back in position
    
    func displayNameGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.displayNameLbl.frame.origin.y = self.displayNameTXT.frame.origin.y
            self.displayNameLbl.font = UIFont(name: (self.displayNameLbl.font?.fontName)!,size: 16)
            self.displayNameLbl.font = UIFont(name: self.displayNameLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnDisplayNameOutlet.isUserInteractionEnabled = true
            
        }
    }
    */
    
    
    // MARK:- // Private Section Validate with alerts
    
    func validateWithAlerts()
    {
             //        (firstNameTXT.text?.isEmpty)!
        
        if  (firstNameTXT.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            let alert = UIAlertController(title: "First Name Empty", message: "Please Enter First Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
            
        else if (lastNameTXT.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            let alert = UIAlertController(title: "Last Name Empty", message: "Please Enter Last Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
            
        else if (displayNameTXT.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            let alert = UIAlertController(title: "Display Name Empty", message: "Please Enter Display Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        
    }
    
    
    
    // MARK:- // Business section Validate with Alerts
    
    func businessValidateWithAlerts()
    {
        
        if (firstNameTXT.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            let alert = UIAlertController(title: "Business Name Empty", message: "Please Enter Business Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
            
//        else if (lastNameTXT.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
//        {
//            let alert = UIAlertController(title: "Website Url Empty", message: "Please Enter URL", preferredStyle: .alert)
//
//            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
//
//            alert.addAction(ok)
//
//            self.present(alert, animated: true, completion: nil )
//        }
            
            //MARK:-  //Valid URL Checking 
            
        else if lastNameTXT.text!.isValidURL == false
        {
            let alert = UIAlertController(title: "Invalid URL", message: "Please Enter Proper URL", preferredStyle: .alert)
                       
                       let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                       
                       alert.addAction(ok)
                       
                       self.present(alert, animated: true, completion: nil )
        }
            
        else if (displayNameTXT.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            let alert = UIAlertController(title: "Display Name Empty", message: "Please Enter Display Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        
    }
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
//        signLbl.font = UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
//        upLbl.font = UIFont(name: upLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        privateLbl.font = UIFont(name: privateLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        BusinessLbl.font = UIFont(name: BusinessLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        firstNameTXT.font = UIFont(name: (firstNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        lastNameTXT.font = UIFont(name: (lastNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        displayNameTXT.font = UIFont(name: (displayNameTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        nextButtonOutlet.titleLabel?.font = UIFont(name: (nextButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    
    // MARK:- // Add Data In Array
    
    func addDataInArray()
    {
        
        signUpInfoArray.add(firstNameTXT.text!)
        signUpInfoArray.add(lastNameTXT.text!)
        signUpInfoArray.add(displayNameTXT.text!)
        
    }
    
    
    
    /*
    // MARK:- // Textfield Delegates
    
    // For pressing return on the keyboard to dismiss keyboard
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        firstNameTXT.resignFirstResponder()
        lastNameTXT.resignFirstResponder()
        displayNameTXT.resignFirstResponder()
        mainScroll.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        
        if (self.firstNameTXT.text?.isEmpty)!
        {
            self.firstNameGetBackInPosition()
        }
        
        if (self.lastNameTXT.text?.isEmpty)!
        {
            self.lastNameGetBackInPosition()
        }
        
        if (self.displayNameTXT.text?.isEmpty)!
        {
            self.displayNameGetBackInPosition()
        }
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == lastNameTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (10/568)*self.FullHeight), animated: true)
            
        }
        else if textField == displayNameTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (90/568)*self.FullHeight), animated: true)
            
        }
        
    }
    */
    
}
