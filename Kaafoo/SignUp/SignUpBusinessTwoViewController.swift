//
//  SignUpBusinessTwoViewController.swift
//  Kaafoo
//
//  Created by admin on 23/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class SignUpBusinessTwoViewController: GlobalViewController,UITextFieldDelegate {
    
    // Global Font Applied
    
    
    
    @IBAction func tapPreviousButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var PreviousButtonOutlet: UIButton!
    @IBOutlet weak var signLbl: UILabel!
    @IBOutlet weak var privateLbl: UILabel!
    @IBOutlet weak var businessLbl: UILabel!
    @IBOutlet weak var privateBottomView: UIView!
    @IBOutlet weak var businessBottomView: UIView!
    @IBOutlet weak var landLineTXT: UITextField!
    @IBOutlet weak var mobileNoTXT: UITextField!
    @IBOutlet weak var addressTXT: UITextField!
    
    @IBOutlet weak var nextButtonOutlet: UIButton!
    
    
    var signUpInfoArray : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.landLineTXT.keyboardType = .numberPad
        self.mobileNoTXT.keyboardType = .numberPad
        
        setAttributedTitle(toLabel: signLbl, boldText: "SIGN", boldTextFont: UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (landLineTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: " UP")

        landLineTXT.delegate = self
        mobileNoTXT.delegate = self
        addressTXT.delegate = self
        
        set_font()
      
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    // MARK:- // Buttons
    
    // MARK:- // Click on Private Section
    
    
    @IBOutlet weak var clickOnPrivateOutlet: UIButton!
    @IBAction func clickOnPrivate(_ sender: Any) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpOneVC") as! SignUpOneViewController
        
        navigate.signUpType = "Private"
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Click on business Section
    
    
    @IBOutlet weak var clickOnBusinessOutlet: UIButton!
    @IBAction func clickOnBusiness(_ sender: Any) {
    }
    
    
    
    /*
    @IBOutlet weak var clickOnLandLineOutlet: UIButton!
    @IBAction func clickOnLandLIne(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.landLineLbl.frame.origin.y = (self.landLineLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.landLineLbl.font = UIFont(name: (self.landLineLbl.font?.fontName)!,size: 11)
            self.landLineLbl.font = UIFont(name: self.landLineLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnLandLineOutlet.isUserInteractionEnabled = false
            self.landLineTXT.isUserInteractionEnabled = true
            self.landLineTXT.becomeFirstResponder()
            if (self.mobileNoTXT.text?.isEmpty)!
            {
                self.mobileNumberGetBackInPosition()
                if (self.addressTXT.text?.isEmpty)!
                {
                    self.addressGetBackInPosition()
                }
            }
            else if (self.addressTXT.text?.isEmpty)!
            {
                self.addressGetBackInPosition()
            }
            
        }

        
    }
    
    
    
    
    // MARK:- // Click on Mobile Number
    
    @IBOutlet weak var clickOnMobileNumberOutlet: UIButton!
    @IBAction func clickOnMobileNumber(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.mobileNoLbl.frame.origin.y = (self.mobileNoLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.mobileNoLbl.font = UIFont(name: (self.mobileNoLbl.font?.fontName)!,size: 11)
            self.mobileNoLbl.font = UIFont(name: self.mobileNoLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnMobileNumberOutlet.isUserInteractionEnabled = false
            self.mobileNoTXT.isUserInteractionEnabled = true
            self.mobileNoTXT.becomeFirstResponder()
            if (self.landLineTXT.text?.isEmpty)!
            {
                self.landLineGetBackInPosition()
                if (self.addressTXT.text?.isEmpty)!
                {
                    self.addressGetBackInPosition()
                }
            }
            else if (self.addressTXT.text?.isEmpty)!
            {
                self.addressGetBackInPosition()
            }
            
        }
        
    }
    
    
    
    // MARK:- // Click on Address
    
    @IBOutlet weak var clickOnAddressOutlet: UIButton!
    @IBAction func clickOnAddress(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3){
            
            self.addressLbl.frame.origin.y = (self.addressLbl.frame.origin.y - ((30/568)*self.FullHeight))
            self.addressLbl.font = UIFont(name: (self.addressLbl.font?.fontName)!,size: 11)
            self.addressLbl.font = UIFont(name: self.addressLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            self.clickOnAddressOutlet.isUserInteractionEnabled = false
            self.addressTXT.isUserInteractionEnabled = true
            self.addressTXT.becomeFirstResponder()
            if (self.landLineTXT.text?.isEmpty)!
            {
                self.landLineGetBackInPosition()
                if (self.mobileNoTXT.text?.isEmpty)!
                {
                    self.mobileNumberGetBackInPosition()
                }
            }
            else if (self.mobileNoTXT.text?.isEmpty)!
            {
                self.mobileNumberGetBackInPosition()
            }
            
        }
        
    }
    */
    
    
    // MARK:- // Next Button

    
    @IBOutlet weak var nextOutlet: UIButton!
    @IBAction func nextTapped(_ sender: Any) {
        
        validateWithAlerts()
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "signUpBusinessThreeVC") as! SIgnUpBusinessThreeViewController
        
        addDataInArray()
        
        print("Sign UP Info Array : ", signUpInfoArray)
        
        navigate.signUpInfoArray = signUpInfoArray.mutableCopy() as! NSMutableArray
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
        
    }
    
    
    
    // MARK:- // Cross Button
    
    

    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBAction func tapOnCross(_ sender: Any) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    // MARK:- // Defined Functions
    
    
    /*
    // MARK:- // Land line get back in Position
    
    
    func landLineGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.landLineLbl.frame.origin.y = self.landLineTXT.frame.origin.y
            self.landLineLbl.font = UIFont(name: (self.landLineLbl.font?.fontName)!,size: 16)
            self.landLineLbl.font = UIFont(name: self.landLineLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnLandLineOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    // MARK:- // Mobile Number Get back in Position
    
    
    func mobileNumberGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.mobileNoLbl.frame.origin.y = self.mobileNoTXT.frame.origin.y
            self.mobileNoLbl.font = UIFont(name: (self.mobileNoLbl.font?.fontName)!,size: 16)
            self.mobileNoLbl.font = UIFont(name: self.mobileNoLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnMobileNumberOutlet.isUserInteractionEnabled = true
            
        }
    }
    
    
    // MARK:- // Address Get back in Position
    
    
    func addressGetBackInPosition()
    {
        UIView.animate(withDuration: 0.3){
            
            self.addressLbl.frame.origin.y = self.addressTXT.frame.origin.y
            self.addressLbl.font = UIFont(name: (self.addressLbl.font?.fontName)!,size: 16)
            self.addressLbl.font = UIFont(name: self.addressLbl.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
            self.clickOnAddressOutlet.isUserInteractionEnabled = true
            
        }
    }
    */
    
    
    
    
    // MARK:- // Add Data in Array
    
    func addDataInArray()
    {
        
        signUpInfoArray.add(landLineTXT.text!)
        signUpInfoArray.add(mobileNoTXT.text!)
        signUpInfoArray.add(addressTXT.text!)
        
    }
    
    
    // MARK:- // Validate with Alerts
    
    func validateWithAlerts()
    {
        
        if (mobileNoTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Mobile Number Empty", message: "Please Enter Mobile Number", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
            
        else if (addressTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Address Empty", message: "Please Enter Address", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (landLineTXT.text?.isEmpty == false)
               {
                   if self.landLineTXT.text?.isPhoneNumber == false
                   {
                       self.ShowAlertMessage(title: "warning", message: "Phone Number is not in correct format")
                   }
                   else
                   {
                       //do nothing
                   }
               }
            
        
        
    }
    
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
//        signLbl.font = UIFont(name: signLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
//        upLbl.font = UIFont(name: upLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))
        privateLbl.font = UIFont(name: privateLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        businessLbl.font = UIFont(name: businessLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        landLineTXT.font = UIFont(name: (landLineTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        mobileNoTXT.font = UIFont(name: (mobileNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        addressTXT.font = UIFont(name: (addressTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
        nextButtonOutlet.titleLabel?.font = UIFont(name: (nextButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
    }
    
    
    
    
    /*
    // MARK:- // Textfield Delegates
    
    // For pressing return on the keyboard to dismiss keyboard
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        landLineTXT.resignFirstResponder()
        mobileNoTXT.resignFirstResponder()
        addressTXT.resignFirstResponder()
        mainScroll.setContentOffset(CGPoint (x: 0, y: 0), animated: true)
        
        if (self.landLineTXT.text?.isEmpty)!
        {
            self.landLineGetBackInPosition()
        }
        
        if (self.mobileNoTXT.text?.isEmpty)!
        {
            self.mobileNumberGetBackInPosition()
        }
        
        if (self.addressTXT.text?.isEmpty)!
        {
            self.addressGetBackInPosition()
        }
        
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == mobileNoTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (10/568)*self.FullHeight), animated: true)
            
        }
        else if textField == addressTXT
        {
            mainScroll.setContentOffset(CGPoint (x: 0, y: (90/568)*self.FullHeight), animated: true)
            
        }
        
    }
    */
    
    
}
