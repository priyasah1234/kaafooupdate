//
//  CheckOutViewController.swift
//  Kaafoo
//
//  Created by Kaustabh on 10/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class CheckOutViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate{
    let array = ["Hello","Hi","Hello","Hi","Hello","Hi"]
    let userID = UserDefaults.standard.string(forKey: "userID")
    //let langID = UserDefaults.standard.string(forKey: "langID")
    @IBOutlet weak var CountryViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var CheckOutTableHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var SuburbViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var CityViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var StateViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var SelectCountryTableviewheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var SelectSuburbTableviewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var SelectStateTableViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var SelectCityTableViewheightConstraint: NSLayoutConstraint!










    @IBOutlet var Pickup_Outlet: UIButton!
    @IBOutlet var Delivery_Outlet: UIButton!
    
    
    
    @IBOutlet var pickup_image: UIImageView!
    @IBOutlet var Delivery_image: UIImageView!
    //MARK:- Stripe View TextField Design
    
    @IBOutlet var name_txt: UITextField!
    @IBOutlet var cardno_txt: UITextField!
    @IBOutlet var expire_txt: UITextField!
    @IBOutlet var cvv_txt: UITextField!
    
    @IBOutlet var Lbl1: UILabel!
    @IBOutlet var Lbl2: UILabel!
    @IBOutlet var Lbl3: UILabel!
    @IBOutlet var Lbl4: UILabel!
    @IBOutlet var Lbl5: UILabel!
    @IBOutlet var Lbl6: UILabel!
    @IBOutlet var Lbl7: UILabel!
    @IBOutlet var Lbl8: UILabel!
    @IBOutlet var Lbl9: UILabel!
    @IBOutlet var Lbl10: UILabel!
    @IBOutlet var Lbl11: UILabel!
    @IBOutlet var Lbl12: UILabel!
    @IBOutlet var Lbl13: UILabel!
    @IBOutlet var Lbl14: UILabel!
    @IBOutlet var Lbl15: UILabel!
    @IBOutlet var Lbl16: UILabel!
    @IBOutlet var Lbl17: UILabel!
    @IBOutlet var Lbl18: UILabel!
    @IBOutlet var Lbl19: UILabel!
    @IBOutlet var Lbl20: UILabel!
    
    
    
    
    
    @IBOutlet var BankTransfer_btn_outlet: UIButton!
    @IBOutlet var COD_btn_outlet: UIButton!
    @IBOutlet var stripe_btn_outlet: UIButton!
    @IBOutlet var paypal_btn_outlet: UIButton!
    @IBOutlet var Wallet_btn_outlet: UIButton!
    @IBOutlet var Stripe_image: UIImageView!
    @IBOutlet var paypal_image: UIImageView!
    @IBOutlet var Wallet_image: UIImageView!
    
    
    
    @IBOutlet var Stripe_View: UIView!
    
    let userEmail = UserDefaults.standard.string(forKey: "EmailID")
    
    let userPhone = UserDefaults.standard.string(forKey: "PhoneNo")
    let userFirstName = UserDefaults.standard.string(forKey: "FirstName")
    let userLastName = UserDefaults.standard.string(forKey: "LastName")
    let MyCountry = UserDefaults.standard.string(forKey: "SavedCountry")
    
    @IBOutlet var CheckImageView: UIImageView!
    @IBAction func Image_Check_Btn(_ sender: UIButton) {
        
        
        if sender.isSelected
        {
            //CheckImageView.image = UIImage(named: "Square")
            Check_image_outlet.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
            self.First_name_lbl.text = ""
            self.Last_name_lbl.text = ""
            self.Email_address_lbl.text = ""
            self.Phone_no_lbl.text = ""
            
        }
        else
        {
            // self.CheckImageView.image = UIImage(named: "Right Tik")
            // Check_image_outlet.imageView?.image = UIImage(named: "Right Tik")
            Check_image_outlet.setImage(UIImage(named: "checkBoxFilled"), for: .normal)
            self.First_name_lbl.text = userFirstName
            self.Last_name_lbl.text = userLastName
            self.Email_address_lbl.text = userEmail
            self.Phone_no_lbl.text = userPhone
            
        }
        
        sender.isSelected = !sender.isSelected
        
        
    }
    
    
    
    @IBAction func Select_state_btn(_ sender: Any) {
        
        if self.RecordsArray2 == nil
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Country First", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else
        {
            if self.RecordsArray1.count == 0
            {
                let alert = UIAlertController(title: "Warning", message: "Please Enter Your Country First", preferredStyle: .alert)
                let ok = UIAlertAction.init(title:"Ok", style: .cancel , handler: nil)
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                print("Self RecordsArray1",self.RecordsArray1!)
                City_View.isHidden = true
                State_View.autoresizesSubviews = false
                State_View.frame.size.height = (200/568)*self.FullHeight
                Select_State_View.autoresizesSubviews = false
                Select_State_View.isHidden = false
                Select_State_View.frame.size.height = (100/568)*self.FullHeight
                Select_State_TableView.frame.size.height = (100/568)*self.FullHeight
            }
            //        if Select_State_View.isHidden{
            //            animateState(toggle: true)
            //
            //        }
            //        else {
            //            animateState(toggle: false)
            //
            //        }
        }
        
    }
    @IBAction func Select_City_Btn(_ sender: Any) {
        if self.RecordsArray2.count == 0
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your State First", preferredStyle: .alert)
            let ok = UIAlertAction.init(title:"Ok", style: .cancel , handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
//            SuburbView.isHidden = true
//            City_View.autoresizesSubviews = false
//            City_View.frame.size.height = (200/568)*self.FullHeight
//            Select_City_View.autoresizesSubviews = false
//            Select_City_View.isHidden = false
//            Select_City_View.frame.size.height = (150/568)*self.FullHeight
//            Select_City_tableview.frame.size.height = (150/568)*self.FullHeight
        }
        
    }
    
    
    @IBOutlet var SuburbView: UIView!
    @IBOutlet var SelectSuburbView: UIView!
    @IBOutlet var SelectSuburbTableView: UITableView!
    
    
    
    @IBOutlet var country_btn_outlet: UIButton!
    @IBOutlet var state_btn_outlet: UIButton!
    @IBOutlet var city_btn_outlet: UIButton!
    @IBOutlet var suburb_btn_outlet: UIButton!
    
    
    @IBOutlet var Details1View: UIView!
    
    
    @IBOutlet var Check_out: UIButton!
    
    
    @IBOutlet var Deals_Payment_View: UIView!
    var RecordsArray : NSMutableArray! = NSMutableArray()
    var RecordsArray1 : NSMutableArray! = NSMutableArray()
    var RecordsArray2 : NSMutableArray! = NSMutableArray()
    var RecordsArray3 : NSMutableArray! = NSMutableArray()
    
    @IBOutlet var SelectCountry_View: UIView!
    
    
    
    @IBOutlet var Image2: UIImageView!
    @IBOutlet var Image1: UIImageView!
    @IBOutlet var Stripe_Card_View: UIView!
    @IBOutlet var Choose_Location_View: UIView!
    @IBOutlet var Wallet_View: UIView!
    @IBOutlet var Shipping_Type_View: UIView!
    @IBOutlet var Payment_View: UIView!
    @IBOutlet var First_name_lbl: UITextField!
    @IBOutlet var Last_name_lbl: UITextField!
    @IBOutlet var Email_address_lbl: UITextField!
    @IBOutlet var Phone_no_lbl: UITextField!
    @IBOutlet var House_no_lbl: UITextField!
    @IBOutlet var Street_name_lbl: UITextField!
    @IBOutlet var Postal_code_lbl: UITextField!
    @IBOutlet var Other_info_lbl: UITextField!
    @IBOutlet var Nearby_town_lbl: UITextField!
    @IBOutlet var Check_image_outlet: UIButton!
    
    
    @IBOutlet var SelectCountry_tableView: UITableView!
    
    @IBOutlet var Details2View: UIView!
    
    @IBOutlet var Select_Suburb_Tableview: UITableView!
    @IBOutlet var Select_Suburb_View: UIView!
    
    
    @IBOutlet var Suburb_View: UIView!
    
    
    @IBOutlet var Select_City_tableview: UITableView!
    @IBOutlet var Select_State_View: UIView!
    @IBOutlet var City_lbl: UILabel!
    @IBOutlet var Country_lbl: UILabel!
    
    @IBOutlet var Select_City_View: UIView!
    @IBOutlet var Select_State_TableView: UITableView!
    
    @IBOutlet var Suburb_lbl: UILabel!
    @IBOutlet var State_lbl: UILabel!
    var CheckSeller : String!
    var info_array1:NSArray!
    var info_array2:NSArray!
    var info_array3:NSArray!
    var info_array4:NSArray!
    var CountryDetails : NSMutableArray! = NSMutableArray()
    var RegionDetails : NSMutableArray! = NSMutableArray()
    var CityDetails : NSMutableArray! = NSMutableArray()
    var SuburbDetails : NSMutableArray! = NSMutableArray()
    
    
    
    @IBAction func Select_Country_Btn(_ sender: UIButton) {
        
//        if SelectCountry_View.isHidden{
//            animateCountry(toggle: true)
//
//        }
//        else {
//            animateCountry(toggle: false)
//
//        }

        if SelectCountry_View.isHidden == true
        {
           self.SelectCountry_View.isHidden = false
           self.SelectCountryTableviewheightConstraint.constant = (44/568)*self.FullHeight * CGFloat(self.RecordsArray.count)
           self.CountryViewHeightConstraint.constant = self.SelectCountryTableviewheightConstraint.constant

        }
        else
        {
//            self.SelectCountry_View.isHidden = true
           self.SelectCountryTableviewheightConstraint.constant = 0
            self.CountryViewHeightConstraint.constant = 0

        }


    }
    
    @IBAction func Select_Suburb_Btn(_ sender: UIButton) {
        if self.RecordsArray3.count == 0
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your City First", preferredStyle: .alert)
            let ok = UIAlertAction.init(title:"Ok", style: .cancel , handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            SuburbView.autoresizesSubviews = false
            SuburbView.frame.size.height = (200/568)*self.FullHeight
            Select_Suburb_View.isHidden = false
            Select_Suburb_View.frame.size.height = (50/568)*self.FullHeight
            Select_Suburb_Tableview.frame.size.height = (50/568)*self.FullHeight
            print("Button Pressed")
        }
        
    }
    
    
    
    
    @IBOutlet var MainScroll: UIScrollView!
    var info_array: NSDictionary!
    let dispatchGroup = DispatchGroup()
    var ProductDetails : NSMutableArray! = NSMutableArray()
    var recordsArray : NSMutableArray! = NSMutableArray()
    var PriceDetails: NSMutableDictionary! = NSMutableDictionary()
    var allCountryListDictionary : NSDictionary!
    var selectedCountry : String!
    @IBOutlet var ProductListing: UITableView!
    @IBOutlet var PriceDetailsView: UIView!
    @IBOutlet var DetailsView: UIView!
    
    @IBOutlet var Country_View: UIView!
    
    @IBOutlet var State_View: UIView!
    
    @IBOutlet var City_View: UIView!
    @IBOutlet var Region_View: UIView!
    @IBOutlet var Total_Price: UILabel!
    @IBOutlet var Delivery_Price: UILabel!
    @IBOutlet var Item_Price: UILabel!
    @IBOutlet var Item_lbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
        
        self.getCountryList()
        
        print("USEREMAIL",userEmail!)

        self.Stripe_View.isHidden = true
        self.Wallet_View.isHidden = true
        
        print("USERPHONENO",userPhone!)
        print("USERFIRSTNAME",userFirstName!)
        print("USERLASTNAME",userLastName!)
        ProductListing.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.SelectCountry_tableView.allowsSelection = true
        self.Select_State_TableView.allowsSelection = true
        self.Select_City_tableview.allowsSelection = true
        self.Select_Suburb_Tableview.allowsSelection = true
        self.COD_btn_outlet.isSelected = true
        Pickup_Outlet.tag = 0
        Delivery_Outlet.tag = 1
        COD_btn_outlet.tag = 2
        BankTransfer_btn_outlet.tag = 3
        Wallet_btn_outlet.tag = 4
        paypal_btn_outlet.tag = 5
        stripe_btn_outlet.tag = 6
        Pickup_Outlet.addTarget(self,action:#selector(buttonClicked),
                                for:.touchUpInside)
        Delivery_Outlet.addTarget(self,action:#selector(buttonClicked),
                                  for:.touchUpInside)
        COD_btn_outlet.addTarget(self,action:#selector(buttonClicked),
                                 for:.touchUpInside)
        BankTransfer_btn_outlet.addTarget(self,action:#selector(buttonClicked),
                                          for:.touchUpInside)
        Wallet_btn_outlet.addTarget(self,action:#selector(buttonClicked),
                                    for:.touchUpInside)
        paypal_btn_outlet.addTarget(self,action:#selector(buttonClicked),
                                    for:.touchUpInside)
        stripe_btn_outlet.addTarget(self,action:#selector(buttonClicked),
                                    for:.touchUpInside)
        MainScroll.delegate = self
        self.SetFont()
        
        //        if City_lbl.text == "Select"
        //        {
        //            suburb_btn_outlet.isUserInteractionEnabled = false
        //        }
        //        else
        //        {
        //            suburb_btn_outlet.isUserInteractionEnabled = true
        //        }
        //        if State_lbl.text == "Select"
        //        {
        //            city_btn_outlet.isUserInteractionEnabled = false
        //        }
        //        else
        //        {
        //            city_btn_outlet.isUserInteractionEnabled = true
        //        }
        //        if Country_lbl.text == "Select"
        //        {
        //            state_btn_outlet.isUserInteractionEnabled = false
        //        }
        //        else
        //        {
        //            state_btn_outlet.isUserInteractionEnabled = true
        //        }
        //Country Button Check
        if RecordsArray1 == nil
        {
            
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Country Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
            country_btn_outlet.isUserInteractionEnabled = false
        }
        else
        {
            country_btn_outlet.isUserInteractionEnabled = true
        }
        
        //Adding Subviews
        
//        MainScroll.addSubview(ProductListing)
//        MainScroll.addSubview(PriceDetailsView)
//        MainScroll.addSubview(DetailsView)

        
        //Stripe View Design Code
        
        name_txt.layer.borderWidth = 1.0
        cardno_txt.layer.borderWidth = 1.0
        expire_txt.layer.borderWidth = 1.0
        cvv_txt.layer.borderWidth = 1.0
        name_txt.layer.borderColor = UIColor.black.cgColor
        cardno_txt.layer.borderColor = UIColor.black.cgColor
        expire_txt.layer.borderColor = UIColor.black.cgColor
        cvv_txt.layer.borderColor = UIColor.black.cgColor
        name_txt.layer.cornerRadius = 5.0
        cardno_txt.layer.cornerRadius = 5.0
        expire_txt.layer.cornerRadius = 5.0
        cvv_txt.layer.cornerRadius = 5.0
// Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
       self.CheckOutTableHeightConstraint.constant = self.ProductListing.visibleCells[0].bounds.height * CGFloat(self.recordsArray.count)


    }
    //MARK: - Function For Button Tap Using Sender.tag
    @objc func buttonClicked(sender:UIButton)
    {
        switch sender.tag
        {
        case 0: print("PickUpDelivery")
        pickup_image.image = UIImage(named: "radioSelected")
        Delivery_image.image = UIImage(named: "radioUnselected")
            break
        case 1: print("Delivery")
        Delivery_image.image = UIImage(named: "radioSelected")
        pickup_image.image = UIImage(named: "radioUnselected")
        //        self.SetFrame3()
        //when Button1 is clicked...
            break
        case 2: print("COD Payment")
        Image1.image = UIImage(named: "radioSelected")
        Image2.image = UIImage(named: "radioUnselected")
        //        self.SetFrame3()
        //when Button2 is clicked...
            break
        case 3: print("Bank Transfer")
        Image1.image = UIImage(named: "radioUnselected")
        Image2.image = UIImage(named: "radioSelected")
        //       self.SetFrame3()
        //when Button3 is clicked...
            break
        case 4: print("Wallet Payment")
        Wallet_image.image = UIImage(named: "radioSelected")
        paypal_image.image = UIImage(named: "radioUnselected")
        Stripe_image.image = UIImage(named: "radioUnselected")
        Wallet_View.isHidden = true
        Stripe_View.isHidden = false
        self.SetFrame2()
            break
        case 5: print("Paypal Payment")
        Wallet_image.image = UIImage(named: "radioUnselected")
        paypal_image.image = UIImage(named: "radioSelected")
        Stripe_image.image = UIImage(named: "radioUnselected")
        Wallet_View.isHidden = true
        Stripe_View.isHidden = true
        self.SetFrame3()
            break
        case 6: print("Stripe Payment")
        Wallet_image.image = UIImage(named: "radioUnselected")
        paypal_image.image = UIImage(named: "radioUnselected")
        Stripe_image.image = UIImage(named: "radioSelected")
        Wallet_View.isHidden = false
        Stripe_View.isHidden = true
        self.SetFrame1()
            break
        default: print("Other...")
        Wallet_View.isHidden = true
        Stripe_View.isHidden = true
        self.SetFrame3()
        }
    }
    //MARK:- TableView Delegate & Datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ProductListing
        {
            return  recordsArray.count
        }
        else if tableView == SelectCountry_tableView
        {
            return RecordsArray.count
        }
        else if tableView == Select_State_TableView
        {
            return RecordsArray1.count
        }
        else if tableView == Select_City_tableview
        {
            return RecordsArray2.count
        }
        else if tableView == Select_Suburb_Tableview
        {
            return RecordsArray3.count
        }
        else
        {
            return 1
        }
        
    }
    
    //MARK:- Tableview Cell For Item method
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == ProductListing
        {
            let obj = tableView.dequeueReusableCell(withIdentifier: "checkoutproductlisting") as! CheckOutProductListingTableViewCell
            
            obj.Product_name.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
            obj.Product_name.font = UIFont(name: obj.Product_name.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
            obj.start_price.font = UIFont(name: obj.start_price.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            obj.reserve_price.font = UIFont(name: obj.reserve_price.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            if "\((recordsArray[indexPath.row] as! NSDictionary)["special_offer"]!)" == "0"
            {
                obj.reserve_price.isHidden = true
                obj.start_price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
                obj.start_price.textColor = UIColor.green
            }
            else
            {
                obj.reserve_price.isHidden = false
                
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\((recordsArray[indexPath.item] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.item] as! NSDictionary)["start_price"]!)")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                obj.start_price.attributedText = attributeString
                
                obj.start_price.textColor = UIColor.green
                obj.reserve_price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
                obj.reserve_price.textColor = UIColor.red
            }
            let path = "\((recordsArray[indexPath.row] as! NSDictionary)["product_image"]!)"
            obj.Product_image.sd_setImage(with: URL(string: path))
            obj.selectionStyle = UITableViewCell.SelectionStyle.none
            return obj
        }
            //Select Country TableView
        else if tableView == SelectCountry_tableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCountry", for: indexPath)
            
            cell.textLabel?.text = "\((RecordsArray[indexPath.row] as! NSDictionary)["country_name"]!)"
            cell.selectionStyle = .none
            return cell
        }
            //Select State Tableview
        else if tableView == Select_State_TableView
        {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "SelectState", for: indexPath)
            cell1.textLabel?.text = "\((RecordsArray1[indexPath.row] as! NSDictionary)["region_name"]!)"
            cell1.selectionStyle = .none
            return cell1
        }
        else if tableView == Select_City_tableview
        {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "SelectCity", for: indexPath)
            cell2.textLabel?.text = "\((RecordsArray2[indexPath.row] as! NSDictionary)["city_name"]!)"
            cell2.selectionStyle = .none
            return cell2
        }
        else if tableView == Select_Suburb_Tableview
        {
            let cell3 = tableView.dequeueReusableCell(withIdentifier: "SelectSuburb", for: indexPath)
            cell3.textLabel?.text = "\((RecordsArray3[indexPath.row] as! NSDictionary)["suburbs_name"]!)"
            cell3.selectionStyle = .none
            return cell3
        }
        else
        {
            //By Default
            let  cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
            return cell
        }
        let  cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "Cell")
        return cell
    }
    
    //MARK:- TableView Height For Row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == ProductListing
        {
            return UITableView.automaticDimension
        }
        else if tableView == SelectCountry_tableView
        {
            return (50/568)*self.FullHeight
        }
        else if tableView == Select_State_TableView
        {
            return (50/568)*self.FullHeight
        }
        else if tableView == Select_City_tableview
        {
            return (50/568)*self.FullHeight
        }
        else if tableView == Select_Suburb_Tableview
        {
            return (50/568)*self.FullHeight
        }
        else
        {
            return UITableView.automaticDimension
        }
        
    }
    //MARK:- TableView Did Select Row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == SelectCountry_tableView
        {
            Country_lbl.text = "\((RecordsArray[indexPath.row] as! NSDictionary)["country_name"]!)"
            //animateCountry(toggle: false)
            self.SelectCountry_tableView.isHidden = true
            let SavedCountry = "\((RecordsArray[indexPath.row] as! NSDictionary)["id"]!)"
            print("SAVED_Country",SavedCountry)
            UserDefaults.standard.set(SavedCountry, forKey: "myCountry")
            print("Mycountry",SavedCountry)
            
            self.RecordsArray1.removeAllObjects()
            self.GetStatesList()
        }
        else if tableView == Select_State_TableView
        {
            DispatchQueue.main.async
                {
                    self.State_lbl.text = "\((self.RecordsArray1[indexPath.row] as! NSDictionary)["region_name"]!)"
                    self.Select_State_View.isHidden = true
                    self.State_View.frame.size.height = (80/568)*self.FullHeight
                    self.State_View.autoresizesSubviews = true
                    self.City_View.isHidden = false
                    //                    self.animateState(toggle: false)
                    let SavedRegion = "\((self.RecordsArray1[indexPath.row] as! NSDictionary)["region_id"]!)"
                    UserDefaults.standard.set(SavedRegion, forKey: "myRegion")
                    self.RecordsArray2.removeAllObjects()
                    self.GetCityList()
                    
            }
            
            
        }
        else if tableView == Select_City_tableview
        {
            DispatchQueue.main.async {
                self.City_lbl.text = "\((self.RecordsArray2[indexPath.row] as! NSDictionary)["city_name"]!)"
                
                self.Select_City_View.isHidden = true
                self.City_View.frame.size.height = (80/568)*self.FullHeight
                self.City_View.autoresizesSubviews = true
                self.SuburbView.isHidden = false
                let SavedCity = "\((self.RecordsArray2[indexPath.row] as! NSDictionary)["city_id"]!)"
                UserDefaults.standard.set(SavedCity, forKey: "myCity")
                self.RecordsArray3.removeAllObjects()
                self.GetSuburbList()
            }
            
        }
        else if tableView == Select_Suburb_Tableview
        {
            DispatchQueue.main.async {
                self.Suburb_lbl.text = "\((self.RecordsArray3[indexPath.row] as! NSDictionary)["suburbs_name"]!)"
                self.Select_Suburb_View.isHidden = true
                let SavedSuburb = "\((self.RecordsArray3[indexPath.row] as! NSDictionary)["suburbs_id"]!)"
                UserDefaults.standard.set(SavedSuburb, forKey: "mySuburb")
                self.SuburbView.autoresizesSubviews = false
                self.SuburbView.frame.size.height = (80/568)*self.FullHeight
                self.SuburbView.autoresizesSubviews = true
                
                
            }
            
        }
        else
        {
            State_lbl.text = "\((RecordsArray1[indexPath.row] as! NSDictionary)["region_name"]!)"
            print("Hello Esolz")
        }
    }
    //MARK:- Product Listing API Fire
    func loadData()
    {
        dispatchGroup.enter()
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_cart_page_details")!   //change the url
        
        var parameters : String = ""
        
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    if "\(json["response"]!)".elementsEqual("1")
                        
                    {
                        self.dispatchGroup.leave()
                        
                        self.dispatchGroup.notify(queue: .main, execute: {
                            self.info_array = json["info_array"] as? NSDictionary
//                            print("INFO_ARRAY",self.info_array)
                            self.ProductDetails = self.info_array["product_details"] as? NSMutableArray
                            
                            
                            
                            
                            for i in (0..<self.ProductDetails.count)
                            {
                                let ProDetail = self.ProductDetails[i] as! NSDictionary
                                print("ProDetail is : " , ProDetail)
                                self.CheckSeller = ProDetail["seller_id"] as? String
                                //print("CheckSeller",self.CheckSeller)
                                
                                self.recordsArray.add(ProDetail )
                            }
//                            print("RECORDSARRAY",self.recordsArray)
                            print("CheckSeller",self.CheckSeller!)
                            self.PriceDetails = self.info_array["price_details"] as? NSMutableDictionary
                            
                            
                            
                            SVProgressHUD.dismiss()
                            self.ProductListing.delegate = self
                            self.ProductListing.dataSource = self
                            self.ProductListing.reloadData()
                            self.PriceDetailsAPI()
                            self.ProductListing.rowHeight = UITableView.automaticDimension
                            self.ProductListing.estimatedRowHeight = UITableView.automaticDimension
                            self.SetFrames()


                            
                        })
                    }
                        
                    else
                    {
                        self.dispatchGroup.leave()
                        self.dispatchGroup.notify(queue: .main, execute: {
                        })
                        
                        
                    }

//                      self.ProductListingHeight()

                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
//    func ProductListingHeight()
//    {
//        self.CheckOutTableHeightConstraint.constant = self.ProductListing.visibleCells[0].bounds.height * CGFloat(self.recordsArray.count)
//    }
    //MARK:- Frame Set
    func SetFrames()
    {
//        let tempheight = (114/568)*self.FullHeight * CGFloat(self.recordsArray.count)

//        self.ProductListing.frame = CGRect(x: 0, y: 0, width: self.FullWidth, height: tempheight)
//        self.PriceDetailsView.frame = CGRect(x: 0, y: self.ProductListing.frame.origin.y + self.ProductListing.frame.size.height, width: self.FullWidth, height: (127/568)*self.FullHeight)

        //self.DetailsView.frame = CGRect(x: 0, y: self.PriceDetailsView.frame.origin.y + self.PriceDetailsView.frame.size.height, width: self.FullWidth, height: self.DetailsView.frame.size.height)
//        self.DetailsView.frame.origin.y = self.PriceDetailsView.frame.origin.y + self.PriceDetailsView.frame.size.height

        //        self.Payment_View.frame.origin.y = (888/568)*self.FullHeight
        //        self.Payment_View.frame.size.height = (78/568)*self.FullHeight
        //       self.Details2View.frame.size.height = self.Payment_View.frame.origin.y + self.Payment_View.frame.size.height
        
//        self.Details1View.frame.size.height = (30/568)*self.FullHeight
//        self.Details2View.frame.size.height = (886/568)*self.FullHeight
//        //        self.DetailsView.frame.size.height = (1004/5658)*self.FullHeight
//        self.Payment_View.frame.size.height = (54/568)*self.FullHeight
//        self.Deals_Payment_View.frame.size.height = (54/568)*self.FullHeight

        
        if CheckSeller.elementsEqual("0")
        {
            self.Deals_Payment_View.isHidden = false
            self.Payment_View.isHidden = true
        }
        else if CheckSeller.elementsEqual("1")
        {
            self.Deals_Payment_View.isHidden = true
            self.Payment_View.isHidden = false
        }
        else
        {
            self.Deals_Payment_View.isHidden = true
            self.Payment_View.isHidden = false
        }
        //        if Wallet_View.isHidden == false
        //        {
        //            self.Stripe_View.isHidden = true
        //            self.Wallet_View.frame.origin.y = self.Details2View.frame.origin.y + self.Details2View.frame.size.height
        //            self.Wallet_View.frame.size.height = (86/568)*self.FullHeight
        //            self.Check_out.frame.origin.y = self.Wallet_View.frame.origin.y + self.Wallet_View.frame.size.height
        //            self.Check_out.frame.size.height = (34/568)*self.FullHeight
        //            self.DetailsView.frame.size.height = self.Check_out.frame.origin.y + self.Check_out.frame.size.height
        //
        //            self.MainScroll.contentSize = CGSize(width: self.FullWidth, height: self.DetailsView.frame.origin.y + self.DetailsView.frame.size.height + (95/568)*self.FullHeight)
        //
        //
        //        }
        //        else if Stripe_View.isHidden == false
        //        {
        //            self.Wallet_View.isHidden = true
        //             self.Stripe_View.frame.origin.y = self.Details2View.frame.origin.y + self.Details2View.frame.size.height
        //            self.Stripe_View.frame.size.height = (54/568)*self.FullHeight
        //            self.Check_out.frame.origin.y = self.Stripe_View.frame.origin.y + self.Stripe_View.frame.size.height
        //            self.Check_out.frame.size.height = (34/568)*self.FullHeight
        //            self.DetailsView.frame.size.height = self.Check_out.frame.origin.y + self.Check_out.frame.size.height
        //            self.MainScroll.contentSize = CGSize(width: self.FullWidth, height: self.DetailsView.frame.origin.y + self.DetailsView.frame.size.height)
        //        }
        //        else
        //        {
        //            self.Stripe_View.isHidden = true
        //            self.Wallet_View.isHidden = true
        //            self.Check_out.frame.origin.y = self.Details2View.frame.origin.y + self.Details2View.frame.size.height
        //            self.Check_out.frame.size.height = (34/568)*self.FullHeight
        //            self.DetailsView.frame.size.height = self.Check_out.frame.origin.y + self.Check_out.frame.size.height
        //
        //            self.MainScroll.contentSize = CGSize(width: self.FullWidth, height: self.DetailsView.frame.origin.y + self.DetailsView.frame.size.height + (90/568)*self.FullHeight)
        //        }
//        self.Check_out.frame.origin.y = self.Details2View.frame.origin.y + self.Details2View.frame.size.height
//        self.Check_out.frame.size.height = (34/568)*self.FullHeight
//        self.DetailsView.frame.size.height = self.Check_out.frame.origin.y + self.Check_out.frame.size.height
//        self.MainScroll.contentSize = CGSize(width: self.FullWidth, height: self.DetailsView.frame.origin.y + self.DetailsView.frame.size.height)
//        print("MainScrollViewHeight",self.MainScroll.contentSize)

    }
    func SetFrame1()
    {
        //        let tempheight = (114/568)*self.FullHeight * CGFloat(self.recordsArray.count)
        //        self.ProductListing.frame = CGRect(x: 0, y: 0, width: self.FullWidth, height: tempheight)
        //        self.PriceDetailsView.frame = CGRect(x: 0, y: self.ProductListing.frame.origin.y + self.ProductListing.frame.size.height, width: self.FullWidth, height: (127/568)*self.FullHeight)
        //
        //        self.DetailsView.frame.origin.y = self.PriceDetailsView.frame.origin.y + self.PriceDetailsView.frame.size.height
        //
        //
        //        self.Details1View.frame.size.height = (30/568)*self.FullHeight
        //        self.Details2View.frame.size.height = (886/568)*self.FullHeight
        //        self.Payment_View.frame.size.height = (54/568)*self.FullHeight
        //        self.Deals_Payment_View.frame.size.height = (54/568)*self.FullHeight
        //
        //
        
        self.Wallet_View.isHidden = false
        self.Stripe_View.isHidden = true
//        self.Wallet_View.frame.size.height = (86/568)*self.FullHeight

        //        self.DetailsView.autoresizesSubviews = false
//        self.Details2View.frame.size.height = (886/568)*self.FullHeight
//        self.Wallet_View.frame.origin.y = self.Details2View.frame.origin.y + self.Details2View.frame.size.height
//        //self.Wallet_View.frame.size.height = (86/568)*self.FullHeight
//        self.Check_out.frame.origin.y = self.Wallet_View.frame.origin.y + self.Wallet_View.frame.size.height
//        self.Check_out.frame.size.height = (34/568)*self.FullHeight
//        self.DetailsView.autoresizesSubviews = false
//        self.DetailsView.frame.size.height = self.Check_out.frame.origin.y + self.Check_out.frame.size.height
//        self.DetailsView.autoresizesSubviews = true

//        self.MainScroll.contentSize = CGSize(width: self.FullWidth, height: self.DetailsView.frame.origin.y + self.DetailsView.frame.size.height)
//        print("ScrollView1 Height",self.MainScroll.contentSize)
    }
    func SetFrame2()
    {
        //        let tempheight = (114/568)*self.FullHeight * CGFloat(self.recordsArray.count)
        //        self.ProductListing.frame = CGRect(x: 0, y: 0, width: self.FullWidth, height: tempheight)
        //        self.PriceDetailsView.frame = CGRect(x: 0, y: self.ProductListing.frame.origin.y + self.ProductListing.frame.size.height, width: self.FullWidth, height: (127/568)*self.FullHeight)
        //
        //        self.DetailsView.frame.origin.y = self.PriceDetailsView.frame.origin.y + self.PriceDetailsView.frame.size.height
        //
        //
        //        self.Details1View.frame.size.height = (30/568)*self.FullHeight
        //        self.Details2View.frame.size.height = (886/568)*self.FullHeight
        //        self.Payment_View.frame.size.height = (54/568)*self.FullHeight
        //        self.Deals_Payment_View.frame.size.height = (54/568)*self.FullHeight
        //
        
        
        self.Wallet_View.isHidden = true
        self.Stripe_View.isHidden = false
//        self.Stripe_View.frame.size.height = (54/568)*self.FullHeight

        //self.Details2View.frame.size.height = (886/568)*self.FullHeight
//        self.Stripe_View.frame.origin.y = self.Details2View.frame.origin.y + self.Details2View.frame.size.height
//
//        self.Check_out.frame.origin.y = self.Stripe_View.frame.origin.y + self.Stripe_View.frame.size.height
//        self.Check_out.frame.size.height = (34/568)*self.FullHeight
//        self.DetailsView.autoresizesSubviews = false
//        self.DetailsView.frame.size.height = self.Check_out.frame.origin.y + self.Check_out.frame.size.height
//        self.DetailsView.autoresizesSubviews = true
//
//        self.MainScroll.contentSize = CGSize(width: self.FullWidth, height: self.DetailsView.frame.origin.y + self.DetailsView.frame.size.height)
//        print("ScrollView2 Height",self.MainScroll.contentSize)
    }
    func SetFrame3()
    {
        //        let tempheight = (114/568)*self.FullHeight * CGFloat(self.recordsArray.count)
        //        self.ProductListing.frame = CGRect(x: 0, y: 0, width: self.FullWidth, height: tempheight)
        //        self.PriceDetailsView.frame = CGRect(x: 0, y: self.ProductListing.frame.origin.y + self.ProductListing.frame.size.height, width: self.FullWidth, height: (127/568)*self.FullHeight)
        //
        //        self.DetailsView.frame.origin.y = self.PriceDetailsView.frame.origin.y + self.PriceDetailsView.frame.size.height
        //
        //
        //        self.Details1View.frame.size.height = (30/568)*self.FullHeight
        //        self.Details2View.frame.size.height = (886/568)*self.FullHeight
        //        self.Payment_View.frame.size.height = (54/568)*self.FullHeight
        //        self.Deals_Payment_View.frame.size.height = (54/568)*self.FullHeight
//        self.DetailsView.frame.origin.y = self.PriceDetailsView.frame.origin.y + self.PriceDetailsView.frame.size.height

        self.Stripe_View.isHidden = true
        self.Wallet_View.isHidden = true
        //self.Details2View.frame.size.height = (886/568)*self.FullHeight
//        self.Check_out.frame.origin.y = self.Details2View.frame.origin.y + self.Details2View.frame.size.height
//        self.Check_out.frame.size.height = (34/568)*self.FullHeight
//        self.DetailsView.autoresizesSubviews = false
//        self.DetailsView.frame.size.height = self.Check_out.frame.origin.y + self.Check_out.frame.size.height
//        self.DetailsView.autoresizesSubviews = true
//        self.MainScroll.contentSize = CGSize(width: self.FullWidth, height: self.DetailsView.frame.origin.y + self.DetailsView.frame.size.height)
//        print("ScrollView3 Height",self.MainScroll.contentSize)
    }
    //MARK:- PriceDetails API Fire
    func PriceDetailsAPI()
    {
        self.Delivery_Price.text = "\(self.PriceDetails["shipping_charge"]!)"
        self.Delivery_Price.textColor = UIColor.green
        self.Total_Price.text = "\(self.PriceDetails["currency_symbol"]!)" + "\(self.PriceDetails["total_amount"]!)"
        self.Item_lbl.text = "Price(" + "\(self.PriceDetails["count_items"]!)" + "Item)"
        self.Item_Price.text = "\(self.PriceDetails["currency_symbol"]!)" + "\(self.PriceDetails["total_price"]!)"
        self.Delivery_Price.font = UIFont(name: self.Delivery_Price.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
        self.Total_Price.font = UIFont(name: self.Total_Price.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Item_lbl.font = UIFont(name: self.Item_lbl.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Item_Price.font = UIFont(name: self.Item_Price.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    //MARK:- Country List API
    func getCountryList()
    {
        
        dispatchGroup.enter()
        
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_country_region_city")!       //change the url
        
        print("all country list URL : ", url)
        
        var parameters : String = ""
        
//        let langID = UserDefaults.standard.string(forKey: "langID")

        parameters = "lang_id=EN"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("all Country List Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("1")
                    {
                        self.allCountryListDictionary = json.copy() as? NSDictionary
                        print("ALLCOUNTRYLIST",self.allCountryListDictionary!)
                        //Code Start
                        self.info_array1 = json["info_array"] as? NSArray
                        print("CountryListAPI",self.info_array1!)
                        for j in (0..<self.info_array1.count)
                        {
                            let CountryDetail = self.info_array1[j] as! NSDictionary
                            print("CountryDetails is : " , CountryDetail)
                            self.RecordsArray.add(CountryDetail )
                        }
                        print("RecordsArrayCount",self.RecordsArray!)
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            
                            self.SelectCountry_tableView.delegate = self
                            self.SelectCountry_tableView.dataSource = self
                            self.SelectCountry_tableView.reloadData()
                            SVProgressHUD.dismiss()
                            
                        }
                        
                    }
                    else
                    {
                        self.dispatchGroup.leave()
                        self.dispatchGroup.notify(queue: .main, execute: {
                        })
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }
    //MARK:- GET States List From API
    func GetStatesList()
    {
        
        dispatchGroup.enter()
        
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        let CountryId = UserDefaults.standard.string(forKey: "myCountry")
        print("CountryId",CountryId ?? "")
        //self.selectedCountry = UserDefaults.standard.string(forKey: "SavedCountry")
        let url = URL(string: GLOBALAPI + "app_country_region_city")!       //change the url
        
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "lang_id=\(langID!)&mycountry_id=\(CountryId!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("all Country List Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("1")
                    {
                        
                        //Code Start
                        self.info_array2 = json["info_array"] as? NSArray
                        print("CountryListAPI",self.info_array2!)
                        for i in (0..<self.info_array2.count)
                        {
                            let RegionDetail = self.info_array2[i] as! NSDictionary
                            print("RegionDetail is : " , RegionDetail)
                            self.RecordsArray1.add(RegionDetail )
                        }
                        print("RecordsArrayCount1",self.RecordsArray1!)
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            
                            self.Select_State_TableView.delegate = self
                            self.Select_State_TableView.dataSource = self
                            self.Select_State_TableView.reloadData()
                            
                            SVProgressHUD.dismiss()
                        }
                        
                    }
                    else
                    {
                        self.dispatchGroup.leave()
                        self.dispatchGroup.notify(queue: .main, execute: {
                            SVProgressHUD.dismiss()
                        })
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }
    
    //MARK:- Get City List
    func GetCityList()
    {
        
        dispatchGroup.enter()
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        let RegionId = UserDefaults.standard.string(forKey: "myRegion")
        print("RegionId",RegionId ?? "")
        
        let url = URL(string: GLOBALAPI + "app_country_region_city")!   //change the url
        
        var parameters : String = ""
        // parameters = "region_id=14&lang_id=\(langID!)"
        
        parameters = "region_id=\(RegionId!)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    if "\(json["response"]!)".elementsEqual("1")
                        
                    {
                        //Code Start
                        self.info_array3 = json["info_array"] as? NSArray
                        print("CountryListAPI",self.info_array3!)
                        for i in (0..<self.info_array3.count)
                        {
                            let CityDetail = self.info_array3[i] as! NSDictionary
                            print("CityDetail is : " , CityDetail)
                            self.RecordsArray2.add(CityDetail )
                        }
                        print("RecordsArrayCount1",self.RecordsArray2!)
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            
                            self.Select_City_tableview.delegate = self
                            self.Select_City_tableview.dataSource = self
                            self.Select_City_tableview.reloadData()
                            
                            SVProgressHUD.dismiss()
                        }
                        
                    }
                        
                    else
                    {
                        self.dispatchGroup.leave()
                        self.dispatchGroup.notify(queue: .main, execute: {
                            SVProgressHUD.dismiss()
                        })
                        
                        
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    
    //MARK:- Get Suburb List
    func GetSuburbList()
    {
        dispatchGroup.enter()
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        let CityId = UserDefaults.standard.string(forKey: "myCity")
        print("CityId",CityId ?? "")
        
        let url = URL(string: GLOBALAPI + "app_country_region_city")!   //change the url
        
        var parameters : String = ""
        
        
        parameters = "city_id=\(CityId!)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    if "\(json["response"]!)".elementsEqual("1")
                        
                    {
                        //Code Start
                        self.info_array4 = json["info_array"] as? NSArray
                        print("SuburbListAPI",self.info_array4!)
                        for i in (0..<self.info_array4.count)
                        {
                            let SuburbDetail = self.info_array4[i] as! NSDictionary
                            print("SuburbDetail is : " , SuburbDetail)
                            self.RecordsArray3.add(SuburbDetail )
                        }
                        print("RecordsArrayCount3",self.RecordsArray3!)
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            
                            self.Select_Suburb_Tableview.delegate = self
                            self.Select_Suburb_Tableview.dataSource = self
                            self.Select_Suburb_Tableview.reloadData()
                            
                            SVProgressHUD.dismiss()
                        }
                        
                    }
                        
                    else
                    {
                        self.dispatchGroup.leave()
                        self.dispatchGroup.notify(queue: .main, execute: {
                            SVProgressHUD.dismiss()
                        })
                        
                        
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }
    //MARK:- Animate TableView For Selected Country Catagory
    func animateCountry(toggle: Bool) {
        if toggle {
            
            UIView.animate(withDuration: 0.3){
                //self.SelectCountry_View.autoresizesSubviews = true
                self.Country_View.autoresizesSubviews = false
                self.Country_View.frame.size.height = (300/568)*self.FullHeight
                self.SelectCountry_View.isHidden = false
                self.SelectCountry_View.frame.size.height = ((250/568)*self.FullHeight)
                self.SelectCountry_tableView.frame.size.height = ((250/568)*self.FullHeight)
                self.SelectCountry_View.autoresizesSubviews = true
            }
        }
        else {
            
            UIView.animate(withDuration: 0.3){
                self.SelectCountry_View.autoresizesSubviews = false
                self.Country_View.frame.size.height = (80/568)*self.FullHeight
                self.SelectCountry_View.isHidden = true
                self.SelectCountry_View.frame.size.height = ((1/568)*self.FullHeight)
                self.SelectCountry_tableView.frame.size.height = ((1/568)*self.FullHeight)
            }
        }
        
    }
    //MARK: - Animate TableView For Selected State Catagory
    func animateState(toggle: Bool){
        if toggle {
            
            UIView.animate(withDuration: 0.3){
                // self.Select_State_View.autoresizesSubviews = true
                self.State_View.autoresizesSubviews = false
                self.State_View.frame.size.height = (200/568)*self.FullHeight
                self.Select_State_View.isHidden = false
                
                self.City_View.isHidden = true
                self.SuburbView.isHidden = true
                
                self.Select_State_TableView.frame.size.height = ((150/568)*self.FullHeight)
                self.Select_State_TableView.frame.size.height = ((150/568)*self.FullHeight)
                self.Select_State_View.autoresizesSubviews = true
            }
        }
        else {
            
            UIView.animate(withDuration: 0.3){
                self.Select_State_View.autoresizesSubviews = false
                self.State_View.frame.size.height = (80/568)*self.FullHeight
                self.Select_State_View.isHidden = true
                
                self.City_View.isHidden = false
                self.SuburbView.isHidden = false
                
                self.Select_State_TableView.frame.size.height = ((1/568)*self.FullHeight)
                self.Select_State_TableView.frame.size.height = ((1/568)*self.FullHeight)
                
            }
        }
    }
    //MARK:- Animate Tableview for Selected City
    func animateCity(toggle: Bool){
        if toggle {
            
            UIView.animate(withDuration: 0.3){
                self.City_View.autoresizesSubviews = false
                self.City_View.frame.size.height = (300/568)*self.FullHeight
                self.Select_City_View.isHidden = false
                self.Select_City_tableview.frame.size.height = ((250/568)*self.FullHeight)
                self.Select_City_tableview.frame.size.height = ((250/568)*self.FullHeight)
                self.Select_City_View.autoresizesSubviews = true
            }
        }
        else {
            
            UIView.animate(withDuration: 0.3){
                self.Select_City_View.autoresizesSubviews = false
                self.Select_City_View.isHidden = true
                self.Select_City_tableview.frame.size.height = ((1/568)*self.FullHeight)
                self.Select_City_tableview.frame.size.height = ((1/568)*self.FullHeight)
            }
        }
    }
    
    //MARK:- Check Out Button
    @IBAction func Check_Out_Btn(_ sender: UIButton) {
        
        if (First_name_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your First Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (Last_name_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Last Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (Email_address_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Email Address", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (Phone_no_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Phone Number", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (House_no_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your House Number", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
            
        }
        else if (Street_name_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Street Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (Country_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Country Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if ((Country_lbl.text?.elementsEqual("Select"))!)
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Country Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (State_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your State Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if ((State_lbl.text?.elementsEqual("Select"))!)
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your State Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (City_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your City Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if ((City_lbl.text?.elementsEqual("Select"))!)
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your City Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (Suburb_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Suburb Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if ((Suburb_lbl.text?.elementsEqual("Select"))!)
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Suburb Name", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (Postal_code_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Postal Code", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (Other_info_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Other Information", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (Nearby_town_lbl.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Your Nearby Town", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if Wallet_View.isHidden == false
        {
            if (name_txt.text?.isEmpty)!
            {
                let alert = UIAlertController(title: "Warning", message: "Please Enter Your Name", preferredStyle: .alert)
                
                let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
            }
            else if (cardno_txt.text?.isEmpty)!
            {
                let alert = UIAlertController(title: "Warning", message: "Please Enter Your Card No", preferredStyle: .alert)
                
                let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
            }
            else if (cvv_txt.text?.isEmpty)!
            {
                let alert = UIAlertController(title: "Warning", message: "Please Enter Your Card CVV No", preferredStyle: .alert)
                
                let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
            }
            else if (expire_txt.text?.isEmpty)!
            {
                let alert = UIAlertController(title: "Warning", message: "Please Enter Card Expiry Date", preferredStyle: .alert)
                
                let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
            }
            else
            {
                //Do Nothing
            }
        }
        else if Shipping_Type_View.isHidden == false
        {
            if Pickup_Outlet.isSelected == false || Delivery_Outlet.isSelected == false
            {
                print("selectednot")
                let alert = UIAlertController(title: "Warning", message: "Please Select A Shipping Type", preferredStyle: .alert)
                
                let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
            }
            else
            {
                print("Selected")
                //Do Nothing
            }
        }
        else if Payment_View.isHidden == false
        {
            if stripe_btn_outlet.isSelected == false || Wallet_btn_outlet.isSelected == false || paypal_btn_outlet.isHidden == false
                
            {
                print("selectednot")
                let alert = UIAlertController(title: "Warning", message: "Please Select A Payment Type", preferredStyle: .alert)
                
                let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
                
                alert.addAction(ok)
                
                self.present(alert, animated: true, completion: nil )
            }
        }
            //MARK:- Navigation To Order Placed Successfully Page
        else
        {
            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "orderplaced") as! OrderPlacedViewController
            
            self.navigationController?.pushViewController(nav, animated: true)
        }
    }
    //MARK:- Set Font
    func SetFont()
    {
        self.First_name_lbl.font = UIFont(name: self.First_name_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Last_name_lbl.font = UIFont(name: self.Last_name_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Email_address_lbl.font = UIFont(name: self.Email_address_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Phone_no_lbl.font = UIFont(name: self.Phone_no_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.House_no_lbl.font = UIFont(name: self.House_no_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Street_name_lbl.font = UIFont(name: self.Street_name_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Postal_code_lbl.font = UIFont(name: self.Postal_code_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Other_info_lbl.font = UIFont(name: self.Other_info_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Nearby_town_lbl.font = UIFont(name: self.Nearby_town_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Country_lbl.font = UIFont(name: self.Country_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 16)))
        self.State_lbl.font = UIFont(name: self.State_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 16)))
        self.City_lbl.font = UIFont(name: self.City_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 16)))
        self.Suburb_lbl.font = UIFont(name: self.Suburb_lbl.font!.fontName, size: CGFloat(Get_fontSize(size: 16)))
        self.Lbl1.font = UIFont(name: self.Lbl1.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl2.font = UIFont(name: self.Lbl2.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl3.font = UIFont(name: self.Lbl3.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl4.font = UIFont(name: self.Lbl4.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl5.font = UIFont(name: self.Lbl5.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl6.font = UIFont(name: self.Lbl6.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl7.font = UIFont(name: self.Lbl7.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl8.font = UIFont(name: self.Lbl8.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl9.font = UIFont(name: self.Lbl9.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl10.font = UIFont(name: self.Lbl10.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl11.font = UIFont(name: self.Lbl11.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl12.font = UIFont(name: self.Lbl12.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl13.font = UIFont(name: self.Lbl13.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl14.font = UIFont(name: self.Lbl14.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl15.font = UIFont(name: self.Lbl15.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl16.font = UIFont(name: self.Lbl16.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl17.font = UIFont(name: self.Lbl17.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl18.font = UIFont(name: self.Lbl18.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl19.font = UIFont(name: self.Lbl19.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Lbl20.font = UIFont(name: self.Lbl20.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.Check_out.titleLabel?.font = UIFont(name: self.Check_out.titleLabel!.font.fontName, size: 16)
        
    }
    
    
}
