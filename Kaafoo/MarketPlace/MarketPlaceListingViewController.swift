//
//  MarketPlaceListingViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 16/04/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import GoogleMaps
import GooglePlaces

class MarketPlaceListingViewController: GlobalViewController,GMSMapViewDelegate {
    
    var SelectedSortPickerRow : Int! = 0

    @IBOutlet weak var SortView: UIView!
    @IBOutlet weak var SortPickerView: UIPickerView!
    @IBOutlet weak var SortSearchHeaderView: UIView!
    @IBOutlet weak var ProductSearchView: UIView!
    @IBOutlet weak var AddtoCartOutlet: UIButton!
    @IBOutlet weak var DataPicker: UIPickerView!
    @IBOutlet weak var ShowPickerView: UIView!
    @IBOutlet weak var ListGridShowOutlet: UIButton!
    @IBOutlet weak var GridView: UIView!
    @IBOutlet weak var GridCollectionView: UICollectionView!
    @IBOutlet weak var ListingView: UIView!
    @IBOutlet weak var MarketPkaceListingTableview: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var GProductDetailsView: UIView!
    
    //MARK:- GPRoduct Details View Outlet Connections
    
    @IBOutlet weak var GProductImage: UIImageView!
    @IBOutlet weak var GProductName: UILabel!
    @IBOutlet weak var GProductType: UILabel!
    @IBOutlet weak var GStartPrice: UILabel!
    @IBOutlet weak var GReservePrice: UILabel!
    @IBOutlet weak var GProductListingTime: UILabel!
    @IBOutlet weak var GProductClosingTime: UILabel!
    
    var SelectedCategoryBool : Bool! = false
    var ProductsOnMapArray = [MapListing]()
    var index : Int! = 0
    var markers = [GMSMarker]()
    var ShowPIcker : String! = "A"
    var sortDict = [["key" : "" , "value" : "All"],["key" : "B" , "value" : "Most Bids"] , ["key" : "T" , "value" : "Title"] , ["key" : "LP" , "value" : "Lowest Priced"],["key" : "HP" , "value" : "Highest Priced"],["key" : "C" , "value" : "Closing Soon"],["key" : "N" , "value" : "Newest Listed"]]
    var searchActive : Bool = false
    var searchString : String!
    var ShowPicker : Bool! = false
    var PerPage : String! = ""
    var Search_option1 : String! = ""
    var Search_option2 : String! = ""
    var myCountryId : String! = ""
    var MycountryName : String! = ""
    var SortSearchRegionId : String! = ""
    var SortSearchSuburbId : String! = ""
    var SortSearchCityId : String! = ""
    var MinAmount : String! = ""
    var MaxAmount : String! = ""
    var SearchBy : String! = ""
    var ProductBarcode : String! = ""
    var MaplistStatus : String! = ""
    var MaxDist : String! = ""
    var subtype : String! = ""
    var watchListProductID : String! = ""
    let userID = UserDefaults.standard.string(forKey: "userID")
    var nextStart : String! = ""
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    let dispatchGroup = DispatchGroup()
    var typeCommaSeparatedString : String! = ""
    var filterCommaSeparatedString : String! = ""
    var conditionCommaSeparatedString : String! = ""
    var allTypesDictionary : NSDictionary!
    var typeID : String! = ""
    var sortingType : String! = ""
    var userAccountTypeCommaSeparatedString : String! = ""
    var allTypesArray : NSArray!
    var allProductListingDictionary : NSDictionary!
    var allProductListingArray : NSArray!
    var recordsArray : NSMutableArray! = NSMutableArray()
    var AllCategorylist : NSArray!
    var MainCategoryList : NSMutableArray! = NSMutableArray()
    var SubTypeCategorylist : NSMutableArray! = NSMutableArray()
    var ChildCategoryListing : NSArray!
    var tempImagesearchImageview : UIImageView = {
        let tempimagesearchimageview = UIImageView()
        tempimagesearchimageview.translatesAutoresizingMaskIntoConstraints = false
        return tempimagesearchimageview
    }()
    
    @IBOutlet weak var GPreviousBtnOutlet: UIButton!
    @IBOutlet weak var GNextButtonOutlet: UIButton!
    
    @IBAction func GNextBtn(_ sender: UIButton) {
        
        if index == self.markers.count
        {
            self.ShowAlertMessage(title: "Warning", message: "No More Product Found")
        }
            
        else if index > self.markers.count
        {
            self.ShowAlertMessage(title: "Warning", message: "No More Product Found")
        }
        else
        {
            
            CATransaction.begin()
            CATransaction.setAnimationDuration(0.5)
            
            index = index + 1
            
            if index == self.markers.count
            {
                self.ShowAlertMessage(title: "Warning", message: "NO DATA FOUND")
            }
            else if index > self.markers.count
            {
                self.ShowAlertMessage(title: "Warning", message: "NO DATA FOUND")
            }
                
            else
            {
                self.GProductDetailsView.isHidden = false
                let tempdict = markers[index].userData as! MapListing
                self.dispatchGroup.enter()
               
                self.GProductName.text = "\(tempdict.ProductName ?? "")"
                self.GProductType.text = "\(tempdict.ProductName ?? "")"
                self.GStartPrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
                self.GReservePrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
                self.GProductImage.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
                
                self.dispatchGroup.leave()
                
                self.GProductDetailsView.isHidden = false
                self.view.bringSubviewToFront(self.GProductDetailsView)
                
                self.dispatchGroup.notify(queue: .main, execute: {
                    let updateOne = GMSCameraUpdate.setTarget(self.markers[self.index].position, zoom: 20)
                    self.mapView?.moveCamera(updateOne)
                })
                
            }
            CATransaction.commit()
        }
    }
    @IBAction func GPreviousBtn(_ sender: UIButton) {
        
        CATransaction.begin()
        
        CATransaction.setAnimationDuration(0.5)
        
        index = index - 1
        
        if self.index == 0
        {
            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
        }
            
        else if self.index > self.markers.count
        {
            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
        }
        else if self.index < 0
        {
            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
        }
        else
        {
            self.GProductDetailsView.isHidden = false
            
            let tempdict = markers[index].userData as! MapListing
            
            self.dispatchGroup.enter()
            
            self.GProductName.text = "\(tempdict.ProductName ?? "")"
            self.GProductType.text = "\(tempdict.ProductName ?? "")"
            self.GStartPrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
            self.GReservePrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
            self.GProductImage.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
//            self.GProductDetailsSellerImage.sd_setImage(with: URL(string: "\(tempdict.SellerImage ?? "")"))
            
            self.dispatchGroup.leave()
            
            self.GProductDetailsView.isHidden = false
            self.view.bringSubviewToFront(self.GProductDetailsView)
            
            self.dispatchGroup.notify(queue: .main, execute: {
                
                let updateOne = GMSCameraUpdate.setTarget(self.markers[self.index].position, zoom: 20)
                self.mapView?.moveCamera(updateOne)
            })
        }
        
        CATransaction.commit()
    }
    

    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateWatchlist), name: NSNotification.Name(rawValue: "UpdateWatchlist"), object: nil)
        self.setLayer()
        self.GProductDetailsView.isHidden = true
        self.mapView.delegate = self
        self.ProductsOnMapArray = []
        self.startProductListingData()
        self.AddSortGesture()
        self.SortView.isHidden = true
        self.ListGridShowOutlet.setImage(UIImage(named: "list-1"), for: .normal)
        self.PickerBtn()
        self.CreateFavouriteView(inview: self.view)
        searchView.searchBar.delegate = self
        self.ProductSearchView.isHidden = true
        self.SearchBtnTap()
        self.ListingView.isHidden = false
        self.GridView.isHidden = true
        self.searchView.closeSearchviewButton.addTarget(self, action: #selector(HideSearchBar), for: .touchUpInside)
        self.MarketPkaceListingTableview.separatorStyle = .none
        self.MarketPkaceListingTableview.showsVerticalScrollIndicator = false
        self.ShowPickerView.isHidden = true
        self.SortPickerView.delegate = self
        self.SortPickerView.dataSource = self
        self.SortPickerView.reloadAllComponents()
        self.defineSortButtonActions()
        self.globalDispatchgroup.notify(queue: .main, execute: {
            if self.allProductListingArray.count == 0
            {
               self.ShowAlertMessage(title: "Warning", message: "No Data Found")
            }
            else
            {
               print("do nothing")
            }
        })
        
    }
    
    //MARK:- // Updation of Watchlist Status Using NSNotification
    
    @objc func updateWatchlist()
    {
       self.nextStart = "0"
        self.recordsArray.removeAllObjects()
        self.startProductListingData()
    }
    
//    //MARK: - View Will Appear Method
//
//    override func viewWillAppear(_ animated: Bool) {
//        self.MainCategoryOutlet.setTitle(MarketPlaceParameters.shared.SelectedMarketplaceCategory, for: .normal)
//    }
    
    //MARK:- Set Layer Function
    func setLayer()
    {
        self.GProductDetailsView.layer.borderColor = UIColor.black.cgColor
        self.GProductDetailsView.layer.borderWidth = 1.0
//        self.MarketPkaceListingTableview.alwaysBounceHorizontal = false
//        self.MarketPkaceListingTableview.alwaysBounceVertical = false
        
    }
    
    //MARK:- //Append Data in Map
    
    func AppendMapData()
    {
        self.dispatchGroup.enter()
        
        for i in 0..<self.recordsArray.count
        {
            self.ProductOnMapArray.append(MapListing(ProductName: "\((recordsArray[i] as! NSDictionary)["product_name"] ?? "")", ProductLatitude: Double(("\((recordsArray[i] as! NSDictionary)["lat"] ?? "")" as NSString).doubleValue), ProductLongitude: Double(("\((recordsArray[i] as! NSDictionary)["long"] ?? "")" as NSString).doubleValue), ProductImage: "\((recordsArray[i] as! NSDictionary)["product_photo"] ?? "")", CurrencySymbol: "\((recordsArray[i] as! NSDictionary)["currency_symbol"] ?? "")", StartPrice: "\((recordsArray[i] as! NSDictionary)["start_price"] ?? "")", ReservePrice: "\((recordsArray[i] as! NSDictionary)["reserve_price"] ?? "")", ProductID: "\((recordsArray[i] as! NSDictionary)["product_id"] ?? "")", SellerImage: "\((recordsArray[i] as! NSDictionary)["refund_type"] ?? "")", WatchlistStatus: "\((recordsArray[i] as! NSDictionary)["watchlist"] ?? "")"))
        }
        
        self.dispatchGroup.leave()
        
        self.dispatchGroup.notify(queue: .main, execute: {
            
            let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
            //            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            self.mapView.animate(to: cameraPosition)
            
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
            
            
            for state in self.ProductOnMapArray {
                let state_marker = GMSMarker()
                state_marker.position = CLLocationCoordinate2D(latitude: state.ProductLatitude ?? 0.0, longitude: state.ProductLongitude ?? 0.0)
                state_marker.title = state.ProductName
                state_marker.snippet = "Hey, this is \(state.ProductName!)"
                state_marker.map = self.mapView
                state_marker.userData = state
                self.markers.append(state_marker)
                
            }
        })
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        //        customInfoWindow?.removeFromSuperview()
       
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.5)
        
        self.index = 0
        self.GProductDetailsView.isHidden = true
        for i in 0..<self.markers.count
        {
            self.markers[i].icon = GMSMarker.markerImage(with: nil)
        }
        //        let update = GMSCameraUpdate.zoom(by: 10)
        let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
        //        GmapView.moveCamera(update)
        mapView.animate(to: cameraPosition)
        CATransaction.commit()
    }
    
    func centerInMarker(marker: GMSMarker) {
        
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate((marker as AnyObject).position)
        //let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: (self.mapView?.frame.height)!/2, left: (self.mapView?.frame.width)!/2, bottom: 0, right: 0))
        let updateOne = GMSCameraUpdate.setTarget(marker.position, zoom: 20)
        mapView?.moveCamera(updateOne)
        mapView.transform = .identity
        
    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        self.centerInMarker(marker: marker)
        
        print("markerPosition",markers[0].position)
        
        if let selectedMarker = mapView.selectedMarker {
            selectedMarker.icon = GMSMarker.markerImage(with: nil)
        }
        mapView.selectedMarker = marker
        marker.icon = GMSMarker.markerImage(with: UIColor.seaGreen())
        
        print("usermarkerdata",marker.userData!)
        
        
        self.GProductDetailsView.isHidden = false
        
        let tempdict = marker.userData as! MapListing
        
        self.dispatchGroup.enter()
        
        //        if (tempdict.WatchlistStatus?.elementsEqual("0"))!
        //        {
        //            self.GProductDetailsWatchlistOutlet.isHidden = false
        //            self.GProductDetailsWatchlistedOutlet.isHidden = true
        //        }
        //        else
        //        {
        //            self.GProductDetailsWatchlistOutlet.isHidden = true
        //            self.GProductDetailsWatchlistedOutlet.isHidden = false
        //        }
        
        self.GProductName.text = "\(tempdict.ProductName ?? "")"
        self.GProductType.text = "\(tempdict.ProductName ?? "")"
        self.GStartPrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
        self.GReservePrice.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
        self.GProductImage.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
        
        self.dispatchGroup.leave()
        
        //self.DetailsView.isHidden = false
        //self.view.bringSubviewToFront(self.DetailsView)
        
        //        self.GProductDetailsView.isHidden = true
        return true
    }



    // MARK:- // Buttons


    // MARK:- // Image Search Button

    @IBOutlet weak var imageSearchButtonOutlet: UIButton!
    @IBAction func imageSearchButton(_ sender: UIButton) {

        let imagePickerController = UIImagePickerController()

        imagePickerController.delegate = self

        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in

            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)

        }))

        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in

            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)

        }))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))

        self.present(actionSheet, animated: true, completion: nil)

    }



    // MARK:- // Filter Button

    @IBOutlet weak var FilterBtnOutlet: UIButton!
    @IBAction func FilterBtn(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {
            if self.filterView.isHidden == true {
                self.view.bringSubviewToFront(self.filterView)
                self.filterView.isHidden = false
            }
            else
            {
                self.view.sendSubviewToBack(self.filterView)
                self.filterView.isHidden = true
            }
        }

    }



    // MARK:- // MapVIew Button

    @IBOutlet weak var MapViewBtnOutlet: UIButton!
    @IBAction func MapviewBtn(_ sender: UIButton) {
        if mapView.isHidden == true
        {
            self.mapView.isHidden = false
            self.ListingView.isHidden = true
            self.GridView.isHidden = true
        }
        else
        {
            self.mapView.isHidden = true
            self.ListingView.isHidden = false
            self.GridView.isHidden = false
        }
    }





    // MARK:- // Add to Cart Button

    @IBAction func AddtoCartBtn(_ sender: UIButton) {
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "addtocart") as! AddToCartViewController
        self.navigationController?.pushViewController(navigate, animated: true)
    }



    // MARK:- // List Grid Button

    @IBAction func ListGridBtn(_ sender: UIButton) {
        
        if self.mapView.isHidden == false
        {
            self.mapView.isHidden = true
        }
        else
        {
            if ListingView.isHidden == false
            {
                self.GridView.isHidden = false
                self.ListingView.isHidden = true
                self.ListGridShowOutlet.setImage(UIImage(named: "Group 18"), for: .normal)
            }
            else
            {
                self.GridView.isHidden = true
                self.ListingView.isHidden = false
                self.ListGridShowOutlet.setImage(UIImage(named: "list-1"), for: .normal)
            }
        }
    }



    // MARK:- // Main Category Button

    @IBOutlet weak var MainCategoryOutlet: UIButton!
    @IBAction func MainCategoryBtn(_ sender: UIButton) {
        self.ShowPIcker = "A"
        self.DataPicker.selectRow(0, inComponent: 0, animated: false)
        self.DataPicker.reloadAllComponents()
        self.ShowPickerView.bringSubviewToFront(self.ShowPickerView)
        self.ShowPickerView.isHidden = !self.ShowPickerView.isHidden
    }


    // MARK:- // Sub Category Button

    @IBOutlet weak var SubcategoryOutlet: UIButton!
    @IBAction func SubCategoryBtn(_ sender: UIButton) {

        if SubcategoryOutlet.titleLabel?.text == "No Subtype Found"
        {
            print("Nothing")
        }
        else
        {
            if self.SubTypeCategorylist.count > 0
            {
                self.ShowPIcker = "S"
                self.DataPicker.selectRow(0, inComponent: 0, animated: false)
                self.DataPicker.reloadAllComponents()
                self.ShowPickerView.isHidden = !self.ShowPickerView.isHidden
            }
            else
            {
                self.ShowPickerView.isHidden = true
            }
        }

    }

    // MARK:- // Get Listing

    func startProductListingData() {

        self.myCountryId = "99"
        self.MycountryName = "INDIA"


        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)&cat_id=\(typeID!)&sorting_type=\(sortingType!)&user_acc_type_search=\(userAccountTypeCommaSeparatedString!)&type=\(self.typeFilter!)&filter=\(self.filterFilter!)&condition=\(conditionCommaSeparatedString!)&start_from=\(nextStart!)&top_cat_id=9&per_page=\(PerPage!)&search_option1=\(Search_option1!)&search_option2=\(Search_option2!)&mycountry_id=\(myCountryId!)&mycountry_name=\(MycountryName!)&sort_search_region_id=\(SortSearchRegionId!)&sort_search_suburb_id=\(SortSearchSuburbId!)&sort_search_city_id=\(SortSearchCityId!)&min_amount=\(MinAmount!)&max_amount=\(self.maxFilterPrice!)&search_by=\(SearchBy!)&product_barcode=\(ProductBarcode!)&map_list_status=\(MaplistStatus!)&max_dist=\(self.maxFilterDistance!)"

        self.CallAPI(urlString: "app_product_list", param: parameters) {

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.allProductListingDictionary = self.globalJson["product_info"] as? NSDictionary

            self.ChildCategoryListing = self.allProductListingDictionary["child_category_listing"] as? NSArray
            self.allProductListingArray = self.allProductListingDictionary["product_listing"] as? NSArray

            self.AllCategorylist = self.allProductListingDictionary["category_listing"]! as? NSArray


            for i in (0..<self.AllCategorylist.count)
            {
                self.MainCategoryList.add((self.AllCategorylist[i] as! NSDictionary)["cat_name"]!)
            }

            if self.ChildCategoryListing.count > 0
            {
                for i in (0..<self.ChildCategoryListing.count)
                {
                    self.SubTypeCategorylist.add((self.ChildCategoryListing[i] as! NSDictionary)["cat_name"]!)
                    print("Subtypename",((self.ChildCategoryListing[i] as! NSDictionary)["cat_name"]!))
                }

            }
            else
            {
                print("Do nothing")
            }


            for i in 0..<self.allProductListingArray.count

            {
                let tempDict = self.allProductListingArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)
            }
            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()
                
                self.AppendMapData()

                self.MarketPkaceListingTableview.delegate = self
                self.MarketPkaceListingTableview.dataSource = self
                self.MarketPkaceListingTableview.reloadData()

                self.GridCollectionView.delegate = self
                self.GridCollectionView.dataSource = self
                self.GridCollectionView.reloadData()

                self.DataPicker.delegate = self
                self.DataPicker.dataSource = self
                self.DataPicker.reloadAllComponents()

                SVProgressHUD.dismiss()

            }
        }
    }






    //MARK:- // Add to WatchList Selector Method

    @objc func addToWatchlist(sender : UIButton)
    {
        if userID?.isEmpty == true
        {
            let alert = UIAlertController(title: "Warning", message: "Please login and try gain later", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Warning", message: "Product is added to watchlist", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: {
                self.watchListProductID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"
                self.addToWatchList {
                    self.nextStart = "0"
                    self.recordsArray.removeAllObjects()
                    self.startProductListingData()

                }
            })
        }
    }


    // MARK:- // Remove From Watchlist Selector Method

    @objc func Removefromwatchlist(sender : UIButton)
    {
        if userID?.isEmpty == true
        {
            let alert = UIAlertController.init(title: "Warning", message: "Please login and try gain later", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "warning", message: "Product is removed from Watchlist", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: {
                self.watchListProductID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"
                self.removeFromWatchList()
                    {
                        self.nextStart = ""
                        self.recordsArray.removeAllObjects()
                        self.nextStart = "0"
                        self.startProductListingData()
                }
            })
        }
    }




    // MARK:- // JSON Post Method to Remove from Watchlist

    @objc func removeFromWatchList(completion : @escaping () -> ())

    {
        dispatchGroup.enter()
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        let url = URL(string: GLOBALAPI + "app_remove_watchlist")!

        print("add from watchlist URL : ", url)

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "user_id=\(userID!)&remove_product_id=\(watchListProductID!)&lang_id=\(langID!)"

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("remove from watchlist Response: " , json)
                    let alertmsg = "\(json["message"]!)"

                    if "\(json["response"] as! Bool)".elementsEqual("true")
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()

                            let alert = UIAlertController(title: "Warning", message: alertmsg, preferredStyle: .alert)
                            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                            alert.addAction(ok)
                            self.present(alert, animated: true, completion: nil)
                        }
                        completion()
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                        }
                        let alert = UIAlertController(title: "Warning", message: alertmsg, preferredStyle: .alert)
                        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                        alert.addAction(ok)
                        self.present(alert, animated: true, completion: nil)
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()



    }



    // MARK:- // JSON Post method to Remove from Watchlist


    func addToWatchList(completion : @escaping () -> ())

    {
        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }



        let url = URL(string: GLOBALAPI + "app_add_watchlist")!   //change the url

        print("add from watchlist URL : ", url)

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "user_id=\(userID!)&add_product_id=\(watchListProductID!)&lang_id=\(langID!)"

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        } 

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("add to watchlist Response: " , json)

                    if "\(json["response"] as! Bool)".elementsEqual("true")
                    {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()

                        }

                        completion()
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                        }
                    }



                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()



    }




    //MARK:- // HeaderView Search Button Tap

    func SearchBtnTap()
    {
        headerView.tapSearch.addTarget(self, action: #selector(ToggleShow), for: .touchUpInside)
    }
    @objc func ToggleShow()
    {
        if self.ProductSearchView.isHidden == true
        {
            self.ProductSearchView.isHidden = false
            let textFieldInsideSearchBar = searchView.searchBar.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.text = ""
            print("Do")
        }
        else if self.ProductSearchView.isHidden == false
        {
            self.ProductSearchView.isHidden = true
            let textFieldInsideSearchBar = searchView.searchBar.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.text = ""
            print("Do")
        }
        else
        {
            print("Do nothing")
        }
    }
    @objc func HideSearchBar()
    {
        self.ProductSearchView.isHidden = true
    }



    // MARK:- // Define Filter Button Actions

    func defineSortButtonActions() {

        self.filterView.buttonArray[0].addTarget(self, action: #selector(self.sortAction(sender:)), for: .touchUpInside)
        self.filterView.buttonArray[1].addTarget(self, action: #selector(self.filterAction(sender:)), for: .touchUpInside)


        self.filterView.tapToCloseButton.addTarget(self, action: #selector(self.tapToClose(sender:)), for: .touchUpInside)

    }


    // MARK;- // Sort Action

    @objc func sortAction(sender: UIButton) {

        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)

        self.SortView.isHidden = false
    }

    // MARK;- // Filter Action

    @objc func filterAction(sender: UIButton) {

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController

        navigate.rootVC = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "marketplacelisting") as! MarketPlaceListingViewController

        navigate.locationArray = self.allProductListingDictionary["filter_country_list"] as! NSArray

        navigate.typeArray = [sortClass(key: "P", value: "Asking Price", isClicked: false, totalNo: "nil"),sortClass(key: "B", value: "Buy Now", isClicked: false, totalNo: "nil"),sortClass(key: "A", value: "Auction", isClicked: false, totalNo: "nil")]

        navigate.filterArray = [sortClass(key: "S", value: "On Sale", isClicked: false, totalNo: "nil"),sortClass(key: "C", value: "Cash on Delivery", isClicked: false, totalNo: "nil")]

        navigate.showDynamicType = false
        navigate.showLocation = true
        navigate.showCondition = false
        navigate.showType = true
        navigate.showFilter = true
        navigate.showPrice = true
        navigate.showDistance = true

        self.navigationController?.pushViewController(navigate, animated: true)

    }


    // MARK;- // Tap To Close

    @objc func tapToClose(sender: UIButton) {

        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)
    }
}







// MARK:- // Imagepicker Delegates

extension MarketPlaceListingViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)


        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage

        self.tempImagesearchImageview.image = image

        picker.dismiss(animated: true, completion: nil)

        if let data = self.tempImagesearchImageview.image!.jpegData(compressionQuality: 1) {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "imageSearchResultVC") as! ImageSearchResultViewController

            navigate.imageData = data

            self.navigationController?.pushViewController(navigate, animated: true)

        }


    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {

        dismiss(animated: true, completion: nil)

    }

    func PickerBtn()
    {
        self.ShowPickerView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.ShowPickerView.addGestureRecognizer(tap)
    }
    @objc func handleTap(sender: UITapGestureRecognizer)
    {
        self.ShowPickerView.isHidden = true
    }
    func AddSortGesture()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(HandleTAp(sender:)))
        self.SortView.isUserInteractionEnabled = true
        self.SortView.addGestureRecognizer(tap)
    }
    @objc func HandleTAp(sender: UITapGestureRecognizer)
    {
        self.SortView.isHidden = true
    }

}



// MARK:- // Tableview Delegate Methods

extension MarketPlaceListingViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let obj = tableView.dequeueReusableCell(withIdentifier: "marketplacetblcell") as! MarketPlaceListingTableViewCell
        let path = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_photo"]!)"
        obj.Product_imageview.sd_setImage(with: URL(string: path))
        obj.Product_name.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        obj.Product_type.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type"]!)"
        obj.Product_address.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["address"]!)"
        obj.Closing_time.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["expire_time_nw"]!)"
        obj.Listing_time.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_time_ago"]!)"
        obj.Product_sellerName.text = "Business:" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type_status"]!)".elementsEqual("A")
        {
            obj.BidStackView.isHidden = false
            obj.BidCountValue.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["bid_count"] ?? "")" + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "bid", comment: "")
            
            let imageUrl = "\((self.recordsArray[indexPath.row] as! NSDictionary)["flag"] ?? "")"
            obj.BidImage.sd_setImage(with: URL(string: imageUrl))
            
        }
        else
        {
            obj.BidStackView.isHidden = true
        }
        
        
        if "\((((self.recordsArray[indexPath.row] as! NSDictionary)["user_details"]) as! NSDictionary)["user_id"]!)".isEmpty == true
        {
            obj.markView.isHidden = false
            obj.Product_sellerName.isHidden = true
        }
        else
        {
            obj.markView.isHidden = true
            obj.Product_sellerName.isHidden = false
        }

        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["special_offer_status"]!)".elementsEqual("1") && "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type_status"]!)".elementsEqual("B")
        {
            obj.discounted_price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
            obj.discounted_price.textColor = UIColor.red
            let txt = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            obj.Final_price.attributedText = txt.strikeThrough()
            obj.Final_price.textColor = UIColor.seaGreen()
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["special_offer_status"]!)".elementsEqual("0") && "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type_status"]!)".elementsEqual("B")
        {
            obj.discounted_price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            obj.discounted_price.textColor = UIColor.seaGreen()
            obj.Final_price.text = ""
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type_status"]!)".elementsEqual("A")
        {
            obj.discounted_price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            obj.discounted_price.textColor = UIColor.seaGreen()
            obj.Final_price.text = ""
        }
        else
        {
            obj.discounted_price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            obj.discounted_price.textColor = UIColor.seaGreen()
            obj.Final_price.text = ""
        }


        if langID.elementsEqual("AR")
        {
            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("Used Product")
            {
                obj.ProductConditionImage.image = UIImage(named: "RightUsed")
                obj.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
                obj.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4)

            }
            else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("New Product")
            {
                obj.ProductConditionImage.image = UIImage(named: "RightNew")
                obj.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "newStr", comment: "")
                 obj.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4)
            }
            else
            {
                obj.ProductConditionImage.image = UIImage(named: "")
                obj.ProductCondition.text = ""
                 obj.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4)
            }
        }
        else
        {
            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("Used Product")
            {
                obj.ProductConditionImage.image = UIImage(named: "LeftUsed")
                obj.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
                 obj.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4 * 7)
            }
            else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("New Product")
            {
                obj.ProductConditionImage.image = UIImage(named: "LeftNew")
                obj.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "newStr", comment: "")
                 obj.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4 * 7)
            }
            else
            {
                obj.ProductConditionImage.image = UIImage(named: "")
                obj.ProductCondition.text = ""
                obj.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4 * 7)
            }
        }


        if "\((((self.recordsArray[indexPath.row] as! NSDictionary)["user_details"]) as! NSDictionary)["user_verified_status"]!)".elementsEqual("V")
        {
            obj.Verified_image.isHidden = false
        }
        else
        {
            obj.Verified_image.isHidden = true
        }
        obj.Watchlist_btn.tag = indexPath.row
        obj.watchlistedBtn.tag = indexPath.row
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("1")
        {
            obj.Watchlist_btn.isHidden = true
            obj.watchlistedBtn.isHidden = false
            obj.watchlistedBtn.addTarget(self, action: #selector(Removefromwatchlist(sender:)), for: .touchUpInside)
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("0")
        {
            obj.Watchlist_btn.isHidden = false
            obj.watchlistedBtn.isHidden = true
            obj.Watchlist_btn.addTarget(self, action: #selector(addToWatchlist(sender:)), for: .touchUpInside)
        }
        else
        {
            obj.Watchlist_btn.isHidden = false
            obj.watchlistedBtn.isHidden = true
        }
        
        obj.selectionStyle = .none;
        obj.Product_name.textColor = UIColor.brown1()
        obj.Listing_time.textColor = UIColor.seaGreen()
        obj.Closing_time.textColor = UIColor.red
        obj.Product_sellerName.textColor = UIColor.brown1()
        obj.Product_address.textColor = UIColor.brown1()


        //MARK:- //Set Font For UILabels



        return obj
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let obj = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "productDetailsVC") as! ProductDetailsViewController
        //        obj.productID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
        //        self.navigationController?.pushViewController(obj, animated: true)
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
        navigate.ProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
        self.navigationController?.pushViewController(navigate, animated: true)
    }


}




// MARK:- // Collectionview Delegate Methods

extension MarketPlaceListingViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.recordsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "marketplacelisting", for: indexPath) as! MarketPlaceListingCollectionViewCell

        let path = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_photo"]!)"
        cell.Product_image.sd_setImage(with: URL(string: path))
        cell.Product_name.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        cell.Listing_time.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["listed_time_ago"]!)"
        cell.Expire_time.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["expire_time_nw"]!)"
        cell.ProductSellerName.text = "Business:" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"
        cell.Product_address.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["address"]!)"
        cell.Product_type.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type"]!)"

        if "\((((self.recordsArray[indexPath.row] as! NSDictionary)["user_details"]) as! NSDictionary)["user_id"]!)".isEmpty == true
        {
            cell.Product_StatusView.isHidden = false
            cell.Product_SellerView.isHidden = true
        }
        else
        {
            cell.Product_StatusView.isHidden = true
            cell.Product_SellerView.isHidden = false

        }
        if "\((((self.recordsArray[indexPath.row] as! NSDictionary)["user_details"]) as! NSDictionary)["user_verified_status"]!)".elementsEqual("V")
        {
            cell.Verified_image.isHidden = false
        }
        else
        {
            cell.Verified_image.isHidden = true
        }
        cell.watchlistBtnOutlet.tag = indexPath.row
        cell.WatchlistedBtnOutlet.tag = indexPath.row

        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("1")
        {
            cell.watchlistBtnOutlet.isHidden = true
            cell.WatchlistedBtnOutlet.isHidden = false
            cell.WatchlistedBtnOutlet.addTarget(self, action: #selector(Removefromwatchlist(sender:)), for: .touchUpInside)
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("0")
        {
            cell.watchlistBtnOutlet.isHidden = false
            cell.WatchlistedBtnOutlet.isHidden = true
            cell.watchlistBtnOutlet.addTarget(self, action: #selector(addToWatchlist(sender:)), for: .touchUpInside)
        }
        else
        {
            cell.watchlistBtnOutlet.isHidden = false
            cell.WatchlistedBtnOutlet.isHidden = true
        }


        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["special_offer_status"]!)".elementsEqual("1") && "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type_status"]!)".elementsEqual("B")
        {
            cell.Product_Price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
            cell.Product_Price.textColor = UIColor.red
            let txt = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            cell.Product_discounted_price.attributedText = txt.strikeThrough()
            cell.Product_discounted_price.textColor = UIColor.seaGreen()
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["special_offer_status"]!)".elementsEqual("0") && "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type_status"]!)".elementsEqual("B")
        {
            cell.Product_Price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            cell.Product_Price.textColor = UIColor.seaGreen()
            cell.Product_discounted_price.text = ""
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_type_status"]!)".elementsEqual("A")
        {
            cell.Product_Price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            cell.Product_Price.textColor = UIColor.seaGreen()
            cell.Product_discounted_price.text = ""
        }
        else
        {
            cell.Product_Price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
            cell.Product_Price.textColor = UIColor.seaGreen()
            cell.Product_discounted_price.text = ""
        }

        if langID.elementsEqual("AR")
        {
            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("Used Product")
            {
                cell.ProductConditionImage.image = UIImage(named: "RightUsed")
                cell.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
                cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4)

            }
            else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("New Product")
            {
                cell.ProductConditionImage.image = UIImage(named: "RightNew")
                cell.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "newStr", comment: "")
                cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4)
            }
            else
            {
                cell.ProductConditionImage.image = UIImage(named: "")
                cell.ProductCondition.text = ""
                cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4)
            }
        }
        else
        {
            if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("Used Product")
            {
                cell.ProductConditionImage.image = UIImage(named: "LeftUsed")
                cell.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "used", comment: "")
                cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4 * 7)
            }
            else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_condition"]!)".elementsEqual("New Product")
            {
                cell.ProductConditionImage.image = UIImage(named: "LeftNew")
                cell.ProductCondition.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "newStr", comment: "")
                cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4 * 7)
            }
            else
            {
                cell.ProductConditionImage.image = UIImage(named: "")
                cell.ProductCondition.text = ""
                cell.ProductCondition.transform = CGAffineTransform(rotationAngle: .pi/4 * 7)
            }
        }




        cell.Product_name.textColor = UIColor.brown1()
        cell.Listing_time.textColor = UIColor.seaGreen()
        cell.Expire_time.textColor = UIColor.red
        cell.ProductSellerName.textColor = UIColor.brown1()
        cell.Product_address.textColor = UIColor.brown1()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }



    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let padding : CGFloat! = 4

        let collectionViewSize = GridCollectionView.frame.size.width - padding

        return CGSize(width: collectionViewSize/2, height: 254 )
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
        navigate.ProductID = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
        self.navigationController?.pushViewController(navigate, animated: true)
    }

}





// MARK:- // Pickerview Delegate MEthods

extension MarketPlaceListingViewController: UIPickerViewDelegate,UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if self.ShowPIcker == "A"
        {
            return 1
        }
        else
        {
            return 1
        }

    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == DataPicker
        {
            if self.ShowPIcker == "A"
            {
                return self.MainCategoryList.count
            }
            else
            {

                return self.SubTypeCategorylist.count
            }

        }
        else if pickerView == SortPickerView
        {
            return sortDict.count
        }
        else
        {
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == DataPicker
        {
            if ShowPIcker == "A"
            {
                if self.MainCategoryList.count == 0
                {
                    return "No Type Found"
                }
                else
                {
                    if self.SelectedCategoryBool == true
                    {
                        // self.MainCategoryOutlet.setTitle(self.MainCategoryList![0] as? String, for: .normal)
                        self.MainCategoryOutlet.setTitle(MarketPlaceParameters.shared.SelectedMarketplaceMainCategoryOutlet, for: .normal)
                    }
                    else
                    {
                    self.MainCategoryOutlet.setTitle(MarketPlaceParameters.shared.SelectedMarketplaceCategory, for: .normal)
                    }
                    print("datashow",self.MainCategoryList[row])
                                              
                    return self.MainCategoryList[row] as? String
                }
            }
            else
            {
                print("Nothing")
                if self.SubTypeCategorylist.count == 0
                {
                    self.SubcategoryOutlet.setTitle("No Subtype Found", for: .normal)
                    return "No Subtype Found"
                }
                else
                {
                    self.SubcategoryOutlet.setTitle(self.SubTypeCategorylist![0] as? String, for: .normal)
                    return self.SubTypeCategorylist[row] as? String
                }

            }

        }
        else if pickerView == SortPickerView
        {
            return "\((sortDict[row] as NSDictionary)["value"]!)"
        }
        else
        {
            return "No Data Found"
        }

    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == DataPicker
        {
            if ShowPIcker == "A"
            {
                self.SelectedCategoryBool = true
                self.MainCategoryOutlet.setTitle("\(self.MainCategoryList[row])", for: .normal)
                self.ShowPickerView.isHidden = true
                MarketPlaceParameters.shared.setSelectedMarketPlaceMainCategoryOutlet(selectedCategory: "\(self.MainCategoryList[row])")
                self.typeID = "\((self.AllCategorylist[row] as! NSDictionary)["id"]!)"
                print("CatId",self.typeID!)
                self.nextStart = "0"
                self.MainCategoryList.removeAllObjects()
                self.recordsArray.removeAllObjects()
                self.SubTypeCategorylist.removeAllObjects()
                self.startProductListingData()
                dispatchGroup.notify(queue: .main, execute: {
                    print("Allproductlistingarray",self.allProductListingArray!)
                    print("Subtypecategoryarray",self.SubTypeCategorylist!)
                    if self.MainCategoryList.count == 0
                    {
                        self.MainCategoryOutlet.setTitle("All", for: .normal)
                    }
                    else
                    {
                        self.MainCategoryOutlet.setTitle("\(self.MainCategoryList[0])", for: .normal)
                    }
                    
                    if self.ChildCategoryListing.count == 0
                    {
                        self.SubcategoryOutlet.setTitle("No Subtype Found", for: .normal)
                    }
                    else if self.ChildCategoryListing.count > 0
                    {
                        //self.SubcategoryOutlet.setTitle("\(self.SubTypeCategorylist[0])", for: .normal)
                    }
                    else
                    {
                        //do nothing
                    }
              
                })
                
            }
            else if ShowPIcker == "S"
            {
                self.MainCategoryOutlet.setTitle("\(self.MainCategoryList[row])", for: .normal)
                self.ShowPickerView.isHidden = true

                self.typeID = "\((self.AllCategorylist[row] as! NSDictionary)["id"]!)"
                print("CatId",self.typeID!)
                self.nextStart = "0"
                self.MainCategoryList.removeAllObjects()
                self.recordsArray.removeAllObjects()
                self.SubTypeCategorylist.removeAllObjects()
                self.startProductListingData()
                dispatchGroup.notify(queue: .main, execute: {
                    print("Allproductlistingarray",self.allProductListingArray!)
                    print("Subtypecategoryarray",self.SubTypeCategorylist!)
                    if self.MainCategoryList.count == 0
                    {
                        self.MainCategoryOutlet.setTitle("All", for: .normal)
                    }
                    else
                    {
                        self.MainCategoryOutlet.setTitle("\(self.MainCategoryList[0])", for: .normal)
                    }
                    
                    if self.ChildCategoryListing.count == 0
                    {
                        self.SubcategoryOutlet.setTitle("No Subtype Found", for: .normal)
                    }
                    else if self.ChildCategoryListing.count > 0
                    {
                        self.SubcategoryOutlet.setTitle("\(self.SubTypeCategorylist[0])", for: .normal)
                    }
                    else
                    {
                        //do nothing
                    }
                })
            }
            else if ShowPIcker == "MA"
            {
                self.MainCategoryOutlet.setTitle("\(self.MainCategoryList[row])", for: .normal)
                
                self.ShowPickerView.isHidden = true

                self.typeID = "\((self.AllCategorylist[row] as! NSDictionary)["id"]!)"
                print("CatId",self.typeID!)
                self.nextStart = "0"
                self.MainCategoryList.removeAllObjects()
                self.recordsArray.removeAllObjects()
                self.SubTypeCategorylist.removeAllObjects()
                self.startProductListingData()
                dispatchGroup.notify(queue: .main, execute: {
                    print("Allproductlistingarray",self.allProductListingArray!)
                    print("Subtypecategoryarray",self.SubTypeCategorylist!)
                    if self.MainCategoryList.count == 0
                    {
                        self.MainCategoryOutlet.setTitle("All", for: .normal)
                    }
                    else
                    {
                        self.MainCategoryOutlet.setTitle("\(self.MainCategoryList[0])", for: .normal)
                    }
                    
                    
                    if self.ChildCategoryListing.count == 0
                    {
                        self.SubcategoryOutlet.setTitle("No Subtype Found", for: .normal)
                    }
                    else if self.ChildCategoryListing.count > 0
                    {
                        self.SubcategoryOutlet.setTitle("\(self.SubTypeCategorylist[0])", for: .normal)
                    }
                    else
                    {
                        //do nothing
                    }

                })
            }
            else
            {
                if self.MainCategoryList.count == 0
                {
                    self.MainCategoryOutlet.setTitle("All", for: .normal)
                }
                else
                {
                    self.MainCategoryOutlet.setTitle("\(self.MainCategoryList[0])", for: .normal)
                }
                
                
                self.SubcategoryOutlet.setTitle("\(self.SubTypeCategorylist[row])", for: .normal)
                self.ShowPickerView.isHidden = true
                self.typeID = "\((self.ChildCategoryListing[row] as! NSDictionary)["id"]!)"
                self.MainCategoryList.removeAllObjects()
                self.recordsArray.removeAllObjects()
                self.SubTypeCategorylist.removeAllObjects()
                self.startProductListingData()

            }
        }
        else if pickerView == SortPickerView
        {
            self.sortingType = "\((self.sortDict[row] as NSDictionary)["key"]!)"
            self.SortView.isHidden = true
            self.recordsArray.removeAllObjects()
            self.nextStart = "0"
            self.startProductListingData()
        }
        else
        {
            print("Do Nothing")
        }
    }
}

// MARK:- // Searchbar Delegate Methods

extension MarketPlaceListingViewController: UISearchBarDelegate {

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
        let textFieldInsideSearchBar = searchView.searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.text = ""

    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.ProductSearchView.isHidden = true

    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        let textFieldInsideSearchBar = searchView.searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .red
        if textFieldInsideSearchBar?.text?.count == 0
        {
            print("Do Nothing")
        }
        else
        {
            self.SearchBy = textFieldInsideSearchBar?.text
            self.recordsArray.removeAllObjects()
            self.startProductListingData()
            self.searchView.isHidden = true
        }
    }

}




// MARK:- // Scrollview Delegate MEthods

extension MarketPlaceListingViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.startProductListingData()
            }
        }
    }

}




// MARK:- //Strike Out Text
extension String {
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0,attributeString.length))
        return attributeString
    }
}





// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}

class MarketPlaceListingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var Product_image: UIImageView!
    @IBOutlet weak var watchlistBtnOutlet: UIButton!
    @IBOutlet weak var Product_name: UILabel!
    @IBOutlet weak var Listing_time: UILabel!
    @IBOutlet weak var Expire_time: UILabel!
    
    @IBOutlet weak var ProductConditionImage: UIImageView!
    @IBOutlet weak var Product_StatusView: UIView!
    @IBOutlet weak var Product_SellerView: UIView!
    @IBOutlet weak var ProductSellerName: UILabel!
    @IBOutlet weak var Product_address: UILabel!
    @IBOutlet weak var Product_Price: UILabel!
    @IBOutlet weak var Product_type: UILabel!
    @IBOutlet weak var Product_discounted_price: UILabel!
    @IBOutlet weak var Verified_image: UIImageView!
    
    @IBOutlet weak var ProductCondition: UILabel!
    @IBOutlet weak var WatchlistedBtnOutlet: UIButton!
    
}


class MarketPlaceListingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var BidImage: UIImageView!
    @IBOutlet weak var BidCountValue: UILabel!
    @IBOutlet weak var BidStackView: UIStackView!
    @IBOutlet weak var Product_imageview: UIImageView!
    @IBOutlet weak var Product_name: UILabel!
    @IBOutlet weak var Listing_time: UILabel!
    @IBOutlet weak var Closing_time: UILabel!
    @IBOutlet weak var markView: UIView!
    @IBOutlet weak var Final_price: UILabel!
    @IBOutlet weak var discounted_price: UILabel!
    @IBOutlet weak var Product_type: UILabel!
    @IBOutlet weak var Product_address: UILabel!
    @IBOutlet weak var Cart_btn: UIButton!
    @IBOutlet weak var Watchlist_btn: UIButton!
    
    @IBOutlet weak var ProductConditionImage: UIImageView!
    @IBOutlet weak var ProductCondition: UILabel!
    @IBOutlet weak var watchlistedBtn: UIButton!
    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var Product_SellerView: UIView!
    @IBOutlet weak var Verified_image: UIImageView!
    @IBOutlet weak var Product_sellerName: UILabel!
    @IBOutlet weak var WatchlistImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

