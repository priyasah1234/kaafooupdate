//
//  ItemsLostViewController.swift
//  Kaafoo
//  Created by admin on 19/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class ItemsLostViewController: GlobalViewController,UITableViewDataSource,UITableViewDelegate {

    // Global Font Applied

    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var itemsWonArray : NSMutableArray! = NSMutableArray()

    var recordsArray : NSMutableArray! = NSMutableArray()

    let dispatchGroup = DispatchGroup()



    @IBOutlet weak var itemsLostTableView: UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()

        getItemsLostList()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
    }


    // MARK:- // Tableview Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! ItemsLostTVCTableViewCell

        cell.productTitle.frame = CGRect(x: cell.productImage.frame.origin.x + cell.productImage.frame.size.width + (10/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (170/320)*self.FullWidth, height: (80/568)*self.FullHeight)

        cell.productTitle.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        cell.productTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.productTitle.sizeToFit()
        cell.productType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_type"]!)"
        cell.price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"

        let path = "\((recordsArray[indexPath.row] as! NSDictionary)["product_image"]!)"

        cell.productImage.sd_setImage(with: URL(string: path))

        if "\((recordsArray[indexPath.row] as! NSDictionary)["product_type"]!)".elementsEqual("Auction")
        {
            cell.price.isHidden = false
        }
        else
        {
            cell.price.isHidden = true
        }

        // Set Font //////


        cell.productTitle.font = UIFont(name: cell.productTitle.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        cell.productType.font = UIFont(name: cell.productType.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.price.font = UIFont(name: cell.price.font.fontName, size: CGFloat(Get_fontSize(size: 14)))


        return cell

    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        let cell = cell as! ItemsLostTVCTableViewCell

        cell.cellView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }




    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }


            }
            else
            {
                getItemsLostList()
            }

        }
    }

    //MARK:- //Get Listing using Global API
    func getItemsLostList()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
//        let langID = UserDefaults.standard.string(forKey: "langID")

        let  parameters = "user_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_items_lost", param: parameters, completion: {
            self.itemsWonArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.itemsWonArray.count - 1)

            {

                let tempDict = self.itemsWonArray[i] as! NSDictionary
                print("tempdict is : " , tempDict)

                self.recordsArray.add(tempDict as! NSMutableDictionary)


            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.itemsWonArray.count

            print("Next Start Value : " , self.startValue)


            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.itemsLostTableView.delegate = self
                self.itemsLostTableView.dataSource = self
                self.itemsLostTableView.reloadData()

                SVProgressHUD.dismiss()
            }
        })
    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text =  LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }



}
//MARK:- //Items Lost TableviewCell
class ItemsLostTVCTableViewCell: UITableViewCell {


    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productType: UILabel!
    @IBOutlet weak var price: UILabel!





    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



