//
//  BookingInformationViewController.swift
//  Kaafoo
//
//  Created by admin on 11/10/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class BookingInformationViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {

    // Global Font Applied


    @IBOutlet weak var bookingInformationTableview: UITableView!


    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var recordsArray : NSMutableArray! = NSMutableArray()


    var bookingInformationArray : NSMutableArray!

    var bookingRequestID : String!


    override func viewDidLoad() {
        super.viewDidLoad()

        self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        self.getBookingInformationList()

    }
    
    // MARK:- // Delegate Methods

    // MARK:- // Tableview Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return recordsArray.count

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! BookingInformationTVCTableViewCell

        cell.bookingNumber.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["booking_no"]!)"
        cell.status.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["status_name"]!)"
        cell.productName.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_name"]!)"
        cell.pickUpDate.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["pickup_date"]!)"
        cell.totalPrice.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["total_price"]!)"
        cell.dropOffDate.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["dropoff_date"]!)"

        cell.bookingNumberLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "booking_no", comment: "")
        cell.statusLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "status", comment: "")
        cell.productNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_name", comment: "")
        cell.pickUpDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickup_date", comment: "")
        cell.totalPriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalPrice", comment: "")
        cell.dropOffDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropoff_date", comment: "")

        cell.Delete_Btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "delete", comment: ""), for: .normal)
        cell.Cancel_Btn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: ""), for: .normal)





        cell.Delete_Btn.tag = indexPath.row
        cell.Cancel_Btn.tag = indexPath.row


        cell.Delete_Btn.addTarget(self, action: #selector(deleteBookingTap(sender:)), for: .touchUpInside)

        cell.Cancel_Btn.addTarget(self, action: #selector(cancelBookingTap(sender:)), for: .touchUpInside)


        //Delete and Cancel button checking
        if "\((recordsArray[indexPath.row] as! NSMutableDictionary)["button"]!)".elementsEqual("")
        {
            cell.Cancel_Btn.isHidden = true
            //            cell.BtnsView.isHidden = false
        }
        else
        {
            cell.Cancel_Btn.isHidden = false
        }

        if "\((recordsArray[indexPath.row] as! NSMutableDictionary)["delete_button"]!)".elementsEqual("")
        {
            //            cell.BtnsView.isHidden = false
            cell.Delete_Btn.isHidden = true
        }
        else
        {
            cell.Delete_Btn.isHidden = false
        }

        if "\((recordsArray[indexPath.row] as! NSMutableDictionary)["button"]!)".elementsEqual("") && "\((recordsArray[indexPath.row] as! NSMutableDictionary)["delete_button"]!)".elementsEqual("")
        {
            cell.BtnsView.isHidden = true
        }
        else
        {
            cell.BtnsView.isHidden = false
        }



        //////// Set Font


        cell.bookingNumberLBL.font = UIFont(name: cell.bookingNumberLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingNumber.font = UIFont(name: cell.bookingNumber.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.statusLBL.font = UIFont(name: cell.statusLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.status.font = UIFont(name: cell.status.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.productNameLBL.font = UIFont(name: cell.productNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.productName.font = UIFont(name: cell.productName.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.pickUpDateLBL.font = UIFont(name: cell.pickUpDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.pickUpDate.font = UIFont(name: cell.pickUpDate.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalPriceLBL.font = UIFont(name: cell.totalPriceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalPrice.font = UIFont(name: cell.totalPrice.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.dropOffDateLBL.font = UIFont(name: cell.dropOffDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.dropOffDate.font = UIFont(name: cell.dropOffDate.font.fontName, size: CGFloat(Get_fontSize(size: 13)))

        cell.Cancel_Btn.titleLabel?.font = UIFont(name: (cell.Cancel_Btn.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
        cell.Delete_Btn.titleLabel?.font = UIFont(name: (cell.Delete_Btn.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!






        //        cell.bookingInformationCellView.layer.borderWidth = 2
        //        cell.bookingInformationCellView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor

        //        cell.BtnsView.layer.borderWidth = 2
        //        cell.BtnsView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor


        cell.cellStackview.layer.cornerRadius = 8

        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        return cell

    }

    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //
    //        let cell = cell as! BookingInformationTVCTableViewCell
    //
    //        // Check for Status and set Frames
    //        if "\((recordsArray[indexPath.row] as! NSMutableDictionary)["button"]!)".elementsEqual("cancel")
    //        {
    //           cell.Cancel_Btn.isHidden = false
    //            cell.BtnsView.isHidden = false
    //        }
    //        else
    //        {
    //           cell.Cancel_Btn.isHidden = true
    //        }
    //
    //        if "\((recordsArray[indexPath.row] as! NSMutableDictionary)["delete_button"]!)".elementsEqual("delete")
    //        {
    //            cell.BtnsView.isHidden = false
    //            cell.Delete_Btn.isHidden = false
    //        }
    //        else
    //        {
    //            cell.Delete_Btn.isHidden = true
    //        }
    //
    //    }



    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        bookingRequestID = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["booking_rq_id"]!)"


        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookingInformationDetailVC") as! BookingInformationDetailViewController

        navigate.bookingRequestID = bookingRequestID

        self.navigationController?.pushViewController(navigate, animated: true)

    }


    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {

                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                getBookingInformationList()
            }

        }
    }

    // MARK:- // Delete Booking


    @objc func deleteBookingTap(sender: UIButton)
    {

        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in


            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary

            self.bookingRequestID = "\(tempDictionary["booking_rq_id"]!)"

            self.deleteBooking()

            self.globalDispatchgroup.notify(queue: .main, execute: {

                self.recordsArray.removeAllObjects()

                self.startValue = 0

                self.getBookingInformationList()

            })




        }

        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alert.addAction(ok)
        alert.addAction(cancel)

        self.present(alert, animated: true, completion: nil )


    }
    //MARK:- //Delete Booking Using Global API
    func deleteBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&booking_rq_id=\(bookingRequestID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_booking_delete", param: parameters, completion: {
            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()
            }
        })
    }

    //MARK:- //Cancel Booking Using Global API
    func cancelBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&booking_rq_id=\(bookingRequestID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_booking_cancle", param: parameters, completion: {
            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()
            }
        })
    }

    //MARK:- //Get Listing using Global API
    func getBookingInformationList()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&start_value=\(startValue)&per_load=\(perLoad)&lang_id=\(langID!)"

        self.CallAPI(urlString: "app_booking_info_list", param: parameters, completion: {
            self.bookingInformationArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.bookingInformationArray.count-1)

            {

                let tempDict = self.bookingInformationArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)

            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.bookingInformationArray.count

            print("Next Start Value : " , self.startValue)

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.bookingInformationTableview.delegate = self
                self.bookingInformationTableview.dataSource = self
                self.bookingInformationTableview.reloadData()
                self.bookingInformationTableview.rowHeight = UITableView.automaticDimension
                self.bookingInformationTableview.estimatedRowHeight = UITableView.automaticDimension

                SVProgressHUD.dismiss()
            }
        })
    }
    // MARK:- // Cancel Booking
    @objc func cancelBookingTap(sender: UIButton)
    {

        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in


            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary

            self.bookingRequestID = "\(tempDictionary["booking_rq_id"]!)"

            self.cancelBooking()

            self.globalDispatchgroup.notify(queue: .main, execute: {

                self.recordsArray.removeAllObjects()

                self.startValue = 0

                self.getBookingInformationList()

            })
        }

        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alert.addAction(ok)
        alert.addAction(cancel)

        self.present(alert, animated: true, completion: nil )


    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }


}
//MARK:- //Booking Information Tableviewcell
class BookingInformationTVCTableViewCell: UITableViewCell {
    @IBOutlet weak var bookingInformationCellView: UIView!
    @IBOutlet weak var cellStackview: UIStackView!
    @IBOutlet weak var bookingNumberLBL: UILabel!
    @IBOutlet weak var bookingNumber: UILabel!
    @IBOutlet weak var statusLBL: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var productNameLBL: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var pickUpDateLBL: UILabel!
    @IBOutlet weak var pickUpDate: UILabel!
    @IBOutlet weak var totalPriceLBL: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var dropOffDateLBL: UILabel!
    @IBOutlet weak var dropOffDate: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var BtnsView: UIView!
    @IBOutlet weak var Delete_Btn: UIButton!
    @IBOutlet weak var Cancel_Btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
