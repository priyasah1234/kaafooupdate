//
//  PastOrderViewController.swift
//  Kaafoo
//
//  Created by admin on 11/10/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class PastOrderViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    // Global Font Applied
    

    @IBOutlet weak var pastOrderTableview: UITableView!
    
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    let dispatchGroup = DispatchGroup()
    
    var pastOrderArray : NSMutableArray!
    
    var orderID : String!
    
    var CateGoryID : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        set_font()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        getPastOrderList()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.pastOrderTableview.rowHeight = UITableView.automaticDimension
        self.pastOrderTableview.estimatedRowHeight = UITableView.automaticDimension
    }
    
    
    
    // MARK:- // Delegate Methods
    
    // MARK:- // TableviewDelegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! PastOrderTVCTableViewCell
        
        cell.orderNo.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["order_no"]!)"
        cell.date.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["order_date"]!)"
        
        cell.totalItem.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["total_item"]!)"
        cell.totalPrice.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["currency"]!)" + "\((recordsArray[indexPath.row] as! NSMutableDictionary)["total_price"]!)"
        
        // checking if a string is an URL
        let text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["seller_name"]!)"
        let types: NSTextCheckingResult.CheckingType = .link
        var URLStrings = [NSURL]()
        let detector = try? NSDataDetector(types: types.rawValue)
        let matches = detector!.matches(in: text, options: .reportCompletion, range: NSMakeRange(0, text.count))
        
        for match in matches {
            print(match.url!)
            URLStrings.append(match.url! as NSURL)
        }
        print(URLStrings)
        
        
        
        if URLStrings.count == 0
        {
            cell.sellerName.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["seller_name"]!)"
            cell.sellerName.isHidden = false
            cell.sellerImage.isHidden = true
            
        }
        else
        {
            
            cell.sellerImage.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSMutableDictionary)["seller_name"]!)"))
            cell.sellerName.isHidden = true
            cell.sellerImage.isHidden = false
        }
        
        
        
        
        /////////
        //setfont
        
        cell.orderNoLBL.font = UIFont(name: cell.orderNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.orderNo.font = UIFont(name: cell.orderNo.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.sellerNameLBL.font = UIFont(name: cell.sellerNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.sellerName.font = UIFont(name: cell.sellerName.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalItemLBL.font = UIFont(name: cell.totalItemLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalItem.font = UIFont(name: cell.totalItem.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalPriceLBL.font = UIFont(name: cell.totalPriceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalPrice.font = UIFont(name: cell.totalPrice.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
        
        /////////
        
        cell.orderNoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "order_no", comment: "")
        cell.dateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
        cell.sellerNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "seller_name", comment: "")
        cell.totalItemLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalItem", comment: "")
        cell.totalPriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalPrice", comment: "")
        
        
        
        /////////
        
//        cell.cellView.layer.borderWidth = 2
//        cell.cellView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        
        cell.cellView.layer.cornerRadius = 8
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return (120/568)*self.FullHeight
//
//    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        orderID = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["order_id"]!)"
        
        //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
        
//        let navigate = UIStoryboard(name: storyboardName!, bundle: nil).instantiateViewController(withIdentifier: "orderDetailVC") as! OrderDetailViewController
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "orderDetailVC") as! OrderDetailViewController
        
        navigate.orderID = orderID
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK: - // Scrollview Delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            
//            print("next start : ",nextStart )
            
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                getPastOrderList()
            }
            
        }
    }
    

    //MARK:- //Get Listing Using Global API
    func getPastOrderList()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)&cat_id=\(CateGoryID!)"
        self.CallAPI(urlString: "app_order_information", param: parameters, completion:
            {
                self.pastOrderArray = self.globalJson["info_array"] as? NSMutableArray

                for i in 0...(self.pastOrderArray.count-1)

                {

                    let tempDict = self.pastOrderArray[i] as! NSDictionary

                    self.recordsArray.add(tempDict as! NSMutableDictionary)

                }

                self.nextStart = "\(self.globalJson["next_start"]!)"

                self.startValue = self.startValue + self.pastOrderArray.count

                print("Next Start Value : " , self.startValue)

                DispatchQueue.main.async {

                    self.globalDispatchgroup.leave()

                    self.pastOrderTableview.delegate = self
                    self.pastOrderTableview.dataSource = self
                    self.pastOrderTableview.reloadData()

                    SVProgressHUD.dismiss()
                }
        })
    }
    
    
    // MARK:- // Set Font
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }
    
}
//MARK:- Past Order Tableviewcell
class PastOrderTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var orderNoLBL: UILabel!
    @IBOutlet weak var orderNo: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var sellerNameLBL: UILabel!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var sellerImage: UIImageView!
    @IBOutlet weak var totalItemLBL: UILabel!
    @IBOutlet weak var totalItem: UILabel!
    @IBOutlet weak var totalPriceLBL: UILabel!
    @IBOutlet weak var totalPrice: UILabel!


    @IBOutlet weak var separatorView: UIView!








    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

//struct Hello : Codable
//{
//    var next_start : String
//    var info_array : NSMutableArray
//}

