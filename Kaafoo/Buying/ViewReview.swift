//
//  ViewReview.swift
//  Kaafoo
//
//  Created by esolz on 14/08/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class ViewReview : UIView
{
    @IBOutlet var ContentView: UIView!
    @IBOutlet weak var DescriptionTxt: UILabel!
    @IBOutlet weak var dateTimeTxt: UILabel!
    @IBOutlet weak var rating: CosmosView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("ViewReview", owner: self, options: nil)
        addSubview(ContentView)
        
        ContentView.frame = self.bounds
        ContentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        
    }
    
}
