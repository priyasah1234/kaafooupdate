//
//  MyFavouritesViewController.swift
//  Kaafoo
//
//  Created by admin on 18/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import Cosmos

class MyFavouritesViewController: GlobalViewController {

    // Global Font Applied
    @IBOutlet weak var myFavouritesTableView: UITableView!
    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var recordsArray : NSMutableArray! = NSMutableArray()

    let dispatchGroup = DispatchGroup()

    var myFavouritesArray : NSMutableArray!

    var ID : String!
    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.set_font()
        getFavouritesData()
        self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
    }

    // MARK:- // View Did Appear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.myFavouritesTableView.estimatedRowHeight = 500
        self.myFavouritesTableView.rowHeight = UITableView.automaticDimension
    }
    //MARK:- //JSON Post Method to get My Favourites List

    func getFavouritesData()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let parameters = "user_id=\(userID!)&per_load=\(10)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_my_favourite", param: parameters, completion: {

            self.myFavouritesArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.myFavouritesArray.count-1)

            {
                let tempDict = self.myFavouritesArray[i] as! NSDictionary
                print("tempdict is : " , tempDict)
                self.recordsArray.add(tempDict as! NSMutableDictionary)
            }
            self.nextStart = "\(self.globalJson["next_start"]!)"
            self.startValue = self.startValue + self.myFavouritesArray.count
            print("Next Start Value : " , self.startValue)
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.myFavouritesTableView.delegate = self
                self.myFavouritesTableView.dataSource = self
                self.myFavouritesTableView.reloadData()
                SVProgressHUD.dismiss()
            }
        })
    }
    // MARK:- // JSON Post Method to Remove from Favourites

    func removeFromFavourites() {

        let userID = UserDefaults.standard.string(forKey: "userID")
//        var langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(self.langID!)&remove_id=\(ID!)"

        self.CallAPI(urlString: "app_remove_my_favourite", param: parameters) {

            if self.globalJson["response"] as! Bool == true {

                DispatchQueue.main.async {
                    self.globalDispatchgroup.leave()

                    SVProgressHUD.dismiss()
                }

            }
            else {
                DispatchQueue.main.async {
                    self.globalDispatchgroup.leave()
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    // MARK:- // View Profile

    @objc func viewProfile(sender: UIButton) {

        if "\((self.recordsArray[sender.tag] as! NSDictionary)["user_type"]!)".elementsEqual("B") {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController

            print("Seller ID --- ", (self.recordsArray[sender.tag] as! NSDictionary)["id"] ?? "")

            navigate.sellerID = "\((self.recordsArray[sender.tag] as! NSDictionary)["id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "privateUserProfileVC") as! PrivateUserProfileViewController

            print("Seller ID --- ", (self.recordsArray[sender.tag] as! NSDictionary)["id"] ?? "")

            navigate.sellerID = "\((self.recordsArray[sender.tag] as! NSDictionary)["id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
    // MARK:- // Set Font

    func set_font()
    {

        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
    // MARK:- // Remove from My Favourites
    @objc func remove(sender: UIButton)
    {

        self.customAlert(title: "Confirm", message: "Are you sure you want to remove this from Favourites?") {

            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary

            self.ID = "\(tempDictionary["id"]!)"

            self.removeFromFavourites()

            self.recordsArray.removeObject(at: sender.tag)

            self.myFavouritesTableView.reloadData()
        }
    }
}



// MARK:- // Tableview Delegates

extension MyFavouritesViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! MyFavouritesTVCTableViewCell

        cell.title.text = "\((recordsArray[indexPath.row] as! NSDictionary)["buss_name"] ?? "")"
        cell.addressLineOne.text = "\((recordsArray[indexPath.row] as! NSDictionary)["address"] ?? "")"
        cell.reviewCount.text = "\((recordsArray[indexPath.row] as! NSDictionary)["rating"] ?? "")"



        cell.starView.rating = Double("\((recordsArray[indexPath.row] as! NSDictionary)["rating"] ?? "")")!

        cell.starView.settings.filledImage = UIImage(named: "Shape")

        cell.cellImage.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["logo_image"] ?? "")"))

        // Set Font //////

        cell.title.font = UIFont(name: cell.title.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.addressLineOne.font = UIFont(name: cell.addressLineOne.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
        cell.viewProfile.titleLabel?.font = UIFont(name: (cell.viewProfile.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        cell.reviewCount.font = UIFont(name: cell.reviewCount.font.fontName, size: CGFloat(Get_fontSize(size: 10)))

        /////////
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        cell.cellView.layer.cornerRadius = 8

        cell.viewProfile.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "view_profile", comment: ""), for: .normal)

        cell.viewProfile.tag = indexPath.row
        cell.deleteButton.tag = indexPath.row

        cell.viewProfile.addTarget(self, action: #selector(MyFavouritesViewController.viewProfile(sender:)), for: .touchUpInside)

        cell.deleteButton.addTarget(self, action: #selector(MyFavouritesViewController.remove(sender:)), for: .touchUpInside)
        return cell

    }

}


// MARK:- // Scrollview Delegates

extension MyFavouritesViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {
//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                getFavouritesData()
            }
        }
    }
}
//MARK:- My Favourites TableviewCell
class MyFavouritesTVCTableViewCell: UITableViewCell {
    @IBOutlet weak var starView: CosmosView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var addressLineOne: UILabel!
    @IBOutlet weak var addressLineTwo: UILabel!
    @IBOutlet weak var viewProfileLbl: UILabel!
    @IBOutlet weak var viewProfile: UIButton!
    @IBOutlet weak var reviewCount: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}





