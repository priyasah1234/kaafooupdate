//
//  BookingInformationDetailViewController.swift
//  Kaafoo
//
//  Created by admin on 12/10/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class BookingInformationDetailViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    

    // Global Font Applied
    
    @IBOutlet weak var mainScroll: UIScrollView!
    
    @IBOutlet weak var totalAmountView: UIView!
    @IBOutlet weak var totalAmountLBL: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    
    @IBOutlet weak var bookingNumberAndDateView: UIView!
    @IBOutlet weak var bookingNumberLBL: UILabel!
    @IBOutlet weak var bookingNumber: UILabel!
    @IBOutlet weak var bookingDateLBL: UILabel!
    @IBOutlet weak var bookingDate: UILabel!
    
    
    @IBOutlet weak var personalDetailsView: UIView!
    @IBOutlet weak var personalInfoLBL: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var contactNumber: UILabel!
    @IBOutlet weak var commentLBL: UILabel!
    @IBOutlet weak var comment: UILabel!
    
    
    @IBOutlet weak var pickupAndDropoffLocationView: UIView!
    @IBOutlet weak var pickupLocationTitleView: UIView!
    @IBOutlet weak var pickupLocationTitleLBL: UILabel!
    @IBOutlet weak var pickupLocationView: UIView!
    @IBOutlet weak var pickupLocation: UILabel!
    @IBOutlet weak var dropoffLocationTitleView: UIView!
    @IBOutlet weak var dropoffLocationTitleLBL: UILabel!
    @IBOutlet weak var dropoffLocationView: UIView!
    @IBOutlet weak var dropoffLocation: UILabel!
    
    
    @IBOutlet weak var pickupAndBookingDateView: UIView!
    @IBOutlet weak var pickupDateLBL: UILabel!
    @IBOutlet weak var pickupDate: UILabel!
    @IBOutlet weak var dropoffDateLBL: UILabel!
    @IBOutlet weak var dropoffDate: UILabel!
    
    
    @IBOutlet weak var totalDaysAndSecurityAmountView: UIView!
    @IBOutlet weak var totalDaysLBL: UILabel!
    @IBOutlet weak var totalDays: UILabel!
    @IBOutlet weak var securityAmountLBL: UILabel!
    @IBOutlet weak var securityAmount: UILabel!
    
    
    
    @IBOutlet weak var productDetailsView: UIView!
    @IBOutlet weak var productDetailsTitleLBL: UILabel!
    @IBOutlet weak var productNameLBL: UILabel!
    @IBOutlet weak var priceLBL: UILabel!
    @IBOutlet weak var productNameAndQuantity: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productDetailsTotalDays: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    
    
    @IBOutlet weak var extraServicesView: UIView!
    @IBOutlet weak var extraServicesTitleLBL: UILabel!
    @IBOutlet weak var extraServicesTableview: UITableView!
    
    
    
    
    var bookingRequestID : String!
    let dispatchGroup = DispatchGroup()
    
    var bookingInformationDetailDictionary : NSDictionary!
    
    var response : Bool!
    
    @IBOutlet weak var ContentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        getBookingInformationDetail()
        
        dispatchGroup.notify(queue: .main) {
            self.mainScroll.isHidden = true
            self.totalAmountView.isHidden = true
            
            self.set_font()
            
            if self.response == true
            {
               self.mainScroll.isHidden = false
               self.totalAmountView.isHidden = false
               self.putData()
            }
            else
            {
                self.mainScroll.isHidden = true
                self.totalAmountView.isHidden = true
                
                let noDataLBL = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: self.FullWidth - (40/320)*self.FullWidth, height: (100/568)*self.FullHeight))
                
                noDataLBL.textAlignment = .center
                noDataLBL.center = self.view.center
                noDataLBL.numberOfLines = 0
                
                noDataLBL.text = "No Booking Information Available for Selected Product."
                
                self.view.addSubview(noDataLBL)
            }
            
        }

        
    }
    
    
    // MARK:- // Delegate MEthods
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (bookingInformationDetailDictionary["extra"] as! NSArray).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! BiExtraFeaturesTVCTableViewCell
        
        cell.productNameAndQTY.text = "\(((bookingInformationDetailDictionary["extra"] as! NSArray)[indexPath.row] as! NSDictionary)["product_name"]!)" + "(Qty" + "\(((bookingInformationDetailDictionary["extra"] as! NSArray)[indexPath.row] as! NSDictionary)["quantity"]!)" + ")"
        
        cell.price.text = "\(((bookingInformationDetailDictionary["extra"] as! NSArray)[indexPath.row] as! NSDictionary)["per_price"]!)"
        cell.totalDays.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalDays", comment: "") + "   " + "\(((bookingInformationDetailDictionary["extra"] as! NSArray)[indexPath.row] as! NSDictionary)["total_days"]!)"
        cell.subTotal.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "sub_total", comment: "") + "   " + "\(((bookingInformationDetailDictionary["extra"] as! NSArray)[indexPath.row] as! NSDictionary)["sub_total"]!)"
        
        // localization
        cell.productNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_name", comment: "")
        cell.priceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "price", comment: "")
        
        // Set font
        cell.productNameLBL.font = UIFont(name: cell.productNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.productNameAndQTY.font = UIFont(name: cell.productNameAndQTY.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.priceLBL.font = UIFont(name: cell.priceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.price.font = UIFont(name: cell.price.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.totalDays.font = UIFont(name: cell.totalDays.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.subTotal.font = UIFont(name: cell.subTotal.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    //MARK:- //Fetch Details Using Global API
    func getBookingInformationDetail()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&booking_rq_id=\(bookingRequestID!)"
        self.CallAPI(urlString: "app_booking_info_details", param: parameters, completion: {
            self.bookingInformationDetailDictionary = self.globalJson["info_array"] as? NSDictionary

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.extraServicesTableview.delegate = self
                self.extraServicesTableview.dataSource = self
                self.extraServicesTableview.reloadData()
                self.extraServicesTableview.rowHeight = UITableView.automaticDimension
                self.extraServicesTableview.estimatedRowHeight = UITableView.automaticDimension

                SVProgressHUD.dismiss()


            }
        })
    }
    
    
    
    // MARK:- // Put All Required Data fetched from Web
    
    func putData()
    {
        
        bookingNumber.text = "\(bookingInformationDetailDictionary["booking_no"] ?? "")"
        bookingDate.text = "\(bookingInformationDetailDictionary["booking_date"] ?? "")"
        name.text = "\(bookingInformationDetailDictionary["name"] ?? "")"
        email.text = "\(bookingInformationDetailDictionary["email"] ?? "")"
        contactNumber.text = "\(bookingInformationDetailDictionary["phone_no"] ?? "")"
        comment.text = "\(bookingInformationDetailDictionary["comment"] ?? "")"
        pickupLocation.text = "\(bookingInformationDetailDictionary["pick_up_loc"] ?? "")"
        dropoffLocation.text = "\(bookingInformationDetailDictionary["drop_of_loc"] ?? "")"
        pickupDate.text = "\(bookingInformationDetailDictionary["pickup_date"] ?? "")"
        dropoffDate.text = "\(bookingInformationDetailDictionary["drop_of"] ?? "")"
        totalDays.text = "\(bookingInformationDetailDictionary["total_days_booking"] ?? "")"
        securityAmount.text = "\(bookingInformationDetailDictionary["security_amount"] ?? "")"
        
        productNameAndQuantity.text = "\(((bookingInformationDetailDictionary["product_details"] as! NSArray)[0] as! NSDictionary)["product_name"] ?? "")" + "(Qty" + "\(((bookingInformationDetailDictionary["product_details"] as! NSArray)[0] as! NSDictionary)["quantity"] ?? "")" + ")"
        
        price.text = "\(((bookingInformationDetailDictionary["product_details"] as! NSArray)[0] as! NSDictionary)["per_price"] ?? "")"
        productDetailsTotalDays.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalDays", comment: "") + "   " + "\(((bookingInformationDetailDictionary["product_details"] as! NSArray)[0] as! NSDictionary)["total_days"] ?? "")"
        subTotal.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "sub_total", comment: "") + "   " + "\(((bookingInformationDetailDictionary["product_details"] as! NSArray)[0] as! NSDictionary)["sub_total"] ?? "")"
        
        
        
        
        totalAmount.text = "\(bookingInformationDetailDictionary["total_price"]!)"
        
        
        bookingNumberLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "booking_no", comment: "")
        bookingDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingDate", comment: "")
        personalInfoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "personal_info", comment: "")
        commentLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "comment", comment: "")
        pickupLocationTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupLocation", comment: "")
        dropoffLocationTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropOffLocation", comment: "")
        pickupDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickup_date", comment: "")
        dropoffDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropoff_date", comment: "")
        totalDaysLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalDays", comment: "")
        securityAmountLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "securityAmount", comment: "")
        productDetailsTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_details", comment: "")
        productNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_name", comment: "")
        priceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "price", comment: "")
        extraServicesTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "extraServices", comment: "")
        totalAmountLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalAmount", comment: "")
        
        
        // set scroll contentsize
        
//        self.extraServicesView.autoresizesSubviews = false
//        self.extraServicesTableview.autoresizesSubviews = false
//
//        self.extraServicesTableview.frame.size.height = CGFloat((bookingInformationDetailDictionary["extra"] as! NSArray).count) * ((75/568)*self.FullHeight)
//
//        self.extraServicesView.frame.size.height = self.extraServicesTableview.frame.origin.y + self.extraServicesTableview.frame.size.height
//
//        self.extraServicesView.autoresizesSubviews = true
//        self.extraServicesTableview.autoresizesSubviews = true
//
//        self.mainScroll.contentSize = CGSize(width: self.FullWidth, height: self.extraServicesView.frame.origin.y + self.extraServicesView.frame.size.height)
        
        
        
    }
    
    // MARK:- // Set Font
    
    func set_font()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        bookingNumberLBL.font = UIFont(name: bookingNumberLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        bookingNumber.font = UIFont(name: bookingNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        bookingDateLBL.font = UIFont(name: bookingDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        bookingDate.font = UIFont(name: bookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        personalInfoLBL.font = UIFont(name: personalInfoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        name.font = UIFont(name: name.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        email.font = UIFont(name: email.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        contactNumber.font = UIFont(name: contactNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        commentLBL.font = UIFont(name: commentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        comment.font = UIFont(name: comment.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        pickupLocationTitleLBL.font = UIFont(name: pickupLocationTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        pickupLocation.font = UIFont(name: pickupLocation.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        dropoffLocationTitleLBL.font = UIFont(name: dropoffLocationTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        dropoffLocation.font = UIFont(name: dropoffLocation.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        pickupDateLBL.font = UIFont(name: pickupDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        pickupDate.font = UIFont(name: pickupDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        dropoffDateLBL.font = UIFont(name: dropoffDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        dropoffDate.font = UIFont(name: dropoffDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        totalDaysLBL.font = UIFont(name: totalDaysLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        totalDays.font = UIFont(name: totalDays.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        securityAmountLBL.font = UIFont(name: securityAmountLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        securityAmount.font = UIFont(name: securityAmount.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        productDetailsTitleLBL.font = UIFont(name: productDetailsTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        productNameLBL.font = UIFont(name: productNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        productNameAndQuantity.font = UIFont(name: productNameAndQuantity.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        priceLBL.font = UIFont(name: priceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        price.font = UIFont(name: price.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        productDetailsTotalDays.font = UIFont(name: productDetailsTotalDays.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        subTotal.font = UIFont(name: subTotal.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        extraServicesTitleLBL.font = UIFont(name: extraServicesTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        totalAmountLBL.font = UIFont(name: totalAmountLBL.font.fontName, size: CGFloat(Get_fontSize(size: 20)))
        totalAmount.font = UIFont(name: totalAmount.font.fontName, size: CGFloat(Get_fontSize(size: 20)))
        
    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }

    
}
//MARK:- //Booking Information Extra Features Tableviewcell
class BiExtraFeaturesTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productNameLBL: UILabel!
    @IBOutlet weak var priceLBL: UILabel!
    @IBOutlet weak var productNameAndQTY: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var totalDays: UILabel!
    @IBOutlet weak var subTotal: UILabel!




    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

