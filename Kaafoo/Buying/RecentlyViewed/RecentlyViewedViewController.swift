//
//  RecentlyViewedViewController.swift
//  Kaafoo
//
//  Created by admin on 18/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class RecentlyViewedViewController: GlobalViewController {

    // Global Font Applied

    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var recordsArray : NSMutableArray! = NSMutableArray()

    let dispatchGroup = DispatchGroup()

    var recentlyViewedArray : NSMutableArray!

    var recentlyViewedProductID : String!


    @IBOutlet weak var recentlyViewedTableView: UITableView!



    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()

        set_font()

        getRecentlyViewedList()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

    }


    // MARK:- // View Will Appear

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.recentlyViewedTableView.estimatedRowHeight = 300
        self.recentlyViewedTableView.rowHeight = UITableView.automaticDimension

    }









    //    @objc func remove(sender: UIButton)
    //    {
    //
    //
    //        print("records array " , recordsArray)
    //
    //        let tempDictionary = recordsArray[sender.tag] as! NSDictionary
    //
    //        watchListProductID = "\(tempDictionary["product_id"]!)"
    //
    //        //removeFromWatchList()
    //
    //        recordsArray.removeObject(at: sender.tag)
    //
    //        print("records array " , recordsArray)
    //
    //        print("sender.tag" , sender.tag)
    //
    //        recentlyViewedTableView.reloadData()
    //
    ////        dataArray.remove(at: sender.tag)
    ////        recentlyViewedTableView.reloadData()
    ////        print("dataArray is : ", dataArray)
    ////
    //    }



    // MARK:- // Set FOnt


    func set_font()
    {

        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }



    //MARK:- // JSON Post Method to get Recently Viewed Listing

    func getRecentlyViewedList()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)"

        self.CallAPI(urlString: "app_recently_view", param: parameters, completion:
            {
                self.recentlyViewedArray = self.globalJson["info_array"] as? NSMutableArray

                for i in 0...(self.recentlyViewedArray.count-1)

                {
                    let tempDict = self.recentlyViewedArray[i] as! NSDictionary

                    self.recordsArray.add(tempDict as! NSMutableDictionary)
                }

                self.nextStart = "\(self.globalJson["next_start"]!)"

                self.startValue = self.startValue + self.recentlyViewedArray.count

                print("Next Start Value : " , self.startValue)

                DispatchQueue.main.async {

                    self.globalDispatchgroup.leave()

                    self.recentlyViewedTableView.delegate = self
                    self.recentlyViewedTableView.dataSource = self
                    self.recentlyViewedTableView.reloadData()

                    SVProgressHUD.dismiss()
                }
        })
    }





}





// MARK:- // Tableview Delegates

extension RecentlyViewedViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! RecentlyViewedTVCTableViewCell

        cell.title.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"] ?? "")"

        cell.listingTime.text = "\((recordsArray[indexPath.row] as! NSDictionary)["listed_time"] ?? "")" + " " + "ago"
        cell.closingTime.text = "\((recordsArray[indexPath.row] as! NSDictionary)["closed_time"] ?? "")"
        cell.addressLbl.text = "\((recordsArray[indexPath.row] as! NSDictionary)["address"] ?? "")"

        cell.businessTitle.text = "\((recordsArray[indexPath.row] as! NSDictionary)["seller_type"] ?? "") :"

        cell.businessName.text = "\((recordsArray[indexPath.row] as! NSDictionary)["business_name"] ?? "")"

        if ("\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_type_status"]!)").elementsEqual("B") {

            cell.productType.text = "Buy Now" //"\((recordsArray[indexPath.row] as! NSDictionary)["product_type"] ?? "")"


            if "\((recordsArray[indexPath.row] as! NSMutableDictionary)["special_offer"]!)".elementsEqual("0") {

                cell.standardPrice.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["currency"]!)" + "\((recordsArray[indexPath.row] as! NSMutableDictionary)["start_price"]!)"

                cell.standardPrice.textColor = UIColor.seaGreen()

                cell.standardPrice.isHidden = false
                cell.reservedPrice.isHidden = true
                cell.bidView.isHidden = true

            }
            else {

                cell.standardPrice.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["currency"] ?? "")" + "\((recordsArray[indexPath.row] as! NSMutableDictionary)["reserve_price"] ?? "")"

                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: ("\((recordsArray[indexPath.row] as! NSMutableDictionary)["currency"] ?? "")") + "\((recordsArray[indexPath.row] as! NSMutableDictionary)["start_price"] ?? "")")

                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))

                cell.reservedPrice.attributedText = attributeString

                cell.standardPrice.textColor = UIColor.red()
                cell.reservedPrice.textColor = UIColor.seaGreen()

                cell.standardPrice.isHidden = false
                cell.reservedPrice.isHidden = false
                cell.bidView.isHidden = true

            }

        }
        else if ("\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_type_status"]!)").elementsEqual("A") {

            cell.productType.text = "Auction"//"\((recordsArray[indexPath.row] as! NSDictionary)["product_type"] ?? "")"

            cell.standardPrice.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["currency"] ?? "")" + "\((recordsArray[indexPath.row] as! NSMutableDictionary)["start_price"] ?? "")"

            cell.bidFlag.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSMutableDictionary)["flag"]!)"))
            cell.bidCount.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["count_bid"] ?? "")"

            cell.standardPrice.textColor = UIColor.seaGreen()

            cell.standardPrice.isHidden = false
            cell.reservedPrice.isHidden = true
            cell.bidView.isHidden = false

        }
        else {

            cell.productType.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_type"] ?? "")"

            cell.standardPrice.isHidden = true
            cell.reservedPrice.isHidden = true
            cell.bidView.isHidden = true

        }


        let path = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_image"] ?? "")"

        cell.productImage.sd_setImage(with: URL(string: path))

        cell.crossButton.tag = indexPath.row

        cell.crossButton.addTarget(self, action: #selector(WatchListViewController.remove(sender:)), for: .touchUpInside)


        if "\((recordsArray[indexPath.row] as! NSMutableDictionary)["address"] ?? "")".isEmpty
        {
            cell.addressPin.isHidden = true
        }
        else
        {
            cell.addressPin.isHidden = false
        }


        // Set Font //////

        cell.title.font = UIFont(name: cell.title.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        cell.listingTime.font = UIFont(name: cell.listingTime.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.closingTime.font = UIFont(name: cell.closingTime.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.addressLbl.font = UIFont(name: cell.addressLbl.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.businessTitle.font = UIFont(name: cell.businessTitle.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.businessName.font = UIFont(name: cell.businessName.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.productType.font = UIFont(name: cell.productType.font.fontName, size: CGFloat(Get_fontSize(size: 9)))
        cell.standardPrice.font = UIFont(name: cell.standardPrice.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
        cell.reservedPrice.font = UIFont(name: cell.reservedPrice.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
        cell.bidCount.font = UIFont(name: cell.bidCount.font.fontName, size: CGFloat(Get_fontSize(size: 12)))


        /////////
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        cell.cellView.layer.cornerRadius = 8

        return cell
    }

}




// MARK: - // Scrollview Delegates

extension RecentlyViewedViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {
//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                getRecentlyViewedList()
            }
        }
    }
}
//MARK:- //Recently Viewed Tableviewcell
class RecentlyViewedTVCTableViewCell: UITableViewCell {


    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var listingTime: UILabel!
    @IBOutlet weak var closingTime: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var addressPin: UIImageView!
    @IBOutlet weak var businessTitle: UILabel!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var productType: UILabel!
    @IBOutlet weak var standardPrice: UILabel!
    @IBOutlet weak var reservedPrice: UILabel!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var bidCount: UILabel!
    @IBOutlet weak var bidFlag: UIImageView!
    @IBOutlet weak var bidView: UIView!




    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



