//
//  ServiceInformationViewController.swift
//  Kaafoo
//
//  Created by admin on 12/10/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import Cosmos


class ServiceInformationViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {

    // Global Font Applied
    
    @IBOutlet weak var serviceInformationTableview: UITableView!
    @IBOutlet weak var reviewMainView: UIView!
    @IBOutlet weak var reviewWindowView: UIView!
    @IBOutlet weak var reviewTitleLBL: UILabel!
    @IBOutlet weak var crossImageview: UIImageView!
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var ratingLBL: UILabel!
    @IBOutlet weak var commentLBL: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentTXTView: UITextView!
    @IBOutlet weak var saveButtonOutlet: UIButton!
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    let dispatchGroup = DispatchGroup()
    
    var serviceInformationArray : NSMutableArray!
    
    var serviceRequestID : String!
    
    var reviewType : String!
    
    var reviewRating : String! = "0"
    
    var orderID : String!
    
    var sellerID : String!
    
    var productID : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let currentDate = Date.getCurrentDate()
        
        print("currentDate",currentDate)
        
        let currentTime = Date.getCurrentTime()
        
        print("CurrentTime",currentTime)


        self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        getServiceInformationList()
        
        setupReviewMainView()
        
        self.commentView.layer.borderWidth = 2
        self.commentView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        self.commentView.layer.cornerRadius = (5/568)*self.FullHeight
        
        rating.didTouchCosmos = didTouchCosmos
        rating.didFinishTouchingCosmos = didFinishTouchingCosmos
        
        
        
    }
    
    // MARK:- // Delegate Methods
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! serviceInformationTVCTableViewCell
        
        
        // Put Data
        cell.bookingNumber.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["booking_no"]!)"
        cell.status.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["status_name"]!)"
        cell.serviceName.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_name"]!)"
        cell.bookingDate.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["booking_date"]!)"
        cell.bookingTime.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["booking_time"]!)"
        cell.servicePrice.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["service-price"]!)"
        
        
        // Localization
        cell.bookingNumberLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "booking_no", comment: "")
        cell.statusLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "status", comment: "")
        cell.serviceNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "serviceName", comment: "")
        cell.servicePriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "servicePrice", comment: "")
        cell.bookingTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingTime", comment: "")
        cell.bookingDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingDate", comment: "")
        
        cell.deleteServiceButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "delete", comment: ""), for: .normal)
        cell.CancelBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: ""), for: .normal)
        cell.userReviewBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_user_review", comment: ""), for: .normal)
        cell.ServiceReviewBtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_product_review", comment: ""), for: .normal)
        
        
        

        
       cell.ServiceReviewBtn.tag = indexPath.row
        
       cell.userReviewBtn.tag = indexPath.row
        
       cell.CancelBtn.tag = indexPath.row
        
       cell.deleteServiceButton.tag = indexPath.row
        
        cell.CancelBtn.addTarget(self, action: #selector(ServiceInformationViewController.cancelServiceTap(sender:)), for: .touchUpInside)
        
        cell.deleteServiceButton.tag = indexPath.row
        
        cell.deleteServiceButton.addTarget(self, action: #selector(ServiceInformationViewController.deleteServiceTap(sender:)), for: .touchUpInside)
        
        // Set Font
        
        cell.bookingNumberLBL.font = UIFont(name: cell.bookingNumberLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingNumber.font = UIFont(name: cell.bookingNumber.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.statusLBL.font = UIFont(name: cell.status.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.serviceNameLBL.font = UIFont(name: cell.serviceNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.serviceName.font = UIFont(name: cell.serviceName.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingDateLBL.font = UIFont(name: cell.bookingDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingDate.font = UIFont(name: cell.bookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingTimeLBL.font = UIFont(name: cell.bookingTimeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingTime.font = UIFont(name: cell.bookingTime.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.servicePriceLBL.font = UIFont(name: cell.servicePriceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.servicePrice.font = UIFont(name: cell.servicePrice.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
        
        cell.ServiceReviewBtn.titleLabel?.font = UIFont(name: (cell.ServiceReviewBtn.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
        cell.userReviewBtn.titleLabel?.font = UIFont(name: (cell.userReviewBtn.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
        cell.deleteServiceButton.titleLabel?.font = UIFont(name: (cell.deleteServiceButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
        cell.CancelBtn.titleLabel?.font = UIFont(name: (cell.CancelBtn.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!

//Buttons Checking
        if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["delete_status"]!)".isEmpty
        {
            cell.deleteServiceButton.isHidden = true
        }
        else
        {
            cell.deleteServiceButton.isHidden = false
        }
        //Cancel Button Checking
        
        if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button1"]!)" == "Cancel"
        {
            cell.CancelBtn.isHidden = false
        }
        else
        {
            cell.CancelBtn.isHidden = true
        }
        //user review button checking
        if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["usr_rvw_btn"]!)" == ""
        {
            cell.userReviewBtn.isHidden = true
        }
        else if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["usr_rvw_btn"]!)" == "0"
        {
            cell.userReviewBtn.isHidden = false
            cell.userReviewBtn.setTitle("Add User Review", for: .normal)
            cell.userReviewBtn.addTarget(self, action: #selector(ServiceInformationViewController.addUserReview(sender:)), for: .touchUpInside)
            
        }
        else
        {
            cell.userReviewBtn.isHidden = false
            cell.userReviewBtn.setTitle("View User Review", for: .normal)
        }
        //Product review button checking
        if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["prd_rvw_btn"]!)" == ""
        {
            cell.ServiceReviewBtn.isHidden = true
        }
        else if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["prd_rvw_btn"]!)" == "0"
        {
            cell.ServiceReviewBtn.isHidden = false
            cell.ServiceReviewBtn.setTitle("Add Product Review", for: .normal)
            cell.ServiceReviewBtn.addTarget(self, action: #selector(ServiceInformationViewController.addProductReview(sender:)), for: .touchUpInside)
            
            
        }
        else
        {
            cell.ServiceReviewBtn.isHidden = false
            cell.ServiceReviewBtn.setTitle("View Product Review", for: .normal)
        }
        if  "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["delete_status"]!)".elementsEqual("") && "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button1"]!)".elementsEqual("")  &&  "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["usr_rvw_btn"]!)".elementsEqual("")  &&
            "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button1"]!)".elementsEqual("")
        {
            cell.buttonView.isHidden = true
        }
        else
        {
            cell.buttonView.isHidden = false
        }
        
        
        cell.userReviewBtn.layer.cornerRadius = cell.userReviewBtn.frame.size.height / 2
        cell.ServiceReviewBtn.layer.cornerRadius = cell.ServiceReviewBtn.frame.size.height / 2
        
        cell.cellStackview.layer.cornerRadius = 8
        
        
//       //Buttons View Layer
//        cell.buttonView.layer.borderWidth = 2
//        cell.buttonView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        

        
//        cell.serviceInformationView.layer.borderWidth = 2
//        cell.serviceInformationView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
        
    }
    
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        serviceRequestID = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["service_req_id"]!)"
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "serviceInformationDetailsVC") as! ServiceInformationDetailsViewController
        
        navigate.serviceRequestID = serviceRequestID
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK: - // Scrollview Delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            
//            print("next start : ",nextStart )
            
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                getServiceInformationList()
            }
            
        }
    }

    //MARK:- //Get listing using Global API
    func getServiceInformationList()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_service_information", param: parameters, completion: {
            self.serviceInformationArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.serviceInformationArray.count-1)

            {

                let tempDict = self.serviceInformationArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)

            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.serviceInformationArray.count

            print("Next Start Value : " , self.startValue)

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.serviceInformationTableview.delegate = self
                self.serviceInformationTableview.dataSource = self
                self.serviceInformationTableview.reloadData()
                self.serviceInformationTableview.rowHeight = UITableView.automaticDimension
                self.serviceInformationTableview.estimatedRowHeight = UITableView.automaticDimension

                SVProgressHUD.dismiss()
            }
        })
    }

    //MARK:- //Global API for Cancelling Service
    func cancelService()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&service_req_id=\(serviceRequestID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_service_cancel", param: parameters, completion: {
            DispatchQueue.main.async {

                SVProgressHUD.dismiss()

                self.globalDispatchgroup.leave()

            }
        })
    }

    // MARK: - // JSON POST Method to Delete Service
    func deleteService()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&ser_req_id=\(serviceRequestID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_serviceinfo_delete", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()

            }
        })
    }
    
    // MARK: - // JSON POST Method to Send Review
    func sendReview()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&rev_prod_id=\(productID!)&service_req_id=\(serviceRequestID!)&rev_seller_id=\(sellerID!)&ratinginput=\(reviewRating!)&description=\(commentTXTView.text!)&add_rev_sec=\(reviewType!)"
        self.CallAPI(urlString: "app_addreview_service", param: parameters, completion: {
//            print("Send Review Response: " , self.globalJson)
            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                let alert = UIAlertController(title: "Rating Response", message: "\(self.globalJson["message"]!)", preferredStyle: .alert)

                let ok = UIAlertAction(title: "OK", style: .default) {
                    UIAlertAction in

                    UIView.animate(withDuration: 0.5)
                    {
                        self.view.sendSubviewToBack(self.reviewMainView)
                        self.reviewMainView.isHidden = true
                    }

                }

                alert.addAction(ok)

                self.present(alert, animated: true, completion: nil )

                SVProgressHUD.dismiss()
            }

        })
    }
    
    // MARK:- // Add User Review
    
    @objc func addUserReview(sender: UIButton)
    {
        UIView.animate(withDuration: 0.5)
        {
            
            self.reviewTitleLBL.text =  LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_user_review", comment: "")
            
            self.rating.rating = 0
            self.commentTXTView.text = ""
            
            
            self.reviewType = "U"
            
            self.serviceRequestID = "\((self.recordsArray[sender.tag] as! NSDictionary)["service_req_id"]!)"
            self.sellerID = "\((self.recordsArray[sender.tag] as! NSDictionary)["selelr_id"]!)"
            self.productID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"
            
            self.view.bringSubviewToFront(self.reviewMainView)
            self.reviewMainView.isHidden = false
        }
    }
    
    // MARK:- // Add Product Review
    
    @objc func addProductReview(sender: UIButton)
    {
        UIView.animate(withDuration: 0.5)
        {
            
           self.reviewTitleLBL.text =  LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_product_review", comment: "")
            
            self.rating.rating = 0
            self.commentTXTView.text = ""
            
            
            self.reviewType = "P"
            
            self.serviceRequestID = "\((self.recordsArray[sender.tag] as! NSDictionary)["service_req_id"]!)"
            self.sellerID = "\((self.recordsArray[sender.tag] as! NSDictionary)["selelr_id"]!)"
            self.productID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"
            
            self.view.bringSubviewToFront(self.reviewMainView)
            self.reviewMainView.isHidden = false
        }
    }
    
    
    // MARK:- // Cancel Service
    
    
    @objc func cancelServiceTap(sender: UIButton)
    {
        
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            
            
            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
            
            self.serviceRequestID = "\(tempDictionary["service_req_id"]!)"
            
            self.cancelService()
            
            self.dispatchGroup.notify(queue: .main, execute: {
                
                self.recordsArray.removeAllObjects()
                
                self.startValue = 0
                
                self.getServiceInformationList()
                
            })
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil )
        
        
    }
    // MARK:- // Delete Service
    @objc func deleteServiceTap(sender: UIButton)
    {
        
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            
            
            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
            
            self.serviceRequestID = "\(tempDictionary["service_req_id"]!)"
            
            self.deleteService()
            
            self.dispatchGroup.notify(queue: .main, execute: {
                
                self.recordsArray.removeAllObjects()
                
                self.startValue = 0
                
                self.getServiceInformationList()
            })
            
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil )
        
        
    }
    
    
    // MARK:- // Cosmos Methods
    
    private class func formatValue(_ value: Double) -> String {
        return String(format: "%.2f", value)
    }
    
    private func didTouchCosmos(_ rating: Double) {
        
        let ratingValue = ServiceInformationViewController.formatValue(rating)
        
        reviewRating = ratingValue
        
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        
        let ratingValue = ServiceInformationViewController.formatValue(rating)
        
        reviewRating = ratingValue
        
    }
    // MARK: - // Review Section Buttons
    
    // Close Review View
    
    @IBAction func crossReview(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.reviewMainView)
            self.reviewMainView.isHidden = true
        }
        
    }
    // Save Review
    
    @IBAction func saveReview(_ sender: UIButton) {
        
        if reviewRating.elementsEqual("0")
        {
            let alert = UIAlertController(title: "Select Rating", message: "Please Select Any Valid Rating", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else
        {
            sendReview()
        }
        
    }
    
    // MARK:- // ReviewMainView Multilingual setup
    
    func setupReviewMainView()
    {
        reviewTitleLBL.font = UIFont(name: reviewTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        ratingLBL.font = UIFont(name: ratingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        commentLBL.font = UIFont(name: commentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        commentTXTView.font = UIFont(name: (commentTXTView.font?.fontName)!, size: CGFloat(Get_fontSize(size: 15)))
        
        saveButtonOutlet.titleLabel?.font = UIFont(name: (saveButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!
        
        
        
        ratingLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "rating", comment: "")
        commentLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "comment", comment: "")
        saveButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "save", comment: ""), for: .normal)
    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }

}
//MARK:- //Service Information Tablevie cell
class serviceInformationTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellStackview: UIStackView!

    @IBOutlet weak var serviceInformationView: UIView!

    @IBOutlet weak var bookingNumberLBL: UILabel!
    @IBOutlet weak var bookingNumber: UILabel!

    @IBOutlet weak var statusLBL: UILabel!
    @IBOutlet weak var status: UILabel!

    @IBOutlet weak var serviceNameLBL: UILabel!
    @IBOutlet weak var serviceName: UILabel!

    @IBOutlet weak var bookingDateLBL: UILabel!
    @IBOutlet weak var bookingDate: UILabel!

    @IBOutlet weak var bookingTimeLBL: UILabel!
    @IBOutlet weak var bookingTime: UILabel!

    @IBOutlet weak var servicePriceLBL: UILabel!
    @IBOutlet weak var servicePrice: UILabel!

    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var deleteServiceButton: UIButton!
    @IBOutlet weak var buttonView: UIView!

    @IBOutlet weak var userReviewBtn: UIButton!
    @IBOutlet weak var ServiceReviewBtn: UIButton!

    @IBOutlet weak var CancelBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

