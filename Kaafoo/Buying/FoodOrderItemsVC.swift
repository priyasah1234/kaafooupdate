//
//  FoodOrderItemsVC.swift
//  Kaafoo
//
//  Created by esolz on 14/12/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class FoodOrderItemsVC: GlobalViewController {
    
    var Perload : Int! = 20
    var StartValue : String! = "0"
    var OrderArray : NSMutableArray! = NSMutableArray()
    
    var ScrollBegin : CGFloat!
    var ScrollEnd : CGFloat!
    
    var ReadyStatus : String! = "1"
    var ProcessingStatus : String! = "2"
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    let LangID = UserDefaults.standard.string(forKey: "langID")
    @IBOutlet weak var OrederTV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getListing()
        self.OrederTV.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    
    func getListing()
    {
        let parameters = "user_id=\(userID ?? "")&lang_id=\(LangID ?? "")&per_load=\(Perload!)&start_value=\(StartValue ?? "")"
        self.CallAPI(urlString: "app_food_item_preparing", param: parameters, completion: {
            let infoArray = self.globalJson["info_array"] as! NSArray
            
            self.globalDispatchgroup.leave()
            for i in 0..<infoArray.count
            {
              let tempdict = infoArray[i] as! NSDictionary
              self.OrderArray.add(tempdict)
            }
            self.StartValue = "\(self.globalJson["next_start"] ?? "")"
            self.globalDispatchgroup.notify(queue: .main, execute: {
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.OrederTV.delegate = self
                    self.OrederTV.dataSource = self
                    self.OrederTV.reloadData()
                }
            })
            
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func SuggestedBtnReadyTarget(sender : UIButton)
    {
        print("Ready")
        let parameters = "user_id=\(userID ?? "")&lang_id=\(LangID ?? "")&status=\(ReadyStatus ?? "")&item_id=\((self.OrderArray[sender.tag] as! NSDictionary)["item_id"] ?? "")"
        self.CallAPI(urlString: "app_preparing_status_change", param: parameters, completion:
            {
                self.globalDispatchgroup.leave()
                self.globalDispatchgroup.notify(queue: .main, execute: {
                    DispatchQueue.main.async {
                        self.ShowAlertMessage(title: "Alert", message: "\(self.globalJson["message"] ?? "")")
                        self.StartValue = "0"
                        self.OrderArray.removeAllObjects()
                        self.getListing()
                        self.OrederTV.reloadData()
                        SVProgressHUD.dismiss()
                    }
                })
        })
    }
    
    @objc func SuggestedBtnProcessingTarget(sender : UIButton)
    {
        print("Processing")
        let parameters = "user_id=\(userID ?? "")&lang_id=\(LangID ?? "")&status=\(ProcessingStatus ?? "")&item_id=\((self.OrderArray[sender.tag] as! NSDictionary)["item_id"] ?? "")"
        self.CallAPI(urlString: "app_preparing_status_change", param: parameters, completion:
            {
                self.globalDispatchgroup.leave()
                self.globalDispatchgroup.notify(queue: .main, execute: {
                    SVProgressHUD.dismiss()
                    DispatchQueue.main.async {
                        self.ShowAlertMessage(title: "Alert", message: "\(self.globalJson["message"] ?? "")")
                        self.StartValue = "0"
                        self.OrderArray.removeAllObjects()
                        self.getListing()
                        self.OrederTV.reloadData()
                        SVProgressHUD.dismiss()
                    }
                })
        })
    }
    
    @objc func DoNothing(sender : UIButton)
    {
        print("Ready To Deliver")
    }

}

class FoodsOrderItemsVC : UITableViewCell
{
    @IBOutlet weak var ViewContent: UIView!
    @IBOutlet weak var ProuctIV: UIImageView!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var ProductDescripion: UILabel!
    @IBOutlet weak var ProductQuantity: UILabel!
    @IBOutlet weak var ProductOrder: UILabel!
    @IBOutlet weak var ProductSpecials: UILabel!
    @IBOutlet weak var SuggestedBtnOutlet: UIButton!
}

extension FoodOrderItemsVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.OrderArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orders") as! FoodsOrderItemsVC
        cell.ViewContent.layer.borderWidth = 1.0
        cell.ViewContent.layer.borderColor = UIColor.lightGray.cgColor
        cell.ViewContent.layer.cornerRadius = 5.0
        
        cell.ProuctIV.clipsToBounds = true
        
        cell.ProuctIV.image = UIImage(named: "Food3")
        cell.ProuctIV.contentMode = .scaleToFill
        cell.ProuctIV.clipsToBounds = true
        
        cell.SuggestedBtnOutlet.layer.cornerRadius = cell.SuggestedBtnOutlet.frame.size.height / 2
        
        
        
        
        cell.ProductDescripion.attributedText = createMultiFontLabel(ColorOne: .black, ColorTwo: .black, textOne: "This Product is ", textTwo: "Available", fontOneSize: 10, fontTwoSize: 12)
        
        let imageURL = "\((self.OrderArray[indexPath.row] as! NSDictionary)["image"] ?? "")"
        cell.ProuctIV.sd_setImage(with: URL(string: imageURL))
        cell.ProductDescripion.attributedText = createMultiFontLabel(ColorOne: .black, ColorTwo: .black, textOne: "Description : ", textTwo: "\((self.OrderArray[indexPath.row] as! NSDictionary)["description"] ?? "")", fontOneSize: 12, fontTwoSize: 10)
        
        cell.ProductName.attributedText = createMultiFontLabel(ColorOne: .black, ColorTwo: .black, textOne: "Food Name : ", textTwo: "\((self.OrderArray[indexPath.row] as! NSDictionary)["product_name"] ?? "")", fontOneSize: 12, fontTwoSize: 10)
        
        cell.ProductQuantity.attributedText = createMultiFontLabel(ColorOne: .black, ColorTwo: .black, textOne: "Food Quantity : ", textTwo: "\((self.OrderArray[indexPath.row] as! NSDictionary)["quantity"] ?? "")", fontOneSize: 12, fontTwoSize: 10)
        
        cell.ProductSpecials.attributedText = createMultiFontLabel(ColorOne: .black, ColorTwo: .black, textOne: "Specials : ", textTwo: "\((self.OrderArray[indexPath.row] as! NSDictionary)["specials"] ?? "")", fontOneSize: 12, fontTwoSize: 10)
        
        cell.ProductOrder.attributedText = createMultiFontLabel(ColorOne: .black, ColorTwo: .black, textOne: "Order : #", textTwo: "\((self.OrderArray[indexPath.row] as! NSDictionary)["order_no"] ?? "")", fontOneSize: 12, fontTwoSize: 10)
        
        let PreparingText = "\((self.OrderArray[indexPath.row] as! NSDictionary)["preparing_text"] ?? "")"
        
        cell.SuggestedBtnOutlet.setTitle("\(PreparingText)", for: .normal)
        
        if "\((self.OrderArray[indexPath.row] as! NSDictionary)["preparing_status"] ?? "")".elementsEqual("1")
        {
            cell.SuggestedBtnOutlet.isUserInteractionEnabled = true
            cell.SuggestedBtnOutlet.setTitleColor(.white, for: .normal)
            cell.SuggestedBtnOutlet.addTarget(self, action: #selector(SuggestedBtnReadyTarget(sender:)), for: .touchUpInside)
        }
        else if "\((self.OrderArray[indexPath.row] as! NSDictionary)["preparing_status"] ?? "")".elementsEqual("2")
        {
            cell.SuggestedBtnOutlet.isUserInteractionEnabled = true
            cell.SuggestedBtnOutlet.setTitleColor(.white, for: .normal)
            cell.SuggestedBtnOutlet.addTarget(self, action: #selector(SuggestedBtnProcessingTarget(sender:)), for: .touchUpInside)
        }
        else if "\((self.OrderArray[indexPath.row] as! NSDictionary)["preparing_status"] ?? "")".elementsEqual("3")
        {
            cell.SuggestedBtnOutlet.setTitleColor(.black, for: .normal)
            cell.SuggestedBtnOutlet.isUserInteractionEnabled = false
            cell.SuggestedBtnOutlet.addTarget(self, action: #selector(DoNothing(sender:)), for: .touchUpInside)
        }
        
        cell.SuggestedBtnOutlet.tag = indexPath.row
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}

// MARK:- // Scrollview Delegate MEthods

extension FoodOrderItemsVC: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        ScrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        ScrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if ScrollBegin > ScrollEnd
        {

        }
        else
        {
            if self.StartValue.elementsEqual("")
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.getListing()
            }
        }
    }

}


