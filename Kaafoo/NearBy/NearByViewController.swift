//
//  NearByViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 04/04/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import MapKit
import SVProgressHUD
import Foundation
import GoogleMaps
import SDWebImage

class CityLocation: NSObject, MKAnnotation {
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var subtitle  : String?


    init(subtitle: String , title: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
        self.subtitle = subtitle

    }
}

class NearByViewController: GlobalViewController,UIPickerViewDelegate,UIPickerViewDataSource,MKMapViewDelegate {

    @IBOutlet weak var ShowDetailsView: UIView!
    var ProductId : String!

    var info_array : NSDictionary!
    var product_info_array : NSMutableArray! = NSMutableArray()
    var cat_info_array : NSMutableArray! = NSMutableArray()
    var recordsArray : NSMutableArray! = NSMutableArray()
    var categorylistarray : NSMutableArray! = NSMutableArray()
    @IBOutlet weak var Product_ImageView: UIImageView!
    @IBOutlet weak var Product_name: UILabel!
    @IBOutlet weak var Listing_Time: UILabel!
    @IBOutlet weak var Closing_Time: UILabel!
    @IBOutlet weak var Address: UILabel!
    @IBOutlet weak var PriceType: UILabel!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var RefundableType: UILabel!
    @IBOutlet weak var CrossBtnOutlet: UIButton!
    @IBAction func Cross_Btn(_ sender: UIButton) {
        self.ShowDetailsView.isHidden = true
    }
    @IBOutlet weak var LocalHeaderView: UIView!
    @IBOutlet weak var CategoryView: UIView!
    @IBOutlet weak var DataPickerView: UIPickerView!
    var SearchCat : String! = ""
    @IBOutlet weak var CategorySelectOutlet: UIButton!
    @IBOutlet weak var SearchBtn: UIButton!
    @IBOutlet weak var SearchOutlet: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBAction func CategorySelectBtn(_ sender: UIButton) {
        self.DataPickerView.reloadAllComponents()
        CategoryView.isHidden = !CategoryView.isHidden
        if CategoryView.isHidden == false
        {
           CategoryView.bringSubviewToFront(view)
        }
        else
        {
            //Do Nothing
        }
    }
    var CategoryArray = ["All","Marketplace","Daily Deals","Services","Jobs","Food Court","Happening","Daily Rental","Holiday Accommodation"]

    let coords = [  CLLocation(latitude: 88.00, longitude: 22.00),
                    CLLocation(latitude: 88.00, longitude: 22.00),
                    CLLocation(latitude: 88.00, longitude:22.00)
    ];

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        self.fetchData()
        self.TapGesture()
        self.tapOnMap()
//        self.addAnnotations(coords: <#T##[CLLocation]#>)
        //Create Annotations

//        let sikkim=CityLocation(title: "Sikkim", coordinate: CLLocationCoordinate2D(latitude: 27.8236356, longitude:88.556531))
//        let delhi = CityLocation(title: "Delhi", coordinate: CLLocationCoordinate2D(latitude: 28.619570, longitude: 77.088104))
//        let kashmir = CityLocation(title: "Kahmir", coordinate: CLLocationCoordinate2D(latitude: 34.1490875, longitude: 74.0789389))
//        let gujrat = CityLocation(title: "Gujrat", coordinate: CLLocationCoordinate2D(latitude: 22.258652, longitude: 71.1923805))
//        let kerala = CityLocation(title: "Kerala", coordinate: CLLocationCoordinate2D(latitude: 9.931233, longitude:76.267303))
//
//        mapView.addAnnotation(sikkim)
//        mapView.addAnnotation(delhi)
//        mapView.addAnnotation(kashmir)
//        mapView.addAnnotation(gujrat)
//        mapView.addAnnotation(kerala)
//
//        mapView.addAnnotations([sikkim, delhi, kashmir, gujrat, kerala])

        // Do any additional setup after loading the view.
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }

        let reuseId = "Pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView?.canShowCallout = true

            let rightButton: AnyObject! = UIButton(type: UIButton.ButtonType.detailDisclosure)
            pinView?.rightCalloutAccessoryView = rightButton as? UIView

        }
        else {
            pinView?.annotation = annotation
        }

        return pinView
    }
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        print(#function)
        if control == view.rightCalloutAccessoryView {
            print("notning")
            print(self.ProductId!)
            self.ShowDetails()
            self.ShowDetailsView.isHidden = false
        }
        else
        {
            self.ShowDetailsView.isHidden = true
        }
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        print("The annotation selected was","\(view.annotation?.subtitle!)")

        self.ProductId = "\(view.annotation?.subtitle! ?? "")"
    }

    //Fetch Data From API

    func fetchData()
    {
        if categorylistarray.count > 0
        {
            self.categorylistarray.removeAllObjects()
        }
        else
        {
            //Do Nothing
        }

        let userID = UserDefaults.standard.string(forKey: "userID")
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
        let parameters = "user_id=\(userID!)&mycountry_name=\(myCountryName!)&lang_id=\(langID!)&search_cat=\(SearchCat!)"
        self.CallAPI(urlString: "app_nearby_list", param: parameters, completion: {
            self.info_array = self.globalJson["info_array"] as? NSMutableDictionary
            self.product_info_array = self.info_array["product_list"] as? NSMutableArray
            self.cat_info_array = self.info_array["cat_info"] as? NSMutableArray
//            print("hello",self.globalJson)
            for i in (0..<self.product_info_array.count)
            {
                let DetailDict = self.product_info_array[i] as! NSDictionary
                print("DetailDict is : " , DetailDict)

                self.recordsArray.add(DetailDict )
            }
            for j in (0..<self.cat_info_array.count)
            {
                let catInfo = self.cat_info_array[j] as! NSDictionary
                self.categorylistarray.add(catInfo )
            }
            print("CategoryListArray",self.categorylistarray!)

//            print("recordsarray",self.recordsArray)

            self.ShowLocation()


            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()
            }
            self.globalDispatchgroup.notify(queue: .main, execute: {
                self.DataPickerView.delegate = self
                self.DataPickerView.dataSource = self
                self.DataPickerView.reloadAllComponents()
            })
        })
    }

    func ShowLocation()
    {
        for i in (0..<self.recordsArray.count)
        {
            let locationDetail = CityLocation(subtitle: "\((self.recordsArray[i] as! NSDictionary)["id"]!)", title: "\((self.recordsArray[i] as! NSDictionary)["product_name"]!)", coordinate: CLLocationCoordinate2D(latitude:Double("\((self.recordsArray[i] as! NSDictionary)["lat"] ?? "")")!, longitude:Double("\((self.recordsArray[i] as! NSDictionary)["long"] ?? "")")!))
            print("longitude===",("\((self.recordsArray[i] as! NSDictionary)["long"] ?? "")"))

            self.mapView.addAnnotation(locationDetail)
            //                        self.mapView.addAnnotation(location.coordinate as! MKAnnotation)
        }

    }
    func ShowDetails()
    {
        for i in (0..<self.recordsArray.count)
        {
            let tempId =  "\((self.recordsArray[i] as! NSDictionary)["id"]!)"
            if tempId == ProductId
            {
                let path = "\((self.recordsArray[i] as! NSDictionary)["photo_url"]!)"
               self.Product_ImageView.sd_setImage(with: URL(string: path))



               self.Product_name.text = "\((self.recordsArray[i] as! NSDictionary)["product_name"]!)"
               self.Listing_Time.text = "\((self.recordsArray[i] as! NSDictionary)["listed_time"]!)"
               self.Closing_Time.text = "\((self.recordsArray[i] as! NSDictionary)["expire_time"]!)"
               self.Address.text = "\((self.recordsArray[i] as! NSDictionary)["address"]!)"
               self.Price.text = "\((self.recordsArray[i] as! NSDictionary)["reserve_price"]!)"

            }
            else
            {
               print("Do nothing")
            }
        }
    }

    //MARK:- Pickerview Delegate and datasource methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categorylistarray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

print("\((categorylistarray[row] as! NSDictionary)["cat_name"]!)")

        return "\((categorylistarray[row] as! NSDictionary)["cat_name"]!)"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("categorylistarray",self.categorylistarray!)
        let pickedname = "\((categorylistarray[row] as! NSDictionary)["id"]!)"
        let CateGoryName = "\((categorylistarray[row] as! NSDictionary)["cat_name"]!)"
        CategorySelectOutlet.setTitle(CateGoryName, for: .normal)
        self.SearchCat = "\(pickedname)"
        self.removeAnnotations()
//        self.recordsArray.removeAllObjects()
        self.fetchData()
        print("pickedname",pickedname)
        CategoryView.isHidden = true
    }

    //MARK:- //Remove all annotations from MAPkit
    func removeAnnotations()
    {
        let allannotations = self.mapView.annotations
        self.mapView.removeAnnotations(allannotations)
        self.recordsArray.removeAllObjects()

    }
    //MARK:- //Add Gesture to Uiview
    func TapGesture()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        CategoryView.addGestureRecognizer(tap)
        CategoryView.isUserInteractionEnabled = true
    }


    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.CategoryView.isHidden = true
    }
    //MARK:- //Tap on MAPview to Hide Product Details View
    func tapOnMap()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.TapHandle(_sender:)) )
        mapView.addGestureRecognizer(tap)
        mapView.isUserInteractionEnabled = true
    }
    @objc func TapHandle(_sender : UITapGestureRecognizer)
    {
        self.ShowDetailsView.isHidden = true
    }



}

