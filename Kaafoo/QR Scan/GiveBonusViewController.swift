//
//  GiveBonusViewController.swift
//  Kaafoo
//
//  Created by admin on 04/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class GiveBonusViewController: GlobalViewController,UITextFieldDelegate {

    @IBOutlet weak var userInfoView: UIView!
    @IBOutlet weak var userImageview: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    
    @IBOutlet weak var invoiceView: UIView!
    
    @IBOutlet weak var invoiceNumberView: UIView!
    @IBOutlet weak var invoiceNumberLBL: UILabel!
    @IBOutlet weak var invoiceNumberTXT: UITextField!
    @IBOutlet weak var invoiceSeparator: UIView!
    
    
    @IBOutlet weak var invoiceAmountView: UIView!
    @IBOutlet weak var invoiceAmountLBL: UILabel!
    @IBOutlet weak var invoiceAmountTXT: UITextField!
    @IBOutlet weak var invoiceAmountSeparator: UIView!
    
    
    @IBOutlet weak var bonusAmountView: UIView!
    @IBOutlet weak var bonusAmount: UILabel!
    
    
    
    
    // MARK:- // Submit Button
    
    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBAction func tapOnSubmit(_ sender: UIButton) {
        
        self.giveBonus()
        
    }
    
    
    var userInfoDictionary : NSDictionary!
    
    let dispatchGroup = DispatchGroup()
    
    
    
    // MARK:- // View Did Load
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bonusAmountView.isHidden = true
        
        self.setFont()

        self.submitButtonOutlet.layer.cornerRadius = self.submitButtonOutlet.frame.size.height / 2
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.userImageview.sd_setImage(with: URL(string: "\(self.userInfoDictionary["logo_image"]!)"))
        self.userName.text = "\(self.userInfoDictionary["business_name"]!)"
        
        self.invoiceAmountTXT.delegate = self
        
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {

        if textField == invoiceAmountTXT
        {
            if (self.invoiceAmountTXT.text?.elementsEqual(""))! {
                self.bonusAmountView.isHidden = true
            }
            else {
                self.bonusAmountView.isHidden = false
                bonusAmount.text = "Bonus Amount : " + "\(Int(invoiceAmountTXT.text!)! / 10)"
            }
        }

    }
    
    
    
    
    
    // MARK: - // JSON POST Method to Give Bonus
    
    func giveBonus()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_get_seller_bonus")!   //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
        let currencyID = UserDefaults.standard.string(forKey: "currencyID")
        
        parameters = "buyer_id=\(userID!)&seller_id=\(userInfoDictionary["user_id"]!)&invoice_number=\(invoiceNumberTXT.text!)&price=\(invoiceAmountTXT.text!)&bonus_price=\(Int(invoiceAmountTXT.text!)! / 10)&country_name=\(myCountryName!)&bonus_type=\(1)&currency_id=\(currencyID!)"
        
        print("Give Bonus URL is : ",url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Daily Rental Product Details Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            let alert = UIAlertController(title: "Invalid Request", message: "\(json["message"]!)", preferredStyle: .alert)
                            
                            let ok = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                            
                            alert.addAction(ok)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            SVProgressHUD.dismiss()
                            
                        }
                        
                    }
                    else
                    {
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            let alert = UIAlertController(title: "Success", message: "\(json["message"]!)", preferredStyle: .alert)
                            
                            let ok = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                            
                            alert.addAction(ok)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            SVProgressHUD.dismiss()
                        }
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
    }
    
    
    
    
    // MARK:- // Set Font
    
    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        userName.font = UIFont(name: userName.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        invoiceNumberLBL.font = UIFont(name: invoiceNumberLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        invoiceNumberTXT.font = UIFont(name: (invoiceNumberTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        
        invoiceAmountLBL.font = UIFont(name: invoiceAmountLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        invoiceAmountTXT.font = UIFont(name: invoiceAmountTXT.font!.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        submitButtonOutlet.titleLabel?.font = UIFont(name: (submitButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!
    }
   
}
