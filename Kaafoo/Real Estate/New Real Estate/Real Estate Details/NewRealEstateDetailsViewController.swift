//
//  NewRealEstateDetailsViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 31/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import GoogleMaps
import GooglePlaces
import WebKit

class NewRealEstateDetailsViewController: GlobalViewController {

    @IBOutlet weak var AccountTypeLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    @IBOutlet weak var sliderCollectionview: UICollectionView!
    @IBOutlet weak var sliderPagecontrol: UIPageControl!
    @IBOutlet weak var timeLineView: UIView!
    @IBOutlet weak var listedOn: UILabel!
    @IBOutlet weak var closingIn: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var viewOfMap: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var productAddress: UILabel!
    @IBOutlet weak var reportImageview: UIImageView!
    @IBOutlet weak var reportLBL: UILabel!
    @IBOutlet weak var viewsLBL: UILabel!
    @IBOutlet weak var watchlistLBL: UILabel!
    @IBOutlet weak var listingLBL: UILabel!
    @IBOutlet weak var listingNo: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var descriptionTitleview: UIView!
    @IBOutlet weak var descriptionTitleLBL: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var aboutThePropertyView: UIView!
    @IBOutlet weak var aboutThePropertyTitleLBL: UILabel!
    @IBOutlet weak var aboutThePropertyTable: UITableView!
    @IBOutlet weak var aboutPropertyTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var questionAnswerVIew: UIView!
    @IBOutlet weak var questionAnswerTitleLBL: UILabel!
    @IBOutlet weak var questionAnswerTable: UITableView!
    @IBOutlet weak var qnaTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var noQuestionsPostedLBL: UILabel!



    @IBOutlet weak var postedByView: UIView!
    @IBOutlet weak var postedByTitleLBL: UILabel!
    @IBOutlet weak var followLBL: UILabel!
    @IBOutlet weak var sellerImageview: UIImageView!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var feedback: UILabel!
    @IBOutlet weak var totalRating: UILabel!
    @IBOutlet weak var memberSince: UILabel!
    @IBOutlet weak var contactNo: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var totalRatingStarImageview: UIImageView!
    @IBOutlet weak var reviewsView: UIView!
    @IBOutlet weak var reviewsTitleLBL: UILabel!
    @IBOutlet weak var noReviewsLBL: UILabel!

    @IBOutlet weak var accountTypeImageview: UIImageView!

    @IBOutlet weak var sellerStatusView: UIView!
    @IBOutlet weak var bonusView: UIView!
    @IBOutlet weak var bonusImageview: UIImageView!
    @IBOutlet weak var bonusLBL: UILabel!
    @IBOutlet weak var verifiedView: UIView!
    @IBOutlet weak var verifiedImageview: UIImageView!
    @IBOutlet weak var verifiedLBL: UILabel!
    @IBOutlet weak var maroofView: UIView!
    @IBOutlet weak var maroofImageview: UIImageView!
    @IBOutlet weak var maroofLBL: UILabel!

    @IBOutlet weak var sellerOtherListingsView: UIView!
    @IBOutlet weak var sellerOtherListingsTitleLBL: UILabel!
    @IBOutlet weak var sellerOtherListingsCollectionview: UICollectionView!
    @IBOutlet weak var noOtherListingsLBL: UILabel!


    @IBOutlet weak var productTypeView: UIView!
    @IBOutlet weak var auctionView: UIView!
    @IBOutlet weak var otherProducttypeView: UIView!
    @IBOutlet weak var currentBidTitleLBL: UILabel!
    @IBOutlet weak var totalBids: UILabel!
    @IBOutlet weak var currentBid: UILabel!
    @IBOutlet weak var nextBidAmountLBL: UILabel!
    @IBOutlet weak var nextBidAmount: UITextField!
    @IBOutlet weak var bidHistoryStackview: UIStackView!
    @IBOutlet weak var bidHistoryTable: UITableView!

    @IBOutlet weak var investmentType: UILabel!
    @IBOutlet weak var productType: UILabel!
    @IBOutlet weak var weeklyPrice: UILabel!
    @IBOutlet weak var monthlyPrice: UILabel!
    @IBOutlet weak var yearlyPrice: UILabel!
    @IBOutlet weak var weeklyPriceView: UIView!
    @IBOutlet weak var monthlyPriceView: UIView!
    @IBOutlet weak var yearlyPriceView: UIView!

    @IBOutlet weak var bidHistoryTableHeightConstraint: NSLayoutConstraint!


    @IBOutlet weak var termsAndConditionsView: UIView!
    @IBOutlet weak var tcTitleLBL: UILabel!
    @IBOutlet weak var tcWebview: UIWebView!

    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var blurviewTitleLBL: UILabel!
    @IBOutlet weak var blurviewContentTXT: UITextView!

    @IBOutlet weak var breadCrumbBarView: UIView!
    @IBOutlet weak var browseCategoryTitleLBL: UILabel!
    @IBOutlet weak var breadcrumbBar: UILabel!
    
    
    @IBOutlet weak var showImageview: UIView!
    @IBOutlet weak var showImageCollectionview: UICollectionView!
    
    
    @IBOutlet weak var mapArrowImage: UIImageView!
    @IBOutlet weak var breadcrumbArrowImage: UIImageView!
    
    
    
    
    


    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var productID : String!

    var productInfoDictionary : NSDictionary!

    var watchlisted : Bool!

    var descriptionHeightConstraint : NSLayoutConstraint!

    var showFullDescription : Bool! = true

    var termsAndConditionsDictionary : NSDictionary!

    var sendMessageTapped : Bool! = false

    var reportTapped : Bool! = false



    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        self.setFont()

        self.FetchData()

        self.globalDispatchgroup.notify(queue: .main) {

            self.doAdditionalWork()

        }

        self.blurviewContentTXT.delegate = self
        
        self.showImageCollectionview.backgroundColor = UIColor.clear
        
        self.changeLanguageStrings()

    }



    // MARK:- // View Will Appear

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.aboutThePropertyTable.estimatedRowHeight = 500
        self.aboutThePropertyTable.rowHeight = UITableView.automaticDimension

        self.questionAnswerTable.estimatedRowHeight = 500
        self.questionAnswerTable.rowHeight = UITableView.automaticDimension



        self.globalDispatchgroup.notify(queue: .main) {

            self.SetTableheight(table: self.aboutThePropertyTable, heightConstraint: self.aboutPropertyTableHeightConstraint)

            if (self.productInfoDictionary["question_answer"] as! NSArray).count > 4 {

                var setheight: CGFloat  = 0
                self.questionAnswerTable.frame.size.height = 3000

                var tempVar = 0

                for cell in self.questionAnswerTable.visibleCells {
                    if tempVar < 4
                    {
                        setheight += cell.bounds.height
                        tempVar = tempVar + 1
                    }
                }

                print("SetHeight---",setheight)

                self.qnaTableHeightConstraint.constant = CGFloat(setheight)
            }
            else {
                self.SetTableheight(table: self.questionAnswerTable, heightConstraint: self.qnaTableHeightConstraint)
            }

            self.SetTableheight(table: self.bidHistoryTable, heightConstraint: self.bidHistoryTableHeightConstraint)

        }
        
        
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        if (langID?.elementsEqual("AR"))! {
            self.mapArrowImage.transform = CGAffineTransform(rotationAngle: .pi)
            self.breadcrumbArrowImage.transform = CGAffineTransform(rotationAngle: .pi)
        }

    }

    // MARK:- // View did Appear

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)



    }



    // MARK:- // Buttons


    // MARK:- // Close Show Imageview Button
    
    @IBOutlet weak var closeShowImageviewButtonOutlet: UIButton!
    @IBAction func closeShowImageviewButton(_ sender: UIButton) {
        
        self.showImageview.isHidden = true
        self.view.sendSubviewToBack(self.showImageview)
        
    }
    
    
    
    
    
    // MARK:- // Breadcrumb Bar Button

    @IBOutlet weak var breadcrumbBarButtonOutlet: UIButton!
    @IBAction func breadcrumbBarButton(_ sender: UIButton) {

        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newrealestate") as! NewRealEstateListingViewController

        self.navigationController?.pushViewController(navigate, animated: true)

    }



    // MARK:- // Submit in Blurview Button

    @IBOutlet weak var submitButtonOutlet: UIButton!
    @IBAction func submitButton(_ sender: UIButton) {

        if self.sendMessageTapped == true {
            self.sendMessage()
        }
        else  if self.reportTapped == true {
            self.reportProduct()
        }

    }



    // MARK:- // Close Blurview

    @IBOutlet weak var closeBlurviewButtonOutlet: UIButton!
    @IBAction func closeBlurviewButton(_ sender: UIButton) {

        self.view.sendSubviewToBack(self.blurView)

        self.blurView.isHidden = true

        self.sendMessageTapped = false
        self.reportTapped = false

    }





    // MARK:- // Close Terms and Conditions


    @IBOutlet weak var closeTCButtonOutlet: UIButton!
    @IBAction func closeTC(_ sender: UIButton) {

        self.view.sendSubviewToBack(self.termsAndConditionsView)
        self.termsAndConditionsView.isHidden = true

    }



    // MARK:- // Place Bid Button


    @IBOutlet weak var placeBidButtonOutlet: UIButton!
    @IBAction func placeBidButton(_ sender: UIButton) {

        if self.checkboxTCButtonOutlet.isSelected {
            if (self.nextBidAmount.text?.elementsEqual(""))! {
                self.ShowAlertMessage(title: "Warning", message: "Please Enter a Bid Amount.")
            }
            else {
                self.placeBid()

                self.globalDispatchgroup.notify(queue: .main) {

                    self.FetchData()

                    self.globalDispatchgroup.notify(queue: .main, execute: {

                        self.SetTableheight(table: self.bidHistoryTable, heightConstraint: self.bidHistoryTableHeightConstraint)

                        self.bidHistoryStackview.isHidden = false

                    })

                }
            }
        }
        else {
            self.ShowAlertMessage(title: "Warning", message: "Please check Terms and Conditions.")
        }

    }



    // MARK:- // Checkbox Terms and Conditions

    @IBOutlet weak var checkboxTCButtonOutlet: UIButton!
    @IBAction func checkboxTCButton(_ sender: UIButton) {

        if self.checkboxTCButtonOutlet.isSelected == true {
            self.checkboxTCButtonOutlet.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
        }
        else {
            self.checkboxTCButtonOutlet.setImage(UIImage(named: "checkBoxFilled"), for: .normal)
        }


        self.checkboxTCButtonOutlet.isSelected = !self.checkboxTCButtonOutlet.isSelected


    }



    // MARK;- // Terms and Conditions Button

    @IBOutlet weak var termsAndConditionsButtonOutlet: UIButton!
    @IBAction func termsAndConditionsButton(_ sender: UIButton) {


        self.tcTitleLBL.text = "Terms and Conditions"

        let url = URL (string: self.GLOBALAPI + "app_kaafoo_terms_condition_page?page_name=terms_condition")
        let requestObj = URLRequest(url: url!)

        self.tcWebview.loadRequest(requestObj)

        self.view.bringSubviewToFront(self.termsAndConditionsView)
        self.termsAndConditionsView.isHidden = false

        /*
         self.fetchTCData()

         self.globalDispatchgroup.notify(queue: .main) {


         self.tcTitleLBL.text = "\(self.termsAndConditionsDictionary["title"] ?? "")"

         //let stringToShow = "\(self.termsAndConditionsDictionary["content"] ?? "")".htmlToString

         // self.tcWebview.loadHTMLString(stringToShow, baseURL: nil)
         let url = URL (string: "http://esolz.co.in/lab6/kaafoo/appcontrol/app_kaafoo_terms_condition_page?page_name=terms_condition")
         let requestObj = URLRequest(url: url!)

         self.tcWebview.loadRequest(requestObj)

         self.view.bringSubview(toFront: self.termsAndConditionsView)
         self.termsAndConditionsView.isHidden = false
         }
         */

    }


    // MARK:- // Show Bid History Button

    @IBOutlet weak var showBidHistoryButtonOutlet: UIButton!
    @IBAction func showBidHistoryButton(_ sender: UIButton) {

        if self.bidHistoryTable.isHidden {
            self.bidHistoryTable.isHidden = false

            self.showBidHistoryButtonOutlet.setTitle("Hide Bid History", for: .normal)
        }
        else {
            self.bidHistoryTable.isHidden = true

            self.showBidHistoryButtonOutlet.setTitle("Show Bid History", for: .normal)
        }

    }





    // MARK:- // Send Message Button

    @IBOutlet weak var sendMessageButtonOutlet: UIButton!
    @IBAction func sendMessageButton(_ sender: UIButton) {

        self.blurviewTitleLBL.text = "Send Message"

        self.blurView.isHidden = false

        self.blurviewContentTXT.text = ""

        self.view.bringSubviewToFront(self.blurView)

        self.sendMessageTapped = true

        self.blurviewContentTXT.text = "Message..."

        self.blurviewContentTXT.textColor = .lightGray


    }


    // MARK:- // View Account Button

    @IBOutlet weak var viewAccountButtonOutlet: UIButton!
    @IBAction func viewAccountButton(_ sender: UIButton) {

        if "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["user_type_status"]!)".elementsEqual("B") {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController

            navigate.sellerID = "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["user_id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "privateUserProfileVC") as! PrivateUserProfileViewController

            navigate.sellerID = "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["user_id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)
        }

    }



    // MARK:- // Follow Button

    @IBOutlet weak var followButtonOutlet: UIButton!
    @IBAction func followButton(_ sender: UIButton) {

        self.followButtonOutlet.isSelected = !self.followButtonOutlet.isSelected

        if self.followLBL.text!.elementsEqual("Follow") {
            self.followSeller()

            self.followLBL.text = "Unfollow"
        }
        else {
            self.unfollowSeller()

            self.followLBL.text = "Follow"
        }


    }




    // MARK:- // All Question and Answer Button


    @IBOutlet weak var allQuestionAndAnswerButtonOutlet: UIButton!
    @IBAction func allQuestionAndAnswerButton(_ sender: UIButton) {

        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newRealEstateAllQnaVC") as! NewRealEstateAllQNAViewController

        navigate.productID = self.productID

        self.navigationController?.pushViewController(navigate, animated: true)

    }



    // MARK:- // Show Full Description

    @IBOutlet weak var showFullDescriptionOutlet: UIButton!
    @IBAction func showFullDescription(_ sender: UIButton) {

        if self.showFullDescription == true {
            self.productDescription.removeConstraint(self.descriptionHeightConstraint)

            let descriptionHeight = "\(self.productInfoDictionary["product_description"] ?? "")".heightWithConstrainedWidth(width: self.FullWidth - 20, font: UIFont(name: self.productDescription.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))!)

            self.descriptionHeightConstraint = self.productDescription.heightAnchor.constraint(equalToConstant: descriptionHeight)

            self.productDescription.addConstraint(self.descriptionHeightConstraint)

            self.showFullDescriptionOutlet.setTitle("Hide Full Description", for: .normal)
        }
        else {
            self.productDescription.removeConstraint(self.descriptionHeightConstraint)
            self.descriptionHeightConstraint = self.productDescription.heightAnchor.constraint(equalToConstant: 80)
            self.productDescription.addConstraint(self.descriptionHeightConstraint)
            self.showFullDescriptionOutlet.setTitle("Show Full Description", for: .normal)
        }

        self.showFullDescription = !self.showFullDescription


    }


    // MARK:- // Report Button

    @IBOutlet weak var reportButtonOutlet: UIButton!
    @IBAction func reportButton(_ sender: UIButton) {


        self.blurviewTitleLBL.text = "Report"

        self.blurView.isHidden = false

        self.blurviewContentTXT.text = ""

        self.view.bringSubviewToFront(self.blurView)

        self.reportTapped = true

        self.blurviewContentTXT.text = "Please Provide a brief description of what is being reported."

        self.blurviewContentTXT.textColor = .lightGray


    }




    // MARK:- // Open Address Button

    @IBOutlet weak var openAddressButtonOutlet: UIButton!
    @IBAction func openAddress(_ sender: UIButton) {
    }




    // MARK:- // WatchList Button

    @IBOutlet weak var watchlistButtonOutlet: UIButton!
    @IBAction func watchlistButton(_ sender: UIButton) {

        let userID = UserDefaults.standard.string(forKey: "userID")

        if userID?.isEmpty == true
        {
            let alert = UIAlertController.init(title: "Warning", message: "Please login and try again later", preferredStyle: .alert)
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if self.watchlisted == true {
                self.GlobalRemoveFromWatchlist(ProductID: self.productID!, completion: {

                    DispatchQueue.main.async {
                        self.watchlistButtonOutlet.setImage(UIImage(named: "heart"), for: .normal)
                        self.watchlisted = false
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateRealEstateWatchlist"), object: nil)
                        
                    }
                })
            }
            else {
                self.GlobalAddtoWatchlist(ProductID: self.productID!, completion: {
                    DispatchQueue.main.async {
                        self.watchlistButtonOutlet.setImage(UIImage(named: "heart_red"), for: .normal)
                        self.watchlisted = true
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateRealEstateWatchlist"), object: nil)
                    }
                })
            }
        }

    }






    //MARK:- // JSON Post Method to Get Real Estate Details

    func FetchData()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "product_id=\(productID!)&user_id=\(userID!)&lang_id=\(langID!)&start_value=\(startValue)&per_load=\(perLoad)"

        self.CallAPI(urlString: "app_product_details", param: parameters, completion: {

            self.productInfoDictionary = self.globalJson["product_info"] as? NSDictionary

            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()

                self.sliderCollectionview.delegate = self
                self.sliderCollectionview.dataSource = self
                self.sliderCollectionview.reloadData()

                self.aboutThePropertyTable.delegate = self
                self.aboutThePropertyTable.dataSource = self
                self.aboutThePropertyTable.reloadData()

                self.questionAnswerTable.delegate = self
                self.questionAnswerTable.dataSource = self
                self.questionAnswerTable.reloadData()

                self.sellerOtherListingsCollectionview.delegate = self
                self.sellerOtherListingsCollectionview.dataSource = self
                self.sellerOtherListingsCollectionview.reloadData()
                
                self.showImageCollectionview.delegate = self
                self.showImageCollectionview.dataSource = self
                self.showImageCollectionview.reloadData()

                if "\(self.productInfoDictionary["product_type_status"]!)".elementsEqual("A") {
                    self.bidHistoryTable.delegate = self
                    self.bidHistoryTable.dataSource = self
                    self.bidHistoryTable.reloadData()
                }



                SVProgressHUD.dismiss()
            }

        })
    }


    //MARK:- // JSON Post Method to get Terms and Conditions Data

    func fetchTCData()
    {
        var parameters : String = ""

        parameters = "page_name=auction_terms_condition"

        self.CallAPI(urlString: "app_kaafoo_terms_condition_page", param: parameters) {

            self.termsAndConditionsDictionary = self.globalJson["info_array"] as? NSDictionary

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

            }
        }
    }




    // MARK:- // JSON POST method to Place a Bid

    func placeBid()
    {
        var parameters : String = ""
        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "user_id=\(userID!)&lang_id=\(langID!)&product_id=\(self.productID!)&bid_price=\(self.nextBidAmount.text!)"

        self.CallAPI(urlString: "app_add_to_bid", param: parameters) {

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

                self.ShowAlertMessage(title: "Warning", message: "\(self.globalJson["message"] ?? "")")
                self.nextBidAmount.text = ""

            }
        }
    }


    //MARK:- //  JSON Post Method to Follow the Seller

    func followSeller()
    {
        var parameters : String = ""
        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "user_id=\(userID!)&buss_seller_id=\("\((self.productInfoDictionary["seller_details"]! as! NSDictionary)["user_id"] ?? "")")&lang_id=\(langID!)"

        self.CallAPI(urlString: "app_add_to_favourite", param: parameters) {

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

                self.ShowAlertMessage(title: "Success", message: "\(self.globalJson["message"] ?? "")")

            }
        }
    }



    // MARK:- // JSON Post method to Unfollow Seller


    func unfollowSeller() {

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "user_id=\(userID!)&lang_id=\(self.langID!)&remove_id=\("\((self.productInfoDictionary["seller_details"]! as! NSDictionary)["user_id"] ?? "")")"

        self.CallAPI(urlString: "app_remove_my_favourite", param: parameters) {

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

                self.ShowAlertMessage(title: "Success", message: "\(self.globalJson["message"] ?? "")")

            }
        }

    }



    // MARK:- // JSON Post Method to Send Message to User

    func sendMessage() {


        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "user_id=\(userID!)&seller_id=\("\((self.productInfoDictionary["seller_details"]! as! NSDictionary)["user_id"] ?? "")")&message=\(blurviewContentTXT.text!)"

        self.CallAPI(urlString: "app_friend_send_message", param: parameters) {

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

            }

        }

        self.globalDispatchgroup.notify(queue: .main) {

            self.customAlert(title: "Submit Success", message: "\(self.globalJson["message"] ?? "")", completion: {

                UIView.animate(withDuration: 0.3, animations: {

                    self.blurView.isHidden = true
                    self.view.sendSubviewToBack(self.blurView)

                    self.sendMessageTapped = false

                })

            })
        }
    }



    // MARK:- // JSON Post Method to Report Product

    func reportProduct() {

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        parameters = "user_id=\(userID!)&product_id=\(self.productID!)&report_msg=\(blurviewContentTXT.text!)&lang_id=\(langID!)"

        self.CallAPI(urlString: "app_report_product", param: parameters) {

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

            }

        }

        self.globalDispatchgroup.notify(queue: .main) {

            self.customAlert(title: "Report Success", message: "\(self.globalJson["message"] ?? "")", completion: {

                UIView.animate(withDuration: 0.3, animations: {

                    self.blurView.isHidden = true
                    self.view.sendSubviewToBack(self.blurView)

                    self.reportTapped = false

                })

            })
        }


    }





    // MARK:- // Show Location

    func showLocation(addressLat: Double , addressLong: Double) {

        print("Address Lat----",addressLat)
        print("Address Long----",addressLong)



        let camera = GMSCameraPosition.camera(withLatitude: Double(addressLat), longitude: Double(addressLong), zoom: 12.0)

        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(addressLat), longitude: Double(addressLong))
        marker.title = "Selected Address"
        marker.snippet = "Description"
        marker.map = self.mapView

        self.mapView.animate(to: camera)

    }





    // MARK:- // Do Additional Work

    func doAdditionalWork() {

        self.listedOn.textColor = UIColor.seaGreen()
        self.closingIn.textColor = UIColor.red()

        if "\(self.productInfoDictionary["watchlist_status"] ?? "")".elementsEqual("0") {
            self.watchlisted = false
            self.watchlistButtonOutlet.setImage(UIImage(named: "heart"), for: .normal)
        }
        else {
            self.watchlisted = true
            self.watchlistButtonOutlet.setImage(UIImage(named: "heart_red"), for: .normal)
        }


        self.listedOn.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Listed", comment: "") + ": " + "\(self.productInfoDictionary["listed_time"] ?? "") ago"

        self.closingIn.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Closing", comment: "") + ": " + "\(self.productInfoDictionary["closein_format"] ?? "")"

        self.productName.text = "\(self.productInfoDictionary["product_name"] ?? "")"

        self.productAddress.text = "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["location"] ?? "")"

        var lat : String!
        var long : String!

        if "\(self.productInfoDictionary["lat"]!)".elementsEqual("") {
            lat = "00.00"
            long = "00.00"
        }
        else {
            lat = "\(self.productInfoDictionary["lat"] ?? "")"
            long = "\(self.productInfoDictionary["long"] ?? "")"
        }

        self.showLocation(addressLat: Double(lat)!, addressLong: Double(long)!)

        self.viewsLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Views", comment: "") + ": " + "\(self.productInfoDictionary["count_view_list"] ?? "")"
        self.watchlistLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Watchlist", comment: "") + ": " + "\(self.productInfoDictionary["count_watchlist"] ?? "")"

        self.listingNo.text = "\(self.productInfoDictionary["listing_number"] ?? "")"

        self.productDescription.text = "\(self.productInfoDictionary["product_description"] ?? "")"

        let descriptionHeight = "\(self.productInfoDictionary["product_description"] ?? "")".heightWithConstrainedWidth(width: self.FullWidth - 20, font: UIFont(name: self.productDescription.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))!)

        self.descriptionHeightConstraint = self.productDescription.heightAnchor.constraint(equalToConstant: 80)
        self.productDescription.translatesAutoresizingMaskIntoConstraints = false


        if descriptionHeight > 80 {
            self.showFullDescriptionOutlet.isHidden = false
            self.productDescription.addConstraint(self.descriptionHeightConstraint)
            //self.showFullDescription = true
        }
        else {
            self.showFullDescriptionOutlet.isHidden = true
            //self.showFullDescription = false
        }


        if (self.productInfoDictionary["question_answer"] as! NSArray).count == 0 {
            self.questionAnswerTable.isHidden = true
            self.allQuestionAndAnswerButtonOutlet.isHidden = true
            self.noQuestionsPostedLBL.isHidden = false
        }
        else if (self.productInfoDictionary["question_answer"] as! NSArray).count > 4 {
            self.questionAnswerTable.isHidden = false
            self.allQuestionAndAnswerButtonOutlet.isHidden = false
            self.noQuestionsPostedLBL.isHidden = true
        }
        else {
            self.questionAnswerTable.isHidden = false
            self.allQuestionAndAnswerButtonOutlet.isHidden = true
            self.noQuestionsPostedLBL.isHidden = true
        }


        self.sellerName.text = "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["name"] ?? "")"
        self.sellerImageview.sd_setImage(with: URL(string: "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["user_image"] ?? "")"))


        if "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["favourite_status"] ?? "")".elementsEqual("0") {
            self.followLBL.text = "Follow"
        }
        else {
            self.followLBL.text = "Unfollow"
        }




        let feedbackString = "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["positive_feedback"] ?? "")" + "%"
        self.feedback.halfTextColorChange(fullText: (feedbackString + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "positivefeedback", comment: "")), changeText: feedbackString, color: UIColor.yellow2())
        self.memberSince.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "memberSince", comment: "") + ": " + "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["member_since"] ?? "")"
        self.contactNo.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "contact", comment: "") + ": " + "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["mobile_no"] ?? "")"
        self.location.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "location", comment: "") + ": " + "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["location"] ?? "")"

        if "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["total_rating"] ?? "")".elementsEqual("") {
            self.totalRatingStarImageview.isHidden = true
        }
        else {
            self.totalRatingStarImageview.isHidden = false
        }
        self.totalRating.text = "\((self.productInfoDictionary["seller_details"] as! NSDictionary)["total_rating"] ?? "")"

        if (self.productInfoDictionary["selelr_others_listing"] as! NSArray).count > 0 {
            self.sellerOtherListingsCollectionview.isHidden = false
            self.noOtherListingsLBL.isHidden = true
        }
        else {
            self.sellerOtherListingsCollectionview.isHidden = true
            self.noOtherListingsLBL.isHidden = false
        }

        // Profile Type

        if langID.elementsEqual("AR")
        {
            if "\((productInfoDictionary["seller_details"] as! NSDictionary)["user_type_status"]!)".elementsEqual("B") {
                self.accountTypeImageview.image = UIImage(named: "button-2")
                self.AccountTypeLbl.text = "Business Account"
            }
            else {
                self.accountTypeImageview.image = UIImage(named: "button-2")
                self.AccountTypeLbl.text = "Private Account"
            }
        }
        else
        {
            if "\((productInfoDictionary["seller_details"] as! NSDictionary)["user_type_status"]!)".elementsEqual("B") {
                self.accountTypeImageview.image = UIImage(named: "button-1")
                self.AccountTypeLbl.text = "Business Account"
            }
            else {
                self.accountTypeImageview.image = UIImage(named: "button-1")
                self.AccountTypeLbl.text = "Private Account"
            }
        }




        // Profile Status

        if "\((productInfoDictionary["seller_details"] as! NSDictionary)["bonus_status"]!)".elementsEqual("1")
        {
            self.bonusView.isHidden = false
            //cell.bonusImageview.sd_setImage(with: URL(string: "\((productInfoDictionary["seller_info"] as! NSDictionary)["bonus_status_logo"]!)"))
            self.bonusLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bonus", comment: "")

        }
        else
        {
            self.bonusView.isHidden = true
        }

        if "\((productInfoDictionary["seller_details"] as! NSDictionary)["user_verified_status"]!)".elementsEqual("V")
        {
            self.verifiedView.isHidden = false
            //cell.verifiedImageview.sd_setImage(with: URL(string: "\((productInfoDictionary["seller_info"] as! NSDictionary)["verified_status_logo"]!)"))
            self.verifiedLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "verified", comment: "")

        }
        else
        {
            self.verifiedView.isHidden = true
        }

        if "\((productInfoDictionary["seller_details"] as! NSDictionary)["marrof_status"]!)".elementsEqual("1")
        {
            self.maroofView.isHidden = false
            //cell.maroofImageview.sd_setImage(with: URL(string: "\((productInfoDictionary["seller_info"] as! NSDictionary)["marrof_status_logo"]!)"))
            self.maroofLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "maroof", comment: "")

        }
        else
        {
            self.maroofView.isHidden = true
        }



        // Product Type Checking

        if "\(self.productInfoDictionary["product_type_status"]!)".elementsEqual("A") {
            self.auctionView.isHidden = false
            self.otherProducttypeView.isHidden = true

            // BID

            if ((self.productInfoDictionary["bid_content"] as! NSDictionary)["bid_history"] as! NSArray).count > 0 {
                self.currentBid.text = "\((((self.productInfoDictionary["bid_content"] as! NSDictionary)["bid_history"] as! NSArray)[0] as! NSDictionary)["price"] ?? "")"

                self.bidHistoryStackview.isHidden = false

                self.totalBids.text = "Total Bids: " + "\(((self.productInfoDictionary["bid_content"] as! NSDictionary)["count_bid"] ?? ""))"
            }
            else {
                self.currentBid.text = "No Bids Yet"

                self.bidHistoryStackview.isHidden = true

                self.totalBids.text = "Total Bids: 0"
            }


        }
        else {
            self.auctionView.isHidden = true
            self.otherProducttypeView.isHidden = false

            self.investmentType.text = "\(self.productInfoDictionary["realestate_invest_type"] ?? "")"
            self.productType.text = "\(self.productInfoDictionary["product_type"] ?? "")"
            self.weeklyPrice.text = "Weekly Price  " + "\(self.productInfoDictionary["currency_symbol"] ?? "")" + "\(self.productInfoDictionary["start_price"] ?? "")"
            self.monthlyPrice.text = "Monthly Price  " + "\(self.productInfoDictionary["currency_symbol"] ?? "")" + "\(self.productInfoDictionary["reserve_price"] ?? "")"
            self.yearlyPrice.text = "Yearly Price  " + "\(self.productInfoDictionary["currency_symbol"] ?? "")" + "\(self.productInfoDictionary["yearly_price"] ?? "")"

            if "\(self.productInfoDictionary["start_price"]!)".elementsEqual("0.00") {
                self.weeklyPriceView.isHidden = true
            }
            if "\(self.productInfoDictionary["reserve_price"]!)".elementsEqual("0.00") {
                self.monthlyPriceView.isHidden = true
            }
            if "\(self.productInfoDictionary["yearly_price"]!)".elementsEqual("0.00") {
                self.yearlyPriceView.isHidden = true
            }
        }



        // BReadcrumb Bar




//        let breadcrumbbarString = "\(self.productInfoDictionary["breadcrumb_bar"] ?? "")".replacingOccurrences(of: ",", with: " / ") + " / \(self.productInfoDictionary["product_name"] ?? "")"
//
//        self.breadcrumbBar.halfTextColorChange(fullText: breadcrumbbarString, changeText: "\(self.productInfoDictionary["product_name"] ?? "")", color: .black)

        let BreadCrumpString = "\(self.productInfoDictionary["breadcrumb_bar"]!)"

        let BRStr = BreadCrumpString.replacingOccurrences(of: ",", with: "/", options: .literal, range: nil) + "/"
        let BreadCrumbArray = BRStr.components(separatedBy: "/")
        print("Array",BreadCrumbArray)

        let reversedNames : [String] = Array(BreadCrumbArray.reversed())

        print("ReversedArray",reversedNames)

        if langID.elementsEqual("AR")
        {
            let str = reversedNames.joined(separator: "/")
            print("String",str)
            self.breadcrumbBar.halfTextColorChange(fullText: ( "\(self.productInfoDictionary["product_name"]!)" + str), changeText: "\(self.productInfoDictionary["product_name"]!)", color: .black())
        }
        else
        {
            self.breadcrumbBar.halfTextColorChange(fullText: (BRStr + "\(self.productInfoDictionary["product_name"]!)" ), changeText: "\(self.productInfoDictionary["product_name"]!)", color: .black())
        }

    }



    // MARK:- // Set Table Height

    func SetTableheight(table: UITableView , heightConstraint: NSLayoutConstraint) {

        var setheight: CGFloat  = 0
        table.frame.size.height = 3000

        for cell in table.visibleCells {
            setheight += cell.bounds.height
        }

        heightConstraint.constant = CGFloat(setheight)
    }


    // MARK:- // Set Font

    func setFont() {

        self.listedOn.font = UIFont(name: self.listedOn.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        self.closingIn.font = UIFont(name: self.closingIn.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        self.productName.font = UIFont(name: self.productName.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
        self.productAddress.font = UIFont(name: self.productAddress.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.reportLBL.font = UIFont(name: self.productName.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        self.viewsLBL.font = UIFont(name: self.viewsLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        self.watchlistLBL.font = UIFont(name: self.watchlistLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        self.listingLBL.font = UIFont(name: self.listingLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.listingNo.font = UIFont(name: self.listingNo.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))

        self.followLBL.font = UIFont(name: self.followLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        self.descriptionTitleLBL.font = UIFont(name: self.descriptionTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        self.productDescription.font = UIFont(name: self.productDescription.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.showFullDescriptionOutlet.titleLabel!.font = UIFont(name: self.showFullDescriptionOutlet.titleLabel!.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.aboutThePropertyTitleLBL.font = UIFont(name: self.aboutThePropertyTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        self.questionAnswerTitleLBL.font = UIFont(name: self.questionAnswerTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        self.noQuestionsPostedLBL.font = UIFont(name: self.noQuestionsPostedLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        self.allQuestionAndAnswerButtonOutlet.titleLabel!.font = UIFont(name: self.allQuestionAndAnswerButtonOutlet.titleLabel!.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.postedByTitleLBL.font = UIFont(name: self.postedByTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        self.sellerName.font = UIFont(name: self.sellerName.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.feedback.font = UIFont(name: self.feedback.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.totalRating.font = UIFont(name: self.totalRating.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.memberSince.font = UIFont(name: self.memberSince.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.contactNo.font = UIFont(name: self.contactNo.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.location.font = UIFont(name: self.location.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.sendMessageButtonOutlet.titleLabel!.font = UIFont(name: self.sendMessageButtonOutlet.titleLabel!.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        self.viewAccountButtonOutlet.titleLabel!.font = UIFont(name: self.viewAccountButtonOutlet.titleLabel!.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))

        self.reviewsTitleLBL.font = UIFont(name: self.reviewsTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        self.noReviewsLBL.font = UIFont(name: self.noReviewsLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))

        self.sellerOtherListingsTitleLBL.font = UIFont(name: self.sellerOtherListingsTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        self.noOtherListingsLBL.font = UIFont(name: self.noOtherListingsLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))


        self.currentBidTitleLBL.font = UIFont(name: self.currentBidTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
        self.totalBids.font = UIFont(name: self.totalBids.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
        self.currentBid.font = UIFont(name: self.currentBid.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
        self.nextBidAmountLBL.font = UIFont(name: self.nextBidAmountLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
        self.placeBidButtonOutlet.titleLabel!.font = UIFont(name: self.placeBidButtonOutlet.titleLabel!.font.fontName, size: CGFloat(self.Get_fontSize(size: 17)))
        self.termsAndConditionsButtonOutlet.titleLabel!.font = UIFont(name: self.termsAndConditionsButtonOutlet.titleLabel!.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        self.showBidHistoryButtonOutlet.titleLabel!.font = UIFont(name: self.showBidHistoryButtonOutlet.titleLabel!.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
        self.investmentType.font = UIFont(name: self.investmentType.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.productType.font = UIFont(name: self.productType.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.weeklyPrice.font = UIFont(name: self.weeklyPrice.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.monthlyPrice.font = UIFont(name: self.monthlyPrice.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        self.yearlyPrice.font = UIFont(name: self.yearlyPrice.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))





    }
    
    
    
    // MARK:- // Change Language Strings
    
    func changeLanguageStrings() {
        
        self.reportLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "report", comment: "")
        self.browseCategoryTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "browseCategory", comment: "")
        self.descriptionTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "description", comment: "")
        self.aboutThePropertyTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "aboutTheProperty", comment: "")
        self.questionAnswerTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "questionsAndAnswers", comment: "")
        self.reviewsTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "reviewAndRating", comment: "")
        self.noReviewsLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "noReviewsFound", comment: "")
        self.postedByTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "posted_by", comment: "")
        self.followLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "follow", comment: "")
    self.viewAccountButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "viewAccount", comment: ""), for: .normal)
    self.sendMessageButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sendMessage", comment: ""), for: .normal)
        self.sellerOtherListingsTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "sellerOtherListing", comment: "")
        
        
    }


}


// MARK:- // Collectionview Delegates

extension NewRealEstateDetailsViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView == sliderCollectionview {
            let count = (self.productInfoDictionary["product_image"] as! NSArray).count

            sliderPagecontrol.numberOfPages = count
            //sliderPagecontrol.isHidden = !(count > 1)

            return count
        }
        else if collectionView == self.showImageCollectionview {
            return (self.productInfoDictionary["product_image"] as! NSArray).count
        }
        else {
            return (self.productInfoDictionary["selelr_others_listing"] as! NSArray).count
        }



    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if collectionView == sliderCollectionview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "slider", for: indexPath) as! SliderCollectionViewCell

            cell.cellImage.sd_setImage(with: URL(string: "\(((self.productInfoDictionary["product_image"] as! NSArray)[indexPath.item]))"))

            return cell
        }
        else if collectionView == self.showImageCollectionview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "showImage", for: indexPath) as! showImageviewCVC
            
            cell.cellImage.sd_setImage(with: URL(string: "\(((self.productInfoDictionary["product_image"] as! NSArray)[indexPath.item]))"))
            
            cell.cellView.backgroundColor = UIColor.clear
            cell.cellImage.backgroundColor = UIColor.clear
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "otherListings", for: indexPath) as! newRealEstateSellerOtherListingsCVC

            cell.PropertyImage.sd_setImage(with: URL(string: "\((((self.productInfoDictionary["selelr_others_listing"] as! NSArray)[indexPath.item] as! NSDictionary)["photo"] ?? ""))"))
            cell.PropertyName.text = "\((((self.productInfoDictionary["selelr_others_listing"] as! NSArray)[indexPath.item] as! NSDictionary)["product_name"] ?? ""))"

            cell.address.text = "\(((self.productInfoDictionary["selelr_others_listing"] as! NSArray)[indexPath.item] as! NSDictionary)["address"]!)"


            cell.PropertyListingTime.text = "\(((self.productInfoDictionary["selelr_others_listing"] as! NSArray)[indexPath.item] as! NSDictionary)["listing_time"]!)" + " ago"
            cell.PropertyClosingTime.text = "\(((self.productInfoDictionary["selelr_others_listing"] as! NSArray)[indexPath.item] as! NSDictionary)["expiry_time"]!)"
            cell.PropertySize.text = "\(((self.productInfoDictionary["selelr_others_listing"] as! NSArray)[indexPath.item] as! NSDictionary)["total_square_m"]!)" + "Sq-m"
            cell.PropertyOwner.text = "\(((self.productInfoDictionary["selelr_others_listing"] as! NSArray)[indexPath.item] as! NSDictionary)["seller_name"]!)"
            cell.PropertyType.text = "\(((self.productInfoDictionary["selelr_others_listing"] as! NSArray)[indexPath.item] as! NSDictionary)["product_type"]!)"
            cell.PropertyPrice.text = "\(((self.productInfoDictionary["selelr_others_listing"] as! NSArray)[indexPath.item] as! NSDictionary)["currency_symbol"]!)" + "\(((self.productInfoDictionary["selelr_others_listing"] as! NSArray)[indexPath.item] as! NSDictionary)["start_price"]!)"


            // SEt Font
            cell.address.font = UIFont(name: cell.address.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.PropertyName.font = UIFont(name: cell.PropertyName.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
            cell.PropertySize.font = UIFont(name: cell.PropertySize.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.PropertyListingTime.font = UIFont(name: cell.PropertyListingTime.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.PropertyClosingTime.font = UIFont(name: cell.PropertyClosingTime.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.PropertyOwner.font = UIFont(name: cell.PropertyOwner.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            //cell.PropertyAddress.font = UIFont(name: cell.PropertyAddress.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            cell.PropertyType.font = UIFont(name: cell.PropertyType.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
            cell.PropertyPrice.font = UIFont(name: cell.PropertyPrice.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))

            return cell
        }



    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if collectionView == sliderCollectionview {
            return CGSize(width: self.FullWidth, height: 220)
        }
        else if collectionView == self.showImageCollectionview {
            return CGSize(width: self.FullWidth, height: self.showImageCollectionview.frame.size.height)
        }
        else {
            return CGSize(width: (self.FullWidth - 30)/2, height: 300)
        }


    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0

    }



    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if collectionView == self.sliderCollectionview {

            self.showImageview.isHidden = false
            self.view.bringSubviewToFront(self.showImageview)
            
            let toIndex: IndexPath = IndexPath(item: indexPath.item, section: 0)
            
            self.showImageCollectionview.scrollToItem(at: toIndex, at: .left, animated: true)
            
            
            
        }

    }




}


// MARK:- // Scrollview Delegates

extension NewRealEstateDetailsViewController: UIScrollViewDelegate {

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        sliderPagecontrol?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {

        sliderPagecontrol?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }


}



// MARK:- // Tableview Delegates

extension NewRealEstateDetailsViewController: UITableViewDelegate,UITableViewDataSource {


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == self.aboutThePropertyTable {
            return (self.productInfoDictionary["product_option"] as! NSArray).count
        }
        else if tableView == questionAnswerTable {
            return (self.productInfoDictionary["question_answer"] as! NSArray).count
        }
        else {
            return ((self.productInfoDictionary["bid_content"] as! NSDictionary)["bid_history"] as! NSArray).count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == self.aboutThePropertyTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "aboutTheProperty") as! AboutThePropertyTableViewCell

            cell.cellKey.text = "\(((self.productInfoDictionary["product_option"] as! NSArray)[indexPath.row] as! NSDictionary)["option_label"] ?? "")"
            cell.cellValue.text = "\(((self.productInfoDictionary["product_option"] as! NSArray)[indexPath.row] as! NSDictionary)["option_value"] ?? "")"

            // Set Font

            cell.cellKey.font = UIFont(name: cell.cellKey.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
            cell.cellValue.font = UIFont(name: cell.cellValue.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }
        else if tableView == questionAnswerTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "qna") as! RealEstateQNATableViewCell

            cell.question.text = "\(((self.productInfoDictionary["question_answer"] as! NSArray)[indexPath.row] as! NSDictionary)["question"] ?? "")"
            cell.askedBy.text = "\(((self.productInfoDictionary["question_answer"] as! NSArray)[indexPath.row] as! NSDictionary)["qusn_user"] ?? "")" + " (\(((self.productInfoDictionary["question_answer"] as! NSArray)[indexPath.row] as! NSDictionary)["total_rating"] ?? ""))"
            cell.askedOn.text = "\(((self.productInfoDictionary["question_answer"] as! NSArray)[indexPath.row] as! NSDictionary)["question_date"] ?? "")"

            if "\(((self.productInfoDictionary["question_answer"] as! NSArray)[indexPath.row] as! NSDictionary)["answer"] ?? "")".elementsEqual("") {
                cell.answer.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "notAnsweredYet", comment: "")
            }
            else {
                cell.answer.text = "\(((self.productInfoDictionary["question_answer"] as! NSArray)[indexPath.row] as! NSDictionary)["answer"] ?? "")"
            }

            cell.answeredOn.text = "\(((self.productInfoDictionary["question_answer"] as! NSArray)[indexPath.row] as! NSDictionary)["reply_date"] ?? "")"


            cell.qBackgroundView.backgroundColor = UIColor.yellow2()
            cell.qTitleLBL.textColor = .white
            cell.qBackgroundView.layer.cornerRadius = 5

            // Set Font

            cell.qTitleLBL.font = UIFont(name: cell.qTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
            cell.question.font = UIFont(name: cell.question.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
            cell.askedBy.font = UIFont(name: cell.askedBy.font.fontName, size: CGFloat(self.Get_fontSize(size: 10)))
            cell.askedOn.font = UIFont(name: cell.askedOn.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            cell.aTitleLBL.font = UIFont(name: cell.aTitleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
            cell.answer.font = UIFont(name: cell.answer.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
            cell.answeredOn.font = UIFont(name: cell.answeredOn.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "bidHistory") as! bidHistoryTVC

            cell.bidDate.text = "\((((self.productInfoDictionary["bid_content"] as! NSDictionary)["bid_history"] as! NSArray)[indexPath.row] as! NSDictionary)["date"] ?? "")"

            cell.bidAmount.text = "\((((self.productInfoDictionary["bid_content"] as! NSDictionary)["bid_history"] as! NSArray)[indexPath.row] as! NSDictionary)["price"] ?? "")"

            cell.bidderName.text = "\((((self.productInfoDictionary["bid_content"] as! NSDictionary)["bid_history"] as! NSArray)[indexPath.row] as! NSDictionary)["name"] ?? "")"

            cell.bidTime.text = "\((((self.productInfoDictionary["bid_content"] as! NSDictionary)["bid_history"] as! NSArray)[indexPath.row] as! NSDictionary)["time"] ?? "")"

            //setFont
            cell.bidDate.font = UIFont(name: cell.bidDate.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
            cell.bidAmount.font = UIFont(name: cell.bidAmount.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            cell.bidderName.font = UIFont(name: cell.bidderName.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))
            cell.bidTime.font = UIFont(name: cell.bidTime.font.fontName, size: CGFloat(self.Get_fontSize(size: 11)))


            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }


    }




}



// MARK:- // Textview Delegates

extension NewRealEstateDetailsViewController: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {

        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = .black
        }


    }

}





// MARK:- // Slider Collectionview Cell

class SliderCollectionViewCell: UICollectionViewCell {


    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellImage: UIImageView!



}




// MARK:- // About the Property TableviewCell

class AboutThePropertyTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellKey: UILabel!
    @IBOutlet weak var cellValue: UILabel!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}





// MARK:- // New Real Estate QNA Tableview Cell

class RealEstateQNATableViewCell: UITableViewCell {

    @IBOutlet weak var qBackgroundView: UIView!
    @IBOutlet weak var qTitleLBL: UILabel!
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var askedBy: UILabel!
    @IBOutlet weak var askedOn: UILabel!
    @IBOutlet weak var aTitleLBL: UILabel!
    @IBOutlet weak var answer: UILabel!
    @IBOutlet weak var answeredOn: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}





// MARK:- // Seller Other Listings Collectionview Cell

class newRealEstateSellerOtherListingsCVC: UICollectionViewCell {


    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var PropertyImage: UIImageView!
    @IBOutlet weak var PropertyName: UILabel!
    @IBOutlet weak var PropertySize: UILabel!
    @IBOutlet weak var PropertyListingTime: UILabel!
    @IBOutlet weak var PropertyClosingTime: UILabel!
    @IBOutlet weak var PropertyOwner: UILabel!
    @IBOutlet weak var PropertyType: UILabel!
    @IBOutlet weak var PropertyPrice: UILabel!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var placeholderImageview: UIImageView!
    @IBOutlet weak var address: UILabel!

}




// MARK:- // Bid History Tableview Cell

class bidHistoryTVC: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var bidDate: UILabel!
    @IBOutlet weak var bidAmount: UILabel!
    @IBOutlet weak var bidTime: UILabel!
    @IBOutlet weak var bidderName: UILabel!


}



// MARK:- // Show Imageview Collectionview Cell

class showImageviewCVC: UICollectionViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellImage: UIImageView!
    
    
}



