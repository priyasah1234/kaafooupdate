
//
//  NewRealEstateSortViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 6/11/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

struct AdvanceSort {
    var TypeArray : NSMutableArray!
    var Clicked : Bool!
    var isExpandable : Bool!

    init(TypeArray : NSMutableArray, Clicked:Bool , isExpandable : Bool) {
        self.TypeArray = TypeArray
        self.Clicked = Clicked
        self.isExpandable = isExpandable
    }
}

class NewRealEstateSortViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var SortTableView: UITableView!
    var SortArray : NSMutableArray! = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        print("SortArray",SortArray!)
        self.SetCustom()
        // Do any additional setup after loading the view.
    }
    //MARK:- //Tableview Delegate and DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.SortArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "sortcell") as! NewRealEstateSortTableViewCell
        obj.SortName.text = "\((self.SortArray[indexPath.row] as! NSDictionary)["option_name"]!)"
        return obj
    }
    func SetCustom()
    {
        self.SortTableView.delegate = self
        self.SortTableView.dataSource = self
        self.SortTableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class NewRealEstateSortTableViewCell : UITableViewCell
{
    @IBOutlet weak var SortName: UILabel!
    @IBOutlet weak var SortBtn: UIButton!
    @IBOutlet weak var SeparatorView: UIView!

}
