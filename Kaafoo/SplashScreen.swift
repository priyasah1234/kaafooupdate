//
//  SplashScreen.swift
//  Kaafoo
//
//  Created by admin on 28/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import CoreLocation

class SplashScreen: GlobalViewController,CLLocationManagerDelegate {
    
    @IBOutlet weak var splashImageview: UIImageView!
    

    var locManager = CLLocationManager()
    
    var currentLocation: CLLocation!
   
    
    let dispatchGroup = DispatchGroup()
    
    let lastLoggedEmail = UserDefaults.standard.string(forKey: "lastLoggedEmail")
    let lastLoggedPassword = UserDefaults.standard.string(forKey: "lastLoggedPassword")
    var signInResponseDict : NSMutableDictionary!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        locManager.delegate = self
        
        globalDispatchgroup.enter()
        
        locManager.requestWhenInUseAuthorization()
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            locManager.delegate = self
            
            locManager.startUpdatingLocation()
            
            currentLocation = locManager.location
            
            print("Latitude",LocationDetailsParameters.shared.LocationLatitude!)
//            if "\(currentLocation.coordinate.latitude)".elementsEqual("")
//            {
//
//            }
//            else
//            {
//                LocationDetailsParameters.shared.setCurrentLatitude(latitude: "\(currentLocation.coordinate.latitude)")
//            }
//            
//            if "\(currentLocation.coordinate.longitude)".elementsEqual("")
//            {
//
//            }
//            else
//            {
//                LocationDetailsParameters.shared.setCurrentLatitude(latitude: "\(currentLocation.coordinate.longitude)")
//            }
            
            
        }
        
        
        globalDispatchgroup.leave()
        
        
        
        
        //UserDefaults.standard.set("INDIA", forKey: "myCountryName")
        
        //let langId: String? = NSLocale.preferredLanguages.first
        let langID : String? = LocalizationSystem.sharedInstance.getLanguage()
        
        UserDefaults.standard.set(langID, forKey: "DeviceLanguageID")
        
        print("Device Language Code : ",langID!)
        
        if (langID?.elementsEqual("en"))!
        {
            splashImageview.image = UIImage(named: "splashEnglish")
            UserDefaults.standard.set("EN", forKey: "langID")
            UserDefaults.standard.set("ENGLISH", forKey: "langNAME")
            UserDefaults.standard.set("Main", forKey: "storyboard")
            
            UserDefaults.standard.set("categoryname_en", forKey: "categoryName")
        }
        else if (langID?.elementsEqual("ar"))!
        {
            splashImageview.image = UIImage(named: "splashArabic")
            UserDefaults.standard.set("AR", forKey: "langID")
             UserDefaults.standard.set("ARABIC", forKey: "langNAME")
            UserDefaults.standard.set("Arabic", forKey: "storyboard")
        }
        else
        {
            splashImageview.image = UIImage(named: "splashEnglish")
            UserDefaults.standard.set("EN", forKey: "langID")
             UserDefaults.standard.set("ENGLISH", forKey: "langNAME")
            UserDefaults.standard.set("Main", forKey: "storyboard")
        }
        
        //UserDefaults.standard.set("INDIA", forKey: "myCountryName")
        UserDefaults.standard.set("1", forKey: "isGuest")
        
        perform(#selector(SplashScreen.autoLogin), with: nil, afterDelay: 1)

        
    }
    
    
    // MARK:- // Navigate to Home
    
    func navigateToHome()
    {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    // MARK:- // Navigate to Preferences
    
    func navigateTopreferences() {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "preferenceVC") as! PreferenceViewController
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Login
    
    @objc func autoLogin()
    {

        let userDefaultsStatus = isKeyPresentInUserDefaults(key: "userID")
        
        if userDefaultsStatus == true
        {
            let userID = UserDefaults.standard.string(forKey: "userID")
            
            if (userID?.isEmpty)!
            {
                self.navigateToHome()
            }
            else
            {
                self.loginAPI()
            }
        }
        else
        {
            if isKeyPresentInUserDefaults(key: "myCountryName") == true {
                self.navigateToHome()
            }
            else {
                self.navigateTopreferences()
            }
        }
        
//
//        if (lastLoggedEmail?.isEmpty)!
//        {
//            
//        }
//        else
//        {
//
//        }
        
    }
    
    
    // MARK:- // Login API
    
    //func loginAPI(completion : @escaping () -> ())
    func loginAPI()
    {
        let loginType = "I"
        
        let deviceToken = UserDefaults.standard.string(forKey: "DeviceToken")
 
        dispatchGroup.enter()
        
        let url = URL(string: GLOBALAPI + "app_login")!   //change the url
        
        print("Login URL : ", url)
        
        var parameters : String = ""
        
        parameters = "login_email=\(lastLoggedEmail!)&login_password=\(lastLoggedPassword!)&login_type=\(loginType)&device_token=\(deviceToken!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
//                    print("Login Response: " , json)
                    
                    self.signInResponseDict = json.mutableCopy() as? NSMutableDictionary
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        self.dispatchGroup.leave()

                    }
                    else
                    {
                        
                        
                        UserDefaults.standard.set("\((self.signInResponseDict["info_array"] as! NSDictionary)["logo_img"]!)", forKey: "userImageURL")
                        
                        
                        let tempArray = ((json["info_array"] as! NSDictionary)["basic_info"] as! NSArray)
                        
                        UserDefaults.standard.set("\((tempArray[0] as! NSDictionary)["usertype"]!)", forKey: "userType")
                        
                        let EmailID = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["basic_info"] as! NSArray)[0] as! NSDictionary)["emailid"]!))"
                        
                        UserDefaults.standard.set(EmailID, forKey: "EmailID")
                        
                        if "\((tempArray[0] as! NSDictionary)["usertype"]!)".elementsEqual("B")
                        {
                            let PhoneNo = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["business_info"] as! NSArray)[0] as! NSDictionary)["mobile_num"]!))"
                            
                            UserDefaults.standard.set(PhoneNo, forKey: "PhoneNo")
                            
                            let userDisplayName = "\((((json["info_array"] as! NSDictionary)["business_info"] as! NSArray)[0] as! NSDictionary)["displayname"]!)"
                            
                            UserDefaults.standard.set(userDisplayName, forKey: "userDisplayName")
                            
                            //NAME SPLITING
                            
                            var components = userDisplayName.components(separatedBy: " ")
                            
                            if(components.count > 0)
                                
                            {
                                
                                let FirstName = components.removeFirst()
                                
                                let LastName = components.joined(separator: " ")
                                
                                print("FIRSTNAME",FirstName)
                                
                                print("LASTNAME",LastName)
                                
                                UserDefaults.standard.set(FirstName, forKey: "FirstName")
                                
                                UserDefaults.standard.set(LastName, forKey: "LastName")
                                
                                print("FIRSTANDLASTBUsiness",FirstName,LastName)
                                
                            }
                        }
                        else
                        {
                            let userDisplayName = "\((((json["info_array"] as! NSDictionary)["private_info"] as! NSArray)[0] as! NSDictionary)["display_name"]!)"
                            
                            UserDefaults.standard.set(userDisplayName, forKey: "userDisplayName")
                            
                            let PhoneNo = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["private_info"] as! NSArray)[0] as! NSDictionary)["mobilenum"]!))"
                            
                            UserDefaults.standard.set(PhoneNo, forKey: "PhoneNo")
                            
                            let FirstName  = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["private_info"] as! NSArray)[0] as! NSDictionary)["fname"]!))"
                            
                            let LastName  = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["private_info"] as! NSArray)[0] as! NSDictionary)["lname"]!))"
                            
                            UserDefaults.standard.set(FirstName, forKey: "FirstName")
                            
                            UserDefaults.standard.set(LastName, forKey: "LastName")
                            
                            print("FIRSTANDLASTPrivate",FirstName,LastName)
                        }
                        
                        self.dispatchGroup.leave()
                        
                        self.dispatchGroup.notify(queue: .main, execute: {
                            
                            self.checkLoginConditions()
                            
                        })
                        
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }

    
    // MARK:- // Login Condition Checks
    
    func checkLoginConditions()
    {
    
    
    let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
    
    
    if "\(self.signInResponseDict["response"]!)".elementsEqual("1")
    
    {
    
    print("Storyboard Name----",storyboardName!)
    
    //let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "newHomeVC") as! NewHomeViewController
    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
    
    let userID = "\(((((self.signInResponseDict["info_array"] as! NSDictionary)["basic_info"] as! NSArray)[0] as! NSDictionary)["id"]!))"
    //
    let langID = UserDefaults.standard.string(forKey: "langID")
    //
    UserDefaults.standard.set(userID, forKey: "userID")
    
    UserDefaults.standard.set("0", forKey: "isGuest")
    
    print("User ID is : ", userID)
    print("Lang ID is : ", langID!)
    print("Storyboard is : ",storyboardName!)
    
    self.navigationController?.pushViewController(navigate, animated: true)
    }
    else if "\(self.signInResponseDict["message"]!)".elementsEqual("Email and password does not match")
    {
    let alert = UIAlertController(title: "Login Invalid", message: "Email and Password does not match.", preferredStyle: .alert)
    
    let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
    
    alert.addAction(ok)
    
    self.present(alert, animated: true, completion: nil )
    }
    
    else if "\(self.signInResponseDict["response"]!)".elementsEqual("0")
    {
    
//    let navigate = UIStoryboard(name: storyboardName!, bundle: nil).instantiateViewController(withIdentifier: "activationVC") as! ActivationViewController
        
     let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "activationVC") as! ActivationViewController
    
    navigate.newUserID = "\(self.signInResponseDict["new_userid"]!)"
    
    self.navigationController?.pushViewController(navigate, animated: true)
    
    }
    
    }
    
    

}
