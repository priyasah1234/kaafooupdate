//

//  BonusRequestViewController.swift

//  Kaafoo

//

//  Created by IOS-1 on 15/02/19.

//  Copyright © 2019 ESOLZ. All rights reserved.

//



import UIKit

import SVProgressHUD
class BonusRequestViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    var BonusReqArray : NSMutableArray!
    var recordsArray : NSMutableArray! = NSMutableArray()
    var nextStart : String!
    var startValue = 0
    var perLoad = 10
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    @IBOutlet weak var BonusReqTableView: UITableView!
    var bonusRequestId : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetBonusRequest()
//        self.globalDispatchgroup.notify(queue: .main, execute: {
//            if self.recordsArray.count == 0
//            {
//                self.BonusReqTableView.isHidden = true
//                self.noDataFound(mainview: self.view)
//                self.ShowAlertMessage(title: "Warning", message: "No Information Found")
//            }
//            else
//            {
//              print("do nothing")
//            }
//        })
        // Do any additional setup after loading the view
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.BonusReqTableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.BonusReqTableView.rowHeight = UITableView.automaticDimension
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listcell") as! BonusRequestTableViewCell
        
        // cell.date.text = "\((self.BonusReqArray[indexPath.row] as! NSMutableDictionary)["created_at"]!)"
        
        
        
        cell.name.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["name"]!)"
        
        cell.date.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["created_at"]!)"
        
        cell.Email.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["email"]!)"
        
        cell.price.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["amount"]!)"
        
        cell.status.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["approve_type"]!)"
        
        cell.ApproveBtn.tag = indexPath.row
        
        cell.RejectBtn.tag = indexPath.row
        
        cell.RejectBtn.addTarget(self, action: #selector(rejectBooking(sender:)), for: .touchUpInside)
        
        cell.ApproveBtn.addTarget(self, action: #selector(ApproveBonus(sender:)), for: .touchUpInside)
        
        
        
        bonusRequestId = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["bonus_rqst_id"]!)"
        
        
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let cell = cell as! BonusRequestTableViewCell
        
        cell.mainView.layer.borderWidth = 1.0
        
        
        
        cell.mainView.layer.borderColor = UIColor.lightGray.cgColor
        
        
        
        
        
        cell.buttonsView.layer.borderWidth = 1.0
        
        
        
        cell.buttonsView.layer.borderColor = UIColor.lightGray.cgColor
        
        
        
        if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["approve_status"]!)" == "0"
            
        {
            
            cell.buttonsView.isHidden = false
            
        }
            
        else
            
        {
            
            cell.buttonsView.isHidden = true
            
        }
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        scrollBegin = scrollView.contentOffset.y
        
    }
    
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        scrollEnd = scrollView.contentOffset.y
        
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollBegin > scrollEnd
            
        {
            
            
            
        }
            
        else
            
        {
            
            
            
//            print("next start : ",nextStart )
            
            
            
            if (nextStart).isEmpty
                
            {
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                    
                }
                
            }
                
            else
                
            {
                
                
                
                GetBonusRequest()
                
            }
            
            
            
        }
        
    }
    
    
    
    
    
    //Button API Fire
    func Reject()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&bonus_rqst_id=\(bonusRequestId!)&lang_id=\(langID!)&status=\("2")"
        self.CallAPI(urlString: "app_bonus_request_status_change", param: parameters, completion: {
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                SVProgressHUD.dismiss()
            }
        })
    }
    
    
    
    
    
    
    // MARK:- // Delete Booking
    
    
    
    
    
    @objc func rejectBooking(sender: UIButton)
        
    {
        
        
        
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        
        
        
        let ok = UIAlertAction(title: "OK", style: .default) {
            
            UIAlertAction in
            
            
            
            
            
            let tempDictionary = self.BonusReqArray[sender.tag] as! NSDictionary
            
            
            
            self.bonusRequestId = "\(tempDictionary["bonus_rqst_id"]!)"
            
            
            
            self.Reject()
            
            
            
            self.BonusReqArray.removeAllObjects()
            
            
            
            self.startValue = 0
            
            
            
            self.GetBonusRequest()
            
            
            
            
            
        }
        
        
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        
        alert.addAction(ok)
        
        alert.addAction(cancel)
        
        
        
        self.present(alert, animated: true, completion: nil)
        
        
        
        
        
    }
    
    //Bonus request approve btn
    
    @objc func ApproveBonus(sender: UIButton)

    {
        
        
        
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        
        
        
        let ok = UIAlertAction(title: "OK", style: .default) {
            
            UIAlertAction in
            
            
            
            
            
            let tempDictionary = self.BonusReqArray[sender.tag] as! NSDictionary
            
            
            
            self.bonusRequestId = "\(tempDictionary["bonus_rqst_id"]!)"
            
            
            
            self.Approve()
            
            
            
            self.BonusReqArray.removeAllObjects()
            
            
            
            self.startValue = 0
            
            
            
            self.GetBonusRequest()
            
            
            
            
            
        }
        
        
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        
        
        alert.addAction(ok)
        
        alert.addAction(cancel)
        
        
        
        self.present(alert, animated: true, completion: nil )
        
        
        
    }
    func Approve()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&bonus_rqst_id=\(bonusRequestId!)&lang_id=\(langID!)&status=\("1")"
        self.CallAPI(urlString: "app_bonus_request_status_change", param: parameters, completion: {
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                SVProgressHUD.dismiss()
            }
        })
    }
    func GetBonusRequest()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_bonus_request_info", param: parameters, completion: {
            self.BonusReqArray = self.globalJson["info_array"] as? NSMutableArray
            for i in 0...(self.BonusReqArray.count-1)
            {
                let tempDict = self.BonusReqArray[i] as! NSDictionary
                self.recordsArray.add(tempDict as! NSMutableDictionary)
            }
            self.nextStart = "\(self.globalJson["next_start"]!)"
            self.startValue = self.startValue + self.BonusReqArray.count
            print("Next Start Value : " , self.startValue)
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.BonusReqTableView.delegate = self
                self.BonusReqTableView.dataSource = self
                self.BonusReqTableView.reloadData()
                SVProgressHUD.dismiss()
            }

        })
    }
    
    /*
     
     // MARK: - Navigation
     
     
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
     // Get the new view controller using segue.destination.
     
     // Pass the selected object to the new view controller.
     
     }
     
     */
}
//MARK:- //Bonus Request Tableviewcell
class BonusRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var topSeparatorView: UIView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var AmountLbl: UILabel!
    @IBOutlet weak var StatusLbl: UILabel!
    @IBOutlet weak var Email: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var buttonsView: UIView!

    @IBOutlet weak var ApproveBtn: UIButton!
    @IBOutlet weak var RejectBtn: UIButton!


    @IBOutlet weak var StackView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
