//
//  UsersHolidayServiceVC.swift
//  Kaafoo
//
//  Created by esolz on 12/12/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class UsersHolidayServiceVC: GlobalViewController {
    
    //MARK:- //Outlet Connections
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var ScrollContentView: UIView!
    @IBOutlet weak var DataTableView: UITableView!
    @IBOutlet weak var DataTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var SubmitBtnOutlet: UIButton!
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    var HolidayServiceList : NSMutableArray! = NSMutableArray()
    var CloseByHoliday : String! = ""
    var titleCommaSeparated : String! = ""
    var descriptionCommaSeparated : String! = ""
    var titleArray = [String]()
    var descriptionArray = [String]()
    
    @IBAction func SubmitBtn(_ sender: UIButton) {
        self.SaveDetails()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getListing()

        // Do any additional setup after loading the view.
    }
    
    
    //MARK:- //View will appear method
    
    override func viewWillAppear(_ animated: Bool) {
        self.setLayer()
        self.DataTableView.isScrollEnabled = false
        
    }
    
    //MARK:- // Set Layer
    
    func setLayer()
    {
        self.SubmitBtnOutlet.layer.cornerRadius = self.SubmitBtnOutlet.frame.size.height / 2
        self.SubmitBtnOutlet.clipsToBounds = true
    }
    
    //MARK:- //Set Table Height

       func SetTableheight(table: UITableView , heightConstraint: NSLayoutConstraint) {
           var setheight: CGFloat  = 0
           table.frame.size.height = 3000
           for cell in table.visibleCells {
               setheight += cell.bounds.height
           }
           heightConstraint.constant = CGFloat(setheight)
       }
    
    //MARK:- //Get Listing
    
    func getListing()
    {
        let parameters = "user_id=\(userID ?? "")&lang_id=\(LangID ?? "")"
        self.CallAPI(urlString: "app_user_holidayservice_list", param: parameters, completion: {
            self.globalDispatchgroup.leave()
            self.HolidayServiceList = ((self.globalJson["info_array"] as! NSDictionary)["holiday_service_list"] as! NSMutableArray)
            self.CloseByHoliday = "\((self.globalJson["info_array"] as! NSDictionary)["close_by_holiday"] ?? "")"
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.DataTableView.delegate = self
                    self.DataTableView.dataSource = self
                    self.DataTableView.reloadData()
                    self.SetTableheight(table: self.DataTableView, heightConstraint: self.DataTableViewHeightConstraint)
                }
            })
        })
    }
    
    //MARK:- // Generate Title and Description
    
    func GenerateTitleDesc()
    {
        for i in 0..<self.HolidayServiceList.count
        {
            let tempDict = self.HolidayServiceList[i] as! NSDictionary
            self.titleArray.append("\(tempDict["id"] ?? "")")
            self.descriptionArray.append("\(tempDict["description"] ?? "")")
        }
        
        self.titleCommaSeparated = self.titleArray.joined(separator: ",")
        self.descriptionCommaSeparated = self.descriptionArray.joined(separator: ",")
    }
    
    //MARK:- //Save Details with Title and description
    
    func SaveDetails()
    {
        self.GenerateTitleDesc()
        let parameters = "user_id=\(userID ?? "")&lang_id=\(LangID ?? "")&close_by_det=\(self.CloseByHoliday ?? "")&title[]=\(self.titleCommaSeparated ?? "")&description[]=\(self.descriptionCommaSeparated ?? "")"
        self.CallAPI(urlString: "app_user_holidayservice_save", param: parameters, completion: {
            self.globalDispatchgroup.leave()
            self.globalDispatchgroup.notify(queue: .main, execute: {
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.ShowAlertMessage(title: "Alert", message: "\(self.globalJson["message"] ?? "")")
                    self.DataTableView.delegate = self
                    self.DataTableView.dataSource = self
                    self.DataTableView.reloadData()
                    self.SetTableheight(table: self.DataTableView, heightConstraint: self.DataTableViewHeightConstraint)
                    
                }
            })
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class usersholidayTVC : UITableViewCell
{
    @IBOutlet weak var ViewContent: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var titlename: UITextField!
    @IBOutlet weak var titledescription: UITextView!
    
}

extension UsersHolidayServiceVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.HolidayServiceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "data") as! usersholidayTVC
        cell.titlename.text = "\((self.HolidayServiceList[indexPath.row] as! NSDictionary)["title"] ?? "")"
        cell.titledescription.text = "\((self.HolidayServiceList[indexPath.row] as! NSDictionary)["description"] ?? "")"
        
        cell.titlename.isUserInteractionEnabled = false
        cell.ViewContent.layer.borderColor = UIColor.lightGray.cgColor
        cell.ViewContent.layer.borderWidth = 1.0
        
        cell.titledescription.layer.borderWidth = 1.0
        cell.titledescription.layer.borderColor = UIColor.black.cgColor
        cell.titledescription.clipsToBounds = true
        cell.titledescription.layer.cornerRadius = 4.0
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
