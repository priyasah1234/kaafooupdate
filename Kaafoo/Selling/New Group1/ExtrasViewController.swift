//
//  ExtrasViewController.swift
//  Kaafoo
//
//  Created by admin on 06/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ExtrasViewController: GlobalViewController {


    @IBOutlet weak var mainView: UIView!


    // MARK:- // Next Button

    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBAction func nextButtonTap(_ sender: UIButton) {

        self.saveExtraFeatures()

        self.dispatchGroup.notify(queue: .main) {
            NotificationCenter.default.post(name: .passDataAddPost, object: "3")
        }



    }


    var allDataDictionary : NSDictionary!

    let dispatchGroup = DispatchGroup()

    var featureButtonsArray = [UIButton]()

    var totalExtraFees : String!
    var extraFeatureID : String!


    var mainScroll : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        //scrollView.backgroundColor = .green
        return scrollView
    }()


    var scrollContentview : UIView = {
        let scrollcontentview = UIView()
        scrollcontentview.translatesAutoresizingMaskIntoConstraints = false
        //scrollcontentview.backgroundColor = .blue
        return scrollcontentview
    }()


    var allFeaturesView : UIView = {
        let allfeaturesview = UIView()
        allfeaturesview.translatesAutoresizingMaskIntoConstraints = false
        //allfeaturesview.backgroundColor = .red
        return allfeaturesview
    }()

    var feesAndBalanceView : UIView = {
        let feesandbalanceview = UIView()
        feesandbalanceview.translatesAutoresizingMaskIntoConstraints = false
        //feesandbalanceview.backgroundColor = .cyan
        return feesandbalanceview
    }()

    var totalExtraFeesLBL : UILabel!

    var accountBalanceLBL : UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()

        self.headerView.isHidden = true

        self.getExtraFeatures()

        self.dispatchGroup.notify(queue: .main) {


            self.mainView.addSubview(self.feesAndBalanceView)
            self.mainView.addSubview(self.mainScroll)
            self.mainScroll.addSubview(self.scrollContentview)
            self.scrollContentview.addSubview(self.allFeaturesView)

            self.setupLayout()

        }





    }


    // MARK:- // Setup Layout

    func setupLayout() {

        self.feesAndBalanceView.anchor(top: nil, leading: self.mainView.leadingAnchor, bottom: self.mainView.bottomAnchor, trailing: self.mainView.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 10, right: 10))

        createFeesAndBalanceView()

        mainScroll.anchor(top: self.mainView.topAnchor, leading: self.mainView.leadingAnchor, bottom: self.feesAndBalanceView.topAnchor, trailing: self.mainView.trailingAnchor)

        self.scrollContentview.anchor(top: self.mainScroll.topAnchor, leading: self.mainScroll.leadingAnchor, bottom: self.mainScroll.bottomAnchor, trailing: self.mainScroll.trailingAnchor)
        self.scrollContentview.widthAnchor.constraint(equalTo: self.mainView.widthAnchor).isActive = true

        self.allFeaturesView.anchor(top: self.scrollContentview.topAnchor, leading: self.scrollContentview.leadingAnchor, bottom: self.scrollContentview.bottomAnchor, trailing: self.scrollContentview.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))




        let tempCount = (self.allDataDictionary["product_feature"] as! NSArray).count

        for i in 0..<tempCount
        {

            let tempView : UIView = {
                let tempview = UIView()
                tempview.translatesAutoresizingMaskIntoConstraints = false
                //tempview.backgroundColor = .yellow
                return tempview
            }()

            self.allFeaturesView.addSubview(tempView)


            if i == 0
            {
                tempView.anchor(top: self.allFeaturesView.topAnchor, leading: self.allFeaturesView.leadingAnchor, bottom: nil, trailing: self.allFeaturesView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 5, right: 5))
            }
            else if i == (tempCount - 1)
            {
                tempView.anchor(top: self.allFeaturesView.subviews[i-1].bottomAnchor, leading: self.allFeaturesView.leadingAnchor, bottom: self.allFeaturesView.bottomAnchor, trailing: self.allFeaturesView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 5, right: 5))
            }
            else
            {
                tempView.anchor(top: self.allFeaturesView.subviews[i-1].bottomAnchor, leading: self.allFeaturesView.leadingAnchor, bottom: nil, trailing: self.allFeaturesView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 5, right: 5))
            }

            createSingleFeatureView(dataDictionary: (self.allDataDictionary["product_feature"] as! NSArray)[i] as! NSDictionary, index: i)

        }


        for i in 0..<(self.allDataDictionary["product_feature"] as! NSArray).count {
            if "\(((self.allDataDictionary["product_feature"] as! NSArray)[i] as! NSDictionary)["checked_status"] ?? "")".elementsEqual("1") {
                self.featureButtonsArray[i].isSelected = true
            }
            else {
                self.featureButtonsArray[i].isSelected = false
            }
        }



    }



    // MARK:- // Create Single Feature View

    func createSingleFeatureView(dataDictionary : NSDictionary , index : Int) {

        let featureBTN : UIButton = {
            let featurebtn = UIButton()
            featurebtn.translatesAutoresizingMaskIntoConstraints = false
            featurebtn.setImage(UIImage(named: "radioSelected"), for: .selected)
            featurebtn.setImage(UIImage(named: "radioUnselected"), for: .normal)
            featurebtn.addTarget(self, action: #selector(ExtrasViewController.selectFeature(sender:)), for: .touchUpInside)
            return featurebtn
        }()

        featureBTN.tag = index

        let titleLBL : UILabel = {
            let titlelbl = UILabel()
            titlelbl.translatesAutoresizingMaskIntoConstraints = false
            titlelbl.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 15)))
            titlelbl.textColor = .darkGray
            titlelbl.textAlignment = .natural
            titlelbl.numberOfLines = 0

            titlelbl.text = "\(dataDictionary["name"] ?? "")" + ", \(dataDictionary["price"] ?? "")"

            return titlelbl
        }()

        let separatorView : UIView = {
            let separatorview = UIView()
            separatorview.translatesAutoresizingMaskIntoConstraints = false
            separatorview.backgroundColor = .lightGray
            return separatorview
        }()

        let tempView = self.allFeaturesView.subviews[index]

        tempView.addSubview(featureBTN)
        tempView.addSubview(titleLBL)
        tempView.addSubview(separatorView)

        self.featureButtonsArray.append(featureBTN)

        featureBTN.anchor(top: tempView.topAnchor, leading: tempView.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 10, bottom: 0, right: 0), size: .init(width: 20, height: 20))

        titleLBL.anchor(top: featureBTN.topAnchor, leading: featureBTN.trailingAnchor, bottom: nil, trailing: tempView.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 0, right: 10))

        separatorView.anchor(top: nil, leading: tempView.leadingAnchor, bottom: tempView.bottomAnchor, trailing: tempView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 1))

        let optionsView : UIView = {
            let optionsview = UIView()
            optionsview.translatesAutoresizingMaskIntoConstraints = false
            //optionsview.backgroundColor = .cyan
            return optionsview
        }()

        tempView.addSubview(optionsView)

        optionsView.anchor(top: featureBTN.bottomAnchor, leading: titleLBL.leadingAnchor, bottom: tempView.bottomAnchor, trailing: tempView.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 10, right: 10))


        let tempcount = (dataDictionary["option"] as! NSArray).count



        for i in 0..<tempcount {

            let optionLBL : UILabel = {
                let optionlbl = UILabel()
                optionlbl.translatesAutoresizingMaskIntoConstraints = false
                optionlbl.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
                optionlbl.textColor = .black
                optionlbl.textAlignment = .natural
                optionlbl.numberOfLines = 0

                let tempString = "\(((dataDictionary["option"] as! NSArray)[i] as! NSDictionary)["name"] ?? "")"

                optionlbl.text = tempString.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)

                return optionlbl
            }()

            optionsView.addSubview(optionLBL)

            if tempcount == 1 {
                optionLBL.anchor(top: optionsView.topAnchor, leading: optionsView.leadingAnchor, bottom: optionsView.bottomAnchor, trailing: optionsView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 5))
            }
            else {
                if i == 0 {
                    optionLBL.anchor(top: optionsView.topAnchor, leading: optionsView.leadingAnchor, bottom: nil, trailing: optionsView.trailingAnchor)
                }
                else if i == tempcount - 1 {
                    optionLBL.anchor(top: optionsView.subviews[i-1].bottomAnchor, leading: optionsView.leadingAnchor, bottom: optionsView.bottomAnchor, trailing: optionsView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 5, right: 0))
                }
                else {
                    optionLBL.anchor(top: optionsView.subviews[i-1].bottomAnchor, leading: optionsView.leadingAnchor, bottom: nil, trailing: optionsView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0))
                }
            }

        }

    }


    // MARK:- // Select Feature Button Action

    @objc func selectFeature(sender : UIButton) {

        for i in 0..<self.featureButtonsArray.count {
            self.featureButtonsArray[i].isSelected = false
        }
        sender.isSelected = true

        self.totalExtraFeesLBL.isHidden = false

        print(sender.tag)

        self.totalExtraFeesLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalExtraFees", comment: "") + ": \(((self.allDataDictionary["product_feature"] as! NSArray)[sender.tag] as! NSDictionary)["price"] ?? "")"

        self.totalExtraFees = "\(((self.allDataDictionary["product_feature"] as! NSArray)[sender.tag] as! NSDictionary)["price_val"] ?? "")"
        self.extraFeatureID = "\(((self.allDataDictionary["product_feature"] as! NSArray)[sender.tag] as! NSDictionary)["id"] ?? "")"

        self.accountBalanceLBL.text =  LocalizationSystem.sharedInstance.localizedStringForKey(key: "accountBalanceBeforeExtras", comment: "") + ": \(((self.allDataDictionary["product_feature"] as! NSArray)[sender.tag] as! NSDictionary)["price"] ?? "")"



    }




    // MARK:- // Create Fees and Balance View

    func createFeesAndBalanceView() {

        totalExtraFeesLBL = {
            let totalextrafeeslbl = UILabel()
            totalextrafeeslbl.translatesAutoresizingMaskIntoConstraints = false
            totalextrafeeslbl.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
            totalextrafeeslbl.textColor = .black
            totalextrafeeslbl.textAlignment = .natural
            totalextrafeeslbl.numberOfLines = 0

            totalextrafeeslbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalExtraFees", comment: "") + ": \(self.allDataDictionary["total_extra_fees"] ?? "")"

            return totalextrafeeslbl
        }()


        accountBalanceLBL = {
            let accountbalancelbl = UILabel()
            accountbalancelbl.translatesAutoresizingMaskIntoConstraints = false
            accountbalancelbl.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 13)))
            accountbalancelbl.textColor = .black
            accountbalancelbl.textAlignment = .natural
            accountbalancelbl.numberOfLines = 0

            accountbalancelbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "accountBalanceBeforeExtras", comment: "") + ": \(self.allDataDictionary["account_balance_before_extras"] ?? "")"

            return accountbalancelbl
        }()


        let feesStackview: UIStackView = {
            let feesstackview = UIStackView(arrangedSubviews: [totalExtraFeesLBL, accountBalanceLBL])
            feesstackview.translatesAutoresizingMaskIntoConstraints = false
            feesstackview.alignment = .fill
            feesstackview.distribution = .fillProportionally
            feesstackview.axis = .vertical
            return feesstackview
        }()

        self.feesAndBalanceView.addSubview(feesStackview)

        feesStackview.anchor(top: feesAndBalanceView.topAnchor, leading: feesAndBalanceView.leadingAnchor, bottom: feesAndBalanceView.bottomAnchor, trailing: feesAndBalanceView.trailingAnchor, padding: .init(top: 10, left: 10, bottom: 10, right: 10))


        if "\(self.allDataDictionary["step_final"] ?? "")".elementsEqual("1") {
            totalExtraFeesLBL.isHidden = true
        }
        else {
            totalExtraFeesLBL.isHidden = false
        }

    }






    // MARK: - // JSON POST Method to get Extra Features

    func getExtraFeatures()

    {
        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }



        let url = URL(string: GLOBALAPI + "app_item_extra_list")!       //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        let productID = UserDefaults.standard.object(forKey: "productID") as! String

        parameters = "lang_id=\(langID!)&user_id=\(userID!)&product_id=\(productID)"


        print("Get Data with Product ID URL : ", url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        } 

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Get Data with Product ID Response: " , json)

                    if (json["response"] as! Bool) == true
                    {

                        self.allDataDictionary = json["details_info"] as? NSDictionary

                    }

                    DispatchQueue.main.async {

                        self.dispatchGroup.leave()

                        SVProgressHUD.dismiss()
                    }


                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }
            }
        })

        task.resume()



    }






    // MARK: - // JSON POST Method to save Extra Features

    func saveExtraFeatures()

    {
        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }



        let url = URL(string: GLOBALAPI + "app_item_extra_save")!       //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        let productID = UserDefaults.standard.object(forKey: "productID") as! String

        parameters = "lang_id=\(langID!)&user_id=\(userID!)&product_id=\(productID)&extra_feature_price=\(self.totalExtraFees ?? "")&extra_feature_id=\(self.extraFeatureID ?? "")"


        print("Extra Feature URL : ", url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Save Extra Features Response: " , json)


                    DispatchQueue.main.async {

                        self.dispatchGroup.leave()

                        SVProgressHUD.dismiss()
                    }


                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }
            }
        })

        task.resume()



    }
}
