//
//  postAdDetailsViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 01/02/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class postAdDetailsViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {


    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBAction func nextButtonTap(_ sender: UIButton) {

        self.saveDetails()

        self.dispatchGroup.notify(queue: .main) {

            UserDefaults.standard.setValue(false, forKey: "autoLoad")

            NotificationCenter.default.post(name: .passDataAddPost, object: "1")

        }



    }



    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var detailsTitleLBL: UILabel!
    @IBOutlet weak var detailsTXT: UITextView!
    @IBOutlet weak var detailsSepatatorView: UIView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLBL: UILabel!
    @IBOutlet weak var priceTXT: UITextField!
    @IBOutlet weak var priceSpecialOfferView: UIView!
    @IBOutlet weak var priceSpecialOfferStackview: UIStackView!
    @IBOutlet weak var specialOfferView: UIView!
    @IBOutlet weak var specialOfferCheckboxImageview: UIImageView!
    @IBOutlet weak var specialOfferLBL: UILabel!
    @IBOutlet weak var newPriceView: UIView!
    @IBOutlet weak var newPriceLBL: UILabel!
    @IBOutlet weak var newPriceTXT: UITextField!
    @IBOutlet weak var shippingOptionView: UIView!
    @IBOutlet weak var shippingOptionTitleLBL: UILabel!
    @IBOutlet weak var shippingOptionStackview: UIStackView!
    @IBOutlet weak var shippingOptionStackViewFirstSection: UIView!
    @IBOutlet weak var pickupFromStoreView: UIView!
    @IBOutlet weak var pickupFromStoreCheckboxImageview: UIImageView!
    @IBOutlet weak var pickupFromStoreLBL: UILabel!
    @IBOutlet weak var deliveryInAddressView: UIView!
    @IBOutlet weak var deliveryInAddressCheckboxImageview: UIImageView!
    @IBOutlet weak var deliveryInAddressLBL: UILabel!
    @IBOutlet weak var shippingTypeView: UIView!
    @IBOutlet weak var shippingTypeTitleLBL: UILabel!
    @IBOutlet weak var shippingTypeStackView: UIStackView!
    @IBOutlet weak var shippingTypeStackviewFirstSection: UIView!
    @IBOutlet weak var freeView: UIView!
    @IBOutlet weak var freeCheckboxImageview: UIImageView!
    @IBOutlet weak var freeLBL: UILabel!
    @IBOutlet weak var chargeView: UIView!
    @IBOutlet weak var chargeCheckboxImageview: UIImageView!
    @IBOutlet weak var chargeLBL: UILabel!
    @IBOutlet weak var shippingAmountView: UIView!
    @IBOutlet weak var shippingAmountLBL: UILabel!
    @IBOutlet weak var shippingAmountTXT: UITextField!
    @IBOutlet weak var featuresView: UIView!
    @IBOutlet weak var featuresLBL: UILabel!
    @IBOutlet weak var featuresTableview: UITableView!
    @IBOutlet weak var featureTableHeightConstraint: NSLayoutConstraint!


    @IBOutlet weak var productTypeStackview: UIStackView!
    @IBOutlet weak var buyNowView: UIView!
    @IBOutlet weak var auctionView: UIView!
    @IBOutlet weak var askingPriceView: UIView!


    @IBOutlet weak var startingPriceLBL: UILabel!
    @IBOutlet weak var startingPriceTXT: UITextField!
    @IBOutlet weak var reservePriceLBL: UILabel!
    @IBOutlet weak var reservePriceTXT: UITextField!


    @IBOutlet weak var askingPriceLBL: UILabel!
    @IBOutlet weak var askingPriceTXT: UITextField!


    @IBOutlet weak var paymentTypeView: UIView!
    @IBOutlet weak var paymentTypeTitleLBL: UILabel!
    @IBOutlet weak var paymentTypeStackview: UIStackView!
    @IBOutlet weak var paymentTypeOptionsView: UIView!
    @IBOutlet weak var bankDetailsView: UIView!
    @IBOutlet weak var bankNameLBL: UILabel!
    @IBOutlet weak var bankNameTXT: UITextField!
    @IBOutlet weak var accountNoLBL: UILabel!
    @IBOutlet weak var accountNoTXT: UITextField!



    var currencySymbol : String!


    // MARK:- // Cash On Delivery

    @IBOutlet weak var cashOnDeliveryView: UIView!
    @IBOutlet weak var codImageview: UIImageView!
    @IBOutlet weak var codTitleLBL: UILabel!
    @IBOutlet weak var codButtonOutlet: UIButton!
    @IBAction func tapOnCOD(_ sender: UIButton) {
        
        


        if self.paymentTypeArray.contains("C")
        {
            for i in 0..<self.paymentTypeArray.count
            {
                if self.paymentTypeArray[i].elementsEqual("C")
                {
                    self.paymentTypeArray.remove(at: i)
                }
            }
        }
        else
        {
            self.paymentTypeArray.append("C")
        }

        self.checkPaymentType()

    }



    // MARK:- // Bank Transfer


    @IBOutlet weak var bankTransferView: UIView!
    @IBOutlet weak var bankTransferImageview: UIImageView!
    @IBOutlet weak var bankTransferLBL: UILabel!
    @IBOutlet weak var bankTransferButtonOutlet: UIButton!
    @IBAction func tapOnBankTransfer(_ sender: UIButton) {

        if self.paymentTypeArray.contains("B")
        {
            for i in 0..<self.paymentTypeArray.count
            {
                if self.paymentTypeArray[i].elementsEqual("B")
                {
                    self.paymentTypeArray.remove(at: i)
                }
            }
        }
        else
        {
            self.paymentTypeArray.append("B")
        }


        self.checkPaymentType()

    }










    var bankTransferClicked : Bool! = false
    var codClicked : Bool! = false

    var specialOfferClicked : Bool! = false

    var shippingOptionArray = [String]()

    var deliveryInAddressClicked : Bool! = false

    var shippingType : String! = "0"

    var chargeClicked : Bool! = false

    var featureTableCount : Int! = 1

    var productID : String!

    var dispatchGroup = DispatchGroup()

    var allDetailsDictionary : NSDictionary!

    var productType : String!

    var specialOffer : String! = "0"

    var paymentTypeArray = [String]()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        currencySymbol = UserDefaults.standard.string(forKey: "currencySymbol")
        
        self.startingPriceLBL.text = "Starting Price" + " (\(currencySymbol!))"
        self.reservePriceLBL.text = "Reserve Price" + " (\(currencySymbol!))"
        self.askingPriceLBL.text = "Asking Price" + " (\(currencySymbol!))"

        self.setStaticStrings()

        self.loadDetails()

        self.dispatchGroup.notify(queue: .main) {

            self.loadProductType()


            self.productID = "\(self.allDetailsDictionary["product_id"] ?? "")"

//            print("Product ID ------",self.productID)

            self.fillUpDetails()

        }



    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.featuresTableview.estimatedRowHeight = 300
        self.featuresTableview.rowHeight = UITableView.automaticDimension

    }


    // MARK:- // Buttons


    // MARK:- // Price Special Offer Button


    @IBOutlet weak var priceSpecialOfferButtonOutlet: UIButton!
    @IBAction func priceSpecialOfferButtonTap(_ sender: UIButton) {

        self.specialOfferClicked = !self.specialOfferClicked

        self.checkSpecialOffer()


    }


    // MARK:- // Pickup From Store Button


    @IBOutlet weak var pickupFromStoreButtonOutlet: UIButton!
    @IBAction func pickupFromStoreButtonTap(_ sender: UIButton) {

        if self.shippingOptionArray.contains("1")
        {
            var indexToDelete : Int!
            for i in 0..<self.shippingOptionArray.count
            {
                if self.shippingOptionArray[i].elementsEqual("1")
                {
                    indexToDelete = i
                }
            }
            self.shippingOptionArray.remove(at: indexToDelete)
        }
        else
        {
            self.shippingOptionArray.append("1")
        }


        self.checkShippingOption()



        print("Shipping Option Array-----", self.shippingOptionArray.joined(separator: ","))

    }


    // MARK:- // Delivery in Address Button


    @IBOutlet weak var deliveryInAddressButtonOutlet: UIButton!
    @IBAction func deliveryInAddressButtonTap(_ sender: UIButton) {

        if self.shippingOptionArray.contains("2")
        {
            var indexToDelete : Int!

            for i in 0..<self.shippingOptionArray.count
            {
                if self.shippingOptionArray[i].elementsEqual("2")
                {
                    indexToDelete = i
                }
            }
            self.shippingOptionArray.remove(at: indexToDelete)
        }
        else
        {
            self.shippingOptionArray.append("2")
        }




        self.deliveryInAddressClicked  = !self.deliveryInAddressClicked

        self.checkShippingOption()

        print("Shipping Option Array-----", self.shippingOptionArray.joined(separator: ","))

    }


    // MARK:- // Free Shipping Type Button


    @IBOutlet weak var freeButtonOutlet: UIButton!
    @IBAction func freeButtonTap(_ sender: UIButton) {

        if chargeClicked == true
        {
            self.chargeClicked = !self.chargeClicked
        }


        self.shippingType = "1"

        self.checkShippingType()


        //print("Shipping Type Array -----", self.shippingTypeArray.joined(separator: ","))
        print("Shipping Type : -----  Free",self.shippingType!)


    }


    // MARK:- // Charge Shipping Type Button


    @IBOutlet weak var chargeButtonOutlet: UIButton!
    @IBAction func chargeButtonTap(_ sender: UIButton) {


        self.shippingType = "2"

        if self.chargeClicked == false
        {
            self.chargeClicked = !self.chargeClicked
        }


        self.checkShippingType()


        //print("Shipping Type Array -----", self.shippingTypeArray.joined(separator: ","))
        print("Shipping Type : -----  Charge",self.shippingType!)

    }


    // MARK:- // Add More Button


    @IBOutlet weak var addMoreButtonOutlet: UIButton!
    @IBAction func addMoreButtonTap(_ sender: UIButton) {

        self.featureTableCount = self.featureTableCount + 1
        self.featuresTableview.reloadData()

        self.featureTableHeightConstraint.constant = CGFloat(self.featureTableCount * 70)

        let bottomOffset = CGPoint(x: 0, y: self.scrollContentView.frame.size.height - self.mainScroll.bounds.size.height)
        self.mainScroll.setContentOffset(bottomOffset, animated: true)
    }


    // MARK:- // Check Payment Type

    func checkPaymentType()
    {

        if self.paymentTypeArray.contains("C")
        {
            self.codClicked = true
        }
        else
        {
            self.codClicked = false
        }

        if self.paymentTypeArray.contains("B")
        {
            self.bankTransferClicked = true
        }
        else
        {
            self.bankTransferClicked = false
        }



        if self.codClicked
        {
            self.codImageview.image = UIImage(named: "checkBoxFilled")
        }
        else
        {
            self.codImageview.image = UIImage(named: "checkBoxEmpty")
        }

        if self.bankTransferClicked
        {
            self.bankDetailsView.isHidden = false
            self.bankTransferImageview.image = UIImage(named: "checkBoxFilled")
        }
        else
        {
            self.bankDetailsView.isHidden = true
            self.bankTransferImageview.image = UIImage(named: "checkBoxEmpty")
        }

    }



    // MARK:- // Check If Special Offer Clicked

    func checkSpecialOffer()
    {

        if self.specialOfferClicked == true
        {
            self.newPriceView.isHidden = false
            self.specialOfferCheckboxImageview.image = UIImage(named: "checkBoxFilled")
        }
        else
        {
            self.newPriceView.isHidden = true
            self.specialOfferCheckboxImageview.image = UIImage(named: "checkBoxEmpty")
        }

    }


    // MARK: - // Check Shipping Option

    func checkShippingOption()
    {

        if self.shippingOptionArray.count == 0
        {
            self.pickupFromStoreCheckboxImageview.image = UIImage(named: "checkBoxEmpty")
            self.deliveryInAddressCheckboxImageview.image = UIImage(named: "checkBoxEmpty")
        }
        else
        {
            self.pickupFromStoreCheckboxImageview.image = UIImage(named: "checkBoxEmpty")
            self.deliveryInAddressCheckboxImageview.image = UIImage(named: "checkBoxEmpty")

            if self.shippingOptionArray.contains("1")  // Pickup on Store
            {
                self.pickupFromStoreCheckboxImageview.image = UIImage(named: "checkBoxFilled")
            }

            if self.shippingOptionArray.contains("2")  // Delivery in Address
            {
                self.deliveryInAddressClicked = true

                self.deliveryInAddressCheckboxImageview.image = UIImage(named: "checkBoxFilled")
            }
        }



        if deliveryInAddressClicked == true
        {
            self.shippingTypeView.isHidden = false
        }
        else
        {
            self.shippingTypeView.isHidden = true
        }

    }


    // MARK:- // Check Shipping Type

    func checkShippingType()
    {


        if self.shippingType.elementsEqual("0")  // None
        {
            self.freeCheckboxImageview.image = UIImage(named: "radioUnselected")
            self.chargeCheckboxImageview.image = UIImage(named: "radioUnselected")
        }
        else
        {
            if self.shippingType.elementsEqual("1")  // Free
            {
                self.freeCheckboxImageview.image = UIImage(named: "radioSelected")
                self.chargeCheckboxImageview.image = UIImage(named: "radioUnselected")

                self.chargeClicked = false
            }
            else   // Charge
            {
                self.freeCheckboxImageview.image = UIImage(named: "radioUnselected")
                self.chargeCheckboxImageview.image = UIImage(named: "radioSelected")

                self.chargeClicked = true
            }
        }



        if self.chargeClicked == true
        {
            self.shippingAmountView.isHidden = false
        }
        else
        {
            self.shippingAmountView.isHidden = true
        }

    }





    // MARK:- // Delegate Methods

    // MARK:- // Tableview Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.featureTableCount

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "features") as! PostAdDetailsFeaturesTVCTableViewCell

        if indexPath.row > 0
        {
            cell.deleteButton.isHidden = false
        }
        else
        {
            cell.deleteButton.isHidden = true
        }

        cell.deleteButton.tag = indexPath.row

        cell.deleteButton.addTarget(self, action: #selector(postAdDetailsViewController.delete(sender:)), for: .touchUpInside)

        return cell

    }



    // MARK:- // Delete Feature Tableview Cell

    @objc func delete(sender: UIButton)
    {

        self.featureTableCount = self.featureTableCount - 1
        self.featuresTableview.reloadData()

        self.featureTableHeightConstraint.constant = CGFloat(self.featureTableCount * 70)

    }





    // MARK:- // Load Product Type

    func loadProductType()
    {
        self.productType = "\(self.allDetailsDictionary["product_type"] ?? "")"

        if productType.elementsEqual("A")
        {
            self.buyNowView.isHidden = true
            self.auctionView.isHidden = false
            self.askingPriceView.isHidden = true
        }
        else if productType.elementsEqual("B")
        {
            self.buyNowView.isHidden = false
            self.auctionView.isHidden = true
            self.askingPriceView.isHidden = true


            self.featuresTableview.delegate = self
            self.featuresTableview.dataSource = self

            self.checkPaymentType()
            self.checkSpecialOffer()
            self.checkShippingOption()
            self.checkShippingType()

            self.addMoreButtonOutlet.layer.cornerRadius = self.addMoreButtonOutlet.frame.size.height / 2

        }
        else
        {
            self.buyNowView.isHidden = true
            self.auctionView.isHidden = true
            self.askingPriceView.isHidden = false
        }

    }






    // MARK: - // JSON POST Method to Load Details

    func loadDetails()

    {
        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }



        let url = URL(string: GLOBALAPI + "app_post_details")!       //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        self.productID = UserDefaults.standard.object(forKey: "productID") as? String

        let stepFinal = UserDefaults.standard.object(forKey: "stepFinal")

        parameters = "lang_id=\(langID!)&user_id=\(userID!)&product_id=\(self.productID!)&step_final=\(stepFinal ?? "")"


        print("Load Details URL : ", url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Load Details Response: " , json)

                    if (json["response"] as! Bool) == true
                    {

                        self.allDetailsDictionary = json["details_info"] as? NSDictionary

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }




                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }
            }
        })

        task.resume()



    }






    // MARK: - // JSON POST Method to save Details

    func saveDetails()

    {
        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }



        let url = URL(string: GLOBALAPI + "app_add_item_details")!       //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")

        //        if (langID?.elementsEqual("EN"))!
        //        {
        //            self.langSuffix = "_en"
        //        }

        var price : String!
        var secondPrice : String!
        var thirdPrice : String!



        if self.productType.elementsEqual("A")
        {
            price = self.startingPriceTXT.text
            secondPrice = self.reservePriceTXT.text
            thirdPrice = ""
        }
        else if self.productType.elementsEqual("P")
        {
            price = self.askingPriceTXT.text
            secondPrice = ""
            thirdPrice = ""
        }
        else if self.productType.elementsEqual("B")
        {
            price = self.priceTXT.text
            secondPrice = self.newPriceTXT.text

            if self.specialOfferClicked
            {
                self.specialOffer = "1"
            }
            else
            {
                self.specialOffer = "0"
            }

        }


        parameters = "lang_id=\(langID!)&user_id=\(userID!)&product_id=\(self.productID!)&step_final=0&prod_content=\(self.detailsTXT.text ?? "")&price=\(price ?? "")&price_two=\(secondPrice ?? "")&price_three=\(thirdPrice ?? "")&special_offer=\(self.specialOffer ?? "")&shipping=\(self.shippingOptionArray.joined(separator: ","))&shipping_type=\(self.shippingType ?? "")&shipping_amount=\(self.shippingAmountTXT.text ?? "")&pay_type=\(self.paymentTypeArray.joined(separator: ","))"


        print("Save Details URL : ", url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        } 

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("First Category List Response: " , json)

                    if (json["response"] as! Bool) == true
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }




                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }
            }
        })

        task.resume()



    }






    // MARK:- // Fill Up Details

    func fillUpDetails()
    {
        self.detailsTXT.text = "\(self.allDetailsDictionary["description"] ?? "")"

        self.productTypeFillUpData()
    }




    // MARK:- // Product Type Fill Up Data

    func productTypeFillUpData()
    {
        if self.productType.isEmpty == false
        {
            if self.productType.elementsEqual("P")
            {
                self.askingPriceTXT.text = "\(self.allDetailsDictionary["start_price"] ?? "")"
            }
            else if productType.elementsEqual("A")
            {
                self.startingPriceTXT.text = "\(self.allDetailsDictionary["start_price"] ?? "")"
                self.reservePriceTXT.text = "\(self.allDetailsDictionary["reserve_price"] ?? "")"
            }
            else if productType.elementsEqual("B")
            {


                // Payment Type
                self.fillUpPaymentInformation()
                self.checkPaymentType()


                // Special Offer
                if "\(self.allDetailsDictionary["special_offer"] ?? "")".elementsEqual("1")
                {

                    self.specialOfferClicked = true
                    self.checkSpecialOffer()

                    self.priceTXT.text = "\(self.allDetailsDictionary["start_price"] ?? "")"
                    self.newPriceTXT.text = "\(self.allDetailsDictionary["reserve_price"] ?? "")"
                }



                //Shipping Option
                self.shippingOptionArray = "\(self.allDetailsDictionary["shipping_option"] ?? "")".components(separatedBy: ",")

                self.checkShippingOption()

                //Shipping Type
                self.shippingType = "\(self.allDetailsDictionary["shipping_type"] ?? "")"
                self.checkShippingType()

                if self.shippingType.elementsEqual("2")
                {
                    self.shippingAmountTXT.text = "\(self.allDetailsDictionary["shipping_amount"] ?? "")"
                }


            }
        }
    }




    // MARK:- // Fill up PaymentInfo

    func fillUpPaymentInformation()
    {
        let paymentInfoArray = self.allDetailsDictionary["paytype_info"] as! NSArray


        for i in 0..<paymentInfoArray.count
        {
            if "\((paymentInfoArray[i] as! NSDictionary)["code"] ?? "")".elementsEqual("B")
            {
                self.bankNameTXT.text = "\((paymentInfoArray[i] as! NSDictionary)["bank_name"] ?? "")"
                self.accountNoTXT.text = "\((paymentInfoArray[i] as! NSDictionary)["account_number"] ?? "")"

                if "\((paymentInfoArray[i] as! NSDictionary)["status"] ?? "")".elementsEqual("1")
                {
                    //self.bankTransferClicked = true
                    self.paymentTypeArray.append("B")
                }
            }

            if "\((paymentInfoArray[i] as! NSDictionary)["code"] ?? "")".elementsEqual("C")
            {
                if "\((paymentInfoArray[i] as! NSDictionary)["status"] ?? "")".elementsEqual("1")
                {
                    //self.codClicked = true
                    self.paymentTypeArray.append("C")
                }
            }
        }


    }


    // MARK:- // Set Static Strings

    func setStaticStrings() {
        
        
        
        self.detailsTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")
        self.paymentTypeTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "payment_type", comment: "")
        self.bankTransferLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bankTransfer", comment: "")
        self.codTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "cash_on_delivery", comment: "")
        self.bankNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bankName", comment: "")
        self.accountNoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "accountNumber", comment: "")
        self.specialOfferLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "special_offer", comment: "")
        self.priceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "price", comment: "") + " (\(currencySymbol!))"
        self.newPriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "new_price", comment: "") + " (\(currencySymbol!))"
        self.shippingOptionTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "shippingOption", comment: "")
        self.deliveryInAddressLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "deliveryIn", comment: "")
        self.pickupFromStoreLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickupFrom", comment: "")
        self.shippingTypeTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "shipping_type", comment: "")
        self.chargeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "charge", comment: "")
        self.freeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "free", comment: "")
        self.shippingAmountLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "shippingAmmount", comment: "") + " (\(currencySymbol!))"

        self.bankNameTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter", comment: "") + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "bankName", comment: "")
        self.accountNoTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter", comment: "") + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "accountNumber", comment: "")
        self.priceTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter", comment: "") + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "price", comment: "")
        self.newPriceTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter", comment: "") + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "new_price", comment: "")
        self.shippingAmountTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "enter", comment: "") + " " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "shippingAmmount", comment: "")
        
        

    }


}
