//
//  ServiceItemViewController.swift
//  Kaafoo
//
//  Created by admin on 15/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import Cosmos

class ServiceItemViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {
    @IBOutlet weak var serviceItemTableview: UITableView!
    @IBOutlet weak var reviewMainView: UIView!
    @IBOutlet weak var reviewWindowView: UIView!
    @IBOutlet weak var reviewTitleLBL: UILabel!
    @IBOutlet weak var crossImageview: UIImageView!
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var ratingLBL: UILabel!
    @IBOutlet weak var commentLBL: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentTXTView: UITextView!
    @IBOutlet weak var saveButtonOutlet: UIButton!


    //MARK:- //View User Review View
    @IBOutlet weak var ViewUserReview: UIView!
    @IBOutlet weak var ViewComment: UILabel!
    @IBOutlet weak var ViewDate: UILabel!
    @IBAction func ReviewShownOk(_ sender: UIButton) {
        self.ViewUserReview.isHidden = true
    }
    @IBOutlet weak var ShowRating: CosmosView!

    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()

    var bookingItemArray : NSMutableArray!
    
    var serviceRequestID : String!
    
    var cell : ServiceItemTVCTableViewCell!
    
    var reviewRating : String! = "0"
    
    var orderID : String!
    
    var sellerID : String!
    
    var productID : String!

    var reviewType : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getServiceItemList()


        self.globalDispatchgroup.notify(queue: .main, execute: {
            if self.recordsArray.count == 0
            {
               self.serviceItemTableview.isHidden = true
                self.noDataFound(mainview: self.view)
            }
            else
            {
              print("do nothing")
            }
        })
        
        commentTXTView.delegate = self
        

        setupReviewMainView()
        
        set_font()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        
        self.commentView.layer.borderWidth = 2
        self.commentView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        self.commentView.layer.cornerRadius = (5/568)*self.FullHeight
        
        rating.didTouchCosmos = didTouchCosmos
        rating.didFinishTouchingCosmos = didFinishTouchingCosmos
        
    }
    
    
    // MARK:- // Delegate Methods
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as? ServiceItemTVCTableViewCell
        
        // Setting Frames and Allignment to manage dynamic Height and Arabic UI
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        if (langID?.elementsEqual("EN"))!
        {
        }
        else
        {
        }
        
        // set cornerradius of addUserReviewButton
        cell.addUserReviewButton.layer.cornerRadius = cell.addUserReviewButton.frame.size.height / 2
        
        
        // Put Value
        cell.bookingNumber.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["booking_no"]!)"
        cell.status.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["status_name"]!)"
        cell.serviceName.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_name"]!)"
        cell.bookingDate.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["service_bkng_date"]!)"
        cell.bookingTime.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["service_bkng_time"]!)"
        cell.servicePrice.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["service_price"]!)"
        
        
        // Localize Static Texts
        cell.bookingNumberLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "booking_no", comment: "")
        cell.statusLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "status", comment: "")
        cell.serviceNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "serviceName", comment: "")
        cell.bookingDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingDate", comment: "")
        cell.bookingTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingTime", comment: "")
        cell.servicePriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "servicePrice", comment: "")
//        cell.confirmLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "confirm", comment: "")
//
//        cell.calcelLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: "")
        
        cell.addUserReviewButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_user_review", comment: ""), for: .normal)
        
        
        // Buttons
        cell.confirmserviceItemButton.tag = indexPath.row
        
        cell.confirmserviceItemButton.addTarget(self, action: #selector(ServiceItemViewController.confirmServiceTap(sender:)), for: .touchUpInside)
        
        cell.cancelServiceItemButton.tag = indexPath.row
        
        cell.cancelServiceItemButton.addTarget(self, action: #selector(ServiceItemViewController.cancelServiceTap(sender:)), for: .touchUpInside)
        
        cell.deleteServiceItemButton.tag = indexPath.row
        
        cell.deleteServiceItemButton.addTarget(self, action: #selector(ServiceItemViewController.deleteServiceTap(sender:)), for: .touchUpInside)
        
        
        cell.addUserReviewButton.tag = indexPath.row
        

        
        
        
        //////// Set Font
        
        
        cell.bookingNumberLBL.font = UIFont(name: cell.bookingNumberLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingNumber.font = UIFont(name: cell.bookingNumber.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.statusLBL.font = UIFont(name: cell.statusLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.status.font = UIFont(name: cell.status.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.serviceNameLBL.font = UIFont(name: cell.serviceNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.serviceName.font = UIFont(name: cell.serviceName.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingDateLBL.font = UIFont(name: cell.bookingDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingDate.font = UIFont(name: cell.bookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingTimeLBL.font = UIFont(name: cell.bookingTimeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingTime.font = UIFont(name: cell.bookingTime.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.servicePriceLBL.font = UIFont(name: cell.servicePriceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.servicePrice.font = UIFont(name: cell.servicePrice.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        
//        cell.confirmLBL.font = UIFont(name: cell.confirmLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
//        cell.calcelLBL.font = UIFont(name: cell.calcelLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.addUserReviewButton.titleLabel?.font = UIFont(name: (cell.addUserReviewButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 11)))!
        
        ///////// give border to cellview
        
//        cell.cellView.layer.borderWidth = 2
//        cell.cellView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        
        // no selection style
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
        
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        let cell = cell as! ServiceItemTVCTableViewCell
//
//        // Check for Status and set Frames
//
//
//
//        if "\((recordsArray[indexPath.row] as! NSMutableDictionary)["status_name"]!)".elementsEqual("Cancelled")
//        {
//            cell.confirmServiceView.isHidden = true
//            cell.cancelServiceView.isHidden = true
//            cell.addUserReviewButton.isHidden = true
//            cell.deleteServiceView.isHidden = false
//        }
//        else if "\((recordsArray[indexPath.row] as! NSMutableDictionary)["status_name"]!)".elementsEqual("Confirmed")
//        {
//            cell.confirmServiceView.isHidden = true
//            cell.cancelServiceView.isHidden = true
//            cell.addUserReviewButton.isHidden = false
//            cell.deleteServiceView.isHidden = true
//        }
//        else
//        {
//            cell.confirmServiceView.isHidden = false
//            cell.cancelServiceView.isHidden = false
//            cell.addUserReviewButton.isHidden = true
//            cell.deleteServiceView.isHidden = true
//        }
//
//
//        }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = cell as! ServiceItemTVCTableViewCell
        
        if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button"]!)".isEmpty
        {
            cell.confirmServiceView.isHidden = true
        }
        else
        {
            cell.confirmServiceView.isHidden = false
        }
        if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button2"]!)".isEmpty
        {
            cell.cancelServiceView.isHidden = true
        }
        else
        {
            cell.cancelServiceView.isHidden = false
        }
        if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["delete_button"]!)".isEmpty
        {
            cell.deleteServiceView.isHidden = true
        }
        else
        {
            cell.deleteServiceView.isHidden = false
        }
        
        if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["delete_button"]!)".isEmpty && "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button2"]!)".isEmpty && "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button"]!)".isEmpty &&
        "\(((self.recordsArray[indexPath.row] as! NSMutableDictionary)["review_section"] as! NSMutableDictionary )["review_valid_status"]!)".isEmpty
        {
            cell.buttonsView.isHidden = true
        }
        else
        {
            cell.buttonsView.isHidden = false
        }
        //Add Review Button
        if "\(((self.recordsArray[indexPath.row] as! NSMutableDictionary)["review_section"] as! NSMutableDictionary)["review_valid_status"]!)" == "0"
        {
            cell.addUserReviewButton.isHidden = false
            cell.addUserReviewButton.setTitle("Add User Review", for: .normal)
            cell.addUserReviewButton.addTarget(self, action: #selector(ServiceItemViewController.addUserReview(sender:)), for: .touchUpInside)
        }
        else if "\(((self.recordsArray[indexPath.row] as! NSMutableDictionary)["review_section"] as! NSMutableDictionary)["review_valid_status"]!)" == "1"
        {
            cell.addUserReviewButton.isHidden = false
            cell.addUserReviewButton.setTitle("View User Review", for: .normal)
             cell.addUserReviewButton.addTarget(self, action: #selector(ServiceItemViewController.ViewUserReview(sender:)), for: .touchUpInside)
        }
        else
        {
            cell.addUserReviewButton.isHidden = true
            cell.addUserReviewButton.setTitle("Add User Review", for: .normal)
            cell.addUserReviewButton.addTarget(self, action: #selector(ServiceItemViewController.addUserReview(sender:)), for: .touchUpInside)
        }
    }
    
    

        
    
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        serviceRequestID = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["rq_id"]!)"
        
//        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

          let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "serviceInformationDetailsVC") as! ServiceInformationDetailsViewController
        
        navigate.serviceRequestID = serviceRequestID
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    // MARK: - // Scrollview Delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            
//            print("next start : ",nextStart )
            
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                
                getServiceItemList()
            }
            
        }
    }
    
    // MARK:- // Textview Delegates
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            commentTXTView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    // MARK: - // JSON POST Method to get Booking Information Data

    func getServiceItemList()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_sell_service_item", param: parameters, completion: {
            self.bookingItemArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.bookingItemArray.count-1)

            {

                let tempDict = self.bookingItemArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)

            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.bookingItemArray.count

            print("Next Start Value : " , self.startValue)

            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.serviceItemTableview.delegate = self
                self.serviceItemTableview.dataSource = self
                self.serviceItemTableview.reloadData()
                self.serviceItemTableview.rowHeight = UITableView.automaticDimension
                self.serviceItemTableview.estimatedRowHeight = UITableView.automaticDimension
                SVProgressHUD.dismiss()
            }
        })
    }
    
    
    
    
    
    // MARK: - // JSON POST Method to Confirm Booking
    func confirmBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&ser_req_id=\(serviceRequestID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_sell_serviceitem_confirm", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()
            }

        })
    }
    
    
    
    // MARK: - // JSON POST Method to Cancel Booking
    func cancelBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&ser_req_id=\(serviceRequestID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_sell_serviceitem_cancel", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()
            }
        })
    }
    
    
    // MARK: - // JSON POST Method to Delete Booking
    func deleteBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&ser_req_id=\(serviceRequestID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_sell_serviceitem_delete", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()
            }
        })
    }
    
    
    // MARK: - // JSON POST Method to Send Review

    func sendReview()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&product_id=\(productID!)&ser_req_id=\(serviceRequestID!)&seller_id=\(sellerID!)&ratinginput=\(reviewRating!)&description=\(commentTXTView.text!)"

        self.CallAPI(urlString: "app_add_serviceitem_review", param: parameters, completion: {
            DispatchQueue.main.async {

                SVProgressHUD.dismiss()

                let alert = UIAlertController(title: "Rating Response", message: "\(self.globalJson["message"]!)", preferredStyle: .alert)

                let ok = UIAlertAction(title: "OK", style: .default) {
                    UIAlertAction in

                    UIView.animate(withDuration: 0.5)
                    {
                        self.view.sendSubviewToBack(self.reviewMainView)
                        self.reviewMainView.isHidden = true
                    }

                }

                alert.addAction(ok)

                self.present(alert, animated: true, completion: nil )

                self.globalDispatchgroup.leave()
            }
        })
    }
    
    
    
    
    // MARK:- // Confirm Booking
    
    
    @objc func confirmServiceTap(sender: UIButton)
    {
        
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            
            
            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
            
            self.serviceRequestID = "\(tempDictionary["rq_id"]!)"
            
            self.confirmBooking()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                self.startValue = 0
                
                self.recordsArray.removeAllObjects()
                
                self.getServiceItemList()
                
            })
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil )
        
        
    }
    
    // MARK:- // Cancel Booking
    
    
    @objc func cancelServiceTap(sender: UIButton)
    {
        
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            
            
            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
            
            self.serviceRequestID = "\(tempDictionary["rq_id"]!)"
            
            self.cancelBooking()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                self.recordsArray.removeAllObjects()
                
                self.startValue = 0
                
                self.getServiceItemList()
                
            })
            
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil )
        
    }
    
    
    // MARK:- // Delete Service
    
    
    @objc func deleteServiceTap(sender: UIButton)
    {
        
        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in
            
            
            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary
            
            self.serviceRequestID = "\(tempDictionary["rq_id"]!)"
            
            self.deleteBooking()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                self.recordsArray.removeAllObjects()
                
                self.startValue = 0
                
                self.getServiceItemList()
                
            })
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil )
        
        
    }
    
    // MARK:- // Cross User Review View
    
    @IBAction func crossButtonTap(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.reviewMainView)
            self.reviewMainView.isHidden = true
        }
        
    }
    
    // MARK:- // Save Review Button Tap
    
    @IBAction func saveButtonTap(_ sender: UIButton) {
        
        if reviewRating.elementsEqual("0")
        {
            let alert = UIAlertController(title: "Select Rating", message: "Please Select Any Valid Rating", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else
        {
            sendReview()
            self.globalDispatchgroup.notify(queue: .main, execute: {
                self.recordsArray.removeAllObjects()
                self.startValue = 0
                self.getServiceItemList()
                })
        }
        
    }
    
    
    // MARK:- // Add User Review
    
    @objc func addUserReview(sender: UIButton)
    {
        self.ViewUserReview.isHidden = true
        self.reviewMainView.isHidden = false
        UIView.animate(withDuration: 0.5)
        {
            self.rating.rating = 0
            self.commentTXTView.text = ""
            
            self.serviceRequestID = "\((self.recordsArray[sender.tag] as! NSDictionary)["rq_id"]!)"
            self.sellerID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_seller_id"]!)"
            self.productID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"
            
            self.view.bringSubviewToFront(self.reviewMainView)
            self.reviewMainView.isHidden = false
        }
    }

    //MARK:- //ViewUserReview
    @objc func ViewUserReview(sender: UIButton)
    {
        self.ViewUserReview.isHidden = false
        self.reviewMainView.isHidden = true
        self.view.bringSubviewToFront(self.ViewUserReview)
        self.ShowRating.settings.updateOnTouch = false
        UIView.animate(withDuration: 0.5)
        {
            let userRating = "\((((((self.recordsArray[sender.tag] as! NSDictionary)["review_section"]!) as! NSDictionary)["review_details"]!) as! NSDictionary)["reting_no"]!)"
            self.ViewComment.text = "\((((((self.recordsArray[sender.tag] as! NSDictionary)["review_section"]!) as! NSDictionary)["review_details"]!) as! NSDictionary)["comment"]!)"
            self.ViewDate.text = "\((((((self.recordsArray[sender.tag] as! NSDictionary)["review_section"]!) as! NSDictionary)["review_details"]!) as! NSDictionary)["datetime"]!)"
            self.ShowRating.rating = Double(userRating)!
        }

    }


    
    
    // Cosmos Format
    
    //
    
    public class func formatValue(_ value: Double) -> String {
        return String(format: "%.2f", value)
    }
    
    private func didTouchCosmos(_ rating: Double) {
        
        let ratingValue = ServiceItemViewController.formatValue(rating)
        
        reviewRating = ratingValue
        
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        
        let ratingValue = ServiceItemViewController.formatValue(rating)
        
        reviewRating = ratingValue
        
    }
    
    
    // MARK:- // ReviewMainView Multilingual setup
    
    func setupReviewMainView()
    {
        reviewTitleLBL.font = UIFont(name: reviewTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        ratingLBL.font = UIFont(name: ratingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        commentLBL.font = UIFont(name: commentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        commentTXTView.font = UIFont(name: (commentTXTView.font?.fontName)!, size: CGFloat(Get_fontSize(size: 15)))
        
        saveButtonOutlet.titleLabel?.font = UIFont(name: (saveButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!
        
        
        reviewTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_user_review", comment: "")
        ratingLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "rating", comment: "")
        commentLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "comment", comment: "")
        saveButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "save", comment: ""), for: .normal)
    }
    
    // MARK:- // Set FOnt
    
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
    }
    func NoData()
    {
        let nolbl = UILabel()
        nolbl.center = self.view.center
        nolbl.center.x = self.view.center.x
        nolbl.center.y = self.view.center.y
        nolbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        nolbl.textAlignment = .natural
        nolbl.textColor = UIColor.black
        view.addSubview(nolbl)
    }
}
//MARK:- //Service Items Tableview cell
class ServiceItemTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!

    @IBOutlet weak var bookingNumberLBL: UILabel!
    @IBOutlet weak var bookingNumber: UILabel!
    @IBOutlet weak var statusLBL: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var serviceNameLBL: UILabel!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var bookingDateLBL: UILabel!
    @IBOutlet weak var bookingDate: UILabel!
    @IBOutlet weak var bookingTimeLBL: UILabel!
    @IBOutlet weak var bookingTime: UILabel!
    @IBOutlet weak var servicePriceLBL: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var topSeparatorView: UIView!
    @IBOutlet weak var midSeparatorView: UIView!

    @IBOutlet weak var confirmServiceView: UIView!
    @IBOutlet weak var cofirmimageview: UIImageView!
    @IBOutlet weak var confirmLBL: UILabel!
    @IBOutlet weak var confirmserviceItemButton: UIButton!

    @IBOutlet weak var cancelServiceView: UIView!
    @IBOutlet weak var cancelImageview: UIImageView!
    @IBOutlet weak var calcelLBL: UILabel!
    @IBOutlet weak var cancelServiceItemButton: UIButton!

    @IBOutlet weak var deleteServiceView: UIView!
    @IBOutlet weak var deleteImageview: UIImageView!
    @IBOutlet weak var deleteLBL: UILabel!
    @IBOutlet weak var deleteServiceItemButton: UIButton!

    @IBOutlet weak var Delete_btn: UIButton!

    @IBOutlet weak var addUserReviewButton: UIButton!
    @IBOutlet weak var Add_userReviewBtn: UIButton!

    @IBOutlet weak var buttonsView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

