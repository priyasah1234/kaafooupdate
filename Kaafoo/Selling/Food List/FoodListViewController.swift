//

//  FoodListViewController.swift

//  Kaafoo

//

//  Created by IOS-1 on 12/02/19.

//  Copyright © 2019 ESOLZ. All rights reserved.

//

import UIKit
import SVProgressHUD

class FoodListViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate {
    @IBOutlet weak var FoodListingTableView: UITableView!
    var startValue = 0
    var perLoad = 10
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    var nextStart : String!
    var recordsArray : NSMutableArray! = NSMutableArray()
    var FoodListArray : NSMutableArray!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetFoodListAPI()
        self.SetFont()
        self.FoodListingTableView.separatorStyle = .none
        self.globalDispatchgroup.notify(queue: .main, execute: {
            if self.recordsArray.count == 0
            {
             self.ShowAlertMessage(title: "Warning", message: "No Information Found")
            }
            else
            {
               print("Do nothing")
            }
        })
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.FoodListingTableView.rowHeight = UITableView.automaticDimension
        self.FoodListingTableView.estimatedRowHeight = UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.recordsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "foodlistselling") as! FoodListSellingTableViewCell
        cell.order_no.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["order_no"]!)"
        cell.order_date.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["order_date"]!)"
        cell.buyer_name.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["buyer_name"]!)"
        cell.item_no.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["total_item"]!)"
        cell.item_price.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["total_price"]!)"
        cell.listView.layer.cornerRadius = 8.0
        cell.selectionStyle = .none
        //Set Font For UILabels Inside UItableViewCell
        cell.order_no.font = UIFont(name: cell.order_no.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.order_date.font = UIFont(name: cell.order_no.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.buyer_name.font = UIFont(name: cell.order_no.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.item_no.font = UIFont(name: cell.order_no.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.item_price.font = UIFont(name: cell.order_no.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.OrderNoLbl.font = UIFont(name: cell.order_no.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.dateLbl.font = UIFont(name: cell.order_no.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.TotalPriceLbl.font = UIFont(name: cell.order_no.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.TotalItemLbl.font = UIFont(name: cell.order_no.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.buyerNameLbl.font = UIFont(name: cell.order_no.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        //Set String Localization
        cell.OrderNoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "order_no", comment: "")
        cell.dateLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
        cell.TotalPriceLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalPrice", comment: "")
        cell.TotalItemLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalItem", comment: "")
        cell.buyerNameLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "buyerName", comment: "")
        return cell
    }
    //MARK:- ScrollView Delegate Methods
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
        }
        else
        {
//            print("next start : ",nextStart )
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                GetFoodListAPI()
            }
        }
    }
    //MARK:- //Get listing using global api
    func GetFoodListAPI()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_food_list_info", param: parameters, completion: {
            self.FoodListArray = self.globalJson["info_array"] as? NSMutableArray
            for i in 0...(self.FoodListArray.count-1)
            {
                let tempDict = self.FoodListArray[i] as! NSDictionary
                self.recordsArray.add(tempDict as! NSMutableDictionary)
            }
            self.nextStart = "\(self.globalJson["next_start"]!)"
            self.startValue = self.startValue + self.recordsArray.count
            print("Next Start Value : " , self.startValue)
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.FoodListingTableView.delegate = self
                self.FoodListingTableView.dataSource = self
                self.FoodListingTableView.reloadData()
                SVProgressHUD.dismiss()
            }
        })
    }
    func SetFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
    func NoData()
    {
        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
        nolbl.textAlignment = .right
        nolbl.text =  LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
        view.addSubview(nolbl)
        nolbl.bringSubviewToFront(view)
    }
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
//MARK:- //Food List Seller Listing Tableviewcell
class FoodListSellingTableViewCell: UITableViewCell {
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var order_no: UILabel!
    @IBOutlet weak var order_date: UILabel!
    @IBOutlet weak var buyer_name: UILabel!
    @IBOutlet weak var item_price: UILabel!
    @IBOutlet weak var item_no: UILabel!


    @IBOutlet weak var OrderNoLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var buyerNameLbl: UILabel!
    @IBOutlet weak var TotalItemLbl: UILabel!
    @IBOutlet weak var TotalPriceLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

