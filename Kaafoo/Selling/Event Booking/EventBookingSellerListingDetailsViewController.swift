//
//  EventBookingSellerListingDetailsViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 09/03/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class EventBookingSellerListingDetailsViewController: GlobalViewController {
    var infoArray : NSMutableDictionary!
    
    @IBOutlet weak var ContentView: UIView!
    
    var EventId : String!
    @IBOutlet weak var BookingnumberLbl: UILabel!
    @IBOutlet weak var EventNameLbl: UILabel!
    @IBOutlet weak var Bookingnumber: UILabel!
    @IBOutlet weak var EventName: UILabel!
    @IBOutlet weak var SellerNameLbl: UILabel!
    @IBOutlet weak var SellerName: UILabel!
    @IBOutlet weak var SellerEmailIdLbl: UILabel!
    @IBOutlet weak var SellerEmailId: UILabel!
    @IBOutlet weak var SellerPhNo: UILabel!
    @IBOutlet weak var SellerPh: UILabel!
    @IBOutlet weak var SellerMessageLbl: UILabel!
    @IBOutlet weak var SellerMessage: UILabel!
    @IBOutlet weak var SellerBookingDateLbl: UILabel!
    @IBOutlet weak var SellerBookingDate: UILabel!
    @IBOutlet weak var SellerBookingTimeLbl: UILabel!
    @IBOutlet weak var SellerBookingTime: UILabel!
    @IBOutlet weak var PeopleNoLbl: UILabel!
    @IBOutlet weak var peopleNo: UILabel!
    @IBOutlet weak var EventPriceLbl: UILabel!
    @IBOutlet weak var EventPrice: UILabel!
    @IBOutlet weak var EventStatusLbl: UILabel!
    @IBOutlet weak var EventStatus: UILabel!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDetails()
        self.SetFont()
        self.StringLocalization()

        // Do any additional setup after loading the view.
    }
    
    //API Fire Method
    func getDetails()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&eventid=\(EventId!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_event_booking_seller_details", param: parameters, completion: {
            self.infoArray = self.globalJson["info_array"] as? NSMutableDictionary
            DispatchQueue.main.async
                {
                    SVProgressHUD.dismiss()
                    self.globalDispatchgroup.leave()
            }
            self.globalDispatchgroup.notify(queue: .main, execute:
                {
                    self.dataFetch()
            })
        })
    }
    func dataFetch()
    {
        
        self.Bookingnumber.text =  "\(self.infoArray["booking_no"]!)"
        self.EventName.text  = "\(self.infoArray["event_name"]!)"
        self.SellerName.text = "\(self.infoArray["buyer_name"]!)"
        self.SellerEmailId.text = "\(self.infoArray["email"]!)"
        self.SellerPhNo.text = "\(self.infoArray["phone_no"]!)"
        self.SellerMessage.text = "\(self.infoArray["message"]!)"
        self.SellerBookingDate.text = "\(self.infoArray["booking_date"]!)"
        self.SellerBookingTime.text = "\(self.infoArray["booking_time"]!)"
        self.peopleNo.text = "\(self.infoArray["noof_people"]!)"
        self.EventPrice.text = "\(self.infoArray["event_price"]!)"
        self.EventStatus.text = "\(self.infoArray["status"]!)"
        
    }
    
    func SetFont()
    {
        self.Bookingnumber.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.BookingnumberLbl.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventStatus.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventStatusLbl.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventPrice.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventPriceLbl.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventName.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.EventNameLbl.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerMessage.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerMessageLbl.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerName.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerNameLbl.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerEmailId.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerEmailIdLbl.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.peopleNo.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.PeopleNoLbl.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerBookingDate.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerBookingDateLbl.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerBookingTime.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        self.SellerBookingTimeLbl.font = UIFont(name: self.Bookingnumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }

    //String Localization
    func StringLocalization()
    {
        self.BookingnumberLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "booking_no", comment: "")
        self.EventNameLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventname", comment: "")
        self.SellerEmailIdLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "emailId", comment: "")
        self.SellerMessageLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "message", comment: "")
        self.SellerBookingDateLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingDate", comment: "")
        self.SellerBookingTimeLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingTime", comment: "")
        self.PeopleNoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "numberofpeople", comment: "")
        self.EventPriceLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventprice", comment: "")
        self.EventStatusLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "status", comment: "")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
