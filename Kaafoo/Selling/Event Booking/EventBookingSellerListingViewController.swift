//
//  EventBookingSellerListingViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 08/03/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class EventBookingSellerListingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var EventBookingListingTableview: UITableView!
    
    
    var startValue = 0
    
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    
    var scrollEnd : CGFloat!
    
    
    
    var nextStart : String!
    
    
    
    var BookingId : String!
    
    
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    
    
    var EventSellerListingArray : NSMutableArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetEvenListing()
        self.EventBookingListingTableview.separatorStyle = .none
        self.globalDispatchgroup.notify(queue: .main, execute: {
            if self.recordsArray.count == 0
            {
               self.EventBookingListingTableview.isHidden = true
               self.noDataFound(mainview: self.view)
            }
            else
            {
               print("Do Nothing")
            }
        })

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.EventBookingListingTableview.rowHeight = UITableView.automaticDimension
        self.EventBookingListingTableview.estimatedRowHeight = UITableView.automaticDimension
    }
    
    //MARK:- Tableview Delegate and Datasource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "bookinglisting") as! EventBookingSellerListingTableViewCell
        obj.BookingDate.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["date"]!)"
        obj.BookingNumber.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["event_no"]!)"
        obj.BuyerName.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["buyer_name"]!)"
        obj.EventName.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["event_name"]!)"
        obj.NoOfPeople.text = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["people_count"]!)"
        
        //SET Font For UILABels inside TableViewCells
        
        obj.BookingDate.font = UIFont(name: obj.BookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.BookingNumber.font = UIFont(name: obj.BookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.BuyerName.font = UIFont(name: obj.BookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.EventName.font = UIFont(name: obj.BookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.NoOfPeople.font = UIFont(name: obj.BookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        obj.BookingDateLbl.font = UIFont(name: obj.BookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.BookingNumberLbl.font = UIFont(name: obj.BookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.BuyerNameLbl.font = UIFont(name: obj.BookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.EventNameLbl.font = UIFont(name: obj.BookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        obj.NoOfPeopleLbl.font = UIFont(name: obj.BookingDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        obj.BookingNumberLbl.textColor = .yellow2()


        //String Localization
        obj.BookingNumberLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "booking_no", comment: "")
        obj.BookingDateLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingDate", comment: "")
        obj.EventNameLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventname", comment: "")
        obj.NoOfPeopleLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "numberofpeople", comment: "")
        obj.BuyerNameLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "buyerName", comment: "")

        obj.CellView.layer.cornerRadius = 8.0
//        obj.ShadowView.layer.cornerRadius = 8.0
        obj.selectionStyle = .none
        return obj
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "eventbookingsellerlistingdetails") as! EventBookingSellerListingDetailsViewController
        nav.EventId = "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["id"]!)"
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    //API Data Fetching Method
    func GetEvenListing()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_event_booking_seller_list", param: parameters, completion: {
            self.EventSellerListingArray = self.globalJson["info_array"] as? NSMutableArray
            for i in 0...(self.EventSellerListingArray.count-1)
            {
                let tempDict = self.EventSellerListingArray[i] as! NSDictionary
                self.recordsArray.add(tempDict as! NSMutableDictionary)
            }
            self.nextStart = "\(self.globalJson["next_start"]!)"
            self.startValue = self.startValue + self.recordsArray.count
            print("Next Start Value : " , self.startValue)
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.EventBookingListingTableview.delegate = self
                self.EventBookingListingTableview.dataSource = self
                self.EventBookingListingTableview.reloadData()
                SVProgressHUD.dismiss()
            }
        })
    }
    func SetFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
    
    //MARK:- ScrollView Delegate Methods
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        scrollBegin = scrollView.contentOffset.y
        
    }
    
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        scrollEnd = scrollView.contentOffset.y
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollBegin > scrollEnd
            
        {
        }
        else
        {
//            print("next start : ",nextStart )
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                GetEvenListing()
            }
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK:- //Event Booking Seller Listing Tableviewcell
class EventBookingSellerListingTableViewCell: UITableViewCell {

    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var BookingNumberLbl: UILabel!
    @IBOutlet weak var BookingDateLbl: UILabel!
    @IBOutlet weak var BookingNumber: UILabel!
    @IBOutlet weak var BookingDate: UILabel!
    @IBOutlet weak var EventNameLbl: UILabel!
    @IBOutlet weak var EventName: UILabel!
    @IBOutlet weak var BuyerNameLbl: UILabel!
    @IBOutlet weak var NoOfPeopleLbl: UILabel!
    @IBOutlet weak var BuyerName: UILabel!
    @IBOutlet weak var NoOfPeople: UILabel!
    @IBOutlet weak var ShadowView: ShadowView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
