//
//  ItemsSoldViewController.swift
//  Kaafoo
//
//  Created by admin on 31/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import Cosmos

//struct hello : Codable
//{
//    var hhhk: String?
//    var hjhjkjkj : String?
//}

class ItemsSoldViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {

    // Global Font Applied

    @IBOutlet weak var itemsSoldTableView: UITableView!
    @IBOutlet weak var reviewMainView: UIView!
    @IBOutlet weak var reviewWindowView: UIView!
    @IBOutlet weak var reviewTitleLBL: UILabel!
    @IBOutlet weak var crossImageview: UIImageView!
    @IBOutlet weak var crossButtonOutlet: UIButton!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var ratingLBL: UILabel!
    @IBOutlet weak var commentLBL: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentTXTView: UITextView!
    @IBOutlet weak var saveButtonOutlet: UIButton!

    @IBOutlet weak var showReviewMainView: UIView!
    @IBOutlet weak var showReviewWindowView: UIView!
    @IBOutlet weak var showReviewRatingLBL: UILabel!
    @IBOutlet weak var showReviewRating: CosmosView!
    @IBOutlet weak var showReviewCommentLBL: UILabel!
    @IBOutlet weak var showReviewComment: UILabel!
    @IBOutlet weak var showReviewDatetimeLBL: UILabel!
    @IBOutlet weak var showReviewDatetime: UILabel!



    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var itemsWonArray : NSMutableArray! = NSMutableArray()

    var recordsArray : NSMutableArray! = NSMutableArray()


    var reviewType : String!

    var reviewRating : String! = "0"

    var orderID : String!

    var sellerID : String!

    var productID : String!



    override func viewDidLoad() {
        super.viewDidLoad()

        itemsSold()

        self.globalDispatchgroup.notify(queue: .main, execute:
            {
                if self.recordsArray.count == 0
                {
                    self.itemsSoldTableView.isHidden = true
                    self.noDataFound(mainview: self.view)
                }
                else
                {
                    print("do nothing")
                }
        })

        set_font()

        setupReviewMainView()

        setupShowReviewMainView()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        self.commentView.layer.borderWidth = 2
        self.commentView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        self.commentView.layer.cornerRadius = (5/568)*self.FullHeight

        rating.didTouchCosmos = didTouchCosmos
        rating.didFinishTouchingCosmos = didFinishTouchingCosmos

        commentTXTView.delegate = self


    }


    // MARK:- // Delegate Methods

    // MARK:- // Tableview Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! ItemsSoldTVCTableViewCell

        cell.productTitle.text = "\((recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        cell.soldOn.text = "Sold on " + "\((recordsArray[indexPath.row] as! NSDictionary)["booking_time"]!)"
        cell.price.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["order_price"]!)"
        cell.BuyerName.text = "Buyer Name" + " " + "\((recordsArray[indexPath.row] as! NSDictionary)["buyer_name"]!)"

        let path = "\((recordsArray[indexPath.row] as! NSDictionary)["image"]!)"
        cell.productImage.sd_setImage(with: URL(string: path))

        cell.userReviewOutlet.tag = indexPath.row
        cell.productReviewOutlet.tag = indexPath.row

        cell.viewUserReview.tag = indexPath.row
        cell.viewProductReview.tag = indexPath.row

        cell.userReviewOutlet.addTarget(self, action: #selector(ItemsSoldViewController.addUserReview(sender:)), for: .touchUpInside)
        cell.productReviewOutlet.addTarget(self, action: #selector(ItemsSoldViewController.addProductReview(sender:)), for: .touchUpInside)

        cell.viewUserReview.addTarget(self, action: #selector(ItemsSoldViewController.showUserReview(sender:)), for: .touchUpInside)
        cell.viewProductReview.addTarget(self, action: #selector(ItemsSoldViewController.showProductReview(sender:)), for: .touchUpInside)

        cell.userReviewOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_user_review", comment: ""), for: .normal)
        cell.productReviewOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_product_review", comment: ""), for: .normal)
        cell.viewUserReview.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "view_user_review", comment: ""), for: .normal)
        cell.viewProductReview.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "view_product_review", comment: ""), for: .normal)


        // Set Font //////

        cell.productTitle.font = UIFont(name: cell.productTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.soldOn.font = UIFont(name: cell.soldOn.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.price.font = UIFont(name: cell.price.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.userReviewOutlet.titleLabel?.font = UIFont(name: (cell.userReviewOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 10)))
        cell.productReviewOutlet.titleLabel?.font = UIFont(name: (cell.productReviewOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 10)))

        cell.viewUserReview.titleLabel?.font = UIFont(name: (cell.viewUserReview.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 10)))
        cell.viewProductReview.titleLabel?.font = UIFont(name: (cell.viewProductReview.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 10)))

        /////////////////

        cell.productReviewOutlet.layer.cornerRadius = cell.productReviewOutlet.frame.size.height / 2.0
        cell.userReviewOutlet.layer.cornerRadius = cell.userReviewOutlet.frame.size.height / 2.0

        cell.viewUserReview.layer.cornerRadius = cell.viewUserReview.frame.size.height / 2.0
        cell.viewProductReview.layer.cornerRadius = cell.viewProductReview.frame.size.height / 2.0


        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none


        return cell

    }


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        let cell = cell as! ItemsSoldTVCTableViewCell

        // Check fow show or add user Review
        if ((recordsArray[indexPath.row] as! NSDictionary)["user_review"] as! NSArray).count == 0
        {
            //            cell.userReviewOutlet.setTitle("Add User Review", for: .normal)
            //
            //            cell.userReviewOutlet.addTarget(self, action: #selector(ItemsSoldViewController.addUserReview(sender:)), for: .touchUpInside)

            cell.userReviewOutlet.isHidden = false
            cell.viewUserReview.isHidden = true

        }
        else
        {
            //            cell.userReviewOutlet.setTitle("View User Review", for: .normal)
            //
            //            cell.userReviewOutlet.addTarget(self, action: #selector(ItemsSoldViewController.showUserReview(sender:)), for: .touchUpInside)

            cell.userReviewOutlet.isHidden = true
            cell.viewUserReview.isHidden = false

        }


        // Check for show or add Product Review
        if ((recordsArray[indexPath.row] as! NSDictionary)["product_review"] as! NSArray).count == 0
        {
            //            cell.productReviewOutlet.setTitle("Add Product Review", for: .normal)
            //
            //            cell.productReviewOutlet.addTarget(self, action: #selector(ItemsSoldViewController.addProductReview(sender:)), for: .touchUpInside)

            cell.productReviewOutlet.isHidden = false
            cell.viewProductReview.isHidden = true

        }
        else
        {
            //            cell.productReviewOutlet.setTitle("View Product Review", for: .normal)
            //            cell.productReviewOutlet.addTarget(self, action: #selector(ItemsSoldViewController.showProductReview(sender:)), for: .touchUpInside)

            cell.productReviewOutlet.isHidden = true
            cell.viewProductReview.isHidden = false

        }

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        return ((170/568)*FullHeight)
        return UITableView.automaticDimension
    }



    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }


            }
            else
            {
                itemsSold()
            }

        }
    }


    // MARK:- // Textview Delegates

    //    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    //        if(text == "\n") {
    //            commentTXTView.resignFirstResponder()
    //            return false
    //        }
    //        return true
    //    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.commentTXTView.text = ""
    }
    //    func textViewDidEndEditing(_ textView: UITextView) {
    //        return true
    //    }

    // MARK: - // JSON POST Method to get Items I am Selling Data
    func itemsSold()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        //        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_sold_items", param: parameters, completion: {

            self.itemsWonArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.itemsWonArray.count - 1)

            {
                let tempDict = self.itemsWonArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)
            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.itemsWonArray.count

            print("Next Start Value : " , self.startValue)
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.itemsSoldTableView.delegate = self
                self.itemsSoldTableView.dataSource = self
                self.itemsSoldTableView.reloadData()
                self.itemsSoldTableView.rowHeight = UITableView.automaticDimension
                self.itemsSoldTableView.estimatedRowHeight = UITableView.automaticDimension
                SVProgressHUD.dismiss()
            }
        })
    }


    // MARK: - // JSON POST Method to Send Review
    func sendReview()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&product_id=\(productID!)&order_id=\(orderID!)&seller_id=\(sellerID!)&ratinginput=\(reviewRating!)&description=\(commentTXTView.text!)&add_rev_type=\(reviewType!)"
        self.CallAPI(urlString: "app_add_user_review", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                let alert = UIAlertController(title: "Rating Response", message: "\(self.globalJson["message"]!)", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default) {
                    UIAlertAction in
                    UIView.animate(withDuration: 0.5)
                    {
                        self.view.sendSubviewToBack(self.reviewMainView)
                        self.reviewMainView.isHidden = true
                    }
                }
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
                self.globalDispatchgroup.leave()

            }
        })
    }




    // MARK:- // Add User Review

    @objc func addUserReview(sender: UIButton)
    {
        UIView.animate(withDuration: 0.5)
        {
            self.rating.rating = 0
            self.commentTXTView.text = ""

            self.reviewTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_user_review", comment: "")

            self.reviewType = "U"

            self.orderID = "\((self.recordsArray[sender.tag] as! NSDictionary)["order_id"]!)"
            self.sellerID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_seller_id"]!)"
            self.productID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"

            self.view.bringSubviewToFront(self.reviewMainView)
            self.reviewMainView.isHidden = false
        }
    }

    // MARK:- // Add Product Review

    @objc func addProductReview(sender: UIButton)
    {
        UIView.animate(withDuration: 0.5)
        {
            self.rating.rating = 0
            self.commentTXTView.text = ""

            self.reviewTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_product_review", comment: "")

            self.reviewType = "P"

            self.orderID = "\((self.recordsArray[sender.tag] as! NSDictionary)["order_id"]!)"
            self.sellerID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_seller_id"]!)"
            self.productID = "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)"

            self.view.bringSubviewToFront(self.reviewMainView)
            self.reviewMainView.isHidden = false
        }
    }

    // MARK:- // Show User Review

    @objc func showUserReview(sender: UIButton)
    {
        UIView.animate(withDuration: 0.5)
        {

            let userRating = "\((((self.recordsArray[sender.tag] as! NSDictionary)["user_review"] as! NSArray)[0] as! NSDictionary)["user_review_rating_no"]!)"

            self.showReviewRating.rating = Double(userRating)!

            self.commentTXTView.text = "\((((self.recordsArray[sender.tag] as! NSDictionary)["user_review"] as! NSArray)[0] as! NSDictionary)["user_review_comment"]!)"

            self.showReviewDatetime.text = "\((((self.recordsArray[sender.tag] as! NSDictionary)["user_review"] as! NSArray)[0] as! NSDictionary)["user_review_date"]!)"

            self.view.bringSubviewToFront(self.showReviewMainView)
            self.showReviewMainView.isHidden = false
        }
    }


    // MARK:- // Show Product Review

    @objc func showProductReview(sender: UIButton)
    {
        UIView.animate(withDuration: 0.5)
        {

            let userRating = "\((((self.recordsArray[sender.tag] as! NSDictionary)["product_review"] as! NSArray)[0] as! NSDictionary)["product_review_rating_no"]!)"

            self.showReviewRating.rating = Double(userRating)!

            self.commentTXTView.text = "\((((self.recordsArray[sender.tag] as! NSDictionary)["product_review"] as! NSArray)[0] as! NSDictionary)["product_review_comment"]!)"

            self.showReviewDatetime.text = "\((((self.recordsArray[sender.tag] as! NSDictionary)["product_review"] as! NSArray)[0] as! NSDictionary)["product_review_date"]!)"

            self.view.bringSubviewToFront(self.showReviewMainView)
            self.showReviewMainView.isHidden = false
        }
    }


    // MARK:- // Cross Review

    @IBAction func tapOnCrossCommentView(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.reviewMainView)
            self.reviewMainView.isHidden = true
        }

    }


    // MARK:- // OK Button

    @IBOutlet weak var okButtonOutlet: UIButton!
    @IBAction func tapOnOK(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.showReviewMainView)
            self.showReviewMainView.isHidden = true
        }
    }



    // MARK:- // Save Review Button

    @IBAction func saveReview(_ sender: UIButton) {

        if reviewRating.elementsEqual("0")
        {
            let alert = UIAlertController(title: "Select Rating", message: "Please Select Any Valid Rating", preferredStyle: .alert)

            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)

            alert.addAction(ok)

            self.present(alert, animated: true, completion: nil )
        }
        else
        {
            sendReview()
            self.globalDispatchgroup.notify(queue: .main, execute: {
                self.recordsArray.removeAllObjects()
                self.startValue = 0
                self.itemsSold()
            })
        }

    }


    //

    public class func formatValue(_ value: Double) -> String {
        return String(format: "%.2f", value)
    }

    private func didTouchCosmos(_ rating: Double) {

        let ratingValue = ItemsWonViewController.formatValue(rating)

        reviewRating = ratingValue

    }

    private func didFinishTouchingCosmos(_ rating: Double) {

        let ratingValue = ItemsWonViewController.formatValue(rating)

        reviewRating = ratingValue

    }

    // MARK:- // ReviewMainView Multilingual setup

    func setupReviewMainView()
    {
        reviewTitleLBL.font = UIFont(name: reviewTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        ratingLBL.font = UIFont(name: ratingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        commentLBL.font = UIFont(name: commentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        commentTXTView.font = UIFont(name: (commentTXTView.font?.fontName)!, size: CGFloat(Get_fontSize(size: 15)))

        saveButtonOutlet.titleLabel?.font = UIFont(name: (saveButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!


        reviewTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_user_review", comment: "")
        ratingLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "rating", comment: "")
        commentLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "comment", comment: "")
        saveButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "save", comment: ""), for: .normal)
    }


    // MARK:- // ShowReviewMainView Multilingual setup

    func setupShowReviewMainView()
    {
        showReviewRatingLBL.font = UIFont(name: showReviewRatingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        showReviewCommentLBL.font = UIFont(name: showReviewCommentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        showReviewComment.font = UIFont(name: showReviewComment.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        showReviewDatetimeLBL.font = UIFont(name: showReviewDatetimeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        showReviewDatetime.font = UIFont(name: showReviewDatetime.font.fontName, size: CGFloat(Get_fontSize(size: 14)))


        okButtonOutlet.titleLabel?.font = UIFont(name: (okButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 17)))!

        showReviewRatingLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "rating", comment: "")
        showReviewCommentLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "comment", comment: "")
        showReviewDatetimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "datetime", comment: "")
        okButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "ok", comment: ""), for: .normal)
    }


    // MARK:- // SETTING FONTS

    func set_font()
    {

        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
    //    func NoData()
    //    {
    //        let nolbl = UILabel(frame: CGRect(x: (110/320)*self.FullWidth, y: (258/568)*self.FullHeight, width: (100/320)*self.FullWidth, height: (40/568)*self.FullHeight))
    //        nolbl.textAlignment = .right
    //        nolbl.text =  LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_data_found", comment: "")
    //        view.addSubview(nolbl)
    //        nolbl.bringSubview(toFront: view)
    //    }
    //

}
//MARK:- // Items Sold Tableviewcell
class ItemsSoldTVCTableViewCell: UITableViewCell {


    @IBOutlet weak var BuyerName: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var soldOn: UILabel!
    @IBOutlet weak var productReviewOutlet: UIButton!
    @IBOutlet weak var userReviewOutlet: UIButton!

    @IBOutlet weak var viewProductReview: UIButton!
    @IBOutlet weak var viewUserReview: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


