//
//  BookingItemViewController.swift
//  Kaafoo
//
//  Created by admin on 14/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class BookingItemViewController: GlobalViewController {

    // Global Font Applied

    @IBOutlet weak var bookingItemTableview: UITableView!


    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var recordsArray : NSMutableArray! = NSMutableArray()
    var bookingItemArray : NSMutableArray!

    var bookingRequestID : String!

    var cell : BookingItemTVCTableViewCell!


    // MARK:- // View Did Load

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getBookingItemList()
        self.globalDispatchgroup.notify(queue: .main, execute: {
            if self.recordsArray.count == 0
            {
                self.bookingItemTableview.isHidden = true
                self.noDataFound(mainview: self.view)
            }
            else
            {
                print("do nothing")
            }
        })



        set_font()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

    }

    // MARK:- // View will appear

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.bookingItemTableview.rowHeight = UITableView.automaticDimension
        self.bookingItemTableview.estimatedRowHeight = UITableView.automaticDimension

    }




    // MARK: - // JSON POST Method to get Booking Information Data

    func getBookingItemList()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(perLoad)&start_value=\(startValue)"
        self.CallAPI(urlString: "app_bookingitem_list", param: parameters, completion: {

            self.bookingItemArray = self.globalJson["info_array"] as? NSMutableArray

            for i in 0...(self.bookingItemArray.count-1)

            {

                let tempDict = self.bookingItemArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)

            }

            self.nextStart = "\(self.globalJson["next_start"]!)"

            self.startValue = self.startValue + self.bookingItemArray.count

            print("Next Start Value : " , self.startValue)

            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                self.bookingItemTableview.delegate = self
                self.bookingItemTableview.dataSource = self
                self.bookingItemTableview.reloadData()
                SVProgressHUD.dismiss()
            }
        })
    }



    // MARK: - // JSON POST Method to Delete Booking

    func deleteBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&booking_rq_id=\(bookingRequestID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_booking_item_delete", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()
            }
        })
    }



    // MARK: - // JSON POST Method to Confirm Booking

    func confirmBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&booking_rq_id=\(bookingRequestID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_booking_item_confirm", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()

            }
        })
    }



    // MARK: - // JSON POST Method to Cancel Booking

    func cancelBooking()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "user_id=\(userID!)&booking_rq_id=\(bookingRequestID!)&lang_id=\(langID!)"
        self.CallAPI(urlString: "app_booking_item_cancel", param: parameters, completion: {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.globalDispatchgroup.leave()

            }
        })
    }



    // MARK:- // Delete Booking Selector Method

    @objc func deleteBookingTap(sender: UIButton)
    {

        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in


            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary

            self.bookingRequestID = "\(tempDictionary["id"]!)"

            self.deleteBooking()

            self.globalDispatchgroup.notify(queue: .main, execute: {

                self.startValue = 0

                self.recordsArray.removeAllObjects()

                self.getBookingItemList()

            })

        }

        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alert.addAction(ok)
        alert.addAction(cancel)

        self.present(alert, animated: true, completion: nil )


    }



    // MARK:- // Confirm Booking Selector Method

    @objc func confirmBookingTap(sender: UIButton)
    {

        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in


            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary

            self.bookingRequestID = "\(tempDictionary["id"]!)"

            self.confirmBooking()

            self.globalDispatchgroup.notify(queue: .main, execute: {

                self.startValue = 0

                self.recordsArray.removeAllObjects()

                self.getBookingItemList()

            })
        }

        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alert.addAction(ok)
        alert.addAction(cancel)

        self.present(alert, animated: true, completion: nil )

    }


    // MARK:- // Cancel Booking Selector Method

    @objc func cancelBookingTap(sender: UIButton)
    {

        let alert = UIAlertController(title: "Confirm", message: "Are You Sure?", preferredStyle: .alert)

        let ok = UIAlertAction(title: "OK", style: .default) {
            UIAlertAction in


            let tempDictionary = self.recordsArray[sender.tag] as! NSDictionary

            self.bookingRequestID = "\(tempDictionary["id"]!)"

            self.cancelBooking()

            self.globalDispatchgroup.notify(queue: .main, execute: {

                self.startValue = 0

                self.recordsArray.removeAllObjects()

                self.getBookingItemList()

            })

        }

        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alert.addAction(ok)
        alert.addAction(cancel)

        self.present(alert, animated: true, completion: nil )

    }


    // MARK:- // Set FOnt

    func set_font()
    {

        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

    }



}



// MARK:- // Tableview Delegates

extension BookingItemViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return recordsArray.count

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! BookingItemTVCTableViewCell


        // Put Value
        cell.bookingNumber.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["booking_no"]!)"
        cell.status.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["status_name"]!)"
        cell.productName.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["product_name"]!)"
        cell.pickUpDate.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["pick_up_date"]!)"
        cell.totalPrice.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["total_price"]!)"
        cell.dropOffDate.text = "\((recordsArray[indexPath.row] as! NSMutableDictionary)["drop_off_date"]!)"


        // Localize Static Texts
        cell.bookingNumberLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "booking_no", comment: "")
        cell.statusLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "status", comment: "")
        cell.productNameLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_name", comment: "")
        cell.pickUpDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "pickup_date", comment: "")
        cell.totalPriceLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "totalPrice", comment: "")
        cell.dropOffDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "dropoff_date", comment: "")
        //        cell.confirmLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "confirm", comment: "")
        //
        //        cell.calcelLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: "")
        //
        //        cell.deleteLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "delete", comment: "")


        // Buttons
        cell.confirmBookingItemButton.tag = indexPath.row

        cell.confirmBookingItemButton.addTarget(self, action: #selector(BookingItemViewController.confirmBookingTap(sender:)), for: .touchUpInside)

        cell.cancelBookingItemButton.tag = indexPath.row

        cell.cancelBookingItemButton.addTarget(self, action: #selector(BookingItemViewController.cancelBookingTap(sender:)), for: .touchUpInside)

        cell.deleteBookingItemButton.tag = indexPath.row

        cell.deleteBookingItemButton.addTarget(self, action: #selector(BookingItemViewController.deleteBookingTap(sender:)), for: .touchUpInside)



        ////
        //        if  "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button"]!)".isEmpty
        //        {
        //            cell.confirmBookingItemButton.isHidden = true
        //        }
        //        else
        //        {
        //            cell.confirmBookingItemButton.isHidden = false
        //        }
        //
        //
        //        if  "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button2"]!)".isEmpty
        //        {
        //            cell.cancelBookingItemButton.isHidden = true
        //
        //        }
        //        else
        //        {
        //            cell.cancelBookingItemButton.isHidden = false
        //        }





        //        //ButtonsView Checking
        //        if  "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button"]!)".isEmpty && "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button2"]!)".isEmpty && "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["delete_button"]!)".isEmpty
        //        {
        //
        //        }
        //        else
        //        {
        //
        //        }


        if  "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button"]!)".isEmpty {

            cell.confirmBookingItemButton.isHidden = true

            if "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["button2"]!)".isEmpty {

                cell.cancelBookingItemButton.isHidden = true

                cell.confirmAndCancelStackview.isHidden = true

                if  "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["delete_button"]!)".isEmpty {

                    cell.deleteBookingItemButton.isHidden = true

                    cell.cellButtons.isHidden = true

                }
                else {
                    cell.deleteBookingItemButton.isHidden = false

                    cell.cellButtons.isHidden = false
                }
            }
            else {
                cell.cancelBookingItemButton.isHidden = false

                cell.confirmAndCancelStackview.isHidden = false
            }
        }
        else {
            cell.confirmBookingItemButton.isHidden = false

            if  "\((self.recordsArray[indexPath.row] as! NSMutableDictionary)["delete_button"]!)".isEmpty {
                cell.deleteBookingItemButton.isHidden = true
            }
            else {
                cell.deleteBookingItemButton.isHidden = false
            }

        }


        //////// Set Font

        cell.bookingNumberLBL.font = UIFont(name: cell.bookingNumberLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.bookingNumber.font = UIFont(name: cell.bookingNumber.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.statusLBL.font = UIFont(name: cell.statusLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.status.font = UIFont(name: cell.status.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.productNameLBL.font = UIFont(name: cell.productNameLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.productName.font = UIFont(name: cell.productName.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.pickUpDateLBL.font = UIFont(name: cell.pickUpDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.pickUpDate.font = UIFont(name: cell.pickUpDate.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalPriceLBL.font = UIFont(name: cell.totalPriceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.totalPrice.font = UIFont(name: cell.totalPrice.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.dropOffDateLBL.font = UIFont(name: cell.dropOffDateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
        cell.dropOffDate.font = UIFont(name: cell.dropOffDate.font.fontName, size: CGFloat(Get_fontSize(size: 13)))

        // no selection style
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        return cell

    }





    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        bookingRequestID = "\((recordsArray[indexPath.row] as! NSDictionary)["id"]!)"

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bookingInformationDetailVC") as! BookingInformationDetailViewController

        navigate.bookingRequestID = bookingRequestID

        self.navigationController?.pushViewController(navigate, animated: true)

    }

}



// MARK:- // Scrollview Delegates

extension BookingItemViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                getBookingItemList()
            }

        }
    }

}








//MARK :- //Booking Items Tableviewcell

class BookingItemTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellContents: UIView!
    @IBOutlet weak var cellButtons: UIView!

    @IBOutlet weak var bookingNumberLBL: UILabel!
    @IBOutlet weak var bookingNumber: UILabel!
    @IBOutlet weak var statusLBL: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var productNameLBL: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var pickUpDateLBL: UILabel!
    @IBOutlet weak var pickUpDate: UILabel!
    @IBOutlet weak var totalPriceLBL: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var dropOffDateLBL: UILabel!
    @IBOutlet weak var dropOffDate: UILabel!
    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var topSeparatorView: UIView!
    @IBOutlet weak var midSeparatorView: UIView!

    @IBOutlet weak var confirmAndCancelStackview: UIStackView!
    @IBOutlet weak var confirmBookingItemButton: UIButton!
    @IBOutlet weak var cancelBookingItemButton: UIButton!
    @IBOutlet weak var deleteBookingItemButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}


