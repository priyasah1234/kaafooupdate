//
//  NewFoodListingViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 7/2/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewFoodListingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var totalCategoryProductArray : NSMutableArray! = NSMutableArray()
    
    var storedOffsets = [Int: CGFloat]()
    
    var GetFoodListingStructArray = [GetFoodListingStruct]()

    var tempArray = ["HD Family MI","HD Family","Family MI"]
    
    var startValue = 0
    
    var perLoad = 10
    
    var sellerID : String!
    
    var listingType : String! =  "P"
    
    var searchCategoryID : String!
    
    var nextStart : String!
    
    var showMap : Bool! = true
    
    var searchCategoryName : String!
    
    var seller : String!
    
     var sellerImageURL : String!
    
    var RowIndex : Int!

    var currentIndexPath = [IndexPath]()
    
    var featureFoodDetails = [foodDetails]()
    
    var MainFoodDetails = [mainFoodDetails]()


    @IBAction func SortBtn(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            
            if self.filterView.isHidden == true {
                
                self.view.bringSubviewToFront(self.filterView)
                
                self.filterView.isHidden = false
            }
            else {
                self.view.sendSubviewToBack(self.filterView)
                
                self.filterView.isHidden = true
            }
        }

    }
    @IBOutlet weak var SortBtnOutlet: UIButton!
    
    @IBAction func ClosePickerView(_ sender: UIButton) {
        self.ShowPickerView.isHidden = true
    }
    @IBOutlet weak var ClosePickerViewBtn: UIButton!
    @IBAction func ShowCategoryPicker(_ sender: UIButton) {
        if ShowPickerView.isHidden == true
        {
            ShowPickerView.isHidden = false
        }
        else
        {
            ShowPickerView.isHidden = true
        }
    }
    
    @IBOutlet weak var ShowCategoryPickerOutlet: UIButton!
    
    @IBOutlet weak var CategoryLbl: UILabel!
    
    @IBOutlet weak var CategoryPickerView: UIPickerView!
    
    @IBOutlet weak var ShowPickerView: UIView!
    
    @IBOutlet weak var SellerName: UILabel!
    
    @IBOutlet weak var SellerImage: UIImageView!
    
    @IBOutlet weak var PaymentTableviewHeightConstriant: NSLayoutConstraint!
    
    var allDataDictionary : NSMutableDictionary!
    
    var reviewArray : NSArray!
    
    var infoDictionary : NSDictionary!
    
     var paymentTypeArray : NSArray!
    
    var mapArray = [NSDictionary]()
    var dataArray = [NSDictionary]()
    var recordsArray : NSMutableArray! = NSMutableArray()
    var currentRecordsArray : NSMutableArray! = NSMutableArray()
    var restaurantListArray = NSMutableArray()
    var popularDishesArray = NSMutableArray()
    var categoryListArray = NSMutableArray()
    var PickerViewCategoryListArray : NSMutableArray! = NSMutableArray()


    
    @IBOutlet weak var PaymentTableviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var FoodMapView: mapView!
    @IBOutlet weak var InfoView: UIView!
    @IBOutlet weak var BlurBiggerView: UIView!

    @IBAction func MapBtn(_ sender: UIButton) {
        if self.showMap == true {

            self.startValue = 0

            self.listingType = "C"

            self.MapBtnOutlet.setImage(UIImage(named: "list"), for: .normal)

            self.getFoodListings()

            self.globalDispatchgroup.notify(queue: .main) {

                self.FoodMapView.getData(dataArray: self.mapArray, listingDataArray: self.currentRecordsArray as! [NSDictionary])

                self.accordingToListingType()

            }
        }
        else {
            self.listingType = "P"

            self.FoodListingTableView.isHidden = false
            self.ReviewsView.isHidden = true
            self.InfoView.isHidden = true
            self.FoodMapView.isHidden = true

            self.MapBtnOutlet.setImage(UIImage(named: "Map-1"), for: .normal)

            self.showMap = true
        }
    }
    @IBOutlet weak var MapBtnOutlet: UIButton!
    @IBOutlet weak var ReviewsTableView: UITableView!
    @IBOutlet weak var ReviewsLbl: UILabel!
    @IBOutlet weak var ReviewsView: UIView!
    
    @IBOutlet weak var PaymentTableview: UITableView!
    @IBOutlet weak var DeliveryTimeTableviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var DeliveryTimeTableview: UITableView!
    @IBAction func CrossBiggerBtn(_ sender: UIButton) {
        self.BlurBiggerView.isHidden = true
    }
    @IBOutlet weak var ShowReviewsBtnOutlet: UIButton!
    @IBAction func ShowReviewsBtn(_ sender: UIButton) {
        self.startValue = 0

        self.listingType = "R"

        self.MapBtnOutlet.setImage(UIImage(named: "list"), for: .normal)

        self.showMap = false

        self.getFoodListings()

        self.globalDispatchgroup.notify(queue: .main) {

            self.accordingToListingType()

//            self.setTableviewHeight(tableview: self.ReviewsTableView, heightConstraint: self.reviewsTableHeightConstraint)

        }


    }
    @IBAction func InfoBtn(_ sender: UIButton) {
        self.startValue = 0

        self.listingType = "C"

        self.MapBtnOutlet.setImage(UIImage(named: "list"), for: .normal)

        self.showMap = false

        self.getFoodListings()

        self.globalDispatchgroup.notify(queue: .main) {

            self.accordingToListingType()

            self.setTableviewHeight(tableview: self.DeliveryTimeTableview, heightConstraint: self.DeliveryTimeTableviewHeightConstraint)
//
            self.setTableviewHeight(tableview: self.PaymentTableview, heightConstraint: self.PaymentTableviewHeightConstraint)

        }
    }
    @IBOutlet weak var InfoBtnOutlet: UIButton!
    
    
    @IBOutlet weak var BiggerImageCollectionView: UICollectionView!
    fileprivate var tableViewCellCoordinator: [Int: IndexPath] = [:]
    @IBOutlet weak var FoodListingTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()



        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        self.searchCategoryID = UserDefaults.standard.string(forKey: "foodCourtSearchCategoryID")
        self.searchCategoryName = UserDefaults.standard.string(forKey: "foodCourtSearchCategoryName")
        self.seller = UserDefaults.standard.string(forKey: "foodCourtSellerName")
        self.sellerID = UserDefaults.standard.string(forKey: "foodCourtSellerID")
        self.sellerImageURL = UserDefaults.standard.string(forKey: "foodCourtSellerImageURL")


        self.getFoodListings()

        self.SetData()
        self.FoodMapView.isHidden = true
        self.InfoView.isHidden = true
        self.ReviewsView.isHidden = true
        
        
//        self.FoodListingTableView.delegate = self
//        self.FoodListingTableView.dataSource = self
//        self.FoodListingTableView.reloadData()

        // Do any additional setup after loading the view.
    }


    override func viewWillAppear(_ animated: Bool) {

        self.ReviewsTableView.estimatedRowHeight = 500
        self.ReviewsTableView.rowHeight = UITableView.automaticDimension

        self.FoodListingTableView.estimatedRowHeight = 300
        self.FoodListingTableView.rowHeight = UITableView.automaticDimension

        self.DeliveryTimeTableview.estimatedRowHeight = 300
        self.DeliveryTimeTableview.rowHeight = UITableView.automaticDimension

        self.PaymentTableview.estimatedRowHeight = 300
        self.PaymentTableview.rowHeight = UITableView.automaticDimension

    }

    override func viewDidAppear(_ animated: Bool) {

        self.globalDispatchgroup.notify(queue: .main) {

            self.accordingToListingType()

        }
    }

    // MARK:- // Define Filter Button Actions

    func defineSortButtonActions() {

        self.filterView.buttonArray[0].addTarget(self, action: #selector(self.filterAction(sender:)), for: .touchUpInside)

        self.filterView.tapToCloseButton.addTarget(self, action: #selector(self.tapToClose(sender:)), for: .touchUpInside)

    }

    // MARK;- // Sort Action

    @objc func tapToClose(sender: UIButton) {

        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)

    }

    // MARK;- // Filter Action

    @objc func filterAction(sender: UIButton) {

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController

        navigate.rootVC = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newfoodlisting") as! NewFoodListingViewController

        navigate.showDeliveryCost = true

        self.navigationController?.pushViewController(navigate, animated: true)

    }
    

    //MARK:- //Tableview Delegate an Datasource Methods

    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == FoodListingTableView
        {
            return self.categoryListArray.count
        }
        else
        {
           return 1
        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.FoodListingTableView
        {
            print("Count==",self.currentRecordsArray.count)
            if section == 0
            {
               let Number = Int("\((self.categoryListArray[0] as! NSDictionary)["total_count"]!)")
                if Number == nil
                {
                    return 0
                }
                else
                {
                    return Number!
                }
             }

           else if section == 1
            {
                let Number = Int("\((self.categoryListArray[1] as! NSDictionary)["total_count"]!)")
                if Number == nil
                {
                    return 0
                }
                else
                {
                    return Number!
                }
            }
            else if section == 2
            {
                let Number = Int("\((self.categoryListArray[2] as! NSDictionary)["total_count"]!)")
                if Number == nil
                {
                    return 0
                }
                else
                {
                    return Number!
                }
            }
            else if section == 3
            {
                let Number = Int("\((self.categoryListArray[3] as! NSDictionary)["total_count"]!)")
                if Number == nil
                {
                    return 0
                }
                else
                {
                    return Number!
                }
            }

            else
            {
                let Number = Int("\((self.categoryListArray[4] as! NSDictionary)["total_count"]!)")
                if Number == nil
                {
                    return 0
                }
                else
                {
                    return Number!
                }
            }
//            else if section == 1
//            {
//                let Number = ((self.categoryListArray[1] as! NSDictionary)["total_count"]!) as! Int
//                return Number
//            }
//            else if section == 2
//            {
//                let Number = ((self.categoryListArray[2] as! NSDictionary)["total_count"]!) as! Int
//                return Number
//            }
//            else if section == 4
//            {
//                let Number = ((self.categoryListArray[3] as! NSDictionary)["total_count"]!) as! Int
//                return Number
//            }
//            else
//            {
//                let Number = ((self.categoryListArray[4] as! NSDictionary)["total_count"]!) as! Int
//                return Number
//            }
//            return self.currentRecordsArray.count
        }
        else if tableView == ReviewsTableView
        {
          return self.reviewArray.count
        }
        else if tableView == self.DeliveryTimeTableview
        {  // Delivery Time Table
            return (self.infoDictionary["service_available"] as! NSArray).count
        }
        else if tableView == PaymentTableview
        {  // Payment Type Table
            return (self.infoDictionary["payment_type"] as! NSArray).count
        }
        else
        {
            return 1
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if tableView == FoodListingTableView
         {
            let cell = tableView.dequeueReusableCell(withIdentifier: "foodlisting") as! NewFoodListingTVC

            var rowNumber = indexPath.row
            for i in 0..<indexPath.section {
                rowNumber += self.FoodListingTableView.numberOfRows(inSection: i)
            }
            print("rowNumber",rowNumber)

//            cell.FoodName.text = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["product_name"] ?? "")"
//            cell.FoodPrice.text = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["currency_symbol"] ?? "")" +     "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["start_price"] ?? "")"
//            cell.FoodPrice.textColor = .seaGreen()
//            cell.FoodCartBtnOutlet.tag = indexPath.row
//            cell.FoodCartBtnOutlet.addTarget(self, action: #selector(AddToCart(sender:)), for: .touchUpInside)
//            cell.FoodDescription.text = "\((self.currentRecordsArray[indexPath.row] as! NSDictionary)["description"] ?? "")"
//
//            cell.FoodListingCollectionView.delegate = self
//            cell.FoodListingCollectionView.dataSource = self
//            cell.FoodListingCollectionView.reloadData()
//
//            let tag = tableViewCellCoordinator.count
//            cell.FoodListingCollectionView.tag = tag
//            tableViewCellCoordinator[tag] = indexPath
//            RowIndex = indexPath.row
//            currentIndexPath.append(indexPath)
            
            cell.FoodName.text = "\(self.GetFoodListingStructArray[indexPath.row].FoodName ?? "")"
            
            cell.FoodPrice.text = "\(self.GetFoodListingStructArray[indexPath.row].FoodPrice ?? "")"
            
            cell.FoodDescription.text =  "\(self.GetFoodListingStructArray[indexPath.row].FoodDesc ?? "")"
            
            cell.FoodPrice.textColor = .seaGreen()
            
            cell.FoodCartBtnOutlet.tag = indexPath.row
            
            cell.FoodCartBtnOutlet.addTarget(self, action: #selector(AddToCart(sender:)), for: .touchUpInside)
            
             RowIndex = indexPath.row
            
//            guard let tableViewCell = cell as? demoTVC else { return }
            
            cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            
            self.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            
            cell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
            
//            print("RowIndex==",RowIndex)

            return cell
         }
         else if tableView == ReviewsTableView{

            let cell = tableView.dequeueReusableCell(withIdentifier: "reviews") as! FoodListReviewsTVCTableViewCell

            cell.name.text = "\((reviewArray[indexPath.row] as! NSDictionary)["name"]!)"
            
            cell.reviewDate.text = "\((reviewArray[indexPath.row] as! NSDictionary)["review_date"]!)"
            
            cell.comment.text = "\((reviewArray[indexPath.row] as! NSDictionary)["comment"]!)"
            
            cell.totalRating.text = "\((reviewArray[indexPath.row] as! NSDictionary)["rvwtotal_rating"]!)"
            
            cell.reviewRating.rating = Double("\((reviewArray[indexPath.row] as! NSDictionary)["rvwavg_rating"]!)")!

            cell.totalRatingImage.sd_setImage(with: URL(string: "\((reviewArray[indexPath.row] as! NSDictionary)["rating_img"]!)"))



            //Set Font

            cell.name.font = UIFont(name: cell.name.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.name.font = UIFont(name: cell.name.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.dateLBL.font = UIFont(name: cell.dateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.reviewDate.font = UIFont(name: cell.reviewDate.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.commentLBL.font = UIFont(name: cell.commentLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.comment.font = UIFont(name: cell.comment.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.ratingLBL.font = UIFont(name: cell.ratingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.totalRatingLBL.font = UIFont(name: cell.totalRatingLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.totalRating.font = UIFont(name: cell.totalRating.font.fontName, size: CGFloat(Get_fontSize(size: 14)))


            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            cell.cellView.layer.cornerRadius = 8

            return cell

        }
        else if tableView == self.DeliveryTimeTableview
         { // Info Delivery Time Table
            let cell = tableView.dequeueReusableCell(withIdentifier: "info") as! FoodListInfoTVCTableViewCell

            cell.deliveryTimesLBL.text = "\(((self.infoDictionary["service_available"] as! NSArray)[indexPath.row] as! NSDictionary)["day"] ?? "")"
            cell.deliveryTime.text = "\(((self.infoDictionary["service_available"] as! NSArray)[indexPath.row] as! NSDictionary)["open_time"] ?? "")" + " - " + "\(((self.infoDictionary["service_available"] as! NSArray)[indexPath.row] as! NSDictionary)["close_time"] ?? "")"

            //Set Font
            cell.deliveryTimesLBL.font = UIFont(name: cell.deliveryTimesLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.deliveryTime.font = UIFont(name: cell.deliveryTime.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
         }
         else if tableView == PaymentTableview
         { // Info Payment Type Table
            if "\((self.paymentTypeArray[indexPath.row] as! NSDictionary)["type"] ?? "")".elementsEqual("COD") {
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentCOD")

                cell?.textLabel!.text = "\((self.paymentTypeArray[indexPath.row] as! NSDictionary)["type"] ?? "")"

                //Set Font
                cell?.textLabel!.font = UIFont(name: (cell?.textLabel!.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
                
                //
                cell?.selectionStyle = UITableViewCell.SelectionStyle.none

                return cell!
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "paymentBank") as! PaymentMethodBankTableViewCell

                cell.bankNameTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "bankName", comment: "")
                cell.bankName.text = "\((self.paymentTypeArray[indexPath.row] as! NSDictionary)["bank_name"] ?? "")"
                cell.accountNumberTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "accountNumber", comment: "")
                cell.accountNumber.text = "\((self.paymentTypeArray[indexPath.row] as! NSDictionary)["account_name"] ?? "")"

                //Set Font
                cell.bankNameTitleLBL.font = UIFont(name: cell.bankNameTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                cell.bankName.font = UIFont(name: cell.bankName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                cell.accountNumberTitleLBL.font = UIFont(name: cell.accountNumberTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
                cell.accountNumber.font = UIFont(name: cell.accountNumber.font.fontName, size: CGFloat(Get_fontSize(size: 14)))


                //
                cell.selectionStyle = UITableViewCell.SelectionStyle.none

                return cell
            }
        }

        else
         {
           let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UITableViewCell

           return cell
         }

    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == FoodListingTableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "foodlisting") as! NewFoodListingTVC


            let TestView = UIView(frame: CGRect(x: 0, y: 10, width: FoodListingTableView.frame.size.width, height: 180))
            let RName : UILabel! = UILabel()
            TestView.addSubview(RName)
            RName.translatesAutoresizingMaskIntoConstraints = false
            RName.leadingAnchor.constraint(equalTo: TestView.leadingAnchor, constant: 8).isActive = true
            RName.topAnchor.constraint(equalTo: TestView.topAnchor, constant: 0).isActive = true
            RName.heightAnchor.constraint(equalToConstant: 50).isActive = true

            let imgView = UIImageView(frame: CGRect(x: 0, y: 50, width: FoodListingTableView.frame.size.width, height: 130))

            if section == 0
            {
//                let imgPath = "\((self.categoryListArray[0] as! NSDictionary)["cat_image"]!)"
                imgView.image = UIImage(named: "Favourites")
                RName.text = "\((self.categoryListArray[0] as! NSDictionary)["cat_name"]!)"
            }
            else if section == 1
            {
                let imgPath = "\((self.categoryListArray[1] as! NSDictionary)["cat_image"]!)"
                imgView.sd_setImage(with: URL(string: imgPath))
                RName.text = "\((self.categoryListArray[1] as! NSDictionary)["cat_name"]!)"
            }
            else if section == 2
            {
                let imgPath = "\((self.categoryListArray[2] as! NSDictionary)["cat_image"]!)"
                imgView.sd_setImage(with: URL(string: imgPath))
                RName.text = "\((self.categoryListArray[2] as! NSDictionary)["cat_name"]!)"
            }
            else if section == 3
            {
                let imgPath = "\((self.categoryListArray[3] as! NSDictionary)["cat_image"]!)"
                imgView.sd_setImage(with: URL(string: imgPath))
                RName.text = "\((self.categoryListArray[3] as! NSDictionary)["cat_name"]!)"
            }
            else
            {
               let imgPath = "\((self.categoryListArray[4] as! NSDictionary)["cat_image"]!)"
               imgView.sd_setImage(with: URL(string: imgPath))
               RName.text = "\((self.categoryListArray[4] as! NSDictionary)["cat_name"]!)"
            }
            RName.font = UIFont(name: (cell.FoodName.font.fontName), size: CGFloat(Get_fontSize(size: 20)))!
            RName.textColor = .orange
            TestView.backgroundColor = .white
            TestView.addSubview(imgView)
            self.view.addSubview(TestView)
            return TestView
        }
     else 
        {
            return nil
        }

    }

//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if tableView == FoodListingTableView
//        {
//            let cell =  cell as! NewFoodListingTVC
//            cell.FoodListingCollectionView.backgroundColor = .white
//            cell.FoodListingCollectionView.reloadData()
//            cell.FoodListingCollectionView.contentOffset = .zero
//
//        }
//        else
//        {
//
//        }
//    }

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tableView == FoodListingTableView
//        {
//            let cell =  cell as! NewFoodListingTVC
//            cell.FoodListingCollectionView.backgroundColor = .white
//            cell.FoodListingCollectionView.reloadItems(at: currentIndexPath)
//            cell.FoodListingCollectionView.contentOffset = .zero
//        }
//        else
//        {
//
//        }
//    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == FoodListingTableView
        {
           return 200
        }
        else
        {
           return 0
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }


    //MARK:- Collectionview Delegate and Datasource Methods

     func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == BiggerImageCollectionView
        {
            return ((self.currentRecordsArray[RowIndex] as! NSDictionary)["photos"] as! NSArray).count
        }
        else
        {
           return ((self.currentRecordsArray[RowIndex] as! NSDictionary)["photos"] as! NSArray).count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == BiggerImageCollectionView
        {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "biggerimage", for: indexPath) as! BiggerImageCVC
            
            cell.backgroundColor = .white

            let temImageArray = self.GetFoodListingStructArray[RowIndex].FoodPicArray as! NSMutableArray
            
            
            let imgPath = "\((temImageArray[indexPath.item] as! NSDictionary)["img"]!)"
            
            cell.BiggerImageView.sd_setImage(with: URL(string:imgPath))
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "foodlisting", for: indexPath) as! NewFoodListingCVC
            
            cell.backgroundColor = .white
            
            print("PhotosCount",((self.currentRecordsArray[RowIndex] as! NSDictionary)["photos"] as! NSArray).count)
            
            
             let temImageArray = self.GetFoodListingStructArray[RowIndex].FoodPicArray as! NSMutableArray
            
        
            print("TempArray",temImageArray)
            
            let imgPath = "\((temImageArray[indexPath.item] as! NSDictionary)["img"]!)"
            
            cell.FoodImage.sd_setImage(with: URL(string:imgPath))
            return cell
        }

    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == BiggerImageCollectionView
        {

        }
        else
        {
//            self.BiggerImageCollectionView.delegate = self
//            self.BiggerImageCollectionView.dataSource = self
//            self.BiggerImageCollectionView.reloadData()


            self.BlurBiggerView.isHidden = false
            
         
            let toIndex : IndexPath = IndexPath(row: indexPath.item, section: 0)
            self.BiggerImageCollectionView.scrollToItem(at: toIndex, at: .left, animated: true)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == BiggerImageCollectionView
        {
            let collectionviewsize = self.BiggerImageCollectionView.frame.size.width
            return CGSize(width: collectionviewsize, height: 528)
        }
        else
        {
           return CGSize(width: 71, height: 61)
        }
    }

    // MARK: - // JSON POST Method to get Restaurant List

    func getFoodListings()
    {
        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")

        parameters = "login_id=\(userID ?? "")&lang_id=\(langID ?? "")&per_load=\(10)&start_value=\(startValue)&seller_id=\(sellerID ?? "")&mycountry_name=\(myCountryName ?? "")&listing_type=\(listingType ?? "")&search_cat=\(searchCategoryID ?? "")&delivery_type=\(self.filterDeliveryType ?? "")"

        self.CallAPI(urlString: "app_restaurent_details", param: parameters) {



            if self.listingType.elementsEqual("P") {

                self.allDataDictionary = self.globalJson["info_array"] as? NSMutableDictionary
                
                self.totalCategoryProductArray = self.allDataDictionary["cat_product"] as? NSMutableArray

//                self.nextStart = "\(self.globalJson["next_start"]!)"

            }
            else if self.listingType.elementsEqual("R") {

                if "\(self.globalJson["response"]! as! Bool)".elementsEqual("true")
                {
                  self.reviewArray = self.globalJson["info_array"] as? NSArray
                }
                else
                {
                    //do nothing
                }




     

            }
            else if self.listingType.elementsEqual("C") {

                self.infoDictionary = (self.globalJson["info_array"] as! NSArray)[0] as? NSDictionary
                self.paymentTypeArray = self.infoDictionary["payment_type"] as? NSArray

                for i in 0..<(self.infoDictionary["map_details"] as! NSArray).count {
                    self.mapArray.append((self.infoDictionary["map_details"] as! NSArray)[i] as! NSDictionary)
                }

            }

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                self.GetFoodStruct()
                SVProgressHUD.dismiss()
            }

        }

    }


    
    //MARK:- //Appending Data To GetFoodListing Struct Array From API
    
    func GetFoodStruct()
    {
        for i in 0..<self.totalCategoryProductArray.count
        {
            self.GetFoodListingStructArray.append(GetFoodListingStruct(FoodName: "\((self.totalCategoryProductArray[i] as! NSDictionary)["product_name"] ?? "")", FoodPrice: "\((self.totalCategoryProductArray[i] as! NSDictionary)["currency_symbol"] ?? "")" + "\((self.totalCategoryProductArray[i] as! NSDictionary)["reserve_price"] ?? "")", FoodDesc: "\((self.totalCategoryProductArray[i] as! NSDictionary)["description"] ?? "")", FoodPicArray: ((self.totalCategoryProductArray[i] as! NSDictionary)["photos"] as! NSMutableArray), FoodID: "\((self.totalCategoryProductArray[i] as! NSDictionary)["product_id"] ?? "")"))
        }
    }


    // MARK:- // Set Product Details Tableview Height

    func setTableviewHeight(tableview: UITableView,heightConstraint: NSLayoutConstraint)
    {

        var height : CGFloat! = 0

        tableview.frame.size.height = 3000

        for cell in tableview.visibleCells {
            height += cell.bounds.height
        }

        heightConstraint.constant = height


    }


    // MARK:- // According to Listing Type

    func accordingToListingType() {



        if self.listingType.elementsEqual("P") {

            self.FoodListingTableView.isHidden = false
            self.ReviewsView.isHidden = true
            self.InfoView.isHidden = true
            self.FoodMapView.isHidden = true

//            print("PtypeArray",self.allDataDictionary)

                self.restaurantListArray = self.allDataDictionary["cat_product"] as! NSMutableArray

                self.popularDishesArray = self.allDataDictionary["popular_dish"] as! NSMutableArray

                self.categoryListArray = self.allDataDictionary["p_category_list"] as! NSMutableArray

                self.PickerViewCategoryListArray = self.allDataDictionary["category_list"] as! NSMutableArray


            if self.popularDishesArray.count > 0
            {
                for i in 0...(self.popularDishesArray.count-1)
                {
                    let tempdict = self.popularDishesArray[i] as! NSDictionary
                    self.recordsArray.add(tempdict as! NSMutableDictionary)
                }
            }

            if self.restaurantListArray.count > 0
            {
                for i in 0...(self.restaurantListArray.count-1)

                {
                    let tempDict = self.restaurantListArray[i] as! NSDictionary

                    self.recordsArray.add(tempDict as! NSMutableDictionary)
                }

                self.currentRecordsArray = self.recordsArray.mutableCopy() as? NSMutableArray
            }

            print("CurrentRecordsArray",self.currentRecordsArray!)



            self.startValue = self.startValue + self.restaurantListArray.count

            self.setLayout()

//            self.popularDishesTableview.delegate = self
//            self.popularDishesTableview.dataSource = self
//            self.popularDishesTableview.reloadData()

            if currentRecordsArray == nil
            {

            }
            else
            {
                self.FoodListingTableView.delegate = self
                self.FoodListingTableView.dataSource = self
                self.FoodListingTableView.reloadData()
            }

            self.CategoryPickerView.delegate = self
            self.CategoryPickerView.dataSource = self
            self.CategoryPickerView.reloadAllComponents()


        }
        else if self.listingType.elementsEqual("R") {

            self.FoodListingTableView.isHidden = true
            self.ReviewsView.isHidden = false
            self.InfoView.isHidden = true
            self.FoodMapView.isHidden = true

                if reviewArray == nil
                {


                }
                else
                {
                    self.ReviewsTableView.delegate = self
                    self.ReviewsTableView.dataSource = self
                    self.ReviewsTableView.reloadData()
                }


        }
        else if self.listingType.elementsEqual("C") {

            if self.showMap == true {
                self.FoodListingTableView.isHidden = true
                self.ReviewsView.isHidden = true
                self.InfoView.isHidden = true
                self.FoodListingTableView.isHidden = false

                self.showMap = !self.showMap

                self.FoodMapView.getData(dataArray: self.mapArray, listingDataArray: self.currentRecordsArray as! [NSDictionary])

            }
            else {

                self.FoodListingTableView.isHidden = true
                self.ReviewsView.isHidden = true
                self.InfoView.isHidden = false
                self.FoodMapView.isHidden = true

                if (self.infoDictionary["service_available"] as! NSArray) == nil
                {

                }
                else
                {
                    self.DeliveryTimeTableview.delegate = self
                    self.DeliveryTimeTableview.dataSource = self
                    self.DeliveryTimeTableview.reloadData()
                }
                if (self.infoDictionary["payment_type"] as! NSArray) == nil
                {

                }
                else
                {
                    self.PaymentTableview.delegate = self
                    self.PaymentTableview.dataSource = self
                    self.PaymentTableview.reloadData()
                }
            }



        }

    }

    //MARK:- //PickerView Delegate and Datasource Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.PickerViewCategoryListArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\((self.PickerViewCategoryListArray[row] as! NSDictionary)["cat_name"] ?? "")"
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        self.CategoryLbl.text = "\((self.PickerViewCategoryListArray[row] as! NSDictionary)["cat_name"] ?? "")"

        self.searchCategoryID = "\((self.PickerViewCategoryListArray[row] as! NSDictionary)["id"] ?? "")"
        
//        print("searchCategoryID",self.searchCategoryID)
        
        self.startValue = 0
        
        //self.allDataDictionary.removeAllObjects()
        
        self.recordsArray.removeAllObjects()
        
        self.currentRecordsArray.removeAllObjects()
        
        self.restaurantListArray.removeAllObjects()
        
        self.popularDishesArray.removeAllObjects()
        
        self.categoryListArray.removeAllObjects()
        
//        self.PickerViewCategoryListArray.removeAllObjects()

        self.getFoodListings()

        self.ShowPickerView.isHidden = true

        self.globalDispatchgroup.notify(queue: .main) {

            self.accordingToListingType()

//            self.setTableviewHeight(tableview: self.popularDishesTableview, heightConstraint: self.popularDishesTableHeightConstraint)
//            self.setTableviewHeight(tableview: self.FoodListingTableView, heightConstraint: self.listingCategoryTableHeightConstraint)

        }
    }

    // MARK:- // Show Category

    func setLayout()
    {

        if self.currentRecordsArray.count == 0
        {
            self.FoodListingTableView.isHidden = true
//            self.popularDishesView.isHidden = true
        }
        else
        {
            self.FoodListingTableView.isHidden = false
//            self.popularDishesView.isHidden = false
        }

//        for i in 0..<(self.allDataDictionary["category_list"] as! NSArray).count {
//
//            if "\(((self.allDataDictionary["category_list"] as! NSArray)[i] as! NSDictionary)["id"] ?? "")".elementsEqual(self.searchCategoryID!) {
//
//                self.listingCategoryImage.sd_setImage(with: URL(string: "\(((self.allDataDictionary["category_list"] as! NSArray)[i] as! NSDictionary)["cat_image"] ?? "")"))
//
//                self.listingCategoryTitleLBl.text = "\(((self.allDataDictionary["category_list"] as! NSArray)[i] as! NSDictionary)["cat_name"] ?? "")"
//
//            }
//
//        }
    }



    // MARK:- // Create Image Scroll

    func createImageScroll(popularDishes : Bool , index : Int) {

        var imageStringArray = [String]()
//        var imageviewArray = [UIImageView]()

        if popularDishes == true {
            for i in 0..<((self.popularDishesArray[index] as! NSDictionary)["photos"] as! NSArray).count {
                imageStringArray.append("\((((self.popularDishesArray[index] as! NSDictionary)["photos"] as! NSArray)[i] as! NSDictionary)["img"] ?? "")")
            }
        }
        else {
            for i in 0..<((self.currentRecordsArray[index] as! NSDictionary)["photos"] as! NSArray).count {
                imageStringArray.append("\((((self.currentRecordsArray[index] as! NSDictionary)["photos"] as! NSArray)[i] as! NSDictionary)["img"] ?? "")")
            }
        }
    }

    //MARK:- Set Data
    func SetData()
    {
        self.SellerImage.sd_setImage(with: URL(string: "\(self.sellerImageURL ?? "")"))
        self.SellerName.text = self.seller
    }

    @objc func AddToCart(sender : UIButton)
    {
//        let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "foodcourtaddcart") as! FoodCourtAddCartViewController
//        nav.productId = "\((self.restaurantListArray[sender.tag] as! NSDictionary)["product_id"]!)"
//        nav.FeatureArray = ((self.restaurantListArray[sender.tag] as! NSDictionary)["extra_feature_val"]!) as? NSMutableArray
//        self.navigationController?.pushViewController(nav, animated: true)
        
        self.MainFoodDetails = []
        self.featureFoodDetails = []
        
        self.MainFoodDetails.append(mainFoodDetails(foodName: "\((self.restaurantListArray[sender.tag] as! NSDictionary)["product_name"] ?? "")", foodDescription: "\((self.restaurantListArray[sender.tag] as! NSDictionary)["description"] ?? "")", foodPrice: ("\((self.restaurantListArray[sender.tag] as! NSDictionary)["currency_symbol"] ?? "")" + "\((self.restaurantListArray[sender.tag] as! NSDictionary)["reserve_price"] ?? "")" ), foodOnlyPrice: "\((self.restaurantListArray[sender.tag] as! NSDictionary)["reserve_price"] ?? "")", foodPriceCurrency: "\((self.restaurantListArray[sender.tag] as! NSDictionary)["currency_symbol"] ?? "")"))
        
        let ExtraValArray = ((self.restaurantListArray[sender.tag] as! NSDictionary)["extra_feature_val"]!) as! NSMutableArray
        
        for i in 0..<ExtraValArray.count
        {
            self.featureFoodDetails.append(foodDetails(featureName: "\((ExtraValArray[i] as! NSDictionary)["feature_name"] ?? "")", featureId: "\((ExtraValArray[i] as! NSDictionary)["feature_id"] ?? "")", featurePrice: ("\((ExtraValArray[i] as! NSDictionary)["currency_symbol"] ?? "")" + "\((ExtraValArray[i] as! NSDictionary)["feature_price"] ?? "")"), featureQuantity: 0, featureOnlyPrice: "\((ExtraValArray[i] as! NSDictionary)["feature_price"] ?? "")"))
        }
        
        
        let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "foodcart") as! NewFoodCartViewController
        
        navigate.ProductID = "\(self.GetFoodListingStructArray[sender.tag].FoodID ?? "")"
        navigate.foodDetailsStruct = self.featureFoodDetails
        navigate.mainFoodStruct = self.MainFoodDetails
        self.navigationController?.pushViewController(navigate, animated: true)
        
        
    }
    
    
    //MARK:- //Bigger ImageView CollectionView Custom Property
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        BiggerImageCollectionView.delegate = dataSourceDelegate
        BiggerImageCollectionView.dataSource = dataSourceDelegate
        BiggerImageCollectionView.tag = row
        BiggerImageCollectionView.setContentOffset(BiggerImageCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        BiggerImageCollectionView.reloadData()
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class NewFoodListingTVC : UITableViewCell
{
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var FoodName: UILabel!
    @IBOutlet weak var FoodPrice: UILabel!
    @IBOutlet weak var FoodCartBtnOutlet: UIButton!
    @IBOutlet weak var FoodDescription: UILabel!
    @IBOutlet weak var PreviousBtnOutlet: UIButton!
    @IBOutlet weak var NextBtnOutlet: UIButton!
    

    @IBOutlet weak var FoodListingCollectionView: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

class NewFoodListingCVC : UICollectionViewCell
{
    
    @IBOutlet weak var FoodImage: UIImageView!

}

class BiggerImageCVC : UICollectionViewCell
{
    
    @IBOutlet weak var BiggerImageView: UIImageView!
    
}


class FoodListingTVCTableViewCell: UITableViewCell {


    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var bottomVew: UIView!
    @IBOutlet weak var priceStackview: UIStackView!
    @IBOutlet weak var startPrice: UILabel!
    @IBOutlet weak var reservePrice: UILabel!

    @IBOutlet weak var bottomViewFirstSeparator: UIView!
    @IBOutlet weak var viewImagesButton: UIButton!
    @IBOutlet weak var addToCartButton: UIButton!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

struct foodDetails {
    var featureName : String?
    var featureId : String?
    var featurePrice : String?
    var featureQuantity : Int!
    var featureOnlyPrice : String!
    
    init(featureName : String , featureId : String , featurePrice : String , featureQuantity : Int , featureOnlyPrice : String )
    {
       self.featureName = featureName
        self.featureId = featureId
        self.featurePrice = featurePrice
        self.featureQuantity = featureQuantity
        self.featureOnlyPrice = featureOnlyPrice
    }
}

struct mainFoodDetails {
    var foodName : String?
    var foodDescription : String?
    var foodPrice : String?
    var foodOnlyPrice : String!
    var foodPriceCurrency : String?
    
    init(foodName : String , foodDescription : String , foodPrice : String , foodOnlyPrice : String , foodPriceCurrency : String)
    {
        self.foodName = foodName
        self.foodDescription = foodDescription
        self.foodPrice = foodPrice
        self.foodOnlyPrice = foodOnlyPrice
        self.foodPriceCurrency = foodPriceCurrency
    }
}


//MARK:- // Structure For TableView and CollectionView data Handling
struct GetFoodListingStruct
{
    var FoodName : String?
    var FoodPrice : String?
    var FoodDesc : String?
    var FoodPicArray : NSMutableArray!
    var FoodID : String?
    init(FoodName : String , FoodPrice : String , FoodDesc : String , FoodPicArray : NSMutableArray , FoodID : String)
    {
        self.FoodName = FoodName
        self.FoodPrice = FoodPrice
        self.FoodPrice = FoodPrice
        self.FoodPicArray = FoodPicArray
        self.FoodID = FoodID
    }
}

extension NewFoodListingTVC
{
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        FoodListingCollectionView.delegate = dataSourceDelegate
        FoodListingCollectionView.dataSource = dataSourceDelegate
        FoodListingCollectionView.tag = row
        FoodListingCollectionView.setContentOffset(FoodListingCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        FoodListingCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { FoodListingCollectionView.contentOffset.x = newValue }
        get { return FoodListingCollectionView.contentOffset.x }
    }
}


