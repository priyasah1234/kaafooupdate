//
//  FoodCourtMainViewController.swift
//  Kaafoo
//
//  Created by admin on 30/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class FoodCourtMainViewController: GlobalViewController {


    @IBOutlet weak var searchAndSortMainVIew: UIView!
    @IBOutlet weak var searchAndSortView: UIView!
    @IBOutlet weak var searchTXT: UITextField!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var foodCourtMainTableview: UITableView!
    @IBOutlet weak var sortBackgroundView: UIView!
    @IBOutlet weak var sortTransparentView: UIView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var upImage: UIImageView!
    @IBOutlet weak var sortContainerView: UIView!
    @IBOutlet weak var sortLBL: UILabel!
    @IBOutlet weak var searchLBL: UILabel!
    @IBOutlet weak var sortCategoryTableview: UITableView!


    @IBOutlet weak var foodCourtMainCollectionview: UICollectionView!


    // MARK:- // Change Listing Type Button

    @IBOutlet weak var changeListingtypeButtonOutlet: UIButton!
    @IBAction func changeListingtypeButton(_ sender: UIButton) {

        if self.foodCourtMainTableview.isHidden == true {
            self.view.bringSubviewToFront(self.foodCourtMainTableview)
            self.view.sendSubviewToBack(self.foodCourtMainCollectionview)
            self.foodCourtMainTableview.isHidden = false
            self.foodCourtMainCollectionview.isHidden = true
        }
        else {
            self.view.bringSubviewToFront(self.foodCourtMainCollectionview)
            self.view.sendSubviewToBack(self.foodCourtMainTableview)
            self.foodCourtMainTableview.isHidden = true
            self.foodCourtMainCollectionview.isHidden = false
        }

    }

    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var recordsArray : NSMutableArray! = NSMutableArray()

    var currentRecordsArray : NSMutableArray = NSMutableArray()

    let dispatchGroup = DispatchGroup()

    var foodCourtArray : NSArray!

    var tapGesture = UITapGestureRecognizer()

    var localSortCategoryArray = [["key" : "" , "value" : "All"] , ["key" : "T" , "value" : "Name"] , ["key" : "R" , "value" : "Review"]]

    var sortType : String! = ""

    var ratingValue : String! = ""

    var searchCategoryID : String!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.setFont()

        self.sortCategoryTableview.delegate = self
        self.sortCategoryTableview.dataSource = self

        self.sortContainerView.layer.cornerRadius = (5/320)*self.FullWidth

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)


    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.getFoodCourtList()

        self.sortBackgroundView.isHidden = true

    }


    // MARK:- // Buttons

    // MARK:- // Tap Search Button

    @IBOutlet weak var searchButtonOutlet: UIButton!
    @IBAction func tapSearchButton(_ sender: UIButton) {

    }


    // MARK:- // Tap Sort Button


    @IBOutlet weak var sortButtonOutlet: UIButton!
    @IBAction func tapSortButton(_ sender: UIButton) {

        if self.sortBackgroundView.isHidden == true
        {
            if sortView.isHidden == true
            {
                self.sortView.isHidden = false
                self.sortCategoryTableview.isHidden = true
            }
            UIView.animate(withDuration: 0.5)
            {
                self.view.bringSubviewToFront(self.sortBackgroundView)
                self.sortBackgroundView.isHidden = false

                // *** Hide sortBackgroundView when tapping outside ***
                self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
                self.sortTransparentView.addGestureRecognizer(self.tapGesture)

                //self.categoryDropDownButtonOutlet.isUserInteractionEnabled = false
            }
        }
        else
        {
            if sortView.isHidden == true
            {
                self.sortView.isHidden = false
                self.sortCategoryTableview.isHidden = true
            }
            else
            {
                UIView.animate(withDuration: 0.5)
                {
                    self.view.sendSubviewToBack(self.sortBackgroundView)
                    self.sortBackgroundView.isHidden = true

                    if self.sortCategoryTableview.isHidden == false
                    {
                        self.sortCategoryTableview.isHidden = true
                    }
                }
            }
        }

    }

    // MARK:- // Open Star Rating Search

    @IBOutlet weak var searchOutlet: UIButton!
    @IBAction func tapOnSearch(_ sender: UIButton) {

//        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "foodCourtMainFilterProductsVC") as! FoodCourtMainFilterProductsViewController

        navigate.searchCategoryID = self.searchCategoryID

        self.navigationController?.pushViewController(navigate, animated: true)

    }


    // MARK:- // Open Sort or Search Option

    @IBOutlet weak var sortOutlet: UIButton!
    @IBAction func tapOnSort(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5)
        {
            self.sortCategoryTableview.isHidden = false
            self.view.bringSubviewToFront(self.sortCategoryTableview)
            self.sortView.isHidden = true
        }

    }


    // MARK:- // Delegate Functions

    // MARK:- // Tableview Delegates




    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }


            }
            else
            {
                getFoodCourtList()
            }



        }
    }




    // MARK: - // JSON POST Method to get Food Court List

    func getFoodCourtList()

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }

        let url = URL(string: GLOBALAPI + "app_restaurent_listing")!   //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")

        if (ratingValue?.isEmpty)!
        {

            parameters = "lang_id=\(langID!)&mycountry_name=\(myCountryName!)&search_sort=\(sortType!)&search_cat=\(searchCategoryID!)"

        }
        else
        {

            parameters = "lang_id=\(langID!)&mycountry_name=\(myCountryName!)search_cat=\(searchCategoryID!)&search_sort=\(sortType!)&ratingvalue=\(ratingValue!)"

        }

        print("Food Court List URL is : ",url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }


        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Food Court List Response: " , json)

                    if "\(json["response"]!)".elementsEqual("0")
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()

                        }

                    }
                    else
                    {
                        self.foodCourtArray = (json["info_array"] as! NSDictionary)["product"] as? NSArray



                        for i in 0...(self.foodCourtArray.count-1)

                        {

                            let tempDict = self.foodCourtArray[i] as! NSDictionary

                            self.recordsArray.add(tempDict as! NSMutableDictionary)


                        }

                        self.currentRecordsArray = self.recordsArray.mutableCopy() as! NSMutableArray

                        self.nextStart = "\(json["next_start"]!)"

                        self.startValue = self.startValue + self.foodCourtArray.count

                        print("Next Start Value : " , self.startValue)

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.foodCourtMainTableview.delegate = self
                            self.foodCourtMainTableview.dataSource = self
                            self.foodCourtMainTableview.reloadData()
                            self.foodCourtMainTableview.rowHeight = UITableView.automaticDimension
                            self.foodCourtMainTableview.estimatedRowHeight = UITableView.automaticDimension

                            self.foodCourtMainCollectionview.delegate = self
                            self.foodCourtMainCollectionview.dataSource = self
                            self.foodCourtMainCollectionview.reloadData()

                            SVProgressHUD.dismiss()
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()



    }


    // MARK: - // JSON POST Method to get Food Court List searched By Rating

    func getFoodCourtListByRating()

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }

        let url = URL(string: GLOBALAPI + "app_restaurent_listing")!   //change the url

        var parameters : String = ""

        let langID = UserDefaults.standard.string(forKey: "langID")
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")

        parameters = "lang_id=\(langID!)&mycountry_name=\(myCountryName!)&search_sort=\(sortType!)&ratingvalue=\(ratingValue!)"

        print("Food Court List URL is : ",url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }
//        catch let error {
//            print(error.localizedDescription)
//        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Food Court List Response: " , json)

                    if "\(json["response"]!)".elementsEqual("0")
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()

                        }

                    }
                    else
                    {
                        self.foodCourtArray = (json["info_array"] as! NSDictionary)["product"] as? NSArray



                        for i in 0...(self.foodCourtArray.count-1)

                        {

                            let tempDict = self.foodCourtArray[i] as! NSDictionary

                            self.recordsArray.add(tempDict as! NSMutableDictionary)


                        }

                        self.currentRecordsArray = self.recordsArray.mutableCopy() as! NSMutableArray

                        self.nextStart = "\(json["next_start"]!)"

                        self.startValue = self.startValue + self.foodCourtArray.count

                        print("Next Start Value : " , self.startValue)

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.foodCourtMainTableview.delegate = self
                            self.foodCourtMainTableview.dataSource = self
                            self.foodCourtMainTableview.reloadData()
                            self.foodCourtMainTableview.rowHeight = UITableView.automaticDimension
                            self.foodCourtMainTableview.estimatedRowHeight = UITableView.automaticDimension

                            SVProgressHUD.dismiss()
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()



    }



    // MARK: - Hide sortBackgroundView when tapping outside
    @objc func tapGestureHandler() {
        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.sortBackgroundView)
            self.sortBackgroundView.isHidden = true

            if self.sortCategoryTableview.isHidden == false
            {
                self.sortCategoryTableview.isHidden = true
                self.view.sendSubviewToBack(self.sortCategoryTableview)
            }

            //self.categoryDropDownButtonOutlet.isUserInteractionEnabled = true
        }
    }


    // SET FOnt

    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        footerView.homeLabel.font = UIFont(name: footerView.homeLabel.font.fontName, size: CGFloat(Get_fontSize(size: 9)))
        footerView.messageLabel.font = UIFont(name: footerView.messageLabel.font.fontName, size: CGFloat(Get_fontSize(size: 9)))
        footerView.notificationLBL.font = UIFont(name: footerView.notificationLBL.font.fontName, size: CGFloat(Get_fontSize(size: 9)))
        footerView.addLBL.font = UIFont(name: footerView.addLBL.font.fontName, size: CGFloat(Get_fontSize(size: 9)))
        footerView.dailyDealsLabel.font = UIFont(name: footerView.dailyDealsLabel.font.fontName, size: CGFloat(Get_fontSize(size: 9)))

    }

}




// MARK:- // Tableview Delegates

extension FoodCourtMainViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == foodCourtMainTableview
        {
            //return self.recordsArray.count
            return self.currentRecordsArray.count
        }
        else
        {
            return localSortCategoryArray.count
        }


    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == foodCourtMainTableview
        {

            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! FoodCourtMainTVCTableViewCell

            cell.businessImage.sd_setImage(with: URL(string: "\((currentRecordsArray[indexPath.row] as! NSDictionary)["logo"]!)"))

            cell.businessName.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["businessname"]!)"

            if "\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"]!)".isEmpty
            {
                cell.ratingView.rating = 0
            }
            else
            {
                cell.ratingView.rating = Double("\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"]!)")!
            }



            //cell.ratingView.settings.filledImage = UIImage(named: "Shape")

            cell.addressLBL.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["address"]!)"

            cell.deliveryTypeLBL.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["delivery_type"]!)"
            cell.priceLBL.isHidden = true
            cell.estimatedDeliveryTime.isHidden = true

            //            cell.estimatedDeliveryTime.text = "est. " + "\((currentRecordsArray[indexPath.row] as! NSDictionary)["est_time"]!)"


            //---// Attributed Price
            //            let priceString = "Min  " + "\((currentRecordsArray[indexPath.row] as! NSDictionary)["est_price"]!)"
            //            let charCount = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["est_price"]!)".count
            //            let range = NSRange(location:5,length:charCount) // specific location. This means "range" handle 1 character at location 2
            //            let attributedString = NSMutableAttributedString(string: priceString, attributes: [NSAttributedStringKey.font:UIFont(name: cell.priceLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))!])
            //            // here you change the character to red color
            //            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: range)
            //            cell.priceLBL.attributedText = attributedString


            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell

        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! FoodCourtMainSortTVCTableViewCell

            cell.cellLBL.text = "\((localSortCategoryArray[indexPath.row] as NSDictionary)["value"]!)"

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }

    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if tableView == foodCourtMainTableview
        {

            let cell = cell as! FoodCourtMainTVCTableViewCell

            //Dynamic Address Height
            //            let addressString = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["address"]!)"
            //            let addressHeight = addressString.heightWithConstrainedWidth(width: (170/320)*self.FullWidth, font: UIFont(name: cell.addressLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))!)
            //            //dynamic address frame
            //            cell.addressLBL.frame = CGRect(x: (130/320)*self.FullWidth, y: (70/568)*self.FullHeight, width: (170/320)*self.FullWidth, height: addressHeight)


            cell.cellView.clipsToBounds = false

            cell.cellView.layer.cornerRadius = (5/320)*self.FullWidth
            cell.cellBottomView.layer.cornerRadius = (5/320)*self.FullWidth

            //            cell.cellView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)


        }

    }

    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        if tableView == foodCourtMainTableview
    //        {
    //            return (180/568)*self.FullHeight
    //        }
    //        else
    //        {
    //            return (40/568)*self.FullHeight
    //        }
    //
    //
    //    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == foodCourtMainTableview
        {

            //let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "restaurantDetailsVC") as! RestaurantDetailsViewController
            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "foodListingVC") as! FoodListingViewController

            navigate.sellerID = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["id"]!)"
            navigate.seller = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["businessname"]!)"
            navigate.sellerImageURL = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["logo"]!)"

            navigate.searchCategoryID = searchCategoryID


            self.navigationController?.pushViewController(navigate, animated: true)

        }
        else
        {
            self.sortCategoryTableview.isHidden = true
            self.sortBackgroundView.isHidden = true

            self.sortType = "\((self.localSortCategoryArray[indexPath.row] as NSDictionary)["key"]!)"

            self.startValue = 0

            self.recordsArray.removeAllObjects()
            self.currentRecordsArray.removeAllObjects()

            self.getFoodCourtList()

            self.sortType = ""
        }

    }


}




// MARK:- // Collectionview Delegates

extension FoodCourtMainViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {



    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return self.currentRecordsArray.count

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCollectionview", for: indexPath) as! FoodCourtMainCVCCollectionViewCell

        cell.businessImage.sd_setImage(with: URL(string: "\((currentRecordsArray[indexPath.row] as! NSDictionary)["logo"]!)"))

        cell.businessName.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["businessname"]!)"

        if "\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"]!)".isEmpty
        {
            cell.ratingView.rating = 0
        }
        else
        {
            cell.ratingView.rating = Double("\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"]!)")!
        }



        //cell.ratingView.settings.filledImage = UIImage(named: "Shape")

        cell.addressLBL.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["address"]!)"

        cell.deliveryType.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["delivery_type"]!)"

        cell.cellView.layer.cornerRadius = 8

        return cell

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        //let heightVal = self.photosCollectionview.frame.size.width / 3

        return CGSize(width: self.FullWidth / 2, height: 230)

    }



    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "foodListingVC") as! FoodListingViewController

        navigate.sellerID = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["id"]!)"
        navigate.seller = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["businessname"]!)"
        navigate.sellerImageURL = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["logo"]!)"

        navigate.searchCategoryID = searchCategoryID


        self.navigationController?.pushViewController(navigate, animated: true)



    }


}

