//
//  RestaurantReviewTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 05/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos

class RestaurantReviewTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var nameView: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var reviewDate: UILabel!
    @IBOutlet weak var commentLBL: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var ratingLBL: UILabel!
    @IBOutlet weak var totalRatingLBL: UILabel!
    @IBOutlet weak var reviewRating: CosmosView!
    @IBOutlet weak var totalReviewCount: UILabel!
    @IBOutlet weak var totalReviewsImage: UIImageView!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
