//
//  PaymentMethodBankTableViewCell.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 14/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class PaymentMethodBankTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var bankNameTitleLBL: UILabel!
    @IBOutlet weak var accountNumberTitleLBL: UILabel!
    @IBOutlet weak var bankName: UILabel!
    @IBOutlet weak var accountNumber: UILabel!
    
    
    
    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
