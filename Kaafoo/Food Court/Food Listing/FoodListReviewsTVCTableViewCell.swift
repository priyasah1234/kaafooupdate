//
//  FoodListReviewsTVCTableViewCell.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 16/04/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos

class FoodListReviewsTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var nameLBL: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var reviewDate: UILabel!
    @IBOutlet weak var commentLBL: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var ratingLBL: UILabel!
    @IBOutlet weak var totalRatingLBL: UILabel!
    @IBOutlet weak var reviewRating: CosmosView!
    @IBOutlet weak var totalRating: UILabel!
    @IBOutlet weak var totalRatingImage: UIImageView!
    
    
    
    
    
    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
