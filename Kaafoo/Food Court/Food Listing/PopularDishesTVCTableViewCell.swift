//
//  PopularDishesTVCTableViewCell.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 13/04/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class PopularDishesTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var priceStackview: UIStackView!
    
    @IBOutlet weak var startPrice: UILabel!
    @IBOutlet weak var reservePrice: UILabel!
    @IBOutlet weak var bottomViewFIrstSeparator: UIView!
    
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var viewImagesButton: UIButton!
    
    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
