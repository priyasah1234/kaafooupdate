//
//  FoodListInfoTVCTableViewCell.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 13/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class FoodListInfoTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var deliveryTimesLBL: UILabel!
    @IBOutlet weak var clockImageview: UIImageView!
    @IBOutlet weak var deliveryTime: UILabel!
    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
