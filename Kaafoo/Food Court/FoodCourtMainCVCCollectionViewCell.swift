//
//  FoodCourtMainCVCCollectionViewCell.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 10/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos

class FoodCourtMainCVCCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var businessImage: UIImageView!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var addressPlaceholderImage: UIImageView!
    @IBOutlet weak var addressLBL: UILabel!
    @IBOutlet weak var deliveryType: UILabel!
    
    
    
    
    
    
}
