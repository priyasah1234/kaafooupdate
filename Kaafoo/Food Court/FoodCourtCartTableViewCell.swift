//
//  FoodCourtCartTableViewCell.swift
//  Kaafoo
//
//  Created by KAUSTABH K B on 30/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class FoodCourtCartTableViewCell: UITableViewCell {
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var Quantity_Lbl: UILabel!
    @IBOutlet weak var QBtn: UIButton!
    @IBOutlet weak var feature_name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
