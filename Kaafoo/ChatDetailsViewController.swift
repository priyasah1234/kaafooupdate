//
//  ChatDetailsViewController.swift
//  Kaafoo
//
//  Created by esolz on 23/10/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import PusherSwift
import MediaPlayer
import AVFoundation
import Alamofire


class ChatDetailsViewController: GlobalViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVAudioPlayerDelegate,AVAudioRecorderDelegate {
    
    var imagePicker = UIImagePickerController()
    var ShowTextInput : Bool! = true
    var audioMessagePlayer : AVPlayer!
    var CellAudioPlayer: AVPlayer?
    let demoImageView : UIImageView! = UIImageView()
    
    //MARK:- //Audio Recording Session
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer:AVAudioPlayer!
    
    var ChatLatitude : String! = ""
    var ChatLongitude : String! = ""
    var MessageImagePath : String! = ""
    
    var MessagesArray = [String]()
    
    //MARK:- //AUdio Recording in Swift
    
    
     let dispatchGroup = DispatchGroup()
    let UserID = UserDefaults.standard.string(forKey: "userID")
    let LangID = UserDefaults.standard.string(forKey: "langID")
    var imageData = Data()
    
    var imageDataArray = [Data]()
   
    
    @IBAction func AudioPlayBtn(_ sender: UIButton) {
        if (sender.titleLabel?.text == "Play"){
                   audioMessageBtnOutlet.isEnabled = false
                   sender.setTitle("Stop", for: .normal)
                   preparePlayer()
                   audioPlayer.play()
               } else {
                   audioPlayer.stop()
                   sender.setTitle("Play", for: .normal)
               }
    }
    @IBOutlet weak var AudioPlayBtnOutlet: UIButton!
    @IBAction func textMessageBtn(_ sender: UIButton) {
        //Send Text Message
        if self.MessagetextView.text.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Text Message Can not be empty")
        }
        else
        {
             self.SendTextMessageToChatServer()
        }
    }
    
    //MARK:- //Send Text Message To Chat
    
    func SendTextMessageToChatServer()
    {
        let parameters = "lang_id=\(LangID!)&user_id=\(UserID!)&receiver_id=\(SenderId!)&msg=\(self.MessagetextView.text ?? "")&chatlatitude=\(self.ChatLatitude!)&chatlongitude=\(self.ChatLongitude!)&message_img=\(self.MessageImagePath!)"
        self.CallAPI(urlString: "app_user_send_message", param: parameters, completion: {
            self.globalDispatchgroup.leave()
            self.globalDispatchgroup.notify(queue: .main, execute: {
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.ShowAlertMessage(title: "Alert", message: "\(self.globalJson["message"] ?? "")")
                    self.MessagetextView.text = ""
                }
            })
        })
    }
    
   
    @IBOutlet weak var audioRecordingView: AudioRecorder!
    
    @IBAction func imageMessageBtn(_ sender: UIButton) {
        let alert : UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertAction.Style.default)
            {
                UIAlertAction in
                self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            {
                UIAlertAction in
        }

        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        }
    
    //MARK:- //ImagePicker Delegate Methods
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
           self.dismiss(animated: true, completion: { () -> Void in

           })
           print("Image",image)
           //imageView.image = image
       }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        let imagefinal = tempImage.jpegData(compressionQuality: 1)
        
        imageData = imagefinal as! Data
        
        self.imageDataArray.append(imageData)
        
        self.demoImageView.image = UIImage(named: "\(tempImage)")
        self.UploadImageToServer()
        print("ImageDetails",tempImage)
        self.dismiss(animated: true, completion: nil)

    }
    
//    func BlockUser()
//    {
//        let parameters =
//        self.CallAPI(urlString: <#T##String#>, param: <#T##String#>, completion: <#T##() -> ()#>)
//    }
    
    func openCamera(){
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            alert.title = "Warning"
            alert.message = "You don't have camera"
            alert.addButton(withTitle: "OK")
            alert.show()
        }
    }
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    
    @IBOutlet weak var recorderHideBtnOutlet: UIButton!
    
    @IBAction func recorderHideBtn(_ sender: UIButton) {
        self.recorderView.isHidden = true
    }
    
    @IBOutlet weak var recorderView: UIView!
    @IBAction func audioMessageBtn(_ sender: UIButton) {
        self.audioRecorder = nil
        self.recorderView.isHidden = false
        self.view.bringSubviewToFront(self.recorderView)
    }
    
    @IBOutlet weak var textMessageBtnOutlet: UIButton!
    @IBOutlet weak var imageMessageBtnOutlet: UIButton!
    @IBOutlet weak var audioMessageBtnOutlet: UIButton!
    @IBOutlet weak var MessagetextView: UITextView!
    @IBOutlet weak var InputView: UIView!
    @IBOutlet weak var DetailsView: UIView!
    @IBOutlet weak var MessageTableView: UITableView!
    @IBOutlet weak var ReceiverName: UILabel!
    @IBOutlet weak var BlockLabel: UILabel!
    @IBOutlet weak var BlockBtnOutlet: UIButton!
    @IBAction func BlockBtn(_ sender: UIButton) {
    }
    
   // let UserID = UserDefaults.standard.string(forKey: "userID")
    
    var pusher: Pusher!
    var receiverName : String! = ""
    var StartFrom : Int! = 0
    var PerLoad : Int! = 100
    var recordsArray : NSMutableArray! = NSMutableArray()
    var SenderId : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.BlockBtnOutlet.addTarget(self, action: #selector(BlockUserAPI(sender:)), for: .touchUpInside)
        self.SetGradient()
        self.SetAudioRecordsButtons()
        self.audioRecordingView.isHidden = false
        self.setupView()
        self.SetLayer()
        self.ReceiverName.text = "\(receiverName ?? "")"
        self.getMessageListing()
        //push notifications
              let options = PusherClientOptions(
                     host: .cluster("ap2")
                   )

                   pusher = Pusher(
                     key: "e53a9c4a375968423d66",
                     options: options
                   )

                   pusher.delegate = self

                   // subscribe to channel
              
              //MARK:- //Friends who are Online Event CallBack
              
                   let channel = pusher.subscribe("myonline-channel")
              let channelOne = pusher.subscribe("my-channel")

                   // bind a callback to handle an event
              let _ = channel.bind(eventName: "myonline_event", callback: { (data: Any?) -> Void in
                         if let data = data as? [String : AnyObject] {
                             
                             print("Data==",data)
                          
                         }
                     })
              
              //MARK:- //One Person to One Person Chat Section
              
              let _ = channelOne.bind(eventName: "my_event", callback: { (data: Any?) -> Void in
                                 if let data = data as? [String : AnyObject] {
                                     
                                     print("DataOne==",data)
                                    
                                  if let message = data["message"] as? String {
                                      print("ChatMessage",message)
                                    self.MessagesArray.append(message)
                                      }
                                    print("MessageArray",self.MessagesArray)
                                 }
                             })

                   pusher.connect()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- //Add Gradient Color to BackGround View
    
    func SetGradient()
    {
        let gradient: CAGradientLayer = CAGradientLayer()

        gradient.colors = [UIColor.blue.cgColor, UIColor.red.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.0)
        gradient.frame = self.view.frame
        
        let animation = CABasicAnimation(keyPath: "colors")
        animation.fromValue = [UIColor.red.cgColor, UIColor.green.cgColor]
        animation.toValue = [UIColor.green.cgColor, UIColor.red.cgColor]
        animation.duration = 5.0
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        
        gradient.add(animation, forKey: nil)
        
        //self.recorderView.layer.addSublayer(gradient)
       self.recorderView.layer.insertSublayer(gradient, at: 0)
    }
    
    //MARK:- //View Did Appear Check
    
    override func viewDidAppear(_ animated: Bool) {
        self.checkBlockingStatus()
    }
    
    //MARK:- //ViewWillAppear Check
    
    override func viewWillAppear(_ animated: Bool) {
        self.recorderView.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.recorderView.isHidden = true
    }
    
    //MARK:- //Checking Input View For Block Users
    
    func checkBlockingStatus()
    {
        if self.ShowTextInput == true
        {
            //do nothing
            self.InputView.isHidden = false
            self.BlockLabel.text = "Block"
        }
        else
        {
            self.InputView.isHidden = true
            self.BlockLabel.text = "Blocked"
        }
    }
    
    func SetLayer()
    {
        self.MessagetextView.layer.borderWidth = 2.0
        self.MessagetextView.layer.borderColor = UIColor.lightGray.cgColor
        self.MessagetextView.clipsToBounds = true
        self.MessagetextView.layer.cornerRadius = 5.0
        self.InputView.layer.borderColor = UIColor.green.cgColor
        self.InputView.layer.borderWidth = 1.0
        self.audioRecordingView.layer.borderWidth = 1.0
        self.audioRecordingView.layer.borderColor = UIColor.black.cgColor
    }
    
    //MARK:- //Set Audio Recorder Buttons Target
    
    func SetAudioRecordsButtons()
    {
//        self.audioRecordingView.AudioPlayerView.addSubview(self.audioPlayer)
        self.audioRecordingView.AudioPlayButton.addTarget(self, action: #selector(SetAudioPlay(sender:)), for: .touchUpInside)
        self.audioRecordingView.AudioStopButton.addTarget(self, action: #selector(SetAudioStop(sender:)), for: .touchUpInside)
        self.audioRecordingView.AudioSaveButton.addTarget(self, action: #selector(SetAudioSave(sender:)), for: .touchUpInside)
        self.audioRecordingView.AudioSendButton.addTarget(self, action: #selector(SendAudioFileToServer(sender:)), for: .touchUpInside)
    }
    
    
    @objc func SetAudioPlay(sender : UIButton)
    {
            if audioRecorder == nil {
                    startRecording()
                    print("Start Recording")
                } else {
//                    finishRecording(success: true)
//                    self.audioMessageBtnOutlet.setTitle("Record", for: .normal)
//                    print("Recording finish")
               // do nothing
                }
        print("Set Audio Play")
    }
    
    @objc func SetAudioStop(sender : UIButton)
    {
        print("Set Audio Stop")
        self.finishRecording(success: true)
        //self.audioRecorder.stop()
    }
    
    @objc func SetAudioSave(sender : UIButton)
    {
        self.preparePlayer()
        self.audioPlayer.play()
        print("Set Audio Save")
    }
    
    @objc func SendAudioFileToServer(sender : UIButton)
    {
        print("Send Audio")
        self.uploadAudioToServer()
    }
    
    
    //MARK:- //Get Individual Message Listing
    
    func getMessageListing()
    {
        let UserID = UserDefaults.standard.string(forKey: "userID")
        let LangID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "lang_id=\(LangID!)&user_id=\(UserID!)&sender_id=\(SenderId!)&start_from=\(StartFrom!)&per_page=\(PerLoad!)"
        self.CallAPI(urlString: "app_user_message_listing", param: parameters, completion: {
            self.globalDispatchgroup.leave()
            let infoArray = self.globalJson["info_array"] as! NSArray
            for i in 0..<infoArray.count
            {
                let tempDict = infoArray[i] as! NSDictionary
                self.recordsArray.add(tempDict)
            }
            self.globalDispatchgroup.notify(queue: .main, execute: {
                DispatchQueue.main.async {
                    self.MessageTableView.delegate = self
                    self.MessageTableView.dataSource = self
                    self.MessageTableView.reloadData()
                    SVProgressHUD.dismiss()
                }
            })
        })
    }

    
    func UploadImageToServer() {
            
            dispatchGroup.enter()
            
            DispatchQueue.main.async {
                SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
            }
            
            let langID = UserDefaults.standard.string(forKey: "langID")
            
            let parameters = [
                "lang_id" : "\(LangID!)" , "user_id" : "\(self.UserID!)" ,"receiver_id" : "\(SenderId!)" ,"msg" : "\(self.MessagetextView.text ?? "")" , "chatlatitude" : "\(self.ChatLatitude!)" ,"chatlongitude" : "\(self.ChatLongitude!)"]
            
            Alamofire.upload(
                multipartFormData: { MultipartFormData in
                    
                    for (key, value) in parameters {
                        MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                    
                    for i in 0..<self.imageDataArray.count
                    {
                        MultipartFormData.append(self.imageDataArray[i], withName: "message_img[]", fileName: "messageimg\(i).jpeg", mimeType: "image/jpeg")
                    }
                    
                    
                    
            }, to: (GLOBALAPI + "app_user_send_message"))
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
    //
                        print(response.request!)  // original URL request
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        print(response.result)   // result of response serialization

                        if let JSON = response.result.value {
                            
                            print("Image Search Response-----: \(JSON)")
                            
                            DispatchQueue.main.async {
                                
                                SVProgressHUD.dismiss()
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                
                                self.dispatchGroup.leave()
                                
                                SVProgressHUD.dismiss()
                            }
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                    print(encodingError)
                    
                    DispatchQueue.main.async {
                        
                        self.dispatchGroup.leave()
                        
                        SVProgressHUD.dismiss()
                    }
                }
                
            }
            
        }
    
    @objc func SetCellAudioPlay(sender : UIButton)
    {
        CellAudioPlayer?.play()
        print("Play Audio")
    }
    
    @objc func SetSenderAudioPause(sender : UIButton)
    {
        CellAudioPlayer?.pause()
        print("Pause Audio")
    }
    @objc func SetSenderAudioResume(sender : UIButton)
    {
        CellAudioPlayer?.play()
        print("Resume Audio")
    }
    @objc func SetSenderAudioStop(sender : UIButton)
    {
        CellAudioPlayer?.pause()
        print("Stop Audio")
    }
    
    @objc func SetSenderAudioDownload(sender : UIButton)
    {
        print("Download Audio")
        
       // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
         let destinationFileUrl = documentsUrl.appendingPathComponent("downloadedFile.mp3")
         
         //Create URL to the source file you want to download
         let fileURL = URL(string: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3")
         
         let sessionConfig = URLSessionConfiguration.default
         let session = URLSession(configuration: sessionConfig)
      
         let request = URLRequest(url:fileURL!)
         
         let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
             if let tempLocalUrl = tempLocalUrl, error == nil {
                 // Success
                 if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                     print("Successfully downloaded. Status code: \(statusCode)")
                 }
                 
                 do {
                     try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                 } catch (let writeError) {
                     print("Error creating a file \(destinationFileUrl) : \(writeError)")
                 }
                 
             } else {
                 print("Error took place while downloading a file. Error description: %@", error?.localizedDescription);
             }
         }
         task.resume()
         
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class MessageReceiverCell : UITableViewCell
{
    @IBOutlet weak var receiverImage: UIImageView!
    @IBOutlet weak var receiverTextMessage: UILabel!
    @IBOutlet weak var ReceiverAudioPlayerView: UIView!
    @IBOutlet weak var SeparatorView: UIView!
    @IBOutlet weak var receivedImageView: UIImageView!
    
}

class MessageSenderCell : UITableViewCell
{
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var senderImage: UIImageView!
    @IBOutlet weak var senderTextMessage: UILabel!
    @IBOutlet weak var SenderAudioPlayerView: UIView!
    @IBOutlet weak var SeparatorView: UIView!
    @IBOutlet weak var SentImageView: UIImageView!
    @IBOutlet weak var SenderAudioPlayBtnOutlet: UIButton!
    @IBOutlet weak var SenderAudioResumeBtnOutlet: UIButton!
    @IBOutlet weak var SenderAudioDownloadBtnOutlet: UIButton!
    @IBOutlet weak var SenderAudioPauseBtnOutlet: UIButton!
    @IBOutlet weak var SenderAudioStopBtnOutlet: UIButton!
    
    
}

extension ChatDetailsViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["sender_id"]!)".elementsEqual(UserID!)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sender", for: indexPath) as! MessageSenderCell
            let imageURL = "\((self.recordsArray[indexPath.row] as! NSDictionary)["sender_img"] ?? "")"
            print("ImageURL",imageURL)
            cell.senderImage.sd_setImage(with: URL(string: imageURL))
            cell.senderTextMessage.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["message_info"] ?? "")"
            let imageArray = ((self.recordsArray[indexPath.row] as! NSDictionary)["message_image"] as! NSMutableArray)
            
            if imageArray.count == 0
            {
               cell.SentImageView.isHidden = true
            }
            else
            {
                cell.SentImageView.isHidden = false
                let sendImageURL = "\((((self.recordsArray[indexPath.row] as! NSDictionary)["message_image"] as! NSArray)[0] as! NSDictionary)["image_path"] ?? "")"
                print("SendImageURL",sendImageURL)
                
                cell.SentImageView.sd_setImage(with: URL(string: sendImageURL))
            }
            
            cell.SenderAudioPlayerView.clipsToBounds = true
            cell.SenderAudioPlayerView.layer.cornerRadius = 10.0
            cell.SenderAudioPlayerView.layer.masksToBounds = true
            
            let url = "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3"
            let playerItem = AVPlayerItem( url:NSURL( string:url )! as URL )
            CellAudioPlayer = AVPlayer(playerItem:playerItem)
//            CellAudioPlayer?.pause()
            //CellAudioPlayer!.rate = 1.0
            //cell.SenderAudioPlayerView.addSubview(CellAudioPlayer)
            cell.SenderAudioPlayBtnOutlet.addTarget(self, action: #selector(SetCellAudioPlay(sender:)), for: .touchUpInside)
            cell.SenderAudioPauseBtnOutlet.addTarget(self, action: #selector(SetSenderAudioPause(sender:)), for: .touchUpInside)
            cell.SenderAudioStopBtnOutlet.addTarget(self, action: #selector(SetSenderAudioStop(sender:)), for: .touchUpInside)
            cell.SenderAudioResumeBtnOutlet.addTarget(self, action: #selector(SetSenderAudioResume(sender:)), for: .touchUpInside)
            cell.SenderAudioDownloadBtnOutlet.addTarget(self, action: #selector(SetSenderAudioDownload(sender:)), for: .touchUpInside)
            cell.SenderAudioPlayerView.isHidden = false
            return cell
        }
        else if "\((self.recordsArray[indexPath.row] as! NSDictionary)["sender_id"]!)" != UserID!
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receiver", for: indexPath) as! MessageReceiverCell
            let imageURLOne = "\((self.recordsArray[indexPath.row] as! NSDictionary)["sender_img"] ?? "")"
            print("ImageOneURL",imageURLOne)
            cell.receiverImage.sd_setImage(with: URL(string: imageURLOne))
            cell.receiverTextMessage.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["message_info"] ?? "")"
             let imageArray = ((self.recordsArray[indexPath.row] as! NSDictionary)["message_image"] as! NSMutableArray)
            if imageArray.count == 0
            {
                cell.receivedImageView.isHidden = true
            }
            else
            {
                cell.receivedImageView.isHidden = false
                let ReceiveImageURL = "\((((self.recordsArray[indexPath.row] as! NSDictionary)["message_image"] as! NSArray)[0] as! NSDictionary)["image_path"] ?? "")"
                print("ReceiveImageURL",ReceiveImageURL)
                cell.receivedImageView.sd_setImage(with: URL(string: ReceiveImageURL))
                
            }
            cell.ReceiverAudioPlayerView.isHidden = true
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receiver", for: indexPath) as! MessageReceiverCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
}

extension ChatDetailsViewController : MPMediaPickerControllerDelegate
{
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
                //self.dismiss(animated: true, completion: nil)
                print("you picked: \(mediaItemCollection)")//This is the picked media item.
        
        let stringPath = Bundle.main.path(forResource: "input", ofType: "mp3")
        let urlPath = Bundle.main.url(forResource: "input", withExtension: "mp3")
        print("URLPath",urlPath!)
        print("stringPath",stringPath!)
    //  If you allow picking multiple media, then mediaItemCollection.items will return array of picked media items(MPMediaItem)
                }

    // MPMediaPickerController Delegate methods
        func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
                self.dismiss(animated: true, completion: nil)
            }
    
    func setupView() {
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.loadRecordingUI()
                    } else {
                        // failed to record
                    }
                }
            }
        } catch {
            // failed to record
        }
    }
    
    func loadRecordingUI() {
        audioMessageBtnOutlet.isEnabled = true
        AudioPlayBtnOutlet.isEnabled = false
        audioMessageBtnOutlet.setTitle("Record", for: .normal)
        audioMessageBtnOutlet.addTarget(self, action: #selector(recordAudioButtonTapped), for: .touchUpInside)
//        view.addSubview(recordButton)
    }
    
    @objc func recordAudioButtonTapped(_ sender: UIButton) {
        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
        }
    }
    
    
    @objc func BlockUserAPI(sender : UIButton)
    {
        let parameters = "lang_id=\(LangID!)&senderid=\(UserID!)&receiverid=\(self.SenderId!)"
        self.CallAPI(urlString: "app_chat_block_user", param: parameters, completion: {
            self.globalDispatchgroup.leave()
            self.globalDispatchgroup.notify(queue: .main, execute: {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.ShowAlertMessage(title: "Alert", message: "\(self.globalJson["message"] ?? "")")
                }
            })
        })
    }
    
    func startRecording() {
        let audioFilename = getFileURL()
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            
            //audioMessageBtnOutlet.setTitle("Stop", for: .normal)
            AudioPlayBtnOutlet.isEnabled = false
        } catch {
            finishRecording(success: false)
        }
    }
    
    func finishRecording(success: Bool) {
        
        //audioRecorder = nil
        
        if success {
            //audioMessageBtnOutlet.setTitle("Re-record", for: .normal)
            print("Success")
            audioRecorder.stop()
            
        } else {
            //audioMessageBtnOutlet.setTitle("Record", for: .normal)
            // recording failed :(
            print("Recording Failed")
        }
        
        
        
        AudioPlayBtnOutlet.isEnabled = true
        audioMessageBtnOutlet.isEnabled = true
    }
    
  
    func preparePlayer() {
        var error: NSError?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: getFileURL() as URL)
        } catch let error1 as NSError {
            error = error1
            audioPlayer = nil
        }
        
        if let err = error {
            print("AVAudioPlayer error: \(err.localizedDescription)")
        } else {
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 10.0
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getFileURL() -> URL {
        let path = getDocumentsDirectory().appendingPathComponent("recording.mp3")
        return path as URL
    }
    
    //MARK: Delegates
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        audioMessageBtnOutlet.isEnabled = true
        AudioPlayBtnOutlet.setTitle("Play", for: .normal)
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
    
    //MARK: To upload video on server
    
    func uploadAudioToServer() {
        
        let parameters = [
        "lang_id" : "\(LangID!)" , "user_id" : "\(self.UserID!)" ,"receiver_id" : "\(SenderId!)" ,"msg" : "\(self.MessagetextView.text ?? "")" , "chatlatitude" : "\(self.ChatLatitude!)" ,"chatlongitude" : "\(self.ChatLongitude!)"]
        
        Alamofire.upload(
         multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                                  multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                              }
            multipartFormData.append(self.getFileURL(), withName: "blob")
         },
         to: "https://staging.esolzbackoffice.com/kaafoo/web/appcontrol/app_user_send_message",
         encodingCompletion: { encodingResult in
         switch encodingResult {
         case .success(let upload, _, _):
         upload.responseJSON { response in
         print(response)
         }
         case .failure(let encodingError):
         print(encodingError)
         }
         })
    }
}

