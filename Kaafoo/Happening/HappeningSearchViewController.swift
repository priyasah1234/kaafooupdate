//
//  HappeningSearchViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 21/02/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class HappeningSearchViewController: GlobalViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate
{
    @IBOutlet weak var BlurPickerView: UIView!
    
    @IBOutlet weak var DataPicker: UIView!
    
    var SelectedCountryPickerRow : Int! = 0
    
    var SelectedCityPickerRow : Int! = 0
    
    var StateID : String! = ""
    
    var city : String! = ""
    
    var myCountryName : String! = ""
    
    var startDate : String! = ""
    
    var endDate : String! = ""
    
    var SearchByWord : String! = ""
    
    var StateListArray : NSMutableArray! = NSMutableArray()
    
    @IBOutlet weak var SearchOutlet: UIButton!
    
    var MyCountry : String! = ""
    
    @IBOutlet weak var DatePicker1: UIDatePicker!
    
    var CityArray = NSArray()
    
    var demoArray : NSMutableArray!
    
    var recordsArray = NSMutableArray()
    
    @IBOutlet weak var Button1View: UIView!
    
    var ShowPicker : Bool! = false
    
    @IBOutlet weak var SelectStateStatic: UILabel!
    
    @IBOutlet weak var SelectCityStatic: UILabel!
    
    @IBOutlet weak var StartDateStatic: UILabel!
    
    @IBOutlet weak var EndDateStatic: UILabel!
    
    @IBAction func ok1_Btn(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.3)
        {
            let formatter = DateFormatter()
            
            formatter.dateFormat = "dd-MM-yyyy"
            
            self.EndDateLbl.text = formatter.string(from: self.DatePicker1.date)
            
            HappeningSearchParameters.shared.setEndDate(EndDate: formatter.string(from: self.DatePicker1.date))
            
            self.endDate = formatter.string(from: self.DatePicker1.date)
            
            self.DatePicker1.isHidden = true
            
            self.Button1View.isHidden = true
        }
    }
    @IBAction func cancel1_Btn(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        self.DatePicker1.isHidden = true
        
        self.Button1View.isHidden = true
    }
    
    var SelectedPickerView : UIPickerView!
    
    var SelectedIndex : Int! = 0
    
    var doneClick : Bool! = false
    
    var allcountrylistArray : NSArray!
    
    var CountryArray = [String]()
    
    var CountryId = [String]()
    
    @IBOutlet weak var ButtonView: UIView!
    
    let dispatchGroup = DispatchGroup()
    
    let DataArray : NSArray! = nil
    
    @IBAction func Ok_Btn(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.3)
        {
            let formatter = DateFormatter()
            
            formatter.dateFormat = "dd-MM-yyyy"
            
            self.startDateLbl.text = formatter.string(from: self.datePicker.date)
            
            self.startDate = formatter.string(from: self.datePicker.date)
            
            HappeningSearchParameters.shared.setStartDate(startDate: formatter.string(from: self.datePicker.date))
            
            self.datePicker.isHidden = true
            
            self.ButtonView.isHidden = true
        }

    }
    
    @IBAction func Cancel_Btn(_ sender: UIButton) {
        
         self.view.endEditing(true)
        
        self.datePicker.isHidden = true
        
        self.ButtonView.isHidden = true
        
    }
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var Cross_Btn: UIButton!
    
    @IBOutlet weak var SelectCountryOutlet: UIButton!
    
    @IBOutlet weak var SelectCityOutlet: UIButton!
    
    @IBAction func Select_Country_Btn(_ sender: UIButton) {
        
        self.doneClick = false
        
        self.ShowPicker = false
        
        self.SelectedPickerView = self.DataPickerView
        
        self.DataPicker.isHidden = false
        
        self.SelectedPickerView.selectRow(0, inComponent: 0, animated: false)
        
//        self.DataPickerView.isHidden = false
        self.DataPickerView.delegate = self
        
        self.DataPickerView.dataSource = self
        
    }
    @IBAction func Select_City_Btn(_ sender: UIButton) {
        
        self.doneClick = false
        
        self.ShowPicker = true
        
        self.SelectedPickerView = self.DataPickerView
        
        self.SelectedPickerView.selectRow(0, inComponent: 0, animated: false)
        
        self.DataPickerView.reloadAllComponents()
        
        self.DataPicker.isHidden = false
    }
    @IBAction func Start_Date_Btn(_ sender: UIButton) {
        
        self.datePicker.isHidden = false
        
        self.ButtonView.isHidden = false
    }
    @IBAction func End_date_Btn(_ sender: UIButton) {
        
        self.DatePicker1.isHidden = false
        
        self.Button1View.isHidden = false
    }
    
    @IBAction func Cross_Btn(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var Start_date_Outlet: UIButton!
    
    @IBOutlet weak var End_date_Outlet: UIButton!
    
    @IBOutlet weak var startDateLbl: UILabel!
    
    @IBOutlet weak var EndDateLbl: UILabel!
    
    @IBAction func Search_Btn(_ sender: UIButton) {
        
        if  self.CountryLbl.text == LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_country", comment: "")
        {
            let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_select_the_country", comment: ""), preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
        }
        else if self.CityLbl.text == LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_city", comment: "")
        {
            let alert = UIAlertController(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_select_the_city", comment: ""), preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil)
        }
//        else if self.startDateLbl.text == "Select"
//        {
//            let alert = UIAlertController(title: "Warning", message: "Please Enter Start Date", preferredStyle: .alert)
//            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//            alert.addAction(ok)
//            self.present(alert, animated: true, completion: nil)
//        }
//        else if self.EndDateLbl.text == "Select"
//        {
//            let alert = UIAlertController(title: "Warning", message: "Please Enter End Date", preferredStyle: .alert)
//            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//            alert.addAction(ok)
//            self.present(alert, animated: true, completion: nil)
//        }
//        else
//        {
//            let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "happeninglisting") as! HappeningListingViewController
//            nav.SelectedCountry = myCountryName
//            nav.SelectedCity = city
//            nav.StartDate = startDate
//            nav.EndDate = endDate
//            SearchByWord = SearchTextField.text
//            nav.SearchWord = SearchByWord
//            self.navigationController?.pushViewController(nav, animated: true)
//        }
        else
        {
            let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "HappeningListingVC") as! NewHappeningListingViewController
            
            nav.SelectedState = myCountryName
            
            nav.SelectedCity = city
            
            nav.StartDate = startDate
            
            nav.EndDate = endDate
            
            SearchByWord = SearchTextField.text
            
            nav.SearchWord = SearchByWord
            
            self.navigationController?.pushViewController(nav, animated: true)
        }
    }
    
    @IBOutlet weak var DataPickerView: UIPickerView!
    
    @IBOutlet weak var CityLbl: UILabel!
    
    @IBOutlet weak var CountryLbl: UILabel!
    
    @IBOutlet weak var SearchTextField: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.createToolbar()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)

       // self.getCountryData()
        self.GetCityData()
        
        self.tapGesture()
        
        self.SetButton()
        
        self.LanguageString()
        
        // Do any additional setup after loading the view.
    }
    
    func LanguageString()
    {
        self.CountryLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_state", comment: "")
        
        self.CityLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_city", comment: "")
        
        self.SearchTextField.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "search_by_word", comment: "")
        
        self.SelectStateStatic.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_state", comment: "")
        
        self.SelectCityStatic.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_city", comment: "")
        
        self.StartDateStatic.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "startDate", comment: "")
        
        self.EndDateStatic.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "end_date", comment: "")
        
     self.SearchOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "search", comment: ""), for: .normal)
    }
    
    //MARK:- //Create ToolBar for PickerView
    
    func createToolbar()
    {
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        
        toolBar.isTranslucent = true
        
        toolBar.barTintColor = .black
        
//        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.tintColor = .white
        
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "done", comment: ""), style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePickerView))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: "") , style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelPickerView))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true
        
        self.DataPicker.addSubview(toolBar)
        
        toolBar.anchor(top: nil, leading: self.DataPicker.leadingAnchor, bottom: self.DataPickerView.topAnchor, trailing: self.DataPicker.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
    }
    
    //MARK:- //PickerView Done Button
    
    @objc func donePickerView()
    {
        if self.ShowPicker == false
        {
            self.CountryLbl.text = self.StateListArray[SelectedCountryPickerRow] as? String
            
            self.DataPicker.isHidden = true
            
            self.recordsArray.removeAllObjects()
            
            self.StateID = "\((self.allcountrylistArray[SelectedCountryPickerRow] as! NSDictionary)["id"]!)"
            
            self.myCountryName = self.StateListArray[SelectedCountryPickerRow] as? String
            
            print("statename",self.myCountryName!)
            
            self.StateListArray.removeAllObjects()
            
            self.CityLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_city", comment: "")
            
            self.GetCityData()
            
            HappeningSearchParameters.shared.setSelectedState(state: "\(self.myCountryName!)")
        }
        else
        {
            self.CityLbl.text = "\((self.recordsArray[SelectedCityPickerRow] as! NSDictionary)["city_name"]!)"
            
            self.city = "\((self.recordsArray[SelectedCityPickerRow] as! NSDictionary)["city_name"]!)"
            
            HappeningSearchParameters.shared.setSelcectedCity(city: "\((self.recordsArray[SelectedCityPickerRow] as! NSDictionary)["city_name"] ?? "")")
            
            self.DataPicker.isHidden = true
            
            self.StateListArray.removeAllObjects()
            
            self.recordsArray.removeAllObjects()
            
            self.GetCityData()
        }
        
        print("Done")
        
    }
    
    //MARK:- //PickerView Cancel Button
    
    @objc func cancelPickerView()
    {
        self.DataPicker.isHidden = true
        
        print("Cancel")
        
    }
    
    //MARK:- //Dismiss Keyboard
    
    @objc func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    func donedatePicker(){
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd-MM-yyyy"
        
        self.view.endEditing(true)
    }
    func cancelDatePicker()
    {
        self.view.endEditing(true)
    }
   // MARK:- Pickerview Delegate and Datasource Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
      if ShowPicker == false
      {
        return self.StateListArray.count
      }
      else if ShowPicker == true
      {
        return self.recordsArray.count
      }
        else
      {
        return 1
        }

    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if ShowPicker == false
        {
            return self.StateListArray[row] as? String
        }
        else if ShowPicker == true
        {
            return ((self.recordsArray[row] as! NSDictionary)["city_name"]!) as? String
        }
        else
        {
            return nil
        }

    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if ShowPicker == false
        {
            self.SelectedCountryPickerRow = row
        }
        else if ShowPicker == true
        {
            self.SelectedCityPickerRow = row
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

    func GetCityData()
        
    {
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        let url = URL(string: GLOBALAPI + "app_country_to_city")!       //change the url
        
        print("all country list URL : ", url)
        
        var parameters : String = ""
        
//        let langID = UserDefaults.standard.string(forKey: "langID")
        MyCountry = "99"
        
        parameters = "lang_id=EN&mycountry_id=\(MyCountry!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("all Country List Response: " , json)
                    
                    self.allcountrylistArray = json["info_array"] as? NSArray


                        for i in 0..<self.allcountrylistArray.count
                        {
                            self.StateListArray.add("\((self.allcountrylistArray[i] as! NSDictionary)["region_name"]!)")
                        }

                    if self.StateID.elementsEqual("")
                    {
                    print("Do Nothing")
                    }
                    else
                    {
                        for j in 0..<self.allcountrylistArray.count
                        {
                            if "\((self.allcountrylistArray[j] as! NSDictionary)["id"]!)".elementsEqual(self.StateID!)
                            {
                                for k in 0..<((self.allcountrylistArray[j] as! NSDictionary)["city_list"]! as! NSArray).count
                                {
                                    let tempdict = (((self.allcountrylistArray[j] as! NSDictionary)["city_list"]! as! NSArray)[k] as! NSDictionary)
                                    self.recordsArray.add(tempdict)
                                    print("RecordsArray",self.recordsArray)
                                }
                            }
                            else
                            {
                                print("Do Nothing")
                            }
                        }
                    }


                    print("RecordsArray",self.recordsArray)



                    DispatchQueue.main.async {
                        
                        self.dispatchGroup.leave()
                        
                        SVProgressHUD.dismiss()
                        
                        self.DataPickerView.delegate = self
                        
                        self.DataPickerView.dataSource = self
                        
                        self.DataPickerView.reloadAllComponents()
                    }
                    
                }
                
            } catch let error {
                
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    func SetButton()
    {
        self.SearchOutlet.clipsToBounds = true
        
        self.SearchOutlet.layer.cornerRadius = 5.0
    }
    //MARK:- //UITapgesture
    func tapGesture()
    {
        let tap = UITapGestureRecognizer(target: self, action: #selector(touchTapped(sender:)))
        
        self.BlurPickerView.addGestureRecognizer(tap)
    }
    @objc func touchTapped(sender : UITapGestureRecognizer)
    {
        self.DataPicker.isHidden = true
    }
    
    //MARK:- //TextField Delegate Methods
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        if textField == self.SearchTextField
        {
            HappeningSearchParameters.shared.SearchWord = "\(self.SearchTextField.text ?? "")"
        }
    }
}
