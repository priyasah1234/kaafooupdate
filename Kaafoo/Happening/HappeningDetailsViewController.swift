//
//  HappeningDetailsViewController.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 27/04/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

import SVProgressHUD


class HappeningDetailsViewController: GlobalViewController {

    @IBOutlet weak var fontLabel: UILabel!

    let dispatchGroup = DispatchGroup()

    var eventID : String!

    var allDataDictionary : NSDictionary!

    var availableTimeArray : NSArray!

    var testint : Int! = 0

    var bookingtimeTableHeightConstraint : NSLayoutConstraint!

    var monthArray = ["January","February","March","April","May","June","July","August","September","October","November","December"]

    var dateIndexpath : Int!

    var checkImageArray = [UIImageView]()

    var mainScroll : UIScrollView = {
        let mainscroll = UIScrollView()
        mainscroll.showsHorizontalScrollIndicator = false
        mainscroll.showsVerticalScrollIndicator = false
        mainscroll.translatesAutoresizingMaskIntoConstraints = false
        return mainscroll
    }()

    var mainScrollContentview : UIView = {
        let mainscrollcontentview = UIView()
        mainscrollcontentview.translatesAutoresizingMaskIntoConstraints = false
        return mainscrollcontentview
    }()


    var sendBookingRequest : UIButton = {
        let sendbookingrequest = UIButton()
        sendbookingrequest.backgroundColor = .orange
        sendbookingrequest.translatesAutoresizingMaskIntoConstraints = false
        return sendbookingrequest
    }()


    var eventImageview : UIImageView = {
        let eventimageview = UIImageView()
        eventimageview.contentMode = .scaleAspectFill
        eventimageview.clipsToBounds = true
        eventimageview.translatesAutoresizingMaskIntoConstraints = false
        return eventimageview
    }()


    var eventName : UILabel = {
        let eventname = UILabel()
        eventname.textAlignment = .natural
        eventname.numberOfLines = 0
        eventname.translatesAutoresizingMaskIntoConstraints = false
        return eventname
    }()

    var addressView : UIView = {
        let addressview = UIView()
        addressview.translatesAutoresizingMaskIntoConstraints = false
        return addressview
    }()

    var addressPlaceholderImageview : UIImageView = {
        let addressplaceholderimageview = UIImageView()
        addressplaceholderimageview.contentMode = .center
        addressplaceholderimageview.clipsToBounds = true
        addressplaceholderimageview.translatesAutoresizingMaskIntoConstraints = false
        return addressplaceholderimageview
    }()

    var address : UILabel = {
        let address = UILabel()
        address.textAlignment = .natural
        address.numberOfLines = 0
        address.translatesAutoresizingMaskIntoConstraints = false
        return address
    }()


    var dateTimeView : UIView = {
        let datetimeview = UIView()
        datetimeview.translatesAutoresizingMaskIntoConstraints = false
        return datetimeview
    }()

    var startDateLBL : UILabel = {
        let startdatelbl = UILabel()
        startdatelbl.textAlignment = .natural
        startdatelbl.numberOfLines = 0
        startdatelbl.translatesAutoresizingMaskIntoConstraints = false
        return startdatelbl
    }()

    var startDate : UILabel = {
        let startdate = UILabel()
        startdate.textAlignment = .natural
        startdate.numberOfLines = 0
        startdate.translatesAutoresizingMaskIntoConstraints = false
        return startdate
    }()


    var eventTimeLBL : UILabel = {
        let eventtimelbl = UILabel()
        eventtimelbl.textAlignment = .natural
        eventtimelbl.numberOfLines = 0
        eventtimelbl.translatesAutoresizingMaskIntoConstraints = false
        return eventtimelbl
    }()

    var eventTime : UILabel = {
        let eventtime = UILabel()
        eventtime.textAlignment = .natural
        eventtime.numberOfLines = 0
        eventtime.translatesAutoresizingMaskIntoConstraints = false
        return eventtime
    }()


    var eventSpecificationView : UIView = {
        let eventSpecificationview = UIView()
        eventSpecificationview.translatesAutoresizingMaskIntoConstraints = false
        return eventSpecificationview
    }()

    var eventSpecificationLBL : UILabel = {
        let eventspecificationlbl = UILabel()
        eventspecificationlbl.textAlignment = .natural
        eventspecificationlbl.numberOfLines = 0
        eventspecificationlbl.translatesAutoresizingMaskIntoConstraints = false
        return eventspecificationlbl
    }()

    var eventSpecificationTable : UITableView = {
        let eventspecificationtable = UITableView()
        eventspecificationtable.isScrollEnabled = false
        eventspecificationtable.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        eventspecificationtable.translatesAutoresizingMaskIntoConstraints = false
        return eventspecificationtable
    }()


    var detailsView : UIView = {
        let detailsview = UIView()
        detailsview.translatesAutoresizingMaskIntoConstraints = false
        return detailsview
    }()

    var detailsLBL : UILabel = {
        let detailslbl = UILabel()
        detailslbl.numberOfLines = 0
        detailslbl.textAlignment = .natural
        detailslbl.translatesAutoresizingMaskIntoConstraints = false
        return detailslbl
    }()

    var eventDetails : UILabel = {
        let eventdetails = UILabel()
        eventdetails.numberOfLines = 0
        eventdetails.textAlignment = .natural
        eventdetails.translatesAutoresizingMaskIntoConstraints = false
        return eventdetails
    }()


    var postedByView : UIView = {
        let postedbyview = UIView()
        postedbyview.translatesAutoresizingMaskIntoConstraints = false
        return postedbyview
    }()

    var postedByLBL : UILabel = {
        let postedbylbl = UILabel()
        postedbylbl.numberOfLines = 0
        postedbylbl.textAlignment = .natural
        postedbylbl.textColor = .orange
        postedbylbl.translatesAutoresizingMaskIntoConstraints = false
        return postedbylbl
    }()

    var postedByBottomView : UIView = {
        let postedbybottomview = UIView()
        postedbybottomview.backgroundColor = .orange
        postedbybottomview.translatesAutoresizingMaskIntoConstraints = false
        return postedbybottomview
    }()

    var sellerName : UILabel = {
        let sellername = UILabel()
        sellername.numberOfLines = 0
        sellername.textAlignment = .natural
        sellername.translatesAutoresizingMaskIntoConstraints = false
        return sellername
    }()

    var feedbackPercentage : UILabel = {
        let feedbackpercentage = UILabel()
        feedbackpercentage.numberOfLines = 0
        feedbackpercentage.textAlignment = .natural
        feedbackpercentage.translatesAutoresizingMaskIntoConstraints = false
        return feedbackpercentage
    }()

    var memberSince : UILabel = {
        let membersince = UILabel()
        membersince.numberOfLines = 0
        membersince.textAlignment = .natural
        membersince.translatesAutoresizingMaskIntoConstraints = false
        return membersince
    }()

    var feedback : UILabel = {
        let feedback = UILabel()
        feedback.numberOfLines = 0
        feedback.textAlignment = .natural
        feedback.translatesAutoresizingMaskIntoConstraints = false
        return feedback
    }()

    var sendMessage : UIButton = {
        let sendmessage = UIButton()
        sendmessage.backgroundColor = .blue
        sendmessage.layer.cornerRadius = 10
        sendmessage.translatesAutoresizingMaskIntoConstraints = false
        return sendmessage
    }()

    var viewAccount : UIButton = {
        let viewaccount = UIButton()
        viewaccount.backgroundColor = .green
        viewaccount.layer.cornerRadius = 10
        viewaccount.translatesAutoresizingMaskIntoConstraints = false
        return viewaccount
    }()


    var locationView : UIView = {
        let locationview = UIView()
        locationview.translatesAutoresizingMaskIntoConstraints = false
        return locationview
    }()

    var locationLBL : UILabel = {
        let locationlbl = UILabel()
        locationlbl.textColor = .gray
        locationlbl.translatesAutoresizingMaskIntoConstraints = false
        return locationlbl
    }()

    var sellerRating : UILabel = {
        let sellerrating = UILabel()
        sellerrating.textColor = .gray
        sellerrating.translatesAutoresizingMaskIntoConstraints = false
        return sellerrating
    }()

    var ratingImageview : UIImageView = {
        let ratingimageview = UIImageView()
        ratingimageview.translatesAutoresizingMaskIntoConstraints = false
        return ratingimageview
    }()

    var sellerLocation : UILabel = {
        let sellerlocation = UILabel()
        sellerlocation.textColor = .gray
        sellerlocation.translatesAutoresizingMaskIntoConstraints = false
        return sellerlocation
    }()


    var bookingStackview : UIStackView = {

        let bookingstackview = UIStackView()
        bookingstackview.axis = .vertical
        bookingstackview.alignment = .fill // .leading .firstBaseline .center .trailing .lastBaseline
        bookingstackview.distribution = .fill // .fillEqually .fillProportionally .equalSpacing .equalCentering
        bookingstackview.spacing = 10

        //mainstackview.backgroundColor = .magenta
        bookingstackview.translatesAutoresizingMaskIntoConstraints = false
        return bookingstackview

    }()

    var bookingTimeView : UIView = {
        let bookingtimeview = UIView()
        bookingtimeview.translatesAutoresizingMaskIntoConstraints = false
        return bookingtimeview
    }()

    var bookingTimeCollectionview : UICollectionView = {

        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        let width = (UIScreen.main.bounds.size.width - 30)
        layout.itemSize = CGSize(width: floor(width), height: 120)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal

        let listingcollectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)

        listingcollectionview.backgroundColor = .white
        listingcollectionview.showsVerticalScrollIndicator = false
        listingcollectionview.showsHorizontalScrollIndicator = false

        listingcollectionview.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "bookingTime")

        listingcollectionview.isPagingEnabled = true
        listingcollectionview.isScrollEnabled = false


        listingcollectionview.translatesAutoresizingMaskIntoConstraints = false

        return listingcollectionview
    }()

    var bookingTimeTableview : UITableView = {
        let bookingtimetableview = UITableView()
        bookingtimetableview.isScrollEnabled = false
        bookingtimetableview.register(UITableViewCell.self, forCellReuseIdentifier: "bookingTime")

        bookingtimetableview.backgroundColor = .green

        bookingtimetableview.translatesAutoresizingMaskIntoConstraints = false
        return bookingtimetableview
    }()

    var nextButton : UIButton = {
        let nextbutton = UIButton()
        nextbutton.setImage(UIImage(named: "right"), for: .normal)
        nextbutton.addTarget(self, action: #selector(tapOnNext(sender:)), for: .touchUpInside)
        nextbutton.translatesAutoresizingMaskIntoConstraints = false
        return nextbutton
    }()

    var previousButton : UIButton = {
        let previousbutton = UIButton()
        previousbutton.setImage(UIImage(named: "left arrow"), for: .normal)
        previousbutton.addTarget(self, action: #selector(tapOnPrevious(sender:)), for: .touchUpInside)
        previousbutton.translatesAutoresizingMaskIntoConstraints = false
        return previousbutton
    }()
    
    
    var ShowReport : UIView = {
        let showReport = UIView()
        showReport.translatesAutoresizingMaskIntoConstraints = false
        showReport.backgroundColor = UIColor(red: 65.0, green: 65.0, blue: 65.0, alpha: 1.0)
        showReport.layer.opacity = 0.85
        return showReport
    }()
    
    var ReportBox : Report = {
        let reportBox = Report()
        reportBox.translatesAutoresizingMaskIntoConstraints = false
        reportBox.backgroundColor = .white
        return reportBox
    }()
    
    var hideReportButton : UIButton = {
        let HideReportBtn = UIButton()
        HideReportBtn.translatesAutoresizingMaskIntoConstraints = false
        HideReportBtn.backgroundColor = .clear
        return HideReportBtn
    }()



    override func viewDidLoad() {
        super.viewDidLoad()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        self.getEventDetails()

    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)



    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.dispatchGroup.notify(queue: .main) {

            self.setLayout()

            self.putData()

            self.eventSpecificationTable.separatorStyle = UITableViewCell.SeparatorStyle.none
            
            self.eventSpecificationTable.separatorColor = .white
            
            self.bookingTimeTableview.separatorStyle = UITableViewCell.SeparatorStyle.none
            
            self.bookingTimeTableview.separatorColor = .white

            self.eventSpecificationTable.estimatedRowHeight = 100
            
            self.eventSpecificationTable.rowHeight = UITableView.automaticDimension

            self.setTableHeights()

            self.hideNextOrPrevious()
            
            self.addActions()
        }
        
        self.ShowReport.isHidden = true



    }
    
    //MARK:- //Button Actions Method
    
    func addActions()
    {
        self.viewAccount.addTarget(self, action: #selector(self.ViewAccountTarget(sender:)), for: .touchUpInside)
        
        self.sendMessage.addTarget(self, action: #selector(self.SendMessageTarget(sender:)), for: .touchUpInside)
        
        self.hideReportButton.addTarget(self, action: #selector(self.hideReportViewTarget(sender:)), for: .touchUpInside)
    }



    // MARK:- // Set Layout

    func setLayout() {

        //Send Booking Request Button

        self.view.addSubview(self.sendBookingRequest)

        self.sendBookingRequest.anchor(top: nil, leading: self.view.leadingAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, trailing: self.view.trailingAnchor, size: .init(width: 0, height: 50))


        // Main Scroll
        self.view.addSubview(self.mainScroll)

        mainScroll.anchor(top: self.headerView.bottomAnchor, leading: self.view.leadingAnchor, bottom: self.sendBookingRequest.topAnchor, trailing: self.view.trailingAnchor)


        // Main Scroll Content View
        self.mainScroll.addSubview(self.mainScrollContentview)

        self.mainScrollContentview.anchor(top: self.mainScroll.topAnchor, leading: self.mainScroll.leadingAnchor, bottom: self.mainScroll.bottomAnchor, trailing: self.mainScroll.trailingAnchor, size: .init(width: self.FullWidth, height: 0))


        // Event Imageview
        self.mainScrollContentview.addSubview(self.eventImageview)

        self.eventImageview.anchor(top: self.mainScrollContentview.topAnchor, leading: self.mainScrollContentview.leadingAnchor, bottom: nil, trailing: self.mainScrollContentview.trailingAnchor, size: .init(width: 0, height: 350))


        // Event Name
        self.mainScrollContentview.addSubview(self.eventName)

        self.eventName.font = UIFont(name: self.fontLabel.font.fontName, size: CGFloat(self.Get_fontSize(size: 16)))
        self.eventName.anchor(top: self.eventImageview.bottomAnchor, leading: self.mainScrollContentview.leadingAnchor, bottom: nil, trailing: self.mainScrollContentview.trailingAnchor, padding: .init(top: 10, left: 15, bottom: 10, right: 15))



        // Address View
        self.mainScrollContentview.addSubview(self.addressView)

        self.addressView.anchor(top: self.eventName.bottomAnchor, leading: self.mainScrollContentview.leadingAnchor, bottom: nil, trailing: self.mainScrollContentview.trailingAnchor, padding: .init(top: 10, left: 15, bottom: 0, right: 15))


        self.addressView.addSubview(self.addressPlaceholderImageview)

        self.addressPlaceholderImageview.anchor(top: self.addressView.topAnchor, leading: self.addressView.leadingAnchor, bottom: nil, trailing: nil, size: .init(width: 20, height: 20))

        self.addressView.addSubview(self.address)

        self.address.font = UIFont(name: self.fontLabel.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
        address.anchor(top: self.addressView.topAnchor, leading: self.addressPlaceholderImageview.trailingAnchor, bottom: self.addressView.bottomAnchor, trailing: self.addressView.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 0, right: 0))
        address.heightAnchor.constraint(greaterThanOrEqualToConstant: 20)




        // Date and Time View
        self.mainScrollContentview.addSubview(self.dateTimeView)

        self.dateTimeView.anchor(top: self.addressView.bottomAnchor, leading: self.mainScrollContentview.leadingAnchor, bottom: nil, trailing: self.mainScrollContentview.trailingAnchor, padding: .init(top: 20, left: 15, bottom: 0, right: 15))

        self.dateTimeView.addSubview(self.startDateLBL)

        self.startDateLBL.font = UIFont(name: self.fontLabel.font.fontName, size: 14)
        self.startDateLBL.anchor(top: self.dateTimeView.topAnchor, leading: self.dateTimeView.leadingAnchor, bottom: nil, trailing: nil)

        self.dateTimeView.addSubview(self.startDate)

        self.startDate.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 14)
        self.startDate.anchor(top: self.dateTimeView.topAnchor, leading: self.startDateLBL.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 0, left: 10, bottom: 0, right: 0))
        self.startDate.trailingAnchor.constraint(greaterThanOrEqualTo: self.dateTimeView.trailingAnchor, constant: 0).isActive = true


        self.dateTimeView.addSubview(self.eventTimeLBL)

        self.eventTimeLBL.font = UIFont(name: self.fontLabel.font.fontName, size: 14)
        eventTimeLBL.anchor(top: self.startDate.bottomAnchor, leading: self.dateTimeView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 10, left: 0, bottom: 0, right: 0))

        self.dateTimeView.addSubview(self.eventTime)

        self.eventTime.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 14)
        self.eventTime.anchor(top: self.startDate.bottomAnchor, leading: self.eventTimeLBL.trailingAnchor, bottom: self.dateTimeView.bottomAnchor, trailing: nil, padding: .init(top: 10, left: 10, bottom: 0, right: 0))
        self.eventTime.trailingAnchor.constraint(greaterThanOrEqualTo: self.dateTimeView.trailingAnchor, constant: 0).isActive = true



        // Event Specification View

        self.mainScrollContentview.addSubview(self.eventSpecificationView)

        self.eventSpecificationView.anchor(top: self.dateTimeView.bottomAnchor, leading: self.mainScrollContentview.leadingAnchor, bottom: nil, trailing: self.mainScrollContentview.trailingAnchor, padding: .init(top: 20, left: 15, bottom: 0, right: 15))

        self.eventSpecificationView.addSubview(self.eventSpecificationLBL)

        self.eventSpecificationLBL.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 14)
        self.eventSpecificationLBL.anchor(top: self.eventSpecificationView.topAnchor, leading: self.eventSpecificationView.leadingAnchor, bottom: nil, trailing: self.eventSpecificationView.trailingAnchor)

        self.eventSpecificationView.addSubview(self.eventSpecificationTable)

        self.eventSpecificationTable.anchor(top: self.eventSpecificationLBL.bottomAnchor, leading: self.eventSpecificationView.leadingAnchor, bottom: self.eventSpecificationView.bottomAnchor, trailing: self.eventSpecificationView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        self.eventSpecificationTable.delegate = self
        self.eventSpecificationTable.dataSource = self



        // Details View

        self.mainScrollContentview.addSubview(self.detailsView)

        self.detailsView.anchor(top: self.eventSpecificationView.bottomAnchor, leading: self.mainScrollContentview.leadingAnchor, bottom: nil, trailing: self.mainScrollContentview.trailingAnchor, padding: .init(top: 20, left: 15, bottom: 0, right: 15))

        self.detailsView.addSubview(self.detailsLBL)

        self.detailsLBL.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 14)
        self.detailsLBL.anchor(top: self.detailsView.topAnchor, leading: self.detailsView.leadingAnchor, bottom: nil, trailing: self.detailsView.trailingAnchor)

        self.detailsView.addSubview(self.eventDetails)

        self.eventDetails.font = UIFont(name: self.fontLabel.font.fontName, size: 14)
        self.eventDetails.anchor(top: self.detailsLBL.bottomAnchor, leading: self.detailsView.leadingAnchor, bottom: self.detailsView.bottomAnchor, trailing: self.detailsView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0))



        // Posted By View

        self.mainScrollContentview.addSubview(self.postedByView)

        self.postedByView.anchor(top: self.detailsView.bottomAnchor, leading: self.mainScrollContentview.leadingAnchor, bottom: nil, trailing: self.mainScrollContentview.trailingAnchor, padding: .init(top: 0, left: 15, bottom: 0, right: 15))

        self.postedByView.addSubview(self.postedByLBL)

        self.postedByLBL.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 15)
        self.postedByLBL.anchor(top: self.postedByView.topAnchor, leading: self.postedByView.leadingAnchor, bottom: nil, trailing: nil)




        self.postedByView.addSubview(self.postedByBottomView)

        self.postedByBottomView.anchor(top: self.postedByLBL.bottomAnchor, leading: self.postedByView.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 5, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 2))
        self.postedByBottomView.widthAnchor.constraint(equalTo: self.postedByLBL.widthAnchor, multiplier: 1).isActive = true


        self.postedByView.addSubview(self.sellerName)

        self.sellerName.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 14)
        self.sellerName.anchor(top: self.postedByBottomView.bottomAnchor, leading: self.postedByView.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        self.sellerName.widthAnchor.constraint(equalTo: self.postedByView.widthAnchor, multiplier: 0.5).isActive = true

        self.postedByView.addSubview(self.feedbackPercentage)

        self.feedbackPercentage.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 14)
        self.feedbackPercentage.anchor(top: self.postedByBottomView.bottomAnchor, leading: self.sellerName.trailingAnchor, bottom: nil, trailing: self.postedByView.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        self.feedbackPercentage.widthAnchor.constraint(equalTo: self.postedByView.widthAnchor, multiplier: 0.5).isActive = true

        self.postedByView.addSubview(self.memberSince)

        self.memberSince.font = UIFont(name: self.fontLabel.font.fontName, size: 14)
        self.memberSince.anchor(top: self.sellerName.bottomAnchor, leading: self.postedByView.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 5, left: 0, bottom: 0, right: 0))
        self.memberSince.widthAnchor.constraint(equalTo: self.postedByView.widthAnchor, multiplier: 0.5).isActive = true

        self.postedByView.addSubview(self.feedback)

        self.feedback.font = UIFont(name: self.fontLabel.font.fontName, size: 14)
        self.feedback.anchor(top: self.sellerName.bottomAnchor, leading: self.memberSince.trailingAnchor, bottom: nil, trailing: self.postedByView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0))
        self.feedback.widthAnchor.constraint(equalTo: self.postedByView.widthAnchor, multiplier: 0.5).isActive = true



        let sendMessageView : UIView = {
            let sendmessageview = UIView()
            //sendmessageview.backgroundColor = .green
            sendmessageview.translatesAutoresizingMaskIntoConstraints = false
            return sendmessageview
        }()

        self.postedByView.addSubview(sendMessageView)
        sendMessageView.addSubview(self.sendMessage)

        sendMessageView.anchor(top: self.memberSince.bottomAnchor, leading: self.postedByView.leadingAnchor, bottom: self.postedByView.bottomAnchor, trailing: nil, padding: .init(top: 30, left: 0, bottom: 30, right: 0))
        sendMessageView.widthAnchor.constraint(equalTo: self.postedByView.widthAnchor, multiplier: 0.5).isActive = true

        self.sendMessage.titleLabel?.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 13)
        self.sendMessage.anchor(top: sendMessageView.topAnchor, leading: nil, bottom: sendMessageView.bottomAnchor, trailing: sendMessageView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 10), size: .init(width: 120, height: 30))
        //self.sendMessage.widthAnchor.constraint(equalTo: self.sendMessage.widthAnchor, multiplier: 1.3).isActive = true

        let viewAccountView : UIView = {
            let viewaccountview = UIView()
            //viewaccountview.backgroundColor = .red
            viewaccountview.translatesAutoresizingMaskIntoConstraints = false
            return viewaccountview
        }()


        self.postedByView.addSubview(viewAccountView)
        viewAccountView.addSubview(self.viewAccount)

        viewAccountView.anchor(top: self.memberSince.bottomAnchor, leading: nil, bottom: self.postedByView.bottomAnchor, trailing: self.postedByView.trailingAnchor, padding: .init(top: 30, left: 0, bottom: 30, right: 0))
        viewAccountView.widthAnchor.constraint(equalTo: self.postedByView.widthAnchor, multiplier: 0.5).isActive = true

        self.viewAccount.titleLabel?.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 13)
        self.viewAccount.anchor(top: viewAccountView.topAnchor, leading: viewAccountView.leadingAnchor, bottom: viewAccountView.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 10, bottom: 0, right: 0), size: .init(width: 120, height: 30))
        //self.viewAccount.widthAnchor.constraint(equalTo: self.viewAccount.widthAnchor, multiplier: 1.3).isActive = true






        // Location View

        self.mainScrollContentview.addSubview(self.locationView)

        self.locationView.anchor(top: self.postedByView.bottomAnchor, leading: self.mainScrollContentview.leadingAnchor, bottom: nil, trailing: self.mainScrollContentview.trailingAnchor, padding: .init(top: 15, left: 15, bottom: 0, right: 15))

        self.locationView.addSubview(self.locationLBL)

        self.locationLBL.font = UIFont(name: self.fontLabel.font.fontName, size: 14)
        locationLBL.anchor(top: self.locationView.topAnchor, leading: self.locationView.leadingAnchor, bottom: nil, trailing: nil, size: .init(width: 150, height: 0))

        self.locationView.addSubview(ratingImageview)

        self.ratingImageview.anchor(top: self.locationView.topAnchor, leading: nil, bottom: nil, trailing: self.locationView.trailingAnchor, size: .init(width: 20, height: 20))

        self.locationView.addSubview(self.sellerRating)

        self.sellerRating.font = UIFont(name: self.fontLabel.font.fontName, size: 14)
        self.sellerRating.anchor(top: self.locationView.topAnchor, leading: nil, bottom: nil, trailing: self.ratingImageview.leadingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 10))

        self.locationView.addSubview(self.sellerLocation)

        self.sellerLocation.font = UIFont(name: self.fontLabel.font.fontName, size: 14)
        self.sellerLocation.anchor(top: self.ratingImageview.bottomAnchor, leading: self.locationView.leadingAnchor, bottom: self.locationView.bottomAnchor, trailing: self.locationView.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0))


        // Booking Time View

        self.mainScrollContentview.addSubview(self.bookingStackview)

        self.bookingStackview.anchor(top: self.locationView.bottomAnchor, leading: self.mainScrollContentview.leadingAnchor, bottom: self.mainScrollContentview.bottomAnchor, trailing: self.mainScrollContentview.trailingAnchor, padding: .init(top: 10, left: 15, bottom: 10, right: 15))

        self.bookingStackview.addArrangedSubview(self.bookingTimeView)


        self.bookingTimeView.addSubview(self.bookingTimeCollectionview)

        self.bookingTimeCollectionview.anchor(top: self.bookingTimeView.topAnchor, leading: self.bookingTimeView.leadingAnchor, bottom: nil, trailing: self.bookingTimeView.trailingAnchor, size: .init(width: 0, height: 120))
        self.bookingTimeCollectionview.delegate = self
        self.bookingTimeCollectionview.dataSource = self

        self.bookingTimeView.addSubview(self.bookingTimeTableview)

        self.bookingTimeTableview.anchor(top: self.bookingTimeCollectionview.bottomAnchor, leading: self.bookingTimeView.leadingAnchor, bottom: self.bookingTimeView.bottomAnchor, trailing: self.bookingTimeView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        self.bookingTimeTableview.delegate = self
        self.bookingTimeTableview.dataSource = self


        self.bookingTimeView.addSubview(self.nextButton)

        self.nextButton.anchor(top: self.bookingTimeView.topAnchor, leading: nil, bottom: nil, trailing: self.bookingTimeCollectionview.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 10), size: .init(width: 20, height: 20))

        self.bookingTimeView.addSubview(self.previousButton)

        self.previousButton.anchor(top: self.bookingTimeView.topAnchor, leading: self.bookingTimeCollectionview.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 10, left: 10, bottom: 0, right: 0), size: .init(width: 20, height: 20))


        if self.availableTimeArray.count > 0 {
            self.bookingTimeView.isHidden = false
        }
        else {
            self.bookingTimeView.isHidden = true
        }
        
        self.view.addSubview(self.ShowReport)
        
        self.ShowReport.anchor(top: self.view.topAnchor, leading: self.view.leadingAnchor, bottom: self.view.bottomAnchor, trailing: self.view.trailingAnchor)
        
        self.ShowReport.addSubview(self.ReportBox)
        
        self.ShowReport.addSubview(self.hideReportButton)
        
        self.hideReportButton.anchor(top: self.ShowReport.topAnchor, leading: self.ShowReport.leadingAnchor, bottom: self.ShowReport.bottomAnchor, trailing: self.ShowReport.trailingAnchor)
        
        self.ReportBox.widthAnchor.constraint(equalToConstant: 300).isActive = true
        
        self.ReportBox.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
        self.ReportBox.centerYAnchor.constraint(equalToSystemSpacingBelow: self.ShowReport.centerYAnchor, multiplier: 0).isActive = true
        
        self.ReportBox.centerXAnchor.constraint(equalToSystemSpacingAfter: self.ShowReport.centerXAnchor, multiplier: 0).isActive = true

    }


    // MARK:- // Put Data

    func putData() {

        self.sendBookingRequest.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sendBookingRequest", comment: ""), for: .normal)

        self.eventImageview.sd_setImage(with: URL(string: "\((((self.allDataDictionary["event_details"] as! NSDictionary)["event_img"] as! NSArray)[0] as! NSDictionary)["image"] ?? "")"))

        self.eventName.text = "\("\(((self.allDataDictionary["event_details"] as! NSDictionary)["event_name"] ?? ""))")"

        self.addressPlaceholderImageview.image = UIImage(named: "placeholder")

        self.address.text = "\("\(((self.allDataDictionary["event_details"] as! NSDictionary)["address"] ?? ""))")"

        self.startDateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "startDate", comment: "")

        self.startDate.text = "\("\(((self.allDataDictionary["event_details"] as! NSDictionary)["start_time"] ?? ""))")" + " - " + "\("\(((self.allDataDictionary["event_details"] as! NSDictionary)["end_time"] ?? ""))")"

        self.eventTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "time", comment: "")

        self.eventTime.text = "\("\(((self.allDataDictionary["event_details"] as! NSDictionary)["opening_time"] ?? ""))")" + " - " + "\("\(((self.allDataDictionary["event_details"] as! NSDictionary)["closing_time"] ?? ""))")"

        self.eventSpecificationLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventSpecifications", comment: "")


        self.detailsLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "details", comment: "")

        self.eventDetails.text = "\("\(((self.allDataDictionary["event_details"] as! NSDictionary)["event_description"] ?? ""))")"

        self.postedByLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "posted_by", comment: "")


        self.sellerName.text = "\("\((((self.allDataDictionary["event_details"] as! NSDictionary)["seller_info"] as! NSDictionary)["display_name"] ?? ""))")"

        self.feedbackPercentage.text = "\("\((((self.allDataDictionary["event_details"] as! NSDictionary)["seller_info"] as! NSDictionary)["avg_rating"] ?? ""))")" + " %"

        self.memberSince.text = "\("\((((self.allDataDictionary["event_details"] as! NSDictionary)["seller_info"] as! NSDictionary)["member_since"] ?? ""))")"

        self.feedback.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "positiveFeedback", comment: "")

        self.sendMessage.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sendMessage", comment: ""), for: .normal)
        self.viewAccount.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "viewAccount", comment: ""), for: .normal)

        self.locationLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "location", comment: "")

        self.sellerRating.text = "\("\((((self.allDataDictionary["event_details"] as! NSDictionary)["seller_info"] as! NSDictionary)["total_rating"] ?? ""))")"

        self.ratingImageview.image = UIImage(named: "Shape")

        self.sellerLocation.text = "\("\((((self.allDataDictionary["event_details"] as! NSDictionary)["seller_info"] as! NSDictionary)["location"] ?? ""))")"



    }





    // MARK:- // Set Table Heights

    func setTableHeights() {
        var tempHeight :  CGFloat! = 0

        tempHeight = CGFloat(40 * ((self.allDataDictionary["event_details"] as! NSDictionary)["other_otp"] as! NSArray).count)

//        print("Event Booking Table Height -----",tempHeight)

        self.eventSpecificationTable.heightAnchor.constraint(equalToConstant: tempHeight).isActive = true


        if self.availableTimeArray.count > 0 {
            tempHeight = CGFloat(35 * ((self.availableTimeArray[self.testint] as! NSDictionary)["time_slot_list"] as! NSArray).count)


//            print("Booking time Table Height -----",tempHeight)

            self.bookingtimeTableHeightConstraint = self.bookingTimeTableview.heightAnchor.constraint(equalToConstant: tempHeight)

            self.bookingTimeTableview.addConstraint(self.bookingtimeTableHeightConstraint)
        }




    }




    // MARK:- // Tap Next

    func TapOnNext()
    {
        testint = testint + 1

        if testint > (self.availableTimeArray.count - 1) // tapping next on last item
        {
            testint = 0

            // Scrolling CollectionView

            let nextDate: IndexPath = IndexPath(item: testint, section: 0)
            // Scroll happens here
            if nextDate.row < self.availableTimeArray.count {
                self.bookingTimeCollectionview.scrollToItem(at: nextDate, at: .left, animated: true)

            }

        }
        else  // tapping next normally
        {

            // Scrolling CollectionView According to Selected Property

            let nextDate: IndexPath = IndexPath(item: testint, section: 0)
            // Scroll happens here
            if nextDate.row < self.availableTimeArray.count {
                self.bookingTimeCollectionview.scrollToItem(at: nextDate, at: .left, animated: true)

            }


        }

    }


    // MARK:- // Tap on Previous

    func TapOnPrevious()
    {

        testint = testint - 1

        if testint < 0   // tapping previous on first item
        {
            testint = self.availableTimeArray.count - 1

            // Scrolling CollectionView According to Selected Property

            let nextDate: IndexPath = IndexPath(item: testint, section: 0)
            // Scroll happens here
            if nextDate.row < self.availableTimeArray.count {
                self.bookingTimeCollectionview.scrollToItem(at: nextDate, at: .left, animated: true)

            }

        }
        else  // tapping previous normally
        {

            // Scrolling CollectionView According to Selected Property

            let nextDate: IndexPath = IndexPath(item: testint, section: 0)
            // Scroll happens here
            if nextDate.row < self.availableTimeArray.count {
                self.bookingTimeCollectionview.scrollToItem(at: nextDate, at: .left, animated: true)

            }

        }
    }


    // MARK:- // Hide Previous Arrow on first and Next Scroll on Last

    func hideNextOrPrevious()
    {

        if testint == 0
        {
            self.previousButton.isHidden = true
            self.nextButton.isHidden = false
        }
        else if testint == (self.availableTimeArray.count - 1)
        {
            self.previousButton.isHidden = false
            self.nextButton.isHidden = true
        }
        else
        {
            self.previousButton.isHidden = false
            self.nextButton.isHidden = false
        }

    }


    // MARK:- // Next Button Action Method

    @objc func tapOnNext(sender : UIButton) {

        self.TapOnNext()

        self.bookingTimeTableview.reloadData()

        self.hideNextOrPrevious()

        let tempHeight = CGFloat(35 * ((self.availableTimeArray[self.testint] as! NSDictionary)["time_slot_list"] as! NSArray).count)

        self.bookingtimeTableHeightConstraint.constant = tempHeight

        self.checkImageArray.removeAll()

    }


    // MARK:- // Previous Button Action Method

    @objc func tapOnPrevious(sender : UIButton) {

        self.TapOnPrevious()

        self.bookingTimeTableview.reloadData()

        self.hideNextOrPrevious()

        let tempHeight = CGFloat(35 * ((self.availableTimeArray[self.testint] as! NSDictionary)["time_slot_list"] as! NSArray).count)

        self.bookingtimeTableHeightConstraint.constant = tempHeight

        self.checkImageArray.removeAll()

    }




    // MARK: - // JSON POST Method to get Event Details Data

    func getEventDetails()

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }



        let url = URL(string: GLOBALAPI + "app_event_details")!   //change the url

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        parameters = "user_id=\(userID!)&lang_id=\(langID!)&event_id=\(self.eventID!)"

        print("Event Details URL is : ",url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {

            request.httpBody = parameters.data(using: String.Encoding.utf8)

        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            guard let data = data else {

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Event Details Response: " , json)


                    if (json["response"] as! Bool) == false
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()

                        }

                    }
                    else
                    {

                        self.allDataDictionary = json["info_array"] as? NSDictionary

                        self.availableTimeArray = (self.allDataDictionary["event_details"] as! NSDictionary)["available_time"] as? NSArray

                        print(self.availableTimeArray.count)

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            SVProgressHUD.dismiss()
                        }
                    }


                }
                else
                {
                    DispatchQueue.main.async {

                        self.dispatchGroup.leave()

                        SVProgressHUD.dismiss()

                    }
                }

            } catch let error {
                print(error.localizedDescription)

                DispatchQueue.main.async {

                    self.dispatchGroup.leave()

                    SVProgressHUD.dismiss()
                }

            }
        })

        task.resume()



    }
    
    
    //MARK:- //View Account Add Target Method
    
    @objc func ViewAccountTarget(sender : UIButton)
    {
        if "\(((self.allDataDictionary["event_details"] as! NSDictionary)["seller_info"] as! NSDictionary)["seller_type"] ?? "")".elementsEqual("B")
        {
            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController
            
            nav.sellerID = "\(((self.allDataDictionary["event_details"] as! NSDictionary)["seller_info"] as! NSDictionary)["seller_id"] ?? "")"
            
            self.navigationController?.pushViewController(nav, animated: true)
        }
        else
        {
            let nav = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "privateUserProfileVC") as! PrivateUserProfileViewController
            
            nav.sellerID = "\(((self.allDataDictionary["event_details"] as! NSDictionary)["seller_info"] as! NSDictionary)["seller_id"] ?? "")"
            
            self.navigationController?.pushViewController(nav, animated: true)
        }
    }
    
    @objc func SendMessageTarget(sender : UIButton)
    {
        print("Tap on Message")
        
        if self.ShowReport.isHidden == true
        {
            self.ShowReport.isHidden = false
            self.view.bringSubviewToFront(self.ShowReport)
        }
        else if self.ShowReport.isHidden == false
        {
            self.ShowReport.isHidden = true
            self.view.sendSubviewToBack(self.ShowReport)
        }
    }
    
    @objc func hideReportViewTarget(sender : UIButton)
    {
        self.ShowReport.isHidden = true
        
        self.view.sendSubviewToBack(self.ShowReport)
    }

}



// MARK:- // Tableview Delegates


extension HappeningDetailsViewController: UITableViewDelegate,UITableViewDataSource {


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == self.eventSpecificationTable {
            return ((self.allDataDictionary["event_details"] as! NSDictionary)["other_otp"] as! NSArray).count
        }
        else {
            if self.availableTimeArray.count == 0 {
                return 0
            }
            else {
                return ((self.availableTimeArray[testint] as! NSDictionary)["time_slot_list"] as! NSArray).count
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == self.eventSpecificationTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell")

            let cellView : UIView = {
                let cellview = UIView()
                cellview.translatesAutoresizingMaskIntoConstraints = false
                return cellview
            }()

            let optionNameLBL : UILabel = {
                let optionnamelbl = UILabel()
                optionnamelbl.numberOfLines = 0
                optionnamelbl.textAlignment = .natural
                optionnamelbl.translatesAutoresizingMaskIntoConstraints = false
                return optionnamelbl
            }()

            let optionName : UILabel = {
                let optionname = UILabel()
                optionname.numberOfLines = 0
                optionname.textAlignment = .natural
                optionname.translatesAutoresizingMaskIntoConstraints = false
                return optionname
            }()

            cell?.addSubview(cellView)
            cellView.addSubview(optionNameLBL)
            cellView.addSubview(optionName)

            cellView.anchor(top: cell?.topAnchor, leading: cell?.leadingAnchor, bottom: cell?.bottomAnchor, trailing: cell?.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0))

            optionNameLBL.font = UIFont(name: self.fontLabel.font.fontName, size: 13)
            optionNameLBL.anchor(top: cellView.topAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, size: .init(width: 0, height: 15))


            optionName.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 14)
            optionName.anchor(top: optionNameLBL.bottomAnchor, leading: cellView.leadingAnchor, bottom: cellView.bottomAnchor, trailing: cellView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 20))

            optionNameLBL.text = "\((((self.allDataDictionary["event_details"] as! NSDictionary)["other_otp"] as! NSArray)[indexPath.row] as! NSDictionary)["opt_name"] ?? "")"

            optionName.text = "\((((self.allDataDictionary["event_details"] as! NSDictionary)["other_otp"] as! NSArray)[indexPath.row] as! NSDictionary)["other_otp"] ?? "")"


            cell?.selectionStyle = UITableViewCell.SelectionStyle.none


            return cell!
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "bookingTime")

            cell?.backgroundColor = UIColor(red: 0/255, green: 209/255, blue: 114/255, alpha: 1)

            for view in (cell?.subviews)! {
                view.removeFromSuperview()
            }

            let timeLBL : UILabel = {
                let timelbl = UILabel()
                timelbl.textColor = .white
                timelbl.translatesAutoresizingMaskIntoConstraints = false
                return timelbl
            }()

            cell?.addSubview(timeLBL)

            timeLBL.font = UIFont(name: self.fontLabel.font.fontName, size: 14)
            timeLBL.anchor(top: cell?.topAnchor, leading: cell?.leadingAnchor, bottom: cell?.bottomAnchor, trailing: cell?.trailingAnchor, padding: .init(top: 5, left: 35, bottom: 5, right: 5), size: .init(width: 0, height: 25))


            let checkImage : UIImageView = {
                let checkimage = UIImageView()
                checkimage.contentMode = .scaleAspectFit
                checkimage.clipsToBounds = true
                checkimage.translatesAutoresizingMaskIntoConstraints = false
                return checkimage
            }()

            cell?.addSubview(checkImage)

            checkImage.anchor(top: cell?.topAnchor, leading: nil, bottom: cell?.bottomAnchor, trailing: cell?.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 5, right: 10), size: .init(width: 25, height: 25))

            self.checkImageArray.append(checkImage)


            let separatorView : UIView = {
                let separatorview = UIView()
                separatorview.backgroundColor = .white
                separatorview.translatesAutoresizingMaskIntoConstraints = false
                return separatorview
            }()

            cell?.addSubview(separatorView)

            separatorView.anchor(top: nil, leading: cell?.leadingAnchor, bottom: cell?.bottomAnchor, trailing: cell?.trailingAnchor, size: .init(width: 0, height: 1))



            timeLBL.text = "\((((self.availableTimeArray[testint] as! NSDictionary)["time_slot_list"] as! NSArray)[indexPath.row] as! NSDictionary)["time"] ?? "")"

            cell?.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell!
        }




    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.bookingTimeTableview {
            for i in 0..<checkImageArray.count
            {
                checkImageArray[i].image = UIImage(named: " ")
            }

            checkImageArray[indexPath.row].image = UIImage(named: "checked")

            self.dateIndexpath = indexPath.row
        }

    }






}



// MARK:- // Collectionview Delegates

extension HappeningDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return self.availableTimeArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bookingTime", for: indexPath)

        let cellView : UIView = {
            let cellview = UIView()
            cellview.backgroundColor = UIColor(red: 0/255, green: 175/255, blue: 95/255, alpha: 1)
            cellview.translatesAutoresizingMaskIntoConstraints = false
            return cellview
        }()

        let dateLBL : UILabel = {
            let datelbl = UILabel()
            datelbl.textAlignment = .natural
            datelbl.numberOfLines = 0
            datelbl.textColor = .white
            datelbl.translatesAutoresizingMaskIntoConstraints = false
            return datelbl
        }()

        cell.addSubview(cellView)

        cellView.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: cell.bottomAnchor, trailing: cell.trailingAnchor, size: .init(width: self.FullWidth - 30, height: 120))

        cell.addSubview(dateLBL)

        dateLBL.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 25)
        dateLBL.anchor(top: cellView.topAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 50, left: 20, bottom: 0, right: 0), size: .init(width: 0, height: 30))


        let monthYearLBL : UILabel = {
            let monthyearlbl = UILabel()
            monthyearlbl.textAlignment = .natural
            monthyearlbl.numberOfLines = 0
            monthyearlbl.textColor = .white
            monthyearlbl.translatesAutoresizingMaskIntoConstraints = false
            return monthyearlbl
        }()

        cellView.addSubview(monthYearLBL)

        monthYearLBL.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 18)
        monthYearLBL.anchor(top: dateLBL.bottomAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: 20), size: .init(width: 0, height: 25))


        let separatorView : UIView = {
            let separatorview = UIView()
            separatorview.backgroundColor = .white
            separatorview.translatesAutoresizingMaskIntoConstraints = false
            return separatorview
        }()

        cellView.addSubview(separatorView)

        separatorView.topAnchor.constraint(equalTo: cellView.topAnchor, constant: 50).isActive = true
        separatorView.widthAnchor.constraint(equalToConstant: 2).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        separatorView.centerXAnchor.constraint(equalTo: cellView.centerXAnchor).isActive = true


        let dayLBL : UILabel = {
            let daylbl = UILabel()
            daylbl.textAlignment = .natural
            daylbl.numberOfLines = 0
            daylbl.textColor = .white
            daylbl.translatesAutoresizingMaskIntoConstraints = false
            return daylbl
        }()

        cellView.addSubview(dayLBL)

        dayLBL.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: 20)
        dayLBL.anchor(top: cellView.topAnchor, leading: separatorView.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 50, left: 20, bottom: 0, right: 0))

        var dateMonthYearArray = NSArray()

        dateMonthYearArray = "\((self.availableTimeArray[indexPath.item] as! NSDictionary)["date"] ?? "")".components(separatedBy: "-") as NSArray

        dateLBL.text = "\(dateMonthYearArray[2])"
        //monthYearLBL.text = "\(self.monthArray[Int("\(dateMonthYearArray[1])")!])" + ", \(dateMonthYearArray[0])"
        dayLBL.text = "\((self.availableTimeArray[indexPath.item] as! NSDictionary)["day"] ?? "")"


        return cell

    }




}



