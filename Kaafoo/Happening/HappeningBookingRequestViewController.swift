//
//  HappeningBookingRequestViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 22/02/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class HappeningBookingRequestViewController: UIViewController,UITextViewDelegate {
    @IBAction func Cross_Btn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var Change_Btn_Outlet: UIButton!
    
    @IBOutlet weak var date_lbl: UILabel!
    @IBOutlet weak var time_lbl: UILabel!
    @IBAction func Change_Btn(_ sender: UIButton) {
    }
    @IBOutlet weak var adult_txtfield: UITextField!
    @IBOutlet weak var child_txtfield: UITextField!
    
    @IBAction func booking_request_btn(_ sender: UIButton) {
    }
    @IBOutlet weak var booking_request_outlet: UIButton!
    @IBOutlet weak var msg_txtview: UITextView!
    @IBOutlet weak var phone_txtfield: UITextField!
    @IBOutlet weak var email_txtfield: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        msg_txtview.text = "Please leave your message here"
        msg_txtview.textColor = UIColor.lightGray
        msg_txtview.delegate = self


        // Do any additional setup after loading the view.
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        msg_txtview.text = ""
        msg_txtview.textColor = UIColor.black
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if msg_txtview.text.count == 0 {
            msg_txtview.textColor = UIColor.lightGray
            msg_txtview.text = "Please leave your message here"
            msg_txtview.resignFirstResponder()
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {

        if msg_txtview.text.count == 0 {
            msg_txtview.textColor = UIColor.lightGray
            msg_txtview.text = "Please leave your message here"
            msg_txtview.resignFirstResponder()
            
        }
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
