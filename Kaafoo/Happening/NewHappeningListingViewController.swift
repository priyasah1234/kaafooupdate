//
//  NewHappeningListingViewController.swift
//  Kaafoo
//
//  Created by esolz on 28/08/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class NewHappeningListingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var sortDict = [["key" : "" , "value" : "All"],["key" : "T" , "value" : "Title"],["key" : "C" , "value" : "Closing Soon"],["key" : "N" , "value" : "Newest Listed"]]
    
    
    @IBOutlet weak var DetailsText: UILabel!
    
    @IBOutlet weak var LocationLabel: UILabel!
    
    @IBOutlet weak var ListGridBtnOutlet: UIButton!
    
    @IBAction func ListGridBtn(_ sender: UIButton) {
        
        if HappeningListingView.isHidden == false
        {
            self.HappeningListingView.isHidden = true
            self.EventListingCollectionView.isHidden = false
            self.ListGridBtnOutlet.setImage(UIImage(named: "Group 18"), for: .normal)
        }
        else
        {
            self.HappeningListingView.isHidden = false
            self.EventListingCollectionView.isHidden = true
            self.ListGridBtnOutlet.setImage(UIImage(named: "list-1"), for: .normal)
        }
        
    }
    
    
    @IBOutlet weak var EventListingCollectionView: UICollectionView!
    @IBOutlet weak var LocalHeaderContentView: UIView!
    @IBOutlet weak var LocalHeaderView: UIView!
    @IBOutlet weak var MainScroll: UIScrollView!
    @IBOutlet weak var ScrollContentView: UIView!
    @IBOutlet weak var HappeningListingView: UITableView!
    
    
    @IBOutlet weak var SortPicker: UIPickerView!
    @IBOutlet weak var ShowPickerView: UIView!
    @IBOutlet weak var HidePickerBtn: UIButton!
    @IBAction func HidePicker(_ sender: UIButton) {
        self.ShowPickerView.isHidden = true
    }
    
    @IBOutlet weak var SortBtnOutlet: UIButton!
    @IBAction func SortBtn(_ sender: UIButton) {
        self.ShowPickerView.isHidden = false
    }
    
    
    var ProId: String!
    var StartDate : String! = ""
    var EndDate : String! = ""
    var SelectedCountry : String! = ""
    var SelectedCity : String! = ""
    var SearchWord : String! = ""
    var Currentlang : String! = "22.572646"
    var CurrentLong : String! = "88.363895"
    var SearchOptionFirst : String! = ""
    var SearchOptionTwo : String! = ""
    var UserStatusSearch : String! = ""
    var SortSearch : String! = ""
    var MAxDist : String! = ""
    var SelectedState : String! = ""
    var start_value = 0
    var per_value = 10
    
    var HappeningStructArray = [HappeningStruct]()
    var SelectedInt : Int!
    
    let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")
    let userID = UserDefaults.standard.string(forKey: "userID")
    let MyCountryName = UserDefaults.standard.string(forKey: "myCountryName")
    
    var EventListArray : NSMutableArray! = NSMutableArray()
    
    @IBOutlet weak var HappeningListingTableViewHeightConstraint: NSLayoutConstraint!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDetailsView()
       
        
        self.ListGridBtnOutlet.setImage(UIImage(named: "Group 18"), for: .normal)
        self.EventListingCollectionView.isHidden = true
        self.HappeningListingView.isHidden = false
        self.GetListing()
        self.SortPicker.delegate = self
        self.SortPicker.dataSource = self
        self.SortPicker.reloadAllComponents()
       
        // Do any additional setup after loading the view.
    }
    
    func setDetailsView()
    {
        self.LocationLabel.text = HappeningSearchParameters.shared.SelectedCity + " , " + HappeningSearchParameters.shared.SelectedState
        self.DetailsText.text = HappeningSearchParameters.shared.StartDate + " - " + HappeningSearchParameters.shared.EndDate
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setLayer()
    }
    
    func setLayer()
    {
      self.LocalHeaderContentView.layer.borderColor = UIColor.lightGray.cgColor
      self.LocalHeaderContentView.layer.borderWidth = 1.0
      self.LocalHeaderContentView.clipsToBounds = true
      self.LocalHeaderContentView.layer.cornerRadius = 5.0
    }
    
    func setAPIdata()
    {
        self.HappeningListingView.delegate = self
        self.HappeningListingView.dataSource = self
        self.HappeningListingView.reloadData()
        
        self.HappeningListingView.rowHeight = UITableView.automaticDimension
        self.HappeningListingView.estimatedRowHeight = 300
        
        self.EventListingCollectionView.delegate = self
        self.EventListingCollectionView.dataSource = self
        self.EventListingCollectionView.reloadData()
        
        
    }
    
    //MARK:- //Set Data From Structure
    
    func setData()
    {
        for i in 0..<self.EventListArray.count
        {
            self.HappeningStructArray.append(HappeningStruct(EventNameString: "\((self.EventListArray[i] as! NSDictionary)["event_name"] ?? "")", EventAddressString: "\((self.EventListArray[i] as! NSDictionary)["address"] ?? "")", EventDatesString: ("\((self.EventListArray[i] as! NSDictionary)["start_date"] ?? "")" + "-" + "\((self.EventListArray[i] as! NSDictionary)["end_date"] ?? "")"), EventImageString: "\((self.EventListArray[i] as! NSDictionary)["photo"] ?? "")", EventTimingsString: ("\((self.EventListArray[i] as! NSDictionary)["opening_time"] ?? "")" + "-" + "\((self.EventListArray[i] as! NSDictionary)["closing_time"] ?? "")"), EventOptions: ((self.EventListArray[i] as! NSDictionary)["options"] as! NSArray), ProductID: "\((self.EventListArray[i] as! NSDictionary)["id"] ?? "")"))
        }
    }
    
    //MARK:- //UItableView Delegate and DataSource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.HappeningStructArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        SelectedInt = indexPath.row
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "happening") as! HappenningListingTVC
        cell.EventName.text = "\(self.HappeningStructArray[indexPath.row].EventNameString ?? "")"
        cell.EventAddress.text = "\(self.HappeningStructArray[indexPath.row].EventAddressString ?? "")"
        cell.EventDates.text = "Dates : " + "\(self.HappeningStructArray[indexPath.row].EventDatesString ?? "")"
        cell.EventTimings.text = "Timings : " + "\(self.HappeningStructArray[indexPath.row].EventTimingsString ?? "")"
        let path = "\(self.HappeningStructArray[indexPath.row].EventImageString ?? "")"
        cell.EventImage.sd_setImage(with: URL(string: path))
        cell.EventImage.contentMode = .scaleAspectFill
        
        cell.TypeCollectionView.showsVerticalScrollIndicator = false
        cell.TypeCollectionView.showsHorizontalScrollIndicator = false
        
        cell.ShadowView.layer.cornerRadius = 8.0
        
        cell.selectionStyle = .none
        cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "happeningDetailsVC") as! HappeningDetailsViewController
        
        navigate.eventID = "\(self.HappeningStructArray[indexPath.row].ProductID ?? "")"
        
        self.navigationController?.pushViewController(navigate, animated: true)
    }
    
    //MARK:- //Get Product Listing using Global API
    func GetListing()
    {
        let  parameters = "user_id=\(userID!)&mycountry_name=\(MyCountryName!)&start_value=\(start_value)&per_value=\(per_value)&lang_id=\(langID!)&city_name=\(SelectedCity!)&start_date=\(HappeningSearchParameters.shared.StartDate!)&end_date=\(HappeningSearchParameters.shared.EndDate!)&search_by_wrd=\(SearchWord!)&search_option2=\(SearchOptionTwo!)&search_option1=\(SearchOptionFirst!)&max_dist=\(MAxDist!)&current_lat=\(Currentlang!)&current_long=\(CurrentLong!)&user_status_search=\(UserStatusSearch!)&sort_search=\(SortSearch!)&state=\(SelectedState!)"
        self.CallAPI(urlString: "app_event_search_listing", param: parameters, completion: {
            
            let infoDict = (self.globalJson["info_array"] as! NSDictionary)
            
            self.EventListArray = (infoDict["event_list"] as! NSMutableArray)
            

            print("Json Object",self.globalJson!)
            
            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()
                SVProgressHUD.dismiss()
                
                 self.setData()
                
            }
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                self.setAPIdata()
//                self.SetTableheight(table: self.HappeningListingView, heightConstraint: self.HappeningListingTableViewHeightConstraint)
            })
           
        })
    }

    // MARK:- // Set Table Height
    
    func SetTableheight(table: UITableView , heightConstraint: NSLayoutConstraint) {
        var setheight: CGFloat  = 0
        table.frame.size.height = 3000
        
        for cell in table.visibleCells {
            setheight += cell.bounds.height
        }
        heightConstraint.constant = CGFloat(setheight)
    }
    
    //MARK:- //UICollectionView Delegate and datasource methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == EventListingCollectionView
        {
            return self.HappeningStructArray.count
        }
        else
        {
           return self.HappeningStructArray[SelectedInt].EventOptions.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == EventListingCollectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "event", for: indexPath) as! EventListingCVC
            cell.EventName.text = "\(self.HappeningStructArray[indexPath.row].EventNameString ?? "")"
//            cell.EventAddress.text = "\(self.HappeningStructArray[indexPath.row].EventAddressString ?? "")"
            cell.EventDates.text = "Dates : " + "\(self.HappeningStructArray[indexPath.row].EventDatesString ?? "")"
            cell.EventTimings.text = "Timings : " + "\(self.HappeningStructArray[indexPath.row].EventTimingsString ?? "")"
            let path = "\(self.HappeningStructArray[indexPath.row].EventImageString ?? "")"
            cell.EventImage.sd_setImage(with: URL(string: path))
            cell.EventImage.contentMode = .scaleAspectFill
            
            cell.ContentView.layer.cornerRadius = 8.0
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "option", for: indexPath) as! OptionsTVC
            cell.OptionNAme.text = "\(((self.HappeningStructArray[SelectedInt].EventOptions[indexPath.row] as! NSDictionary)["other_otp"] ?? ""))"
            
            cell.ContentView.layer.borderColor = UIColor.seaGreen().cgColor
            cell.ContentView.layer.borderWidth = 1.5
            cell.ContentView.clipsToBounds = true
            cell.ContentView.layer.cornerRadius = 5.0
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == EventListingCollectionView
        {
            let padding : CGFloat! = 4
            
            let collectionViewSize = self.EventListingCollectionView.frame.size.width - padding
            return CGSize(width: collectionViewSize/2, height: 310 )
            
        }
        else
        {
            let size: CGSize = "\(((self.HappeningStructArray[SelectedInt].EventOptions[indexPath.row] as! NSDictionary)["other_otp"] ?? ""))".size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0)])
            return CGSize(width: size.width + 45.0, height: 63)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == EventListingCollectionView
        {
            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "happeningDetailsVC") as! HappeningDetailsViewController
            navigate.eventID = "\(self.HappeningStructArray[indexPath.row].ProductID ?? "")"
            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            
        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class HappenningListingTVC : UITableViewCell
{
    @IBOutlet weak var EventImage: UIImageView!
    @IBOutlet weak var EventName: UILabel!
    @IBOutlet weak var EventAddress: UILabel!
    @IBOutlet weak var EventDates: UILabel!
    @IBOutlet weak var EventTimings: UILabel!
    @IBOutlet weak var TypeCollectionView: UICollectionView!
    @IBOutlet weak var ShadowView: ShadowView!
    
    
    
    
    
}

struct HappeningStruct
{
    var EventNameString : String?
    var EventAddressString : String?
    var EventDatesString : String?
    var EventTimingsString : String?
    var EventImageString : String?
    var EventOptions : NSArray!
    var ProductID : String?
    
    init(EventNameString : String , EventAddressString : String , EventDatesString : String , EventImageString : String , EventTimingsString : String , EventOptions : NSArray , ProductID : String)
    {
        self.EventNameString = EventNameString
        self.EventAddressString = EventAddressString
        self.EventDatesString = EventDatesString
        self.EventTimingsString = EventTimingsString
        self.EventImageString = EventImageString
        self.EventOptions = EventOptions
        self.ProductID = ProductID
    }    
}

class OptionsTVC : UICollectionViewCell
{
    @IBOutlet weak var OptionNAme: UILabel!
    @IBOutlet weak var ContentView: UIView!
}

extension HappenningListingTVC {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        TypeCollectionView.delegate = dataSourceDelegate
        TypeCollectionView.dataSource = dataSourceDelegate
        TypeCollectionView.tag = row
        
        TypeCollectionView.setContentOffset(TypeCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        TypeCollectionView.reloadData()
        TypeCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    var collectionViewOffset: CGFloat {
        set { TypeCollectionView.contentOffset.x = newValue }
        get { return TypeCollectionView.contentOffset.x }
    }
}

extension NewHappeningListingViewController : UIPickerViewDelegate,UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortDict.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ((sortDict[row] as NSDictionary)["value"] as! String)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.SortSearch = ((sortDict[row] as NSDictionary)["key"] as! String)
        self.ShowPickerView.isHidden = true
        self.HappeningStructArray = []
        self.GetListing()
    }
}

class EventListingCVC : UICollectionViewCell
{
    @IBOutlet weak var EventImage: UIImageView!
    @IBOutlet weak var EventName: UILabel!
    @IBOutlet weak var EventDates: UILabel!
    @IBOutlet weak var EventTimings: UILabel!
    @IBOutlet weak var EventWatchListBtn: UIButton!
    @IBOutlet weak var ShadowView: ShadowView!
    @IBOutlet weak var ContentView: UIView!
}


