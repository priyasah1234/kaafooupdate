//
//  AnimalsViewController.swift
//  Kaafoo
//
//  Created by admin on 16/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class CategoryViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    @IBOutlet weak var secondaryView: UIView!
    @IBOutlet weak var secondaryViewSeparatorView: UIView!
    @IBOutlet weak var filterImageview: UIImageView!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var allTypeView: UIView!
    @IBOutlet weak var categoryListView: UIView!
    @IBOutlet weak var allTypeTableView: UITableView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var categoryDropDownTableView: UITableView!

    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var noListingsAvailable: UILabel!

    @IBOutlet weak var sortBackgroundView: UIView!
    @IBOutlet weak var sortTransparentView: UIView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var upImage: UIImageView!
    @IBOutlet weak var sortContainerView: UIView!

    @IBOutlet weak var userAccountTypeView: UIView!
    @IBOutlet weak var privateLBL: UILabel!
    @IBOutlet weak var businessLBL: UILabel!
    @IBOutlet weak var sortCategoryTableview: UITableView!


    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var tapGesture = UITapGestureRecognizer()

    var allTypesDictionary : NSDictionary!

    var allTypesArray : NSArray!

    var allSubTypesDictionary : NSDictionary!

    var allSubTypesArray : NSArray!

    var allProductListingDictionary : NSDictionary!

    var allProductListingArray : NSArray!

    var sortCategoryArray = [["key" : "" , "value" : "ALL"] , ["key" : "LP" , "value" : "Lowest Price"] , ["key" : "HP" , "value" : "Highest Price"] , ["key" : "T" , "value" : "Title"] , ["key" : "C" , "value" : "Closing Soon"] , ["key" : "N" , "value" : "Newest Listed"] , ["key" : "B" , "value" : "Most Bids"]]

    var recordsArray : NSMutableArray! = NSMutableArray()

    var currentRecordsArray : NSMutableArray! = NSMutableArray()

    var categoryName : String!

    var categoryID : String!

    var typeID : String!

    var subTypeID : String!

    var productID : String!

    var typeIDForProducts : String!

    var watchListProductID : String!

    var watchListedStatus : String!

    let dispatchGroup = DispatchGroup()

    var searchString : String!

    var sortingType : String! = ""

    var userAccountTypeCommaSeparatedString : String! = ""

    var userAccountTypeArray = [String]()

    var privateSelected : Bool! = false
    var businessSelected : Bool! = false

    var typeCommaSeparatedString : String! = ""
    var filterCommaSeparatedString : String! = ""
    var conditionCommaSeparatedString : String! = ""



    override func viewDidLoad() {
        super.viewDidLoad()

        self.setStaticStrings()

        searchView.searchBar.delegate = self

        self.sortCategoryTableview.delegate = self
        self.sortCategoryTableview.dataSource = self

        self.sortContainerView.layer.cornerRadius = (5/320)*self.FullWidth

        headerView.headerViewTitle.text = categoryName

        self.categoryView.frame.origin.y = -categoryView.frame.size.height
        self.categoryView.frame.size.height = (headerView.frame.size.height + categoryTableView.frame.size.height)

        categoryView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        allTypeView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)

        headerView.dropDownButton.addTarget(self, action: #selector(CategoryViewController.headerDropDownClick), for: .touchUpInside)

        startProductListingData()

        self.setFont()

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        // RTL - Set Frames and Allignments for Arabic and English

//        let langID = UserDefaults.standard.string(forKey: "langID")

        //        if (langID?.elementsEqual("AR"))!
        //        {
        //            self.RTL()
        //            sideMenuView.frame.origin.x = (320/320)*self.FullWidth
        //        }
        //        else
        //        {
        //            sideMenuView.frame.origin.x = (-320/320)*self.FullWidth
        //        }

    }



    // MARK:- // Buttons


    // MARK:- // Sort Button

    @IBOutlet weak var sortButtonOutlet: UIButton!
    @IBAction func tapSortButton(_ sender: UIButton) {

        if self.sortBackgroundView.isHidden == true
        {
            if sortView.isHidden == true
            {
                self.sortView.isHidden = false
                self.sortCategoryTableview.isHidden = true
            }
            UIView.animate(withDuration: 0.5)
            {
                self.view.bringSubviewToFront(self.sortBackgroundView)
                self.sortBackgroundView.isHidden = false

                // *** Hide sortBackgroundView when tapping outside ***
                self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
                self.sortTransparentView.addGestureRecognizer(self.tapGesture)
            }
        }
        else
        {
            if sortView.isHidden == true
            {
                self.sortView.isHidden = false
                self.sortCategoryTableview.isHidden = true
            }
            else
            {
                UIView.animate(withDuration: 0.5)
                {
                    self.view.sendSubviewToBack(self.sortBackgroundView)
                    self.sortBackgroundView.isHidden = true

                    if self.sortCategoryTableview.isHidden == false
                    {
                        self.sortCategoryTableview.isHidden = true
                    }
                }
            }
        }

    }


    // MARK:- // Open Sort Category Button

    @IBOutlet weak var openSortCategory: UIButton!
    @IBAction func tapOpenSortCategory(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5)
        {
            self.sortCategoryTableview.isHidden = false
            self.view.bringSubviewToFront(self.sortCategoryTableview)
            self.sortView.isHidden = true
        }

    }



    // MARK:- // Inside Sort Search Button

    @IBOutlet weak var insideSortSearchButton: UIButton!
    @IBAction func tapInsideSOrtSearchButton(_ sender: UIButton) {
    }



    // MARK:- // Inside Sort Advanced Search Button

    @IBOutlet weak var insideSortAdvancedSearchButton: UIButton!
    @IBAction func tapInsideSortAdvancedSearchButton(_ sender: UIButton) {

//        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

        // let navigate = UIStoryboard(name: storyboardName!, bundle: nil).instantiateViewController(withIdentifier: "categoryAdvancedSearchVC") as! CategoryAdvancedSearchViewController

        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "categoryAdvancedSearchVC") as! CategoryAdvancedSearchViewController


        navigate.typeID = typeID

        self.navigationController?.pushViewController(navigate, animated: true)

    }



    // MARK:- // Private User Button

    @IBOutlet weak var privateUserButtonOutlet: UIButton!
    @IBAction func tapPrivateUser(_ sender: UIButton) {

        if privateSelected == false
        {
            self.userAccountTypeArray.append("P")

            self.userAccountTypeCommaSeparatedString = self.userAccountTypeArray.joined(separator: ",")

            self.privateUserButtonOutlet.setImage(UIImage(named: "checkBoxFilled"), for: .normal)
        }
        else
        {
            for i in 0..<userAccountTypeArray.count
            {
                if "\(userAccountTypeArray[i])".elementsEqual("P")
                {
                    userAccountTypeArray.remove(at: i)
                }
            }

            self.userAccountTypeCommaSeparatedString = self.userAccountTypeArray.joined(separator: ",")

            self.privateUserButtonOutlet.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
        }

        self.startValue = 0
        self.recordsArray.removeAllObjects()
        self.currentRecordsArray.removeAllObjects()

        self.startProductListingData()

        self.privateSelected = !privateSelected

    }


    // MARK:- // Business User Button

    @IBOutlet weak var businessUserButtonOutlet: UIButton!
    @IBAction func tapBusinessUser(_ sender: UIButton) {

        if businessSelected == false
        {
            self.userAccountTypeArray.append("B")

            self.userAccountTypeCommaSeparatedString = self.userAccountTypeArray.joined(separator: ",")

            self.businessUserButtonOutlet.setImage(UIImage(named: "checkBoxFilled"), for: .normal)
        }
        else
        {
            for i in 0..<userAccountTypeArray.count
            {
                if "\(userAccountTypeArray[i])".elementsEqual("B")
                {
                    userAccountTypeArray.remove(at: i)
                }
            }

            self.userAccountTypeCommaSeparatedString = self.userAccountTypeArray.joined(separator: ",")

            self.businessUserButtonOutlet.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
        }

        self.startValue = 0
        self.recordsArray.removeAllObjects()
        self.currentRecordsArray.removeAllObjects()

        self.startProductListingData()

        self.businessSelected = !businessSelected

    }



    // MARK:- // Type Drop Down CLick


    @IBOutlet weak var typeDropDownClickButtonOutlet: UIButton!
    @IBAction func typeDropDownClick(_ sender: Any) {

        closeTableViewsOutlet.isUserInteractionEnabled = true

        if allTypeView.isHidden{
            animateType(toggle: true)

        }
        else {
            animateType(toggle: false)

        }

    }



    // MARK:- // Close TableViews


    @IBOutlet weak var closeTableViewsOutlet: UIButton!
    @IBAction func closeTableViews(_ sender: Any) {

        if categoryView.isHidden == false
        {
            animateCategory(toggle: false)
        }
        else if allTypeView.isHidden == false
        {
            animateType(toggle: false)
        }

        closeTableViewsOutlet.isUserInteractionEnabled = false

    }






    // MARK:- // Delegate Functions

    // MARK:- // Tableview Delegates


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == categoryDropDownTableView
        {
            return allTypesArray.count
        }
        else if tableView == allTypeTableView
        {
            return allSubTypesArray.count
        }
        else if tableView == sortCategoryTableview
        {
            return sortCategoryArray.count
        }
        else
        {
            return currentRecordsArray.count
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        if tableView == categoryDropDownTableView  // header allCategorytableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell")

            let tempDictionary = allTypesArray[indexPath.row] as!  NSDictionary


            let categoryName = UserDefaults.standard.string(forKey: "categoryName")
            cell?.textLabel?.text = "\(tempDictionary[categoryName ?? ""]!)" + " " + "(" + "\(tempDictionary["total_count"]!)" + ")"


            // Set Font //

            cell?.textLabel?.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))

            ///////

            // RTL - Set Frames and Allignments for Arabic and English

            let langID = UserDefaults.standard.string(forKey: "langID")

            if (langID?.elementsEqual("AR"))!
            {
                cell?.textLabel?.textAlignment = .right
            }
            else
            {
                cell?.textLabel?.textAlignment = .left
            }

            //
            cell?.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell!
        }
        else if tableView == allTypeTableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "typeTableCell")


            let tempDictionary = allSubTypesArray[indexPath.row] as!  NSDictionary


            let categoryName = UserDefaults.standard.string(forKey: "categoryName")
            cell?.textLabel?.text = "\(tempDictionary[categoryName ?? ""]!)"  + " " + "(" + "\(tempDictionary["total_count"]!)" + ")"

            // Set Font //

            cell?.textLabel?.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))

            ///////

            // RTL - Set Frames and Allignments for Arabic and English

            let langID = UserDefaults.standard.string(forKey: "langID")

            if (langID?.elementsEqual("AR"))!
            {
                cell?.textLabel?.textAlignment = .right
            }
            else
            {
                cell?.textLabel?.textAlignment = .left
            }

            //
            cell?.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell!
        }

        else if tableView == sortCategoryTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! CategorySortTVCTableViewCell

            cell.cellLBL.text = "\((sortCategoryArray[indexPath.row] as NSDictionary)["value"]!)"

            // Set Font //

            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

            return cell
        }

        else  //tableView == categoryTableView // main tableview
        {


            let cell = tableView.dequeueReusableCell(withIdentifier: "productTableCell") as! CategoryTVCTableViewCell

            // Set Font //

            cell.cellTitle.font = UIFont(name: cell.cellTitle.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
            cell.bidCount.font = UIFont(name: cell.bidCount.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
            cell.listingTime.font = UIFont(name: cell.listingTime.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.closesIn.font = UIFont(name: cell.closesIn.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.businessLabel.font = UIFont(name: cell.businessLabel.font.fontName, size: CGFloat(Get_fontSize(size: 11)))
            cell.dailyDealsLBL.font = UIFont(name: cell.dailyDealsLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            cell.addressLabel.font = UIFont(name: cell.addressLabel.font.fontName, size: CGFloat(Get_fontSize(size: 11)))
            cell.buyNowLabel.font = UIFont(name: cell.buyNowLabel.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.buyNowPrice.font = UIFont(name: cell.buyNowPrice.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
            cell.reservePrice.font = UIFont(name: cell.reservePrice.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
            cell.refundTypeLBL.font = UIFont(name: cell.refundTypeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
            cell.watchListOutlet.titleLabel?.font = UIFont(name: (cell.watchListOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
            cell.removeFromWatchlistOutlet.titleLabel?.font = UIFont(name: (cell.removeFromWatchlistOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!

            ///////////

            //cell.cellView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)

            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell

        }



    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {



        if tableView == categoryTableView
        {
            let cell : CategoryTVCTableViewCell = cell as! CategoryTVCTableViewCell

            cell.cellTitle.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
            cell.bidCount.text = ""
            cell.listingTime.text = "Listed" + " " + "\((currentRecordsArray[indexPath.row] as! NSDictionary)["listed_time"]!)" + "ago"
            cell.closesIn.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["closed_time"]!)"
            cell.businessLabel.text = "\(((currentRecordsArray[indexPath.row] as! NSDictionary)["user_details"] as! NSDictionary)["User_type"]!)" + " : " + "\((currentRecordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"
            cell.addressLabel.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["address"]!)"

            cell.refundTypeLBL.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["refund_type"]!)"

            let path = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["product_photo"]!)"

            cell.productImage.sd_setImage(with: URL(string: path))

            if "\((currentRecordsArray[indexPath.row] as! NSDictionary)["special_offer_status"]!)".elementsEqual("0")
            {
                cell.buyNowPrice.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((currentRecordsArray[indexPath.row] as! NSDictionary)["start_price"]!)"
                cell.buyNowPrice.textColor = UIColor(displayP3Red: 0/255, green: 92/255, blue: 69/255, alpha: 1)

                cell.reservePrice.isHidden = true
            }
            else
            {
                cell.buyNowPrice.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((currentRecordsArray[indexPath.row] as! NSDictionary)["reserve_price"]!)"
                cell.buyNowPrice.textColor = UIColor(displayP3Red: 255/255, green: 6/255, blue: 3/255, alpha: 1)
                cell.reservePrice.isHidden = false
                cell.reservePrice.textColor = UIColor(displayP3Red: 0/255, green: 92/255, blue: 69/255, alpha: 1)
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\((currentRecordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((currentRecordsArray[indexPath.row] as! NSDictionary)["start_price"]!)")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))

                cell.reservePrice.attributedText = attributeString

                cell.reservePrice.isHidden = false
            }

            cell.watchListOutlet.tag = indexPath.row

            cell.watchListOutlet.addTarget(self, action: #selector(CategoryViewController.addToWatchList(sender:)), for: .touchUpInside)

            cell.removeFromWatchlistOutlet.tag = indexPath.row

            cell.removeFromWatchlistOutlet.addTarget(self, action: #selector(CategoryViewController.removeFromWatchList(sender:)), for: .touchUpInside)




            if "\((currentRecordsArray[indexPath.row] as! NSDictionary)["watchlist"]!)".elementsEqual("1")
            {
                cell.watchListOutlet.isHidden = true
                cell.removeFromWatchlistOutlet.isHidden = false
            }
            else
            {
                cell.watchListOutlet.isHidden = false
                cell.removeFromWatchlistOutlet.isHidden = true
            }


            if "\(((currentRecordsArray[indexPath.row] as! NSDictionary)["user_details"] as! NSDictionary)["User_type"]!)".elementsEqual("Admin")
            {
                cell.businessView.isHidden = true
                cell.kaafooLogo.isHidden = false
                cell.dailyDealsImageview.isHidden = false
                cell.dailyDealsLBL.isHidden = false

            }
            else
            {
                cell.businessView.isHidden = false
                cell.kaafooLogo.isHidden = true
                cell.dailyDealsImageview.isHidden = true
                cell.dailyDealsLBL.isHidden = true
            }

            if "\((currentRecordsArray[indexPath.row] as! NSDictionary)["address"]!)".isEmpty
            {
                cell.addressView.isHidden = true
            }
            else
            {
                cell.addressView.isHidden = false
            }


            cell.buyNowPrice.sizeToFit()
            //            cell.buyNowPrice.frame.origin.x = cell.cellView.frame.size.width - cell.buyNowPrice.frame.size.width - (10/320)*self.FullWidth

            cell.reservePrice.sizeToFit()
            //            cell.reservePrice.frame.origin.x = cell.buyNowPrice.frame.origin.x - (10/320)*self.FullWidth - cell.reservePrice.frame.size.width

            ///////////

            //            cell.cellView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)

            ////
            cell.cellView.layer.cornerRadius = (5/320)*self.FullWidth
            cell.watchListOutlet.layer.cornerRadius = (5/320)*self.FullWidth

            //setfont
            cell.dailyDealsLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Daily Deals", comment: "")
            cell.refundTypeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "not_refundable", comment: "")
            cell.watchListOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Watchlist", comment: ""), for: .normal)
            cell.removeFromWatchlistOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "watchListed", comment: ""), for: .normal)
            cell.buyNowLabel.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "buy_now", comment: "")
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {


        if tableView == categoryTableView
        {
            return UITableView.automaticDimension
        }
        else
        {
            return UITableView.automaticDimension
        }

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == categoryDropDownTableView // header TableView
        {
            let tempDictionary = allTypesArray[indexPath.row] as!  NSDictionary

            let categoryName = UserDefaults.standard.string(forKey: "categoryName")
            headerView.headerViewTitle.text = "\(tempDictionary[categoryName ?? ""]!)"

            animateCategory(toggle: false)

            typeID = "\(tempDictionary["id"]!)"

            self.startValue = 0
            self.recordsArray.removeAllObjects()
            self.currentRecordsArray.removeAllObjects()

            self.startProductListingData()

            if allSubTypesArray.count > 0
            {
                typeLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "drop_down_for_subtype", comment: "")
            }
            else
            {
                typeLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_subtypes", comment: "")
            }

        }
        else if tableView == allTypeTableView  // secondary View Tableview
        {
            let categoryName = UserDefaults.standard.string(forKey: "categoryName")
            let tempDictionary = allSubTypesArray[indexPath.row] as!  NSDictionary
            headerView.headerViewTitle.text = "\(tempDictionary[categoryName ?? ""]!)"

            animateType(toggle: false)

            typeID = "\(tempDictionary["id"]!)"

            self.startValue = 0
            self.recordsArray.removeAllObjects()
            self.currentRecordsArray.removeAllObjects()

            self.startProductListingData()

            if allSubTypesArray.count > 0
            {
                typeLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "drop_down_for_subtype", comment: "")
            }
            else
            {
                typeLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "no_subtypes", comment: "")
            }
        }
        else if tableView == sortCategoryTableview
        {
            sortingType = "\((self.sortCategoryArray[indexPath.row] as NSDictionary)["key"]!)"

            self.startValue = 0
            self.recordsArray.removeAllObjects()
            self.currentRecordsArray.removeAllObjects()

            self.startProductListingData()

            UIView.animate(withDuration: 0.5)
            {
                self.view.sendSubviewToBack(self.sortBackgroundView)
                self.sortBackgroundView.isHidden = true
                self.sortCategoryTableview.isHidden = true
                self.view.sendSubviewToBack(self.sortCategoryTableview)
            }
        }
        else   // main page listing Tableview
        {
            //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

//            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "productDetailsVC") as! ProductDetailsViewController
//
//            let tempDictionary = currentRecordsArray[indexPath.row] as! NSDictionary
//
//            navigate.productID = "\(tempDictionary["product_id"]!)"
//
//            self.navigationController?.pushViewController(navigate, animated: true)
            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController

            let tempDictionary = currentRecordsArray[indexPath.row] as! NSDictionary

            navigate.ProductID = "\(tempDictionary["product_id"]!)"

            self.navigationController?.pushViewController(navigate, animated: true)

        }

    }



    // MARK:- // Searchbar Delegate

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        UIView.animate(withDuration: 0.5, animations: {

            self.searchViewTopConstraint.constant = self.FullHeight
            self.searchViewBottomConstraint.constant = -self.FullHeight

            self.searchView.searchBar.resignFirstResponder()

        }, completion: nil)

    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        searchString = searchText.trimmingCharacters(in: .whitespaces)

    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        print("Search With ----- ", searchString!)

        UIView.animate(withDuration: 0.5, animations: {

            if self.searchString.isEmpty
            {
                self.currentRecordsArray = self.recordsArray
                self.categoryTableView.reloadData()


                self.searchViewTopConstraint.constant = self.FullHeight
                self.searchViewBottomConstraint.constant = -self.FullHeight


                self.searchView.searchBar.resignFirstResponder()
            }
            else
            {
                // Put your key in predicate that is "Name"
                let searchPredicate = NSPredicate(format: "product_name CONTAINS[C] %@", self.searchString)
                self.currentRecordsArray = NSMutableArray(array: (self.recordsArray).filtered(using: searchPredicate))

                //print (\(self.currentRecordsArray)")

                self.categoryTableView.reloadData()


                self.searchViewTopConstraint.constant = self.FullHeight
                self.searchViewBottomConstraint.constant = -self.FullHeight


                self.searchView.searchBar.resignFirstResponder()
            }

        }, completion: nil)

    }




    // MARK:- // Defined Functions


    // MARK:- // Header Drop Down Click



    @objc func headerDropDownClick() {

        closeTableViewsOutlet.isUserInteractionEnabled = true

        if categoryView.isHidden{
            animateCategory(toggle: true)

        }
        else {
            animateCategory(toggle: false)

        }

    }


    // MARK:- // Add To Watchlist


    @objc func addToWatchList(sender: UIButton)
    {

        let tempDictionary = allProductListingArray[sender.tag] as! NSDictionary

        watchListProductID = "\(tempDictionary["product_id"]!)"

        addToWatchList()

    }

    // MARK:- // Add To Watchlist


    @objc func removeFromWatchList(sender: UIButton)
    {

        let tempDictionary = allProductListingArray[sender.tag] as! NSDictionary

        watchListProductID = "\(tempDictionary["product_id"]!)"

        removeFromWatchList()

    }


    // MARK: - Hide sortBackgroundView when tapping outside
    @objc func tapGestureHandler() {
        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.sortBackgroundView)
            self.sortBackgroundView.isHidden = true

            if self.sortCategoryTableview.isHidden == false
            {
                self.sortCategoryTableview.isHidden = true
                self.view.sendSubviewToBack(self.sortCategoryTableview)
            }
        }
    }



    // MARK: - // Animate Category DropDown Tableview


    func animateCategory(toggle: Bool) {
        if toggle {

            UIView.animate(withDuration: 0.3){
                self.categoryView.isHidden = false
                //                self.categoryView.frame.origin.y = self.headerView.dropDownButtonImage.frame.origin.y
            }
        }
        else {

            UIView.animate(withDuration: 0.3){
                self.categoryView.isHidden = true
                //                self.categoryView.frame.origin.y = -(self.categoryView.frame.size.height-5)
            }
        }

    }



    // MARK: - // Animate Type DropDown Tableview


    func animateType(toggle: Bool) {
        if toggle {

            UIView.animate(withDuration: 0.3){
                self.allTypeView.isHidden = false
                //                self.allTypeView.frame.origin.y = self.secondaryView.frame.origin.y
            }
        }
        else {

            UIView.animate(withDuration: 0.3){
                self.allTypeView.isHidden = true
                //                self.allTypeView.frame.origin.y = -(self.allTypeView.frame.size.height-5)
            }
        }

    }


    // MARK:- // Set Font


    // SET FOnt

    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        footerView.homeLabel.font = UIFont(name: footerView.homeLabel.font.fontName, size: CGFloat(Get_fontSize(size: 9)))
        footerView.messageLabel.font = UIFont(name: footerView.messageLabel.font.fontName, size: CGFloat(Get_fontSize(size: 9)))
        footerView.notificationLBL.font = UIFont(name: footerView.notificationLBL.font.fontName, size: CGFloat(Get_fontSize(size: 9)))
        footerView.addLBL.font = UIFont(name: footerView.addLBL.font.fontName, size: CGFloat(Get_fontSize(size: 9)))
        footerView.dailyDealsLabel.font = UIFont(name: footerView.dailyDealsLabel.font.fontName, size: CGFloat(Get_fontSize(size: 9)))

        typeLbl.font = UIFont(name: typeLbl.font.fontName, size: CGFloat(Get_fontSize(size: 13)))

        self.privateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "private_string", comment: "")
        self.businessLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "business", comment: "")

    }






    // MARK: - // JSON Post Method to get Starting ProductListing Data from Web

    func startProductListingData()

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }



        let url = URL(string: GLOBALAPI + "app_product_list")!   //change the url

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        print("t--",typeCommaSeparatedString!)
        print("f--",filterCommaSeparatedString!)
        print("c--",conditionCommaSeparatedString!)

        parameters = "user_id=\(userID!)&lang_id=\(langID!)&cat_id=\(typeID!)&sorting_type=\(sortingType!)&user_acc_type_search=\(userAccountTypeCommaSeparatedString!)&type=\(typeCommaSeparatedString!)&filter=\(filterCommaSeparatedString!)&condition=\(conditionCommaSeparatedString!)"

        print("Product Listing URL is : ",url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let dict = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("Start ProductListing Data Response : " , dict)

                    self.allProductListingDictionary = dict["product_info"] as? NSDictionary

                    self.allProductListingArray = self.allProductListingDictionary["product_listing"] as? NSArray


                    for i in 0..<self.allProductListingArray.count

                    {

                        let tempDict = self.allProductListingArray[i] as! NSDictionary

                        self.recordsArray.add(tempDict as! NSMutableDictionary)


                    }

                    self.currentRecordsArray = self.recordsArray.mutableCopy() as? NSMutableArray

                    self.nextStart = "\(dict["next_start"]!)"

                    self.startValue = self.startValue + self.allProductListingArray.count

                    print("Next Start Value : " , self.startValue)


                    self.allTypesArray = self.allProductListingDictionary["category_listing"] as? NSArray

                    self.allSubTypesArray = self.allProductListingDictionary["child_category_listing"] as? NSArray

                    if self.allProductListingArray.count == 0
                    {
                        DispatchQueue.main.async {
                            self.categoryTableView.isHidden = true
                            print("No Items To Show")
                            self.noListingsAvailable.isHidden = false
                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.noListingsAvailable.isHidden = true
                            self.categoryTableView.delegate = self
                            self.categoryTableView.dataSource = self
                            self.categoryTableView.reloadData()

                            self.categoryDropDownTableView.delegate = self
                            self.categoryDropDownTableView.dataSource = self
                            self.categoryDropDownTableView.reloadData()

                            self.allTypeTableView.delegate = self
                            self.allTypeTableView.dataSource = self
                            self.allTypeTableView.reloadData()

                            if self.allSubTypesArray.count > 0
                            {
                                self.typeLbl.text = "Drop Down for Subtype"
                            }
                            else
                            {
                                self.typeLbl.text = "No Subtypes"
                            }

                            SVProgressHUD.dismiss()
                        }
                    }
                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()



    }





    // MARK: - // JSON POST Method to add Product to WatchList

    func addToWatchList()

    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }

        dispatchGroup.enter()

        let url = URL(string: GLOBALAPI + "app_add_watchlist")!   //change the url

        print("addtoWatchList URL : ", url)

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "user_id=\(userID!)&add_product_id=\(watchListProductID!)"

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("add to watchlist Response: " , json)

                    self.dispatchGroup.leave()

                    DispatchQueue.main.async {

                        self.startProductListingData()
                        SVProgressHUD.dismiss()
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()

    }



    // MARK: - // JSON POST Method to remove Data from WatchList

    func removeFromWatchList()

    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }

        dispatchGroup.enter()

        let url = URL(string: GLOBALAPI + "app_remove_watchlist")!   //change the url

        print("remove from watchlist URL : ", url)

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "user_id=\(userID!)&remove_product_id=\(watchListProductID!)"

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Remove from watchlist Response: " , json)

                    self.dispatchGroup.leave()

                    DispatchQueue.main.async {

                        self.startProductListingData()
                        SVProgressHUD.dismiss()
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()



    }



    // MARK:- // RTL - Set Frames and Allignments for Arabic

    //    func RTL()
    //    {
    //        secondaryViewSeparatorView.frame.origin.x = (49/320)*self.FullWidth
    //        typeDropDownClickButtonOutlet.frame.origin.x = (60/320)*self.FullWidth
    //        filterImageview.frame.origin.x = (15/320)*self.FullWidth
    //        typeLbl.frame.origin.x = (80/320)*self.FullWidth
    //        categoryView.frame.origin.x = (90/320)*self.FullWidth
    //        allTypeView.frame.origin.x = (80/320)*self.FullWidth
    //    }



    // MARK:- // Set Static Strings

    func setStaticStrings() {
        self.openSortCategory.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "sort", comment: ""), for: .normal)
        self.insideSortSearchButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "search", comment: ""), for: .normal)
        self.insideSortAdvancedSearchButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "advancedSearch", comment: ""), for: .normal)

    }


}
