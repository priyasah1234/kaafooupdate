//
//  FilterDataSet.swift
//  Kaafoo
//
//  Created by admin on 08/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import Foundation

class filterDataSet
{
    
    var key : String?
    var value : String?
    var isClicked : Bool = false
    
    init(key: String, value: String, isclicked: Bool) {
        
        self.key = key
        self.value = value
        self.isClicked = isclicked
        
    }
}
