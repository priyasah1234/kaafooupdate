//
//  CategoryAdvancedSearchTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 07/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class CategoryAdvancedSearchTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellLBL: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellSeparatorView: UIView!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
