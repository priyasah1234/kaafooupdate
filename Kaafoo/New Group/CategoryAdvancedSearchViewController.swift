//
//  CategoryAdvancedSearchViewController.swift
//  Kaafoo
//
//  Created by admin on 07/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class CategoryAdvancedSearchViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var localHeaderView: UIView!
    
    @IBOutlet weak var outsideCategoryTableview: UITableView!
    @IBOutlet weak var insideCategoryTableview: UITableView!
    

    
    var advancedSearchKeyArray = ["Type","Filter","Condition"]
    
    var typeSubCategoryArray = [
        filterDataSet(key: "P", value: "Asking Price", isclicked: false),
        filterDataSet(key: "B", value: "Buy Now", isclicked: false),
        filterDataSet(key: "A", value: "Auction", isclicked: false),
        filterDataSet(key: "R", value: "Rental", isclicked: false),
        filterDataSet(key: "T", value: "Rent", isclicked: false)
    ]
    
    var filterSubCategoryArray = [
        filterDataSet(key: "S", value: "On Sale", isclicked: false),
        filterDataSet(key: "C", value: "Cash On Delivery", isclicked: false)
    ]
    
    
    var conditionSubCategoryArray = [
        filterDataSet(key: "N", value: "New", isclicked: false),
        filterDataSet(key: "U", value: "Used", isclicked: false)
    ]
    
    
    
//    var advancedSearchArray = [
//        [
//            "title" : "Type",
//            "childCategories" : [
//                [
//                    filterDataSet(key: "P", value: "Asking Price", isclicked: false)
//                ] ,
//                [
//                    filterDataSet(key: "B", value: "Buy Now", isclicked: false)
//                ] ,
//                [
//                    filterDataSet(key: "A", value: "Auction", isclicked: false)
//                ] ,
//                [
//                    filterDataSet(key: "R", value: "Rental", isclicked: false)
//                ] ,
//                [
//                    filterDataSet(key: "T", value: "Rent", isclicked: false)
//                ]
//            ]
//        ] ,
//        [
//            "title" : "Filter",
//            "childCategories" : [
//                [
//                    filterDataSet(key: "S", value: "On Sale", isclicked: false)
//                ] ,
//                [
//                    filterDataSet(key: "C", value: "Cash On Delivery", isclicked: false)
//                ]
//            ]
//        ] ,
//        [
//            "title" : "Condition",
//            "childCategories" : [
//                [
//                    filterDataSet(key: "N", value: "New", isclicked: false)
//                ] ,
//                [
//                    filterDataSet(key: "U", value: "Used", isclicked: false)
//                ]
//            ]
//        ]
//    ]
    
    var clickedOnIndex : Int!
    
    var typeArray : NSMutableArray! = NSMutableArray()
    
    var filterArray : NSMutableArray! = NSMutableArray()
    
    var conditionArray : NSMutableArray! = NSMutableArray()
    
    var typeID : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print("Filter Array-----",advancedSearchArray)

        // Do any additional setup after loading the view.
        
        self.outsideCategoryTableview.delegate = self
        self.outsideCategoryTableview.dataSource = self
        
    }

    
    
    
    // MARK:- // Buttons
    
    // MARK:- // Apply Button
    
    @IBOutlet weak var applyButtonOutlet: UIButton!
    @IBAction func tapOnApplyButton(_ sender: UIButton) {
        
        self.typeArray.removeAllObjects()
        self.filterArray.removeAllObjects()
        self.conditionArray.removeAllObjects()
        
        for i in 0..<typeSubCategoryArray.count
        {
            if typeSubCategoryArray[i].isClicked == true
            {
                typeArray.add(typeSubCategoryArray[i].key!)
            }
        }
        
        for i in 0..<filterSubCategoryArray.count
        {
            if filterSubCategoryArray[i].isClicked == true
            {
                filterArray.add(filterSubCategoryArray[i].key!)
            }
        }
        
        for i in 0..<conditionSubCategoryArray.count
        {
            if conditionSubCategoryArray[i].isClicked == true
            {
                conditionArray.add(conditionSubCategoryArray[i].key!)
            }
        }
        
//        print("Type Array----",typeArray)
//        print("Filter Array----",filterArray)
//        print("Condition Array----",conditionArray)
//        
        if self.insideCategoryTableview.isHidden == false
        {

            UIView.animate(withDuration: 0.5, animations: {
                
                self.insideCategoryTableview.isHidden = true
                
                self.outsideCategoryTableview.isHidden = false
                self.view.bringSubviewToFront(self.outsideCategoryTableview)
                
                self.applyButtonOutlet.setTitle("DONE", for: .normal)
                
            })
            
        }
        else
        {
            
//            let storyboardName = UserDefaults.standard.string(forKey: "storyboard")

            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "categoryVC") as! CategoryViewController
            
            if typeArray.count > 0
            {
                navigate.typeCommaSeparatedString = typeArray.componentsJoined(by: ",")
            }
            
            if filterArray.count > 0
            {
                navigate.filterCommaSeparatedString = filterArray.componentsJoined(by: ",")
            }
            
            if conditionArray.count > 0
            {
                navigate.conditionCommaSeparatedString = conditionArray.componentsJoined(by: ",")
            }
            
            navigate.typeID = typeID
            
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
        }
        
    }
    
    
    
    // MARK:- // Back Button
    
    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBAction func tapOnBackButton(_ sender: UIButton) {
        
        if self.insideCategoryTableview.isHidden == true
        {
            
            self.navigationController?.popViewController(animated: true)
            
        }
        else
        {
            UIView.animate(withDuration: 0.3) {
                
                self.view.sendSubviewToBack(self.outsideCategoryTableview)
                self.insideCategoryTableview.isHidden = true
                
                self.outsideCategoryTableview.isHidden = false
                self.view.bringSubviewToFront(self.outsideCategoryTableview)
            }
            
        }
        
    }
    
    
    
    // MARK:- // Reset Button
    
    @IBOutlet weak var resetButtonOutlet: UIButton!
    @IBAction func tapOnResetButton(_ sender: UIButton) {
        
        if insideCategoryTableview.isHidden == true
        {
            for i in 0..<typeSubCategoryArray.count
            {
                typeSubCategoryArray[i].isClicked = false
            }
            
            for i in 0..<filterSubCategoryArray.count
            {
                filterSubCategoryArray[i].isClicked = false
            }
            
            for i in 0..<conditionSubCategoryArray.count
            {
                conditionSubCategoryArray[i].isClicked = false
            }
            
            self.typeArray.removeAllObjects()
            self.filterArray.removeAllObjects()
            self.conditionArray.removeAllObjects()
        }
        else
        {
            if clickedOnIndex == 0
            {
                for i in 0..<typeSubCategoryArray.count
                {
                    typeSubCategoryArray[i].isClicked = false
                }
            }
            else if clickedOnIndex == 1
            {
                for i in 0..<filterSubCategoryArray.count
                {
                    filterSubCategoryArray[i].isClicked = false
                }
            }
            else
            {
                for i in 0..<conditionSubCategoryArray.count
                {
                    conditionSubCategoryArray[i].isClicked = false
                }
            }
            
            self.insideCategoryTableview.reloadData()
        }
        
    }
    
    
    
    
    
    
    
    
    // MARK:- // Delegate Methods
    
    // Mark:- // Tableview Deleagates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.outsideCategoryTableview
        {
            return advancedSearchKeyArray.count
        }
        else
        {
            if clickedOnIndex == 0
            {
                return typeSubCategoryArray.count
            }
            else if clickedOnIndex == 1
            {
                return filterSubCategoryArray.count
            }
            else
            {
                return conditionSubCategoryArray.count
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.outsideCategoryTableview
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! CategoryAdvancedSearchTVCTableViewCell
            
            cell.cellLBL.text = "\(advancedSearchKeyArray[indexPath.row])"
            
            // Set Font //
            
            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            return cell
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! FilterCategoriesTVCTableViewCell
            
            if clickedOnIndex == 0
            {
                cell.cellLBL.text = typeSubCategoryArray[indexPath.row].value
                
                if typeSubCategoryArray[indexPath.row].isClicked == true
                {
                    cell.cellImage.image = UIImage(named: "checkBoxFilled")
                }
                else
                {
                    cell.cellImage.image = UIImage(named: "checkBoxEmpty")
                }
            }
            else if clickedOnIndex == 1
            {
                cell.cellLBL.text = filterSubCategoryArray[indexPath.row].value
                
                if filterSubCategoryArray[indexPath.row].isClicked == true
                {
                    cell.cellImage.image = UIImage(named: "checkBoxFilled")
                }
                else
                {
                    cell.cellImage.image = UIImage(named: "checkBoxEmpty")
                }
            }
            else
            {
                cell.cellLBL.text = conditionSubCategoryArray[indexPath.row].value
                
                if conditionSubCategoryArray[indexPath.row].isClicked == true
                {
                    cell.cellImage.image = UIImage(named: "checkBoxFilled")
                }
                else
                {
                    cell.cellImage.image = UIImage(named: "checkBoxEmpty")
                }
            }
            
            // Set Font //
            
            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.outsideCategoryTableview
        {
            clickedOnIndex = indexPath.row
            
            UIView.animate(withDuration: 0.5, animations: {
                
                self.outsideCategoryTableview.isHidden = true
                
                self.insideCategoryTableview.reloadData()
                
                self.insideCategoryTableview.isHidden = false
                
                self.view.bringSubviewToFront(self.insideCategoryTableview)
                
                self.insideCategoryTableview.delegate = self
                self.insideCategoryTableview.dataSource = self
                
                self.applyButtonOutlet.setTitle("APPLY", for: .normal)
                
            })
        }
        else
        {
            
            if clickedOnIndex == 0
            {

                typeSubCategoryArray[indexPath.row].isClicked = !typeSubCategoryArray[indexPath.row].isClicked
                self.insideCategoryTableview.reloadData()
                
            }
            else if clickedOnIndex == 1
            {
                
                filterSubCategoryArray[indexPath.row].isClicked = !filterSubCategoryArray[indexPath.row].isClicked
                self.insideCategoryTableview.reloadData()
                
            }
            else
            {
                
                conditionSubCategoryArray[indexPath.row].isClicked = !conditionSubCategoryArray[indexPath.row].isClicked
                self.insideCategoryTableview.reloadData()
                
            }
        }
        
    }
    
    

}
