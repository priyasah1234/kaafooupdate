//
//  CompanyProfileViewController.swift
//  Kaafoo
//
//  Created by admin on 23/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class CompanyProfileViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    

    
    @IBOutlet weak var companyProfileTableview: UITableView!
    
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    let dispatchGroup = DispatchGroup()
    
    var companyProfileArray : NSArray!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        getCompanyProfile()
    }
    
    
    
    // MARK:- // Delegate FUnctions
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return companyProfileArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! CompanyProfileTVCTableViewCell
        
        cell.cellTitle.text = "\((companyProfileArray[indexPath.row] as! NSDictionary)["title"]!)"
        cell.cellDescription.text = "\((companyProfileArray[indexPath.row] as! NSDictionary)["description"]!)"
        
        cell.cellTitle.font = UIFont(name: cell.cellTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.cellDescription.font = UIFont(name: cell.cellDescription.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

//        return (65/568)*self.FullHeight
        return UITableView.automaticDimension

    }

    
    
    
    // MARK: - // JSON POST Method to get Company Profile
    
    func getCompanyProfile()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_service_user_details_main")!   //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let sellerID = UserDefaults.standard.string(forKey: "sellerID")
//        let listingType = UserDefaults.standard.string(forKey: "listingType")

        parameters = "login_id=\(userID!)&lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)&seller_id=\(sellerID!)&listing_type=CP"
        
        print("Company PRofile URL is : ",url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Company Profile Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                            
                        }
                        
                    }
                    else
                    {
                        self.companyProfileArray = ((json["info_array"] as! NSDictionary)["company_profile"] as! NSArray)
                        
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.companyProfileTableview.delegate = self
                            self.companyProfileTableview.dataSource = self
                            self.companyProfileTableview.reloadData()
                            self.companyProfileTableview.rowHeight = UITableView.automaticDimension
                            self.companyProfileTableview.estimatedRowHeight = UITableView.automaticDimension
                            SVProgressHUD.dismiss()
                        }
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }

}
