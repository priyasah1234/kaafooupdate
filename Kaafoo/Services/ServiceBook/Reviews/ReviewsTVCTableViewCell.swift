//
//  ReviewsTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 26/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class ReviewsTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var reviewDate: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var totalRating: UILabel!
    @IBOutlet weak var totalRatingStar: UIImageView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingNo: UILabel!
    @IBOutlet weak var ratingNoStar: UIImageView!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
