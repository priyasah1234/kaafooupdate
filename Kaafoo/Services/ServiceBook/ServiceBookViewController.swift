//
//  ServiceBookViewController.swift
//  Kaafoo
//
//  Created by admin on 22/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class ServiceBookViewController: GlobalViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var bannerAndProfileImageView: UIView!
    
    @IBOutlet weak var bannerScroll: UIScrollView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    
    @IBOutlet weak var qrCodeImageview: UIImageView!
    
    @IBOutlet weak var bookingScrollview: UIScrollView!
    
    var serviceBookingProfileDictionary : NSDictionary!
    
    var dispatchGroup = DispatchGroup()
    
    var sellerID : String!
    
    var tempFrame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    var listingTypeArray : NSArray! = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "company_profile", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookATime", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "reviews", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "contact", comment: "")]
    
    var localCategoryArray : NSMutableArray! = [["id" : "" , "name" : "All"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getServiceBookingProfile()
        
        set_font()
        
        dispatchGroup.notify(queue: .main) {
            
            self.setBannerAndProfileImageview()
            
            self.setQRCode()
            
            self.setBookingScroll()
            
        }
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        
    }

    
    
    
    // MARK: - // JSON POST Method to get Watchlist Data
    
    func getServiceBookingProfile()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_service_user_details_main")!   //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "login_id=\(userID!)&lang_id=\(langID!)&seller_id=\(sellerID!)"
        
        print("Service Booking Profile URL is : ",url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
      
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Service Booking Profile Response: " , json)
                    
                    self.serviceBookingProfileDictionary = json
                    
                    self.localCategoryArray.removeAllObjects()
                    self.localCategoryArray.add(["id" : "","name" : "All"])
                    for i in 0..<(((json["info_array"] as! NSDictionary)["category_list"] as! NSArray).count)
                    {
                        self.localCategoryArray.add(((json["info_array"] as! NSDictionary)["category_list"] as! NSArray)[i] as! NSDictionary)
                    }

                    DispatchQueue.main.async {
                        
                        self.dispatchGroup.leave()

                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    // MARK:- // Set Image Scroll
    
    func setBannerAndProfileImageview()
    {
        
        self.profileName.text = "\((((self.serviceBookingProfileDictionary["info_array"] as! NSDictionary)["seller_info"] as! NSDictionary)["businessname"]!))"
        
        self.profileImage.sd_setImage(with: URL(string: "\((((self.serviceBookingProfileDictionary["info_array"] as! NSDictionary)["seller_info"] as! NSDictionary)["logo"]!))"))
        
        let tempArray = (((self.serviceBookingProfileDictionary["info_array"] as! NSDictionary)["seller_info"] as! NSDictionary)["slider_info"] as! NSArray)
        
        //self.imagePageControl.numberOfPages = tempArray.count
        
        for i in 0..<tempArray.count
        {
            self.tempFrame.origin.x = self.bannerScroll.frame.size.width * CGFloat(i)
            self.tempFrame.size = self.bannerScroll.frame.size
            
            let imageview = UIImageView(frame: self.tempFrame)
            imageview.sd_setImage(with: URL(string: "\((tempArray[i] as! NSDictionary)["image"]!)"))
            
            imageview.contentMode = .scaleAspectFit
            imageview.clipsToBounds = true
            
            self.bannerScroll.addSubview(imageview)
        }
        self.bannerScroll.contentSize = CGSize(width: (self.bannerScroll.frame.size.width * CGFloat(tempArray.count)), height: self.bannerScroll.frame.size.height)
        
        self.bannerScroll.delegate = self
        
    }
    
    // MARK:- // Set QR Code
    
    func setQRCode()
    {
        self.qrCodeImageview.sd_setImage(with: URL(string: "\((((self.serviceBookingProfileDictionary["info_array"] as! NSDictionary)["seller_info"] as! NSDictionary)["qrcode_img"]!))"))
    }
    
    
    // MARK:- // Set Booking Scroll
    
    func setBookingScroll()
    {
        
        var tempOrigin : CGFloat! = 0
        
        for i in 0..<listingTypeArray.count
        {
            let listingTypeView = UIView(frame: CGRect(x: tempOrigin, y: 0, width: (100/320)*self.FullWidth, height: (30/568)*self.FullHeight))
            
            // create button
            let listButton = UIButton(frame: CGRect(x: 0, y: 0, width: (100/320)*self.FullWidth, height: (30/568)*self.FullHeight))
            
            listButton.setTitle("\(listingTypeArray[i])", for: .normal)
            
            listButton.sizeToFit()
            
            listButton.frame.origin.x = (10/320)*self.FullWidth
            
            if i == (listingTypeArray.count - 1)
            {
                listingTypeView.frame = CGRect(x: tempOrigin, y: 0, width: (20/320)*self.FullWidth + listButton.frame.size.width, height: (30/568)*self.FullHeight)
            }
            else
            {
                listingTypeView.frame = CGRect(x: tempOrigin, y: 0, width: (10/320)*self.FullWidth + listButton.frame.size.width, height: (30/568)*self.FullHeight)
            }
            
            listButton.addTarget(self, action: #selector(listButtonAction), for: .touchUpInside)
            listButton.tag = i
            
            listButton.setTitleColor(UIColor.black, for: .normal)
            
            listButton.titleLabel?.font = UIFont(name: (headerView.headerViewTitle.font.fontName), size: CGFloat(Get_fontSize(size: 14)))!
            
            listingTypeView.addSubview(listButton)
            bookingScrollview.addSubview(listingTypeView)
            
            tempOrigin = tempOrigin + listingTypeView.frame.size.width
        }
        
        self.bookingScrollview.contentSize = CGSize(width: tempOrigin, height: (30/568)*self.FullHeight)
        
    }
    
    // MARK:- // Function declaring What the Buttons will do
    
    @objc func listButtonAction(sender: UIButton!){
        
//        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
        
//        let navigate = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "serviceBookDetailsVC") as! ServiceBookDetailsViewController
        
        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookDetailsVC") as! ServiceBookDetailsViewController

        
        navigate.localCategoryArray = self.localCategoryArray.mutableCopy() as? NSMutableArray
        
        navigate.buttonTag = sender.tag
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
//    // MARK:- // Set Listing Type
//
//    public func setListingType(tag : NSInteger)
//    {
//        if tag == 0
//        {
//            UserDefaults.standard.set("CP", forKey: "listingType")
//        }
//        else if tag == 1
//        {
//            UserDefaults.standard.set("P", forKey: "listingType")
//        }
//        else if tag == 2
//        {
//            UserDefaults.standard.set("R", forKey: "listingType")
//        }
//        else
//        {
//            UserDefaults.standard.set("C", forKey: "listingType")
//        }
//    }
    
    
    
    // MARK:- // Set FOnt
    
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
    }

}
