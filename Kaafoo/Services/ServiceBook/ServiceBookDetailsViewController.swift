//
//  ServiceBookDetailsViewController.swift
//  Kaafoo
//
//  Created by admin on 22/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class ServiceBookDetailsViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var SelectCategoryTblViewHeight: NSLayoutConstraint!
    @IBOutlet weak var selectCategoryAndSortMainView: UIView!
    @IBOutlet weak var selectCategoryAndSortView: UIView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var ButtonsScrollView: UIView!
    
    
    @IBOutlet weak var selectCategoryView: UIView!
    @IBOutlet weak var selectCategoryTableview: UITableView!
    
    @IBOutlet weak var bookingScrollview: UIScrollView!
    
    @IBOutlet weak var containerView: UIView!
    
    var localCategoryArray : NSMutableArray! = NSMutableArray()
    
    var listingTypeArray : NSArray! = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "company_profile", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookATime", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "reviews", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "contact", comment: "")]
    
    var buttonTag : NSInteger!
    
    var buttonsArray = [UIButton]()
    
    var catID : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.selectCategoryTableview.delegate = self
        self.selectCategoryTableview.dataSource = self
        
        selectCategoryView.layer.borderWidth = 2
        selectCategoryView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        UserDefaults.standard.set(catID, forKey: "catID")
        
        setBookingScroll()
        
        setVC(tag: buttonTag)
        
        set_Color(tag: buttonTag)
        


    }

    //MARK:- //ViewDid Appear
    override func viewDidAppear(_ animated: Bool) {

         setFrames(tag: buttonTag)
    }
    
    // MARK;- // Buttons
    
    
    // MARK:- // Tap Category Dropdown
    
    @IBOutlet weak var categoryDropDownButtonOutlet: UIButton!
    @IBAction func tapCategoryDropDown(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.1)
        {
            if self.selectCategoryView.isHidden{
                self.animateCategory(toggle: true)
                
            }
            else {
                self.animateCategory(toggle: false)
                
            }
        }
        
    }
    
    
    // MARK:- // Function declaring What the Buttons in BookingScroll will do
    
    @objc func listButtonAction(sender: UIButton!){
        
        setVC(tag: sender.tag)
        
        set_Color(tag: sender.tag)
        
        setFrames(tag: sender.tag)
        
        buttonTag = sender.tag
        
    }
    
    
    // MARK: - // Set ChildViewController
    
    func setVC(tag : NSInteger)
    {
        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
        
        if tag == 0
        {
            let companyProfile = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "companyProfileVC") as! CompanyProfileViewController
            
            self.addChild(companyProfile)
            
            self.containerView.addSubview(companyProfile.view)
            
            companyProfile.view.anchor(top: self.containerView.topAnchor, leading: self.containerView.leadingAnchor, bottom: self.containerView.bottomAnchor, trailing: self.containerView.trailingAnchor)
            
            companyProfile.didMove(toParent: self)
        }
        else if tag == 1
        {
            let bookATime = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "bookATimeVC") as! BookATimeViewController
            
            self.addChild(bookATime)
            
            self.containerView.addSubview(bookATime.view)
            
            bookATime.view.anchor(top: self.containerView.topAnchor, leading: self.containerView.leadingAnchor, bottom: self.containerView.bottomAnchor, trailing: self.containerView.trailingAnchor)
            
            bookATime.didMove(toParent: self)
        }
        else if tag == 2
        {
            let reviews = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "reviewsVC") as! ReviewsViewController
            
            self.addChild(reviews)
            
            self.containerView.addSubview(reviews.view)
            
            reviews.view.anchor(top: self.containerView.topAnchor, leading: self.containerView.leadingAnchor, bottom: self.containerView.bottomAnchor, trailing: self.containerView.trailingAnchor)
            
            reviews.didMove(toParent: self)
        }
        else
        {
            let contact = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "contactVC") as! ContactViewController
            
            self.addChild(contact)
            
            self.containerView.addSubview(contact.view)
            
            contact.view.anchor(top: self.containerView.topAnchor, leading: self.containerView.leadingAnchor, bottom: self.containerView.bottomAnchor, trailing: self.containerView.trailingAnchor)
            
            contact.didMove(toParent: self)
        }
    }
    
    // MARK:- // Set Button Color
    
    func set_Color(tag : NSInteger)
    {
        for i in 0..<buttonsArray.count
        {
            buttonsArray[i].setTitleColor(UIColor.black, for: .normal)
        }
        
        buttonsArray[tag].setTitleColor(UIColor.green, for: .normal)
    }
    
    
    // MARK:- // Set Screen Frames
    
    func setFrames(tag : NSInteger)
    {
        UIView.animate(withDuration: 0.1, animations: {
            
            if tag == 1 // Book a time
            {
                self.selectCategoryAndSortMainView.isHidden = false
                
//                self.bookingScrollview.frame.origin.y = self.selectCategoryAndSortMainView.frame.origin.y + self.selectCategoryAndSortMainView.frame.size.height
//
//                self.containerView.autoresizesSubviews = false
//
//                self.containerView.frame = CGRect(x: 0, y: self.bookingScrollview.frame.origin.y + self.bookingScrollview.frame.size.height + (15/568)*self.FullHeight, width: self.FullWidth, height: (398/568)*self.FullHeight)
//
//                self.containerView.autoresizesSubviews = true
            }
            else
            {
                self.selectCategoryAndSortMainView.isHidden = true
                
//                self.bookingScrollview.frame.origin.y = (70/568)*self.FullHeight
//
//                self.containerView.autoresizesSubviews = false
//
//                self.containerView.frame = CGRect(x: 0, y: self.bookingScrollview.frame.origin.y + self.bookingScrollview.frame.size.height + (15/568)*self.FullHeight, width: self.FullWidth, height: ((398/568)*self.FullHeight) + self.selectCategoryAndSortMainView.frame.size.height)
//
//                self.containerView.autoresizesSubviews = true
            }
            
        }, completion: nil)
        
    }
    
    
    // MARK:- // Set Booking Scroll
    
    func setBookingScroll()
    {
        
        var tempOrigin : CGFloat! = 0
        
        for i in 0..<listingTypeArray.count
        {
            let listingTypeView = UIView(frame: CGRect(x: tempOrigin, y: 0, width: (100/320)*self.FullWidth, height: (30/568)*self.FullHeight))
            
            // create button
            let listButton = UIButton(frame: CGRect(x: 0, y: 0, width: (100/320)*self.FullWidth, height: (30/568)*self.FullHeight))
            
            listButton.setTitle("\(listingTypeArray[i])", for: .normal)
            
            listButton.sizeToFit()
            
            listButton.frame.origin.x = (10/320)*self.FullWidth
            
            if i == (listingTypeArray.count - 1)
            {
                listingTypeView.frame = CGRect(x: tempOrigin, y: 0, width: (20/320)*self.FullWidth + listButton.frame.size.width, height: (30/568)*self.FullHeight)
            }
            else
            {
                listingTypeView.frame = CGRect(x: tempOrigin, y: 0, width: (10/320)*self.FullWidth + listButton.frame.size.width, height: (30/568)*self.FullHeight)
            }
            
            listButton.addTarget(self, action: #selector(listButtonAction), for: .touchUpInside)
            listButton.tag = i
            
            listButton.setTitleColor(UIColor.black, for: .normal)
            
            listButton.titleLabel?.font = UIFont(name: (headerView.headerViewTitle.font.fontName), size: CGFloat(Get_fontSize(size: 14)))!
            
            buttonsArray.append(listButton)
            
            listingTypeView.addSubview(listButton)
            bookingScrollview.addSubview(listingTypeView)
            
            tempOrigin = tempOrigin + listingTypeView.frame.size.width
        }
        
        self.bookingScrollview.contentSize = CGSize(width: tempOrigin, height: (30/568)*self.FullHeight)
//        self.bookingScrollview.transform = CGAffineTransform(scaleX:-1,y: -1);

    }
    
    
    // MARK: - // Animate Category DropDown Tableview
    
    
    func animateCategory(toggle: Bool) {
        if toggle {
            
            UIView.animate(withDuration: 0.1){
                
                self.view.bringSubviewToFront(self.selectCategoryView)
                self.selectCategoryView.autoresizesSubviews = false
                self.selectCategoryView.isHidden = false
                let tblHeight = self.selectCategoryTableview.visibleCells[0].bounds.height
                self.SelectCategoryTblViewHeight.constant = CGFloat(self.localCategoryArray.count) * tblHeight
                
                self.selectCategoryView.frame.size.height = ((100/568)*self.FullHeight)
                self.selectCategoryTableview.frame.size.height = ((100/568)*self.FullHeight)
                self.selectCategoryView.autoresizesSubviews = true
            }
        }
        else {
            
            UIView.animate(withDuration: 0.1){
                self.selectCategoryView.autoresizesSubviews = false
                self.selectCategoryView.isHidden = true
                self.SelectCategoryTblViewHeight.constant = 0
                self.selectCategoryView.frame.size.height = ((1/568)*self.FullHeight)
                self.selectCategoryTableview.frame.size.height = ((1/568)*self.FullHeight)
                self.selectCategoryView.autoresizesSubviews = true
                self.view.sendSubviewToBack(self.selectCategoryView)
            }
        }
        
    }
    
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return localCategoryArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell")
            
        
            cell?.textLabel?.text = "\((localCategoryArray[indexPath.row] as! NSDictionary)["name"]!)"
            
            cell?.textLabel?.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        
        //
        cell?.selectionStyle = UITableViewCell.SelectionStyle.none
            
            return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        catID = "\((localCategoryArray[indexPath.row] as! NSDictionary)["id"]!)"
        UserDefaults.standard.set(catID, forKey: "catID")
        
        categoryName.text = "\((localCategoryArray[indexPath.row] as! NSDictionary)["name"]!)"
        
        animateCategory(toggle: false)
        
        setVC(tag: buttonTag)
       
    }
    

}
