//
//  ContactTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 26/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class ContactTVCTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var dayName: UILabel!
    @IBOutlet weak var serviceTime: UILabel!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
