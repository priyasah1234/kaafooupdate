//
//  ContactViewController.swift
//  Kaafoo
//
//  Created by admin on 26/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SVProgressHUD
import GoogleMaps
import GooglePlaces

class ContactViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var locationMapView: GMSMapView!
    @IBOutlet weak var serviceAvailableTimeView: UIView!
    @IBOutlet weak var serviceAvailableTimeLBL: UILabel!
    @IBOutlet weak var serviceAvailableTimeTableview: UITableView!
    
    @IBOutlet weak var noServiceTimeLBL: UILabel!
    
    
    let dispatchGroup = DispatchGroup()
    
    var contactDicitionary : NSDictionary!
    
    var addressLat : Double!
    
    var addressLong : Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getContact()
        
        dispatchGroup.notify(queue: .main) {
            
            //            self.showLocation()
            
            self.serviceTimeAvailability()
            self.setFont()
            
        }
    }
    
    
    // MARK:- // Service Time Availability
    
    func serviceTimeAvailability()
    {
        if (contactDicitionary["service_available_time"] as! NSArray).count == 0
        {
            //            let noServiceAvailableLBL = UILabel(frame: CGRect(x: 0, y: 0, width: self.FullWidth, height: (30/568)*self.FullHeight))
            //
            //            noServiceAvailableLBL.center = serviceAvailableTimeView.center
            //
            //            noServiceAvailableLBL.text = "No service time available"
            //
            //            noServiceAvailableLBL.font = UIFont(name: serviceAvailableTimeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            //
            //            serviceAvailableTimeView.addSubview(noServiceAvailableLBL)
            //
            //            noServiceAvailableLBL.textColor = UIColor.black
            //
            //            noServiceAvailableLBL.backgroundColor = UIColor.red
            
            noServiceTimeLBL.isHidden = false
            
            noServiceTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "noServiceTimeAvailable", comment: "")
            
            noServiceTimeLBL.font = UIFont(name: serviceAvailableTimeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
            
            serviceAvailableTimeView.bringSubviewToFront(noServiceTimeLBL)
            
        }
    }
    
    
    // MARK:- // Functions to show location
    
    
    func showLocation()
    {
        
        for i in 0..<(self.contactDicitionary["map_details"] as! NSArray).count
        {
            addressLat = Double("\(((self.contactDicitionary["map_details"] as! NSArray)[i] as! NSDictionary)["lat"]!)")
            addressLong = Double("\(((self.contactDicitionary["map_details"] as! NSArray)[i] as! NSDictionary)["long"]!)")
            
            let marker = GMSMarker()
            
            let camera = GMSCameraPosition.camera(withLatitude: addressLat, longitude: addressLong, zoom: locationMapView.minZoom)
            
            marker.position = CLLocationCoordinate2D(latitude: addressLat, longitude: addressLong)
            marker.title = ""
            marker.snippet = "\(((contactDicitionary["map_details"] as! NSArray)[i] as! NSDictionary)["address"]!)"
            marker.map = self.locationMapView
            
            self.locationMapView.animate(to: camera)
        }
        
        
    }
    
    
    
    // MARK:- // Delegate Functions
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (contactDicitionary["service_available_time"] as! NSArray).count == 0
        {
            return 0
        }
        else
        {
            return (contactDicitionary["service_available_time"] as! NSArray).count
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! ContactTVCTableViewCell
        
        cell.dayName.text = "\(((contactDicitionary["service_available_time"] as! NSArray)[indexPath.row] as! NSDictionary)["day"]!)"
        cell.serviceTime.text = "\(((contactDicitionary["service_available_time"] as! NSArray)[indexPath.row] as! NSDictionary)["time"]!)"
        
        // Set Font //////
        cell.dayName.font = UIFont(name: cell.dayName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.serviceTime.font = UIFont(name: cell.serviceTime.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    
    
    
    // MARK: - // JSON POST Method to get Contact Information
    
    func getContact()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_service_user_details_main")!   //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let sellerID = UserDefaults.standard.string(forKey: "sellerID")
        
        parameters = "login_id=\(userID!)&lang_id=\(langID!)&seller_id=\(sellerID!)&listing_type=C"
        
        print("Contact URL is : ",url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Reviews Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                            
                        }
                        
                    }
                    else
                    {
                        self.contactDicitionary = ((json["info_array"] as! NSDictionary)["contacts"] as! NSDictionary)
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.serviceAvailableTimeTableview.delegate = self
                            self.serviceAvailableTimeTableview.dataSource = self
                            self.serviceAvailableTimeTableview.reloadData()
                            self.serviceAvailableTimeTableview.estimatedRowHeight = UITableView.automaticDimension
                            self.serviceAvailableTimeTableview.rowHeight = UITableView.automaticDimension
                            
                            SVProgressHUD.dismiss()
                        }
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
    }

    func setFont()
    {
        self.serviceAvailableTimeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "servive_available_time", comment: "")
    }
    
    
    
}
