//
//  BookingTimesTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 28/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class BookingTimesTVCTableViewCell: UITableViewCell {

    
    @IBOutlet weak var timeLBL: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var checkImage: UIImageView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
