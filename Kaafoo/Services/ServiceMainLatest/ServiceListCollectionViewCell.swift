//
//  ServiceListCollectionViewCell.swift
//  Kaafoo
//
//  Created by IOS-1 on 07/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos

class ServiceListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var WatchlistBtn: UIButton!
    @IBOutlet weak var ServiceProviderImage: UIImageView!
    @IBOutlet weak var ServiceProviderName: UILabel!
   
    @IBOutlet weak var VerifiedImage: UIImageView!
    @IBOutlet weak var ReviewView: CosmosView!
    @IBOutlet weak var ReviewsNo: UILabel!
    @IBOutlet weak var ServiceProviderAddress: UILabel!
    @IBOutlet weak var VerifiedAccount: UILabel!
    
    
}
