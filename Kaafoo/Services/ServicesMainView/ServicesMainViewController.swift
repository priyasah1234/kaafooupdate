//
//  ServicesMainViewController.swift
//  Kaafoo
//
//  Created by admin on 19/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class ServicesMainViewController: GlobalViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    //MARK:- //Picker View Outlets
    @IBOutlet weak var ShowPicker: UIView!
    @IBOutlet weak var DataPicker: UIPickerView!

    @IBOutlet weak var CategoryName: UILabel!
    @IBOutlet weak var tapOnCategoryBtnOutlet: UIButton!
    @IBAction func tapOnCategoryBtn(_ sender: UIButton) {
    }
    @IBAction func SortBtn(_ sender: UIButton) {
    }
    @IBOutlet weak var SortBtnOutlet: UIButton!
    
    
    @IBOutlet weak var SelectCategoryTblViewHeight: NSLayoutConstraint!
    @IBOutlet weak var selectCategoryAndSortMainView: UIView!
    @IBOutlet weak var selectCategoryAndSortView: UIView!
    @IBOutlet weak var selectCategoryView: UIView!
    @IBOutlet weak var selectCategoryTableview: UITableView!


    @IBOutlet weak var selectCategorySeparatorView: UIView!

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var imageScrollView: UIView!
    @IBOutlet weak var imageScroll: UIScrollView!
    @IBOutlet weak var imagePageControl: UIPageControl!

    @IBOutlet weak var servicesTableview: UITableView!

    @IBOutlet weak var sortBackgroundView: UIView!
    @IBOutlet weak var sortTransparentView: UIView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var upImage: UIImageView!
    @IBOutlet weak var sortContainerView: UIView!
    @IBOutlet weak var sortLBL: UILabel!

    var typeID : String!
    var typeName : String!

    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var recordsArray : NSMutableArray! = NSMutableArray()

    let dispatchGroup = DispatchGroup()

    var servicesMainDictionary : NSDictionary!

    var tempFrame = CGRect(x: 0, y: 0, width: 0, height: 0)

    var categoryNameToShow : String!

    var businessNameHeight : CGFloat!

    var businessAddressHeight : CGFloat!

    var viewHeight : CGFloat!

    var tableviewHeight: CGFloat = 0

    var localCategoryArray : NSMutableArray! = [["id" : " " , "name" : "All"]]

    var tapGesture = UITapGestureRecognizer()

    var localSortCategoryArray = [["key" : "" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "all", comment: "")] , ["key" : "T" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "title", comment: "")] , ["key" : "N" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "newest_listed", comment: "")] , ["key" : "C" , "value" : LocalizationSystem.sharedInstance.localizedStringForKey(key: "closing_soon", comment: "")]]

    var sortType : String! = ""

    var searchString : String!

    var currentRecordsArray : NSMutableArray = NSMutableArray()

    var sellerID : String! = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        searchView.searchBar.delegate = self


        set_font()

        self.SetFont()

        self.getServicesData()

        dispatchGroup.notify(queue: .main) {

            self.setImageScroll()

        }

        self.sortCategoryTableview.delegate = self
        
        self.sortCategoryTableview.dataSource = self

        self.sortContainerView.layer.cornerRadius = (5/320)*self.FullWidth

        selectCategoryView.layer.borderWidth = 2
        
        selectCategoryView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        sortCategoryTableview.separatorStyle = .none

    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)

        dispatchGroup.notify(queue: .main) {
            
            self.setScrollContent()
        }


    }



    // MARK:- // Buttons

    // MARK:- // Tap Category Dropdown

    @IBOutlet weak var categoryDropDownButtonOutlet: UIButton!
    
    @IBAction func tapCategoryDropDown(_ sender: UIButton) {

        if selectCategoryView.isHidden{
            
            animateCategory(toggle: true)

        }
        else {
            
            animateCategory(toggle: false)

        }
    }


    // MARK:- // Tap Sort

    @IBOutlet weak var sortOutlet: UIButton!
    
    @IBAction func tapSort(_ sender: UIButton) {

        if self.sortBackgroundView.isHidden == true
        {
            if sortView.isHidden == true
            {
                self.sortView.isHidden = false
                
                self.sortCategoryTableview.isHidden = true
            }
            UIView.animate(withDuration: 0.5)
            {
                self.view.bringSubviewToFront(self.sortBackgroundView)
                
                self.sortBackgroundView.isHidden = false

                // *** Hide sortBackgroundView when tapping outside ***
                self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
                
                self.sortTransparentView.addGestureRecognizer(self.tapGesture)

                //self.categoryDropDownButtonOutlet.isUserInteractionEnabled = false
            }
        }
        else
        {
            if sortView.isHidden == true
            {
                self.sortView.isHidden = false
                
                self.sortCategoryTableview.isHidden = true
            }
            else
            {
                UIView.animate(withDuration: 0.5)
                {
                    self.view.sendSubviewToBack(self.sortBackgroundView)
                    self.sortBackgroundView.isHidden = true

                    if self.sortCategoryTableview.isHidden == false
                    {
                        self.sortCategoryTableview.isHidden = true
                    }

                    self.categoryDropDownButtonOutlet.isUserInteractionEnabled = true
                }
            }
        }

    }



    // MARK:- // Open Sort Category

    @IBOutlet weak var openSortCategoryButtonOutlet: UIButton!
    @IBAction func openSortCategoryButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5)
        {
            self.sortCategoryTableview.isHidden = false
            self.view.bringSubviewToFront(self.sortCategoryTableview)
            self.sortView.isHidden = true
        }

    }


    @IBOutlet weak var sortCategoryTableview: UITableView!


    // MARK:- // Tableview Delegates

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == selectCategoryTableview
        {
            return localCategoryArray.count
        }
        else if tableView == sortCategoryTableview
        {
            return localSortCategoryArray.count
        }
        else
        {
            return currentRecordsArray.count
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == selectCategoryTableview
        {

            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell")

            //cell?.textLabel?.text = "\(((servicesMainDictionary["category_list"] as! NSArray)[indexPath.row] as! NSDictionary)["name"]!)"
            cell?.textLabel?.text = "\((localCategoryArray[indexPath.row] as! NSDictionary)["name"]!)"

            cell?.textLabel?.font = UIFont(name: (cell?.textLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))

            return cell!

        }
        else if tableView == sortCategoryTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! SortTVCTableViewCell

            cell.cellLBL.text = "\((localSortCategoryArray[indexPath.row] as NSDictionary)["value"]!)"

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! ServicesTVCTableViewCell


            //dynamic business Name and its height
            let businessNameString = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"

            businessNameHeight = businessNameString.heightWithConstrainedWidth(width: (200/320)*self.FullWidth, font: UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 15)))!)

            //dynamic business address and its height
            let businessAddressString = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["address"]!)"

            businessAddressHeight = businessAddressString.heightWithConstrainedWidth(width: (170/320)*self.FullWidth, font: UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 13)))!)

            //viewheight
            viewHeight = (165/568)*self.FullHeight + ((businessNameHeight + businessAddressHeight) - (40/568)*self.FullHeight)

            cell.businessImage.frame = CGRect(x: (10/320)*self.FullWidth, y: (7/568)*self.FullHeight, width: (70/320)*self.FullWidth, height: (70/568)*self.FullHeight)

            cell.businessName.frame = CGRect(x: (90/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (200/320)*self.FullWidth, height: businessNameHeight)

            let businessRatingYOrigin = (10/568)*self.FullHeight + businessNameHeight + (5/568)*self.FullHeight


            // set frame of businessRatingView after checking screen width of Iphone 5s to fit businessRatingView
            if self.FullWidth == 320
            {
                cell.businessRating.frame = CGRect(x: (90/320)*self.FullWidth, y: businessRatingYOrigin, width: (120/320)*self.FullWidth, height: (20/568)*self.FullHeight)
                cell.businessReviewsCount.frame = CGRect(x: (215/320)*self.FullWidth, y: businessRatingYOrigin, width: (75/320)*self.FullWidth, height: (20/568)*self.FullHeight)
            }
            else
            {
                cell.businessRating.frame = CGRect(x: (90/320)*self.FullWidth, y: businessRatingYOrigin, width: (100/320)*self.FullWidth, height: (20/568)*self.FullHeight)
                cell.businessReviewsCount.frame = CGRect(x: (195/320)*self.FullWidth, y: businessRatingYOrigin, width: (95/320)*self.FullWidth, height: (20/568)*self.FullHeight)
            }



            let businessAddressYOrigin = businessRatingYOrigin + (25/568)*self.FullHeight

            cell.addressPlaceholderImage.frame = CGRect(x: (95/320)*self.FullWidth, y: businessAddressYOrigin, width: (20/320)*self.FullWidth, height: (20/568)*self.FullHeight)

            cell.businessAddress.frame = CGRect(x: (120/320)*self.FullWidth, y: businessAddressYOrigin, width: (170/320)*self.FullWidth, height: (20/568)*self.FullHeight)

            let verifiedImageYOrigin = businessAddressYOrigin + businessAddressHeight + (40/568)*self.FullHeight

            cell.verifiedImage.frame = CGRect(x: (10/320)*self.FullWidth, y: verifiedImageYOrigin, width: (20/320)*self.FullWidth, height: (20/568)*self.FullHeight)

            cell.verifiedStatusLBL.frame = CGRect(x: (40/320)*self.FullWidth, y: verifiedImageYOrigin, width: (120/320)*self.FullWidth, height: (20/568)*self.FullHeight)

            cell.addToFavouriteButton.frame = CGRect(x: (160/320)*self.FullWidth, y: (verifiedImageYOrigin - (5/568)*self.FullHeight), width: (130/320)*self.FullWidth, height: (30/568)*self.FullHeight)

            cell.addedToFavourites.frame = CGRect(x: (160/320)*self.FullWidth, y: (verifiedImageYOrigin - (5/568)*self.FullHeight), width: (130/320)*self.FullWidth, height: (30/568)*self.FullHeight)

            cell.cellView.frame = CGRect(x: (10/320)*self.FullWidth, y: (7/568)*self.FullHeight, width: (300/320)*self.FullWidth, height: verifiedImageYOrigin + (40/568)*self.FullHeight)




            //Put Data
            cell.businessImage.sd_setImage(with: URL(string: "\((currentRecordsArray[indexPath.row] as! NSDictionary)["business_image"]!)"))

            cell.businessName.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"

            cell.businessRating.rating = Double("\((currentRecordsArray[indexPath.row] as! NSDictionary)["avg_rating"]!)")!

            cell.businessReviewsCount.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "review", comment: "") + "(" + "\((currentRecordsArray[indexPath.row] as! NSDictionary)["reviews_count"]!)" + ")"

            cell.businessAddress.text = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["address"]!)"

            cell.verifiedImage.sd_setImage(with: URL(string: "\((currentRecordsArray[indexPath.row] as! NSDictionary)["verified_img"]!)"))

            cell.verifiedStatusLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "verifiedAccount", comment: "")

            cell.addToFavouriteButton.tag = indexPath.row

            cell.addToFavouriteButton.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "addToFavourite", comment: ""), for: .normal)
            cell.addToFavouriteButton.addTarget(self, action: #selector(ServicesMainViewController.addToFavourites(sender:)), for: .touchUpInside)

            cell.addedToFavourites.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "added_to_favourites", comment: ""), for: .normal)
            cell.addedToFavourites.addTarget(self, action: #selector(ServicesMainViewController.removeFromFavourites(sender:)), for: .touchUpInside)

            if "\((currentRecordsArray[indexPath.row] as! NSDictionary)["favourite_status"]!)".elementsEqual("0")
            {
                cell.addToFavouriteButton.isHidden = false
                cell.addedToFavourites.isHidden = true
            }
            else
            {
                cell.addToFavouriteButton.isHidden = true
                cell.addedToFavourites.isHidden = false
            }


            if "\((currentRecordsArray[indexPath.row] as! NSDictionary)["verified_status"]!)".elementsEqual("V")
            {
                cell.verifiedImage.isHidden = false
                cell.verifiedStatusLBL.isHidden = false
            }
            else
            {
                cell.verifiedImage.isHidden = true
                cell.verifiedStatusLBL.isHidden = true
            }


            ///////////

            cell.cellView.layer.borderWidth = 2
            cell.cellView.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor

            cell.addToFavouriteButton.layer.cornerRadius = cell.addToFavouriteButton.frame.size.height / 2

            cell.addedToFavourites.layer.cornerRadius = cell.addedToFavourites.frame.size.height / 2

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if tableView == selectCategoryTableview
        {
            return UITableView.automaticDimension
        }
        else if tableView == sortCategoryTableview
        {
            //            return (40/568)*self.FullHeight
            return UITableView.automaticDimension
        }
        else
        {
            //dynamic business Name and its height
            //            let businessNameString = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["business_name"]!)"
            //
            //            businessNameHeight = businessNameString.heightWithConstrainedWidth(width: (200/320)*self.FullWidth, font: UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 15)))!)
            //
            //            //dynamic business address and its height
            //            let businessAddressString = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["address"]!)"
            //
            //            businessAddressHeight = businessAddressString.heightWithConstrainedWidth(width: (170/320)*self.FullWidth, font: UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 13)))!)
            //
            //            //viewheight
            //            viewHeight = (10/568)*self.FullHeight + businessNameHeight + (30/568)*self.FullHeight + businessAddressHeight + (80/568)*self.FullHeight
            //
            //            return (viewHeight + (7/568)*self.FullHeight)
            return UITableView.automaticDimension
        }


    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == selectCategoryTableview
        {
            //typeID = "\(((servicesMainDictionary["category_list"] as! NSArray)[indexPath.row] as! NSDictionary)["id"]!)"

            typeID = "\((localCategoryArray[indexPath.row] as! NSDictionary)["id"]!)"

            recordsArray.removeAllObjects()
            currentRecordsArray.removeAllObjects()
            self.startValue = 0
            getServicesData()

            animateCategory(toggle: false)
//
//            self.categoryName.text = "\((localCategoryArray[indexPath.row] as! NSDictionary)["name"]!)"

            dispatchGroup.notify(queue: .main, execute: {
                self.setScrollContent()
            })

        }
        else if tableView == sortCategoryTableview
        {
            self.sortCategoryTableview.isHidden = true
            self.sortBackgroundView.isHidden = true

            self.sortType = "\((self.localSortCategoryArray[indexPath.row] as NSDictionary)["key"]!)"

            self.startValue = 0

            self.recordsArray.removeAllObjects()
            self.currentRecordsArray.removeAllObjects()

            self.getServicesData()

            self.sortType = ""
        }
        else
        {
            //            let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
//
//            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookVC") as! ServiceBookViewController
//
//            sellerID = "\((currentRecordsArray[indexPath.row] as! NSDictionary)["seller_id"]!)"
//            
//            print("sellerid",sellerID)
//
//            UserDefaults.standard.set(sellerID, forKey: "sellerID")
//
//            navigate.sellerID = sellerID
//
//            self.navigationController?.pushViewController(navigate, animated: true)

        }

    }


    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        if scrollView == imageScroll
        {

            let pageNumber = imageScroll.contentOffset.x / imageScroll.frame.size.width

            imagePageControl.currentPage = Int(pageNumber)
        }

        else
        {

            if scrollBegin > scrollEnd
            {

            }
            else
            {

//                print("next start : ",nextStart )

                if (nextStart).isEmpty
                {
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }


                }
                else
                {
                    getServicesData()
                }



            }
        }

    }




    // MARK: - // Animate Category DropDown Tableview


    func animateCategory(toggle: Bool) {
        if toggle {

            UIView.animate(withDuration: 0.3){

                self.view.bringSubviewToFront(self.selectCategoryView)
                //                self.selectCategoryView.autoresizesSubviews = false
                self.selectCategoryView.isHidden = false
                let tblheight = self.selectCategoryTableview.visibleCells[0].bounds.height
                self.SelectCategoryTblViewHeight.constant = CGFloat(self.localCategoryArray.count) * tblheight
                //                self.selectCategoryView.frame.size.height = ((100/568)*self.FullHeight)
                //                self.selectCategoryTableview.frame.size.height = ((100/568)*self.FullHeight)
                //                self.selectCategoryView.autoresizesSubviews = true
            }
        }
        else {

            UIView.animate(withDuration: 0.3){
                //                self.selectCategoryView.autoresizesSubviews = false
                self.selectCategoryView.isHidden = true
                self.SelectCategoryTblViewHeight.constant = 0
                //                self.selectCategoryView.autoresizesSubviews = true
                self.view.sendSubviewToBack(self.selectCategoryView)
            }
        }

    }


    // MARK:- // Set Image Scroll

    func setImageScroll()
    {

        let tempArray = (self.servicesMainDictionary["slider_image"] as! NSArray)

        self.imagePageControl.numberOfPages = tempArray.count

        for i in 0..<tempArray.count
        {
            self.tempFrame.origin.x = self.imageScroll.frame.size.width * CGFloat(i)
            self.tempFrame.size = self.imageScroll.frame.size

            let imageview = UIImageView(frame: self.tempFrame)
            imageview.sd_setImage(with: URL(string: "\((tempArray[i] as! NSDictionary)["slider_img"]!)"))

            imageview.contentMode = .scaleAspectFit
            imageview.clipsToBounds = true

            self.imageScroll.addSubview(imageview)
        }
        self.imageScroll.contentSize = CGSize(width: (self.imageScroll.frame.size.width * CGFloat(tempArray.count)), height: self.imageScroll.frame.size.height)


        self.imageScroll.delegate = self


        /////////////
    }



    // MARK: - // JSON POST Method to get Services Data

    func getServicesData()

    {
        self.dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }



        let url = URL(string: GLOBALAPI + "app_service_list_main")!   //change the url

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")

//        print("test",localCategoryArray)

        if sortType.isEmpty
        {

            if (userID?.isEmpty)!
            {
                parameters = "lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)&mycountry_name=\(myCountryName!)&child_cat_id=\(typeID!)"
            }
            else
            {
                parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)&mycountry_name=\(myCountryName!)&child_cat_id=\(typeID!)"
            }



        }
        else
        {

            parameters = "user_id=\(userID!)&lang_id=\(langID!)&per_load=\(10)&start_value=\(startValue)&mycountry_name=\(myCountryName!)&child_cat_id=\(typeID!)&sort_search=\(sortType!)"

        }

        print("Services URL is : ",url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Services Response: " , json)

                    if "\(json["response"]!)".elementsEqual("0")
                    {

                        DispatchQueue.main.async {

                            self.imageScrollView.isHidden = true
                            self.servicesTableview.isHidden = true
                            self.imagePageControl.isHidden = true

                            let noDataLBL = UILabel(frame: CGRect(x: 0, y: 0, width: (320/320)*self.FullWidth, height: (30/568)*self.FullHeight))

                            noDataLBL.center = self.view.center

                            noDataLBL.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 17)))

                            noDataLBL.text = "\(json["message"]!)"
                            noDataLBL.textAlignment = .center

                            self.mainScroll.addSubview(noDataLBL)

                            noDataLBL.center = self.mainScroll.center

                            SVProgressHUD.dismiss()
                        }
                    }
                    else
                    {
                        self.servicesMainDictionary = json["info_array"] as? NSDictionary

                        for i in 0..<((self.servicesMainDictionary["service"] as! NSArray).count)

                        {

                            let tempDict = (self.servicesMainDictionary["service"] as! NSArray)[i] as! NSDictionary

                            self.recordsArray.add(tempDict as! NSMutableDictionary)


                        }

                        self.currentRecordsArray = self.recordsArray.mutableCopy() as! NSMutableArray

                        self.nextStart = "\(json["next_start"]!)"

                        self.startValue = self.startValue + (self.servicesMainDictionary["service"] as! NSArray).count

                        print("Next Start Value : " , self.startValue)

                        self.localCategoryArray.removeAllObjects()
                        self.localCategoryArray.add(["id" : "","name" : "All"])
                        for i in 0..<((self.servicesMainDictionary["category_list"] as! NSArray).count)
                        {
                            self.localCategoryArray.add((self.servicesMainDictionary["category_list"] as! NSArray)[i] as! NSDictionary)
                        }


                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.selectCategoryTableview.delegate = self
                            self.selectCategoryTableview.dataSource = self
                            self.selectCategoryTableview.reloadData()
                            self.selectCategoryTableview.rowHeight = UITableView.automaticDimension
                            self.selectCategoryTableview.estimatedRowHeight = UITableView.automaticDimension

                            self.servicesTableview.delegate = self
                            self.servicesTableview.dataSource = self
                            self.servicesTableview.reloadData()
                            self.servicesTableview.rowHeight = UITableView.automaticDimension
                            self.servicesTableview.estimatedRowHeight = UITableView.automaticDimension

                            SVProgressHUD.dismiss()
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()



    }




    // MARK: - // JSON POST Method to add Favourite Service

    func serviceAddToFavourites(sellerid : String)

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }


        let url = URL(string: GLOBALAPI + "app_add_to_favourite")!   //change the url

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")


        parameters = "user_id=\(userID!)&lang_id=\(langID!)&buss_seller_id=\(sellerid)"

        print("Save Service Booking URL is : ",url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Service Add to Favourites Response: " , json)

                    if "\(json["response"]!)".elementsEqual("0")
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.showAlert(title: "Add to Favourites Response", message: "\(json["message"]!)")

                            SVProgressHUD.dismiss()

                        }

                    }
                    else
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.showAlert(title: "Add to Favourites Response", message: "\(json["message"]!)")


                            self.startValue = 0
                            self.recordsArray.removeAllObjects()
                            self.currentRecordsArray.removeAllObjects()
                            self.getServicesData()

                            SVProgressHUD.dismiss()
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()

    }




    // MARK: - // JSON POST Method to Remove Favourite Service

    func serviceRemoveFromFavourites(sellerid : String)

    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }


        let url = URL(string: GLOBALAPI + "app_remove_my_favourite")!   //change the url

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")


        parameters = "user_id=\(userID!)&lang_id=\(langID!)&remove_id=\(sellerid)"

        print("Remove Service from Favourite URL is : ",url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        } 

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Service Remove from Favourites Response: " , json)

                    if "\(json["response"]!)".elementsEqual("0")
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.showAlert(title: "Remove from Favourites Response", message: "\(json["message"]!)")

                            SVProgressHUD.dismiss()

                        }

                    }
                    else
                    {

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                            self.showAlert(title: "Remove from Favourites Response", message: "\(json["message"]!)")

                            self.startValue = 0
                            self.recordsArray.removeAllObjects()
                            self.currentRecordsArray.removeAllObjects()
                            self.getServicesData()

                            SVProgressHUD.dismiss()
                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()

    }


    // MARK:- // Set Scrollview Contentsize

    func setScrollContent()
    {

        self.tableviewHeight = 0

        self.servicesTableview.contentSize = CGSize(width: self.FullWidth, height: 3000)

        self.servicesTableview.frame.size.height = 3000

        for cell in self.servicesTableview.visibleCells {
            self.tableviewHeight += cell.bounds.height
        }

        print("TableviewHeight---- : ",self.tableviewHeight)

        self.servicesTableview.frame.size.height = self.tableviewHeight
        self.servicesTableview.contentSize = CGSize(width: self.FullWidth, height: self.tableviewHeight)


        self.mainScroll.contentSize = CGSize(width: self.FullWidth, height: self.imageScrollView.frame.size.height + tableviewHeight + (10/568)*self.FullHeight)
    }


    // MARK: - Hide sortBackgroundView when tapping outside
    @objc func tapGestureHandler() {
        UIView.animate(withDuration: 0.5)
        {
            self.view.sendSubviewToBack(self.sortBackgroundView)
            self.sortBackgroundView.isHidden = true

            if self.sortCategoryTableview.isHidden == false
            {
                self.sortCategoryTableview.isHidden = true
                self.view.sendSubviewToBack(self.sortCategoryTableview)
            }

            //self.categoryDropDownButtonOutlet.isUserInteractionEnabled = true
        }
    }


    // MARK:- // Set FOnt


    func set_font()
    {

        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

    }


    // MARK:- // Searchbar Delegate

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {

        UIView.animate(withDuration: 0.5, animations: {

            self.searchViewTopConstraint.constant = self.FullHeight
            self.searchViewBottomConstraint.constant = -self.FullHeight

            self.searchView.searchBar.resignFirstResponder()

        }, completion: nil)

    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        searchString = searchText.trimmingCharacters(in: .whitespaces)

        //searchString = searchText.replacingOccurrences(of: " ", with: "")

    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        print("Search With ----- ", searchString!)

        UIView.animate(withDuration: 0.5, animations: {

            if self.searchString.isEmpty
            {
                self.currentRecordsArray = self.recordsArray
                self.servicesTableview.reloadData()


                self.searchViewTopConstraint.constant = self.FullHeight
                self.searchViewBottomConstraint.constant = -self.FullHeight


                self.searchView.searchBar.resignFirstResponder()
            }
            else
            {
                // Put your key in predicate that is "Name"
                let searchPredicate = NSPredicate(format: "business_name CONTAINS[C] %@", self.searchString)
                self.currentRecordsArray = NSMutableArray(array: (self.recordsArray).filtered(using: searchPredicate))

                print ("Current Records Array------- = \(self.currentRecordsArray)")

                self.servicesTableview.reloadData()


                self.searchViewTopConstraint.constant = self.FullHeight
                self.searchViewBottomConstraint.constant = -self.FullHeight


                self.searchView.searchBar.resignFirstResponder()
            }

        }, completion: nil)

    }


    // MARK:- // Alert

    func showAlert(title: String,message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)

        alert.addAction(ok)

        self.present(alert, animated: true, completion: nil )

    }

    // MARK:- // Add to Favourites

    @objc func addToFavourites(sender: UIButton)
    {
        let userID = UserDefaults.standard.string(forKey: "userID")

        if (userID?.isEmpty)!
        {
            self.showAlert(title: "Not Logged IN", message: "Please Login to use this feature.")
        }
        else
        {
            self.serviceAddToFavourites(sellerid: "\((currentRecordsArray[sender.tag] as! NSDictionary)["seller_id"]!)")
        }

    }

    // MARK:- // Remove From Favourites

    @objc func removeFromFavourites(sender: UIButton)
    {
        let userID = UserDefaults.standard.string(forKey: "userID")

        if (userID?.isEmpty)!
        {
            self.showAlert(title: "Not Logged IN", message: "Please Login to use this feature.")
        }
        else
        {
            self.serviceRemoveFromFavourites(sellerid: "\((currentRecordsArray[sender.tag] as! NSDictionary)["seller_id"]!)")
        }

    }
    func SetFont()
    {
        self.sortLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "sort", comment: "")
    }


}
