//
//  ServicesTVCTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 19/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos

class ServicesTVCTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var businessImage: UIImageView!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var businessRating: CosmosView!
    @IBOutlet weak var businessReviewsCount: UILabel!
    @IBOutlet weak var addressPlaceholderImage: UIImageView!
    @IBOutlet weak var businessAddress: UILabel!
    @IBOutlet weak var verifiedImage: UIImageView!
    @IBOutlet weak var verifiedStatusLBL: UILabel!
    @IBOutlet weak var addToFavouriteButton: UIButton!
    @IBOutlet weak var addedToFavourites: UIButton!
    
    
    
    
    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
