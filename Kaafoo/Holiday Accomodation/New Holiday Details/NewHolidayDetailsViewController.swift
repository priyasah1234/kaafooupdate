//
//  NewHolidayDetailsViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 7/9/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage
import SVProgressHUD
import FSCalendar


class NewHolidayDetailsViewController: GlobalViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,FSCalendarDelegate,FSCalendarDataSource {
    
    let CurrentUserID = UserDefaults.standard.string(forKey: "userID")
    
    let annotation = MKPointAnnotation()
    
    @IBOutlet weak var ViewMessage: UIView!
    
    @IBOutlet weak var hideViewMessage: UIButton!
    
    @IBOutlet weak var MessageView: MessageBox!
    
    @IBOutlet weak var MemberSince: UILabel!
    
    @IBOutlet weak var TimingTableView: UITableView!
    
    @IBOutlet weak var AboutPropertyCheckinOutView: UIView!
    
    @IBOutlet weak var CheckinTimeLbl: UILabel!
    
    @IBOutlet weak var CheckOutTimeLbl: UILabel!
    
    @IBOutlet weak var CheckINTime: UILabel!
    
    @IBOutlet weak var CheckOutTime: UILabel!
    
    @IBAction func HideCalendarBtn(_ sender: UIButton) {
        
        self.ShowCalendarView.isHidden = true
    }
    
    @IBOutlet weak var AboutPropertyHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ShowCalendarView: UIView!
    
    @IBOutlet weak var HideCalendarView: UIButton!
    
    @IBOutlet weak var Calendar: FSCalendar!
    
    @IBOutlet weak var ShowBiggerImageView: UIView!
    
    @IBAction func CloseBiggerImageView(_ sender: UIButton) {
        
        self.ShowBiggerImageView.isHidden = true
        
    }
    
    @IBOutlet weak var BiggerImageCollectionView: UICollectionView!

    @IBOutlet weak var FollowBtnOutlet: UIButton!
    
    @IBAction func FollowBtn(_ sender: UIButton) {
        
        self.FollowBtnOutlet.isSelected = !self.FollowBtnOutlet.isSelected

        if (self.FollowBtnOutlet.titleLabel!.text?.elementsEqual("Follow"))! {
            
            self.followSeller()

            self.FollowBtnOutlet.setTitle("Unfollow", for: .normal)
        }
        else {
            self.unfollowSeller()

            self.FollowBtnOutlet.setTitle("Follow", for: .normal)
        }

    }
    @IBOutlet weak var PrivateBusinessAccount: UILabel!
    
    @IBOutlet weak var SellerName: UILabel!
    
    @IBOutlet weak var SellerImage: UIImageView!
    
    @IBOutlet weak var SendMessageBtnOutlet: UIButton!
    
    @IBAction func SendMessageBtn(_ sender: UIButton) {
        
        if "\(self.InfoArray["product_usr_id"]!)".elementsEqual(userID!)
        {
            self.ShowAlertMessage(title: "Warning", message: "You can not send message for your own product")
        }
        else
        {
            self.ViewMessage.isHidden = false
            
            self.view.bringSubviewToFront(self.ViewMessage)
            
        }
    }
    @IBOutlet weak var ViewAccountBtnOutlet: UIButton!

    @IBAction func ViewAccountBtn(_ sender: UIButton) {

        if "\((self.InfoArray["seller_info"] as! NSDictionary)["seller_type"]!)".elementsEqual("B") {
            
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "businessUserProfileVC") as! BusinessUserProfileViewController

            navigate.sellerID = "\((self.InfoArray["seller_info"] as! NSDictionary)["seller_id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)
        }
        else
        {
            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "privateUserProfileVC") as! PrivateUserProfileViewController

            navigate.sellerID = "\((self.InfoArray["seller_info"] as! NSDictionary)["seller_id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)
        }

    }


    //MARK:- //Variables And Array Declaration
    var ProductID : String! = ""
    
    var checkINDate : String! = ""
    
    var CheckOUTDate : String! = ""
    
    var ChildrenNo : String! = ""
    
    var RoomsNo : String! = ""
    
    var PerLoad : String! = ""
    
    var StartValue : String! = ""
    
    var PerLoadRV : String! = ""
    
    var StartValueRV : String! = ""
    
    let myCountry = UserDefaults.standard.string(forKey: "myCountryName")
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    let langId = UserDefaults.standard.string(forKey: "langID")
    
    var InfoArray : NSDictionary!
    
    var SliderImageArray : NSMutableArray! = NSMutableArray()
    
    var ReviewDict : NSDictionary!
    
    var RoomInfoArray : NSMutableArray!
    
    var SellerInfo : NSDictionary! = NSDictionary()
    
    var ProductOptionArray : NSMutableArray! = NSMutableArray()
    
    var EstimateTimeArrivalArray : NSMutableArray! = NSMutableArray()
    
    var ArrivalCheckInTime : NSMutableArray! = NSMutableArray()
    
    var ShowCheckinDate : Bool! = true
    
    fileprivate let formatter: DateFormatter = {
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd-MMM"
        
        return formatter
    }()
    
    fileprivate let dateFormatter : DateFormatter = {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        return dateFormatter
    }()
    
    fileprivate let APIdateFormatter : DateFormatter = {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter
    }()

    @IBOutlet weak var MainScrollView: UIScrollView!
    
    @IBOutlet weak var ContentView: UIView!
    
    @IBOutlet weak var SliderCollectionView: UICollectionView!
    
    @IBOutlet weak var PageControlWatchlistView: UIView!
    
    @IBOutlet weak var HotelNameView: UIView!
    
    @IBOutlet weak var MapView: UIView!
    
    @IBOutlet weak var CheckingRoomView: UIView!
    
    @IBOutlet weak var SelectRoomBtnOutlet: UIButton!
    
    @IBAction func SelectRoomBtn(_ sender: UIButton) {
        
        let obj = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "roomdetails") as! HolidayRoomDetailsViewController
        
        obj.InfoDict = self.InfoArray
        
        obj.RoomReviewDictionary = self.ReviewDict
        
        obj.SellerInfoDict = SellerInfo
        
        obj.RoomDetailsArray = (self.RoomInfoArray.mutableCopy() as? NSMutableArray)!
        
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBOutlet weak var DescriptionHeaderView: UIView!
    
    @IBOutlet weak var DescriptionStackView: UIStackView!
    
    @IBOutlet weak var AboutPropertyHeaderView: UIView!
    
    @IBOutlet weak var AboutPropertyStackView: UIStackView!
    
    @IBOutlet weak var CloseByHeaderView: UIView!
    
    @IBOutlet weak var CloseByView: UIView!
    
    @IBOutlet weak var ReviewRatingHeaderView: UIView!
    
    @IBOutlet weak var PostedByTotalView: UIStackView!

    @IBOutlet weak var SliderPageControl: UIPageControl!
    
    @IBOutlet weak var WatchlistedBtnOutlet: UIButton!
    
    @IBAction func WatchlistedBtn(_ sender: UIButton) {
        
        if self.CurrentUserID?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Login and Try again later")
        }
        else
        {
            self.removeFromWatchList()
            let alert = UIAlertController(title: "Removed", message: "Succcessfully Removed from Watchlist", preferredStyle: .alert)

            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)

            alert.addAction(ok)

            self.present(alert, animated: true, completion: nil )
            
            self.WatchlistedBtnOutlet.isHidden = true
            
            self.WatchlistBtnOutlet.isHidden = false
        }
    }
    @IBOutlet weak var WatchlistBtnOutlet: UIButton!
    
    @IBAction func WatchlistBtn(_ sender: UIButton) {
        
        if self.CurrentUserID?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Login and Try again later")
        }
        else
        {
            self.addToWatchList()
            
            let alert = UIAlertController(title: "Added", message: "Succcessfully Added to Watchlist", preferredStyle: .alert)

            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)

            alert.addAction(ok)

            self.present(alert, animated: true, completion: nil )
            
            self.WatchlistedBtnOutlet.isHidden = false
            
            self.WatchlistBtnOutlet.isHidden = true
        }
    }
    @IBOutlet weak var HotelLbl: UILabel!
    
    @IBOutlet weak var HotelName: UILabel!
    
    @IBOutlet weak var MapContentView: UIView!
    
    @IBOutlet weak var MkMapView: MKMapView!
    
    @IBOutlet weak var LocationLbl: UILabel!
    
    @IBOutlet weak var LocationDistance: UILabel!
    
    @IBOutlet weak var LocationName: UILabel!
    
    @IBOutlet weak var TeleNo: UILabel!
    
    @IBOutlet weak var CheckInBtnOutlet: UIButton!
    
    @IBOutlet weak var CheckinDate: UILabel!
    
    @IBOutlet weak var CheckOutBtnOutlet: UIButton!
    
    @IBOutlet weak var CheckoutDate: UILabel!
    
    @IBOutlet weak var ChangeDateBtnOutlet: UIButton!
    
    @IBAction func ChangeDateBtn(_ sender: UIButton) {
        
       // self.ShowCalendarView.isHidden = false
        
    }
    
    @IBAction func CheckInBtn(_ sender: UIButton) {
        
        self.ShowCheckinDate = true
        
        self.ShowCalendarView.isHidden = false
    }
    
    @IBAction func CheckOutBtn(_ sender: UIButton) {
        
        self.ShowCheckinDate = false
        
        self.ShowCalendarView.isHidden = false
    }
    
    @IBOutlet weak var DescriptionLbl: UILabel!
    
    @IBOutlet weak var DescriptionView: UIView!
    
    @IBOutlet weak var DescriptionString: UILabel!
    
    @IBAction func ShowFullDescriptionBtn(_ sender: UIButton) {
        
    }
    
    @IBOutlet weak var ShowDescriptionBtnOutlet: UIButton!
    
    @IBOutlet weak var NoDescriptionFoundLbl: UILabel!
    
    @IBOutlet weak var AboutThePropertyLbl: UILabel!
    
    @IBOutlet weak var AboutPropertyTableView: UITableView!
    
    @IBOutlet weak var NoAboutPropertyView: UIView!
    
    @IBOutlet weak var NoAboutPropertyLabel: UILabel!
    
    @IBOutlet weak var CloseByLbl: UILabel!
    
    @IBOutlet weak var CloseByString: UILabel!
    
    @IBOutlet weak var RatingNo: UILabel!
    
    @IBOutlet weak var RatingFeedBack: UILabel!
    
    @IBOutlet weak var TotalRatingReviews: UILabel!
    
    @IBAction func AllRatingBtn(_ sender: UIButton) {
        
    }
    
    @IBOutlet weak var AllRatingBtnOutlet: UIButton!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.CheckinDate.text = holidaySearchParameters.shared.tempCheckIndate!
        
        self.CheckoutDate.text = holidaySearchParameters.shared.tempCheckOutdate!
        
        holidaySearchParameters.shared.setHolidayProductID(productID: ProductID)
        
        self.Calendar.delegate = self
        
        self.Calendar.dataSource = self
        
        self.ShowCalendarView.isHidden = true
        
        self.ShowBiggerImageView.isHidden = true
        
        self.SetViews()
        
        self.GetDetails()
        
        self.SetWatchlistChecking()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.ViewMessage.isHidden = true
        
        self.view.sendSubviewToBack(self.ViewMessage)
    }
    
    //MARK:- //hide Message View Btn target
    
    @objc func hideMessgeTarget(sender : UIButton)
    {
        self.ViewMessage.isHidden = true
        
        self.view.sendSubviewToBack(self.ViewMessage)
    }
    
    //MARK:- //Send Message Button Target
    
    @objc func SendMessageBtnTarget(sender : UIButton)
    {
        print("Send message")
        
        if self.userID?.isEmpty == true
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""))
        }
        else
        {
            if self.MessageView.MessageTextView.text.elementsEqual("")
            {
                self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: "Message Can not be blank")
            }
            else
            {
                self.sendMessage()
            }
            
        }
    }
    
    //MARK:- // Cancel Message Button Target
    
    @objc func CancelMessageBtnTarget(sender : UIButton)
    {
        self.ViewMessage.isHidden = true
        
        self.view.sendSubviewToBack(self.ViewMessage)
    }
    
    func SetWatchlistChecking()
    {
        if self.CurrentUserID?.isEmpty == true
        {
            self.WatchlistedBtnOutlet.isHidden = false
            
            self.WatchlistBtnOutlet.isHidden = true
        }
        else
        {
            self.WatchlistedBtnOutlet.isHidden = true
            
            self.WatchlistBtnOutlet.isHidden = false
        }
        
        self.MessageView.CancelBtnOutlet.addTarget(self, action: #selector(self.CancelMessageBtnTarget(sender:)), for: .touchUpInside)
        
        self.MessageView.SendBtnOutlet.addTarget(self, action: #selector(self.SendMessageBtnTarget(sender:)), for: .touchUpInside)
        
        self.hideViewMessage.addTarget(self, action: #selector(self.hideMessgeTarget(sender:)), for: .touchUpInside)
        
        self.MessageView.MessageTextView.text = ""
    }

    func SetViews()
    {
        self.ReviewRatingHeaderView.layer.borderWidth = 1.0
        
        self.ReviewRatingHeaderView.layer.borderColor = UIColor.lightGray.cgColor

//        self.CheckinDate.layer.borderColor = UIColor.black.cgColor
//        self.CheckinDate.layer.borderWidth = 1.0
//
//        self.CheckoutDate.layer.borderColor = UIColor.black.cgColor
//        self.CheckoutDate.layer.borderWidth = 1.0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- //API Fire
    func GetDetails()
    {
       let parameters = "lang_id=\(langId!)&user_id=\(userID!)&product_id=\(ProductID!)&checkin=\(checkINDate!)&checkout=\(CheckOUTDate!)&children=\(ChildrenNo!)&rooms=\(RoomsNo!)&mycountry_name=\(myCountry!)&per_load=\(PerLoad!)&start_value=\(StartValue!)&per_load_rv=\(PerLoadRV!)&start_value_rv=\(StartValueRV!)"

        self.CallAPI(urlString: "app_holiday_details", param: parameters, completion: {
            
           self.InfoArray = self.globalJson["info_array"] as? NSDictionary
            
            self.SliderImageArray = self.InfoArray["slider_image"] as? NSMutableArray
            
            self.ReviewDict = self.InfoArray["review"] as? NSDictionary
            
            self.RoomInfoArray = self.InfoArray["room_info"] as? NSMutableArray
            
            self.SellerInfo = self.InfoArray["seller_info"] as? NSDictionary
            
            self.ProductOptionArray = self.InfoArray["product_option"] as? NSMutableArray

//            print("SliderImageArray",self.SliderImageArray)
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()

                self.SetData()
                
                self.SetSliderControl()
                
                self.SliderCollectionView.delegate = self
                
                self.SliderCollectionView.dataSource = self
                
                self.SliderCollectionView.reloadData()

                self.BiggerImageCollectionView.delegate = self
                
                self.BiggerImageCollectionView.dataSource = self
                
                self.BiggerImageCollectionView.reloadData()

                self.AboutPropertyTableView.delegate = self
                
                self.AboutPropertyTableView.dataSource = self
                
                self.AboutPropertyTableView.reloadData()

                self.SetTableheight(table: self.AboutPropertyTableView, heightConstraint: self.AboutPropertyHeightConstraint)
            }
        })
    }

    
    func SetData()
    {
        let SellerimagePath = "\((self.InfoArray["seller_info"] as! NSDictionary)["seller_image"]!)"
        
        self.SellerImage.sd_setImage(with: URL(string: SellerimagePath))

        self.SellerName.text = "\((self.InfoArray["seller_info"] as! NSDictionary)["display_name"]!)"
        
        self.MemberSince.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "memberSince", comment: "") + " : " + "\((self.InfoArray["seller_info"] as! NSDictionary)["member_since"]!)"

        if "\((self.InfoArray["seller_info"] as! NSDictionary)["seller_type"]!)".elementsEqual("B")
        {
          self.PrivateBusinessAccount.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "business_account", comment: "")
        }
        else
        {
          self.PrivateBusinessAccount.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "private_account", comment: "")
        }
        
        self.annotation.coordinate = CLLocationCoordinate2D(latitude: 88.6643, longitude: 22.7853)
        
        self.MkMapView.addAnnotation(self.annotation)

//        self.PrivateBusinessAccount

        self.HotelName.text = "\(self.InfoArray["productname"]!)"

        if "\(self.InfoArray["product_desc"]!)".elementsEqual("")
        {
            self.DescriptionView.isHidden = true
            
            self.NoDescriptionFoundLbl.isHidden = false
        }
        else
        {
            self.DescriptionView.isHidden = false
            
            self.NoDescriptionFoundLbl.isHidden = true
            
            self.DescriptionString.text = "\(self.InfoArray["product_desc"]!)".trimmingCharacters(in: .whitespacesAndNewlines)
        }

        if  "\(self.InfoArray["close_by"]!)".elementsEqual("")
        {
           self.CloseByString.text = "No Close By Found"
        }
        else
        {
           self.CloseByString.text = "\(self.InfoArray["close_by"]!)"
        }

        self.LocationName.text = "\(self.InfoArray["address"]!)"
        


        if self.ProductOptionArray.count > 0
        {
            self.AboutPropertyTableView.isHidden = false
            
            self.NoAboutPropertyView.isHidden = true

        }
        else
        {
            self.AboutPropertyTableView.isHidden = true
            
            self.NoAboutPropertyView.isHidden = false
        }

//        self.CheckINTime.text = "\((self.InfoArray["estimate_arrival_time"] as! NSDictionary)["time_val"]!)" + "\((self.InfoArray["estimate_arrival_time"] as! NSDictionary)["time_format"]!)"
        
        self.CheckinTimeLbl.textColor = .yellow2()

        self.CheckOutTime.text = "\((self.InfoArray["estimate_checkout_time"] as! NSDictionary)["checkout_time_val"]!)" + "\((self.InfoArray["estimate_checkout_time"] as! NSDictionary)["checkout_time_format"]!)"
        
        self.CheckOutTimeLbl.textColor = .yellow2()

        self.LocationDistance.text = "\(self.InfoArray["distance_from_city"]!)" + "KM"

        self.SelectRoomBtnOutlet.backgroundColor = .yellow2()

        self.ViewAccountBtnOutlet.backgroundColor = .yellow2()
        
        self.RatingFeedBack.textColor = .yellow2()
        
        self.RatingNo.text = "\((self.InfoArray["review"] as! NSDictionary)["avg_rating"]!)"
        
        self.TotalRatingReviews.text = "\((self.InfoArray["review"] as! NSDictionary)["no of review"]!)"
        
        self.PositiveFeedback.text = "\((self.InfoArray["seller_info"] as! NSDictionary)["positive_feedback"]!)" + "% Positive Feedback"
        self.SellerRating.text = "\((self.InfoArray["seller_info"] as! NSDictionary)["total_rating"]!)"
        self.SellerContact.text = "Contact :" +  "\((self.InfoArray["seller_info"] as! NSDictionary)["mobile"]!)"
        
    }
    
    
    @IBOutlet weak var SellerContact: UILabel!
    
    @IBOutlet weak var SellerRating: UILabel!
    
    @IBOutlet weak var PositiveFeedback: UILabel!
    

    func SetSliderControl()
    {
        self.SliderPageControl.numberOfPages = self.SliderImageArray.count
    }

    //CollectionView Delegate and dataSource Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == SliderCollectionView
        {
           return self.SliderImageArray.count
        }
        else
        {
           return self.SliderImageArray.count
        }

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == SliderCollectionView
        {
            let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "slider", for: indexPath) as! SliderImagesCVC
            
            let path = "\((self.SliderImageArray[indexPath.item] as! NSDictionary)["image"]!)"
            
            obj.SliderImage.sd_setImage(with: URL(string: path))
            
            return obj
        }
        else
        {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bigger", for: indexPath) as! BiGgerImgCVC
            
           let path = "\((self.SliderImageArray[indexPath.item] as! NSDictionary)["image"]!)"
            
           cell.ShowImage.sd_setImage(with: URL(string: path))
            
           return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == SliderCollectionView
        {
            let padding : CGFloat! = 5
            
            let collectionViewSize = SliderCollectionView.frame.size.width - padding
            
            return CGSize(width: collectionViewSize, height: 177)
        }
        else
        {
            let collectionViewSize = BiggerImageCollectionView.frame.size.width
            
            return CGSize(width: collectionViewSize, height: 474)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == SliderCollectionView
        {
            self.ShowBiggerImageView.isHidden = false
            
            self.view.bringSubviewToFront(self.ShowBiggerImageView)

            let toIndex: IndexPath = IndexPath(item: indexPath.item, section: 0)

            self.BiggerImageCollectionView.scrollToItem(at: toIndex, at: .left, animated: true)
        }
        else
        {

        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }

    // MARK: - // JSON POST Method to add Data to WatchList

    func addToWatchList()

    {

        self.globalDispatchgroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }

        let url = URL(string: GLOBALAPI + "app_add_watchlist")!   //change the url

        print("addtoWatchList URL : ", url)

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "user_id=\(userID!)&add_product_id=\(ProductID!)"

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        } 

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    print("add to watchlist Response: " , json)

                    DispatchQueue.main.async {

                        self.globalDispatchgroup.leave()

                        SVProgressHUD.dismiss()
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()

    }

    // MARK: - // JSON POST Method to remove Data from WatchList

    func removeFromWatchList()

    {

        self.globalDispatchgroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }

        let url = URL(string: GLOBALAPI + "app_remove_watchlist")!   //change the url

        print("remove from watchlist URL : ", url)

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")

        parameters = "user_id=\(userID!)&remove_product_id=\(ProductID!)"

        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)


        } catch let error {
            print(error.localizedDescription)
        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("add to watchlist Response: " , json)


                    DispatchQueue.main.async {

                        self.globalDispatchgroup.leave()

                        SVProgressHUD.dismiss()
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()

    }
    
    // MARK:- // JSON Post Method to Send Message to User

    func sendMessage() {
      
        var parameters : String = ""
      
        let userID = UserDefaults.standard.string(forKey: "userID")
      
      parameters = "user_id=\(userID!)&seller_id=\("\(self.InfoArray["product_usr_id"] ?? "")")&message=\(self.MessageView.MessageTextView.text!)"

        self.CallAPI(urlString: "app_friend_send_message", param: parameters) {
          
            DispatchQueue.main.async {
              
                self.globalDispatchgroup.leave()
              
                SVProgressHUD.dismiss()
              
                self.MessageView.MessageTextView.text = ""
            }
        }
        self.globalDispatchgroup.notify(queue: .main) {

            self.customAlert(title: "Submit Success", message: "\(self.globalJson["message"] ?? "")", completion: {

                UIView.animate(withDuration: 0.3, animations: {

                    self.MessageView.isHidden = true
                    
                    self.ViewMessage.isHidden = true
                    
                    self.view.sendSubviewToBack(self.ViewMessage)
                })
            })
        }
    }

    //MARK:- FScalendar Delegate and DataSource Methods
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {

        if ShowCheckinDate == true
        {
           let daTE = self.formatter.string(from: date)
            let dateStr = self.dateFormatter.string(from: date)
            let dateSTROne = self.APIdateFormatter.string(from: date)
            self.CheckinDate.text = daTE
            holidaySearchParameters.shared.checkInDate = dateStr
            holidaySearchParameters.shared.setAPICheckindate(CheckIN: dateSTROne)
        }
        else
        {
           let daTE = self.formatter.string(from: date)
            let dateStr = self.dateFormatter.string(from: date)
            let dateStrone = self.APIdateFormatter.string(from: date)
            self.CheckoutDate.text = daTE
            holidaySearchParameters.shared.checkOutDate = dateStr
            holidaySearchParameters.shared.setAPICheckOutDate(CheckOut: dateStrone)
        }
        self.ShowCalendarView.isHidden = true
        print("did select date \(self.formatter.string(from: date))")


    }


    // MARK:- // Set Table Height

    func SetTableheight(table: UITableView , heightConstraint: NSLayoutConstraint) {
        var setheight: CGFloat  = 0
        table.frame.size.height = 3000

        for cell in table.visibleCells {
            setheight += cell.bounds.height
        }
        heightConstraint.constant = CGFloat(setheight)
    }
    
    //MARK:- // Follow Seller

    func followSeller()
    {
        var parameters : String = ""
        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        let SellerID = "\(self.SellerInfo["seller_id"]!)"

        parameters = "user_id=\(userID!)&buss_seller_id=\(SellerID)&lang_id=\(langID!)"

        self.CallAPI(urlString: "app_add_to_favourite", param: parameters) {

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

                self.ShowAlertMessage(title: "Success", message: "\(self.globalJson["message"] ?? "")")

            }
        }
    }

    // MARK:- // JSON Post method to Unfollow Seller


    func unfollowSeller() {

        let userID = UserDefaults.standard.string(forKey: "userID")
        var langID = UserDefaults.standard.string(forKey: "langID")
        let SellerID = "\(self.SellerInfo["seller_id"]!)"

        let parameters = "user_id=\(userID!)&lang_id=\(self.langID!)&remove_id=\(SellerID)"

        self.CallAPI(urlString: "app_remove_my_favourite", param: parameters) {

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

                self.ShowAlertMessage(title: "Success", message: "\(self.globalJson["message"] ?? "")")

            }
        }

    }

}

class SliderImagesCVC : UICollectionViewCell
{
    @IBOutlet weak var SliderImage: UIImageView!
}
extension NewHolidayDetailsViewController: UIScrollViewDelegate {

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == SliderCollectionView
        {
           SliderPageControl?.currentPage = Int(SliderCollectionView.contentOffset.x) / Int(SliderCollectionView.frame.width)
        }
        else
        {

        }


    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {

        if scrollView == SliderCollectionView
        {
           SliderPageControl?.currentPage = Int(SliderCollectionView.contentOffset.x) / Int(SliderCollectionView.frame.width)
        }
        else
        {

        }


    }
}

class BiGgerImgCVC : UICollectionViewCell
{
    @IBOutlet weak var ShowImage: UIImageView!
    
}
class AboutPropertyTVC : UITableViewCell
{
    @IBOutlet weak var firstKey: UILabel!
    
    @IBOutlet weak var SecondValue: UILabel!
    
}
extension NewHolidayDetailsViewController : UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ProductOptionArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "about") as! AboutPropertyTVC
        
    cell.firstKey.text = "\((self.ProductOptionArray[indexPath.row] as! NSDictionary)["option_label"]!)"
        
        cell.firstKey.textColor = .yellow2()
        
        cell.SecondValue.text = "\((self.ProductOptionArray[indexPath.row] as! NSDictionary)["option_value"]!)"
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableView.automaticDimension
    }

}


class TimingTVC : UITableViewCell
{
    
}
