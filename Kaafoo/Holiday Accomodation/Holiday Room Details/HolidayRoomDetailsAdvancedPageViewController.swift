//
//  HolidayRoomDetailsAdvancedPageViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 7/12/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SDWebImage

class HolidayRoomDetailsAdvancedPageViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var indexNumber = 0
    
     var taxArray = [TaxCalculate]()
    
    //MARK: //Custom Function for reloading page on NextButtonTap
    
    func SetData(index : Int) {
        
        self.tempButtonArray.removeAll()
        
        self.SmokingString = ""
    
        self.FloorString = ""
        
        self.AccessibleString = ""
        
        self.allergryString = ""
        
        self.SelectedGuestString = ""
        
        self.firstNameTextField.text = ""
        
        self.secondNameTextField.text = ""
        
        self.SmokingTxt.text = "Any"
        
        self.Floortxt.text = "Any"
        
        self.GuestTxt.text = "1"
        
        self.AccessibleRoomImg.image = UIImage(named: "checkBoxEmpty")
        
        self.AllergyFriendlyRoomImg.image = UIImage(named: "checkBoxEmpty")
        
        
//        self.imageArray = self.IndividualRoomDetailsArray[index].imageArray
        
        self.ProductImageCollectionVie.delegate = self
        self.ProductImageCollectionVie.dataSource = self
        self.ProductImageCollectionVie.reloadData()
        
        self.BiggerCollectionView.delegate = self
        self.BiggerCollectionView.dataSource = self
        self.BiggerCollectionView.reloadData()
        
        self.HotelName.text = "\(self.IndividualRoomDetailsArray[index].roomName ?? "")"
        self.HotelDescription.text = "\(self.IndividualRoomDetailsArray[index].roomDescription ?? "")"
        self.noRoomsSelected.text = "\(self.IndividualRoomDetailsArray[index].roomNumber ?? "")"
        
    }
    
    var tempButtonArray = [UIButton]()
    
    var ExtraPrice = [String]()
    
    var FirstNameString : String! = ""
    
    var SecondNameString : String! = ""
    
    var IndexNo : Int! = 0
    
    var IndividualRoomDetailsArray = [roomDetailsDataFormat]()
    
    var imageArray = NSArray()
    
    var TotalRoomDetailsStruct = [TotalRoomDetails]()
    
    var allergryString : String! = ""
    
    var AccessibleString : String! = ""
    
    var SmokingString : String! = ""
    
    var FloorString : String! = ""
    
    var SelectedGuestString : String = "1"


    @IBAction func NextBtn(_ sender: UIButton)
    {
        print("ExtraPriceArray",self.ExtraPrice)
        
        if firstNameTextField.text?.isEmpty == true
        {
           self.ShowAlertMessage(title: "Warning", message: "Please Enter Your First Name")
        }
        else if secondNameTextField.text?.isEmpty == true
        {
           self.ShowAlertMessage(title: "Warning", message: "Please Enter Your Last Name")
        }
        else
        {
            self.TotalRoomDetailsStruct.append(TotalRoomDetails(Smoking: self.SmokingString, Floor: self.FloorString, GuestNumber: self.SelectedGuestString, AccessibleRoom: self.AccessibleString, AllergyFriendlyRoom: self.allergryString, RoomId: "\(self.IndividualRoomDetailsArray[IndexNo].roomId ?? "")", FirstName: self.firstNameTextField.text ?? "", LastName: self.secondNameTextField.text ?? "", RoomNumbers: "\(self.IndividualRoomDetailsArray[IndexNo].roomNumber ?? "")"))
            
            self.taxArray.append(TaxCalculate(taxPrice: "\(self.IndividualRoomDetailsArray[IndexNo].roomTotalPrice ?? "")", roomPrice: "\(self.IndividualRoomDetailsArray[IndexNo].roomTaxPrice ?? "")"))
            
            self.IndexNo = self.IndexNo + 1
            
            self.indexNumber = self.indexNumber + 1
            
            print("description",self.TotalRoomDetailsStruct.description)
            
            if IndexNo == IndividualRoomDetailsArray.count
            {
                
                let obj = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "roombooking") as! NewHolidayRoomBookingViewController
                
                obj.TotalDetailsRoomStruct = self.TotalRoomDetailsStruct
                obj.TotalPriceArray = self.taxArray
                self.navigationController?.pushViewController(obj, animated: true)
            }
            else
            {
                self.SetData(index: self.IndexNo)
                
                self.SetAmintiesView(index: self.IndexNo)
                
                self.CreateRoomFeaturePriceView(index: IndexNo)
                
                self.CreateExtraPriceArray(index: IndexNo)
                
                self.ExtraFeaturePriceCalculation(index: IndexNo)
                
            }
        }

    }
    
    @IBOutlet weak var ShowBiggerImageView: UIView!
    @IBAction func HideBiggerImgShowBtn(_ sender: UIButton) {
        self.ShowBiggerImageView.isHidden = true
    }
    @IBOutlet weak var BiggerCollectionView: UICollectionView!

    @IBOutlet weak var ShowDataPickerView: UIView!
    @IBAction func hideDataPicker(_ sender: UIButton) {
        self.ShowDataPickerView.isHidden = true
    }
    @IBOutlet weak var dataPickerView: UIPickerView!


    @IBOutlet weak var RoomSelectionView: UIView!
    @IBOutlet weak var StaticFeatureView: UIView!
    @IBOutlet weak var DynamicFeatureViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var DynamicFeatureView: UIView!
    @IBOutlet weak var NextBtnOutlet: UIButton!
    @IBOutlet weak var DetailsStackView: UIStackView!
    @IBOutlet weak var CustomeerDetailsView: UIView!
    @IBOutlet weak var secondNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var ThirdSeparatorView: UIView!
    @IBOutlet weak var noRoomsSelected: UILabel!
    @IBOutlet weak var AmintiesStackView: UIStackView!
    @IBOutlet weak var HotelDetailsView: UIView!
    @IBOutlet weak var HotelDescription: UILabel!
    @IBOutlet weak var HotelName: UILabel!
    @IBOutlet weak var ProductImageCollectionVie: UICollectionView!
    @IBOutlet weak var featureStackView: UIStackView!
    @IBOutlet weak var specialRequestView: UIView!
    @IBOutlet weak var SecondSeparatorView: UIView!
    @IBOutlet weak var AmiintiesView: UIView!
    @IBOutlet weak var FollowingLblView: UIView!
    @IBOutlet weak var SelectedRoomsView: UIView!
    @IBOutlet weak var FirstSeparatorView: UIView!
    @IBOutlet weak var MainScrollView: UIScrollView!
    @IBOutlet weak var ScrollContentView: UIView!
    @IBOutlet weak var AminitesViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var FloorLbl: UILabel!
    @IBOutlet weak var GuestPerRoomLbl: UILabel!
    @IBOutlet weak var FloorOutlet: UIButton!

    @IBAction func FloorBtn(_ sender: UIButton) {
        self.ShowSmokingValue = false
        self.ShowPickerView.isHidden = false
        self.ValuePickerView.reloadAllComponents()

    }
    @IBOutlet weak var NoGuestOutlet: UIButton!
    @IBAction func NoGuestBtn(_ sender: UIButton) {
        self.ShowDataPickerView.isHidden = false
    }
    @IBOutlet weak var Floortxt: UILabel!
    @IBOutlet weak var GuestTxt: UILabel!
    @IBOutlet weak var SmokingLbl: UILabel!
    @IBAction func SmokingBtn(_ sender: UIButton) {
        self.ShowSmokingValue = true
        self.ShowPickerView.isHidden = false
        self.ValuePickerView.reloadAllComponents()

    }

    @IBAction func hidePickerView(_ sender: UIButton) {
        self.ShowPickerView.isHidden = true
    }
    
    @IBOutlet weak var SmokingOutlet: UIButton!
    @IBOutlet weak var SmokingTxt: UILabel!


    @IBAction func AccessibleRoomBtn(_ sender: UIButton) {
        if self.AccessibleRoomImg.image == UIImage(named: "checkBoxEmpty")
        {
           self.AccessibleRoomImg.image = UIImage(named: "checkBoxFilled")
            self.AccessibleString = "1"
           
        }
        else
        {
           self.AccessibleRoomImg.image = UIImage(named: "checkBoxEmpty")
           self.AccessibleString = ""
        }
    }
    @IBAction func AllergyFriendLyRoomBtn(_ sender: UIButton) {
        if self.AllergyFriendlyRoomImg.image == UIImage(named: "checkBoxEmpty")
        {
            self.AllergyFriendlyRoomImg.image = UIImage(named: "checkBoxFilled")
            self.allergryString = "2"
        }
        else
        {
            self.AllergyFriendlyRoomImg.image = UIImage(named: "checkBoxEmpty")
            self.allergryString = ""
        }
    }
    @IBOutlet weak var AccessibleRoomOutlet: UIButton!
    @IBOutlet weak var AllergyFriendOutlet: UIButton!
    @IBOutlet weak var AccessibleRoomImg: UIImageView!
    @IBOutlet weak var AllergyFriendlyRoomImg: UIImageView!

    @IBOutlet weak var ShowPickerView: UIView!
    

    var ProductImageArray : NSMutableArray! = NSMutableArray()
    var RoomDetailsDict : NSDictionary! = NSDictionary()
    var ProductAmintiesArray : NSMutableArray! = NSMutableArray()

    var ShowSmokingValue : Bool! = true
    @IBOutlet weak var ValuePickerView: UIPickerView!

    var SmokingValue = ["Any","Yes","No"]
    var FloorValue = ["Any","High","Low"]
    var dataArray = ["1","2","3","4","5","6","7","8","9","10"]
    
    //MARK:- Declaring Dictionaries for Smoking,Floor and SelectedGuestNumberArray
    
    var SmokingArray = [["key" : "Any" , "value" : ""],["key" : "Yes" , "value" : "Y"],["key" : "No" , "value" : "N"]]
    var FloorArray = [["key" : "Any" , "value" : ""],["key" : "Low" , "value" : "L"],["key" : "High" , "value" : "H"]]
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.SetData(index: 0)
        
        self.SetAmintiesView(index: 0)
        
        self.CreateRoomFeaturePriceView(index: 0)
        
        self.CreateExtraPriceArray(index: 0)
        
        self.ExtraFeaturePriceCalculation(index: 0)
        
        self.ProductImageCollectionVie.delegate = self
        self.ProductImageCollectionVie.dataSource = self
        self.ProductImageCollectionVie.reloadData()
        
        self.BiggerCollectionView.delegate = self
        self.BiggerCollectionView.dataSource = self
        self.BiggerCollectionView.reloadData()

        self.ShowBiggerImageView.isHidden = true

        self.ShowPickerView.isHidden = true
        self.ValuePickerView.delegate = self
        self.ValuePickerView.dataSource = self
        self.ValuePickerView.reloadAllComponents()

        self.dataPickerView.delegate = self
        self.dataPickerView.dataSource = self
        self.dataPickerView.reloadAllComponents()

        self.SetLayer()
        
        print("Count",self.IndividualRoomDetailsArray.count)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.TotalRoomDetailsStruct = []
        
        self.taxArray = []
        
        self.IndexNo = 0
        self.indexNumber = 0
        
    }

    //MARK:- Call Delegate and DataSouce Methods
    func loadPage()
    {
        self.HotelName.text = "\(self.IndividualRoomDetailsArray[IndexNo].roomName ?? "")"
        print("Hotel Room Name","\(self.IndividualRoomDetailsArray[IndexNo].roomName ?? "")")
        self.HotelDescription.text = "\(self.IndividualRoomDetailsArray[IndexNo].roomDescription ?? "")"
        
        self.ProductImageCollectionVie.delegate = self
        self.ProductImageCollectionVie.dataSource = self
        self.ProductImageCollectionVie.reloadData()

        self.BiggerCollectionView.delegate = self
        self.BiggerCollectionView.dataSource = self
        self.BiggerCollectionView.reloadData()
    }
    //MARK:- //Set Layer
    func SetLayer()
    {
       self.NextBtnOutlet.layer.borderWidth = 1.0
        self.NextBtnOutlet.setTitleColor(.yellow2(), for: .normal)
       self.NextBtnOutlet.clipsToBounds = true
       self.NextBtnOutlet.layer.borderColor = UIColor.yellow2().cgColor
       self.NextBtnOutlet.layer.cornerRadius = 0.5

        self.FloorOutlet.layer.borderWidth = 1.0
        self.FloorOutlet.clipsToBounds = true
        self.FloorOutlet.layer.borderColor = UIColor.darkGray.cgColor
        self.FloorOutlet.layer.cornerRadius = 0.5

        self.NoGuestOutlet.layer.borderWidth = 1.0
        self.NoGuestOutlet.clipsToBounds = true
        self.NoGuestOutlet.layer.borderColor = UIColor.darkGray.cgColor
        self.NoGuestOutlet.layer.cornerRadius = 0.5

        self.SmokingOutlet.layer.borderWidth = 1.0
        self.SmokingOutlet.clipsToBounds = true
        self.SmokingOutlet.layer.borderColor = UIColor.darkGray.cgColor
        self.SmokingOutlet.layer.cornerRadius = 0.5

        self.AccessibleRoomOutlet.layer.borderWidth = 1.0
        self.AccessibleRoomOutlet.clipsToBounds = true
        self.AccessibleRoomOutlet.layer.borderColor = UIColor.darkGray.cgColor
        self.AccessibleRoomOutlet.layer.cornerRadius = 0.5

        self.AllergyFriendOutlet.layer.borderWidth = 1.0
        self.AllergyFriendOutlet.clipsToBounds = true
        self.AllergyFriendOutlet.layer.borderColor = UIColor.darkGray.cgColor
        self.AllergyFriendOutlet.layer.cornerRadius = 0.5
    }

    //MARK:- Create AminnitesView

    func SetAmintiesView(index : Int)
    {
        var tempY : CGFloat! = 0
        
        for view in self.AmiintiesView.subviews {
            view.removeFromSuperview()
        }
        
        if self.IndividualRoomDetailsArray[index].featureArray.count > 0
        {
            for i in 0..<self.IndividualRoomDetailsArray[index].featureArray.count
            {
                let featureTxt = UILabel(frame: CGRect(x: 30, y: tempY, width: 20, height: 25))
                let imgView = UIImageView(frame: CGRect(x: 8, y: tempY, width: 20, height: 25))
                imgView.contentMode = .scaleAspectFit
                imgView.image = UIImage(named: "tickred")
                featureTxt.text = "\((self.IndividualRoomDetailsArray[index].featureArray[i] as! NSDictionary)["title"] ?? "")"
                featureTxt.textColor = .darkgray()
                featureTxt.font = UIFont(name: "Lato-Regular", size: CGFloat(Get_fontSize(size: 14)))
                featureTxt.sizeToFit()
                tempY =  featureTxt.frame.size.height * CGFloat(i + 1)
                AmiintiesView.addSubview(featureTxt)
                AmiintiesView.addSubview(imgView)
            }
            self.AminitesViewHeightConstraint.constant = tempY
        }
        else
        {
            self.AminitesViewHeightConstraint.constant = 0
        }

        
    }
    
    //MARK:- Create Room Feature Price View
    func CreateRoomFeaturePriceView(index : Int)
    {
        for view in self.DynamicFeatureView.subviews {
            view.removeFromSuperview()
        }
        
        let headerLbl = UILabel()
        headerLbl.translatesAutoresizingMaskIntoConstraints = false
        self.DynamicFeatureView.addSubview(headerLbl)
        headerLbl.anchor(top: self.DynamicFeatureView.topAnchor, leading: self.DynamicFeatureView.leadingAnchor, bottom: nil, trailing: self.DynamicFeatureView.trailingAnchor, padding: .init(top: 2.0, left: 8.0, bottom: 0, right: 8.0), size: .init(width: 0, height: 30))
        headerLbl.text = "Food Per Guest Per Night"
        headerLbl.textColor = .black
        headerLbl.font = UIFont(name: "Lato-Bold", size: 14)
        
        for i in 0..<self.IndividualRoomDetailsArray[index].RoomFeaturePrice.count
        {
            let tempButton : UIButton = {
                let tempbutton = UIButton()
                tempbutton.contentMode = .scaleAspectFill
                tempbutton.clipsToBounds = true
                tempbutton.translatesAutoresizingMaskIntoConstraints = false
                return tempbutton
            }()
            
            self.DynamicFeatureView.addSubview(tempButton)
            
            tempButtonArray.append(tempButton)
            
            let nameAndPrice = " " + "\((self.IndividualRoomDetailsArray[index].RoomFeaturePrice[i] as! NSDictionary)["rm_feature_name"] ?? "")" + "   " + "\((self.IndividualRoomDetailsArray[index].RoomFeaturePrice[i] as! NSDictionary)["rm_feature_price"] ?? "")"
            
            tempButton.setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
            tempButton.tag = i
            tempButton.setTitle(nameAndPrice, for: .normal)
            tempButton.titleLabel?.textAlignment = .left
//            tempButton.titleLabel?.textColor = UIColor.black
            tempButton.setTitleColor(.black, for: .normal)
            tempButton.titleLabel?.font = UIFont(name: "Lato-Regular", size: 14)
            
            tempButton.addTarget(self, action: #selector(ChangeButtonImage(sender:)), for: .touchUpInside)
            
            
            
            
            if self.IndividualRoomDetailsArray[index].RoomFeaturePrice.count == 1 {
                tempButton.anchor(top: self.DynamicFeatureView.topAnchor, leading: self.DynamicFeatureView.leadingAnchor, bottom: self.DynamicFeatureView.bottomAnchor, trailing: nil, padding: .init(top: 34, left: 8, bottom: 0, right: 8), size: .init(width: 0, height: 30))
            }
            else if self.IndividualRoomDetailsArray[index].RoomFeaturePrice.count == 2 {
                
                if i == 0 {
                    tempButton.anchor(top: self.DynamicFeatureView.topAnchor, leading: self.DynamicFeatureView.leadingAnchor, bottom: self.DynamicFeatureView.bottomAnchor, trailing: nil, padding: .init(top: 32, left: 8, bottom: 0, right: 8) , size: .init(width: 0, height: 30))
                }
                else {
                    tempButton.anchor(top: self.DynamicFeatureView.topAnchor, leading: tempButtonArray[i - 1].trailingAnchor, bottom: self.DynamicFeatureView.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 8, bottom: 0, right: 8) , size: .init(width: 0, height: 30))
                }
            }
            else {
                
                if i == 0 {
                    tempButton.anchor(top: self.DynamicFeatureView.topAnchor, leading: self.DynamicFeatureView.leadingAnchor, bottom: self.DynamicFeatureView.bottomAnchor, trailing: nil, padding: .init(top: 34, left: 8, bottom: 0, right: 8) , size: .init(width: 0, height: 30))
                }
                else if i == (self.IndividualRoomDetailsArray[index].RoomFeaturePrice.count - 1) {
                    tempButton.anchor(top: self.DynamicFeatureView.topAnchor, leading: tempButtonArray[i - 1].trailingAnchor, bottom: self.DynamicFeatureView.bottomAnchor, trailing: nil, size: .init(width: 0, height: 30))
                }
                else {
                    tempButton.anchor(top: self.DynamicFeatureView.topAnchor, leading: tempButtonArray[i - 1].trailingAnchor, bottom: self.DynamicFeatureView.bottomAnchor, trailing: nil, size: .init(width: 0, height: 30))
                }
                
            }
        }
        
        self.DynamicFeatureViewHeightConstraint.constant = 34 + CGFloat(self.IndividualRoomDetailsArray[index].RoomFeaturePrice.count) * CGFloat(30)
        
    }
    
    //MARK:- Data Inserting in ExtraPrice NSArray
    func CreateExtraPriceArray(index : Int)
    {
        for i in 0..<self.IndividualRoomDetailsArray[index].RoomFeaturePrice.count
        {
            let Str = ""
            self.ExtraPrice.append(Str)
        }
    }
    
    //MARK:- //Create Button Image Change Objective C Function
    @objc func ChangeButtonImage(sender : UIButton)
    {
       if self.tempButtonArray[sender.tag].imageView?.image == UIImage(named: "checkBoxEmpty")
        {
        self.tempButtonArray[sender.tag].setImage(UIImage(named: "checkBoxFilled"), for: .normal)
        }
        else
        {
        self.tempButtonArray[sender.tag].setImage(UIImage(named: "checkBoxEmpty"), for: .normal)
        }
    }
    
    //MARK:- //Checking Extra Feature Price
    
    func ExtraFeaturePriceCalculation(index : Int)
    {
        if indexNumber > self.IndividualRoomDetailsArray[index].RoomFeaturePrice.count
        {
            //dont do anything
        }
        else if indexNumber <= self.IndividualRoomDetailsArray[index].RoomFeaturePrice.count - 1
        {
            for i in 0..<tempButtonArray.count
            {
                if tempButtonArray[i].imageView?.image == UIImage(named: "checkBoxFilled")
                {
                    print("Id==>","\((self.IndividualRoomDetailsArray[index].RoomFeaturePrice[i] as! NSDictionary)["id"] ?? "")")
                    self.ExtraPrice.append("\((self.IndividualRoomDetailsArray[index].RoomFeaturePrice[i] as! NSDictionary)["id"] ?? "")")
                }
                else if tempButtonArray[i].imageView?.image == UIImage(named: "checkBoxEmpty")
                {
                    let EmptyStr = ""
                    self.ExtraPrice.append(EmptyStr)
                }
                else
                {
                    //do nothing
                }
            }
            
            print("ExtraPriceArray",self.ExtraPrice)
        }
        else
        {
            //do nothing
        }
    }
    
   

    //MARK:- //CollectionView delegate and dataSource Methods

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == ProductImageCollectionVie
        {
             return 1
        }
        else
        {
            return 1
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       if collectionView == ProductImageCollectionVie
       {
           return self.IndividualRoomDetailsArray[IndexNo].imageArray.count
        }
        else
       {
           return self.IndividualRoomDetailsArray[IndexNo].imageArray.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == ProductImageCollectionVie
        {
            let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "slider", for: indexPath) as! SliderCollectionCVC
            for i in 0..<self.IndividualRoomDetailsArray[IndexNo].imageArray.count
            {
                let ImagePath = "\((self.IndividualRoomDetailsArray[IndexNo].imageArray[i] as! NSDictionary)["product_image"] ?? "")"
//                print("Imagepath",ImagePath)
                obj.ProductImage.sd_setImage(with: URL(string: ImagePath))
            }
            return obj
        }
        else
        {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bigger", for: indexPath) as! BiggerCVC
            for i in 0..<self.IndividualRoomDetailsArray[IndexNo].imageArray.count
            {
                let ImagePath = "\((self.IndividualRoomDetailsArray[IndexNo].imageArray[i] as! NSDictionary)["product_image"] ?? "")"
//                print("BiggerImagePath",ImagePath)
                cell.ShowiMage.sd_setImage(with: URL(string: ImagePath))
            }
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == ProductImageCollectionVie
        {
            let padding : CGFloat! = 4

            let collectionViewSize = self.ProductImageCollectionVie.frame.size.width - padding

            return CGSize(width: collectionViewSize, height: 180 )
        }
        else
        {
            let collectionViewSize = self.BiggerCollectionView.frame.size.width

            return CGSize(width: collectionViewSize, height: 474 )
        }

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == ProductImageCollectionVie
        {
            self.ShowBiggerImageView.isHidden = false
            self.view.bringSubviewToFront(self.ShowBiggerImageView)

            let toIndex: IndexPath = IndexPath(item: indexPath.item, section: 0)

            self.BiggerCollectionView.scrollToItem(at: toIndex, at: .left, animated: true)
        }
        else
        {

        }
    }

    //MARK:- //PickerView delegate and dataSource Methods

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
      return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == ValuePickerView
        {
            if ShowSmokingValue == true
            {
                return self.SmokingArray.count
            }
            else
            {
                return self.FloorArray.count
            }
        }
        else
        {
           return dataArray.count
        }

    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == ValuePickerView
        {
            if ShowSmokingValue == true
            {
                return self.SmokingArray[row]["key"]
            }
            else
            {
                return self.FloorArray[row]["key"]
            }
        }
        else
        {
           return self.dataArray[row]
        }

    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == ValuePickerView
        {
            if ShowSmokingValue == true
            {
                self.SmokingTxt.text = self.SmokingArray[row]["key"]
                self.ShowPickerView.isHidden = true
                self.SmokingString = self.SmokingArray[row]["value"]
            }
            else
            {
                self.Floortxt.text = self.FloorArray[row]["key"]
                self.ShowPickerView.isHidden = true
                self.FloorString = self.FloorArray[row]["value"]
            }
        }
        else
        {
            self.GuestTxt.text = self.dataArray[row]
            self.ShowDataPickerView.isHidden = true
            self.SelectedGuestString = self.dataArray[row]
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

class SliderCollectionCVC : UICollectionViewCell
{
    @IBOutlet weak var ProductImage: UIImageView!
}

class BiggerCVC : UICollectionViewCell
{

    @IBOutlet weak var ShowiMage: UIImageView!
}


struct TotalRoomDetails
{
    var Smoking : String?
    var Floor : String?
    var GuestNumber : String?
    var AccessibleRoom : String?
    var AllergyFriendlyRoom : String?
    var RoomId : String?
    var FirstName : String?
    var LastName : String?
    var RoomNumbers : String?
//    var ExtraFeaturePrice : NSArray!
    
    init(Smoking : String , Floor : String , GuestNumber : String , AccessibleRoom : String , AllergyFriendlyRoom : String , RoomId : String , FirstName : String , LastName : String , RoomNumbers : String ) {
        self.Smoking = Smoking
        self.Floor = Floor
        self.GuestNumber = GuestNumber
        self.AccessibleRoom = AccessibleRoom
        self.AllergyFriendlyRoom = AllergyFriendlyRoom
        self.RoomId = RoomId
        self.FirstName = FirstName
        self.LastName = LastName
        self.RoomNumbers = RoomNumbers
//        self.ExtraFeaturePrice = ExtraFeaturePrice
        
    }
}
