//
//  HolidayRoomDetailsViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 7/10/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import MapKit
import SVProgressHUD
import SDWebImage

class HolidayRoomDetailsViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource {
    
    
    @IBOutlet weak var ProductSellerImageView: UIImageView!
    
    @IBOutlet weak var ProductSellerName: UILabel!
    
    @IBOutlet weak var ProductSellerAccountType: UILabel!
    
    @IBOutlet weak var ProductSellerAccountImage: UIImageView!
    
    @IBOutlet weak var FollowBtnOutlet: UIButton!
    
    @IBAction func FollowBtn(_ sender: UIButton) {
        
        self.FollowBtnOutlet.isSelected = !self.FollowBtnOutlet.isSelected

        if (self.FollowBtnOutlet.titleLabel!.text?.elementsEqual("Follow"))! {
            
            self.followSeller()

            self.FollowBtnOutlet.setTitle("Unfollow", for: .normal)
        }
        else {
            self.unfollowSeller()

            self.FollowBtnOutlet.setTitle("Follow", for: .normal)
        }
    }
    @IBOutlet weak var PositiveFeedBacK: UILabel!
    
    @IBOutlet weak var MemberSince: UILabel!
    
    @IBOutlet weak var RatingNumbeR: UILabel!
    
    @IBOutlet weak var ProductSellerLocation: UILabel!
    
    @IBOutlet weak var ProductSellerContact: UILabel!
    
    @IBAction func SendMessageBtn(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func ViewAccountBtn(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBOutlet weak var SendMessageOutlet: UIButton!
    
    @IBOutlet weak var ViewAccountOutlet: UIButton!
    
    
    func setAboutSectionData()
    {
        self.ProductSellerName.text = "\(self.SellerInfoDict["display_name"] ?? "")"
        
        if "\(self.SellerInfoDict["seller_type"] ?? "")".elementsEqual("B")
        {
            self.ProductSellerAccountType.text = "Business Account"
        }
        else
        {
            self.ProductSellerAccountType.text = "Private Account"
        }
        
//        self.ProductSellerImageView.
        self.PositiveFeedBacK.text = "\(self.SellerInfoDict["positive_feedback"] ?? "")" + "% Positive Feedback"
        
        self.MemberSince.text = "Member Since : " + "\(self.SellerInfoDict["member_since"] ?? "")"
        
        self.RatingNumbeR.text = "\(self.SellerInfoDict["total_rating"] ?? "")"
        
        self.ProductSellerContact.text = "Contact : " + "\(self.SellerInfoDict["mobile"] ?? "")"
        
        self.ProductSellerLocation.text = "Location : " + "\(self.SellerInfoDict["location"] ?? "")"
        
        
        if langID.elementsEqual("AR")
           {
                self.ProductSellerAccountImage.image = UIImage(named: "button-2")
            }
            else
            {
                self.ProductSellerAccountImage.image = UIImage(named: "button-1")
            }
        
    }
    
    
    @IBOutlet weak var RoomDetailsAboutStackView: UIStackView!
    
    
    @IBOutlet weak var InCheckDate: UILabel!
    @IBOutlet weak var OutCheckDate: UILabel!
    
    
    @IBAction func reserveBtn(_ sender: UIButton) {
        
        if self.USERID?.isEmpty == true
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "warning", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "please_log_in_and_try_again_later", comment: ""))
        }
        else
        {
            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "advancedpage") as! HolidayRoomDetailsAdvancedPageViewController
            navigate.IndividualRoomDetailsArray = roomDetailsDataArray
            self.navigationController?.pushViewController(navigate, animated: true)
        }
    }
    
    
    @IBOutlet weak var reserveBtnOutlet: UIButton!
    
    //
    var roomDetailsDataArray = [roomDetailsDataFormat]()
    
    // MARK:- // Create Room Details Data Array
    
    func createRoomDetailsDataArray() {
        
        for i in 0..<self.RoomDetailsArray.count {
            
            self.roomDetailsDataArray.append(roomDetailsDataFormat(roomId: "\((self.RoomDetailsArray[i] as! NSDictionary)["room_id"] ?? "")",roomName: "\((self.RoomDetailsArray[i] as! NSDictionary)["room_name"] ?? "")", roomDescription: "\((self.RoomDetailsArray[i] as! NSDictionary)["room_details"] ?? "")", imageArray: ((self.RoomDetailsArray[i] as! NSDictionary)["product_img"] as! NSArray), featureArray: ((self.RoomDetailsArray[i] as! NSDictionary)["aminites"] as! NSArray), roomPrice: ("\((self.RoomDetailsArray[i] as! NSDictionary)["currency_symbol"] ?? "")" + "\((self.RoomDetailsArray[i] as! NSDictionary)["price_per_night"] ?? "")"), roomOnlyPrice: "\((self.RoomDetailsArray[i] as! NSDictionary)["price_per_night"] ?? "")", specialOfferPrice: ("\((self.RoomDetailsArray[i] as! NSDictionary)["currency_symbol"] ?? "")" + "\((self.RoomDetailsArray[i] as! NSDictionary)["special_offer_price"] ?? "")"), specialOfferOnlyPrice: "\((self.RoomDetailsArray[i] as! NSDictionary)["special_offer_price"] ?? "")", roomNumber: "0", showFeatureCount: 4, RoomFeaturePrice: ((self.RoomDetailsArray[i] as! NSDictionary)["room_feature_price"] as! NSArray), roomtaxAndFees: "\((self.RoomDetailsArray[i] as! NSDictionary)["tax_and_fee"] ?? "")", roomTotalPrice: "0.0", roomTaxPrice: "0.0", CurrencySymbol: "\((self.RoomDetailsArray[i] as! NSDictionary)["currency_symbol"] ?? "")"))
        }
    }
    
    
    
    var testArray = [testDict(testBool: false) , testDict(testBool: false)]
    
    var SelectedRoomArray : NSMutableArray! = NSMutableArray()
    

    @IBOutlet weak var totalRooms: UILabel!
    @IBOutlet weak var totalRoomPrice: UILabel!
    @IBOutlet weak var CloseByLbl: UILabel!
    @IBOutlet weak var CloseByString: UILabel!
    

    var RoomButtonArray = [UIButton]()
    var RoomPriceArray = [String]()
    var RoomAminitesViewHeightArray = [CGFloat]()
    //@IBOutlet weak var mapView: GMSMapView!
    


    var RoomIndex : Int! = 0
    var RoomDetailsArray = NSMutableArray()
    var RoomReviewDictionary : NSDictionary! = NSDictionary()
    var ReviewArray : NSMutableArray! = NSMutableArray()
    var DetailsArray : NSMutableArray! = NSMutableArray()
    var AminitesArray : NSArray! = NSArray()
    var fetchDetails = FetchData()
    var SectionArray = ["Rooms","About","Review & Rating","Location"]
    var roomsNoArray = ["0","1","2","3","4","5","6","7","8","9","10"]
    var roomNumberArray : NSMutableArray! = NSMutableArray()
    var InfoDict : NSDictionary!


    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var ReserveView: UIView!
    @IBOutlet weak var totalPriceView: UIView!
    @IBOutlet weak var ButtonScrollContentView: UIView!
    @IBOutlet weak var ButtonsScrollView: UIScrollView!
    @IBOutlet weak var roomNoPickerView: UIPickerView!
    @IBOutlet weak var ShowPickerView: UIView!
    @IBOutlet weak var RoomDetailsStackView: UIStackView!
    @IBOutlet weak var RoomDetailsAboutSection: UIView!
    @IBOutlet weak var RoomDetailsLocationSection: UIView!
    @IBOutlet weak var RoomsReviewtableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var RoomsReviewtableView: UITableView!
    @IBOutlet weak var RoomDetailsTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var SliderView: UIView!
    @IBOutlet weak var HotelCheckingView: UIView!
    @IBOutlet weak var HotelImagesCollectionView: UICollectionView!
    @IBOutlet weak var RoomDetailsTableview: UITableView!
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var MainScroll: UIScrollView!
    
    var SellerInfoDict : NSDictionary! = NSDictionary()

    var DetailsStructureArray : [FetchData]? = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createRoomDetailsDataArray()
        
//        self.CreateButtons()
        self.SetLayer()
        self.ReviewArray = self.RoomReviewDictionary["review_list"] as? NSMutableArray
        self.Setdata()
        self.SetStructureData()
        print("ProductIDDetails",DetailsStructureArray!)
        self.ShowPickerView.isHidden = true
        self.RoomDetailsAboutStackView.isHidden = true
        self.RoomsReviewtableView.isHidden = true
        self.RoomDetailsLocationSection.isHidden = true
        print("RoomReviewArray",ReviewArray!)
        self.HolidayLocationSection()
        self.roomNoPickerView.delegate = self
        self.roomNoPickerView.dataSource = self
        self.roomNoPickerView.reloadAllComponents()
        self.setAboutSectionData()


        // Do any additional setup after loading the view.
        
        
    }

    //MARK:- //Set layer
    func SetLayer()
    {
        self.priceView.layer.borderWidth = 2
        self.priceView.layer.cornerRadius = 0.5
        self.priceView.clipsToBounds = true
        self.priceView.layer.borderColor = UIColor.yellow.cgColor
        self.ReserveView.backgroundColor = .HolidayGreen()
        self.HotelImagesCollectionView.showsVerticalScrollIndicator = false
        self.HotelImagesCollectionView.showsHorizontalScrollIndicator = false
    }
    
    //MARK:- // Follow Seller

    func followSeller()
    {
        var parameters : String = ""
        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        let SellerID = "\(self.SellerInfoDict["seller_id"] ?? "")"

        parameters = "user_id=\(userID!)&buss_seller_id=\(SellerID)&lang_id=\(langID!)"

        self.CallAPI(urlString: "app_add_to_favourite", param: parameters) {

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

                self.ShowAlertMessage(title: "Success", message: "\(self.globalJson["message"] ?? "")")

            }
        }
    }

    // MARK:- // JSON Post method to Unfollow Seller


    func unfollowSeller() {

        let userID = UserDefaults.standard.string(forKey: "userID")
        var langID = UserDefaults.standard.string(forKey: "langID")
        let SellerID = "\(self.SellerInfoDict["seller_id"] ?? "")"

        let parameters = "user_id=\(userID!)&lang_id=\(self.langID!)&remove_id=\(SellerID)"

        self.CallAPI(urlString: "app_remove_my_favourite", param: parameters) {

            DispatchQueue.main.async {

                self.globalDispatchgroup.leave()

                SVProgressHUD.dismiss()

                self.ShowAlertMessage(title: "Success", message: "\(self.globalJson["message"] ?? "")")

            }
        }

    }


    //MARK :- //Set Data To Structure
    func SetStructureData()
    {
        self.DetailsArray = self.RoomDetailsArray.mutableCopy() as? NSMutableArray
        if self.DetailsArray.count > 0
        {
            for i in 0..<self.DetailsArray.count
            {
              let proID = "\((self.DetailsArray[i] as! NSDictionary)["room_id"]!)"
              let roomPrice = "\((self.DetailsArray[i] as! NSDictionary)["price_per_night"]!)"
              let roomNo = "0"
              let fDetails = FetchData(ProductID: proID, RoomPrice: roomPrice, RoomNo: roomNo)
              self.DetailsStructureArray?.append(fDetails)
//              fetchDetails.ProductID = proID
//              fetchDetails.RoomPrice = roomPrice
//              fetchDetails.RoomNo = roomNo

            }

        }
    }

    //MARK:- //View Will Appear Method

    override func viewWillAppear(_ animated: Bool) {
        self.setTableviewHeight(tableview: self.RoomDetailsTableview, heightConstraint: self.RoomDetailsTableViewHeightConstraint)
//        self.setTableviewHeight(tableview: self.RoomsReviewtableView, heightConstraint: self.RoomsReviewtableViewHeightConstraint)
        
        self.totalCalculation()
        
        //self.RoomDetailsTableViewHeightConstraint =

    }

    //MARK:- //Collectionview delegate and datasource methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.SectionArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "images", for: indexPath) as! HotelImagesCVC
        obj.SectionName.sizeToFit()
        obj.SectionName.text = "\(self.SectionArray[indexPath.row])"
        obj.SectionName.textColor = .gray
        return obj
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size: CGSize = self.SectionArray[indexPath.row].size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0)])
        return CGSize(width: size.width + 45.0, height: 30)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = collectionView.dequeueReusableCell(withReuseIdentifier: "images", for: indexPath) as! HotelImagesCVC
        obj.isSelected = true
        
        if indexPath.row == 0
        {
          self.RoomDetailsTableview.isHidden = false
          self.view.bringSubviewToFront(RoomDetailsTableview)
          self.RoomDetailsAboutStackView.isHidden = true
          self.RoomsReviewtableView.isHidden = true
          self.RoomDetailsLocationSection.isHidden = true
        }
        else if indexPath.row == 1
        {
            self.RoomDetailsTableview.isHidden = true
            self.RoomDetailsAboutStackView.isHidden = false
            self.view.bringSubviewToFront(RoomDetailsAboutStackView)
            self.RoomsReviewtableView.isHidden = true
            self.RoomDetailsLocationSection.isHidden = true
        }
        else if indexPath.row == 2
        {
            self.RoomDetailsTableview.isHidden = true
            self.RoomDetailsAboutStackView.isHidden = true
            self.RoomsReviewtableView.isHidden = false
            self.view.bringSubviewToFront(RoomsReviewtableView)
            self.RoomDetailsLocationSection.isHidden = true
        }
        else
        {
            self.RoomDetailsTableview.isHidden = true
            self.RoomDetailsAboutStackView.isHidden = true
            self.RoomsReviewtableView.isHidden = true
            self.RoomDetailsLocationSection.isHidden = false
            self.view.bringSubviewToFront(RoomDetailsLocationSection)
        }
    }
    //MARK:- // Get Details From API

    func Setdata()
    {
        let imageurl = "\(self.SellerInfoDict["seller_image"] ?? "")"
        
        self.ProductSellerImageView.sd_setImage(with: URL(string: imageurl))
        
        self.InCheckDate.text = holidaySearchParameters.shared.checkInDate!
        
        self.OutCheckDate.text = holidaySearchParameters.shared.checkOutDate!
        
        self.HotelImagesCollectionView.delegate = self
        
        self.HotelImagesCollectionView.dataSource = self
        
        self.HotelImagesCollectionView.reloadData()

        self.RoomDetailsTableview.delegate = self
        
        self.RoomDetailsTableview.dataSource = self
        
        self.RoomDetailsTableview.reloadData()
        
        if self.ReviewArray.count > 0
        {
            self.RoomsReviewtableView.delegate = self
            
            self.RoomsReviewtableView.dataSource = self
            
            self.RoomsReviewtableView.reloadData()
        }
        else
        {

        }
    }

    //MARK:- //TableView Delegate and dataSource Methods

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.RoomDetailsTableview
        {
            //return self.RoomDetailsArray.count
            return self.roomDetailsDataArray.count
        }
        else
        {
           return self.ReviewArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.RoomDetailsTableview
        {
            let obj = tableView.dequeueReusableCell(withIdentifier: "roomdetails") as! RoomDetailsTVC
            
            
            for view in obj.AmintiesView.subviews {
                view.removeFromSuperview()
            }
            
            for view in obj.ScrollContentView.subviews {
                view.removeFromSuperview()
            }
            
            
            obj.RoomName.text = "\(roomDetailsDataArray[indexPath.row].roomName ?? "")"
            obj.RoomDescription.text = "\(roomDetailsDataArray[indexPath.row].roomDescription ?? "")"
            
          
            
            
            // Creating Image Scroll
            
            obj.ScrollContentViewWidthConstraint.constant = (self.FullWidth - 26) * CGFloat(self.roomDetailsDataArray[indexPath.row].imageArray.count)
            
            var imageviewArray = [UIImageView]()
            
            for i in 0..<self.roomDetailsDataArray[indexPath.row].imageArray.count {
                
                let tempImageview : UIImageView = {
                    let tempimageview = UIImageView()
                    tempimageview.contentMode = .scaleAspectFill
                    tempimageview.clipsToBounds = true
                    tempimageview.translatesAutoresizingMaskIntoConstraints = false
                    return tempimageview
                }()
                
                obj.ScrollContentView.addSubview(tempImageview)
                
                imageviewArray.append(tempImageview)
                
                tempImageview.sd_setImage(with: URL(string: "\((self.roomDetailsDataArray[indexPath.row].imageArray[i] as! NSDictionary)["product_image"] ?? "")"))
                
                
                if self.roomDetailsDataArray[indexPath.row].imageArray.count == 1 {
                    tempImageview.anchor(top: obj.ScrollContentView.topAnchor, leading: obj.ScrollContentView.leadingAnchor, bottom: obj.ScrollContentView.bottomAnchor, trailing: obj.ScrollContentView.trailingAnchor, size: .init(width: (self.FullWidth - 26), height: 0))
                }
                else if self.roomDetailsDataArray[indexPath.row].imageArray.count == 2 {
                    
                    if i == 0 {
                        
                        tempImageview.anchor(top: obj.ScrollContentView.topAnchor, leading: obj.ScrollContentView.leadingAnchor, bottom: obj.ScrollContentView.bottomAnchor, trailing: nil, size: .init(width: (self.FullWidth - 26), height: 0))
                    }
                    else {
                        
                        tempImageview.anchor(top: obj.ScrollContentView.topAnchor, leading: imageviewArray[i - 1].trailingAnchor, bottom: obj.ScrollContentView.bottomAnchor, trailing: obj.ScrollContentView.trailingAnchor, size: .init(width: (self.FullWidth - 26), height: 0))
                    }
                }
                else {
                    
                    if i == 0 {
                        tempImageview.anchor(top: obj.ScrollContentView.topAnchor, leading: obj.ScrollContentView.leadingAnchor, bottom: obj.ScrollContentView.bottomAnchor, trailing: nil, size: .init(width: (self.FullWidth - 26), height: 0))
                    }
                        
                    else if i == (self.roomDetailsDataArray[indexPath.row].imageArray.count - 1) {
                        tempImageview.anchor(top: obj.ScrollContentView.topAnchor, leading: imageviewArray[i - 1].trailingAnchor, bottom: obj.ScrollContentView.bottomAnchor, trailing: obj.ScrollContentView.trailingAnchor, size: .init(width: (self.FullWidth - 26), height: 0))
                    }
                    else {
                        tempImageview.anchor(top: obj.ScrollContentView.topAnchor, leading: imageviewArray[i - 1].trailingAnchor, bottom: obj.ScrollContentView.bottomAnchor, trailing: nil, size: .init(width: (self.FullWidth - 26), height: 0))
                    }
                    
                }
            }
            
            
            
            
            
            // Creating Feature View
            var featureViewArray = [UIView]()
            
            for i in 0..<self.roomDetailsDataArray[indexPath.row].showFeatureCount! {
                
                let tempFeatureView : UIView = {
                   let tempfeatureview = UIView()
                    //tempfeatureview.backgroundColor = .red
                    tempfeatureview.translatesAutoresizingMaskIntoConstraints = false
                    return tempfeatureview
                }()
                
                let tickImage : UIImageView = {
                    let tickimage = UIImageView()
                    tickimage.contentMode = .scaleAspectFit
                    tickimage.translatesAutoresizingMaskIntoConstraints = false
                    return tickimage
                }()
                
                let featureLBL : UILabel = {
                    let featurelbl = UILabel()
                    featurelbl.font = UIFont(name: obj.RoomDescription.font.fontName, size: CGFloat(self.Get_fontSize(size: 12)))
                    //featurelbl.backgroundColor = .green
                    featurelbl.translatesAutoresizingMaskIntoConstraints = false
                    return featurelbl
                }()
                
                obj.AmintiesView.addSubview(tempFeatureView)
                tempFeatureView.addSubview(tickImage)
                tempFeatureView.addSubview(featureLBL)
                
                featureViewArray.append(tempFeatureView)
                
                tickImage.image = UIImage(named: "tickred")
                
                if self.roomDetailsDataArray[indexPath.row].featureArray.count >= self.roomDetailsDataArray[indexPath.row].showFeatureCount!
                {
                    if self.roomDetailsDataArray[indexPath.row].featureArray.count > 0
                    {
                        featureLBL.text =  "\((self.roomDetailsDataArray[indexPath.row].featureArray[i] as! NSDictionary)["title"] ?? "")"
                    }
                    else
                    {
                        featureLBL.text =  "No data found"
                    }
                }
                else
                {
                    featureLBL.text =  "No data found"
                }
                
                
                
                
                tickImage.anchor(top: tempFeatureView.topAnchor, leading: tempFeatureView.leadingAnchor, bottom: tempFeatureView.bottomAnchor, trailing: nil, size: .init(width: 20, height: 25))
                
                featureLBL.anchor(top: tempFeatureView.topAnchor, leading: tickImage.trailingAnchor, bottom: tempFeatureView.bottomAnchor, trailing: tempFeatureView.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 0, right: 0), size: .init(width: 0, height: 25))
                
                
                if i >  (self.roomDetailsDataArray[indexPath.row].showFeatureCount! - 1){
                    tempFeatureView.isHidden = true
                }
                else {
                    tempFeatureView.isHidden = false
                }
                
                
                
                if i == 0 {
                    
                    tempFeatureView.anchor(top: obj.AmintiesView.topAnchor, leading: obj.AmintiesView.leadingAnchor, bottom: nil, trailing: obj.AmintiesView.trailingAnchor, size: .init(width: 0, height: 25))
                    
                }
                else if i == (self.roomDetailsDataArray[indexPath.row].featureArray.count - 1) {
                    tempFeatureView.anchor(top: featureViewArray[i-1].bottomAnchor, leading: obj.AmintiesView.leadingAnchor, bottom: obj.AmintiesView.bottomAnchor, trailing: obj.AmintiesView.trailingAnchor, size: .init(width: 0, height: 25))
                }
                else {
                    tempFeatureView.anchor(top: featureViewArray[i-1].bottomAnchor, leading: obj.AmintiesView.leadingAnchor, bottom: nil, trailing: obj.AmintiesView.trailingAnchor, size: .init(width: 0, height: 25))
                }
            }
            
            
            obj.AminitiesHeightConstraint.constant = 100//25 * CGFloat(self.roomDetailsDataArray[indexPath.row].showFeatureCount!)
            
            print(obj.AminitiesHeightConstraint.constant)
            
            
            
            // PRice CHecking
            if "\(self.roomDetailsDataArray[indexPath.row].specialOfferPrice ?? "")".elementsEqual("0") {
                obj.PerNightPrice.text = "\(self.roomDetailsDataArray[indexPath.row].roomPrice ?? "")"
                obj.PerNightPrice.textColor = .seaGreen()
                
                obj.ReservePrice.isHidden = true
            }
            else {
                
                obj.PerNightPrice.text = "\(self.roomDetailsDataArray[indexPath.row].specialOfferPrice ?? "")"
                obj.PerNightPrice.textColor = .red
                
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(self.roomDetailsDataArray[indexPath.row].roomPrice ?? "")")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                
                obj.ReservePrice.attributedText = attributeString
                obj.ReservePrice.textColor = .seaGreen()
                obj.ReservePrice.isHidden = false
                
            }
            
            obj.RoomNo.setTitle("\(self.roomDetailsDataArray[indexPath.row].roomNumber ?? "")", for: .normal)
            
            
            obj.ViewDetailsBtn.tag = indexPath.row
            
            obj.ViewDetailsBtn.addTarget(self, action: #selector(ViewMoreDetails(sender:)), for: .touchUpInside)
            
            
            
            //Room no  button
            obj.RoomNo.addTarget(self, action: #selector(tapOnPicker(sender:)), for: .touchUpInside)
            obj.RoomNo.tag = indexPath.row
            obj.selectionStyle = .none
            obj.ContentView.layer.cornerRadius = 8.0
            return obj
        }
        else if tableView == self.RoomsReviewtableView
        {
            let obj = tableView.dequeueReusableCell(withIdentifier: "review") as! HolidayListingDetailReviewTableViewCell
            obj.ReviewerName.text = "\((self.ReviewArray[indexPath.row] as! NSDictionary)["name"]!)"
            obj.ReviewerDate.text = "\((self.ReviewArray[indexPath.row] as! NSDictionary)["date"]!)"
            obj.ReviewerComment.text = "\((self.ReviewArray[indexPath.row] as! NSDictionary)["comment"]!)"
            obj.TotalReviews.text = "\((self.ReviewArray[indexPath.row] as! NSDictionary)["total_rating"]!)"
            obj.selectionStyle = .none
            return obj
        }
        else
        {
            let cell:UITableViewCell?  = tableView.dequeueReusableCell(withIdentifier: "cell")
            return cell!
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func setTableviewHeight(tableview: UITableView, heightConstraint: NSLayoutConstraint)
    {
        var setheight: CGFloat  = 0
        tableview.frame.size.height = 3000

        for cell in tableview.visibleCells {
            setheight += cell.bounds.height
        }
        heightConstraint.constant = CGFloat(setheight)
        print("height",setheight)
    }
    
    //MARK:- //PickerView delegate and dataSource MEthods

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return roomsNoArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (roomsNoArray[row] )
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.globalDispatchgroup.enter()
        
        self.roomDetailsDataArray[RoomIndex].roomNumber = "\(self.roomsNoArray[row])"
        
        self.ShowPickerView.isHidden = true
        
        self.RoomDetailsTableview.reloadRows(at: [NSIndexPath(row: RoomIndex, section: 0) as IndexPath] , with: UITableView.RowAnimation.fade)
        
        self.RoomDetailsTableview.layoutIfNeeded()
        
        self.totalCalculation()
        
        self.globalDispatchgroup.leave()
        
        DispatchQueue.main.async {
            
            var tempValue : Double = 0.0
            
            var individualRoomPrice = 0.0
            
            var individualRoomTax = 0.0
            
            if (self.roomDetailsDataArray[self.RoomIndex].specialOfferOnlyPrice?.elementsEqual(""))!
            {
                tempValue = Double("\(self.roomDetailsDataArray[self.RoomIndex].roomOnlyPrice! as NSString)")!
            }
            else
            {
                tempValue = Double("\(self.roomDetailsDataArray[self.RoomIndex].specialOfferOnlyPrice! as NSString)")!
            }
            
            print("tempValue",tempValue)
            
            individualRoomPrice = (tempValue * Double(holidaySearchParameters.shared.DateDifference) *
                Double("\(self.roomDetailsDataArray[self.RoomIndex].roomNumber! as NSString)")!)
            
            individualRoomTax = individualRoomPrice/100.0
            
            print("IndividualPrice",individualRoomPrice)
            
            self.roomDetailsDataArray[self.RoomIndex].roomTotalPrice = String(individualRoomPrice)
            
            self.roomDetailsDataArray[self.RoomIndex].roomTaxPrice = String(individualRoomTax)
        }
    }
    
    //MARK:- Calculate Total Rooms and Total Price
    
    func TotalCalculations()
    {
        var tempCount : Int! = 0
        
        for i in 0..<self.roomDetailsDataArray.count {
            
            tempCount = Int("\(self.roomDetailsDataArray[i].roomNumber!)")! + tempCount
            
        }
        self.totalRooms.text = "Rooms: " + "\(tempCount!)"
    }
    
    

    //MARK:- Objective C Function
    @objc func tapOnPicker (sender : UIButton)
    {
        self.ShowPickerView.isHidden = false
        RoomIndex = sender.tag
    }

    //MARK:- Buttons ScrollView

//    func CreateButtons()
//    {
//        var tempOrigin : CGFloat! = 20
//        var TotalWidth : CGFloat! = 0
//        for i in (0..<self.SectionArray.count)
//        {
//            let btnName = UIButton(frame: CGRect(x: tempOrigin, y: 5, width: (20/320)*self.FullWidth, height: (34*FullHeight)))
//            btnName.setTitle(self.SectionArray[i], for: .normal)
//            btnName.titleLabel?.font = UIFont(name: "Lato-Regular", size: 16)
//            btnName.setTitleColor(.black, for: .normal)
//            btnName.addTarget(self, action: #selector(SetViews(sender:)), for: .touchUpInside)
//            btnName.sizeToFit()
//            btnName.backgroundColor = .white
//            btnName.tag = i
//            tempOrigin = tempOrigin + btnName.frame.size.width + (20/320)*self.FullWidth
//            print("tempOrigin",tempOrigin)
//            print("TotalWidth",TotalWidth)
//            ButtonsScrollView.addSubview(btnName)
//        }
//        TotalWidth = TotalWidth + tempOrigin
//        ButtonsScrollView.showsVerticalScrollIndicator = false
//        ButtonsScrollView.contentSize.width = TotalWidth
//    }

    //MARK:- //Set Views According To Section Button

    @objc func SetViews(sender : UIButton)
    {
        switch sender.tag {
        case 0:
            self.RoomDetailsTableview.isHidden = false
            self.view.bringSubviewToFront(RoomDetailsTableview)
            self.RoomDetailsAboutStackView.isHidden = true
            self.RoomsReviewtableView.isHidden = true
            self.RoomDetailsLocationSection.isHidden = true
        case 1 :
            self.RoomDetailsTableview.isHidden = true
            self.RoomDetailsAboutStackView.isHidden = false
            self.view.bringSubviewToFront(RoomDetailsAboutStackView)
            self.RoomsReviewtableView.isHidden = true
            self.RoomDetailsLocationSection.isHidden = true
        case 2 :
            self.RoomDetailsTableview.isHidden = true
            self.RoomDetailsAboutStackView.isHidden = true
            self.RoomsReviewtableView.isHidden = false
            self.view.bringSubviewToFront(RoomsReviewtableView)
            self.RoomDetailsLocationSection.isHidden = true
        case 3 :
            self.RoomDetailsTableview.isHidden = true
            self.RoomDetailsAboutStackView.isHidden = true
            self.RoomsReviewtableView.isHidden = true
            self.RoomDetailsLocationSection.isHidden = false
            self.view.bringSubviewToFront(RoomDetailsLocationSection)
        default:
            self.RoomDetailsTableview.isHidden = false
            self.view.bringSubviewToFront(RoomDetailsTableview)
            self.RoomDetailsAboutStackView.isHidden = true
            self.RoomsReviewtableView.isHidden = true
            self.RoomDetailsLocationSection.isHidden = true
        }

    }
    //MARK:- //ViewMoreDetails Button
    @objc func ViewMoreDetails(sender : UIButton)
    {
        
        self.roomDetailsDataArray[sender.tag].showFeatureCount = self.roomDetailsDataArray[sender.tag].featureArray.count
        
        self.RoomDetailsTableview.reloadRows(at: [NSIndexPath(row: sender.tag, section: 0) as IndexPath] , with: UITableView.RowAnimation.fade)
        
        self.setTableviewHeight(tableview: self.RoomDetailsTableview, heightConstraint: self.RoomDetailsTableViewHeightConstraint)
        self.RoomDetailsTableview.layoutIfNeeded()
        
        
    }
    
    func HolidayLocationSection()
    {
      self.CloseByString.text = "\(self.InfoDict["close_by"]!)"
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK:- //Total Price Calculation
    func totalCalculation()
    {
        var sum = 0.0
        
        var totalRoom = 0
        
        for item in self.roomDetailsDataArray {
            
            var tempValue : Double = 0.0
            
//            var individualTaxPrice = 0.0
            
            if (item.specialOfferOnlyPrice?.elementsEqual(""))!
            {
                tempValue = Double("\(item.roomOnlyPrice! as NSString)")!
            }
            else
            {
                tempValue = Double("\(item.specialOfferOnlyPrice! as NSString)")!
            }
            
//            individualTaxPrice = (tempValue * Double(holidaySearchParameters.shared.DateDifference) * Double("\(item.roomNumber! as NSString)")!)
//
//            print("IndividualTaxPrice",individualTaxPrice)
//
//            taxArray.append(TaxCalculate(taxPrice: String(individualTaxPrice)))
            
            sum += tempValue * Double("\(item.roomNumber! as NSString)")!
            totalRoom += Int("\(item.roomNumber! as NSString)")!
        }
        
//        print("taxarray",taxArray.description)
        
        let pricetotal = sum
        
        holidaySearchParameters.shared.setRoomCount(roomcount: String(totalRoom))
        holidaySearchParameters.shared.setRoomTotalPrice(totalPrice: "\(self.roomDetailsDataArray[0].CurrencySymbol ?? "")" + String(pricetotal))
        
        self.totalRooms.text = "Room : " + "\(totalRoom)"
        self.totalRoomPrice.text = "Room Price : " + "\(self.roomDetailsDataArray[0].CurrencySymbol ?? "")" + "\(pricetotal)"
       
    }
    
    

}
class HotelImagesCVC : UICollectionViewCell
{

    @IBOutlet weak var SectionName: UILabel!
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
               SectionName.textColor = .black
            }
            else
            {
                SectionName.textColor = .gray
            }
        }
    }

}

class RoomDetailsTVC : UITableViewCell
{
    @IBOutlet weak var ReservePrice: UILabel!
    @IBOutlet weak var RoomNo: UIButton!
    @IBOutlet weak var RoomsNOLbl: UILabel!
    @IBOutlet weak var NoPersonsImg: UIImageView!
    @IBOutlet weak var SleepLbl: UILabel!
    @IBOutlet weak var PerNightPrice: UILabel!
    @IBOutlet weak var PerNightLbl: UILabel!
    @IBOutlet weak var ViewDetailsBtn: UIButton!
    @IBOutlet weak var AmintiesView: UIView!
    @IBOutlet weak var FollowingLbl: UILabel!
    @IBOutlet weak var RoomDescription: UILabel!
    @IBOutlet weak var RoomName: UILabel!
    @IBOutlet weak var ScrollContentView: UIView!
    @IBOutlet weak var ViewScrollView: UIScrollView!
    @IBOutlet weak var AminitiesHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ScrollContentViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var ContentView: UIView!
    
    
}

struct FetchData {
    var ProductID : String!
    var RoomPrice : String!
    var RoomNo : String!

//    init(ProductID : String? , RoomPrice : String? , RoomNo : String?) {
//        self.ProductID = ProductID
//        self.RoomPrice = RoomPrice
//        self.RoomNo = RoomNo
    }

class testDict {
    
    var testBool : Bool
    
    init(testBool: Bool) {
        self.testBool = testBool
    }
    
}




// MARK:- // Room Details Data Format

class roomDetailsDataFormat {
    
    var roomId : String?
    var roomName : String?
    var roomDescription : String?
    var featureArray : NSArray!
    var imageArray : NSArray!
    var roomPrice : String?
    var roomOnlyPrice : String?
    var specialOfferPrice : String?
    var specialOfferOnlyPrice : String?
    var roomNumber : String?
    var showFeatureCount : Int?
    var RoomFeaturePrice : NSArray!
    var roomtaxAndFees : String?
    var roomTotalPrice : String?
    var roomTaxPrice : String?
    var CurrencySymbol : String?
    
    
    
    init(roomId : String , roomName: String, roomDescription: String, imageArray: NSArray, featureArray: NSArray, roomPrice: String, roomOnlyPrice : String, specialOfferPrice: String, specialOfferOnlyPrice : String ,roomNumber: String, showFeatureCount: Int , RoomFeaturePrice : NSArray , roomtaxAndFees : String , roomTotalPrice : String , roomTaxPrice : String , CurrencySymbol : String ) {
        
        self.roomId = roomId
        self.roomName = roomName
        self.roomDescription = roomDescription
        self.imageArray = imageArray
        self.featureArray = featureArray
        self.roomPrice = roomPrice
        self.roomOnlyPrice = roomOnlyPrice
        self.specialOfferPrice = specialOfferPrice
        self.specialOfferOnlyPrice = specialOfferOnlyPrice
        self.roomNumber = roomNumber
        self.showFeatureCount = showFeatureCount
        self.RoomFeaturePrice = RoomFeaturePrice
        self.roomtaxAndFees = roomtaxAndFees
        self.roomTotalPrice = roomTotalPrice
        self.roomTaxPrice = roomTaxPrice
        self.CurrencySymbol = CurrencySymbol
        
    }
    
}

struct TaxCalculate
{
    var taxPrice : String?
    var roomPrice : String?
    
    init(taxPrice : String , roomPrice : String)
    {
        self.taxPrice = taxPrice
        self.roomPrice = roomPrice
    }
}









//$tax_and_fee       = $product_info[ $key ][ 'tax_and_fee' ];
//$price             = $reserve_price > 0 ? $reserve_price : $start_price;
//$room_price        = $days * $room_quantity[ $key ] * $price;
//$total_price       = $total_price + $room_price;
////$room_tax_price    = ($room_price * $tax_and_fee / 100);
//$total_tax_price   = $total_tax_price + $room_tax_price;
//$tot_room_quantity = $tot_room_quantity + $room_quantity[ $key ];
//$tot_rm_tax_extra_fee = $tot_rm_tax_extra_fee + ($rm_tax_extra_fee * $tax_and_fee / 100);

