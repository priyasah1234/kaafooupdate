//
//  HolidayListingDetailReviewTableViewCell.swift
//  Kaafoo
//
//  Created by IOS-1 on 29/04/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class HolidayListingDetailReviewTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var DateLbl: UILabel!
    @IBOutlet weak var ReviewerName: UILabel!
    @IBOutlet weak var ReviewerDate: UILabel!
    @IBOutlet weak var CommentLbl: UILabel!
    @IBOutlet weak var ReviewerComment: UILabel!
    @IBOutlet weak var RatingLbl: UILabel!
    @IBOutlet weak var TotalRatingLbl: UILabel!
    @IBOutlet weak var TotalReviews: UILabel!
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
