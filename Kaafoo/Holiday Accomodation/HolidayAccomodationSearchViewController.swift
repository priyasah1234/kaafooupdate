//
//  HolidayAccomodationSearchViewController.swift
//  Kaafoo
//
//  Created by admin on 10/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SVProgressHUD
import DropDown


protocol SearchHolidayListing {
    func ClickSearch(locationName : String! ,CheckinDate : String! ,CheckoutDate : String!, Adults : String! , Children : String! , Rooms : String )
}


class HolidayAccomodationSearchViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    var ExampleSearchListing : SearchHolidayListing!
    var SearchQuery : String! = ""
    var AddressLatitude : String! = ""
    var AddressLongitude : String! = ""
    var markers = [GMSMarker]()
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var screenSubtitle: UILabel!
    @IBOutlet weak var crossImageview: UIImageView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationViewTitle: UILabel!
    @IBOutlet weak var locationTXT: UITextField!
    @IBOutlet weak var checkInCheckOutView: UIView!
    @IBOutlet weak var checkInTitleLBL: UILabel!
    @IBOutlet weak var checkOuTitleLBL: UILabel!
    @IBOutlet weak var checkInDate: UILabel!
    @IBOutlet weak var checkOutDate: UILabel!
    @IBOutlet weak var checkInCalendarImage: UIImageView!
    @IBOutlet weak var checkOutCalendarImage: UIImageView!
    @IBOutlet weak var guestAndRoomView: UIView!
    @IBOutlet weak var adultsLBL: UILabel!
    @IBOutlet weak var childrenLBL: UILabel!
    @IBOutlet weak var roomLBL: UILabel!
    @IBOutlet weak var adultsDropDownView: UIView!
    @IBOutlet weak var childrenDropDownView: UIView!
    @IBOutlet weak var roomsDropDownView: UIView!
    @IBOutlet weak var adults: UILabel!
    @IBOutlet weak var adultsDropDownImage: UIImageView!
    //@IBOutlet weak var children: UILabel!
    @IBOutlet weak var childrenDropDownImage: UIImageView!
    @IBOutlet weak var rooms: UILabel!
    @IBOutlet weak var roomsDropDownImage: UIImageView!
    
    
    @IBOutlet weak var adultsViewOfTableview: UIView!
    
    @IBOutlet weak var adultsTableview: UITableView!
    
    
    @IBOutlet weak var childrenViewOfTableview: UIView!
    
    @IBOutlet weak var childrenTableview: UITableView!
    
    
    @IBOutlet weak var roomsViewOfTableview: UIView!
    
    @IBOutlet weak var roomsTableview: UITableView!
    @IBOutlet weak var datepickerView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet var Lchildren: UILabel!

    var ChoosenDate = Date()
    let adultDropdown = DropDown()
    let childrenDropdown = DropDown()
    let roomsDropdown = DropDown()
    @IBOutlet weak var localToolbarView: UIView!
    
    var isCalendar : String!

    var currentDate = Date()
    
    var staticArray = ["0","1","2","3","4","5","6","7","8","9","10"]

    
    var tapGesture = UITapGestureRecognizer()

    @IBOutlet weak var RoomsTableviewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ChildrenTableviewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var AdultsTableviewHeightConstraint: NSLayoutConstraint!
    
    
    var AdultNumber : String! = ""
    var RoomNumber : String! = ""
    var ChildrenNumber : String! = ""
    
    fileprivate let datestrformatter: DateFormatter = {
        let datestrformatter = DateFormatter()
        datestrformatter.dateFormat = "dd-MMM"
        return datestrformatter
    }()
    
    fileprivate let APIdateFormatter : DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    var StartDate : Date!
    
    var EndDate : Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dropdownFunctionalities()
        
        adultDropdown.dataSource = ["0","1","2","3","4","5","6","7","8","9","10"]
        childrenDropdown.dataSource = ["0","1","2","3","4","5","6","7","8","9","10"]
        roomsDropdown.dataSource = ["0","1","2","3","4","5","6","7","8","9","10"]
        
        adultDropdown.anchorView = view
        childrenDropdown.anchorView = view
        roomsDropdown.anchorView = view
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        self.locationTXT.inputView = UIView.init(frame: CGRect.zero)
        self.locationTXT.inputAccessoryView = UIView.init(frame: CGRect.zero)
        
        self.adults.text = "1"
        self.Lchildren.text = "0"
        self.rooms.text = "1"
        
        adultsTableview.delegate = self
        adultsTableview.dataSource = self
        
        adultsTableview.rowHeight = UITableView.automaticDimension
        adultsTableview.estimatedRowHeight = (44/568)*self.FullHeight
        childrenTableview.delegate = self
        childrenTableview.dataSource = self
        
        childrenTableview.rowHeight = UITableView.automaticDimension
        childrenTableview.estimatedRowHeight = (44/568)*self.FullHeight
        
        roomsTableview.delegate = self
        roomsTableview.dataSource = self
        
        roomsTableview.rowHeight = UITableView.automaticDimension
        roomsTableview.estimatedRowHeight = (44/568)*self.FullHeight
        
        locationTXT.delegate = self

        self.StrngLocalization()

        self.searchButtonOutlet.layer.cornerRadius = 5.0

        let addDays: TimeInterval = (60 * 60 * 24 * 180) //Add 180 for initial Days

        currentDate = currentDate.addingTimeInterval(addDays)

        datePicker.setValue(UIColor.black, forKeyPath: "textColor")

        datePicker.backgroundColor = .white
        
    }
    
    //MARK:- //Dismiss Keyboard
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK:- // Buttons
    
    // MARK:- // Cross Button
    
    @IBOutlet weak var crossButtonOutlet: UIButton!
    
    @IBAction func tapOnCross(_ sender: UIButton) {
        
//        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
//        self.navigationController?.pushViewController(navigate, animated: true)
//
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK:- // Open Check in DatePicker
    
    @IBOutlet weak var checkInCalendarButtonOutlet: UIButton!
    @IBAction func openCheckInCalendar(_ sender: Any) {
        
        isCalendar = "CI"
        
        UIView.animate(withDuration: 0.5) {
            
            self.view.bringSubviewToFront(self.datepickerView)
            self.datepickerView.frame = CGRect(x: 0, y: 0, width: self.FullWidth, height: self.FullHeight)
            self.datepickerView.isHidden = false



            self.datePicker.minimumDate = Date()
            self.datePicker.maximumDate = self.currentDate


            self.ChoosenDate = self.datePicker.date

            
            
            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
            self.datepickerView.addGestureRecognizer(self.tapGesture)
            
        }
        
    }
    
    
    // MARK:- // Open Check Out Datepicker
    
    @IBOutlet weak var checkOutCalendarButtonOutlet: UIButton!
    @IBAction func openCheckOutCalendar(_ sender: UIButton) {
        
        isCalendar = "CO"
        
        UIView.animate(withDuration: 0.5) {
            
            self.view.bringSubviewToFront(self.datepickerView)
            self.datepickerView.frame = CGRect(x: 0, y: 0, width: self.FullWidth, height: self.FullHeight)
            self.datepickerView.isHidden = false

            let addDays: TimeInterval = (60 * 60 * 24 * 89)


            self.datePicker.minimumDate = self.ChoosenDate
            self.datePicker.maximumDate = Date(timeInterval: addDays, since: self.ChoosenDate)
            
            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
            self.datepickerView.addGestureRecognizer(self.tapGesture)
            
        }
        
    }
    

    // MARK:- // Adults Drop Down Button
    
    @IBOutlet weak var adultsDropDownButtonOutlet: UIButton!
    @IBAction func adultsDropDownButton(_ sender: UIButton) {
        
//        if adultsViewOfTableview.isHidden{
//            animateTableview(toggle: true, backView: self.adultsViewOfTableview, tblview: self.adultsTableview)
//            self.AdultsTableviewHeightConstraint.constant = 100
//        }
//        else {
//            animateTableview(toggle: false, backView: self.adultsViewOfTableview, tblview: self.adultsTableview)
//            self.AdultsTableviewHeightConstraint.constant = 1
//        }
        
        self.adultDropdown.selectRow(0)
        self.adultDropdown.show()
        
    }
    
    
    
    
    // MARK:- // Children Drop Down Button
    
    @IBOutlet weak var childrenDropDownButtonOutlet: UIButton!
    @IBAction func childrenDropDownButton(_ sender: UIButton) {
        
//        if childrenViewOfTableview.isHidden{
//            animateTableview(toggle: true, backView: self.childrenViewOfTableview, tblview: self.childrenTableview)
//            self.ChildrenTableviewHeightConstraint.constant = 100
//        }
//        else {
//            animateTableview(toggle: false, backView: self.childrenViewOfTableview, tblview: self.childrenTableview)
//            self.ChildrenTableviewHeightConstraint.constant = 1
//        }
        self.childrenDropdown.selectRow(0)
        self.childrenDropdown.show()
        
    }
    
    
    // MARK:- // Rooms Drop Down Button
    
    @IBOutlet weak var roomsDropDownButtonOutlet: UIButton!
    @IBAction func roomsDropDownButton(_ sender: UIButton) {
        
//        if roomsViewOfTableview.isHidden{
//            animateTableview(toggle: true, backView: self.roomsViewOfTableview, tblview: self.roomsTableview)
//            self.RoomsTableviewHeightConstraint.constant = 100
//        }
//        else {
//            animateTableview(toggle: false, backView: self.roomsViewOfTableview, tblview: self.roomsTableview)
//            self.RoomsTableviewHeightConstraint.constant = 1
//        }
        self.roomsDropdown.selectRow(0)
        self.roomsDropdown.show()
        
        
    }
    
    
    //MARK:- //DropDown Functionality like Layer,Appearance,width,selected row etc
    
    func dropdownFunctionalities()
    {
        self.adultDropdown.width = 200
        self.childrenDropdown.width = 200
        self.roomsDropdown.width = 200
        
        self.adultDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
        print("Selected item: \(item) at index: \(index)")
        self.adults.text = "\(item ?? "")"
        self.AdultNumber = "\(item ?? "")"
        holidaySearchParameters.shared.setAdultCount(adultcount: "\(item ?? "")")
    }
        self.childrenDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.Lchildren.text = "\(item ?? "")"
            self.ChildrenNumber = "\(item ?? "")"
            holidaySearchParameters.shared.setChildrenCount(childrencount: "\(item ?? "")")
        }
        
        self.roomsDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.rooms.text = "\(item ?? "")"
            self.RoomNumber = "\(item ?? "")"
            holidaySearchParameters.shared.setRoomCount(roomcount: "\(item ?? "")")
        }
        
    }
    
    
    // MARK:- // Local Toolbar Ok Button
    
    
    @IBOutlet weak var okButtonOutlet: UIButton!
    @IBAction func okButton(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            
            let dateFormat = DateFormatter()
            
            dateFormat.dateFormat = "dd/MM/yyyy"
            
            if self.isCalendar.elementsEqual("CI")
            {
                self.checkInDate.text = dateFormat.string(from: self.datePicker.date)
                
                self.StartDate = self.datePicker.date
                
                let dateStr = dateFormat.string(from: self.datePicker.date)
                
                let datestrone = self.APIdateFormatter.string(from: self.datePicker.date)
                
                holidaySearchParameters.shared.setCheckinDate(ciDate: dateStr)
                
                let dstr = self.datestrformatter.string(from: self.datePicker.date)
                
                holidaySearchParameters.shared.setTempCheckIndate(checkindate: dstr)
                
                holidaySearchParameters.shared.setAPICheckindate(CheckIN: datestrone)
            }
            else
            {
                self.checkOutDate.text = dateFormat.string(from: self.datePicker.date)
                
                self.EndDate = self.datePicker.date
                
                let dateStr = dateFormat.string(from: self.datePicker.date)
                
                let datestrOne = self.APIdateFormatter.string(from: self.datePicker.date)
                
                holidaySearchParameters.shared.setCheckoutDate(coDate: dateStr)
                
                let dstr = self.datestrformatter.string(from: self.datePicker.date)
                
                holidaySearchParameters.shared.setTempCheckOutdate(checkoutdate: dstr)
                
                holidaySearchParameters.shared.setAPICheckOutDate(CheckOut: datestrOne)
            }
            
            self.datepickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.FullWidth, height: self.FullHeight)
            self.view.sendSubviewToBack(self.datepickerView)
            self.datepickerView.isHidden = true
            
        }
        
    }
    
    
    // MARK:- // Local toolbar Cancel Button
    
    
    @IBOutlet weak var cancelButtonOutlet: UIButton!
    @IBAction func cancelButton(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            
            self.datepickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.FullWidth, height: self.FullHeight)
            self.view.sendSubviewToBack(self.datepickerView)
            self.datepickerView.isHidden = true
            
        }
        
    }
    
    
    // MARK:- // Search Button
    
    @IBOutlet weak var searchButtonOutlet: UIButton!
    @IBAction func searchButton(_ sender: UIButton) {
        
        if self.checkInDate.text == "Check In Date"
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Select Check In Date")
        }
        else if self.checkOutDate.text == "Check Out Date"
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Select Check Out Date")
        }
        else
        {
            let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newholidaylisting") as! NewHolidayListingViewController
            
            
            
//            let stardate = Date(timeInterval: -86400, since: Date())
//            let tomorrow = Date(timeInterval: 86400, since: Date())
            
            let tempdateInt = EndDate.interval(ofComponent: .day, fromDate: StartDate)
            
            print("DateDifferences",tempdateInt)
            
            holidaySearchParameters.shared.setDateDifference(dateDifference: tempdateInt)
            
//            print("datedifference",holidaySearchParameters.shared.DateDifference)
            
        
            
            nav.Adults = self.AdultNumber
            nav.Children = self.ChildrenNumber
            nav.Rooms = self.RoomNumber
            
            nav.Address = locationTXT.text
            nav.CheckIn = checkInDate.text
            nav.CheckOut = checkOutDate.text
            print("locationTxt",locationTXT.text!)
            
            holidaySearchParameters.shared.HolidaySearchAddress = locationTXT.text
            
            self.navigationController?.pushViewController(nav, animated: true)
        }

    }
    
    
    // MARK: - Hide datepickerView when tapping outside
    
    @objc func tapGestureHandler() {
        UIView.animate(withDuration: 0.5)
        {
            self.datepickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.FullWidth, height: self.FullHeight)
            self.view.sendSubviewToBack(self.datepickerView)
            self.datepickerView.isHidden = true
        }
    }
    
    
    
    
    // MARK:- // Delegate Methods
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("static Array COunt-----",staticArray.count)
        
        return staticArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == adultsTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "adults")
            
            cell?.textLabel?.text = "\(staticArray[indexPath.row])"
            
            self.AdultNumber = "\(staticArray[indexPath.row])"
            
            cell?.selectionStyle = .none
            
            return cell!
        }
        else if tableView == childrenTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "children")
            
            cell?.textLabel?.text = "\(staticArray[indexPath.row])"
            
            self.ChildrenNumber = "\(staticArray[indexPath.row])"

            cell?.selectionStyle = .none
            
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "rooms")
            
            cell?.textLabel?.text = "\(staticArray[indexPath.row])"
            
            self.RoomNumber = "\(staticArray[indexPath.row])"

            cell?.selectionStyle = .none
            
            return cell!
        }

        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == adultsTableview
        {
            adults.text = staticArray[indexPath.row]
            animateTableview(toggle: false, backView: self.adultsViewOfTableview, tblview: self.adultsTableview)
            holidaySearchParameters.shared.setAdultCount(adultcount: "\(staticArray[indexPath.row])")
        }
        else if tableView == childrenTableview
        {
            Lchildren.text = staticArray[indexPath.row]
            animateTableview(toggle: false, backView: self.childrenViewOfTableview, tblview: self.childrenTableview)
            holidaySearchParameters.shared.setChildrenCount(childrencount: "\(staticArray[indexPath.row])")
        }
        else
        {
            rooms.text = staticArray[indexPath.row]
            animateTableview(toggle: false, backView: self.roomsViewOfTableview, tblview: self.roomsTableview)
            holidaySearchParameters.shared.setRoomCount(roomcount: "\(staticArray[indexPath.row])")
        }
        
    }
    
//
//    // MARK:- // TextField Delegates
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//
//        locationTXT.resignFirstResponder()
//
//        return true
//    }
//
    
    
    // MARK: - // Animate Tableview
    
    func animateTableview(toggle: Bool,backView: UIView,tblview: UITableView)
    {
        if toggle {
            
            UIView.animate(withDuration: 0.3){
                
                self.view.bringSubviewToFront(backView)
                backView.autoresizesSubviews = false
                backView.isHidden = false
                backView.frame.size.height = ((100/568)*self.FullHeight)
                tblview.frame.size.height = ((100/568)*self.FullHeight)
                backView.autoresizesSubviews = true
                print("Press tableView")
                
                backView.dropShadow(color: UIColor.gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
                
            }
        }
        else {
            
            UIView.animate(withDuration: 0.3){
                backView.autoresizesSubviews = false
                backView.isHidden = true
                backView.frame.size.height = ((1/568)*self.FullHeight)
                tblview.frame.size.height = ((1/568)*self.FullHeight)
                backView.autoresizesSubviews = true
                self.view.sendSubviewToBack(backView)
                print("Hide tableview")
            }
        }
    }

    //MARK:- String Localization
    func StrngLocalization()
    {
        self.screenTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "book_your_holiday", comment: "")
        self.screenSubtitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "accomodation_around_the_world", comment: "")
        self.locationViewTitle.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "where_do_you_want_to_go", comment: "")
        self.checkOuTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "check_out", comment: "")
        self.checkInTitleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "check_in", comment: "")
        self.adultsLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "adults", comment: "")
        self.childrenLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "children", comment: "")
        self.roomLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "rooms", comment: "")
        self.locationTXT.placeholder = LocalizationSystem.sharedInstance.localizedStringForKey(key: "location", comment: "")
        self.searchButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "search", comment: ""), for: .normal)
        

    }
    
    //MARk:- TextField Delegate and DataSource Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.locationTXT
        {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
                UInt(GMSPlaceField.placeID.rawValue))!
            autocompleteController.placeFields = fields
            
            // Specify a filter.
            let filter = GMSAutocompleteFilter()
            filter.type = .address
            autocompleteController.autocompleteFilter = filter
            
            // Display the autocomplete view controller.
            present(autocompleteController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - Fetching Latitude and Longitude
    
    
    func fireGoogleSearchAPI()
        
    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        var strGoogleApi = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(self.SearchQuery!)&key= AIzaSyDZ5jN7O4l7JetwBEo_fNa31FQA2jHVaUM"
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        print("Google Search Two URL is : ", strGoogleApi)
        
        if let url = NSURL(string: strGoogleApi)
            
        {
            
            if let data = try? Data(contentsOf:url as URL)
                
            {
                
                do{
                    
                    let jsonDict = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! NSDictionary
                    
                    print("Search Result Two Response : " , jsonDict)
                    
                    if "\(jsonDict["status"]!)".elementsEqual("OVER_QUERY_LIMIT")
                    {
                        
                    }
                    else
                    {
                        let resultsDictionary = jsonDict["result"] as! NSDictionary
                        
                        self.AddressLatitude = "\(((resultsDictionary["geometry"] as! NSDictionary)["location"] as! NSDictionary)["lat"]!)"
                        self.AddressLongitude = "\(((resultsDictionary["geometry"] as! NSDictionary)["location"] as! NSDictionary)["lng"]!)"
                        
                        print("Latitude",self.AddressLatitude!)
                        print("Longitude",self.AddressLongitude!)
                        
                        holidaySearchParameters.shared.setSearchLatitude(latitude: "\(self.AddressLatitude ?? "")")
                        holidaySearchParameters.shared.setSearchLongitude(longitude: "\(self.AddressLongitude ?? "")")
                        
//                        city = "\(((resultsDictionary["address_components"] as! NSArray)[1] as! NSDictionary)["long_name"]!)"
//                        state = "\(((resultsDictionary["address_components"] as! NSArray)[2] as! NSDictionary)["long_name"]!)"
//                        country = "\(((resultsDictionary["address_components"] as! NSArray)[3] as! NSDictionary)["long_name"]!)"
                    }
                    
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    
                    
                }
                    
                catch
                    
                {
                    
                    print("Error")
                    
                }
                
            }
            
        }
        
    }

}

//MARk:- //Extension for Google AutoComplete

extension HolidayAccomodationSearchViewController : GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        
        self.globalDispatchgroup.enter()
        self.locationTXT.text = "\(place.name!)"
        holidaySearchParameters.shared.setHotelLocation(hotelLocation: self.locationTXT.text ?? "")
        
        self.SearchQuery = "\(place.placeID!)"
        
        print("Place name: \(place.name)")
        print("Place ID: \(place.placeID)")
        print("Place attributions: \(place.attributions)")
        self.globalDispatchgroup.leave()
        self.globalDispatchgroup.notify(queue: .main, execute: {
            
            self.fireGoogleSearchAPI()
        })
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}




