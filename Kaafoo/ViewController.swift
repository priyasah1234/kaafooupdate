//
//  ViewController.swift
//  Kaafoo
//
//  Created by esolz on 20/08/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import MessageUI

class ViewController: GlobalViewController,MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
         controller.dismiss(animated: true, completion: nil)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
                if (MFMessageComposeViewController.canSendText()) {
                    let controller = MFMessageComposeViewController()
                    controller.body = "Message Body"
                    controller.recipients = []
                    controller.messageComposeDelegate = self
                    self.present(controller, animated: true, completion: nil)
                }


        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
