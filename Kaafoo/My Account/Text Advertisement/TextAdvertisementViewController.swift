//
//  TextAdvertisementViewController.swift
//  Kaafoo
//
//  Created by admin on 26/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class TextAdvertisementViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    
    

    
    
    
   
    @IBOutlet weak var textAdvertisementTableview: UITableView!
    
    
    
    var startValue = 0
    
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var textAdvertisementListArray : NSMutableArray! = NSMutableArray()
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    let dispatchGroup = DispatchGroup()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.set_Font()
        
        textAdvertisementList()
        
        self.headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.textAdvertisementTableview.estimatedRowHeight = 200
        
        self.textAdvertisementTableview.rowHeight = UITableView.automaticDimension
        
    }
    
    
    // MARK:- // Delegate Methods
    
    // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! TextAdvertisementTVCTableViewCell
        
        cell.title.text = "\((recordsArray[indexPath.row] as! NSDictionary)["title"]!)"
        
        cell.mobile.text = "\((recordsArray[indexPath.row] as! NSDictionary)["phone_no"]!)"
        
        cell.date.text = "\((recordsArray[indexPath.row] as! NSDictionary)["datetime"]!)"
        
        
        cell.editAdvertisementButton.tag = indexPath.row
        
        cell.editAdvertisementButton.addTarget(self, action: #selector(TextAdvertisementViewController.editAdvertisement(sender:)), for: .touchUpInside)
        
        cell.cellView.layer.cornerRadius = 8
        
        
        cell.titleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "title", comment: "")
        
        cell.dateLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "date", comment: "")
        
        cell.mobileLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "mobile", comment: "")
        
        
        
        //
        cell.titleLBL.font = UIFont(name: cell.titleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        cell.title.font = UIFont(name: cell.title.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        cell.dateLBL.font = UIFont(name: cell.dateLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        cell.date.font = UIFont(name: cell.date.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        cell.mobileLBL.font = UIFont(name: cell.mobileLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        cell.mobile.font = UIFont(name: cell.mobile.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
        
    }
    
    
    // MARK: - // Scrollview Delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            
            print("next start : ",nextStart ?? "" )
            
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                }
                
                
            }
            else
            {
                textAdvertisementList()
            }
            
        }
    }
    
    
    
    // MARK:- // Edit Advertisement
    
    
    @objc func editAdvertisement(sender: UIButton)
    {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "addAdvertisementVC") as! AddAdvertisementViewController
        
        navigate.amIEditing = 1
        
        navigate.advertisementID = "\((self.recordsArray[sender.tag] as! NSDictionary)["id"]!)"
        
        navigate.recordsArray = recordsArray.mutableCopy() as? NSMutableArray
        
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    // MARK: - // JSON POST Method to get Text Advertisement List Data
    
    func textAdvertisementList()
        
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_text_advertisement_list")! //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "user_id=\(userID!)&per_load=\(perLoad)&start_value=\(startValue)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        print("URL",url)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Items I am Selling Response: " , json)
                    
                    self.textAdvertisementListArray = json["info_array"] as? NSMutableArray
                    
                    for i in 0..<self.textAdvertisementListArray.count
                        
                    {
                        let tempDict = self.textAdvertisementListArray[i] as! NSDictionary
                        self.recordsArray.add(tempDict as! NSMutableDictionary)
                    }
                    
                    self.nextStart = "\(json["next_start"] ?? "")"
                    
                    self.startValue = self.startValue + self.textAdvertisementListArray.count
                    
                    print("Next Start Value : " , self.startValue)
                    
                    
                    DispatchQueue.main.async {
                        
                        self.textAdvertisementTableview.delegate = self
                        
                        self.textAdvertisementTableview.dataSource = self
                        
                        self.textAdvertisementTableview.reloadData()
                        
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    // MARK:- // Set Font
    
    func set_Font()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }

    

}



