//
//  FileVerificationViewController.swift
//  Kaafoo
//
//  Created by admin on 28/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class FileVerificationViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    
    @IBOutlet weak var uploadInformationLBL: UILabel!
    
    @IBOutlet weak var fileVerificationCollectionview: UICollectionView!
    
    let dispatchGroup = DispatchGroup()

    var fileVerificationListDictionary : NSDictionary!
    
    var listingArray = [NSDictionary]()
    
    var imagesToUpload = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.fileVerificationList()
        
    }
    
    
    @IBOutlet weak var uploadButtonOutlet: UIButton!
    @IBAction func tapOnUpload(_ sender: UIButton) {
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Upload Files", message: "Choose a Type", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Files", style: .default, handler: { (action:UIAlertAction) in
            
//            imagePickerController.sourceType = .camera
//            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
        
        
    }
    
    
    @IBOutlet weak var saveButtonOutlet: UIButton!
    @IBAction func tapOnSave(_ sender: UIButton) {
        
        uploadImages()
        
    }
    
    
    // MARK:- // Delegate Methods
    
    // MARK:- // Collectionview Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //return (fileVerificationListDictionary["verify_files"] as! NSArray).count
        return (listingArray.count + imagesToUpload.count)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCollectionCell", for: indexPath) as! FileVerificationCVCCollectionViewCell
        
        
        if indexPath.row > (listingArray.count - 1)
        {
            print("test-----",(indexPath.row - (listingArray.count - 1)))
            cell.cellImage.image = imagesToUpload[(indexPath.row - 1) - (listingArray.count - 1)]
            
        }
        else
        {
            
            let path = "\((listingArray[indexPath.item])["files"]!)"
            
            cell.cellImage.sd_setImage(with: URL(string: path))
            
        }
        
        return cell
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsize = CGSize(width: (self.fileVerificationCollectionview.frame.size.width/3) - (10/320)*self.FullWidth, height: (self.fileVerificationCollectionview.frame.size.width/3) - (10/320)*self.FullWidth)
        return cellsize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return (10/320)*self.FullWidth
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return (10/320)*self.FullWidth
        
    }
    
    
    // MARK:- // Delegate Functions
    
    // MARK:- // Imagepicker Delegates
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
        //imagesToUpload.removeAll()
        imagesToUpload.append(image)
        fileVerificationCollectionview.reloadData()
        
        picker.dismiss(animated: true, completion: nil)
        
        
    }
    
    
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    
    
    
    // MARK: - // JSON POST Method to get File Verification Listing Data
    
    func fileVerificationList()
        
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_verification_details")!   //change the url
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("file Verification List Response: " , json)
                    
                    self.fileVerificationListDictionary = json["info_array"] as? NSDictionary
                    
                    for i in 0..<(self.fileVerificationListDictionary["verify_files"] as! NSArray).count
                    {
                        for y in 0..<((((self.fileVerificationListDictionary["verify_files"] as! NSArray)[i] as! NSDictionary)["verify-files_details"] as! NSArray)).count
                        {
                            self.listingArray.append(((((self.fileVerificationListDictionary["verify_files"] as! NSArray)[i] as! NSDictionary)["verify-files_details"] as! NSArray)[y] as! NSDictionary))
                        }
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.fileVerificationCollectionview.delegate = self
                        self.fileVerificationCollectionview.dataSource = self
                        self.fileVerificationCollectionview.reloadData()
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    // MARK:- // Json Post Method to Add Images in an Array uploaded in Gallery
    
    
    
    func uploadImages()
    {
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        dispatchGroup.enter()
        
        let myUrl = NSURL(string: GLOBALAPI + "app_verification_file_save")!   //change the url
        
        print("Image Upload Url : ", myUrl)
        
        let request = NSMutableURLRequest(url: myUrl as URL)
        request.httpMethod = "POST"
        
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        
        // User "authentication":
        let parameters = ["user_id" : "\(userID!)" , "lang_id" : "\(langID!)"]
        
        print("Parameters : ", parameters)
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        //request.setValue("boundary=\(boundary)", forKey: "multipart/form-data")
        
        var imageDataArray = [Data]()
        
        print("Image Array : ", imagesToUpload)
        
        for i in 0..<imagesToUpload.count
        {
            let imageData = imagesToUpload[i].jpegData(compressionQuality: 1)
            imageDataArray.append(imageData!)
        }
        
        
        
        if(imageDataArray.count==0)  {
            
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
            
            return; }
        
        print(imageDataArray)
        
        request.httpBody = createBodyWithParametersAndImages(parameters: parameters, filePathKey: "Verify_file[po]", imageDataKey: imageDataArray, boundary: boundary) as Data
        
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error!)")
                return
            }
            
            // You can print out response object
            print("******* response = \(response!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options:.mutableContainers) as? NSDictionary
                
                print(json!)
                
                self.dispatchGroup.leave()
                
                DispatchQueue.main.async {
                    
                    SVProgressHUD.dismiss()
                }
                
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK:- // Creating HTTP Body with Parameters while Sending an Array of Images
    
    
    func createBodyWithParametersAndImages(parameters: [String: Any]?, filePathKey: String, imageDataKey: [Data], boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        for index in 0..<imageDataKey.count {
            let data = imageDataKey[index]
            
            let filename = "image\(index).jpeg"
            let mimetype = "image/jpeg"
            
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(data)
            body.appendString(string: "\r\n")
            
        }
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    
    
    
    
    // MARK:- // Generating Boundary String for Multipart Form Data
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    

    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
