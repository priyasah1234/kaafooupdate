//
//  EditAccountInfoViewController.swift
//  Kaafoo
//
//  Created by admin on 19/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage

class EditAccountInfoViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    // Global Font Applied
    
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var scrollContentview: UIView!
    
    @IBOutlet weak var usersCategoryView: UIView!
    @IBOutlet weak var userCategoryCollectionview: UICollectionView!
    
    @IBOutlet weak var usersCategorySeparatorView: UIView!
    
    @IBOutlet weak var serviceBookingTypeView: UIView!
    @IBOutlet weak var serviceBookingTypeTitleLBL: UILabel!
    @IBOutlet weak var serviceBookingTypeSeparatorview: UIView!
    
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var bannerTitleLBL: UILabel!
    @IBOutlet weak var bannerCollectionview: UICollectionView!
    @IBOutlet weak var bannerBottomSeparatorView: UIView!
    
    
    @IBOutlet weak var businessAccountInfoView: UIView!
    @IBOutlet weak var businessAccountInfoTitleLBL: UILabel!
    @IBOutlet weak var businessAccountInfoTableview: UITableView!
    
    @IBOutlet weak var addMoreView: UIView!
    
    @IBOutlet weak var userCategoryCollectionviewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bannerCollectionviewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var businessAccountInfoTableHeightCOnstraint: NSLayoutConstraint!
    
    
    var commaSeparatedString : String!
    
    
    let dispatchGroup = DispatchGroup()
    
    var bookingType : String!
    
    var allDataDictionary : NSDictionary!
    
    var categoryTypeArray = [categorySet]()
    
    var imageArray = [NSDictionary]()
    
    var addedImageArray = [UIImage]()
    
    var imagesToDeleteArray = [Int]()
    
    var businessUserInfoDataArray = [NSMutableDictionary]()
    
    
    var removeID : String!
    
    var removeIDTitleDescription : String!
    
    var responseDict : NSDictionary!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set font
        set_font()
        //set headerview color
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)


        getEditAccountInfoData()
        
        dispatchGroup.notify(queue: .main) {
            
            self.setFrames()
            
            self.bannerUploadButtonOutlet.layer.cornerRadius = self.bannerUploadButtonOutlet.frame.size.height / 2
            
        }
        
        
    }
    
    @IBOutlet weak var directServiceBookingButtonOutlet: UIButton!
   
    @IBOutlet weak var manualServiceBookingButtonOutlet: UIButton!
    
    @IBAction func tapOnServiceBookingType(_ sender: UIButton) {
        
        setBookingType(type: sender.tag)
        
    }
    
    
    
    @IBOutlet weak var bannerUploadButtonOutlet: UIButton!
    @IBAction func tapOnBannerUploadButton(_ sender: UIButton) {
        
        let tempCount = self.imageArray.count + self.addedImageArray.count
        
        if tempCount < 3
        {
            self.createActionsheetToPickImage()
        }
        else
        {
            customAlert(title: "Limit Reached", message: "You cannot upload more than 3 Images.") {
                print("Max Limit Reached.")
            }
        }
        
        

        
    }
    
    
    
    @IBOutlet weak var addMoreButtonOutlet: UIButton!
    @IBAction func tapOnAddMoreButton(_ sender: UIButton) {
        
        self.businessUserInfoDataArray.append(["id" : "name" , "title" : "title" + "\(businessUserInfoDataArray.count + 1)" , "description" : "description" + "\(businessUserInfoDataArray.count + 1)"])
        
        self.businessAccountInfoTableview.reloadData()
        
        self.setFrames()
        
    }
    
    
    @IBOutlet weak var removeButtonOutlet: UIButton!
    @IBAction func tapOnRemoveButton(_ sender: UIButton) {
        
        
        
    }
    
    
    
    @IBOutlet weak var saveEditAccountInfoButtonOutlet: UIButton!
    @IBAction func tapOnSaveEditAccountInfoButton(_ sender: UIButton) {
       
        
        var tempArray = [String]()
        
        for i in 0..<self.categoryTypeArray.count
        {
            if (self.categoryTypeArray[i].checkStatus?.elementsEqual("1"))!
            {
                tempArray.append(self.categoryTypeArray[i].id!)
            }
        }
        
        self.commaSeparatedString = tempArray.joined(separator: ",")
        
        
        for i in 0..<businessUserInfoDataArray.count
        {
            let index = IndexPath(row: i, section: 0)
            let cell: EditAccountInfoTVCTableViewCell = self.businessAccountInfoTableview.cellForRow(at: index) as! EditAccountInfoTVCTableViewCell
            
            (businessUserInfoDataArray[i] )["title"] = cell.cellTitle.text
            (businessUserInfoDataArray[i] )["description"] = cell.cellDescription.text
            
        }
        
        print("test>>>",businessUserInfoDataArray)
        
        saveAllData()

    }
    
    
    
    /*
    // MARK:- // Defining the Function to create a View with Radio Buttons
    
    func createViewWithButtons(viewName: UIView,title: String,buttonCount: Int,dataArray: NSArray,xOrigin: CGFloat,yOrigin: CGFloat) -> UIView
    {
        
        // initiating Title Label
        let titleLBL = UILabel(frame: CGRect(x: 10, y: 10, width: 300, height: 20))
        titleLBL.text = title
        //titleLBL.textColor = UIColor.darkGray
        //setting font
        titleLBL.font = UIFont(name: bannerTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        // declaring a temporary y origin to set frames later
        var tempYOrigin = titleLBL.frame.origin.y + titleLBL.frame.size.height + (10/568)*FullHeight
        var tempXOrigin = (10/320)*FullWidth
        
        
        // initiating Buttons
        for i in 0..<buttonCount
        {
            // create button
            let radioButton = UIButton(frame: CGRect(x: tempXOrigin, y: tempYOrigin + (10/568)*FullHeight, width: (20/320)*FullWidth, height: (20/320)*FullWidth))
            
            radioButton.setImage(UIImage(named: "Rectangle 22"), for: .normal)
            radioButton.setImage(UIImage(named: "checkbox2"), for: .selected)
            
            
            
            radioButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            radioButton.tag = i
            
            // initiating Button Name Label
            let buttonNameLBL = UILabel(frame: CGRect(x: radioButton.frame.origin.x + radioButton.frame.size.width + (10/320)*FullWidth, y: tempYOrigin, width: (115/320)*FullWidth, height: (45/320)*FullWidth))
            
            buttonNameLBL.numberOfLines = 0
            buttonNameLBL.text = "\((dataArray[i] as! NSDictionary)["category_name"]!)"
            buttonNameLBL.textColor = UIColor.black
            //setting font
            buttonNameLBL.font = UIFont(name: (directServiceBookingButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
            
            // changing tempXOrigin and tempYOrigin to set frames
            
            if ((i+1)%2) == 0
            {
                tempXOrigin = (10/320)*FullWidth
                tempYOrigin = buttonNameLBL.frame.origin.y + buttonNameLBL.frame.size.height + (10/568)*FullHeight
                
            }
            else
            {
                tempXOrigin = (160/320)*FullWidth
            }
            
            // adding buttonNameLBL as subview
            viewName.addSubview(buttonNameLBL)
            
            // adding button as subview
            viewName.addSubview(radioButton)
            
            buttons.append(radioButton)
            
        }
        
        
        viewName.frame = CGRect(x: xOrigin, y: yOrigin, width: FullWidth, height: (tempYOrigin + (15/320)*FullWidth))
        viewName.backgroundColor = UIColor.white
        
        self.usersCategorySeparatorView.frame = CGRect(x: (10/320)*FullWidth, y: (tempYOrigin + (14/320)*FullWidth), width: (300/320)*FullWidth, height: (1/320)*FullWidth)
        
        // add subviews
        viewName.addSubview(titleLBL)
        
        viewName.addSubview(usersCategorySeparatorView)
        
        return viewName
        
    }
    
    
    // MARK:- // Function declaring What the Buttons will do
    
    @objc func buttonAction(sender: UIButton!)
    {
        buttons[sender.tag].isSelected = !buttons[sender.tag].isSelected
        
        if buttons[sender.tag].isSelected
        {
            buttons[sender.tag].setImage(UIImage(named: "checkbox2"), for: .selected)
            categoryArray.append("\((((self.dataDictionary["info_array"] as! NSDictionary)["parent_category"] as! NSArray)[sender.tag] as! NSDictionary)["category_id"]!)")
        }
        else
        {
            buttons[sender.tag].setImage(UIImage(named: "Rectangle 22"), for: .normal)
            categoryArray.remove(at: sender.tag)
        }
        
        commaSeparatedString = categoryArray.joined(separator: ",")
        print("Comma separated String ------",commaSeparatedString)
    }
    
    */
    
    
    // MARK:- // Imagepicker Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
        self.addedImageArray.append(image)
        
        self.bannerCollectionview.reloadData()
        
        picker.dismiss(animated: true, completion: nil)
        
        self.setFrames()
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
        
    
    
    
    
    // MARK:- // Collectionview Delegates
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.userCategoryCollectionview
        {
            return self.categoryTypeArray.count
        }
        else
        {
            return (self.imageArray.count + self.addedImageArray.count)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.userCategoryCollectionview
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "category", for: indexPath) as! categoryTypeCVCCollectionViewCell
            
            if (self.categoryTypeArray[indexPath.item].checkStatus?.elementsEqual("1"))!
            {
                cell.checkBoxImageview.image = UIImage(named: "checkBoxFilled")
            }
            else
            {
                cell.checkBoxImageview.image = UIImage(named: "checkBoxEmpty")
            }
            
            cell.titleLBL.text = self.categoryTypeArray[indexPath.item].name
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "banner", for: indexPath) as! EditAccountInfoCVCCollectionViewCell
            
            if indexPath.item < imageArray.count
            {
                
                cell.cellImage.sd_setImage(with: URL(string: "\((self.imageArray[indexPath.item] )["Image"] ?? "")"))
            }
            else
            {
                
                cell.cellImage.image = self.addedImageArray[indexPath.item - self.imageArray.count]
            }
            
            
            if imagesToDeleteArray.contains(indexPath.row)
            {
                cell.removeImageButton.isHidden = false
            }
            else
            {
                cell.removeImageButton.isHidden = true
            }
            
            cell.removeImageButton.tag = indexPath.item
            
            cell.removeImageButton.addTarget(self, action: #selector(EditAccountInfoViewController.removeBannerImage(sender:)), for: .touchUpInside)
            
            
            return cell
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.userCategoryCollectionview
        {
            let cellsize = CGSize(width: 160, height: 50)
            return cellsize
        }
        else
        {
            let cellsize = CGSize(width: FullWidth, height: 100)
            return cellsize
        }
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.userCategoryCollectionview
        {
            if (self.categoryTypeArray[indexPath.item].checkStatus?.elementsEqual("0"))!
            {
                self.categoryTypeArray[indexPath.item].checkStatus = "1"
            }
            else
            {
                self.categoryTypeArray[indexPath.item].checkStatus = "0"
            }
            
            self.userCategoryCollectionview.reloadData()
            
        
        }
        else if collectionView == self.bannerCollectionview
        {
            var tempInt : Int!
            
            if imagesToDeleteArray.contains(indexPath.row)
            {
                for i in 0..<imagesToDeleteArray.count
                {
                    if indexPath.row == imagesToDeleteArray[i]
                    {
                        tempInt = i
                    }
                }
                
                self.imagesToDeleteArray.remove(at: tempInt)
            }
            else
            {
                self.imagesToDeleteArray.append(indexPath.row)
            }
            
            print("Images to Delete Array---",self.imagesToDeleteArray)
            
            
            self.bannerCollectionview.reloadData()
            
            
        }
        
        self.setFrames()
        
    }
    
    
    // MARK:- // Tableview Delgates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return businessUserInfoDataArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "accountInfo") as! EditAccountInfoTVCTableViewCell
        
        cell.cellTitleLBL.text = "title" + "\(indexPath.row+1)"
        cell.cellTitle.text = "\((businessUserInfoDataArray[indexPath.row])["title"]!)"
        
        cell.cellDescriptionLBL.text = "description" + "\(indexPath.row+1)"
        cell.cellDescription.text = "\((businessUserInfoDataArray[indexPath.row])["description"]!)"
        
        
        cell.deleteButton.tag = indexPath.row
        
        cell.deleteButton.addTarget(self, action: #selector(EditAccountInfoViewController.remove(sender:)), for: .touchUpInside)
        
        //set font
        cell.cellTitleLBL.font = UIFont(name: cell.cellTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.cellTitle.font = UIFont(name: (cell.cellTitle.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        cell.cellDescriptionLBL.font = UIFont(name: cell.cellDescriptionLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        cell.cellDescription.font = UIFont(name: (cell.cellDescription.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        
        
        return cell
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return (160/568)*FullHeight
//
//    }
    
    
    // MARK:- // Remove address
    
    
    @objc func remove(sender: UIButton)
    {
        if sender.tag > (((self.allDataDictionary["info_array"] as! NSDictionary)["business_acc_info"] as! NSArray).count - 1)
        {
            businessUserInfoDataArray.remove(at: sender.tag)
            businessAccountInfoTableview.reloadData()
            
            self.setFrames()
        }
        else
        {
            removeIDTitleDescription = "\((businessUserInfoDataArray[sender.tag])["id"]!)"
            
            deleteTitleDescription()
            
            dispatchGroup.notify(queue: .main) {
                
                self.getEditAccountInfoData()
                
            }
        }
        
        
        
    }
    
    
    
    // MARK:- // Remove Banner Image
    
    
    @objc func removeBannerImage(sender: UIButton)
    {
        if self.imageArray.count == 0
        {
            
            self.addedImageArray.remove(at: sender.tag)
            
            self.bannerCollectionview.reloadData()
            
            self.setFrames()
            
        }
        else
        {
            
            if sender.tag < self.imageArray.count
            {
                self.removeID = "\((self.imageArray[sender.tag] )["image_id"]!)"
                
                self.deleteBannerImage()
                
                self.dispatchGroup.notify(queue: .main) {
                    
                    self.getEditAccountInfoData()
                    
                    self.dispatchGroup.notify(queue: .main, execute: {
                        
                        self.setFrames()
                        
                    })
                    
                }
            }
            else
            {
                self.addedImageArray.remove(at: sender.tag - self.imageArray.count)
                
                self.bannerCollectionview.reloadData()
                
                self.setFrames()
            }
            
        }
    }
    
    
    
    
    // MARK: - // JSON POST Method to get Edit Account Info Data
    
    func getEditAccountInfoData()
        
    {
        
        self.dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_edit_account_info_details")!       //change the url
        
        print("Edit Account Info URL : ", url)
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "lang_id=\(langID!)&user_id=\(userID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
                return
                
                
            }
            
            guard let data = data else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
                return
                
                
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Edit Account Info Response: " , json)
                    
                    if (json["response"] as! Bool) == true
                    {
                        
                        self.allDataDictionary = json.copy() as? NSDictionary
                        
                        //Creating Category Type Array
                        self.categoryTypeArray.removeAll()
                        
                        for i in 0..<((json["info_array"] as! NSDictionary)["parent_category"] as! NSArray).count
                        {
                            self.categoryTypeArray.append(categorySet(id: "\((((json["info_array"] as! NSDictionary)["parent_category"] as! NSArray)[i] as! NSDictionary)["category_id"] ?? "")", name: "\((((json["info_array"] as! NSDictionary)["parent_category"] as! NSArray)[i] as! NSDictionary)["category_name"] ?? "")", checkStatus: "\((((json["info_array"] as! NSDictionary)["parent_category"] as! NSArray)[i] as! NSDictionary)["checked_status"] ?? "")"))
                        }
                        
                        
                        // Creating Image Array
                        self.imageArray.removeAll()
                        
                        self.imageArray = ((json["info_array"] as! NSDictionary)["banner_image"] as! NSArray).copy() as! [NSDictionary]
                        
                        
                        //Creating Business User Info Array
                        self.businessUserInfoDataArray.removeAll()
                        
                        for i in 0..<((json["info_array"] as! NSDictionary)["business_acc_info"] as! NSArray).count
                        {
                            self.businessUserInfoDataArray.append(["id" : "\((((json["info_array"] as! NSDictionary)["business_acc_info"] as! NSArray)[i] as! NSDictionary)["id"]!)" , "title" : "\((((json["info_array"] as! NSDictionary)["business_acc_info"] as! NSArray)[i] as! NSDictionary)["title"]!)" , "description" : "\((((json["info_array"] as! NSDictionary)["business_acc_info"] as! NSArray)[i] as! NSDictionary)["description"]!)"])
                        }
                        
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            self.userCategoryCollectionview.delegate = self
                            self.userCategoryCollectionview.dataSource = self
                            self.userCategoryCollectionview.reloadData()
                            
                            self.bannerCollectionview.delegate = self
                            self.bannerCollectionview.dataSource = self
                            self.bannerCollectionview.reloadData()
                            
                            self.businessAccountInfoTableview.delegate = self
                            self.businessAccountInfoTableview.dataSource = self
                            self.businessAccountInfoTableview.reloadData()
                            
                            SVProgressHUD.dismiss()
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            SVProgressHUD.dismiss()
                        }
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK: - // JSON POST Method to Save All Data
    
    func saveAllData()
        
    {
        
        self.dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        let url = URL(string: GLOBALAPI + "app_edit_account_info_save")!   //change the url
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        var params : String = ""
        
        params = "user_id=\(userID!)&lang_id=\(langID!)&user_category_id=\(commaSeparatedString!)&title=\(JSONStringify(value: businessUserInfoDataArray as AnyObject))&ser_booking_type=\(bookingType!)"
        
        print("Parameters are : " , params)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
//            let tempRequest = params.data(using: String.Encoding.utf8)

            request.httpBody = params.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Save all Data Response: " , json)
                    
                    
                    DispatchQueue.main.async {
                       
                        self.dispatchGroup.leave()
                        
                        self.dispatchGroup.notify(queue: .main, execute: {
                            
                            self.uploadBannerImages {
                                
                                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
                                
                                self.navigationController?.pushViewController(navigate, animated: true)
                                
                            }
                            
                            SVProgressHUD.dismiss()
                            
                        })
                        
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
 
 
    
    
    
    // MARK:- // Json Post Method to Add Images in an Array uploaded in Gallery
    
    
    
    func uploadBannerImages(completion : @escaping () -> ())
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let myUrl = NSURL(string: GLOBALAPI + "app_edit_account_info_save")!   //change the url
        
        print("Image Upload Url : ", myUrl)
        
        let request = NSMutableURLRequest(url: myUrl as URL)
        request.httpMethod = "POST"
        
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        // User "authentication":
        let parameters = ["user_id" : "\(userID!)" , "user_category_id" : "\(commaSeparatedString ?? "")" , "title" : "\(JSONStringify(value: businessUserInfoDataArray as AnyObject))" , "ser_booking_type" : "\(bookingType ?? "")" , "lang_id" : "\(langID!)" ]
        
        print("Parameters : ", parameters)
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        //request.setValue("boundary=\(boundary)", forKey: "multipart/form-data")
        
        var imageDataArray = [Data]()
        
        print("Image Array : ", imageArray)
        
        for i in 0..<addedImageArray.count
        {
            let imageData = addedImageArray[i].jpegData(compressionQuality: 1)
            imageDataArray.append(imageData!)
        }
        
        
        
        if(imageDataArray.count==0)  {
            
            DispatchQueue.main.async {
                
                self.dispatchGroup.leave()
                
                SVProgressHUD.dismiss()
                
                completion()
            }
            
            return; }
        
        request.httpBody = createBodyWithParametersAndImages(parameters: parameters, filePathKey: "sliderimage[]", imageDataKey: imageDataArray, boundary: boundary) as Data
        
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error!)")
                return
            }
            
            // You can print out response object
            print("******* response = \(response!)")
            
            //self.responseDict = response?.copy() as! NSDictionary
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options:.mutableContainers) as? NSDictionary
                
                print(json!)
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    
                    SVProgressHUD.dismiss()
                    
                    completion()
                }
                
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK:- // Creating HTTP Body with Parameters while Sending an Array of Images
    
    
    func createBodyWithParametersAndImages(parameters: [String: Any]?, filePathKey: String, imageDataKey: [Data], boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        for index in 0..<imageDataKey.count {
            let data = imageDataKey[index]
            
            let filename = "image\(index).jpeg"
            let mimetype = "image/jpeg"
            
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(data)
            body.appendString(string: "\r\n")
            
        }
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    
    
    
    
    // MARK:- // Generating Boundary String for Multipart Form Data
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    
    
    // MARK: - // JSON POST Method to Delete Images from Banner
    
    func deleteBannerImage()
        
    {
        
        self.dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        let url = URL(string: GLOBALAPI + "app_delete_image")!   //change the url
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        var parameters : String = ""
        
        parameters = "user_id=\(userID!)&lang_id=\(langID!)&remove_id=\(removeID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
                return
            }
            
            guard let data = data else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Delete Banner Image Response: " , json)
                    
                    
                    DispatchQueue.main.async {
                        
                        self.dispatchGroup.leave()
                        
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    // MARK: - // JSON POST Method to get Edit Account Info Data
    
    func deleteTitleDescription()
        
    {
        
        self.dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_remove_sec")!       //change the url
        
        print("Delete Title Description : ", url)
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "lang_id=\(langID!)&user_id=\(userID!)&remove_id=\(removeIDTitleDescription!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    self.getEditAccountInfoData()
                    SVProgressHUD.dismiss()
                }
                
                return
            }
            
            guard let data = data else {
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    self.getEditAccountInfoData()
                    SVProgressHUD.dismiss()
                }
                
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    print("Delete Title Description Response: " , json)
                    
                    DispatchQueue.main.async {
                    
                        self.dispatchGroup.leave()
                        
                        self.getEditAccountInfoData()
                        SVProgressHUD.dismiss()
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    self.getEditAccountInfoData()
                    SVProgressHUD.dismiss()
                }
            }
        })
        
        task.resume()
        
        
        
    }
    
    
   
    
    
    /*
    
    // MARK:- // Handle Long Press Function
    
    
    @objc func handleLongPress(longPressGR: UILongPressGestureRecognizer) {
        
        
        if longPressGR.state == .began // Longpress actions commits before touch ends
        {
            
            let point = longPressGR.location(in: self.bannerCollectionview)
            let indexPath = self.bannerCollectionview.indexPathForItem(at: point)
            
            let object = self.bannerCollectionview.cellForItem(at: indexPath!) as! EditAccountInfoCVCCollectionViewCell
            
                if var indexPath = indexPath {
                    if collectionviewCellToDelete.contains(where: { $0 == indexPath.row })
                    {
                        
                        cellsDeselected.removeAll()
                        for i in 0..<tempCollectionviewCellToDelete.count
                        {
                            
                            if "\((tempCollectionviewCellToDelete[i] as NSMutableDictionary)["value"]!)".elementsEqual("\(indexPath.row)")
                            {
                                cellsDeselected.append(i)
                                
                            }
                            
                        }
                        for i in 0..<cellsDeselected.count
                        {
                            tempCollectionviewCellToDelete.remove(at: cellsDeselected[i])
                        }
                        
                    }
                    else
                    {
                        // not
                        var cell = self.bannerCollectionview.cellForItem(at: indexPath)
                        print(indexPath.row)
                        collectionviewCellToDelete.append(indexPath.row)
                        tempCollectionviewCellToDelete.removeAll()
                        for i in 0..<collectionviewCellToDelete.count
                        {
                            tempCollectionviewCellToDelete.append(["key" : "\(i)" , "value" : "\(collectionviewCellToDelete[i])"])
                        }
                        
                        
                        
                    }
                    
                    collectionviewCellToDelete.removeAll()
                    for i in 0..<tempCollectionviewCellToDelete.count
                    {
                        collectionviewCellToDelete.append(Int("\((tempCollectionviewCellToDelete[i] as NSMutableDictionary )["value"]!)")!)
                    }
                    
                    print("Collectionview Cell to Delete : ", collectionviewCellToDelete)
                    
                    let object = self.bannerCollectionview.cellForItem(at: indexPath) as! EditAccountInfoCVCCollectionViewCell
                    if (object.cancelImageView.isHidden)
                    {
                        object.cancelImageView.isHidden = false
                    }
                    else
                    {
                        object.cancelImageView.isHidden = true
                    }
                    
                    
                }
                else {
                    print("Could not find index path")
                }
                
            
            
        }
        
        // show and hide delete option
        if collectionviewCellToDelete.count == 0
        {
            
            removeButtonOutlet.isHidden = true
            removeButtonOutlet.isUserInteractionEnabled = false
            
        }
        else
        {
            
            removeButtonOutlet.isHidden = false
            removeButtonOutlet.isUserInteractionEnabled = true
            
        }
        
    }
    
    */
    
    
    
    
    
    //////////////
    
    
    
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
        
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        
        
        if JSONSerialization.isValidJSONObject(value) {
            
            do{
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    
                    return string as String
                }
            }catch {
                
                print("error")
                //Access error here
            }
            
        }
        return ""
        
    }
    
    
    
    // MARK:- // SEt Font
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        serviceBookingTypeTitleLBL.font = UIFont(name: serviceBookingTypeTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        bannerTitleLBL.font = UIFont(name: bannerTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        businessAccountInfoTitleLBL.font = UIFont(name: businessAccountInfoTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 15)))
        
        saveEditAccountInfoButtonOutlet.titleLabel?.font = UIFont(name: (saveEditAccountInfoButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
    }
    
    
    
    // MARK:- // Set Booking Type
    
    func setBookingType(type: Int)
    {
        if type == 1
        {
            directServiceBookingButtonOutlet.isSelected = !directServiceBookingButtonOutlet.isSelected
            manualServiceBookingButtonOutlet.isSelected = false
            
            if directServiceBookingButtonOutlet.isSelected
            {
                directServiceBookingButtonOutlet.setImage(UIImage(named: "radioSelected"), for: .selected)
                bookingType = "1"
            }
            else
            {
                directServiceBookingButtonOutlet.setImage(UIImage(named: "radioUnselected"), for: .normal)
                bookingType = ""
            }
//            print("Booking Type-------",bookingType)
        }
        else
        {
            manualServiceBookingButtonOutlet.isSelected = !manualServiceBookingButtonOutlet.isSelected
            directServiceBookingButtonOutlet.isSelected = false
            
            if manualServiceBookingButtonOutlet.isSelected
            {
                manualServiceBookingButtonOutlet.setImage(UIImage(named: "radioSelected"), for: .selected)
                bookingType = "0"
            }
            else
            {
                manualServiceBookingButtonOutlet.setImage(UIImage(named: "radioUnselected"), for: .normal)
                bookingType = ""
            }
//            print("Booking Type-------",bookingType)
        }
    }
    
    
    
    
    // MARK:- // Create Actionsheet to Pick Image
    
    func createActionsheetToPickImage()
    {
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
        
    }
    
    
    
    // MARK:- // Set Frames
    
    func setFrames()
    {
        self.userCategoryCollectionviewHeightConstraint.constant = CGFloat(((self.categoryTypeArray.count / 2) + (self.categoryTypeArray.count % 2)) * 50)
        
        self.bannerCollectionviewHeightConstraint.constant = CGFloat((self.imageArray.count + self.addedImageArray.count) * 100)
        
        self.businessAccountInfoTableHeightCOnstraint.constant = CGFloat(self.businessUserInfoDataArray.count * 190)
        
        for i in 0..<self.categoryTypeArray.count
        {
            if (self.categoryTypeArray[i].id?.elementsEqual("11"))!
            {
                if (self.categoryTypeArray[i].checkStatus?.elementsEqual("0"))!
                {
                    self.serviceBookingTypeView.isHidden = true
                }
                else
                {
                    self.serviceBookingTypeView.isHidden = false
                    
                    self.setBookingType(type: Int("\((self.allDataDictionary["info_array"] as! NSDictionary)["service_booking_status"]!)")!)
                }
            }
        }
        

        
    }
    
    
    
}




class categorySet
{
    
    var id : String?
    var name : String?
    var checkStatus : String?
    
    init(id: String, name: String?, checkStatus: String?) {
        
        self.id = id
        self.name = name
        self.checkStatus = checkStatus
        
    }
}



// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
