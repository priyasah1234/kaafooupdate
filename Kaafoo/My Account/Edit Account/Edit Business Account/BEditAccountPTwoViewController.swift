//
//  BEditAccountPTwoViewController.swift
//  Kaafoo
//
//  Created by admin on 01/08/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class BEditAccountPTwoViewController: GlobalViewController {
    
    
    @IBOutlet weak var editLbl: UILabel!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var editAccountLbl: UILabel!
    @IBOutlet weak var editAccountBottomView: UIView!
    @IBOutlet weak var websiteUrlLbl: UILabel!
    @IBOutlet weak var landlineNoLbl: UILabel!
    @IBOutlet weak var mobileNoLbl: UILabel!
    @IBOutlet weak var websiteUrlTXT: UITextField!
    @IBOutlet weak var landlineNoTXT: UITextField!
    @IBOutlet weak var mobileNoTXT: UITextField!
    
    @IBOutlet weak var mainScroll: UIScrollView!
    
    var userInfoDictionary : NSMutableDictionary!
    
    var editedUserInfoDictionary : NSMutableDictionary!
    
    var checkshow_mobile : String!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.mobileNoSwitchOutlet.isOn = true
        self.editedUserInfoDictionary["checkshow_mobile"] = "0"

        mobileNoSwitchOutlet.center = mobileNoTXT.center
        mobileNoSwitchOutlet.frame.origin.x = mainScroll.frame.size.width - mobileNoSwitchOutlet.frame.size.width
        
        set_font()
        
        self.setLanguageStrings()
        
//        print("userinfoDict----",userInfoDictionary)
        
        websiteUrlTXT.text = "\(userInfoDictionary["website"] ?? "")"
        landlineNoTXT.text = "\(userInfoDictionary["landline"] ?? "")"
        mobileNoTXT.text = "\(userInfoDictionary["mobile_num"] ?? "")"
        
    }
    
    @IBOutlet weak var mobileNoSwitchOutlet: UISwitch!
    @IBAction func mobileNoSwitch(_ sender: UISwitch) {
        
        if (sender.isOn == true)
        {
            checkshow_mobile = "1"
            mobileNoTXT.isHidden = false
        }
        else
        {
            checkshow_mobile = "0"
            mobileNoTXT.isHidden = true
        }
        
    }
    
    
    @IBOutlet weak var nextOutlet: UIButton!
    @IBAction func tapOnNext(_ sender: Any) {
        
        validateWithAlerts()
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "bEditAccountPThreeVC") as! BEditAccountPThreeViewController
        
        navigate.userInfoDictionary = userInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        addDataInArray()
        
        navigate.editedUserInfoDictionary = editedUserInfoDictionary.mutableCopy() as? NSMutableDictionary
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    // MARK:- // Defined Functions
    
    // MARK:- // Set Font
    
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

        //setAttributedTitle(toLabel: self.editLbl, boldText: "EDIT", boldTextFont: UIFont(name: self.editLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.websiteUrlTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: "Account")
        
        editAccountLbl.font = UIFont(name: editAccountLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        websiteUrlLbl.font = UIFont(name: websiteUrlLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        websiteUrlTXT.font = UIFont(name: (websiteUrlTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        landlineNoLbl.font = UIFont(name: landlineNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        landlineNoTXT.font = UIFont(name: (landlineNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        mobileNoLbl.font = UIFont(name: mobileNoLbl.font.fontName, size: CGFloat(Get_fontSize(size: 16)))
        mobileNoTXT.font = UIFont(name: (mobileNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 16)))
        
    }
    
    
    func validateWithAlerts()
    {
        if (websiteUrlTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Website URL Empty", message: "Please Enter Website URL", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (landlineNoTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Landline Number Empty", message: "Please Enter Landline Number", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
        else if (mobileNoTXT.text?.isEmpty)!
        {
            let alert = UIAlertController(title: "Mobile Number Empty", message: "Please Enter Mobile Number", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
    }
    
    // MARK:- // Add Data in Array
    
    func addDataInArray()
    {
        
        
        self.editedUserInfoDictionary["website"] = self.websiteUrlTXT.text
        self.editedUserInfoDictionary["landline"] = self.landlineNoTXT.text
        self.editedUserInfoDictionary["mobile"] = self.mobileNoTXT.text
        
        
        
    }
    
    
    
    // MARK:- // Set Language Strings
    
    func setLanguageStrings()
    {
        
        setAttributedTitle(toLabel: self.editLbl, boldText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit", comment: ""), boldTextFont: UIFont(name: self.editLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.websiteUrlTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "account", comment: ""))
        
        
        self.editAccountLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit_account", comment: "")
        
        self.websiteUrlLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "website_url", comment: "")
        
        self.landlineNoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "landline", comment: "")
        
        self.mobileNoLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "mobile_number", comment: "")
        
        self.nextOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "next", comment: ""), for: .normal)
    }
    
    


}
