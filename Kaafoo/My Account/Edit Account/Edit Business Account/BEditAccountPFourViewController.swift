//
//  BEditAccountPFourViewController.swift
//  Kaafoo
//
//  Created by admin on 01/08/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage


class BEditAccountPFourViewController: GlobalViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var editLbl: UILabel!
    @IBOutlet weak var accountLbl: UILabel!
    @IBOutlet weak var editAccountLbl: UILabel!
    @IBOutlet weak var editAccountBottomView: UIView!
    @IBOutlet weak var businessLogoImageView: UIImageView!
    @IBOutlet weak var businessBannerImageView: UIImageView!
    @IBOutlet weak var uploadBusinessLogoLbl: UILabel!
    
    @IBOutlet weak var mainScroll: UIScrollView!
    
    var userInfoDictionary : NSMutableDictionary!
    
    var editedUserInfoDictionary : NSMutableDictionary!
    
    let allImageArray : NSMutableArray! = NSMutableArray()
    
    var imageViewType : Int!
    
    let dispatchGroup = DispatchGroup()
    
    var imageDataArray : NSMutableArray! = NSMutableArray()
    
    var imagePathArray : NSMutableArray! = NSMutableArray()
    
    var isNewLogo : Bool! = false
    var isNewBanner : Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

//        print("test", editedUserInfoDictionary)
        
        set_font()
        
        self.setLanguageStrings()
        
        businessLogoImageView.sd_setImage(with: URL(string: "\(userInfoDictionary["logo_image"]!)"))
        businessBannerImageView.sd_setImage(with: URL(string: "\(userInfoDictionary["banner_image"]!)")) 
        
    }
    
    
    @IBOutlet weak var uploadBusinessLogoOutlet: UIButton!
    @IBAction func uploadBusinessLogo(_ sender: Any) {
        
        imageViewType = 0
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            self.present(actionSheet, animated: true, completion: nil)
        
        
    }
    
    
    
    @IBOutlet weak var uploadBusinessBannerOutlet: UIButton!
    @IBAction func uploadBusinessBanner(_ sender: Any) {
        
        imageViewType = 1
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a Source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    
    
    
    @IBOutlet weak var submitOutlet: UIButton!
    @IBAction func tapOnSubmit(_ sender: Any) {
        
        addDataInArray()
        
        self.saveInfo()
        
        self.dispatchGroup.notify(queue: .main, execute: {
            
            if self.isNewLogo == true
            {
                
                self.logoUploadRequest()
                    
                self.dispatchGroup.notify(queue: .main, execute: {
                    
                    if self.isNewBanner == true
                    {
                        self.bannerUploadRequest()
                        
                        self.dispatchGroup.notify(queue: .main, execute: {
                            
                            self.validateWithAlerts()
                            
                            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
                            
                            self.navigationController?.pushViewController(navigate, animated: true)
                            
                        })
                    }
                    else
                    {
                        self.validateWithAlerts()
                        
                        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
                        
                        self.navigationController?.pushViewController(navigate, animated: true)
                    }
                    
                })
                
            }
            else if self.isNewBanner == true
            {
                self.bannerUploadRequest()
                
                self.dispatchGroup.notify(queue: .main, execute: {
                    
                    self.validateWithAlerts()
                    
                    let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
                    
                    self.navigationController?.pushViewController(navigate, animated: true)
                    
                })
                
            }
            else
            {
                self.validateWithAlerts()
                
                let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "latestHomeVC") as! LatestHomeViewController
                
                self.navigationController?.pushViewController(navigate, animated: true)
            }
            
        })
            
        
        }
    
    
    
    // MARK:- // Defined Functions
    
    // MARK:- // Set Font
    
    
    func set_font()
    {
        
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
 
        setAttributedTitle(toLabel: self.editLbl, boldText: "EDIT", boldTextFont: UIFont(name: self.editLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: (self.editLbl.font?.fontName)!, size: CGFloat(Get_fontSize(size: 34)))!, normalText: "Account")
        
        
        editAccountLbl.font = UIFont(name: editAccountLbl.font.fontName, size: CGFloat(Get_fontSize(size: 17)))
        
        self.uploadBusinessLogoOutlet.titleLabel?.font = UIFont(name: (self.uploadBusinessLogoOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
        self.uploadBusinessBannerOutlet.titleLabel?.font = UIFont(name: (self.uploadBusinessBannerOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
        self.submitOutlet.titleLabel?.font = UIFont(name: (self.submitOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!
        
        
    }
    
    
    func validateWithAlerts()
    {
        if (allImageArray.count == 0)
        {
            let alert = UIAlertController(title: "Images Missing", message: "Atleast add Both Images", preferredStyle: .alert)
            
            let ok = UIAlertAction.init(title: "Ok", style: .cancel, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true, completion: nil )
        }
    }
    
    
    // MARK:- // ADd Data in Array
    
    func addDataInArray()
    {
        let tempImageArray : NSMutableArray! = NSMutableArray()
        
        allImageArray.add(businessLogoImageView.image!)
        allImageArray.add(businessBannerImageView.image!)
        
        self.editedUserInfoDictionary["image"] = tempImageArray
        
//        print("Edited User Info Dictionary : ", editedUserInfoDictionary)
    }
    
    
    // MARK:- // Delegate Functions
    
    // MARK:- // Imagepicker Delegates
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
            let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
            if imageViewType == 0
            {
               
                businessLogoImageView.image = image
                isNewLogo = true
            }
            else
            {
                
                businessBannerImageView.image = image
                isNewBanner = true
            }
        
        picker.dismiss(animated: true, completion: nil)
        
        
    }
        
    
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    // MARK: - // JSON POST Method to Save Account Info
    
    
    func saveInfo()
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }
        
        let url = URL(string: GLOBALAPI + "app_edit_account_save")!   //change the url
        
        print("save account info URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
       
        
        parameters = "user_id=\(userID!)&buss_name=\(editedUserInfoDictionary["business_name"]!)&displayname=\(editedUserInfoDictionary["display_name"]!)&website_url=\(editedUserInfoDictionary["website"]!)&landline_no=\(editedUserInfoDictionary["landline"]!)&mobile_no=\(editedUserInfoDictionary["mobile"]!)&new_pass=\(editedUserInfoDictionary["new_password"]!)&chckshow_add=\(editedUserInfoDictionary["checkshow_add"]!)&chckshow_mobile=\(editedUserInfoDictionary["checkshow_mobile"]!)&chckshow_email=\(editedUserInfoDictionary["checkshow_email"]!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
       
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                print("test : ",data)
                
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String : Any]
                
                print("Submit Account info Response: " , json)
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
 
                    SVProgressHUD.dismiss()
                    self.ShowAlertMessage(title: "Alert", message: "\(json["message"] ?? "")")
                    
                }
                
                
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    

    // MARK: - // JSON POST Method to Upload Logo
    
    
    func logoUploadRequest()
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }
        
        let myUrl = NSURL(string: GLOBALAPI + "app_edit_account_save")!   //change the url
        
        print("Image Upload Url : ", myUrl)
        
        let request = NSMutableURLRequest(url: myUrl as URL)
        request.httpMethod = "POST"
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        // User "authentication":
        
        let parameters = [ "user_id" : "\(userID!)",
            "buss_name" : "\(editedUserInfoDictionary["business_name"]!)",
            "displayname" : "\(editedUserInfoDictionary["display_name"]!)",
            "website_url" : "\(editedUserInfoDictionary["website"]!)",
            "mobile_no" : "\(editedUserInfoDictionary["mobile"]!)",
            "landline_no" : "\(editedUserInfoDictionary["landline"]!)",
            "new_pass" : "\(editedUserInfoDictionary["new_password"]!)",
            "chckshow_add" : "\(editedUserInfoDictionary["checkshow_add"]!)",
            "chckshow_mobile" : "\(editedUserInfoDictionary["checkshow_mobile"]!)",
            "chckshow_email" : "\(editedUserInfoDictionary["checkshow_email"]!)" ]
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        //request.setValue("boundary=\(boundary)", forKey: "multipart/form-data")
        
                let imageData = businessLogoImageView.image!.jpegData(compressionQuality: 1)
        
                if(imageData==nil)  { return; }
                
        request.httpBody = createBodyWithParameters(parameters: parameters, filePathKey: "logo_image", imageDataKey: imageData! as NSData, boundary: boundary) as Data
                
                let task = URLSession.shared.dataTask(with: request as URLRequest) {
                    data, response, error in
                    
                    if error != nil {
                        print("error=\(error!)")
                        return
                    }
                    
                    // You can print out response object
                    print("******* response = \(response!)")
                    
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                        print("Logo Upload Request Response----",json!)
                        
                        self.isNewLogo = false
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            SVProgressHUD.dismiss()

                        }
                        
                    }catch
                    {
                        print(error)
                    }
                    
                }
                
                task.resume()
            
        
        
    }
    
    
    
    // MARK: - // JSON POST Method to Upload Banner
    
    
    func bannerUploadRequest()
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: LocalizationSystem.sharedInstance.localizedStringForKey(key: "loading_please_wait", comment: ""))
        }
        
        let myUrl = NSURL(string: GLOBALAPI + "app_edit_account_save")!   //change the url
        
        print("Image Upload Url : ", myUrl)
        
        let request = NSMutableURLRequest(url: myUrl as URL)
        request.httpMethod = "POST"
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        // User "authentication":
        let parameters = [ "user_id" : "\(userID!)",
            "buss_name" : "\(editedUserInfoDictionary["business_name"]!)",
            "displayname" : "\(editedUserInfoDictionary["display_name"]!)",
            "website_url" : "\(editedUserInfoDictionary["website"]!)",
            "mobile_no" : "\(editedUserInfoDictionary["mobile"]!)",
            "landline_no" : "\(editedUserInfoDictionary["landline"]!)",
            "new_pass" : "\(editedUserInfoDictionary["new_password"]!)",
            "chckshow_add" : "\(editedUserInfoDictionary["checkshow_add"]!)",
            "chckshow_mobile" : "\(editedUserInfoDictionary["checkshow_mobile"]!)",
            "chckshow_email" : "\(editedUserInfoDictionary["checkshow_email"]!)" ]
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        //request.setValue("boundary=\(boundary)", forKey: "multipart/form-data")
        
        let imageData = businessBannerImageView.image!.jpegData(compressionQuality: 1)
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(parameters: parameters, filePathKey: "banner_image", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error!)")
                return
            }
            
            // You can print out response object
            print("******* response = \(response!)")
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                
                print("Banner Upload Request Response----",json!)
                
                self.isNewBanner = false
                
                DispatchQueue.main.async {
                    
                    self.dispatchGroup.leave()
                    
                    SVProgressHUD.dismiss()
                }
                
            }catch
            {
                print(error)
            }
            
        }
        
        task.resume()
        
        
        
    }
    
    
    
    
    
    
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "banner_image.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    
    // MARK:- // Set Language Strings
    
    func setLanguageStrings()
    {
        
        
        setAttributedTitle(toLabel: self.editLbl, boldText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit", comment: ""), boldTextFont: UIFont(name: self.editLbl.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalTextFont: UIFont(name: self.uploadBusinessLogoOutlet.titleLabel!.font.fontName, size: CGFloat(Get_fontSize(size: 34)))!, normalText: LocalizationSystem.sharedInstance.localizedStringForKey(key: "account", comment: ""))
        
        
        self.editAccountLbl.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit_account", comment: "")
        
        self.uploadBusinessLogoOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "upload_business_logo", comment: ""), for: .normal)
        
        self.uploadBusinessBannerOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Upload_banner", comment: ""), for: .normal)
        
        self.submitOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "submit", comment: ""), for: .normal)
        
        
    }
    
}


extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
