//
//  BookingAvailabilityTVCTableViewCell.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 26/02/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class BookingAvailabilityTVCTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var dayLBL: UILabel!
    @IBOutlet weak var checkboxButton: UIButton!
    
    @IBOutlet weak var startTime: UIButton!
    @IBOutlet weak var endTime: UIButton!
    
    @IBOutlet weak var cellSeparator: UIView!
    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
