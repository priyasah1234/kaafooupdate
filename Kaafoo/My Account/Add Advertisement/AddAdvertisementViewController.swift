//
//  AddAdvertisementViewController.swift
//  Kaafoo
//
//  Created by admin on 24/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import FSCalendar
import SVProgressHUD
import JTAppleCalendar


class AddAdvertisementViewController: GlobalViewController,UITextViewDelegate {
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var titleView: UIView!
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var titleTXT: UITextField!
    
    @IBOutlet weak var descriptionView: UIView!
    
    @IBOutlet weak var descriptionLBL: UILabel!
    
    @IBOutlet weak var descriptionTXT: UITextView!
    
    @IBOutlet weak var characterLimitLBL: UILabel!
    
    @IBOutlet weak var mobileNoView: UIView!
    
    @IBOutlet weak var mobileNoLBL: UILabel!
    
    @IBOutlet weak var mobileNoTXT: UITextField!
    
    @IBOutlet weak var dateTImeView: UIView!
    
    @IBOutlet weak var calendarImageview: UIImageView!
    
    @IBOutlet weak var setDateTImeLBL: UILabel!
    
    @IBOutlet weak var datepickerView: UIView!
    
    @IBOutlet weak var localToolbarView: UIView!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    var a = 5
    
    var b = 10
    
    
    
    
    
    var amIEditing : Int!
    
    var advertisementID : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    var formatter = DateFormatter()
    
    var dateString : String!
    
    var timeString : String!
    
    var dateTimeString : String!
    
    var tapGesture = UITapGestureRecognizer()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.descriptionTXT.text = ""
        
        self.setLanguageStrings()
        
        self.set_Font()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapTarget(sender:)))
        
        self.view.addGestureRecognizer(tap)
        
        
        if amIEditing == 1
        {
            
            for i in 0..<recordsArray.count
            {
                if "\((self.recordsArray[i] as! NSDictionary)["id"]!)" == advertisementID
                {
                    titleTXT.text = "\((self.recordsArray[i] as! NSDictionary)["title"]!)"
                    
                    descriptionTXT.text = "\((self.recordsArray[i] as! NSDictionary)["description"]!)"
                    
                    mobileNoTXT.text = "\((self.recordsArray[i] as! NSDictionary)["phone_no"]!)"
                    
                    setDateTImeLBL.text = "\((self.recordsArray[i] as! NSDictionary)["datetime"]!)"
                    
                    // Charcter Limit 160
                    let newText = self.descriptionTXT.text
                    
                    let numberOfChars = newText!.count
                    
                    self.characterLimitLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "youCanAddOnly", comment: "") + " \(160-numberOfChars) " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "characters", comment: "")
                    
                }
            }
            
        }
        
        
        self.descriptionTXT.delegate = self
        
    }
    
    @objc func tapTarget(sender : UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    
    
    // MARK:- // Buttons
    
    
    // MARK:- // Toolbar Ok Button
    
    @IBOutlet weak var okButtonOutlet: UIButton!
    
    @IBAction func toolbarOkButton(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            
            let dateFormat = DateFormatter()
            
            dateFormat.dateFormat = "MM-dd-yyyyy HH:mm:ss"
            
            self.setDateTImeLBL.text = dateFormat.string(from: self.datePicker.date)
            
            self.datepickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.FullWidth, height: self.FullHeight)
            
            self.view.sendSubviewToBack(self.datepickerView)
            
            self.datepickerView.isHidden = true
            
        }
        
    }
    
    
    
    // MARK:- // Toolbar Cancel Button
    
    
    @IBOutlet weak var cancelButtonOutlet: UIButton!
    
    @IBAction func toolbarCancelButton(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            
            self.datepickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.FullWidth, height: self.FullHeight)
            
            self.view.sendSubviewToBack(self.datepickerView)
            
            self.datepickerView.isHidden = true
            
        }
        
    }
    
    
    

    
    @IBOutlet weak var tapONCalendarOutlet: UIButton!
    @IBAction func tapOnCalendar(_ sender: UIButton) {

        
        UIView.animate(withDuration: 0.5) {

            self.view.bringSubviewToFront(self.datepickerView)
            
            self.datepickerView.frame = CGRect(x: 0, y: 0, width: self.FullWidth, height: self.FullHeight)
            
            self.datepickerView.isHidden = false
            
            self.datePicker.setValue(UIColor.white, forKeyPath: "textColor")
            //self.datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
            

            self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler))
            
            self.datepickerView.addGestureRecognizer(self.tapGesture)

        }
        
        
    }
    
    
    // MARK: - Hide datepickerView when tapping outside
    
    @objc func tapGestureHandler() {
        UIView.animate(withDuration: 0.5)
        {
            self.datepickerView.frame = CGRect(x: 0, y: self.FullHeight, width: self.FullWidth, height: self.FullHeight)
            
            self.view.sendSubviewToBack(self.datepickerView)
            
            self.datepickerView.isHidden = true
        }
    }
    
    //MARK:- //Validation Check
    
    func validation()
    {
        if (self.titleTXT.text?.elementsEqual(""))!
        {
            
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "", comment: ""))
            
        }
        else if self.descriptionTXT.text.elementsEqual("")
        {
         
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "", comment: ""))
            
        }
        else if (self.mobileNoTXT.text?.elementsEqual(""))!
        {
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "aler", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "", comment: ""))
            
        }
        else if (self.setDateTImeLBL.text?.elementsEqual(""))!
        {
            
            self.ShowAlertMessage(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "alert", comment: ""), message: LocalizationSystem.sharedInstance.localizedStringForKey(key: "", comment: ""))
            
        }
    }
 
    
    // MARK:- // Save Button
    
    
    @IBOutlet weak var saveButtonOutlet: UIButton!
    @IBAction func tapOnSave(_ sender: UIButton) {
        
        self.saveData()
        
        (a, b) = (b , a)
        
        print("a",a)
        
        print("b" , b)
        
    }
    
    
    
    // MARK: - // JSON POST Method to get Third Category Table Data
    
    func saveData()
        
    {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        //dispatchGroup.enter()
        let url : URL
        
        if amIEditing == 1
        {
            url = URL(string: GLOBALAPI + "app_text_advertisement_edit")!       //change the url
            print("Edit Advertisement URL : ",url)
        }
        else
        {
            url = URL(string: GLOBALAPI + "app_text_advertisement")!       //change the url
            print("Add Advertisement URL : ",url)
        }
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        
        parameters = "lang_id=\(langID!)&user_id=\(userID!)&adv_id=\(advertisementID ?? "")&title=\(titleTXT.text!)&description=\(descriptionTXT.text ?? "")&phone=\(mobileNoTXT.text ?? "")&datetime=\(setDateTImeLBL.text ?? "")"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Save Response: " , json)
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        self.ShowAlertMessage(title: "Alert", message: "\(json["message"] ?? "")")
                    }
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    
    // MARK:- // Delegate Functions
    
    // MARK:- // Textfield Delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Character Limit of 160
        
        let newText = (descriptionTXT.text! as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        
        self.characterLimitLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "youCanAddOnly", comment: "") + " \(160-numberOfChars) " + LocalizationSystem.sharedInstance.localizedStringForKey(key: "characters", comment: "")
        
//        if numberOfChars < 0
//        {
//            self.ShowAlertMessage(title: "Alert", message: "You can not submit more than 160 Characters")
//        }
//        else
//        {
//
//        }
        
        return numberOfChars < 160
        
    }
    
    
    
    
    
    
    
    // MARK:- // Set Font
    
    func set_Font()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        
        
        titleLBL.font = UIFont(name: titleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        titleTXT.font = UIFont(name: (titleTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        descriptionLBL.font = UIFont(name: descriptionLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        descriptionTXT.font = UIFont(name: (descriptionTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        mobileNoLBL.font = UIFont(name: mobileNoLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        mobileNoTXT.font = UIFont(name: (mobileNoTXT.font?.fontName)!, size: CGFloat(Get_fontSize(size: 14)))
        setDateTImeLBL.font = UIFont(name: setDateTImeLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
        characterLimitLBL.font = UIFont(name: characterLimitLBL.font.fontName, size: CGFloat(Get_fontSize(size: 10)))
        
        okButtonOutlet.titleLabel?.font = UIFont(name: (okButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
        cancelButtonOutlet.titleLabel?.font = UIFont(name: (cancelButtonOutlet.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 14)))!
    }
    
    
    // MARK:- // Set language strings
    
    func setLanguageStrings()
    {
        self.setDateTImeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "setDateTime", comment: "")
        
        self.titleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "title", comment: "")
        self.descriptionLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "description", comment: "")
        self.mobileNoLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "mobile_number", comment: "")
        
        saveButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "save", comment: ""), for: .normal)
        
    }
    
}



