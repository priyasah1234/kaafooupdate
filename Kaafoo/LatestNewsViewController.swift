//
//  LatestNewsViewController.swift
//  Kaafoo
//
//  Created by esolz on 23/09/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import UIKit
import SVProgressHUD
import SDWebImage
import AVKit


class LatestNewsViewController: GlobalViewController {
    
    var StartValue : Int! = 0
    var PerLoad : Int! = 10
    var recordsArray : NSMutableArray! = NSMutableArray()
    let LangID = UserDefaults.standard.string(forKey: "langID")
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    @IBOutlet weak var ListingTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getListing()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- API Call Method
    
    func getListing()
    {
        let parameters = "user_id=\(userID!)&lang_id=\(LangID!)&start_value=\(StartValue!)&per_load=\(PerLoad!)"
        self.CallAPI(urlString: "latest-news", param: parameters, completion: {
            self.globalDispatchgroup.leave()
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                let totalDict = (self.globalJson["info_array"] as! NSDictionary)
                
                let tempArray = totalDict["address"] as! NSArray
                for i in 0..<tempArray.count
                {
                    self.recordsArray.add(tempArray[i] as! NSDictionary)
                }
               
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.ListingTableView.delegate = self
                    self.ListingTableView.dataSource = self
                    self.ListingTableView.reloadData()
                }
            })

        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class LatestNewsTVC : UITableViewCell
{
    @IBOutlet weak var NewsDate: UILabel!
    @IBOutlet weak var NewsText: UILabel!
    @IBOutlet weak var NewsImage: UIImageView!
    @IBOutlet weak var NewsVideo: UIView!
    
}

extension LatestNewsViewController : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.recordsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "latest") as! LatestNewsTVC
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["message"] ?? "")".elementsEqual("")
        {
            cell.NewsText.isHidden = true
        }
        else
        {
            cell.NewsText.isHidden = false
            cell.NewsText.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["message"] ?? "")"
        }
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["image_file"] ?? "")".elementsEqual("")
        {
            cell.NewsImage.isHidden = true
        }
        else
        {
            cell.NewsImage.isHidden = false
            cell.NewsImage.sd_setImage(with: URL(string: "\((self.recordsArray[indexPath.row] as! NSDictionary)["image_file"] ?? "")"))
        }
        
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["video_file"] ?? "")".elementsEqual("")
        {
            cell.NewsVideo.isHidden = true
        }
        else
        {
            let Str = "\((self.recordsArray[indexPath.row] as! NSDictionary)["video_file"] ?? "")"
            cell.NewsVideo.isHidden = false
            let videoURL = URL(string: Str)
            let player = AVPlayer(url: videoURL!)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = cell.NewsVideo.bounds
            cell.NewsVideo.layer.addSublayer(playerLayer)
            player.play()
        }
        
        cell.NewsDate.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["send_date"] ?? "")"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //do nothing
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
