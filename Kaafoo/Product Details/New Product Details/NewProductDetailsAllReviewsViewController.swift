//
//  NewProductDetailsAllReviewsViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 6/6/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class NewProductDetailsAllReviewsViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var ReviewsTableview: UITableView!
    var startValue = 0
    var perLoad = 10

    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!

    var nextStart : String!

    var productID : String!

    var productInfoDictionary : NSDictionary!

    var recordsArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.Getlist()
        // Do any additional setup after loading the view.
    }
    //MARK:- //tableview delegate and datasource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.recordsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "newReviews") as! NewProductDetailsReviewTableViewCell
        obj.ReviewerName.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["name"]!)" + "(" + "\((self.recordsArray[indexPath.row] as! NSDictionary)["total_rating"]!)" + ")"
        obj.Review.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["comment"]!)"
        obj.ReviewDate.text =  "\((self.recordsArray[indexPath.row] as! NSDictionary)["date"]!)"
        obj.RatingNo.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["rating_no"]!)"
        obj.selectionStyle = .none
        return obj
     }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    //MARK:- //Get Listing
    func Getlist()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        let langID = UserDefaults.standard.string(forKey: "langID")

        let parameters = "product_id=\(productID!)&user_id=\(userID!)&lang_id=\(langID!)&start_value=\(startValue)&per_load=\(perLoad)"

        self.CallAPI(urlString: "app_product_details", param: parameters, completion: {

            self.productInfoDictionary = self.globalJson["product_info"] as? NSDictionary

            let ReviewArray = ((self.productInfoDictionary["review_rating"] as! NSDictionary)["review_list"] as! NSArray)

            for i in 0..<ReviewArray.count

            {

                let tempDict = ReviewArray[i] as! NSDictionary

                self.recordsArray.add(tempDict as! NSMutableDictionary)

            }

            self.nextStart = "\(self.globalJson["next_start_review"]!)"

            self.startValue = self.startValue + ReviewArray.count

            print("Next Start Value : " , self.startValue)

            DispatchQueue.main.async {
                self.globalDispatchgroup.leave()

                self.ReviewsTableview.delegate = self
                self.ReviewsTableview.dataSource = self
                self.ReviewsTableview.rowHeight = UITableView.automaticDimension
                self.ReviewsTableview.estimatedRowHeight = UITableView.automaticDimension
                self.ReviewsTableview.reloadData()

                SVProgressHUD.dismiss()
            }

        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - // Scrollview Delegates

extension NewProductDetailsAllReviewsViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

//            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                self.Getlist()
            }
        }
    }
}
//MARK:- //TableviewCell
class NewProductDetailsReviewTableViewCell : UITableViewCell
{
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var ReviewerName: UILabel!
    @IBOutlet weak var ReviewDate: UILabel!
    @IBOutlet weak var Review: UILabel!
    @IBOutlet weak var RatingNo: UILabel!
    
}
