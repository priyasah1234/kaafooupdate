//
//  ClassifiedViewController.swift
//  Kaafoo
//
//  Created by IOS-1 on 13/04/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import SDWebImage
import AVKit

class ClassifiedViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var ListingView: UIView!
    @IBOutlet weak var ListingTableView: UITableView!
    let dispatchGroup = DispatchGroup()
    let userID = UserDefaults.standard.string(forKey: "userID")
    let LangID = UserDefaults.standard.string(forKey: "langID")
    var PerValue : String! = ""
    var StartValue = 0
    var recordsArray : NSMutableArray! = NSMutableArray()
    var infoArray : NSMutableArray!
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    var nextStart : String!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.CreateFavouriteView(inview: self.view)
        self.GetListing()
        self.ListingTableView.separatorStyle = .none


        // Do any additional setup after loading the view.
    }

    //MARK:- ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        self.ListingTableView.rowHeight = UITableView.automaticDimension
        self.ListingTableView.estimatedRowHeight = 300
    }

    //MARK:- //Tableview delegate and datasource methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recordsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "classified") as! ClassifiedTableViewCell
        obj.FirstSeparatorView.isHidden = true
        obj.SecondSeparatorView.isHidden = false
        obj.ClassifiedText.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["text"]!)"


        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["image"]!)".elementsEqual("")
        {
            obj.ClassifiedImageView.isHidden = true
        }
        else
        {
            obj.ClassifiedImageView.isHidden = false
            let path = "\((self.recordsArray[indexPath.row] as! NSDictionary)["image"]!)"
            obj.ClassifiedImg.sd_setImage(with: URL(string: path))
        }
        if "\((self.recordsArray[indexPath.row] as! NSDictionary)["video"]!)".elementsEqual("")
        {
            obj.ClassifiedVideoView.isHidden = true
        }
        else
        {
            obj.ClassifiedVideoView.isHidden = false
            let VideoPath = "\((self.recordsArray[indexPath.row] as! NSDictionary)["video"]!)"

            let videoURL = URL(string: VideoPath)
            let player = AVPlayer(url: videoURL!)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = obj.ClassifiedVideoView.bounds
            playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            obj.ClassifiedVideoView.layer.addSublayer(playerLayer)

            let button = UIButton(frame: obj.ClassifiedVideoView.bounds)
            button.tag = indexPath.row
            button.addTarget(self, action: #selector(Tapped(sender:)), for: .touchUpInside)
            player.play()
            player.allowsExternalPlayback = true

        }
        obj.selectionStyle = .none
        return obj
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "roombooking") as! NewHolidayRoomBookingViewController
        self.navigationController?.pushViewController(navigate, animated: true)


    }

//       let obj = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newholidaylisting") as! NewHolidayListingViewController
//        self.navigationController?.pushViewController(obj, animated: true)

    //MARK:- //Get Listing using Global API
    func GetListing()
    {
        self.PerValue = "20"
        let parameters = "user_id=\(userID!)&lang_id=\(LangID!)&per_load=\(PerValue!)&start_value=\(StartValue)"
        self.CallAPI(urlString: "app_view_classified_list", param: parameters, completion: {
            self.globalDispatchgroup.leave()

            self.globalDispatchgroup.notify(queue: .main, execute: {

                self.infoArray = self.globalJson["info_array"] as? NSMutableArray

                for i in (0..<self.infoArray.count)
                {
                    let ProDetail = self.infoArray[i] as! NSDictionary
                    self.recordsArray.add(ProDetail)
                }
                print("RecordsArray",self.recordsArray!)
                self.nextStart = "\(self.globalJson["next_start"]!)"


                self.StartValue = self.StartValue + self.infoArray.count

                SVProgressHUD.dismiss()
                self.ListingTableView.delegate = self
                self.ListingTableView.dataSource = self
                self.ListingTableView.rowHeight = UITableView.automaticDimension
                self.ListingTableView.estimatedRowHeight = 300


            })
        })
    }

    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {

        }
        else
        {

            print("next start : ",nextStart )

            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }


            }
            else
            {
                self.GetListing()
            }
        }
    }
    //MARK:- //AVPlayer Pause and Play
    @objc func Tapped(sender : UIButton)
    {
        //       if self.recordsArray[sender.tag]
    }


    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

}
//MARK:- Classified TableviewCell
class ClassifiedTableViewCell: UITableViewCell {
    @IBOutlet weak var FirstSeparatorView: UIView!
    @IBOutlet weak var TextView: UIView!
    @IBOutlet weak var ClassifiedImageView: UIView!
    @IBOutlet weak var ClassifiedVideoView: UIView!
    @IBOutlet weak var SecondSeparatorView: UIView!
    @IBOutlet weak var ClassifiedText: UILabel!
    @IBOutlet weak var ClassifiedImg: UIImageView!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
