//
//  KaafooBonusVC.swift
//  Kaafoo
//
//  Created by esolz on 18/12/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

class KaafooBonusVC: GlobalViewController,UIScrollViewDelegate {
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var SubTitleView: UIView!
    @IBOutlet weak var BonusListTV: UITableView!
    var Perload : String! = "20"
    var StartValue : Int! = 0
    var nextStart : String! = ""
    var ScrollBegin : CGFloat!
    var ScrollEnd : CGFloat!
    var ListingArray : NSMutableArray! = NSMutableArray()
    let UserID = UserDefaults.standard.string(forKey: "userID")
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getlisting()
        self.ConfigureLayout()
       
        // Do any additional setup after loading the view.
    }
    
    //MARK:- //Get Listing From API
    
    func getlisting()
    {
        let parameters = "user_id=\(UserID ?? "")&lang_id=\(LangID ?? "")&start_value=\(StartValue!)&per_load=\(Perload ?? "")"
        self.CallAPI(urlString: "app_bonus_list_info", param: parameters, completion: {
            self.globalDispatchgroup.leave()
            let infoarray = self.globalJson["info_array"] as! NSArray
            
            for i in 0..<infoarray.count
            {
                let tempdict = infoarray[i] as! NSDictionary
                self.ListingArray.add(tempdict)
            }
            self.nextStart = "\(self.globalJson["next_start"] ?? "")"
            self.StartValue = self.StartValue + self.ListingArray.count
            self.globalDispatchgroup.notify(queue: .main, execute: {
                DispatchQueue.main.async {
                    self.BonusListTV.delegate = self
                    self.BonusListTV.dataSource = self
                    self.BonusListTV.reloadData()
                    
                    SVProgressHUD.dismiss()
                }
            })
        })
    }
    
    // MARK: - // Scrollview Delegates

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
       if scrollView == self.BonusListTV
       {
        ScrollBegin = self.BonusListTV.contentOffset.y
       }
       else
       {
           
       }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
       if scrollView == self.BonusListTV
       {
        ScrollEnd = self.BonusListTV.contentOffset.y
       }
       else
       {
           
       }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
       if scrollView == self.BonusListTV
       {
           if ScrollBegin > ScrollEnd
           {
           }
           else
           {
               if (nextStart).elementsEqual("")
               {
                   DispatchQueue.main.async {
                   SVProgressHUD.dismiss()
               }
               }
               else
               {
                   self.getlisting()
               }
           }
       }
       else
       {
       }
    }
    
    //MARK:- //Configure layer and Cell,designs
    
    func ConfigureLayout()
    {
        self.BonusListTV.separatorStyle = .none
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class BonusListTVC : UITableViewCell
{
    @IBOutlet weak var ViewContent: UIView!
    @IBOutlet weak var BonusIV: UIImageView!
    @IBOutlet weak var BuyerName: UILabel!
    @IBOutlet weak var TransactionID: UILabel!
    @IBOutlet weak var OrderNO: UILabel!
    @IBOutlet weak var OrderAmount: UILabel!
    @IBOutlet weak var OrderDate: UILabel!
    
}

extension KaafooBonusVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ListingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bonuslist") as! BonusListTVC
        cell.BuyerName.text = "Buyer name : " + "\((self.ListingArray[indexPath.row] as! NSDictionary)["message"] ?? "")"
        cell.TransactionID.text = "Transaction ID : " + "\((self.ListingArray[indexPath.row] as! NSDictionary)["payment_id"] ?? "")"
        cell.OrderNO.text = "Order No : " + "\((self.ListingArray[indexPath.row] as! NSDictionary)["order_no"] ?? "")"
        cell.OrderAmount.text = "Amount : " + "\((self.ListingArray[indexPath.row] as! NSDictionary)["pay_type"] ?? "")" + "\((self.ListingArray[indexPath.row] as! NSDictionary)["currency"] ?? "")" + "\((self.ListingArray[indexPath.row] as! NSDictionary)["amount"] ?? "")"
        cell.OrderDate.text = "Date : " + "\((self.ListingArray[indexPath.row] as! NSDictionary)["payment_time"] ?? "")"
        
        if "\((self.ListingArray[indexPath.row] as! NSDictionary)["pay_type"] ?? "")".elementsEqual("+")
        {
            cell.BonusIV.image = UIImage(named: "Add")
        }
        else
        {
            cell.BonusIV.image = UIImage(named: "Delete")
        }
        cell.ViewContent.layer.cornerRadius = 8.0
        cell.ViewContent.clipsToBounds = true
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
}
