//
//  mapView.swift
//  Kaafoo
//
//  Created by Shirsendu Sekhar Paul on 14/05/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces





struct MyPlace {
    var name: String
    var lat: Double
    var long: Double
}


class mapView: UIView, CLLocationManagerDelegate, GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate, UITextFieldDelegate {




    //let sectionIndexPath = IndexPath(row: NSNotFound, section: section)


    let currentLocationMarker = GMSMarker()
    var locationManager = CLLocationManager()
    var chosenPlace: MyPlace?

    let customMarkerWidth: Int = 50
    let customMarkerHeight: Int = 70

    let previewDemoData = [(title: "The Polar Junction", img: #imageLiteral(resourceName: "restaurant1"), price: 10), (title: "The Nifty Lounge", img: #imageLiteral(resourceName: "Daily Deals"), price: 8), (title: "The Lunar Petal", img: #imageLiteral(resourceName: "restaurant3"), price: 12)]

    var mapDataArray = [NSDictionary]()
    var listingDataArray = [NSDictionary]()

    var mapBounds = GMSCoordinateBounds()

    @IBOutlet var contentView: UIView!

    // MARK:- // Override Init

    override init(frame: CGRect) {
        super.init(frame: frame)

    }


    // MARK:- // Required Init

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        Bundle.main.loadNibNamed("mapView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        //        self.title = "Home"
        //        self.view.backgroundColor = UIColor.white
        //myMapView.delegate=self

        //        locationManager.delegate = self
        //        locationManager.requestWhenInUseAuthorization()
        //        locationManager.startUpdatingLocation()
        //        locationManager.startMonitoringSignificantLocationChanges()

        setupViews()

        //initGoogleMaps()

        txtFieldSearch.delegate=self

    }


    //MARK: textfield
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self

        let filter = GMSAutocompleteFilter()
        autoCompleteController.autocompleteFilter = filter

        self.locationManager.startUpdatingLocation()
        //self.present(autoCompleteController, animated: true, completion: nil)
        return false
    }

    // MARK: GOOGLE AUTO COMPLETE DELEGATE
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let lat = place.coordinate.latitude
        let long = place.coordinate.longitude

        showPartyMarkers(mapDataArray: self.mapDataArray, listingDataArray: self.listingDataArray)

        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 0)
        myMapView.camera = camera
        txtFieldSearch.text=place.formattedAddress
        chosenPlace = MyPlace(name: place.formattedAddress!, lat: lat, long: long)
        let marker=GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.title = "\(place.name ?? "")"
        marker.snippet = "\(place.formattedAddress!)"
        marker.map = myMapView

        //self.dismiss(animated: true, completion: nil) // dismiss after place selected
    }

    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
    }

    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //self.dismiss(animated: true, completion: nil)
    }

    func initGoogleMaps() {
        //        let camera = GMSCameraPosition.camera(withLatitude: 28.7041, longitude: 77.1025, zoom: 17.0)
        //        self.myMapView.camera = camera

        self.myMapView.delegate = self
        self.myMapView.isMyLocationEnabled = true
    }

    // MARK: CLLocation Manager Delegate

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while getting location \(error)")
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //locationManager.delegate = nil
        //locationManager.stopUpdatingLocation()
        let location = locations.last
//        var lat = (location?.coordinate.latitude)!
//        var long = (location?.coordinate.longitude)!
       // let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 17.0)

        //self.myMapView.animate(to: camera)

        showPartyMarkers(mapDataArray: self.mapDataArray, listingDataArray: self.listingDataArray)

        let update = GMSCameraUpdate.fit(mapBounds, withPadding: 100)
        myMapView.animate(with: update)
    }

    // MARK: GOOGLE MAP DELEGATE
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let customMarkerView = marker.iconView as? CustomMarkerView else { return false }
        let img = customMarkerView.img!
        let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: customMarkerWidth, height: customMarkerHeight), image: img, borderColor: UIColor.white, tag: customMarkerView.tag)

        marker.iconView = customMarker

        return false
    }

//    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
//        guard let customMarkerView = marker.iconView as? CustomMarkerView else { return nil }
//        let data = previewDemoData[customMarkerView.tag]
//        restaurantPreviewView.setData(title: data.title, img: data.img, price: data.price)
//        return restaurantPreviewView
//    }

    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        guard let customMarkerView = marker.iconView as? CustomMarkerView else { return }
        let tag = customMarkerView.tag
        restaurantTapped(tag: tag)
    }

    func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
        guard let customMarkerView = marker.iconView as? CustomMarkerView else { return }
        let img = customMarkerView.img!
        let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: customMarkerWidth, height: customMarkerHeight), image: img, borderColor: UIColor.darkGray, tag: customMarkerView.tag)
        marker.iconView = customMarker
    }

    //    func showPartyMarkers(lat: Double, long: Double) {
    //        myMapView.clear()
    //        for i in 0..<3 {
    //            let randNum=Double(arc4random_uniform(30))/10000
    //            let marker=GMSMarker()
    //            let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: customMarkerWidth, height: customMarkerHeight), image: previewDemoData[i].img, borderColor: UIColor.darkGray, tag: i)
    //            marker.iconView=customMarker
    //            let randInt = arc4random_uniform(4)
    //            if randInt == 0 {
    //                marker.position = CLLocationCoordinate2D(latitude: lat+randNum, longitude: long-randNum)
    //            } else if randInt == 1 {
    //                marker.position = CLLocationCoordinate2D(latitude: lat-randNum, longitude: long+randNum)
    //            } else if randInt == 2 {
    //                marker.position = CLLocationCoordinate2D(latitude: lat-randNum, longitude: long-randNum)
    //            } else {
    //                marker.position = CLLocationCoordinate2D(latitude: lat+randNum, longitude: long+randNum)
    //            }
    //            marker.map = self.myMapView
    //        }
    //    }

    @objc func btnMyLocationAction() {
        let location: CLLocation? = myMapView.myLocation
        if location != nil {
            myMapView.animate(toLocation: (location?.coordinate)!)
        }
    }

    @objc func restaurantTapped(tag: Int) {
        let v=DetailsVC()
        v.passedData = previewDemoData[tag]
        //self.navigationController?.pushViewController(v, animated: true)
    }

    func setupTextField(textField: UITextField, img: UIImage){
        textField.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 20, height: 20))
        imageView.image = img
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 30, height: 30))
        paddingView.addSubview(imageView)
        textField.leftView = paddingView
    }

    func setupViews() {
        contentView.addSubview(myMapView)
        myMapView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive=true
        myMapView.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive=true
        myMapView.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive=true
        myMapView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 60).isActive=true

        contentView.addSubview(txtFieldSearch)
        txtFieldSearch.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive=true
        txtFieldSearch.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive=true
        txtFieldSearch.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive=true
        txtFieldSearch.heightAnchor.constraint(equalToConstant: 35).isActive=true
        setupTextField(textField: txtFieldSearch, img: #imageLiteral(resourceName: "map_Pin"))

        restaurantPreviewView=RestaurantPreviewView(frame: CGRect(x: 0, y: 0, width: contentView.frame.width, height: 190))

        contentView.addSubview(btnMyLocation)
        btnMyLocation.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -30).isActive=true
        btnMyLocation.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive=true
        btnMyLocation.widthAnchor.constraint(equalToConstant: 50).isActive=true
        btnMyLocation.heightAnchor.constraint(equalTo: btnMyLocation.widthAnchor).isActive=true
    }

    let myMapView: GMSMapView = {
        let v=GMSMapView()
        v.translatesAutoresizingMaskIntoConstraints=false
        return v
    }()

    let txtFieldSearch: UITextField = {
        let tf=UITextField()
        tf.borderStyle = .roundedRect
        tf.backgroundColor = .white
        tf.layer.borderColor = UIColor.darkGray.cgColor
        tf.placeholder="Search for a location"
        tf.translatesAutoresizingMaskIntoConstraints=false
        return tf
    }()

    let btnMyLocation: UIButton = {
        let btn=UIButton()
        btn.backgroundColor = UIColor.white
        btn.setImage(#imageLiteral(resourceName: "my_location"), for: .normal)
        btn.layer.cornerRadius = 25
        btn.clipsToBounds=true
        btn.tintColor = UIColor.gray
        btn.imageView?.tintColor=UIColor.gray
        btn.addTarget(self, action: #selector(btnMyLocationAction), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints=false
        return btn
    }()

    var restaurantPreviewView: RestaurantPreviewView = {
        let v=RestaurantPreviewView()
        return v
    }()



    // MARK:- // Get Data

    func getData(dataArray: [NSDictionary] , listingDataArray : [NSDictionary]) {

        print(dataArray)

        self.mapDataArray = dataArray
        self.listingDataArray = listingDataArray

        self.myMapView.delegate = self


        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()

        //initGoogleMaps()


    }


    // MARK:- // Show Party Markers

    func showPartyMarkers(mapDataArray: [NSDictionary] , listingDataArray : [NSDictionary]) {

        myMapView.clear()
        for i in 0..<mapDataArray.count {

            let marker=GMSMarker()
            let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: customMarkerWidth, height: customMarkerHeight), image: UIImage(named: "home-marker")!, borderColor: UIColor.clear, tag: i)
            marker.iconView=customMarker

            if "\((mapDataArray[i])["lat"] ?? "")".elementsEqual("") == false && "\((mapDataArray[i])["long"] ?? "")".elementsEqual("") == false {

                print("coordinate\(i)-----","\(Double("\((mapDataArray[i])["lat"]!)")!),\(Double("\((mapDataArray[i])["long"]!)")!)")

                marker.position = CLLocationCoordinate2D(latitude: Double("\((mapDataArray[i])["lat"]!)")!, longitude: Double("\((mapDataArray[i])["lat"]!)")!)

                mapBounds = mapBounds.includingCoordinate(marker.position)
            }



            marker.map = self.myMapView

            // mapBounds = mapBounds.includingCoordinate(marker.position)

        }

    }




}



