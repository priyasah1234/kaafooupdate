//
//  SearchView.swift
//  Kaafoo
//
//  Created by admin on 22/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class SearchView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var closeSearchviewButton: UIButton!


    override init(frame: CGRect) {
        super.init(frame: frame)

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        Bundle.main.loadNibNamed("SearchView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]

    }

}
