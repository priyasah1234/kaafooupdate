//
//  RechargeWalletViewController.swift
//  Kaafoo
//
//  Created by esolz on 18/10/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import Stripe
import WebKit
import SVProgressHUD

class RechargeWalletViewController: GlobalViewController,STPPaymentCardTextFieldDelegate,UIWebViewDelegate,WKNavigationDelegate{
 
    @IBOutlet weak var WkWebView: WKWebView!
    @IBOutlet weak var webView: UIView!
    let paymentCardTextField = STPPaymentCardTextField()
    var CardName : String! = "Sumanta Ghosh"
    var CardNumber : String! = "4242424242424242"
    var CVVNumber : String! = "242"
    var PayType : String! = "S"
    var PayPalType : String! = "P"
    var StripeBool : Bool! = true
    
    
   
    @IBAction func PaypalBtn(_ sender: UIButton) {
        self.StripeBool = false
    self.PayNowBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "pay_with_paypal", comment: ""), for: .normal)
        
    }
    @IBAction func StripeBtn(_ sender: UIButton) {
        self.StripeBool = true
    self.PayNowBtnOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "pay_now", comment: ""), for: .normal)
        
    }
    @IBOutlet weak var PaypalLbl: UILabel!
    @IBOutlet weak var StripeLbl: UILabel!
    @IBOutlet weak var PaypalBtnOutlet: UIButton!
    @IBOutlet weak var StripeBtnOutlet: UIButton!
    @IBOutlet weak var EnterNameTxt: UITextField!
    @IBOutlet weak var EnterAmountTxt: UITextField!
    @IBOutlet weak var StripeCardView: UIView!
    @IBOutlet weak var PayNowBtnOutlet: UIButton!
    @IBAction func PayNowBtn(_ sender: UIButton) {
        if self.EnterAmountTxt.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter the amount")
        }
        else if self.EnterNameTxt.text?.isEmpty == true
        {
            self.ShowAlertMessage(title: "Warning", message: "Please Enter the Card Holder Name")
        }
        else
        {
            if self.StripeBool == true
            {
                self.getToken()
            }
            else
            {
                self.setPaypal()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.StripeBool = true
         self.WkWebView.navigationDelegate = self
        paymentCardTextField.delegate = self
        self.StripeCardView.addSubview(paymentCardTextField)

        // Do any additional setup after loading the view.
    }
    
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        // Toggle buy button state
        if paymentCardTextField.isValid
        {
            self.PayNowBtnOutlet.isUserInteractionEnabled = true
        }
    }
    
    private func getToken(){
        let cardParams = STPCardParams()
//        cardParams.number = paymentCardTextField.cardNumber
//        cardParams.expMonth = (paymentCardTextField.expirationMonth)
//        cardParams.expYear = (paymentCardTextField.expirationYear)
//        cardParams.cvc = paymentCardTextField.cvc
        
        
        cardParams.number = "4242424242424242"
        cardParams.expMonth = 04
        cardParams.expYear = 34
        cardParams.cvc = "333"
        
        print("CardNumber",cardParams.number ?? "")
        print("CVCNumber",cardParams.cvc ?? "")
        
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            if let error = error {
            print ("Error",error)
            } else if let token = token
            {
            print ("Token",token)
            }
            guard let token = token, error == nil else {
              print("STripeToken")
                return
            }
            self.postStripeToken(token: token)
        }

    }
    
    func postStripeToken(token : STPToken)
    {
        let UserID = UserDefaults.standard.string(forKey: "userID")
               let LangID = UserDefaults.standard.string(forKey: "langID")
        let parameters = "lang_id=\(LangID!)&user_id=\(UserID!)&stripe_token=\(token)&card_name=\(self.EnterAmountTxt.text ?? "")&amount=\(self.EnterAmountTxt.text ?? "")&card_no=\(self.CardNumber ?? "")&cvv_no=\(self.CVVNumber ?? "")&pay_type=\(self.PayType ?? "")"
        self.CallAPI(urlString: "wallet_recharge_save", param: parameters, completion: {
            self.globalDispatchgroup.leave()
            self.globalDispatchgroup.notify(queue: .main, execute: {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.SetParametersBlank()
                    self.ShowAlertMessage(title: "Warning", message: "\(self.globalJson["message"] ?? "")")
                }
                 
            })
           
        })
    }
    
    func SetParametersBlank()
    {
        self.EnterAmountTxt.text = ""
        self.EnterNameTxt.text = ""
    }
    
    func PayPalPayment()
    {
        let UserID = UserDefaults.standard.string(forKey: "userID")
        let parameters = "user_id=\(UserID!)&pay_type=\(PayPalType!)&amount=\(self.EnterAmountTxt.text ?? "")"
        self.CallAPI(urlString: "wallet_recharge_save", param: parameters, completion: {
            self.globalDispatchgroup.leave()
        })
    }
    
    func setPaypal()
    {
        self.webView.isHidden = false
        let UserID = UserDefaults.standard.string(forKey: "userID")
        let myUrl = "https://staging.esolzbackoffice.com/kaafoo/web/appcontrol/wallet_recharge_save?user_id=\(UserID!)&pay_type=\(PayPalType!)&amount=\(self.EnterAmountTxt.text ?? "")"
        let url = URL(string: myUrl)
        let requestObj = URLRequest(url: url!)
        WkWebView.load(requestObj)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void)
     {
       let response = navigationResponse.response as? HTTPURLResponse
           decisionHandler(.allow)
        print("response",response!)
    }
    
   func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

    debugPrint("didFinish")

    WkWebView.evaluateJavaScript("document.documentElement.outerHTML.toString()",

                                  completionHandler: { (html: Any?, error: Error?) in
            //print(html)
                                    var doc = html as! String
                                    doc = doc.replacingOccurrences(of: "<html>", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "</html>", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "<head>", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "</head>", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "<body>", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "</body>", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "<p>", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "</p>", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "<p style=\"display:none\">", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "<span style=", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "\"color:red;\">", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "\"color:white;\">", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    doc = doc.replacingOccurrences(of: "</span>", with: "", options: NSString.CompareOptions.literal, range: nil)

                                    if doc.hasPrefix("{") && doc.hasSuffix("}") {
                                        let jsonData = doc.data(using: .utf8)!
                                        let json = try! JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? NSDictionary

                                        print(json!)
                                        DispatchQueue.main.async {
                                            self.webView.isHidden = true
                                            self.ShowAlertMessage(title: "Warning", message: "\(json!["message"] ?? "")")
                                            self.SetParametersBlank()
                                        }
                                    }

                    })
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
