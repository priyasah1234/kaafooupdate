//
//  SecondViewController.swift
//  DemoCollectioView
//
//  Created by IOS-1 on 22/04/19.
//  Copyright © 2019 IOS-1. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD
import PushNotifications



class LatestHomeViewController: GlobalViewController,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    
//    var appDelegate: AppDelegate!
//    let UserID = UserDefaults.standard.string(forKey: "userID")
    
    var AutoScroll : Bool! = true
    
    var MainScrollView : UIScrollView = {
        
        let mainscrollview = UIScrollView()
        
        mainscrollview.showsHorizontalScrollIndicator = false
        
        mainscrollview.showsVerticalScrollIndicator = false
        
        mainscrollview.translatesAutoresizingMaskIntoConstraints = false
        
        return mainscrollview
    }()

    var ScrollContentView : UIView = {
        
        let scrollcontentview = UIView()
        
        scrollcontentview.translatesAutoresizingMaskIntoConstraints = false
        
        return scrollcontentview
    }()
    
    var mainStackView : UIStackView = {

        let mainstackview = UIStackView()
        
        mainstackview.axis = .vertical
        
        mainstackview.alignment = .fill // .leading .firstBaseline .center .trailing .lastBaseline
        mainstackview.distribution = .fill // .fillEqually .fillProportionally .equalSpacing .equalCentering
        mainstackview.spacing = 10

        //mainstackview.backgroundColor = .magenta
        mainstackview.translatesAutoresizingMaskIntoConstraints = false
        
        return mainstackview

    }()
    
    var DetailsView : UIView = {
        
        let detailsview = UIView()
        
        detailsview.translatesAutoresizingMaskIntoConstraints = false
        
        return detailsview
    }()
    
    var classifiedView : UIView = {
        
        let classifiedview = UIView()
        
        classifiedview.translatesAutoresizingMaskIntoConstraints = false
        
        return classifiedview
    }()

    var footerAdvertisementView : UIView = {
        
        let footeradvertisementview = UIView()
        
        footeradvertisementview.translatesAutoresizingMaskIntoConstraints = false
        
        return footeradvertisementview
    }()


    @IBOutlet weak var sampleLBL: UILabel!

    let dispatchGroup = DispatchGroup()

    var mergedViewArray = [UIStackView]()
    
    var viewAllButtonArray = [UIButton]()

    var allDataDictionary : NSDictionary!

    var allDataArray = [dataStructure]()
    
    var allCategoriesArray : NSArray!
    
    var allAdvertisementArray : NSArray!
    
    var featureDict : NSDictionary!
    
    var footerArray : NSArray!
    
    var classifiedDict : NSDictionary!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.addSubview(footerView)
        
        self.MainScrollView.bringSubviewToFront(footerView)
        
        if isKeyPresentInUserDefaults(key: "userID") == false {
            UserDefaults.standard.set("", forKey: "userID")
        }

        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)

        self.DataFetch()
            

        self.dispatchGroup.notify(queue: .main) {

            self.createDataArrays()

            self.setPageLayout()
            
            SVProgressHUD.dismiss()
        }
        

//        let options = PusherClientOptions(
//            host: .cluster("ap2")
//        )
//        let pusher = Pusher(key: "e53a9c4a375968423d66", options: options)
//        pusher.connect()
//
//        let myChannel = pusher.subscribe("my-notification-channel")
//
//        let _ = myChannel.bind(eventName: "my-noti-event", callback: { (data: Any?) -> Void in
//            if let data = data as? [String : AnyObject] {
//                if let message = data["message"] as? String {
//                    print(message)
//                }
//            }
//        })



    }
    
       


    //MARK:- //Fetch Data From API
    func DataFetch()
    {

        self.dispatchGroup.enter()

        let url = URL(string: GLOBALAPI + "app_api_homepage")!   //change the url

        var parameters : String = ""

        let userID = UserDefaults.standard.string(forKey: "userID")

        if isKeyPresentInUserDefaults(key: "userID") == true {
            parameters = "user_id=\(userID!)"
        }
        else {
            parameters = "user_id="
        }
        
        

        print("URL : ",url)
        print("Parameters are : " , parameters)

        let session = URLSession.shared

        var request = URLRequest(url: url)

        request.httpMethod = "POST" //set http method as POST

        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
        }

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            do {

                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {

                    if (json["response"] as! Bool) == true
                    {

                        self.allDataDictionary = json["info_array"] as? NSDictionary

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                        }

                    }

                    else
                    {
                        print("Do nothing")

                        DispatchQueue.main.async {

                            self.dispatchGroup.leave()

                        }
                    }

                }

            } catch let error {
                print(error.localizedDescription)
            }
        })

        task.resume()
    }



    // MARK:- // Create Data Arrays

    func createDataArrays() {

        self.featureDict = self.allDataDictionary["feature_product"] as? NSDictionary

        self.allCategoriesArray = self.allDataDictionary["cat_product"] as? NSArray

        self.allAdvertisementArray = self.allDataDictionary["advertisement"] as? NSArray

        self.footerArray = self.allDataDictionary["footer_logo"] as? NSArray

        self.classifiedDict = self.allDataDictionary["classified_list"] as? NSDictionary

        let tempArray = NSMutableArray()


        for i in 0..<self.allAdvertisementArray.count {

            if i == 0 {
                tempArray.add(dataStructure(category: featureDict, advertisement: ((self.allAdvertisementArray[i] as! NSDictionary)["adv"] as! NSArray)))
            }
            else {
                tempArray.add(dataStructure(category: self.allCategoriesArray[i-1] as! NSDictionary, advertisement: ((self.allAdvertisementArray[i] as! NSDictionary)["adv"] as! NSArray)))
            }

        }

        let tempCount = self.allAdvertisementArray.count - 1

        if self.allCategoriesArray.count > tempCount {
            for i in 0..<(self.allCategoriesArray.count - (self.allAdvertisementArray.count - 1)) {
                tempArray.add(dataStructure(category: self.allCategoriesArray[tempCount + i] as! NSDictionary, advertisement: []))
            }
        }


        self.allDataArray = tempArray.mutableCopy() as! [dataStructure]

    }








    // MARK:- // Collectionview Delegates



    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView.tag == 999 { // Classified
            return (self.classifiedDict["product"] as! NSArray).count
        }
        else {
            return ((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray).count
        }

    }



    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {


        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath as IndexPath)

        for view in cell.subviews {
            view.removeFromSuperview()
        }

        cell.backgroundColor = .white

        let shadowView : ShadowView = {
            let shadowview = ShadowView()
            shadowview.translatesAutoresizingMaskIntoConstraints = false
            return shadowview
        }()

        cell.addSubview(shadowView)

        shadowView.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: cell.bottomAnchor, trailing: cell.trailingAnchor , padding: .init(top: 5, left: 5, bottom: 5, right: 5))


        let cellView : UIView = {
            let cellview = UIView()
            cellview.backgroundColor = .white
            cellview.layer.cornerRadius = 8
            cellview.translatesAutoresizingMaskIntoConstraints = false
            return cellview
        }()

        cell.addSubview(cellView)

        cellView.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: cell.bottomAnchor, trailing: cell.trailingAnchor , padding: .init(top: 5, left: 5, bottom: 5, right: 5))


        if collectionView.tag == 999 { // classified

            let tempdict = (self.classifiedDict["product"] as! NSArray)[indexPath.item] as! NSDictionary

            let cellTitle : UILabel = {
                let celltitle = UILabel()
                celltitle.textAlignment = .center
                celltitle.font = UIFont(name: self.headerView.headerViewTitle.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                celltitle.translatesAutoresizingMaskIntoConstraints = false
                return celltitle
            }()

            let cellDescription : UILabel = {
                let celldescription = UILabel()
                celldescription.textAlignment = .center
                //celldescription.backgroundColor = .red
                celldescription.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                celldescription.numberOfLines = 0
                celldescription.translatesAutoresizingMaskIntoConstraints = false
                return celldescription
            }()

            let phoneNo : UILabel = {
                let phoneno = UILabel()
                phoneno.textAlignment = .center
                phoneno.backgroundColor = .green
                phoneno.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                phoneno.translatesAutoresizingMaskIntoConstraints = false
                return phoneno
            }()

            cellView.addSubview(cellTitle)
            cellView.addSubview(cellDescription)
            cellView.addSubview(phoneNo)

            cellTitle.anchor(top: cellView.topAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 15, left: 10, bottom: 0, right: 10), size: .init(width: 0, height: 25))

            phoneNo.anchor(top: nil, leading: cellView.leadingAnchor, bottom: cellView.bottomAnchor, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 10, right: 10), size: .init(width: 0, height: 30))

            cellDescription.anchor(top: cellTitle.bottomAnchor, leading: cellView.leadingAnchor, bottom: phoneNo.topAnchor, trailing: cellView.trailingAnchor, padding: .init(top: 5, left: 10, bottom: 10, right: 10))


            cellTitle.text = "\(tempdict["title"] ?? "")"
            cellDescription.text = "\(tempdict["description"] ?? "")"
            phoneNo.text = "\(tempdict["phone_no"] ?? "")"




        }
        else {


            let tempDict = (((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.item] as! NSDictionary)




            let cellImage : UIImageView = {
                let cellimage = UIImageView()
                cellimage.contentMode = .scaleAspectFill
                cellimage.clipsToBounds = true
                cellimage.translatesAutoresizingMaskIntoConstraints = false
                return cellimage
            }()

            cellView.addSubview(cellImage)

            cellImage.anchor(top: cellView.topAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 0, right: 5), size: .init(width: 0, height: 150))

            let cellTitle : UILabel = {
                let celltitle = UILabel()
                //celltitle.isEditable = false
                //celltitle.isScrollEnabled = false
                celltitle.textAlignment = .natural
                celltitle.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                //celltitle.backgroundColor = .red
                celltitle.translatesAutoresizingMaskIntoConstraints = false
                return celltitle
            }()

            cellView.addSubview(cellTitle)

            cellTitle.anchor(top: cellImage.bottomAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 0, right: 5), size: .init(width: 0, height: 25))

            let price : UILabel = {
                let price = UILabel()
                //price.isEditable = false
                price.textColor = .darkGray
                //price.isScrollEnabled = false
                price.textAlignment = .natural
                price.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                //price.backgroundColor = .green
                price.translatesAutoresizingMaskIntoConstraints = false
                return price
            }()

            cellView.addSubview(price)

            price.anchor(top: cellTitle.bottomAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 5, bottom: 0, right: 5), size: .init(width: 0, height: 25))


            let reservedPrice : UILabel = {
                let reservedprice = UILabel()
                //reservedprice.isEditable = false
                reservedprice.textColor = .red
                //reservedprice.isScrollEnabled = false
                reservedprice.textAlignment = .natural
                reservedprice.font = UIFont(name: self.sampleLBL.font.fontName, size: CGFloat(self.Get_fontSize(size: 14)))
                //reservedprice.backgroundColor = .orange
                reservedprice.translatesAutoresizingMaskIntoConstraints = false
                return reservedprice
            }()

            cellView.addSubview(reservedPrice)

            reservedPrice.anchor(top: price.bottomAnchor, leading: cellView.leadingAnchor, bottom: nil, trailing: cellView.trailingAnchor, padding: .init(top: 0, left: 5, bottom: 0, right: 5), size: .init(width: 0, height: 25))



            // Image and Title Checking for Job and Others

            if "\(((self.allDataArray[collectionView.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("12") {

                cellImage.sd_setImage(with: URL(string: "\(tempDict["company_logo"] ?? "")"))

                cellTitle.text = "\(tempDict["company_name"] ?? "")"

            }
            else {

                cellImage.sd_setImage(with: URL(string: "\(tempDict["product_image"] ?? "")"))

                cellTitle.text = "\(tempDict["product_name"] ?? "")"

            }




            // Price and Reserved Price Checking

            if "\(tempDict["special_offer"] ?? "")".elementsEqual("0")
            {
                price.text = "\(tempDict["currency_symbol"] ?? "")" + "\(tempDict["start_price"] ?? "")"

                price.textColor = .darkGray

                reservedPrice.isHidden = true
            }
            else {
                reservedPrice.text = "\(tempDict["currency_symbol"] ?? "")" + "\(tempDict["reserve_price"] ?? "")"

                price.textColor = .darkGray
                reservedPrice.textColor = .red

                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(tempDict["currency_symbol"] ?? "")" + "\(tempDict["start_price"] ?? "")")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))

                price.attributedText = attributeString

                reservedPrice.isHidden = false

            }
        }



        return cell
    }






    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        var topCatID : String!

        if "\(((self.allDataArray[collectionView.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("12") {
            topCatID = "\(((self.allDataArray[collectionView.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")"
        }
        else {
            topCatID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["top_cat"] ?? "")"
        }




        // Daily Deals
        if topCatID.elementsEqual("10") {

//            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "dailydealsd") as! DailyDealsDetailsViewController

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
            navigate.ProductID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")" 
            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Marketplace
        else if topCatID.elementsEqual("9") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newproductdetails") as! NewProductDetailsViewController
            navigate.ProductID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"
            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Services
        else if topCatID.elementsEqual("11") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "serviceBookingInformationVC") as! serviceBookingInformationViewController

            navigate.productID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Real Estate
        else if topCatID.elementsEqual("999") {

            let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "newRealEstateDetailsVC") as! NewRealEstateDetailsViewController

            nav.productID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"

            self.navigationController?.pushViewController(nav, animated: true)

        }

            // Jobs
        else if topCatID.elementsEqual("12") {

           // let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "jobDetailsVC") as! JobDetailsViewController
            
            let navigate = UIStoryboard(name: "Kaafoo", bundle: nil).instantiateViewController(withIdentifier: "jobdetails") as! NewJobDetailsViewController

            navigate.JobId = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Daily Rental
        else if topCatID.elementsEqual("15") {

             let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "dailyrental") as! NewDailyRentalProductDetailsViewController

            nav.DailyRentalProductID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"

            self.navigationController?.pushViewController(nav, animated: true)

        }

            // Happening
        else if topCatID.elementsEqual("14") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "happeningDetailsVC") as! HappeningDetailsViewController

            navigate.eventID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"

            self.navigationController?.pushViewController(navigate, animated: true)


        }
        
            // Holiday Accomodation
        else if topCatID.elementsEqual("16") {
            
            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newholidaydetails") as! NewHolidayDetailsViewController
            
            navigate.ProductID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["product_id"] ?? "")"
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
            
        }
        
            //Food Court
        else if topCatID.elementsEqual("13") {
            
            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newfoodlisting") as! NewFoodListingViewController
            
            navigate.sellerID = "\((((self.allDataArray[collectionView.tag].category)!["product"] as! NSArray)[indexPath.row] as! NSDictionary)["seller_id"] ?? "")"
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
            
        }

    }



    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if collectionView.tag == 999 { // classified
            let cellSize =  CGSize(width: (self.FullWidth - (60/320)*self.FullWidth) / 2, height: 180)

            return cellSize
        }
        else {
            let cellSize =  CGSize(width: (self.FullWidth - (60/320)*self.FullWidth) / 2, height: 250)

            return cellSize
        }



    }




    // MARK;- // Set Page Layout

    func setPageLayout() {

        self.view.addSubview(self.MainScrollView)
        self.MainScrollView.addSubview(self.ScrollContentView)
        self.ScrollContentView.addSubview(self.mainStackView)
        self.mainStackView.addArrangedSubview(self.DetailsView)
        self.mainStackView.addArrangedSubview(self.classifiedView)
        self.mainStackView.addArrangedSubview(self.footerAdvertisementView)

        let safeGuide = self.view.safeAreaLayoutGuide
//        self.MainScrollView.anchor(top: self.headerView.bottomAnchor, leading: self.view.leadingAnchor, bottom: safeGuide.bottomAnchor, trailing: self.view.trailingAnchor)
        
        self.MainScrollView.anchor(top: self.headerView.bottomAnchor, leading: self.view.leadingAnchor
            , bottom: safeGuide.bottomAnchor, trailing: self.view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 30, right: 0))

        self.ScrollContentView.anchor(top: self.MainScrollView.topAnchor, leading: self.MainScrollView.leadingAnchor, bottom: self.MainScrollView.bottomAnchor, trailing: self.MainScrollView.trailingAnchor, size: .init(width: self.FullWidth, height: 0))

        self.mainStackView.anchor(top: self.ScrollContentView.topAnchor, leading: self.ScrollContentView.leadingAnchor, bottom: self.ScrollContentView.bottomAnchor, trailing: self.ScrollContentView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 20, right: 0))




        for i in 0..<self.allDataArray.count {

            if i == 0 {
                createMergedView(yAnchor: self.DetailsView.topAnchor, isLastView: false, index: i)
            }
            else if i == (self.allDataArray.count - 1) {
                createMergedView(yAnchor: self.mergedViewArray[i-1].bottomAnchor, isLastView: true, index: i)
            }
            else {
                createMergedView(yAnchor: self.mergedViewArray[i-1].bottomAnchor, isLastView: false, index: i)
            }

        }


        // create classified view

        self.createClassifiedView(insideOfView: self.classifiedView, dataDict: self.classifiedDict)

        if (self.classifiedDict["product"] as! NSArray).count > 0 {
            self.classifiedView.isHidden = false
        }
        else {
            self.classifiedView.isHidden = true
        }



        // create Footer advertisement view

        createFooterAdvertisement(viewOnTop: self.footerAdvertisementView, dataArray: self.footerArray, keyString: "footer_image", contentMode: .scaleAspectFit)

    }


    // MARK:- // Create Merged View

    func createMergedView(yAnchor : NSLayoutYAxisAnchor , isLastView : Bool , index : Int)
    {


        let mergedView : UIStackView = {

            let mergedview = UIStackView()
            mergedview.axis = .vertical
            mergedview.alignment = .fill // .leading .firstBaseline .center .trailing .lastBaseline
            mergedview.distribution = .fill // .fillEqually .fillProportionally .equalSpacing .equalCentering
            mergedview.spacing = 10

            //mergedview.backgroundColor = .magenta
            mergedview.translatesAutoresizingMaskIntoConstraints = false
            return mergedview

        }()

        self.mergedViewArray.append(mergedView)

        let viewOfListCollectionview : UIView = {

            let viewoflistcollectionview = UIView()
            //viewoflistcollectionview.backgroundColor = .black
            viewoflistcollectionview.translatesAutoresizingMaskIntoConstraints = false
            return viewoflistcollectionview

        }()

        let viewOfAdvertisementview : UIView = {

            let viewOfAdvertisementview = UIView()
            //viewOfAdvertisementview.backgroundColor = .orange
            viewOfAdvertisementview.translatesAutoresizingMaskIntoConstraints = false
            return viewOfAdvertisementview

        }()

        self.DetailsView.addSubview(mergedView)

        mergedView.addArrangedSubview(viewOfAdvertisementview)
        mergedView.addArrangedSubview(viewOfListCollectionview)

        if isLastView == true {
            mergedView.anchor(top: yAnchor, leading: self.DetailsView.leadingAnchor, bottom: self.DetailsView.bottomAnchor, trailing: self.DetailsView.trailingAnchor)
        }
        else {
            mergedView.anchor(top: yAnchor, leading: self.DetailsView.leadingAnchor, bottom: nil, trailing: self.DetailsView.trailingAnchor)
        }

        self.createAdvertisement(viewOnTop: viewOfAdvertisementview , dataArray: self.allDataArray[index].advertisement!, keyString: "image", contentMode: .scaleAspectFill)

        self.createListCollectionview(viewOnTop: viewOfListCollectionview, isLastView: false, dataDict: self.allDataArray[index].category!, index: index)

        if ((self.allDataArray[index].category)!["product"] as! NSArray).count == 0 {
            viewOfListCollectionview.isHidden = true
        }
        else {
            viewOfListCollectionview.isHidden = false
        }

        if ((self.allDataArray[index].advertisement)!).count == 0 {
            viewOfAdvertisementview.isHidden = true
        }
        else {
            viewOfAdvertisementview.isHidden = false
        }

    }



    // MARK:- // Create Advertisement

    func createAdvertisement(viewOnTop : UIView , dataArray : NSArray , keyString : String , contentMode : UIView.ContentMode) {

        var imageviewArray = [UIImageView]()

        /*
         let shadowView : ShadowView = {
         let shadowview = ShadowView()
         shadowview.translatesAutoresizingMaskIntoConstraints = false
         return shadowview
         }()

         viewOnTop.addSubview(shadowView)

         shadowView.anchor(top: viewOnTop.topAnchor, leading: viewOnTop.leadingAnchor, bottom: viewOnTop.bottomAnchor, trailing: viewOnTop.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 200))
         */
        let scrollView : UIScrollView = {
            let scrollview = UIScrollView()
            scrollview.isPagingEnabled = true
            scrollview.layer.cornerRadius = 8
            scrollview.showsVerticalScrollIndicator = false
            scrollview.showsHorizontalScrollIndicator = false
            scrollview.translatesAutoresizingMaskIntoConstraints = false
            //scrollview.backgroundColor = .red
            return scrollview
        }()


        let scrollContentview : UIView = {
            let scrollcontentview = UIView()
            scrollcontentview.translatesAutoresizingMaskIntoConstraints = false
            //scrollcontentview.backgroundColor = .green
            return scrollcontentview
        }()


        viewOnTop.addSubview(scrollView)
        scrollView.addSubview(scrollContentview)


        scrollView.anchor(top: viewOnTop.topAnchor, leading: viewOnTop.leadingAnchor, bottom: viewOnTop.bottomAnchor, trailing: viewOnTop.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 200))

        scrollContentview.anchor(top: scrollView.topAnchor, leading: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, trailing: scrollView.trailingAnchor, size: .init(width: 0, height: 200))

        for i in 0..<dataArray.count {

            let advertisementImageview : UIImageView = {

                let advertisementimageview = UIImageView()
                advertisementimageview.translatesAutoresizingMaskIntoConstraints = false
                advertisementimageview.contentMode = contentMode
                return advertisementimageview
            }()

            imageviewArray.append(advertisementImageview)

            scrollContentview.addSubview(advertisementImageview)

            advertisementImageview.sd_setImage(with: URL(string: "\((dataArray[i] as! NSDictionary)[keyString] ?? "")"))


            advertisementImageview.topAnchor.constraint(equalTo: scrollContentview.topAnchor, constant: 0).isActive = true
            advertisementImageview.bottomAnchor.constraint(equalTo: scrollContentview.bottomAnchor, constant: 0).isActive = true
            advertisementImageview.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -20).isActive = true
            if dataArray.count == 1
            {

                advertisementImageview.leadingAnchor.constraint(equalTo: scrollContentview.leadingAnchor, constant: 10).isActive = true
                advertisementImageview.trailingAnchor.constraint(equalTo: scrollContentview.trailingAnchor, constant: -10).isActive = true
            }
            else
            {

                if i == 0 {
                    advertisementImageview.leadingAnchor.constraint(equalTo: scrollContentview.leadingAnchor, constant: 10).isActive = true
                }
                else if i == (dataArray.count - 1)
                {
                    advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 20).isActive = true
                    advertisementImageview.trailingAnchor.constraint(equalTo: scrollContentview.trailingAnchor, constant: -10).isActive = true
                }
                else {
                    advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 20).isActive = true
                }
            }



        }


        // Auto Sctoll
        //self.startAutoScroll(scrollView: scrollView)
        
        //scrollView.contentOffset.x = 0
        
        self.animateScrollView(scrollView: scrollView)

    }





    // MARK:- // Create Footer Advertisement

    func createFooterAdvertisement(viewOnTop : UIView , dataArray : NSArray , keyString : String , contentMode : UIView.ContentMode) {

        var imageviewArray = [UIImageView]()

        /*
         let shadowView : ShadowView = {
         let shadowview = ShadowView()
         shadowview.translatesAutoresizingMaskIntoConstraints = false
         return shadowview
         }()

         viewOnTop.addSubview(shadowView)

         shadowView.anchor(top: viewOnTop.topAnchor, leading: viewOnTop.leadingAnchor, bottom: viewOnTop.bottomAnchor, trailing: viewOnTop.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 200))
         */
        let scrollView : UIScrollView = {
            let scrollview = UIScrollView()
            scrollview.isPagingEnabled = true
            scrollview.layer.cornerRadius = 8
            scrollview.contentInsetAdjustmentBehavior = .never
            scrollview.showsVerticalScrollIndicator = false
            scrollview.showsHorizontalScrollIndicator = false
            scrollview.translatesAutoresizingMaskIntoConstraints = false
            //scrollview.backgroundColor = .red
            return scrollview
        }()


        let scrollContentview : UIView = {
            let scrollcontentview = UIView()
            scrollcontentview.translatesAutoresizingMaskIntoConstraints = false
            //scrollcontentview.backgroundColor = .green
            return scrollcontentview
        }()


        viewOnTop.addSubview(scrollView)
        scrollView.addSubview(scrollContentview)


        scrollView.anchor(top: viewOnTop.topAnchor, leading: viewOnTop.leadingAnchor, bottom: viewOnTop.bottomAnchor, trailing: viewOnTop.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 200))

        scrollContentview.anchor(top: scrollView.topAnchor, leading: scrollView.leadingAnchor, bottom: scrollView.bottomAnchor, trailing: scrollView.trailingAnchor, size: .init(width: 0, height: 200))

        for i in 0..<dataArray.count {

            let advertisementImageview : UIImageView = {

                let advertisementimageview = UIImageView()
                advertisementimageview.translatesAutoresizingMaskIntoConstraints = false
                advertisementimageview.contentMode = contentMode
                advertisementimageview.clipsToBounds = true
                return advertisementimageview
            }()

            imageviewArray.append(advertisementImageview)

            scrollContentview.addSubview(advertisementImageview)

            advertisementImageview.sd_setImage(with: URL(string: "\((dataArray[i] as! NSDictionary)[keyString] ?? "")"))


            advertisementImageview.topAnchor.constraint(equalTo: scrollContentview.topAnchor, constant: 0).isActive = true
            advertisementImageview.bottomAnchor.constraint(equalTo: scrollContentview.bottomAnchor, constant: 0).isActive = true
            advertisementImageview.widthAnchor.constraint(equalToConstant: (self.FullWidth - 40) / 3).isActive = true

            if dataArray.count == 1
            {

                advertisementImageview.leadingAnchor.constraint(equalTo: scrollContentview.leadingAnchor, constant: 10).isActive = true
                advertisementImageview.trailingAnchor.constraint(equalTo: scrollContentview.trailingAnchor, constant: -10).isActive = true
            }
            else
            {
                if i % 3 == 0 && i != 0 {
                    if i == (dataArray.count - 1) {
                        advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 20).isActive = true
                        advertisementImageview.trailingAnchor.constraint(equalTo: scrollContentview.trailingAnchor, constant: -10).isActive = true
                    }
                    else {
                        advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 20).isActive = true
                    }
                }
                else {
                    if i == 0 {
                        advertisementImageview.leadingAnchor.constraint(equalTo: scrollContentview.leadingAnchor, constant: 10).isActive = true
                    }
                    else if i == (dataArray.count - 1)
                    {
                        advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 10).isActive = true
                        advertisementImageview.trailingAnchor.constraint(equalTo: scrollContentview.trailingAnchor, constant: -10).isActive = true
                    }
                    else {
                        advertisementImageview.leadingAnchor.constraint(equalTo: imageviewArray[i-1].trailingAnchor, constant: 10).isActive = true
                    }
                }


            }



        }


        // Auto Sctoll
        //self.startAutoScroll(scrollView: scrollView)
        
        //scrollView.contentOffset.x = 0
        
        self.animateScrollView(scrollView: scrollView)

    }






    // MARK:- // Create Listing CollectionVIew

    func createListCollectionview(viewOnTop : UIView ,isLastView : Bool , dataDict : NSDictionary , index : Int) {



        let HeaderView : UIView = {

            let headerview = UIView()
            //headerview.backgroundColor = .cyan
            headerview.translatesAutoresizingMaskIntoConstraints = false
            return headerview

        }()

        viewOnTop.addSubview(HeaderView)


        HeaderView.anchor(top: viewOnTop.topAnchor, leading: viewOnTop.leadingAnchor, bottom: nil, trailing: viewOnTop.trailingAnchor)


        let headerLabel : UILabel = {

            let headerlabel = UILabel()
            //headerlabel.backgroundColor = .green
            headerlabel.font = UIFont(name: self.sampleLBL.font.fontName, size: 16)
            headerlabel.translatesAutoresizingMaskIntoConstraints = false
            return headerlabel


        }()

        HeaderView.addSubview(headerLabel)

        headerLabel.text = "\((dataDict["category"] as! NSDictionary)["cat_name"] ?? "")"

        //        headerLabel.anchor(top: HeaderView.topAnchor, leading: HeaderView.leadingAnchor, bottom: HeaderView.bottomAnchor, trailing: nil, size: .init(width: 100, height: 0))

        let ViewAllBtn : UIButton = {
            let viewallbtn = UIButton()
            viewallbtn.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "viewAll", comment: ""), for: .normal)
            viewallbtn.setTitleColor(.black, for: .normal)
            viewallbtn.titleLabel?.font = UIFont(name: self.sampleLBL.font.fontName, size: 14)
            viewallbtn.addTarget(self, action: #selector(LatestHomeViewController.viewAllButton(sender:)), for: .touchUpInside)
            viewallbtn.translatesAutoresizingMaskIntoConstraints = false
            return viewallbtn
        }()

        HeaderView.addSubview(ViewAllBtn)

        self.viewAllButtonArray.append(ViewAllBtn)

        ViewAllBtn.tag = index

        ViewAllBtn.anchor(top: HeaderView.topAnchor, leading: nil, bottom: HeaderView.bottomAnchor, trailing: HeaderView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 5, right: 10), size: .init(width: 100, height: 30))


        headerLabel.topAnchor.constraint(equalTo: HeaderView.topAnchor, constant: 0).isActive = true
        headerLabel.bottomAnchor.constraint(equalTo: HeaderView.bottomAnchor, constant: 0).isActive = true
        headerLabel.leadingAnchor.constraint(equalTo: HeaderView.leadingAnchor, constant: 15).isActive = true
        headerLabel.trailingAnchor.constraint(lessThanOrEqualTo: ViewAllBtn.leadingAnchor, constant: -10).isActive = true


        ///////



        let listingCollectionView : UICollectionView = {

            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

            let width = (UIScreen.main.bounds.size.width - 10) / 2.1
            layout.itemSize = CGSize(width: floor(width), height: 250)
            
            /*Dynamic Collection View Code*/
            
//            layout.itemSize = UICollectionViewFlowLayout.automaticSize
//
//            layout.estimatedItemSize = CGSize(width: (self.FullWidth - (60/320)*self.FullWidth) / 2, height: 250)
            
            
            layout.minimumInteritemSpacing = 10
            layout.minimumLineSpacing = 10
            layout.scrollDirection = .horizontal

            let listingcollectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)

            listingcollectionview.backgroundColor = .white
            listingcollectionview.showsVerticalScrollIndicator = false
            listingcollectionview.showsHorizontalScrollIndicator = false
            listingcollectionview.translatesAutoresizingMaskIntoConstraints = false

            return listingcollectionview

        }()

        listingCollectionView.tag = index


        self.DetailsView.addSubview(listingCollectionView)

        listingCollectionView.anchor(top: HeaderView.bottomAnchor, leading: viewOnTop.leadingAnchor, bottom: viewOnTop.bottomAnchor, trailing: viewOnTop.trailingAnchor, padding: .init(top: 0, left: 15, bottom: 0, right: 15), size: .init(width: 0, height: 250))

        listingCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")



        listingCollectionView.dataSource = self
        listingCollectionView.delegate = self


        // View all Visibility

        if "\((dataDict["category"] as! NSDictionary)["view_all"] ?? "")".elementsEqual("1") {
            ViewAllBtn.isHidden = false
        }
        else {
            ViewAllBtn.isHidden = true
        }

    }




    // MARK:- // View All Button Action

    @objc func viewAllButton(sender: UIButton) {

        // Daily Deals
        if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("10") {

            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "dailydeals") as! DailyDealsListingViewController

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Marketplace
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("9") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "marketplacelisting") as! MarketPlaceListingViewController

            navigate.typeID = ""

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Services
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("11") {

            //            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "servicesMainVC") as! ServicesMainViewController
            //
            //            navigate.typeID = ""
            //            navigate.categoryNameToShow = "All"
            //
            //            self.navigationController?.pushViewController(navigate, animated: true)
            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "servicemain") as! ServiceMainLatestViewController

            //            navigate.typeID = typeID
            //            navigate.categoryNameToShow = categoryName

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Food Court
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("13") {

//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "foodCourtMainVC") as! FoodCourtMainViewController
            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "restaurantListingVC") as! RestaurantListingViewController
            navigate.searchCategoryID = ""

            self.navigationController?.pushViewController(navigate, animated: true)

        }


            // Real Estate
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("999") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "realestate") as! RealEstateViewController

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Jobs
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("12") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "jobFirstSearchScreenVC") as! JobFirstSearchScreenViewController

            navigate.typeID = ""

            self.navigationController?.pushViewController(navigate, animated: true)

        }

            // Daily Rental
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("15") {

            let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "dailyRentalSearchVC") as! DailyRentalSearchViewController

            navigate.productID = ""

            self.navigationController?.pushViewController(navigate, animated: true)

        }
            //MARK:- //Holiday Accommodation
            
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("16") {
            
            let navigate = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "newholidaydetails") as! NewHolidayDetailsViewController
            
            navigate.ProductID = ""
            
            self.navigationController?.pushViewController(navigate, animated: true)
            
        }
        

            // Happening
        else if "\(((self.allDataArray[sender.tag].category)!["category"] as! NSDictionary)["cat_id"] ?? "")".elementsEqual("14") {

            let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "happeningsearch") as! HappeningSearchViewController

            self.navigationController?.pushViewController(nav, animated: true)

        }

        else {

        }







    }




    // MARK:- // Create Classified View

    func createClassifiedView(insideOfView : UIView, dataDict : NSDictionary) {



        let HeaderView : UIView = {

            let headerview = UIView()
            //headerview.backgroundColor = .cyan
            headerview.translatesAutoresizingMaskIntoConstraints = false
            return headerview

        }()

        insideOfView.addSubview(HeaderView)


        HeaderView.anchor(top: insideOfView.topAnchor, leading: insideOfView.leadingAnchor, bottom: nil, trailing: insideOfView.trailingAnchor)


        let headerLabel : UILabel = {

            let headerlabel = UILabel()
            //headerlabel.backgroundColor = .green
            headerlabel.font = UIFont(name: self.sampleLBL.font.fontName, size: 16)
            headerlabel.translatesAutoresizingMaskIntoConstraints = false
            return headerlabel


        }()

        HeaderView.addSubview(headerLabel)

        headerLabel.text = "\((dataDict["category"] as! NSDictionary)["cat_name"] ?? "")"

        //        headerLabel.anchor(top: HeaderView.topAnchor, leading: HeaderView.leadingAnchor, bottom: HeaderView.bottomAnchor, trailing: nil, size: .init(width: 100, height: 0))

        let ViewAllBtn : UIButton = {
            let viewallbtn = UIButton()
            viewallbtn.setTitle("View All", for: .normal)
            viewallbtn.setTitleColor(.black, for: .normal)
            viewallbtn.titleLabel?.font = UIFont(name: self.sampleLBL.font.fontName, size: 14)
            viewallbtn.translatesAutoresizingMaskIntoConstraints = false
            return viewallbtn
        }()

        HeaderView.addSubview(ViewAllBtn)

        ViewAllBtn.anchor(top: HeaderView.topAnchor, leading: nil, bottom: HeaderView.bottomAnchor, trailing: HeaderView.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 5, right: 10), size: .init(width: 100, height: 30))


        headerLabel.topAnchor.constraint(equalTo: HeaderView.topAnchor, constant: 0).isActive = true
        headerLabel.bottomAnchor.constraint(equalTo: HeaderView.bottomAnchor, constant: 0).isActive = true
        headerLabel.leadingAnchor.constraint(equalTo: HeaderView.leadingAnchor, constant: 15).isActive = true
        headerLabel.trailingAnchor.constraint(lessThanOrEqualTo: ViewAllBtn.leadingAnchor, constant: -10).isActive = true


        ///////



        let listingCollectionView : UICollectionView = {

            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

            let width = (UIScreen.main.bounds.size.width - 10) / 2.1
            layout.itemSize = CGSize(width: floor(width), height: 180)
            layout.minimumInteritemSpacing = 10
            layout.minimumLineSpacing = 10
            layout.scrollDirection = .horizontal

            let listingcollectionview = UICollectionView(frame: .zero, collectionViewLayout: layout)

            listingcollectionview.backgroundColor = .white
            listingcollectionview.showsVerticalScrollIndicator = false
            listingcollectionview.showsHorizontalScrollIndicator = false
            listingcollectionview.translatesAutoresizingMaskIntoConstraints = false

            return listingcollectionview

        }()

        self.DetailsView.addSubview(listingCollectionView)

        listingCollectionView.anchor(top: HeaderView.bottomAnchor, leading: insideOfView.leadingAnchor, bottom: insideOfView.bottomAnchor, trailing: insideOfView.trailingAnchor, padding: .init(top: 0, left: 15, bottom: 0, right: 15), size: .init(width: 0, height: 180))

        listingCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")

        listingCollectionView.tag = 999

        listingCollectionView.dataSource = self
        listingCollectionView.delegate = self



    }



    // MARK:- // Auto Scroll Function

//    private func startAutoScroll(scrollView : UIScrollView)
//    {
//
//        let tempScroll = scrollView
//
//        tempScroll.alwaysBounceVertical = false
//
//        tempScroll.alwaysBounceHorizontal = false
//
//        var timer: Timer?
//
//        timer = Timer.scheduledTimer(withTimeInterval: 20, repeats: true, block: {_ in
//
//            UIView.animate(withDuration: 2.5, delay: 2.5, options: .repeat, animations: {
//
//                    if scrollView.contentOffset.x == (scrollView.contentSize.width - self.FullWidth)
//                    {
//                        scrollView.contentOffset.x = 0
//
//                        //self.startAutoScroll(scrollView: tempScroll)
//                    }
//                    else
//                    {
//                        scrollView.contentOffset.x = scrollView.contentOffset.x + self.FullWidth
//                    }
//
//                }, completion: { (status) in
//                    self.startAutoScroll(scrollView: tempScroll)
//                })
//            })
//    }
    
    private func animateScrollView(scrollView : UIScrollView) {
        
            let scrollWidth = scrollView.bounds.width
        
            let currentXOffset = scrollView.contentOffset.x

            let lastXPos = currentXOffset + scrollWidth
        
        var Autotimer : Timer?
        
         Autotimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: {_ in
            UIView.animate(withDuration: 5, delay: 2.5, options: .repeat, animations: {
                
                if lastXPos != scrollView.contentSize.width {
                    
                    //print("Scroll")
                    
                    if lastXPos > scrollView.contentSize.width
                    {
                        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                        
                        Autotimer?.invalidate()
                        
                        Autotimer = nil
                        
                        scrollView.contentOffset.x = 0
                        
                        self.animateScrollView(scrollView: scrollView)
                    }
                    else if lastXPos <= scrollView.contentSize.width
                    {
                        scrollView.setContentOffset(CGPoint(x: lastXPos, y: 0), animated: false)
                        
                        scrollView.contentOffset.x = lastXPos
                        
                        //self.animateScrollView(scrollView: scrollView)
                    }
                    
                }
                else {
                    
                   // print("Scroll to start")
                    
                    scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                    
                    Autotimer?.invalidate()
                    
                    Autotimer = nil
                    
                    self.animateScrollView(scrollView: scrollView)
                    
                }
                
            }, completion: { (status) in
                
                //print("nothing")
                
                if self.AutoScroll == true
                {
                    self.animateScrollView(scrollView: scrollView)
                    
                    self.AutoScroll = false
                }
                else
                {
//                    self.AutoScroll = true
//                    
//                    Autotimer?.invalidate()
//                    
//                    Autotimer = nil
                }
                
                
            })
            
        })
        }
    
    
   
//    func scheduledTimerWithTimeInterval(){
//            // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
//            timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.animateScrollView), userInfo: nil, repeats: true)
//        }
    
}
class dataStructure
{
    var category : NSDictionary?
    
    var advertisement : NSArray?

    init(category: NSDictionary , advertisement: NSArray) {
        
        self.category = category
        
        self.advertisement = advertisement
        
    }

}

