//
//  DailyDealsMarketplaceTableViewCell.swift
//  Kaafoo
//
//  Created by admin on 13/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class DailyDealsMarketplaceTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellTitleLBL: UILabel!
    @IBOutlet weak var cellCollectionview: UICollectionView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension DailyDealsMarketplaceTableViewCell
{
    
    func setCollectionviewDataSourceDelegate
        <D: UICollectionViewDelegate & UICollectionViewDataSource>
        (_ dataSourceDelegate: D, forRow row:Int)
    {
        
        cellCollectionview.delegate = dataSourceDelegate
        cellCollectionview.dataSource = dataSourceDelegate
        
        cellCollectionview.reloadData()
        
    }
    
}
