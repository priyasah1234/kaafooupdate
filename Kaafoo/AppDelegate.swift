//
//  AppDelegate.swift
//  Kaafoo
//
//  Created by Debarun on 13/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import UserNotifications
import FBSDKCoreKit
import Firebase
import PusherSwift  //Channel
import Stripe
import PushNotifications //Beams
import PusherChatkit //Chatkit
import Fabric


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,PusherDelegate {

    var window: UIWindow?
    
    let deviceToken = UserDefaults.standard.string(forKey: "DeviceToken")
    
    let gcmMessageIDKey = "gcm.message_id"
    
    let UserID = UserDefaults.standard.string(forKey: "userID")
    
    let notificationDelegate = KaafooNotificationSample()
    
    let beamsClient = PushNotifications.shared
    
    var pusher: Pusher!
    
    public var chatManager: ChatManager?
    
    public var currentUser: PCCurrentUser?
    
    var reachability : Reachability?
    
//    let pusherChat = ChatManager(
//      instanceLocator: "v1:us1:6c6a1133-f2b6-4220-9095-0711c5df0fee",
//      tokenProvider: PCTokenProvider(url: "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/6c6a1133-f2b6-4220-9095-0711c5df0fee/token"),
//      userID: "your-user-id"
//    )
    

    

    //MARK:- //Application Did Finish Launching
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
        
        
//        let exception = NSException(name: NSExceptionName(rawValue: "arbitrary"), reason: "arbitrary reason", userInfo: nil)
//        exception.raise()
        
        
//        NotificationCenter.default.addObserver(self, selector: #selector(checkForReachability(notification:)), name: NSNotification.Name.reachabilityChanged, object: nil)
//
//        do
//        {
//            try reachability = Reachability(hostname: "www.apple.com")
//            try reachability?.startNotifier()
//        }
//        catch let error
//        {
//            print(error)
//        }
        

        
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            print("App is Closed")
        }
        
        let randomString = String.random()
        
        print("RandomString",randomString)
        
        self.beamsClient.start(instanceId: "a1dff960-4058-45ff-a842-24e43541dd58")
        
        self.beamsClient.registerForRemoteNotifications()
        
        try? self.beamsClient.addDeviceInterest(interest: "hello")
        
        
        Stripe.setDefaultPublishableKey("pk_test_rdKM7MLxQIovbFCTaVTQa8or")
        
        STPPaymentConfiguration.shared().publishableKey = "pk_test_rdKM7MLxQIovbFCTaVTQa8or"
        
        self.configureNotification()
        
        if #available(iOS 10.0, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            //Messaging.messaging().delegate = self
        }
        
        application.registerForRemoteNotifications()
        
        GMSServices.provideAPIKey("AIzaSyDZ5jN7O4l7JetwBEo_fNa31FQA2jHVaUM")
        
        GMSPlacesClient.provideAPIKey("AIzaSyDZ5jN7O4l7JetwBEo_fNa31FQA2jHVaUM")
        
//        GMSPlacesClient.provideAPIKey("AIzaSyDK7FZ686FoPQF8Q8P5QQHnZxumbrsITfs")
        
        IQKeyboardManager.shared.enable = true
        
        if #available(iOS 10, *)
        {
            UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                
                guard error == nil
                    else
                {
                    UserDefaults.standard.set(false, forKey: "allowpushnotifaction")
                    return
                }
                
                if granted
                {
                    
                    DispatchQueue.main.async(execute: {
                        UserDefaults.standard.set(true, forKey: "allowpushnotifaction")
                        //    application.registerForRemoteNotifications()
                        UIApplication.shared.registerForRemoteNotifications()
                        
                    })
                }
                else
                {
                    //Handle user denying permissions..
                }
            }
            
            //  application.registerForRemoteNotifications()
            UIApplication.shared.registerForRemoteNotifications()
        }
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        if let payload = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? NSDictionary, let identifier = payload["identifier"] as? String {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: identifier)
            window?.rootViewController = vc
        }
        
        //push notifications
        
        let options = PusherClientOptions(
               host: .cluster("ap2")
             )

             pusher = Pusher(
               key: "e53a9c4a375968423d66",
               options: options
             )

             pusher.delegate = self

             // subscribe to channel
             let channel = pusher.subscribe("my-notification-channel")

             // bind a callback to handle an event
        let _ = channel.bind(eventName: "my_noti_event", callback: { (data: Any?) -> Void in
                   if let data = data as? [String : AnyObject] {
                       
                       print("Data==",data)
                       
                       if let message = data["message"] as? String {
                           print("pushermsg",message)
                       }
                      
                    if let message = data["image_link"] as? String {
                        print("PusherImageLink",message)
                    }
                    
                   }
               })

             pusher.connect()
       
        return true
    }
    
  
    
    // print Pusher debug messages
    
       func debugLog(message: String) {
         print(message)
       }
    
    func subscribedToChannel(name: String) {
        print(name)
    }
    
    func configureNotification() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            center.delegate = notificationDelegate
            let openAction = UNNotificationAction(identifier: "OpenNotification", title: NSLocalizedString("Abrir", comment: ""), options: UNNotificationActionOptions.foreground)
            let deafultCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [openAction], intentIdentifiers: [], options: [])
            center.setNotificationCategories(Set([deafultCategory]))
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
    }

    
    
    @objc func checkForReachability(notification:NSNotification)
    {
        // Remove the next two lines of code. You cannot instantiate the object
        // you want to receive notifications from inside of the notification
        // handler that is meant for the notifications it emits.
        
        //var networkReachability = Reachability.reachabilityForInternetConnection()
        //networkReachability.startNotifier()
        
        let networkReachability = notification.object as! Reachability
        let remoteHostStatus = networkReachability.connection
        
        if (remoteHostStatus == .cellular)
        {
            print("Not Reachable")
        }
        else if (remoteHostStatus == .wifi)
        {
            print("Reachable via Wifi")
        }
        else
        {
            print("Reachable")
        }
    }
    
    //MARK:- Application Open URL Method
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled = ApplicationDelegate.shared.application(application, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        // Add any custom logic here.
        return handled
    }
    
    //MARK:- // Fetching Device Token
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            
            return String(format: "%02.2hhx", data)
            
        }
        let token = tokenParts.joined()
        
        // 2. Print device token to use for PNs payloads
       // Messaging.messaging().apnsToken = deviceToken
        
        print("Device Token: \(token)")
        
        let defaults = UserDefaults.standard
        
        defaults.set(token, forKey:"DeviceToken")
        
        self.beamsClient.registerDeviceToken(deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
        
        let defaults = UserDefaults.standard
        
        defaults.set("cb8143a8becd78c317b8e0c722c9177a4b9579ab25f5e2f5f4fe806dc2937a3e", forKey:"DeviceToken")
        
    }
    
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("Error fetching remote instance ID: \(error)")
//            } else if let result = result {
//                print("Remote instance ID token: \(result.token)")
//            }
//        }
//
//        let dataDict:[String: String] = ["token": fcmToken]
//        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
//    }
    
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//
//        print("Received data message: \(remoteMessage.appData)")
//    }
    
//
//    func application(application: UIApplication,
//                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
//
//    }
//
    
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert,.sound,.badge])
    }
    
    //MARK:- //Push Notifications did Receive Method
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        completionHandler()
        print("Push Notification is Clicked")
    }

// [END ios_10_message_handling]

    
    
    
    
    
    
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        
        
        let remoteNotificationType = self.beamsClient.handleNotification(userInfo: userInfo)
        if remoteNotificationType == .ShouldIgnore {
            return // This was an internal-only notification from Pusher.
        }
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
            
        }
        // Print full message.
        print("userinfo==",userInfo)
        
        print("APS",userInfo["aps"] ?? "")
        
//        if "\(((userInfo["aps"] as! NSDictionary)["alert"] as! NSDictionary)["receiverid"] ?? "")".elementsEqual(UserID ?? "")
//        {
//            UIApplication.shared.registerForRemoteNotifications()
//        }
//        else
//        {
//            UIApplication.shared.unregisterForRemoteNotifications()
//        }
        
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
//        let blankViewController = UIViewController()
//        blankViewController.view.backgroundColor = UIColor.white
//        window?.rootViewController?.present(blankViewController, animated: false)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        window?.rootViewController?.dismiss(animated: false)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Kaafoo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK:- Exception Handler Print Function
    
    func storeStackTrace() {
       NSSetUncaughtExceptionHandler { exception in
           print(exception)
           // do stuff with the exception
       }
    }

}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

//extension AppDelegate : PCChatManagerDelegate
//{
//    func initChatkit(_ userId: String, _ callback: @escaping (_ currentUser: PCCurrentUser) -> Void){
//       self.chatManager = ChatManager(
//         instanceLocator: "YOUR_INSTANCE_LOCATOR",
//         tokenProvider: PCTokenProvider(url: "YOUR_TOKEN_PROVIDER_ENDPOINT"),
//         userID: userId
//       )
//       chatManager!.connect(delegate: self) { (currentUser, error) in
//       guard(error == nil) else {
//         print("Error connecting: \(error!.localizedDescription)")
//         return
//       }
//       self.currentUser = currentUser
//       callback(currentUser!)
//       }
//    }
//}


//MARK:- //Creating Random String

extension String {

    static func random(length: Int = 20) -> String {
        
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        var randomString: String = ""

        for _ in 0..<length {
            
            let randomValue = arc4random_uniform(UInt32(base.count))
            
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}
