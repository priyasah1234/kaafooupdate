//
//  DailyRentalListingViewController.swift
//  Kaafoo
//
//  Created by admin on 11/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD
import MapKit
import CoreLocation
import GoogleMaps
import GooglePlaces


class DailyRentalListingViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource,MKMapViewDelegate,GMSMapViewDelegate {
    
    var isMarkerActive : Bool! = false
    
    var selectedMarker : GMSMarker!
    
    var SelectedInt : Int!
    
    @IBOutlet weak var GPreviousBtnOutlet: UIButton!
    
    @IBOutlet weak var GNextBtnOutlet: UIButton!
    
    var markers = [GMSMarker]()
    
    var position = CLLocationCoordinate2DMake(22.5726,88.3639)
    
    
   
    @IBOutlet weak var GmapView: GMSMapView!
    
    @IBOutlet weak var PickupDetails: UILabel!

    @IBOutlet weak var DropOffDetails: UILabel!

    @IBOutlet weak var DetailsView: UIView!
    
    var ProductsOnMapArray = [MapListing]()
    
    
    
    var sortDict = [["key" : "" , "value" : "All"] , ["key" : "T" , "value" : "Title"] , ["key" : "LP" , "value" : "Lowest Priced"],["key" : "HP" , "value" : "Highest Priced"],["key" : "C" , "value" : "Closing Soon"],["key" : "N" , "value" : "Newest Listed"]]
    
    var sortingType : String! = ""
    
    
    @IBOutlet weak var ShowSortView: UIView!
    
    @IBOutlet weak var sortPickerview: UIPickerView!
    
    
    @IBOutlet weak var additionalButtonsStackview: UIStackView!
    
    @IBOutlet weak var filterButtonView: UIView!
    
    @IBOutlet weak var mapButtonView: UIView!
    
    @IBOutlet weak var dailyRentalListingTableview: UITableView!
    
    @IBOutlet weak var LocalHeaderView: UIView!
    
    
    @IBOutlet weak var Product_Hourly_Rental: UILabel!
    
    @IBOutlet weak var Product_DailyRental: UILabel!
    
    @IBOutlet weak var FeatureScrollView: UIScrollView!
    
    @IBOutlet weak var Product_Type: UILabel!
    
    @IBOutlet weak var Product_Name: UILabel!
    
    @IBOutlet weak var Product_Image: UIImageView!
    
    @IBOutlet weak var Product_BookBtnOutlet: UIButton!
    
    @IBOutlet weak var Product_WatchlistBtnOutlet: UIButton!
    
    @IBOutlet weak var mapView: MKMapView!
    
    
    // MARK:- // Filter Button Action
    
    @IBOutlet weak var filterButtonOutlet: UIButton!
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            
            if self.filterView.isHidden == true {
                
                self.view.bringSubviewToFront(self.filterView)
                
                self.filterView.isHidden = false
            }
            else {
                self.view.sendSubviewToBack(self.filterView)
                
                self.filterView.isHidden = true
            }
        }
        
    }
    
    
    @IBOutlet weak var GProductDetailsView: UIView!
    
    //Product Details ViewSubViews
    @IBAction func GProductWatchlistBtn(_ sender: UIButton) {
        
    }
    @IBOutlet weak var GProductDetailsWatchlistOutlet: UIButton!
    
    @IBOutlet weak var GProductDetailsWatchlistedOutlet: UIButton!
    
    @IBOutlet weak var GProductDetailsName: UILabel!
    
    @IBOutlet weak var GProductDetailsType: UILabel!
    
    @IBOutlet weak var GProductDetailsImage: UIImageView!
    
    @IBOutlet weak var GProductDetailsSellerImage: UIImageView!
    
    @IBOutlet weak var GProductDetailsDailyRental: UILabel!
    
    @IBOutlet weak var GProductDetailsHourlyRental: UILabel!
    
    
    
//    func CreateDetailsView()
//    {
//
//
//
//    }
    
    
    func AppendMapData()
    {
        self.dispatchGroup.enter()
        
        for i in 0..<self.recordsArray.count
        {
            self.ProductOnMapArray.append(MapListing(ProductName: "\((recordsArray[i] as! NSDictionary)["product_name"] ?? "")", ProductLatitude: Double(("\((recordsArray[i] as! NSDictionary)["lat"] ?? "")" as NSString).doubleValue), ProductLongitude: Double(("\((recordsArray[i] as! NSDictionary)["long"] ?? "")" as NSString).doubleValue), ProductImage: "\((recordsArray[i] as! NSDictionary)["photo"] ?? "")", CurrencySymbol: "\((recordsArray[i] as! NSDictionary)["currency_symbol"] ?? "")", StartPrice: "\((recordsArray[i] as! NSDictionary)["Daily_Rentals"] ?? "")", ReservePrice: "\((recordsArray[i] as! NSDictionary)["Hourly_Rental"] ?? "")", ProductID: "\((recordsArray[i] as! NSDictionary)["product_id"] ?? "")", SellerImage: "\((recordsArray[i] as! NSDictionary)["userimagedata"] ?? "")", WatchlistStatus: "\((recordsArray[i] as! NSDictionary)["watchlist_status"] ?? "")"))
        }
        
        self.dispatchGroup.leave()
        
        self.dispatchGroup.notify(queue: .main, execute: {
            
            let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
            //            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            self.GmapView.animate(to: cameraPosition)
            
            self.GmapView.isMyLocationEnabled = true
            
            self.GmapView.settings.myLocationButton = true
            
//            var index : Int! = 0
            
            
            for state in self.ProductOnMapArray {
                
                let state_marker = GMSMarker()
                
                state_marker.position = CLLocationCoordinate2D(latitude: state.ProductLatitude ?? 0.0, longitude: state.ProductLongitude ?? 0.0)
                
                state_marker.title = state.ProductName
                
                state_marker.userData = state
                
                state_marker.snippet = "Hey, this is \(state.ProductName!)"
                
                //                state_marker.icon = self.drawTextT(text: "Hello", inImage: UIImage(named: "CustomMarker")!)
//                index = index + 1
                state_marker.map = self.GmapView
                
                self.markers.append(state_marker)
            }
            
//            for i in 0..<self.markers.count
//            {
//                self.markers[i].
//            }
        })
    }
    
    
//    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//        GmapView.selectedMarker = self.state_marker
//    }
//
//    //MARK:- DidTap Marker in swift
//    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
//
//
//
//        if let selectedMarker = GmapView.selectedMarker {
//            selectedMarker.icon = GMSMarker.markerImage(with: nil)
//        }
//
//        // select new marker and make green
//        GmapView.selectedMarker = marker
//        marker.icon = GMSMarker.markerImage(with: UIColor.seaGreen())
//
//        print("usermarkerdata",marker.userData!)
//
//        self.GProductDetailsView.isHidden = true
//
//        return true
//    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
//        self.GProductDetailsView.isHidden = false
//
//        let tempdict = marker.userData as! MapListing
//
//        self.dispatchGroup.enter()
//
//        if (tempdict.WatchlistStatus?.elementsEqual("0"))!
//        {
//            self.GProductDetailsWatchlistOutlet.isHidden = false
//            self.GProductDetailsWatchlistedOutlet.isHidden = true
//        }
//        else
//        {
//            self.GProductDetailsWatchlistOutlet.isHidden = true
//            self.GProductDetailsWatchlistedOutlet.isHidden = false
//        }
//
//        self.GProductDetailsName.text = "\(tempdict.ProductName ?? "")"
//
//        self.GProductDetailsType.text = "\(tempdict.ProductName ?? "")"
//        self.GProductDetailsDailyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
//        self.GProductDetailsHourlyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
//        self.GProductDetailsImage.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
//        self.GProductDetailsSellerImage.sd_setImage(with: URL(string: "\(tempdict.SellerImage ?? "")"))
//
//        self.dispatchGroup.leave()
//
//        self.GProductDetailsView.isHidden = false
//        self.view.bringSubviewToFront(self.GProductDetailsView)

        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//        customInfoWindow?.removeFromSuperview()
        
        self.index = 0
        
        self.GProductDetailsView.isHidden = true
        
        for i in 0..<self.markers.count
        {
            self.markers[i].icon = GMSMarker.markerImage(with: nil)
        }
//        let update = GMSCameraUpdate.zoom(by: 10)
        
        let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)
//        GmapView.moveCamera(update)
        GmapView.animate(to: cameraPosition)
    }
    
    
    func centerInMarker(marker: GMSMarker) {
        
        var bounds = GMSCoordinateBounds()
        
        bounds = bounds.includingCoordinate((marker as AnyObject).position)
        //let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: (self.mapView?.frame.height)!/2, left: (self.mapView?.frame.width)!/2, bottom: 0, right: 0))
        let updateOne = GMSCameraUpdate.setTarget(marker.position, zoom: 20)
        
        GmapView?.moveCamera(updateOne)
    }
    
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        self.centerInMarker(marker: marker)
        
        print("markerPosition",markers[0].position)
        
        if let selectedMarker = GmapView.selectedMarker {
            
                        selectedMarker.icon = GMSMarker.markerImage(with: nil)
                    }
        
        GmapView.selectedMarker = marker
        
        marker.icon = GMSMarker.markerImage(with: UIColor.seaGreen())
        
        print("usermarkerdata",marker.userData!)
        
        
        self.GProductDetailsView.isHidden = false
        
        let tempdict = marker.userData as! MapListing
        
        self.dispatchGroup.enter()
        
        if (tempdict.WatchlistStatus?.elementsEqual("0"))!
        {
            self.GProductDetailsWatchlistOutlet.isHidden = false
            
            self.GProductDetailsWatchlistedOutlet.isHidden = true
        }
        else
        {
            self.GProductDetailsWatchlistOutlet.isHidden = true
            
            self.GProductDetailsWatchlistedOutlet.isHidden = false
        }
        
        self.GProductDetailsName.text = "\(tempdict.ProductName ?? "")"
        
        self.GProductDetailsType.text = "\(tempdict.ProductName ?? "")"
        
        self.GProductDetailsDailyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
        
        self.GProductDetailsHourlyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
        
        self.GProductDetailsImage.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
        
        self.GProductDetailsSellerImage.sd_setImage(with: URL(string: "\(tempdict.SellerImage ?? "")"))
        
        self.dispatchGroup.leave()
        
        self.GProductDetailsView.isHidden = false
        
        self.view.bringSubviewToFront(self.GProductDetailsView)
        
//        self.GProductDetailsView.isHidden = true
        return true
    }
    
    @IBAction func GPreviousBtn(_ sender: UIButton) {
        
        CATransaction.begin()
        
        CATransaction.setAnimationDuration(0.5)
        
        index = index - 1
        
        if self.index == 0
        {
            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
        }
            
        else if self.index > self.markers.count
        {
            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
        }
        else if self.index < 0
        {
            self.ShowAlertMessage(title: "Warning", message: "NO PRODUCT FOUND")
        }
        else
        {
            self.GProductDetailsView.isHidden = false
            
            let tempdict = markers[index].userData as! MapListing
            
            self.dispatchGroup.enter()
            
            if (tempdict.WatchlistStatus?.elementsEqual("0"))!
            {
                self.GProductDetailsWatchlistOutlet.isHidden = false
                self.GProductDetailsWatchlistedOutlet.isHidden = true
            }
            else
            {
                self.GProductDetailsWatchlistOutlet.isHidden = true
                self.GProductDetailsWatchlistedOutlet.isHidden = false
            }
            
            self.GProductDetailsName.text = "\(tempdict.ProductName ?? "")"
            
            self.GProductDetailsType.text = "\(tempdict.ProductName ?? "")"
            self.GProductDetailsDailyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
            self.GProductDetailsHourlyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
            self.GProductDetailsImage.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
            self.GProductDetailsSellerImage.sd_setImage(with: URL(string: "\(tempdict.SellerImage ?? "")"))
            
            self.dispatchGroup.leave()
            
            self.GProductDetailsView.isHidden = false
            self.view.bringSubviewToFront(self.GProductDetailsView)
            
            self.dispatchGroup.notify(queue: .main, execute: {
                
                let updateOne = GMSCameraUpdate.setTarget(self.markers[self.index].position, zoom: 20)
                self.GmapView?.moveCamera(updateOne)
            })
        }
        
        CATransaction.commit()
    }
    
    var index : Int! = 0
    
    @IBAction func GNextBtn(_ sender: UIButton) {
        
        if index == self.markers.count
        {
            self.ShowAlertMessage(title: "Warning", message: "No More Product Found")
        }
        else
        {
            
            CATransaction.begin()
            CATransaction.setAnimationDuration(0.5)
            
            index = index + 1
            
            if index == self.markers.count
            {
                self.ShowAlertMessage(title: "Warning", message: "NO DATA FOUND")
            }
            else if index > self.markers.count
            {
                self.ShowAlertMessage(title: "Warning", message: "NO DATA FOUND")
            }
                
            else
            {
                self.GProductDetailsView.isHidden = false
                let tempdict = markers[index].userData as! MapListing
                self.dispatchGroup.enter()
                if (tempdict.WatchlistStatus?.elementsEqual("0"))!
                {
                    self.GProductDetailsWatchlistOutlet.isHidden = false
                    self.GProductDetailsWatchlistedOutlet.isHidden = true
                }
                else
                {
                    self.GProductDetailsWatchlistOutlet.isHidden = true
                    self.GProductDetailsWatchlistedOutlet.isHidden = false
                }
                self.GProductDetailsName.text = "\(tempdict.ProductName ?? "")"
                self.GProductDetailsType.text = "\(tempdict.ProductName ?? "")"
                self.GProductDetailsDailyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.StartPrice ?? "")"
                self.GProductDetailsHourlyRental.text = "\(tempdict.CurrencySymbol ?? "")" + "\(tempdict.ReservePrice ?? "")"
                self.GProductDetailsImage.sd_setImage(with: URL(string: "\(tempdict.ProductImage ?? "")"))
                self.GProductDetailsSellerImage.sd_setImage(with: URL(string: "\(tempdict.SellerImage ?? "")"))
                
                self.dispatchGroup.leave()
                
                self.GProductDetailsView.isHidden = false
                self.view.bringSubviewToFront(self.GProductDetailsView)
                
                self.dispatchGroup.notify(queue: .main, execute: {
                    let updateOne = GMSCameraUpdate.setTarget(self.markers[self.index].position, zoom: 20)
                    self.GmapView?.moveCamera(updateOne)
                })
                
            }
            CATransaction.commit()
        }
        
    }
    
    
    
    
    // MARK:- // Define Filter Button Actions
    
    func defineSortButtonActions() {
        
        self.filterView.buttonArray[0].addTarget(self, action: #selector(self.sortAction(sender:)), for: .touchUpInside)
        self.filterView.buttonArray[1].addTarget(self, action: #selector(self.filterAction(sender:)), for: .touchUpInside)
        
        
        self.filterView.tapToCloseButton.addTarget(self, action: #selector(self.tapToClose(sender:)), for: .touchUpInside)
        
    }
    
    
    // MARK;- // Sort Action
    
    @objc func sortAction(sender: UIButton) {
        
        self.filterView.isHidden = true
        
        self.view.sendSubviewToBack(self.filterView)
        
        self.ShowSortView.isHidden = false
    }
    
    // MARK;- // Filter Action
    
    @objc func filterAction(sender: UIButton) {
        
        let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController
        
        navigate.sortArray = self.sortArray.mutableCopy() as? NSMutableArray
        
        navigate.tempParams = tempParams
        
        navigate.rootVC = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "dailyRentalListingVC") as! DailyRentalListingViewController
        
        navigate.showDynamicType = true
        navigate.showPrice = true
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }

    // MARK;- // Tap to Close Action
    
    @objc func tapToClose(sender: UIButton) {
        self.filterView.isHidden = true
        self.view.sendSubviewToBack(self.filterView)
    }

    
    // MARK:- // Sorting Function
    
    func sortingFunction()
    {
        self.ShowSortView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        self.ShowSortView.addGestureRecognizer(tap)
    }
    
    func setDetailsView()
    {
        self.PickupDetails.text = dailyRentalSearchParameters.shared.pickupdate + " " + dailyRentalSearchParameters.shared.pickuptime + "  "  + dailyRentalSearchParameters.shared.PickupLocation
        
        self.DropOffDetails.text = dailyRentalSearchParameters.shared.dropoffdate + "  " + dailyRentalSearchParameters.shared.dropofftime + "  " + dailyRentalSearchParameters.shared.dropOffLocation
    }
    
    
    // MARK:- // Sortview Tap Gesture
    
    @objc func handleTap(sender: UITapGestureRecognizer)
    {
        self.ShowSortView.isHidden = true
    }
    
    
    
    
    
    
    
    
    
    
    @IBOutlet weak var ProductDetailsView: UIView!

    
    @IBAction func Product_Book(_ sender: UIButton) {
    }
    
    @IBOutlet weak var Product_watchlistedOutlet: UIButton!
    @IBAction func Product_watchlistedBtn(_ sender: UIButton) {
    }
    @IBAction func CrossBtn(_ sender: UIButton) {
        self.ProductDetailsView.isHidden = true
    }

    @IBOutlet weak var CrossBtnOutlet: UIButton!
    @IBAction func Product_Watchlist(_ sender: UIButton) {
    }
    
    var MapStatus: String! = ""
    var ProId : String!
    
    
    
    // MARK:- // Map Button
    
    @IBOutlet weak var MapViewBtnOutlet: UIButton!
    @IBAction func MapView_Btn(_ sender: UIButton) {
//      self.mapView.isHidden = !self.mapView.isHidden
//
//        if self.mapView.isHidden == true
//        {
//
//          self.MapStatus = "0"
//          self.ProductDetailsView.isHidden = true
////          print("nshsbbksmw",self.MapStatus)
//          self.recordsArray.removeAllObjects()
//          self.getDailyRentalListing()
//          self.MapViewBtnOutlet.setImage(UIImage(named: "Map-1"), for: .normal)
//        }
//        else
//        {
//
//            self.MapStatus = "1"
//          print("nshsbbw",self.MapStatus ?? "")
//          self.recordsArray.removeAllObjects()
//          self.getDailyRentalListing()
//          self.MapViewBtnOutlet.setImage(UIImage(named: "list"), for: .normal)
//        }
        
       if self.GmapView.isHidden == true
       {
        self.GmapView.isHidden = false
        self.view.bringSubviewToFront(self.GmapView)
        }
        else
       {
        self.GmapView.isHidden = true
        self.GProductDetailsView.isHidden = true
        }
       
        
        
        
    }

    
    let dispatchGroup = DispatchGroup()
    
    var pickupDate : String!
    var pickupTime : String!
    var dropoffDate : String!
    var dropoffTime : String!
    var driverAge : String!
    
    var categoryID : String!
    
    var tempParams : String! = ""
    
    var startValue = 0
    var perLoad = 10
    
    var scrollBegin : CGFloat!
    var scrollEnd : CGFloat!
    
    var nextStart : String!
    
    var recordsArray : NSMutableArray! = NSMutableArray()
    
    var dailyRentalListingArray : NSArray!
    
    var extraServicesArray : NSArray!
    
    var productID : String!
    
    var finalPriceString : String!
    
    var depositString : String!
    
    var sortArray : NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.GmapView.delegate = self
        
        let destinationMarker = GMSMarker(position: position)
        
        self.ProductsOnMapArray = []
    
        self.sortingFunction()
        
        self.setDetailsView()
        
        self.sortPickerview.delegate = self
        self.sortPickerview.dataSource = self
        
        
        self.ProductDetailsView.isHidden = true
        self.SetLayer()
        self.mapView.delegate = self
        
//        print("Category ID ------",categoryID)

        self.setFont()
        
        self.getDailyRentalListing()
        
        headerView.contentView.backgroundColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 1)
        
        self.defineSortButtonActions()
        
        self.GProductDetailsView.isHidden = true
        
        self.GmapView.isHidden = true
        
        print("ProductsOnMApArray",self.ProductsOnMapArray)
        
    }
    
    // MARK:- // Delegate Functions
    
    
    // MARK:- // Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recordsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.SelectedInt = indexPath.row
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! DailyRentalListingTVCTableViewCell
        
        cell.CarType.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["title_name"] ?? "")"
        
        cell.DailyRentalProductImage.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["photo"]!)"))

        cell.DailyRentalProductSellerImage.sd_setImage(with: URL(string: "\((recordsArray[indexPath.row] as! NSDictionary)["userimagedata"]!)"))

        cell.dailyRentalProductName.text = "\((self.recordsArray[indexPath.row] as! NSDictionary)["product_name"]!)"
        cell.DailyRentalPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["Daily_Rentals"]!)"
        cell.DailyRentalMonthlyPrice.text = "\((recordsArray[indexPath.row] as! NSDictionary)["currency_symbol"]!)" + "\((recordsArray[indexPath.row] as! NSDictionary)["Hourly_Rental"]!)"

        cell.DailyRentalMonthlyPrice.textColor = .seaGreen()
        cell.DailyRentalPrice.textColor = .seaGreen()
//        cell.DailyRentalProductBookBtnOutlet.tag = indexPath.row
//        cell.DailyRentalProductBookBtnOutlet.addTarget(self, action: #selector(DailyRentalListingViewController.bookRental(sender:)), for: .touchUpInside)

        if "\((recordsArray[indexPath.row] as! NSDictionary)["watchlist_status"]!)".elementsEqual("1")
        {
            cell.WatchlistedBtnOutlet.isHidden = false
            cell.WatchlistBtnOutlet.isHidden = true
            cell.WatchlistedBtnOutlet.addTarget(self, action: #selector(WatchlistedBtn(sender:)), for: .touchUpInside)
        }
        else if "\((recordsArray[indexPath.row] as! NSDictionary)["watchlist_status"]!)".elementsEqual("0")
        {
          cell.WatchlistedBtnOutlet.isHidden = true
          cell.WatchlistBtnOutlet.isHidden = false
          cell.WatchlistBtnOutlet.addTarget(self, action: #selector(WatchlistBtn(sender:)), for: .touchUpInside)
        }
        else
        {
            cell.WatchlistedBtnOutlet.isHidden = false
            cell.WatchlistBtnOutlet.isHidden = true
        }

        
//        cell.titleName.font = UIFont(name: cell.titleName.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
//        cell.productName.font = UIFont(name: cell.titleName.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
//        cell.dailyRentalTitleLBL.font = UIFont(name: cell.dailyRentalTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
//        cell.dailyRentalPriceLBL.font = UIFont(name: cell.dailyRentalPriceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
//        cell.hourlyRentalTitleLBL.font = UIFont(name: cell.hourlyRentalTitleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
//        cell.hourlyRentalPriceLBL.font = UIFont(name: cell.hourlyRentalPriceLBL.font.fontName, size: CGFloat(Get_fontSize(size: 12)))
//        cell.bookButton.titleLabel?.font = UIFont(name: (cell.bookButton.titleLabel?.font.fontName)!, size: CGFloat(Get_fontSize(size: 15)))!

        //
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
//        let cell = cell as! DailyRentalListingTVCTableViewCell


        for view in cell.DailyRentalProductOptionsView.subviews {
            view.removeFromSuperview()
        }

//        for view in cell.DailyRentalProductFeatureView.subviews {
//            view.removeFromSuperview()
//        }

        var tempOrigin : CGFloat! = 0
//        var servicesTempOrigin : CGFloat! = 0


        // Creating Options View
        if ((recordsArray[indexPath.row] as! NSDictionary)["option_value"] as! NSArray).count == 0
        {
            cell.DailyRentalProductOptionsView.isHidden = true
        }
        else
        {
            cell.DailyRentalProductOptionsView.isHidden = false
            for i in 0..<((recordsArray[indexPath.row] as! NSDictionary)["option_value"] as! NSArray).count
            {

                let tempWidth = cell.DailyRentalProductOptionsView.frame.size.width / CGFloat(((recordsArray[indexPath.row] as! NSDictionary)["option_value"] as! NSArray).count)

                let tempBackgroundView = UIView(frame: CGRect(x: tempOrigin, y: 0, width: tempWidth, height: (50/568)*self.FullHeight))

                let tempView = UIView(frame: CGRect(x: 0, y: 0, width: (20/320)*self.FullWidth, height: (50/568)*self.FullHeight))

                let optionImage = UIImageView(frame: CGRect(x: 0, y: (10/568)*self.FullHeight, width: (20/320)*self.FullWidth, height: (20/568)*self.FullHeight))

                optionImage.sd_setImage(with: URL(string: "\((((recordsArray[indexPath.row] as! NSDictionary)["option_value"] as! NSArray)[i] as! NSDictionary)["attribut_img"]!)"))
                optionImage.contentMode = .scaleAspectFit

                let optionName = UILabel(frame: CGRect(x: (25/320)*self.FullWidth, y: (10/568)*self.FullHeight, width: (20/320)*self.FullWidth, height: (20/568)*self.FullHeight))

                optionName.textAlignment = .natural

                optionName.text = "\((((recordsArray[indexPath.row] as! NSDictionary)["option_value"] as! NSArray)[i] as! NSDictionary)["title_value"]!)"

                optionName.font = UIFont(name: cell.dailyRentalProductName.font.fontName, size: CGFloat(Get_fontSize(size: 12)))

                optionName.sizeToFit()

                tempView.frame.size.width = optionName.frame.origin.x + optionName.frame.size.width

                tempView.addSubview(optionImage)
                tempView.addSubview(optionName)

                tempView.frame.origin.x = (tempWidth - tempView.frame.size.width)/2

                tempBackgroundView.addSubview(tempView)
                cell.DailyRentalProductOptionsView.addSubview(tempBackgroundView)

                tempOrigin = tempOrigin + tempWidth
            }

        }


//        cell.DailyRentalProductImage.removeAllConstraints()
//
//
//        //Creating Services View
//        if ((recordsArray[indexPath.row] as! NSDictionary)["service_name"] as! NSArray).count > 0
//        {
//
//            cell.DailyRentalProductFeatureView.isHidden = false
//            cell.DailyRentalProductImage.contentMode = .scaleAspectFill
//
//            cell.DailyRentalProductImage.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = false
//            cell.DailyRentalProductImage.leadingAnchor.constraint(equalTo: cell.leadingAnchor, constant: 8).isActive = true
//            cell.DailyRentalProductImage.topAnchor.constraint(equalTo: cell.dailyRentalProductName.bottomAnchor, constant: 10).isActive = true
//            cell.DailyRentalProductImage.heightAnchor.constraint(equalToConstant: (128)).isActive = true
//            cell.DailyRentalProductImage.widthAnchor.constraint(equalToConstant: 128).isActive = true
//
//            cell.DailyRentalProductFeatureView.leadingAnchor.constraint(equalTo: cell.DailyRentalProductImage.trailingAnchor, constant: 22).isActive = true
//
//            cell.DailyRentalProductFeatureView.heightAnchor.constraint(equalToConstant: 128).isActive = true
//            cell.DailyRentalProductFeatureView.trailingAnchor.constraint(equalTo: cell.trailingAnchor, constant: 8).isActive = true
//
//
//            for i in 0..<((recordsArray[indexPath.row] as! NSDictionary)["service_name"] as! NSArray).count
//            {
//
//
//                let tempView = UIView(frame: CGRect(x: 0, y: servicesTempOrigin, width: cell.DailyRentalProductFeatureView.frame.size.width, height: (20/568)*self.FullHeight))
//
//                let servicesImage = UIImageView(frame: CGRect(x: 0, y: (5/568)*self.FullHeight, width: (15/320)*self.FullWidth, height: (15/568)*self.FullHeight))
//
//                let servicesName = UILabel(frame: CGRect(x: (20/320)*self.FullWidth, y: 0, width: (cell.DailyRentalProductFeatureView.frame.size.width - (10/320)*self.FullWidth), height: (20/568)*self.FullHeight))
//
//                servicesImage.image = UIImage(named: "redCheck")
//                servicesImage.contentMode = .scaleAspectFit
//
//                servicesName.text = "\((((recordsArray[indexPath.row] as! NSDictionary)["service_name"] as! NSArray)[i] as! NSDictionary)["service_name"]!)"
//
//                servicesName.font = UIFont(name: cell.dailyRentalProductName.font.fontName, size: CGFloat(Get_fontSize(size: 10)))
//                servicesName.numberOfLines = 1
//
//                tempView.addSubview(servicesImage)
//                tempView.addSubview(servicesName)
//
//                cell.DailyRentalProductFeatureView.addSubview(tempView)
//
//                servicesTempOrigin = servicesTempOrigin + (20/568)*self.FullHeight
//            }
//            cell.DailyRentalProductImage.layoutIfNeeded()
//            cell.DailyRentalProductFeatureView.layoutIfNeeded()
//        }
//
//        else
//        {
//            cell.DailyRentalProductFeatureView.isHidden = true
//            cell.DailyRentalProductImage.contentMode = .scaleAspectFill
//            cell.DailyRentalProductImage.heightAnchor.constraint(equalToConstant: (128)).isActive = true
//            cell.DailyRentalProductImage.widthAnchor.constraint(equalToConstant: 128).isActive = true
//            cell.DailyRentalProductImage.centerXAnchor.constraint(equalTo: cell.centerXAnchor).isActive = true
//            cell.DailyRentalProductImage.removeConstraint(cell.ProductImageTopConstraint)
//            cell.DailyRentalProductImage.removeConstraint(cell.ProductImageLeadingConstraint)
////            cell.DailyRentalProductImage.trailingAncho
//
//            cell.DailyRentalProductImage.layoutIfNeeded()
//            cell.DailyRentalProductFeatureView.layoutIfNeeded()
//        }


        cell.WatchlistBtnOutlet.tag = indexPath.row
        cell.WatchlistedBtnOutlet.tag = indexPath.row
        

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        

    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
        
//        let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "dailyRentalProductDetailsVC") as! DailyRentalProductDetailsViewController
//
//        navigate.productID = "\((recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
//
//        self.navigationController?.pushViewController(navigate, animated: true)


        let nav = UIStoryboard(name: "Extras", bundle: nil).instantiateViewController(withIdentifier: "dailyrental") as! NewDailyRentalProductDetailsViewController
        nav.DailyRentalProductID = "\((recordsArray[indexPath.row] as! NSDictionary)["product_id"]!)"
        self.navigationController?.pushViewController(nav, animated: true)
        
    }
    
    // MARK: - // Scrollview Delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollBegin = scrollView.contentOffset.y
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        scrollEnd = scrollView.contentOffset.y
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollBegin > scrollEnd
        {
            
        }
        else
        {
            if (nextStart).isEmpty
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
            else
            {
                getDailyRentalListing()
            }
        }
    }
    
    
    
    // MARK: - Book Rental Button
    
//    @objc func bookRental(sender: UIButton) {
//
//        if (UserDefaults.standard.string(forKey: "userID")?.isEmpty)!
//        {
//
////            let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
//
//            let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "logInVC") as! LogInViewController
//
//
//            self.navigationController?.pushViewController(navigate, animated: true)
//
//        }
//        else
//        {
//
//            self.getDailyRentalProductDetails(index: sender.tag, completion: {
//
//                 let navigate = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "dailyRentalBookingVC") as! DailyRentalBookingViewController
//
//                navigate.extraServicesArray = self.extraServicesArray.copy() as? NSArray
//
//                navigate.finalPriceString = self.finalPriceString
//
//                navigate.depositString = self.depositString
//
//                self.navigationController?.pushViewController(navigate, animated: true)
//
//            })
//
//        }
//
//    }

    
    
    // MARK: - // JSON POST Method to get Daily Rental Listing
    
    func getDailyRentalListing()
        
    {
        
        var parameters : String = ""
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        print(searchOptionOneCommaSeparatedString!)
        print(searchOptionTwoCommaSeparatedString!)
        
        parameters = "lang_id=\(langID!)&search_category=\(categoryID ?? "")&start_from=\(startValue)&per_page=\(perLoad)&pick_up_date=\(pickupDate ?? "")&pick_up_time=\(pickupTime ?? "")&drop_off_date=\(dropoffDate ?? "")&drop_off_time=\(dropoffTime ?? "")&driver_age=\(driverAge ?? "")&user_id=\(userID!)&search_option1=\(searchOptionTwoCommaSeparatedString ?? "")&search_option2=\(searchOptionOneCommaSeparatedString ?? "")&map_list_status=\(MapStatus ?? ""),sort_search=\(self.sortingType ?? "")&max_amount=\(self.maxFilterPrice!)"
        
        self.CallAPI(urlString: "app_daily_rental_search_and_listing", param: parameters) {
            

            
            self.dailyRentalListingArray = self.globalJson["info_array"] as! NSMutableArray
            
            for i in 0..<(self.dailyRentalListingArray.count)
                
            {
                
                let tempDict = self.dailyRentalListingArray[i] as! NSDictionary
                
                self.recordsArray.add(tempDict as! NSMutableDictionary)
                
            }
            
            let tempArray = (self.globalJson["search_parameter"] as! NSArray).copy() as! NSArray
            
            self.sortArray.removeAllObjects()
            
            self.makeSortArray(fromArray: tempArray, toArray: self.sortArray)
            
            
            self.nextStart = "\(self.globalJson["next_start"]!)"
            
            self.startValue = self.startValue + self.dailyRentalListingArray.count
            
            print("Next Start Value : " , self.startValue)
            
//            self.globalDispatchgroup.notify(queue: .main, execute: {
//
//                self.AppendMapData()
//            })
//
            DispatchQueue.main.async {
                
                self.globalDispatchgroup.leave()
                
                self.AppendMapData()

                
//                self.AppendMapData()
                
                self.dailyRentalListingTableview.delegate = self
                self.dailyRentalListingTableview.dataSource = self
                self.dailyRentalListingTableview.reloadData()
                self.dailyRentalListingTableview.rowHeight = UITableView.automaticDimension
                self.dailyRentalListingTableview.estimatedRowHeight = UITableView.automaticDimension
                
                SVProgressHUD.dismiss()
            }
            
        }
    }
    
    
    //Marker Highlight Method
    
    func highlight(_ marker: GMSMarker?) {
        if GmapView?.selectedMarker != nil {
        }
        do {
            marker?.icon = UIImage(named: "marker-selected-icon")
        }
    }
    
    //MARK:- //Marker Unhighlighting method
    
    func unhighlightMarker(_ marker: GMSMarker?) {
        marker?.icon = UIImage(named: "marker-icon")
    }
    
    
//    private func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//        if isMarkerActive == true {
//            if mapView.selectedMarker != nil {
//                isMarkerActive = false
//                unhighlightMarker(selectedMarker)
//                selectedMarker = nil
//                mapView.selectedMarker = nil
//            }
//        }
//    }
    
    
   
    
    
    
    // MARK: - // JSON POST Method to get Daily Rental Product Details
    
    func getDailyRentalProductDetails(index : NSInteger, completion: @escaping ()->())
        
    {
        
        dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        
        
        let url = URL(string: GLOBALAPI + "app_daily_rental_details")!   //change the url
        
        var parameters : String = ""
        
        productID = "\((recordsArray[index] as! NSDictionary)["product_id"]!)"
        
        parameters = "product_id=\(productID!)"
        
        print("Daily Rental Product Details URL is : ",url)
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        }
//        catch let error {
//            print(error.localizedDescription)
//        }
//        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("Daily Rental Product Details Response: " , json)
                    
                    if "\(json["response"]!)".elementsEqual("0")
                    {
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            SVProgressHUD.dismiss()
                            
                        }
                        
                    }
                    else
                    {
//                        self.dailyRentalProductDetailsArray = json["info_array"] as! NSMutableArray
//
                        self.extraServicesArray = (((json["info_array"] as! NSArray)[0] as! NSDictionary)["extra_services"] as! NSArray)
                        
                        self.finalPriceString = "\((((json["info_array"] as! NSArray))[0] as! NSDictionary)["final_value"]!)"
                        
                        self.depositString = "\((((json["info_array"] as! NSArray))[0] as! NSDictionary)["security_amount_value"]!)"
                        
                        
                        DispatchQueue.main.async {
                            
                            self.dispatchGroup.leave()
                            
                            completion()
                            
                            SVProgressHUD.dismiss()
                        }
                    }
                    
                    
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }
    
    
    // MARK:- // Set Font
    
    func setFont()
    {
        headerView.headerViewTitle.font = UIFont(name: headerView.headerViewTitle.font.fontName, size: CGFloat(Get_fontSize(size: 14)))
    }
    
    // MARK:- // MAke Sorting Array
    
    public func makeSortArray(fromArray: NSArray , toArray: NSMutableArray)
    {
        
        for i in 0..<fromArray.count
        {

            var tempArray = [sortClass]()
            
            for j in 0..<((fromArray[i] as! NSDictionary)["child_search_parameter"] as! NSArray).count
            {
                
                tempArray.append(sortClass(key: "\((((fromArray[i] as! NSDictionary)["child_search_parameter"] as! NSArray)[j] as! NSDictionary)["child_id"]!)", value: "\((((fromArray[i] as! NSDictionary)["child_search_parameter"] as! NSArray)[j] as! NSDictionary)["child_name"]!)", isClicked: false , totalNo: "\((((fromArray[i] as! NSDictionary)["child_search_parameter"] as! NSArray)[j] as! NSDictionary)["total_no"]!)"))
                
            }
            
//            let tempArrayData = NSKeyedArchiver.archivedData(withRootObject: tempArray)
            
            let tempDict = ["id" : "\((fromArray[i] as! NSDictionary)["parent_id"]!)" , "key" : "\((fromArray[i] as! NSDictionary)["parent_name"]!)" , "value" : tempArray] as [String : Any]
            
            toArray.add(tempDict)
            
        }
        
    }
    
    // MARK:- // Button Action
    
    
    @objc func buttonAction(sender: UIButton)
    {
        
//        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
          let navigate = UIStoryboard(name: "AutoLayout", bundle: nil).instantiateViewController(withIdentifier: "sortVC") as! SortViewController
        
        navigate.sortArray = self.sortArray.mutableCopy() as? NSMutableArray
        
        navigate.tempParams = tempParams
        
        self.navigationController?.pushViewController(navigate, animated: true)
        
    }

//    //MARK:- //mark: Daily Rental Listing page Map Shown
//    func ShowLocation()
//    {
//        for i in (0..<self.recordsArray.count)
//        {
//            if "\((self.recordsArray[i] as! NSDictionary)["lat"]!)".isEmpty == true
//            {
//                "\((self.recordsArray[i] as! NSDictionary)["lat"]!)".elementsEqual("0")
//            }
//            else if "\((self.recordsArray[i] as! NSDictionary)["long"]!)".isEmpty == true
//            {
//                "\((self.recordsArray[i] as! NSDictionary)["lat"]!)".elementsEqual("0")
//            }
//            else
//            {
//                let locationDetail = CityLocation(subtitle: "\((self.recordsArray[i] as! NSDictionary)["product_id"]!)", title: "\((self.recordsArray[i] as! NSDictionary)["product_name"]!)", coordinate: CLLocationCoordinate2D(latitude:Double("\((self.recordsArray[i] as! NSDictionary)["lat"] ?? "")")!, longitude:Double("\((self.recordsArray[i] as! NSDictionary)["long"] ?? "")")!))
//
//                print("longitude===",("\((self.recordsArray[i] as! NSDictionary)["long"] ?? "")"))
//                print("\((self.recordsArray[i] as! NSDictionary)["product_name"]!)")
//                print("\((self.recordsArray[i] as! NSDictionary)["product_id"]!)")
//                self.mapView.addAnnotation(locationDetail)
//            }
//        }
//
//    }

    //MARK:- //Annotation show methods
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }

        let reuseId = "Pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView?.canShowCallout = true

            let rightButton: AnyObject! = UIButton(type: UIButton.ButtonType.detailDisclosure)
            pinView?.rightCalloutAccessoryView = rightButton as? UIView

        }
        else {
            pinView?.annotation = annotation
        }

        return pinView
    }



    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        print(#function)
        if control == view.rightCalloutAccessoryView {
            print("notning")
            print(self.ProId!)
//            self.ShowLocation()
            self.ProductDetailsView.isHidden = false
        }
        else
        {
            self.ProductDetailsView.isHidden = true
        }
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        print("The annotation selected was","\(view.annotation?.subtitle!)")

        self.ProId = "\(view.annotation?.subtitle! ?? "")"
        self.ShowProductDetails()
    }

    //MARK:- //Product Details View
    func ShowProductDetails()
    {
        for i in (0..<self.recordsArray.count)
        {
            let tempId = "\((recordsArray[i] as! NSDictionary)["product_id"]!)"
            if tempId == ProId
            {
                if "\((self.recordsArray[i] as! NSDictionary)["watchlist_status"]!)".isEmpty == true
                {
                    self.Product_WatchlistBtnOutlet.isHidden = false
                    self.Product_watchlistedOutlet.isHidden = true
                }
                else if "\((self.recordsArray[i] as! NSDictionary)["watchlist_status"]!)".elementsEqual("0")
                {
                    self.Product_WatchlistBtnOutlet.isHidden = false
                    self.Product_watchlistedOutlet.isHidden = true
                }
                else if "\((self.recordsArray[i] as! NSDictionary)["watchlist_status"]!)".elementsEqual("1")
                {
                    self.Product_WatchlistBtnOutlet.isHidden = true
                    self.Product_watchlistedOutlet.isHidden = false
                }
                else
                {
                    //Do nothing
                }
               self.Product_Name.text = "\((recordsArray[i] as! NSDictionary)["product_name"]!)"
               self.Product_Type.text =  "\((recordsArray[i] as! NSDictionary)["title_name"]!)"
                self.Product_DailyRental.text = "Product daily rental:-" + " " + "\((recordsArray[i] as! NSDictionary)["currency_symbol"]!)"  + "\((recordsArray[i] as! NSDictionary)["Daily_Rentals"]!)"
                self.Product_Hourly_Rental.text =  "Product hourly rental:-" + " " + "\((recordsArray[i] as! NSDictionary)["currency_symbol"]!)"  + "\((recordsArray[i] as! NSDictionary)["Hourly_Rental"]!)"
                let path = "\((self.recordsArray[i] as! NSDictionary)["photo"]!)"
                self.Product_Image.sd_setImage(with: URL(string: path))
               //Scrollview For Product options

                let imageArray = (((recordsArray[i] as! NSDictionary)["option_value"] as! NSMutableArray))
                print("ImageArray",imageArray)
                let subViews = FeatureScrollView.subviews
                for subview in subViews{
                    subview.removeFromSuperview()
                }
                var tempOrigin : CGFloat! = 10
                for i in 0..<imageArray.count
                {
                    let imgView = UIImageView(frame: CGRect(x: tempOrigin, y: (6/568)*self.FullHeight, width: (20/320)*self.FullWidth, height: (34/568)*self.FullHeight))
                    let imageString = "\((imageArray[i] as! NSDictionary)["attribut_img"]!)"
                    let ImageCount = "\((imageArray[i] as! NSDictionary)["title_value"]!)"
                    imgView.sd_setImage(with: URL(string: imageString))
                    imgView.sizeToFit()
                    imgView.clipsToBounds = true
                    FeatureScrollView.addSubview(imgView)
                    tempOrigin = tempOrigin + imgView.frame.size.width + (55/568)*FullWidth

                    let CountLbl = UILabel(frame: CGRect(x: tempOrigin - (22/320)*self.FullWidth, y: (6/568)*self.FullHeight, width: (10/320)*self.FullWidth, height: (34/568)*self.FullHeight))
                    CountLbl.text = ImageCount
                    let separatorView = UIView(frame: CGRect(x: tempOrigin - (10/320)*self.FullWidth, y: (6/568)*self.FullHeight, width: (1/320)*self.FullWidth, height: (34/568)*self.FullHeight))
                    separatorView.backgroundColor = UIColor.lightGray
                    FeatureScrollView.addSubview(separatorView)
                    FeatureScrollView.addSubview(CountLbl)
                    print("TempOrigin",tempOrigin)
                    imgView.contentMode = .scaleAspectFit
                    imgView.clipsToBounds = true
                    FeatureScrollView.contentSize = CGSize(width: tempOrigin, height: (36/568)*self.FullHeight)
                }
//                self.Product_BookBtnOutlet.addTarget(self, action: #selector(Book), for: .touchUpInside)
                self.Product_WatchlistBtnOutlet.addTarget(self, action: #selector(WatchlistBtn), for: .touchUpInside)
                self.Product_watchlistedOutlet.addTarget(self, action: #selector(WatchlistedBtn), for: .touchUpInside)

            }
            else
            {
               //Do Nothing
            }

        }
    }

    //MARK:- //Set layer for Product Details View
    func SetLayer()
    {
        self.ProductDetailsView.layer.borderWidth = 1.0
        self.ProductDetailsView.layer.borderColor = UIColor.darkText.cgColor

        self.DetailsView.layer.borderWidth = 0.5
        self.DetailsView.layer.borderColor = UIColor.darkText.cgColor
        
        self.GProductDetailsView.layer.borderWidth = 1.0
        self.GProductDetailsView.layer.borderColor = UIColor.black.cgColor
        
        self.GNextBtnOutlet.setImage(UIImage(named: "next"), for: .normal)
        self.GNextBtnOutlet.backgroundColor = .white
        self.GPreviousBtnOutlet.setImage(UIImage(named: "next (1)"), for: .normal)
        self.GPreviousBtnOutlet.backgroundColor = .white
    }
    
    
  

    //MARK:- Navigate to Product Details page

//    @objc func Book()
//    {
//        let nav = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "dailyRentalProductDetailsVC") as! DailyRentalProductDetailsViewController
//        nav.productID = ProId
//        self.navigationController?.pushViewController(nav, animated: true)
//    }

    //MARK:- Add To Watchlist Btn

    @objc func WatchlistBtn(sender: UIButton)
    {
         let userID = UserDefaults.standard.string(forKey: "userID")

        if userID?.isEmpty == true
        {
            let alert = UIAlertController(title: "Warning", message: "Please login and try again later", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)

        }
        else
        {
            self.GlobalAddtoWatchlist(ProductID: "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)", completion: {
            self.recordsArray.removeAllObjects()
            self.startValue = 0
            self.getDailyRentalListing()
            })
        }

    }
    //MARK:- //remove from watchlist
    @objc func WatchlistedBtn(sender: UIButton)
    {
        let userID = UserDefaults.standard.string(forKey: "userID")

        if userID?.isEmpty == true
        {
            let alert = UIAlertController(title: "Warning", message: "Please login and try again later", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)

        }
        else
        {
            self.GlobalRemoveFromWatchlist(ProductID: "\((self.recordsArray[sender.tag] as! NSDictionary)["product_id"]!)", completion: {
                self.recordsArray.removeAllObjects()
                self.startValue = 0
                self.getDailyRentalListing()
            })
        }

    }
    
    //Creating Custom Marker
    
    func drawTextT(text:NSString, inImage:UIImage) -> UIImage? {
        
        let font = UIFont.systemFont(ofSize: 11)
        let size = inImage.size
        
        UIGraphicsBeginImageContext(size)
        
        inImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let style : NSMutableParagraphStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        style.alignment = .center
        let attributes:NSDictionary = [ NSAttributedString.Key.font : font, NSAttributedString.Key.paragraphStyle : style, NSAttributedString.Key.foregroundColor : UIColor.red ]
        
        //let textSize = text.size(attributes: attributes as? [String : Any])
        let textSize = text.size(withAttributes: attributes as? [NSAttributedString.Key : Any] )
        
        let rect = CGRect(x: 0, y: 0, width: inImage.size.width, height: inImage.size.height)
        let textRect = CGRect(x: (rect.size.width - textSize.width)/2, y: (rect.size.height - textSize.height)/2 - 2, width: textSize.width, height: textSize.height)
        text.draw(in: textRect.integral, withAttributes: attributes as? [NSAttributedString.Key : Any]  )
        
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return resultImage
    }


}
extension UIView {
    func removeAllConstraints() {
        self.removeConstraints(self.constraints)
        for view in self.subviews {
            view.removeAllConstraints()
        }
    }
}
//MARK:- //Daily Rental TableViewCell

class DailyRentalListingTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var CarType: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var servicesView: UIView!
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var priceAndBookView: UIView!
    @IBOutlet weak var dailyRentalPriceView: UIView!
    @IBOutlet weak var dailyRentalTitleLBL: UILabel!
    @IBOutlet weak var dailyRentalPriceLBL: UILabel!
    @IBOutlet weak var hourlyRentalPriceView: UIView!
    @IBOutlet weak var hourlyRentalTitleLBL: UILabel!
    @IBOutlet weak var hourlyRentalPriceLBL: UILabel!

    @IBOutlet weak var bookButton: UIButton!


    @IBOutlet weak var cellBottomView: UIView!




    //NEW Outlet Connection
    @IBOutlet weak var WatchlistBtnOutlet: UIButton!
    @IBOutlet weak var dailyRentalProductName: UILabel!
    @IBOutlet weak var DailyRentalProductImage: UIImageView!
    @IBOutlet weak var DailyRentalProductFeatureView: UIView!
    @IBOutlet weak var DailyRentalProductOptionsView: UIView!
    @IBOutlet weak var DailyRentalProductPriceView: UIView!
    @IBOutlet weak var DailyRentalProductSellerImage: UIImageView!
    @IBOutlet weak var DailyRentalProductBookBtnOutlet: UIButton!

    @IBOutlet weak var WatchlistedBtnOutlet: UIButton!
    @IBOutlet weak var ProductImageLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var ProductImageTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var DailyRentalPrice: UILabel!
    @IBOutlet weak var DailyRentalMonthlyPrice: UILabel!











    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}




// MARK:- // Pickerview Delegates

extension DailyRentalListingViewController: UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.sortDict.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\((self.sortDict[row])["value"]!)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        sortingType = "\((self.sortDict[row])["key"]!)"
//        print("SortingType",sortingType)
        self.ShowSortView.isHidden = true
        self.startValue = 0
        self.recordsArray.removeAllObjects()
        self.getDailyRentalListing()
    }
    
}

