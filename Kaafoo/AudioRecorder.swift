//
//  AudioRecorderFile.swift
//  Kaafoo
//
//  Created by esolz on 26/10/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import Foundation

class AudioRecorder : UIView
{
    @IBOutlet var ContentView: UIView!
    @IBOutlet weak var AudioPlayButton: UIButton!
    @IBOutlet weak var AudioStopButton: UIButton!
    @IBOutlet weak var AudioSaveButton: UIButton!
    @IBOutlet weak var AudioPlayerView: UIView!
    @IBOutlet weak var AudioSendButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("AudioRecorder", owner: self, options: nil)
        addSubview(ContentView)
        
        ContentView.frame = self.bounds
        ContentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.SetLayer()
    }
    
    func SetLayer()
    {
        self.AudioPlayButton.layer.cornerRadius = 2.0
        self.AudioStopButton.layer.cornerRadius = 2.0
        self.AudioSaveButton.layer.cornerRadius = 2.0
        self.AudioPlayButton.clipsToBounds = true
        self.AudioStopButton.clipsToBounds = true
        self.AudioSaveButton.clipsToBounds = true
    }
    
}
