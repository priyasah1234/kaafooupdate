//
//  NewReviewListingTableViewCell.swift
//  Kaafoo
//
//  Created by Kaustabh on 15/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit

class NewReviewListingTableViewCell: UITableViewCell {

    @IBOutlet var description_lbl: UILabel!
    @IBOutlet var date_lbl: UILabel!
    @IBOutlet var name_lbl: UILabel!
    
    @IBOutlet var ratingNoLbl: UILabel!
    @IBOutlet var RatingLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
