//
//  NewReviewListViewController.swift
//  Kaafoo
//
//  Created by Kaustabh on 15/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//
//
//  ReviewListViewController.swift
//  Kaafoo
//
//  Created by Kaustabh on 29/11/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class NewReviewListViewController: GlobalViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var NewReviewListTable: UITableView!
    
    @IBAction func Back_btn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    //let langID = UserDefaults.standard.string(forKey: "langID")
    var ProId:NSString!
    let dispatchGroup = DispatchGroup()
    var info_array:NSDictionary!
    var review_rating:NSDictionary!
    var review_listing: NSMutableArray!
    var review_list: NSMutableArray! = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
//        print("PRODUCTId",ProId)
        loadData()
        self.NewReviewListTable.rowHeight = UITableView.automaticDimension
        self.NewReviewListTable.estimatedRowHeight = (104/568)*self.FullHeight
        // Do any additional setup after loading the view.
    }
    func loadData()
    {
        
        //dispatchGroup.enter()
        
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
        let url = URL(string: GLOBALAPI + "app_product_details")!   //change the url
        
        print("add from watchlist URL : ", url)
        
        var parameters : String = ""
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        parameters = "user_id=\(userID!)&product_id=\(ProId!)&lang_id=\(langID!)"
        
        print("Parameters are : " , parameters)
        
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
        } 
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    print("add to watchlist Response: " , json)
                    self.info_array = json["product_info"] as? NSDictionary
                    self.review_rating = self.info_array["review_rating"] as? NSDictionary
                    self.review_listing = self.review_rating["review_list"] as? NSMutableArray
//                    print("REVIEW_LIsting",self.review_listing)
                    for i in (0..<self.review_listing.count)
                    {
                        let ProDetail = self.review_listing[i] as! NSDictionary
                        print("ProDetail is : " , ProDetail)
                        
                        self.review_list.add(ProDetail )
                        DispatchQueue.main.async {
                            
                            //self.dispatchGroup.leave()
                            self.NewReviewListTable.delegate = self
                            self.NewReviewListTable.dataSource = self
                            self.NewReviewListTable.reloadData()
                            SVProgressHUD.dismiss()
                        }
                        
                    }
                }
            }
            catch let error {
                // print(error.localizedDescription)
            }
            
        })
        
        task.resume()
    }
    
    
    //MARK:- TableView Delegate and Datasource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return review_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = tableView.dequeueReusableCell(withIdentifier: "newreview") as! NewReviewListingTableViewCell
        obj.name_lbl.text = "\((review_list[indexPath.row] as! NSDictionary)["name"]!)"
        obj.date_lbl.text = "\((review_list[indexPath.row] as! NSDictionary)["date"]!)"
        obj.description_lbl.text = "\((review_list[indexPath.row] as! NSDictionary)["comment"]!)"
        obj.ratingNoLbl.text = "\((review_list[indexPath.row] as! NSDictionary)["rating_no"]!)"
        obj.RatingLbl.text = "(" + "\((review_list[indexPath.row] as! NSDictionary)["total_rating"]!)" + ")"
//        obj.review_view.layer.cornerRadius = 10.0
        return obj
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}



