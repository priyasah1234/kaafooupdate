//
//  SortView.swift
//  Kaafoo
//
//  Created by admin on 29/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

protocol sort_view_delegate: class {
    
    func navigateToSortOptions()
    
}

class SortView: UIView {
 
    
    @IBOutlet weak var sortBackgroundView: UIView!
    @IBOutlet weak var sortOptionsView: UIView!
    @IBOutlet weak var menuUpImageview: UIImageView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var sampleLBL: UILabel!

    var tapGesture = UITapGestureRecognizer()
    
    var sortOptionsArray = [String]()
    
    var FullHeight = (UIScreen.main.bounds.size.height)
    var FullWidth = (UIScreen.main.bounds.size.width)
    
    static let Isiphone6 = (UIScreen.main.bounds.size.height==667) ? true : false
    static let Isiphone6Plus = (UIScreen.main.bounds.size.height==736) ? true : false
    static let IsiphoneX = (UIScreen.main.bounds.size.height==812) ? true : false
    
    weak var Sort_delegate: sort_view_delegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("SortView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.initiateArray()
        
        self.createView()
        
    }
    
    // MARK:- // Initiate Sort Options View Array
    
    func initiateArray()
    {
        if (self.topViewController()?.isKind(of: DailyRentalListingViewController.self))!
        {
            sortOptionsArray = ["Search" , "Advanced Search"]
        }
    }
    
    // MARK:- // Create Sort OPtions VIew
    
    func createView()
    {
        var tempOrigin : CGFloat! = 0
        
        for i in 0..<sortOptionsArray.count
        {
            let tempView = UIView(frame: CGRect(x: 0, y: tempOrigin, width: (155/320)*self.FullWidth, height: (35/568)*self.FullHeight))
            
            let tempLBL = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (135/320)*self.FullWidth, height: (25/568)*self.FullHeight))
            
            tempLBL.text = sortOptionsArray[i]
            
            tempLBL.font = UIFont(name: sampleLBL.font.fontName, size: CGFloat(Get_fontSize(size: 13)))
            
            let tempBTN = UIButton(frame: CGRect(x: 0, y: 0, width: (155/320)*self.FullWidth, height: (35/568)*self.FullHeight))
            
            tempBTN.tag = i
            
            tempBTN.addTarget(self, action: #selector(self.startSorting(sender:)), for: .touchUpInside)
            
            tempView.addSubview(tempLBL)
            tempView.addSubview(tempBTN)
            
            self.sortOptionsView.addSubview(tempView)
            
            tempOrigin = tempOrigin + (35/568)*self.FullHeight
            
        }
        
        self.sortOptionsView.clipsToBounds = true
        
        self.sortOptionsView.autoresizesSubviews = false
        
        self.sortOptionsView.frame.size.height = tempOrigin
        
        self.sortOptionsView.autoresizesSubviews = true
        
        self.sortOptionsView.layer.cornerRadius = (5/568)*self.FullHeight
        
    }
    
    // MARK:- // Start Sorting
    
    
    @objc func startSorting(sender: UIButton)
    {
        
        Sort_delegate?.navigateToSortOptions()
        
//        let storyboardName = UserDefaults.standard.string(forKey: "storyboard")
//
//        let navigate = UIStoryboard(name: storyboardName!, bundle: nil).instantiateViewController(withIdentifier: "categoryAdvancedSearchVC") as! CategoryAdvancedSearchViewController
//
//        
//
//        self.navigationController?.pushViewController(navigate, animated: true)
        
    }
    
    
    
    
    
    // MARK: - // function TopViewController
    
    
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    // MARK:- // Function Get_Fontsize
    
    
    func Get_fontSize(size: Float) -> Float
    {
        var size1 = size
        
        if(GlobalViewController.Isiphone6)
        {
            size1 += 1.0
        }
        else if(GlobalViewController.Isiphone6Plus)
        {
            size1 += 2.0
        }
        else if(GlobalViewController.IsiphoneX)
        {
            size1 += 3.0
        }
        
        return size1
    }
    
}

