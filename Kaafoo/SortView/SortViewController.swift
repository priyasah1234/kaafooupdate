//
//  SortViewController.swift
//  Kaafoo
//
//  Created by admin on 29/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import Cosmos

class SortViewController: GlobalViewController {

    @IBOutlet weak var localHeaderView: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var locationStackview: UIStackView!
    @IBOutlet weak var locationContainerview: UIView!
    @IBOutlet weak var conditionStackview: UIStackView!
    @IBOutlet weak var conditionContainerview: UIView!
    @IBOutlet weak var locationTitleview: UIView!
    @IBOutlet weak var locationTitleLBL: UILabel!
    @IBOutlet weak var locationTitleImageview: UIImageView!
    @IBOutlet weak var conditionTitleView: UIView!
    @IBOutlet weak var conditionTitleLBL: UILabel!
    @IBOutlet weak var conditionTitleImageview: UIImageView!

    @IBOutlet weak var stateTitleLBL: UILabel!
    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var cityTitleLBL: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var suburbTitleLBL: UILabel!
    @IBOutlet weak var suburb: UILabel!

    @IBOutlet weak var allConditionLBL: UILabel!
    @IBOutlet weak var allConditionImageview: UIImageView!
    @IBOutlet weak var newConditionLBL: UILabel!
    @IBOutlet weak var newConditionImageview: UIImageView!

    @IBOutlet weak var usedConditionLBL: UILabel!
    @IBOutlet weak var usedConditionImageview: UIImageView!

    @IBOutlet weak var sortSubCategoryTableview: UITableView!
    @IBOutlet weak var sortCategoryTableview: UITableView!
    @IBOutlet weak var filterProductsTitleLBL: UILabel!
    @IBOutlet weak var backImageview: UIImageView!


    @IBOutlet weak var pickerBackgroundview: UIView!
    @IBOutlet weak var pagePicker: UIPickerView!

    @IBOutlet weak var sortCategoryTableHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var typeStackview: UIStackView!
    @IBOutlet weak var typeTitleView: UIView!
    @IBOutlet weak var typeTitleLBL: UILabel!
    @IBOutlet weak var typeTitleImageview: UIImageView!
    @IBOutlet weak var typeTableview: UITableView!
    @IBOutlet weak var typeTableHeightConstraint: NSLayoutConstraint!


    @IBOutlet weak var filterStackview: UIStackView!
    @IBOutlet weak var filterTitleView: UIView!
    @IBOutlet weak var filterTitleLBL: UILabel!
    @IBOutlet weak var filterTitleImageview: UIImageView!
    @IBOutlet weak var filterTableview: UITableView!
    @IBOutlet weak var filterTableHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var ratingStackview: UIStackView!
    @IBOutlet weak var ratingTitleview: UIView!
    @IBOutlet weak var ratingTitleLBL: UILabel!
    @IBOutlet weak var ratingTitleImageview: UIImageView!
    @IBOutlet weak var ratingContainerview: UIView!
    @IBOutlet weak var ratingView: CosmosView!

    @IBOutlet weak var deliveryCostStackview: UIStackView!
    @IBOutlet weak var deliveryCostTitleview: UIView!
    @IBOutlet weak var deliveryCostTitleLBL: UILabel!
    @IBOutlet weak var deliveryCostImageview: UIImageView!
    @IBOutlet weak var deliveryCostContainerview: UIView!
    @IBOutlet weak var noPreferenceView: UIView!
    @IBOutlet weak var noPreferenceTitleLBL: UILabel!
    @IBOutlet weak var noPreferenceImageview: UIImageView!
    @IBOutlet weak var freeView: UIView!
    @IBOutlet weak var freeTitleLBL: UILabel!
    @IBOutlet weak var freeImageview: UIImageView!


    @IBOutlet weak var priceStackview: UIStackView!
    @IBOutlet weak var priceTitleView: UIView!
    @IBOutlet weak var priceTitleLBL: UILabel!
    @IBOutlet weak var priceImageview: UIImageView!
    @IBOutlet weak var maxPriceLBL: UILabel!
    @IBOutlet weak var priceContainerview: UIView!
    


    @IBOutlet weak var distanceStackView: UIStackView!
    @IBOutlet weak var distanceTitleView: UIView!
    @IBOutlet weak var distanceTitleLbl: UILabel!
    @IBOutlet weak var distanceImageview: UIImageView!
    @IBOutlet weak var distanceContainerView: UIView!
    @IBOutlet weak var maxDistanceLbl: UILabel!






    var showDynamicType : Bool! = false
    var showLocation : Bool! = false
    var showCondition : Bool! = false
    var showType : Bool! = false
    var showFilter : Bool! = false
    var showRating : Bool! = false
    var showDeliveryCost : Bool! = false
    var showPrice : Bool! = false
    var showDistance : Bool! = false



    var sortArray : NSMutableArray! = NSMutableArray()

    var clickedOnIndex : Int!

    var searchOptionOneArray = [String]()
    var searchOptionTwoArray = [String]()

    var tempParams : String!

    var rootVC = GlobalViewController()

    var locationArray = NSArray()

    var stateArray = NSMutableArray()
    var cityArray = NSMutableArray()
    var suburbArray = NSMutableArray()

    var stateClicked : Bool! = false
    var cityClicked : Bool! = false
    var suburbClicked : Bool! = false

    var stateIndex : Int!
    var cityIndex : Int!
    var suburbIndex : Int!

    var pickerArray = NSMutableArray()

    var conditionImageArray = [UIImageView]()

    var typeArray = [sortClass]()
    var filterArray = [sortClass]()

    var deliveryCost : String! = ""
    
    var maxPrice : String! = ""

    var maxDistance : String! = ""

    var typeString : String = ""
    var filterString : String = ""

    // MARK:- // View DidLoad

    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.ratingView.rating = 0

        self.locationTitleview.backgroundColor = UIColor.yellow1()
        self.conditionTitleView.backgroundColor = UIColor.yellow1()
        self.typeTitleView.backgroundColor = UIColor.yellow1()
        self.filterTitleView.backgroundColor = UIColor.yellow1()
        self.ratingTitleview.backgroundColor = UIColor.yellow1()
        self.deliveryCostTitleview.backgroundColor = UIColor.yellow1()

        self.applyButtonOutlet.backgroundColor = UIColor.yellow2()


        self.sortCategoryTableview.delegate = self
        self.sortCategoryTableview.dataSource = self

        // Setting Sort Category Table Height
        self.sortCategoryTableHeightConstraint.constant = CGFloat(40 * self.sortArray.count)


        // Check if Show Dynamic Type
        if self.showDynamicType == true {
            self.sortCategoryTableview.isHidden = false
        }
        else {
            self.sortCategoryTableview.isHidden = true
        }


        //Check if Show Location
        if self.showLocation == true {
            self.locationStackview.isHidden = false

            //Creating State Array from Location Array
            for i in 0..<((self.locationArray[0] as! NSDictionary)["state"] as! NSArray).count {

                self.stateArray.add(["name" : "\((((self.locationArray[0] as! NSDictionary)["state"] as! NSArray)[i] as! NSDictionary)["region_name"] ?? "")" , "id" : "\((((self.locationArray[0] as! NSDictionary)["state"] as! NSArray)[i] as! NSDictionary)["region_id"] ?? "")"])

            }

            self.pagePicker.delegate = self
            self.pagePicker.dataSource = self
            self.pagePicker.reloadAllComponents()
        }
        else {
            self.locationStackview.isHidden = true
        }


        // Check if Show Condition
        if self.showCondition == true {

            self.conditionStackview.isHidden = false

            // Appending Imageviews in Array to handle Radio Selection in Condition Section
            self.conditionImageArray.append(self.allConditionImageview)
            self.conditionImageArray.append(self.newConditionImageview)
            self.conditionImageArray.append(self.usedConditionImageview)


        }
        else {
            self.conditionStackview.isHidden = true
        }


        //Check if show Type
        if self.showType == true {

            self.typeStackview.isHidden = false

            self.typeTableHeightConstraint.constant = CGFloat(40 * self.typeArray.count)

            self.typeTableview.delegate = self
            self.typeTableview.dataSource = self
            self.typeTableview.reloadData()
        }
        else {
            self.typeStackview.isHidden = true
        }


        // CHeck if Show Filter
        if self.showFilter == true {

            self.filterStackview.isHidden = false

            self.filterTableHeightConstraint.constant = CGFloat(40 * self.filterArray.count)


            self.filterTableview.delegate = self
            self.filterTableview.dataSource = self
            self.filterTableview.reloadData()
        }
        else {
            self.filterStackview.isHidden = true
        }



        // Check if Show Rating
        if self.showRating == true {
            self.ratingStackview.isHidden = false
        }
        else {
            self.ratingStackview.isHidden = true
        }

        // Check if Show Delivery Cost
        if self.showDeliveryCost == true {
            self.deliveryCostStackview.isHidden = false
        }
        else {
            self.deliveryCostStackview.isHidden = true
        }
        
        
        // Check if Show Price
        if self.showPrice == true {
            self.priceStackview.isHidden = false
        }
        else {
            self.priceStackview.isHidden = true
        }

        if self.showDistance == true
        {
            self.distanceStackView.isHidden = false
        }
        else
        {
            self.distanceStackView.isHidden = true
        }


    }







    // MARK:- // View DidAppear

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.sortSubCategoryTableview.estimatedRowHeight = 300
        self.sortSubCategoryTableview.rowHeight = UITableView.automaticDimension

    }




    // MARK:- // Buttons

    
    // MARK:- // Price Slider
    
    @IBOutlet weak var priceSliderOutlet: UISlider!
    @IBAction func priceSlider(_ sender: UISlider) {
        
        self.maxPriceLBL.text = "max: $" + "\(self.priceSliderOutlet.value)"
        
        if "\(self.priceSliderOutlet.value)".elementsEqual("500.0") {
            self.maxPriceLBL.text = "max: $" + "\(self.priceSliderOutlet.value)" + "+"
        }
        
        self.maxPrice = "\(self.priceSliderOutlet.value)"
        
    }

    //MARK:- // Distance Slider

    @IBOutlet weak var distanceSliderOutlet: UISlider!
    @IBAction func distanceSlider(_ sender: UISlider) {

        self.maxDistanceLbl.text = "max: $" + "\(self.distanceSliderOutlet.value)"

        if "\(self.distanceSliderOutlet.value)".elementsEqual("150.0") {
            self.maxDistanceLbl.text = "max: $" + "\(self.distanceSliderOutlet.value)" + "+"
        }

        self.maxDistance = "\(self.distanceSliderOutlet.value)"
    }

    
    
    
    
    // MARK:- // Price Open Close Button
    
    @IBOutlet weak var priceOpenCloseButtonOutlet: UIButton!
    @IBAction func priceOpenCloseButton(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            if self.priceContainerview.isHidden == true {
                self.priceContainerview.isHidden = false
            }
            else {
                self.priceContainerview.isHidden = true
            }
        }
        
    }

    //MARK:- // Distance Open Close Button

    
    @IBOutlet weak var distanceOpenCloseButtonOutlet: UIButton!
    @IBAction func distanceOpenCloseButton(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5) {
            if self.distanceContainerView.isHidden == true {
                self.distanceContainerView.isHidden = false
            }
            else {
                self.distanceContainerView.isHidden = true
            }
        }

    }
    
    
    

    // MARK:- // Delivery Cost Buttons

    @IBOutlet weak var noPreferenceDeliveryCostButtonOutlet: UIButton!
    @IBOutlet weak var freeDeliveryCostButtonOutlet: UIButton!
    @IBAction func deliveryCostButtons(_ sender: UIButton) {

        if sender.tag == 0 {
            self.noPreferenceImageview.image = UIImage(named: "radioSelected")
            self.freeImageview.image = UIImage(named: "radioUnselected")

            self.deliveryCost = "N"
        }
        else {
            self.noPreferenceImageview.image = UIImage(named: "radioUnselected")
            self.freeImageview.image = UIImage(named: "radioSelected")

            self.deliveryCost = "F"
        }

    }




    // MARK:- // Delivery Cost Open Close Button

    @IBOutlet weak var deliveryCostOpenCloseButtonOutlet: UIButton!
    @IBAction func deliveryCostOpenCloseButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {
            if self.deliveryCostContainerview.isHidden == true {
                self.deliveryCostContainerview.isHidden = false
            }
            else {
                self.deliveryCostContainerview.isHidden = true
            }
        }

    }



    // MARK:- // Rating Open Close Button

    @IBOutlet weak var ratingOpenCloseButtonOutlet: UIButton!
    @IBAction func ratingOpenCloseButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {
            if self.ratingContainerview.isHidden == true {
                self.ratingContainerview.isHidden = false
            }
            else {
                self.ratingContainerview.isHidden = true
            }
        }

    }




    // MARK:- // Filter Open Close Button

    @IBOutlet weak var filterOpenCloseButtonOutlet: UIButton!
    @IBAction func filterOpenCloseButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {
            if self.filterTableview.isHidden == true {
                self.filterTableview.isHidden = false
            }
            else {
                self.filterTableview.isHidden = true
            }
        }

    }





    // MARK:- // Type Open Close Button

    @IBOutlet weak var typeOpenCloseButtonOutlet: UIButton!
    @IBAction func typeOpenCloseButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {
            if self.typeTableview.isHidden == true {
                self.typeTableview.isHidden = false
            }
            else {
                self.typeTableview.isHidden = true
            }
        }

    }





    // MARK:- // Close Picker Button

    @IBOutlet weak var closePickerButtonOutlet: UIButton!
    @IBAction func closePickerButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {

            self.pickerBackgroundview.isHidden = true
            self.view.sendSubviewToBack(self.pickerBackgroundview)

        }

    }




    // MARK:- // Location Open Close Button

    @IBOutlet weak var locationOpenCloseButtonOutlet: UIButton!
    @IBAction func locationOpenCloseButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {
            if self.locationContainerview.isHidden == true {
                self.locationContainerview.isHidden = false
            }
            else {
                self.locationContainerview.isHidden = true
            }
        }

    }


    // MARK:- // Condition Open Close Button

    @IBOutlet weak var conditionOpenCLoseButtonOutlet: UIButton!
    @IBAction func conditionOpenCloseButton(_ sender: UIButton) {

        UIView.animate(withDuration: 0.5) {
            if self.conditionContainerview.isHidden == true {
                self.conditionContainerview.isHidden = false
            }
            else {
                self.conditionContainerview.isHidden = true
            }
        }

    }




    // MARK:- // State Button

    @IBOutlet weak var stateButtonOutlet: UIButton!
    @IBAction func stateButton(_ sender: UIButton) {

        if self.stateArray.count == 0 {
            self.ShowAlertMessage(title: "Oops...", message: "There are No states in Record to show here.")
        }
        else {
            self.stateClicked = true

            self.pickerArray = self.stateArray

            self.pagePicker.reloadAllComponents()

            UIView.animate(withDuration: 0.5) {

                self.pickerBackgroundview.isHidden = false
                self.view.bringSubviewToFront(self.pickerBackgroundview)

            }
        }

    }


    // MARK:- // City Button

    @IBOutlet weak var cityButtonOutlet: UIButton!
    @IBAction func cityButton(_ sender: UIButton) {

        if self.cityArray.count == 0 {
            self.ShowAlertMessage(title: "Oops...", message: "There are No cities in Record to show here.")
        }
        else {
            self.cityClicked = true

            self.pickerArray = self.cityArray

            self.pagePicker.reloadAllComponents()

            UIView.animate(withDuration: 0.5) {

                self.pickerBackgroundview.isHidden = false
                self.view.bringSubviewToFront(self.pickerBackgroundview)

            }
        }

    }

    // MARK:- // Suburb Button

    @IBOutlet weak var suburbButtonOutlet: UIButton!
    @IBAction func suburbButton(_ sender: UIButton) {

        if self.suburbArray.count == 0 {
            self.ShowAlertMessage(title: "Oops...", message: "There are No suburbs in Record to show here.")
        }
        else {
            self.suburbClicked = true

            self.pickerArray = self.suburbArray

            self.pagePicker.reloadAllComponents()

            UIView.animate(withDuration: 0.5) {

                self.pickerBackgroundview.isHidden = false
                self.view.bringSubviewToFront(self.pickerBackgroundview)

            }
        }

    }




    // MARK:- // Product Condition Button

    @IBOutlet weak var allConditionButton: UIButton!
    @IBOutlet weak var newConditionButton: UIButton!
    @IBOutlet weak var usedConditionButton: UIButton!


    @IBAction func conditionButtonsAction(_ sender: UIButton) {

        for imageview in self.conditionImageArray {
            imageview.image = UIImage(named: "radioUnselected")
        }

        self.conditionImageArray[sender.tag].image = UIImage(named: "radioSelected")

    }




    // MARK:- // Back Button

    @IBOutlet weak var backButtonOutlet: UIButton!
    @IBAction func tapBackButton(_ sender: UIButton) {

        if self.secondView.isHidden == true
        {

            self.navigationController?.popViewController(animated: true)

        }
        else
        {
            UIView.animate(withDuration: 0.3) {

                self.view.sendSubviewToBack(self.secondView)
                self.secondView.isHidden = true

                self.firstView.isHidden = false
                self.view.bringSubviewToFront(self.firstView)
            }

        }

    }





    // MARK:- // Reset Button

    @IBOutlet weak var resetButtonOutlet: UIButton!
    @IBAction func tapResetButton(_ sender: UIButton) {

        self.searchOptionOneArray.removeAll()
        self.searchOptionTwoArray.removeAll()

        let optionOneCommaSeparatedString = searchOptionOneArray.joined(separator: ",")
        let optionTwoCommaSeparatedString = searchOptionTwoArray.joined(separator: ",")

        print("Search Option One Array----",optionOneCommaSeparatedString)

        print("Search Option Two Array----",optionTwoCommaSeparatedString)

        let navigate = self.rootVC

        navigate.searchOptionOneCommaSeparatedString = optionOneCommaSeparatedString

        navigate.searchOptionTwoCommaSeparatedString = optionTwoCommaSeparatedString

        navigate.maxFilterPrice = self.maxPrice

        navigate.maxFilterDistance = self.maxDistance
        
        self.navigationController?.pushViewController(navigate, animated: true)

    }



    // MARK:- // Apply Button

    @IBOutlet weak var applyButtonOutlet: UIButton!
    @IBAction func tapApplyButton(_ sender: UIButton) {

        self.searchOptionOneArray.removeAll()
        self.searchOptionTwoArray.removeAll()

        for i in 0..<sortArray.count
        {
            for j in 0..<((sortArray[i] as! [String : Any])["value"] as! [sortClass]).count
            {
                if (((sortArray[i] as! [String : Any])["value"] as! [sortClass])[j]).isClicked == true
                {
                    searchOptionTwoArray.append((((sortArray[i] as! [String : Any])["value"] as! [sortClass])[j]).key)

                    if searchOptionOneArray.contains("\((sortArray[i] as! [String : Any])["id"]!)") {

                    }
                    else {
                        searchOptionOneArray.append("\((sortArray[i] as! [String : Any])["id"]!)")
                    }


                }
            }
        }


        if self.secondView.isHidden == false
        {

            UIView.animate(withDuration: 0.5, animations: {

                self.secondView.isHidden = true

                self.firstView.isHidden = false
                self.view.bringSubviewToFront(self.firstView)

                self.applyButtonOutlet.setTitle("DONE", for: .normal)

            })

        }
        else
        {

            let navigate = self.rootVC

            if self.typeStackview.isHidden == false {
                var temparray = [String]()
                for i in 0..<self.typeArray.count {
                    if self.typeArray[i].isClicked == true {
                        temparray.append(self.typeArray[i].key)
                    }
                }

                self.typeString = temparray.joined(separator: ",")

                navigate.typeFilter = self.typeString
            }

            if self.filterStackview.isHidden == false {
                var temparray = [String]()
                for i in 0..<self.filterArray.count {
                    if self.filterArray[i].isClicked == true {
                        temparray.append(self.filterArray[i].key)
                    }
                }

                self.filterString = temparray.joined(separator: ",")

                navigate.filterFilter = self.filterString
            }

            if self.ratingStackview.isHidden == false {
                navigate.filterRating = "\(self.ratingView.rating)"
            }

            if self.deliveryCostStackview.isHidden == false {
                navigate.filterDeliveryType = self.deliveryCost
            }
            
            if self.priceStackview.isHidden == false {
                navigate.maxFilterPrice = self.maxPrice
            }

            if self.distanceStackView.isHidden == false
            {
                navigate.maxFilterDistance = self.maxDistance
            }

            if searchOptionOneArray.count > 0
            {
                let optionOneCommaSeparatedString = searchOptionOneArray.joined(separator: ",")
                let optionTwoCommaSeparatedString = searchOptionTwoArray.joined(separator: ",")

                print("Search Option One Array----",optionOneCommaSeparatedString)

                print("Search Option Two Array----",optionTwoCommaSeparatedString)

                navigate.searchOptionOneCommaSeparatedString = optionOneCommaSeparatedString

                navigate.searchOptionTwoCommaSeparatedString = optionTwoCommaSeparatedString
                
                
                

                //navigate.tempParams = tempParams!
            }

            self.navigationController?.pushViewController(navigate, animated: true)

        }

    }




    // MARK:- // Create Arrays

    func createArrays() {

        for i in 0..<((self.locationArray[0] as! NSDictionary)["state"] as! NSArray).count {

            self.stateArray.add(["name" : "\((((self.locationArray[0] as! NSDictionary)["state"] as! NSArray)[i] as! NSDictionary)["region_name"] ?? "")" , "id" : "\((((self.locationArray[0] as! NSDictionary)["state"] as! NSArray)[i] as! NSDictionary)["region_id"] ?? "")"])

        }


        for i in 0..<self.stateArray.count {

            self.cityArray.add((stateArray[i] as! NSDictionary)["city"] as! NSArray)

        }

        for i in 0..<self.cityArray.count {

            for j in 0..<(self.cityArray[i] as! NSArray).count {
                self.suburbArray.add(((self.cityArray[i] as! NSArray)[j] as! NSDictionary)["suburb"] as! NSArray)
            }

        }

    }



}




// MARK:- // Tableview Delegate Methods

extension SortViewController: UITableViewDelegate,UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == sortCategoryTableview
        {
            return sortArray.count
        }
        else if tableView == self.typeTableview {
            return self.typeArray.count
        }
        else if tableView == self.filterTableview {
            return self.filterArray.count
        }
        else
        {
            var returnCount : Int!
            for i in 0..<sortArray.count
            {
                if clickedOnIndex == i
                {
                    returnCount = ((sortArray[i] as! [String : Any])["value"] as! [sortClass]).count
                }
            }

            return returnCount
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == sortCategoryTableview
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! SortCategoryTVCTableViewCell

            cell.cellLBL.text = "\((sortArray[indexPath.row] as! NSDictionary)["key"]!)"

            // Set Font //

            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }
        else if tableView == self.typeTableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! SortCategoryTVCTableViewCell

            cell.cellLBL.text = self.typeArray[indexPath.row].value ?? ""

            if self.typeArray[indexPath.row].isClicked == true {
                cell.cellImage.image = UIImage(named: "checkBoxFilled")
            }
            else {
                cell.cellImage.image = UIImage(named: "checkBoxEmpty")
            }

            // Set Font //

            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }
        else if tableView == self.filterTableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! SortCategoryTVCTableViewCell

            cell.cellLBL.text = self.filterArray[indexPath.row].value ?? ""

            if self.filterArray[indexPath.row].isClicked == true {
                cell.cellImage.image = UIImage(named: "checkBoxFilled")
            }
            else {
                cell.cellImage.image = UIImage(named: "checkBoxEmpty")
            }

            // Set Font //

            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "listTableCell") as! SortSubcategoryTVCTableViewCell

            cell.cellLBL.text = "\(((((sortArray[clickedOnIndex] as! [String : Any])["value"] as! [sortClass])[indexPath.row]).value)!)" + " (" + "\(((((sortArray[clickedOnIndex] as! [String : Any])["value"] as! [sortClass])[indexPath.row]).totalNo)!)" + ")"

            if ((((sortArray[clickedOnIndex] as! [String : Any])["value"] as! [sortClass])[indexPath.row]).totalNo).elementsEqual("0")
            {
                cell.cellLBL.textColor = UIColor.lightGray
            }
            else
            {
                cell.cellLBL.textColor = UIColor.black
            }


            if ((((sortArray[clickedOnIndex] as! [String : Any])["value"] as! [sortClass])[indexPath.row]).isClicked) == true
            {
                cell.cellImage.image = UIImage(named: "checkBoxFilled")
            }
            else
            {
                cell.cellImage.image = UIImage(named: "checkBoxEmpty")
            }


            // Set Font //

            cell.cellLBL.font = UIFont(name: cell.cellLBL.font.fontName, size: CGFloat(Get_fontSize(size: 14)))

            //
            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == sortCategoryTableview
        {
            clickedOnIndex = indexPath.row

            UIView.animate(withDuration: 0.5, animations: {

                self.firstView.isHidden = true

                self.secondView.isHidden = false

                self.view.bringSubviewToFront(self.secondView)

                self.sortSubCategoryTableview.delegate = self
                self.sortSubCategoryTableview.dataSource = self
                self.sortSubCategoryTableview.reloadData()

                self.applyButtonOutlet.setTitle("APPLY", for: .normal)

            })
        }
        else if tableView == self.typeTableview {
            self.typeArray[indexPath.row].isClicked = !self.typeArray[indexPath.row].isClicked

            self.typeTableview.reloadData()
        }
        else if tableView == self.filterTableview {
            self.filterArray[indexPath.row].isClicked = !self.filterArray[indexPath.row].isClicked

            self.filterTableview.reloadData()
        }
        else
        {
            let tempStatus = ((((sortArray[clickedOnIndex] as! [String : Any])["value"] as! [sortClass])[indexPath.row]).isClicked)

            if ((((sortArray[clickedOnIndex] as! [String : Any])["value"] as! [sortClass])[indexPath.row]).totalNo).elementsEqual("0")
            {
                (((sortArray[clickedOnIndex] as! [String : Any])["value"] as! [sortClass])[indexPath.row]).isClicked = tempStatus!
            }
            else
            {
                (((sortArray[clickedOnIndex] as! [String : Any])["value"] as! [sortClass])[indexPath.row]).isClicked = !tempStatus!
            }

            self.sortSubCategoryTableview.reloadData()

        }

    }

}



// MARK:- // Pickerview delegate Methods

extension SortViewController: UIPickerViewDelegate,UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerArray.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        return "\((self.pickerArray[row] as! NSDictionary)["name"] ?? "")"

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if self.stateClicked == true {

            self.city.text = ""
            self.suburb.text = ""
            self.cityArray.removeAllObjects()
            self.suburbArray.removeAllObjects()

            self.state.text = "\((self.pickerArray[row] as! NSDictionary)["name"] ?? "")"

            self.stateClicked = false

            self.stateIndex = row

            //creating city array
            for i in 0..<((((self.locationArray[0] as! NSDictionary)["state"] as! NSArray)[row] as! NSDictionary)["city"] as! NSArray).count {

                self.cityArray.add(["name" : "\((((((self.locationArray[0] as! NSDictionary)["state"] as! NSArray)[row] as! NSDictionary)["city"] as! NSArray)[i] as! NSDictionary)["city_name"] ?? "")" , "id" : "\((((((self.locationArray[0] as! NSDictionary)["state"] as! NSArray)[row] as! NSDictionary)["city"] as! NSArray)[i] as! NSDictionary)["city_id"] ?? "")"])

            }
            self.city.text = ""
            self.suburb.text = ""

        }



        if self.cityClicked == true {

            self.suburb.text = ""
            self.suburbArray.removeAllObjects()

            self.city.text = "\((self.pickerArray[row] as! NSDictionary)["name"] ?? "")"

            self.cityClicked = false

            self.cityIndex = row

            //creating suburb array
            for i in 0..<((((((self.locationArray[0] as! NSDictionary)["state"] as! NSArray)[stateIndex] as! NSDictionary)["city"] as! NSArray)[cityIndex] as! NSDictionary)["suburb"] as! NSArray).count {

                self.suburbArray.add(["name" : "\((((((((self.locationArray[0] as! NSDictionary)["state"] as! NSArray)[stateIndex] as! NSDictionary)["city"] as! NSArray)[cityIndex] as! NSDictionary)["suburb"] as! NSArray)[i] as! NSDictionary)["suburbs_name"] ?? "")", "id" : "\((((((((self.locationArray[0] as! NSDictionary)["state"] as! NSArray)[stateIndex] as! NSDictionary)["city"] as! NSArray)[cityIndex] as! NSDictionary)["suburb"] as! NSArray)[i] as! NSDictionary)["suburbs_id"] ?? "")"])

            }

        }



        if self.suburbClicked == true {
            self.suburb.text = "\((self.pickerArray[row] as! NSDictionary)["name"] ?? "")"

            self.suburbClicked = false

            self.suburbIndex = row
        }

        UIView.animate(withDuration: 0.5) {

            self.pickerBackgroundview.isHidden = true
            self.view.sendSubviewToBack(self.pickerBackgroundview)

        }


    }



}







// MARK;- // Sort Category Tableview Cell

class SortCategoryTVCTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellLBL: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellSeparatorView: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



// MARK:- // Sort SubCategory Tableview Cell

class SortSubcategoryTVCTableViewCell: UITableViewCell {


    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellLBL: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellSeparatorView: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}




