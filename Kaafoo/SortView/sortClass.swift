//
//  sortClass.swift
//  Kaafoo
//
//  Created by admin on 29/12/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import Foundation

//Custom class.
class sortClass: NSObject, NSCoding {
    
    
    var key: String!
    var value: String!
    var isClicked: Bool!
    var totalNo: String!
    
    required convenience init(coder decoder: NSCoder) {
        self.init()
        self.key = decoder.decodeObject(forKey: "key") as? String
        self.value = decoder.decodeObject(forKey: "value") as? String
        self.isClicked = decoder.decodeObject(forKey: "isClicked") as? Bool
        self.totalNo = decoder.decodeObject(forKey: "totalNo") as? String
        
    }
    
    convenience init(key: String, value: String , isClicked: Bool , totalNo: String) {
        self.init()
        
        self.key = key
        self.value = value
        self.isClicked = isClicked
        self.totalNo = totalNo
        
    }
    
    
    func encode(with aCoder: NSCoder) {
        if let key = key { aCoder.encode(key, forKey: "key") }
        if let value = value { aCoder.encode(value, forKey: "value") }
        if let isClicked = isClicked { aCoder.encode(isClicked, forKey: "isClicked") }
        if let totalNo = value { aCoder.encode(totalNo, forKey: "totalNo") }
    }

}
