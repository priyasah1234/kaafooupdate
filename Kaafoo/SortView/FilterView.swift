//
//  FilterView.swift
//  Kaafoo
//
//  Created by admin on 02/01/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import Foundation
// MARK:- // FilterView class

class FilterView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)

    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if (self.topViewController()?.isKind(of: FoodListingViewController.self))! {
            sortArray = ["Search"]
        }
        else if (self.topViewController()?.isKind(of: NewJobListingViewController.self))!
        {
            sortArray = ["Sort","Filter"]
        }
        else {
            sortArray = ["Sort" , "Search"]
        }
        

        

        self.setupFilterView()
    }
    var FullHeight = (UIScreen.main.bounds.size.height)
    var FullWidth = (UIScreen.main.bounds.size.width)
    var sortArray = [String]()

    var buttonArray = [UIButton]()

    var tapToCloseButton : UIButton = {
        let taptoclosebutton = UIButton()
        taptoclosebutton.translatesAutoresizingMaskIntoConstraints = false
        return taptoclosebutton
    }()

    func setupFilterView() {

        self.backgroundColor = UIColor(red:65/255, green:65/255, blue:65/255, alpha: 0.85)

        let sortBackgroundView = UIView(frame: CGRect(x: (160/320)*self.FullWidth, y: 0, width: (155/320)*self.FullWidth, height: (125/568)*self.FullHeight))

        let menuUpImage = UIImageView(frame: CGRect(x: (120/320)*self.FullWidth, y: 0, width: (30/320)*self.FullWidth, height: (30/568)*self.FullHeight))

        menuUpImage.image = UIImage(named: "menu_up")

        let sortOptionsView = UIView(frame: CGRect(x: 0, y: (15/568)*self.FullHeight, width: (155/320)*self.FullWidth, height: (110/568)*self.FullHeight))

        sortOptionsView.backgroundColor = UIColor.white

        self.addSubview(self.tapToCloseButton)
        sortBackgroundView.addSubview(menuUpImage)
        sortBackgroundView.addSubview(sortOptionsView)
        self.addSubview(sortBackgroundView)

        createFilterView(toAddInView: sortOptionsView)

        self.tapToCloseButton.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor)

    }
    // MARK:- // Create Sort OPtions VIew
    func createFilterView(toAddInView: UIView)
    {
        var tempOrigin : CGFloat! = 0

        for i in 0..<sortArray.count
        {
            let tempView = UIView(frame: CGRect(x: 0, y: tempOrigin, width: (155/320)*self.FullWidth, height: (35/568)*self.FullHeight))

            let tempLBL = UILabel(frame: CGRect(x: (10/320)*self.FullWidth, y: (5/568)*self.FullHeight, width: (135/320)*self.FullWidth, height: (25/568)*self.FullHeight))

            tempLBL.text = sortArray[i]

            //tempLBL.font = UIFont(name: fontLabel.font.fontName, size: CGFloat(Get_fontSize(size: 13)))

            let tempBTN = UIButton(frame: CGRect(x: 0, y: 0, width: (155/320)*self.FullWidth, height: (35/568)*self.FullHeight))

            tempBTN.tag = i

            self.buttonArray.append(tempBTN)

            //tempBTN.addTarget(self, action: #selector(self.startSorting(sender:)), for: .touchUpInside)

            tempView.addSubview(tempLBL)
            tempView.addSubview(tempBTN)

            toAddInView.addSubview(tempView)

            tempOrigin = tempOrigin + (35/568)*self.FullHeight

        }
        toAddInView.clipsToBounds = true

        toAddInView.autoresizesSubviews = false

        toAddInView.frame.size.height = tempOrigin

        toAddInView.autoresizesSubviews = true

        toAddInView.layer.cornerRadius = (5/568)*self.FullHeight

    }
    // MARK: - // function TopViewController
    func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}


