//
//  RequiredClass.swift
//  Kaafoo
//
//  Created by esolz on 06/08/19.
//  Copyright © 2019 ESOLZ. All rights reserved.
//

import UIKit
import Foundation

// MARK:- // Holiday Search Parameters

class holidaySearchParameters {
    
    init() {}
    
    static let shared = holidaySearchParameters()
    
    var checkInDate : String! = ""
    var checkOutDate : String! = ""
    var roomCount : String! = "1"
    var adultCount : String! = "1"
    var childrenCount : String! = "0"
    var hotelPrductId : String! = ""
    var HolidaySearchAddress : String! = ""
    var tempCheckIndate : String! = ""
    var tempCheckOutdate : String! = ""
    var DateDifference : Int! = 0
    var RoomTotalPrice : String! = ""
    var HolidayProductID : String! = ""
    var HolidayRoomID : String! = ""
    var APICheckINDate : String! = ""
    var APIcheckOutDate : String! = ""
    var SearchLatitude : String! = ""
    var SearchLongitude : String! = ""
    
    
    
    public func setCheckinDate(ciDate : String) {
        self.checkInDate = ciDate
    }
    public func setCheckoutDate(coDate : String) {
        self.checkOutDate = coDate
    }
    public func setRoomCount(roomcount : String) {
        self.roomCount = roomcount
    }
    public func setAdultCount(adultcount : String) {
        self.adultCount = adultcount
    }
    public func setChildrenCount(childrencount : String) {
        self.childrenCount = childrencount
    }
    public func setHotelProductID(hotelID : String) {
        self.hotelPrductId = hotelID
    }
    public func setHotelLocation(hotelLocation : String) {
        self.HolidaySearchAddress = hotelLocation
    }
    public func setTempCheckIndate(checkindate : String) {
        self.tempCheckIndate = checkindate
    }
    public func setTempCheckOutdate(checkoutdate : String) {
        self.tempCheckOutdate = checkoutdate
    }
    public func setDateDifference(dateDifference : Int)
    {
        self.DateDifference = dateDifference
    }
    public func setRoomTotalPrice(totalPrice : String)
    {
        self.RoomTotalPrice = totalPrice
    }
    public func setHolidayProductID(productID : String)
    {
       self.HolidayProductID = productID
    }
    public func setHolidayRoomID(roomID : String)
    {
        self.HolidayRoomID = roomID
    }
    public func setAPICheckindate(CheckIN : String)
    {
        self.APICheckINDate = CheckIN
    }
    public func setAPICheckOutDate(CheckOut : String)
    {
        self.APIcheckOutDate = CheckOut
    }
    public func setSearchLatitude(latitude : String)
    {
        self.SearchLatitude = latitude
    }
    public func setSearchLongitude(longitude : String)
    {
        self.SearchLongitude = longitude
    }
    
    
}

class dailyRentalSearchParameters{
    
    init(){}
    
    static let shared = dailyRentalSearchParameters()
    
    var PickupLocation : String! = ""
    var pickupdate : String! = ""
    var pickuptime : String! = ""
    var dropOffLocation : String! = ""
    var dropofftime : String! = ""
    var dropoffdate : String! = ""
    var driverAge : String! = ""
    var rentalProductID : String! = ""
    var finalBookingPrice : String! = ""
    
    
    public func setPickupLocation(pickuplocation : String) {
        self.PickupLocation = pickuplocation
    }
    public func setPickupTime(pickupTIME : String) {
        self.pickuptime = pickupTIME
    }
    public func setPickupDate(PIckupdate : String) {
        self.pickupdate = PIckupdate
    }
    public func setdropofflocation(DropOffLocation : String) {
        self.dropOffLocation = DropOffLocation
    }
    public func setdropOffTime(DropOffTime : String) {
        self.dropofftime = DropOffTime
    }
    public func setDropOffdate(DropOffDate : String) {
        self.dropoffdate = DropOffDate
    }
    public func setDriverAge(driverage : String)
    {
        self.driverAge = driverage
    }
    public func setRentalProductID(productID : String)
    {
        self.rentalProductID = productID
    }
    public func setFinalBookingPrice(BookingPrice : String)
    {
        self.finalBookingPrice = BookingPrice
    }
    
}

class HappeningSearchParameters
{
    init(){}
    
    static let shared = HappeningSearchParameters()
    
    var SelectedState : String! = ""
    var SelectedCity : String! = ""
    var StartDate : String! = ""
    var EndDate : String! = ""
    var SearchWord : String! = ""
    
    public func setSelectedState(state : String)
    {
        self.SelectedCity = state
    }
    public func setSelcectedCity(city : String)
    {
        self.SelectedCity = city
    }
    public func setStartDate(startDate : String)
    {
        self.StartDate = startDate
    }
    public func setEndDate(EndDate : String)
    {
        self.EndDate = EndDate
    }
    public func setSearchWord(Searchword : String)
    {
        self.SearchWord = Searchword
    }
    
}

class LocationDetailsParameters
{
    init(){}
    
    static let shared = LocationDetailsParameters()
    
    var LocationLatitude : String? = ""
    
    var LocationLongitude : String? = ""
    
    public func setCurrentLatitude(latitude : String)
    {
        self.LocationLatitude = latitude
    }
    
    public func setCurrentLongitude(longitude : String)
    {
        self.LocationLongitude = longitude
    }
    
    
}

class ServiceDetailsParameters
{
    init(){}
    
    static let shared = ServiceDetailsParameters()
    
    var SelectedService : String? = "Service"
    
    public func setSelectedService(Service : String)
       {
           self.SelectedService = Service
       }
}

class MarketPlaceParameters
{
    init(){}
    
    static let shared = MarketPlaceParameters()
    
    var SelectedMarketplaceCategory : String?
    
    var SelectedMarketplaceMainCategoryOutlet : String?
    
    public func setMarketPlaceCategory(selectcategory : String)
    {
        self.SelectedMarketplaceCategory = selectcategory
    }
    
    public func setSelectedMarketPlaceMainCategoryOutlet(selectedCategory : String)
    {
        self.SelectedMarketplaceMainCategoryOutlet = selectedCategory
    }
}

class BeamsParameters
{
    init(){}
    
    static let shared = BeamsParameters()
    
    var BeamsToken : String?
    
    public func setBeamsToken(Token : String)
       {
           self.BeamsToken = Token
       }
    
}

class DownloadPDFParameters
{
    init(){}
    
    static let shared = DownloadPDFParameters()
    
    var downloadPDFPath : String?
    
    public func setDownloadPath(path : String)
    {
        self.downloadPDFPath = path
    }
}

class DownloadAudioParameters
{
    init(){}
    
    static let shared = DownloadAudioParameters()
    
    var downloadAudioPath : String?
    
    public func setDownloadPath(path : String)
    {
        self.downloadAudioPath = path
    }
}



