//
//  NearByVC.swift
//  Kaafoo
//
//  Created by esolz on 03/01/20.
//  Copyright © 2020 ESOLZ. All rights reserved.
//

import UIKit

import GoogleMaps

import SVProgressHUD

import SDWebImage

class NearByVC: GlobalViewController,GMSMapViewDelegate,UIScrollViewDelegate {
    
    var CellPosition : Int! = 0
    
    var CellIndex : IndexPath!
    
    let LangID = UserDefaults.standard.string(forKey: "langID")
    
    let userID = UserDefaults.standard.string(forKey: "userID")
    
    let MyCountryName = UserDefaults.standard.string(forKey: "myCountryName")
    
    @IBOutlet weak var CategoryView: UIView!
    
    @IBOutlet weak var ListingCollectionView: UICollectionView!
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var SelectCategoryBtnOutlet: UIButton!
    
    @IBAction func SelectCategoryBtn(_ sender: UIButton) {
        
        self.ShowPickerView.isHidden = false
        
        self.view.bringSubviewToFront(self.ShowPickerView)
        
        self.DataPicker.selectRow(0, inComponent: 0, animated: false)
        
    }
    
    @IBOutlet weak var SelectedCategory: UILabel!
    
    @IBOutlet weak var ShowPickerView: UIView!
    
    @IBAction func hidePickerBtn(_ sender: UIButton) {
        
        self.ShowPickerView.isHidden = true
        
        self.view.sendSubviewToBack(self.ShowPickerView)
        
    }
    
    //var CategoryArray = ["All","Marketplace","Daily Deals","Services","Jobs","Food Court","Happening","Daily Rental","Holiday Accommodation"]
    
    var CategoryArray = [["key" : "All" , "value" : ""],["key" : "Marketplace" , "value" : "9"],["key" : "Daily Deals" , "value" : "10"],["key" : "Services" , "value" : "11"],["key" : "Jobs" , "value" : "12"],["key" : "Food Court" , "value" : "13"],["key" : "Happening" , "value" : "14"],["key" : "Daily Rental" , "value" : "15"],["key" : "Holiday Accommodation" , "value" : "16"],["key" : "Real Estate" , "value" : "999"]]
    
    var ProductListingArray : NSMutableArray! = NSMutableArray()
    
    var SearchCat : String! = ""
    
    var markersArray = [GMSMarker]()
    
    var MapDetailsArray = [mapDetails]()
    
    /*
     9-Marketplace
     10-DailyDeals
     11-Services
     12-Jobs
     13-Food Court
     14-Happening
     15-Daily Rental
     16-Holiday Accommodatin999
     9-Marketplace
     11-Services
     12-Jobs
     13-Food Court
     14-Happening
     15-Daily Rental
     16-Holiday Accommodatin
     999-Real Estate
     */
    
    @IBOutlet weak var hidePickerBtnOutlet: UIButton!
    
    @IBOutlet weak var DataPicker: UIPickerView!
    
    var SelectedPickerInt : Int! = 0
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.CategoryView.backgroundColor = UIColor.white
        
        //self.ListingCollectionView.isPagingEnabled = true
        
        self.ListingCollectionView.isHidden = false
        
        self.SelectedCategory.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_category", comment: "")
        
        self.mapView.delegate = self
        
        self.fetchData()
        
        self.createToolbar()
        
        self.DataPicker.delegate = self
        
        self.DataPicker.dataSource = self
        
        self.DataPicker.reloadAllComponents()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.mapView.isMyLocationEnabled = true
        
        self.mapView.settings.myLocationButton = true
        
        self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 240, right: 0)
        
    }
    
    //MARK:- //Create ToolBar for PickerView
        
        func createToolbar()
        {
            let toolBar = UIToolbar()
            
            toolBar.barStyle = UIBarStyle.default
            
            toolBar.isTranslucent = true
            
            toolBar.barTintColor = .black
            
            toolBar.tintColor = .white
            
            toolBar.sizeToFit()

            let doneButton = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "done", comment: ""), style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePickerView))
            
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            
            let cancelButton = UIBarButtonItem(title: LocalizationSystem.sharedInstance.localizedStringForKey(key: "cancel", comment: "") , style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelPickerView))
            
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
            
            toolBar.isUserInteractionEnabled = true
            
            self.ShowPickerView.addSubview(toolBar)
            
            toolBar.anchor(top: nil, leading: self.ShowPickerView.leadingAnchor, bottom: self.DataPicker.topAnchor, trailing: self.ShowPickerView.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        }
    
    @objc func donePickerView()
    {
        mapView.clear()
        
        self.CellPosition = 0
        
        self.MapDetailsArray.removeAll()
        
        self.markersArray.removeAll()
        
        self.SelectedCategory.text = "\((self.CategoryArray[SelectedPickerInt] as NSDictionary)["key"] ?? "")"
        
        self.SearchCat = "\((self.CategoryArray[SelectedPickerInt] as NSDictionary)["value"] ?? "")"
        
        self.fetchData()
        
        self.ShowPickerView.isHidden = true
    }
    
    @objc func cancelPickerView()
    {
        self.ShowPickerView.isHidden = true
    }
    
    @objc func PreviousBtnTarget(sender : UIButton)
    {
        CellPosition = CellPosition - 1
        
        if CellPosition < 0
        {
            CellPosition = 0
        }
        else
        {
            self.ListingCollectionView.scrollToItem(at: IndexPath(item: CellPosition, section: 0), at: .left, animated: true )
            
            self.SetCameraPosition(lat: self.MapDetailsArray[CellPosition].ProductLat!, long: self.MapDetailsArray[CellPosition].ProductLong!)
        }
        
    }
    
    @objc func NextBtnTarget(sender : UIButton)
    {
        CellPosition = CellPosition + 1
        
        if CellPosition > self.MapDetailsArray.count - 1
        {
            CellPosition = 1
        }
        else
        {
            self.ListingCollectionView.scrollToItem(at: IndexPath(item: CellPosition, section: 0), at: .right, animated: true )
            
            self.SetCameraPosition(lat: self.MapDetailsArray[CellPosition].ProductLat!, long: self.MapDetailsArray[CellPosition].ProductLong!)
        }
        
    }
    
    func fetchData()
    {
        self.ProductListingArray.removeAllObjects()
        
        let parameters = "user_id=\(self.userID ?? "")&lang_id=\(self.LangID ?? "")&mycountry_name=\(self.MyCountryName ?? "")&search_cat=\(self.SearchCat ?? "")"
        
        self.CallAPI(urlString: "app_nearby_list", param: parameters, completion: {
            
            self.globalDispatchgroup.leave()
            
            self.ProductListingArray = ((self.globalJson["info_array"] as! NSDictionary)["product_list"] as! NSMutableArray)
            
            self.globalDispatchgroup.notify(queue: .main, execute: {
                
                let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 11.0)

                self.mapView.animate(to: cameraPosition)
                
                self.mapView.isMyLocationEnabled = true
                
                self.mapView.settings.myLocationButton = true
                
                self.AppendData()
                
            })
            
            DispatchQueue.main.async {
                
                SVProgressHUD.dismiss()
                
                self.ListingCollectionView.delegate = self
                
                self.ListingCollectionView.dataSource = self
                
                self.ListingCollectionView.reloadData()
                
                if self.ProductListingArray.count == 0
                {
                    self.ListingCollectionView.isHidden = true
                }
                else
                {
                    self.ListingCollectionView.isHidden = false
                    
                    self.SetCameraPosition(lat: self.MapDetailsArray[self.CellPosition].ProductLat!, long: self.MapDetailsArray[self.CellPosition].ProductLong!)
                }
                
            }
            
        })
    }
    
    func AppendData()
    {
        for i in 0..<self.ProductListingArray.count
        {
            self.MapDetailsArray.append(mapDetails(ProductLat: Double(("\((self.ProductListingArray[i] as! NSDictionary)["lat"] ?? "")" as NSString).doubleValue), ProductLong: Double(("\((self.ProductListingArray[i] as! NSDictionary)["long"] ?? "")" as NSString).doubleValue), ProductID: "\((self.ProductListingArray[i] as! NSDictionary)["id"] ?? "")", ProductName: "\((self.ProductListingArray[i] as! NSDictionary)["product_name"] ?? "")", ProductListedTime: "\((self.ProductListingArray[i] as! NSDictionary)["listed_time"] ?? "")", ProductExpireTime: "\((self.ProductListingArray[i] as! NSDictionary)["expire_time"] ?? "")", PriceCurrency: "\((self.ProductListingArray[i] as! NSDictionary)["currency"] ?? "")", ProductType: "\((self.ProductListingArray[i] as! NSDictionary)["product_type_text"] ?? "")", ProductImage: "\((self.ProductListingArray[i] as! NSDictionary)["photo_url"] ?? "")", ProductStartPrice: "\((self.ProductListingArray[i] as! NSDictionary)["currency"] ?? "")" + "\((self.ProductListingArray[i] as! NSDictionary)["start_price"] ?? "")", ProductReservePrice: "\((self.ProductListingArray[i] as! NSDictionary)["currency"] ?? "")" + "\((self.ProductListingArray[i] as! NSDictionary)["reserve_price"] ?? "")", ProductTag: i, ProductWatchlistStatus: "\((self.ProductListingArray[i] as! NSDictionary)["currency"] ?? "")", ProductAddress: "\((self.ProductListingArray[i] as! NSDictionary)["address"] ?? "")"))
        }
        
                   for state in self.MapDetailsArray {
                    
                        let state_marker = GMSMarker()
                    
                        let markerImage = UIImage(named: "KaafooMarker")!.withRenderingMode(.alwaysOriginal)

                        let markerView = UIImageView(image: markerImage)
                    
                        state_marker.position = CLLocationCoordinate2D(latitude: state.ProductLat ?? 0.0, longitude: state.ProductLong ?? 0.0)
                    
                        state_marker.title = state.ProductName
                    
                        state_marker.userData = state
                    
                        state_marker.iconView = markerView
                    
                        state_marker.snippet = "Hey, this is \(state.ProductName!)"
        
                        state_marker.map = self.mapView
                    
                        self.markersArray.append(state_marker)
                    }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        targetContentOffset.pointee = scrollView.contentOffset
        
        var indexes = self.ListingCollectionView.indexPathsForVisibleItems
        
        indexes.sort()
        
        var index = indexes.first!
        
        let cell = self.ListingCollectionView.cellForItem(at: index)!
        
        let position = self.ListingCollectionView.contentOffset.x - cell.frame.origin.x
        
        if position > cell.frame.size.width/2{
            
           index.row = index.row+1
        }
        
        self.CellPosition = index.row
        
        self.CellIndex = index
        
        self.ListingCollectionView.scrollToItem(at: index, at: .left, animated: true )
        
        self.SetCameraPosition(lat: self.MapDetailsArray[CellPosition].ProductLat!, long: self.MapDetailsArray[CellPosition].ProductLong!)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func SetCameraPosition(lat : CLLocationDegrees , long : CLLocationDegrees)
    {
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 20)
        
        mapView?.camera = camera
        
        mapView?.animate(to: camera)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        self.CellPosition = 0
        
        self.SetCameraPosition(lat: self.MapDetailsArray[CellPosition].ProductLat!, long: self.MapDetailsArray[CellPosition].ProductLong!)
        
        self.ListingCollectionView.scrollToItem(at: IndexPath(item: CellPosition, section: 0), at: .right, animated: true )
        
        let cameraPosition = GMSCameraPosition.camera(withLatitude: 22.896256, longitude: 88.2461183, zoom: 10.0)

        mapView.animate(to: cameraPosition)
    }

}

extension NearByVC : UIPickerViewDelegate,UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.CategoryArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "\((self.CategoryArray[row] as NSDictionary)["key"] ?? "")"
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        print("Nothing")
        
        self.SelectedPickerInt = row
        
    }
}


struct mapDetails
{
    var ProductLat : CLLocationDegrees? = 88.8765
    
    var ProductLong : CLLocationDegrees? = 22.6576
    
    var ProductID : String?
    
    var ProductName : String?
    
    var ProductListedTime : String?
    
    var ProductExpireTime : String?
    
    var PriceCurrency : String?
    
    var ProductType : String?
    
    var ProductImage : String?
    
    var ProductStartPrice : String?
    
    var ProductReservePrice : String?
    
    var ProductTag : Int?
    
    var ProductWatchlistStatus : String?
    
    var ProductAddress : String?
    
    init(ProductLat : CLLocationDegrees , ProductLong : CLLocationDegrees , ProductID : String , ProductName : String , ProductListedTime : String , ProductExpireTime : String , PriceCurrency : String , ProductType : String , ProductImage : String , ProductStartPrice : String , ProductReservePrice : String , ProductTag : Int , ProductWatchlistStatus : String , ProductAddress : String)
    {
        
        self.ProductLat = ProductLat
        
        self.ProductLong = ProductLong
        
        self.ProductID = ProductID
        
        self.ProductName = ProductName
        
        self.ProductListedTime = ProductListedTime
        
        self.ProductExpireTime = ProductExpireTime
        
        self.PriceCurrency = PriceCurrency
        
        self.ProductType = ProductType
        
        self.ProductImage = ProductImage
        
        self.ProductStartPrice = ProductStartPrice
        
        self.ProductReservePrice = ProductReservePrice
        
        self.ProductTag = ProductTag
        
        self.ProductWatchlistStatus = ProductWatchlistStatus
        
        self.ProductAddress = ProductAddress
        
    }
}


class NearByListingCVC : UICollectionViewCell
{
    @IBOutlet weak var ViewContent: UIView!
    
    @IBOutlet weak var ProductImage: UIImageView!
    
    @IBOutlet weak var ProductName: UILabel!
    
    @IBOutlet weak var ProductListed: UILabel!
    
    @IBOutlet weak var ProductExpire: UILabel!
    
    @IBOutlet weak var ProductAddress: UILabel!
    
    @IBOutlet weak var ProductPrice: UILabel!
    
    @IBOutlet weak var ProductType: UILabel!
    
    @IBOutlet weak var ProductWatchlist: UIButton!
    
    @IBOutlet weak var PreviousProductBtnOutlet: UIButton!
    
    @IBOutlet weak var NextProductBtnOutlet: UIButton!
    
    @IBOutlet weak var CVCWidthConstraint: NSLayoutConstraint!
    
}

extension NearByVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.ProductListingArray.count
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nearbylisting", for: indexPath) as! NearByListingCVC
        
        cell.ViewContent.backgroundColor = .white
        
        cell.ViewContent.layer.borderWidth = 1.5
        
        cell.ViewContent.layer.borderColor = UIColor.black.cgColor
        
        cell.ProductName.text = "\(self.MapDetailsArray[indexPath.row].ProductName ?? "")"
        
        cell.ProductListed.text = "\(self.MapDetailsArray[indexPath.row].ProductListedTime ?? "")"
        
        cell.ProductExpire.text = "\(self.MapDetailsArray[indexPath.row].ProductExpireTime ?? "")"
        
        cell.ProductAddress.text = "\(self.MapDetailsArray[indexPath.row].ProductAddress ?? "")"
        
        let imageURL = "\(self.MapDetailsArray[indexPath.row].ProductImage ?? "")"
        
        cell.ProductImage.sd_setImage(with: URL(string: imageURL))
        
        cell.ProductType.text = "\(self.MapDetailsArray[indexPath.row].ProductType ?? "")"
        
        cell.ProductPrice.text = "\(self.MapDetailsArray[indexPath.row].ProductStartPrice ?? "")"
        
        cell.ProductWatchlist.isHidden = true
        
        cell.PreviousProductBtnOutlet.addTarget(self, action: #selector(self.PreviousBtnTarget(sender:)), for: .touchUpInside)
        
        cell.NextProductBtnOutlet.addTarget(self, action: #selector(self.NextBtnTarget(sender:)), for: .touchUpInside)
        
        cell.CVCWidthConstraint.constant = (304/320)*self.FullWidth
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //let padding : CGFloat! = 4

        //let collectionViewSize = ListingCollectionView.frame.size.width

        return CGSize(width: self.FullWidth, height: 199)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
        
    }
    
    
}
