//
//  HeaderView.swift
//  Kaafoo
//
//  Created by Debarun on 13/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    

    @IBOutlet weak var ShareBtnOutlet: UIButton!
    @IBOutlet weak var HeaderImageView: UIImageView!
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var sideMenuButtonImage: UIImageView!
    @IBOutlet var headerViewTitle: UILabel!
    @IBOutlet var headerViewAdditionalImageOne: UIImageView!
    @IBOutlet var headerVIewAdditionalImageTwo: UIImageView!
    @IBOutlet var sideMenuButton: UIButton!
    @IBOutlet weak var crossImage: UIImageView!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var dropDownButtonImage: UIImageView!
    @IBOutlet weak var dropDownButton: UIButton!
    
    @IBOutlet weak var sideMenuButtonAR: UIButton!
    @IBOutlet weak var crossButtonAR: UIButton!
    
    @IBOutlet weak var tapSearch: UIButton!
    
    @IBOutlet weak var tapSort: UIButton!
    
    var FullHeight = (UIScreen.main.bounds.size.height)
    var FullWidth = (UIScreen.main.bounds.size.width)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        
        let langID = UserDefaults.standard.string(forKey: "langID")
        
        if (langID?.elementsEqual("AR"))!
        {
            self.HeaderImageView.image = UIImage(named: "kaafoo_logo_arabic")
        }
        else
        {
            self.HeaderImageView.image = UIImage(named: "logo")
        }
        
    }
    
    // MARK:- // RTL - Set Frames and Allignments for Arabic
    
    func RTL()
    {
        
        sideMenuButtonAR.isHidden = false
        crossButtonAR.isHidden = false
        
        sideMenuButton.isHidden = true
        crossButton.isHidden = true
        
        headerViewAdditionalImageOne.frame.origin.x = (55/320)*self.FullWidth
        headerVIewAdditionalImageTwo.frame.origin.x = (20/320)*self.FullWidth
        
        dropDownButtonImage.frame.origin.x = (60/320)*self.FullWidth
        dropDownButton.frame.origin.x = (60/320)*self.FullWidth
        
        /*
        sideMenuButtonImage.frame = CGRect(x: (275/320)*self.FullWidth, y: (40/568)*self.FullHeight, width: (25/320)*self.FullWidth, height: (25/568)*self.FullHeight)
        
        headerViewAdditionalImageOne.frame = CGRect(x: (55/320)*self.FullWidth, y: (40/568)*self.FullHeight, width: (20/320)*self.FullWidth, height: (20/568)*self.FullHeight)
        
        sideMenuButton.frame = CGRect(x: (55/320)*self.FullWidth, y: (40/568)*self.FullHeight, width: (25/320)*self.FullWidth, height: (25/568)*self.FullHeight)
        
        headerVIewAdditionalImageTwo.frame = CGRect(x: (55/320)*self.FullWidth, y: (40/568)*self.FullHeight, width: (20/320)*self.FullWidth, height: (20/568)*self.FullHeight)
        
        crossImage.frame = CGRect(x: (55/320)*self.FullWidth, y: (40/568)*self.FullHeight, width: (25/320)*self.FullWidth, height: (25/568)*self.FullHeight)
        
        crossButton.frame = CGRect(x: (55/320)*self.FullWidth, y: (40/568)*self.FullHeight, width: (25/320)*self.FullWidth, height: (25/568)*self.FullHeight)
        
        dropDownButtonImage.frame = CGRect(x: (55/320)*self.FullWidth, y: (40/568)*self.FullHeight, width: (20/320)*self.FullWidth, height: (20/568)*self.FullHeight)
        
        dropDownButton.frame = CGRect(x: (55/320)*self.FullWidth, y: (40/568)*self.FullHeight, width: (20/320)*self.FullWidth, height: (20/568)*self.FullHeight)
 */
    }
 
}
