//
//  SideMenuView.swift
//  Kaafoo
//
//  Created by Debarun on 13/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol Side_menu_delegate: class {
    
    func action_method(sender: NSInteger?)
    
    func myAccount_section(sender: NSInteger?)
    
    func buying_section(sender: NSInteger?)
    
    func selling_section(sender: NSInteger?)
    
    func job_section(sender: NSInteger?)
    
    func isGuest()
    
    func sideMenuButtons(sender: NSInteger?)
    
    func categoryDidSelect(typeID : String , categoryID : String , categoryName : String )
    
    func servicesDidselect(typeID : String, categoryName: String)
    
    func dailyDealsDidSelect()
    
    func HappeningDidSelect()
    
    func foodCourtDidSelect(typeID: String)
    
    func foodCourtdidSelect()
    
    func holidayAccomodationDidSelect()
    
    func dailyRentalDidSelect(productID : String)
    
    func RealEstateDidSelect()
    
    func jobDidSelect()
    
    func ClassifiedDidSelect()
    
    func NearBy()
    
    func myAccount_marketplace_Section(sender : NSInteger?)
    
    func myAccount_dailyRental_Section(sender : NSInteger?)
    
    func myAccount_Happening_Section(sender : NSInteger?)
    
    func myAccount_HolidayAccommodation_Section(sender : NSInteger?)
    
    func myAccount_FoodCourt_Section(sender : NSInteger?)
    
    func myAccount_Service_Section(sender : NSInteger?)
    
    func myAccount_Real_Estate_Section(sender : NSInteger?)
    
    func myAccount_Advertsiement_Section(sender :  NSInteger?)
    
    func myAccount_Latest_news()
    
    func myAccount_Query_Section(sender : NSInteger?)
    
    func myAccount_account_wallet_Section(sender : NSInteger?)
    
    func myAccount_notification()
    
    func SelectAllCategory()
    
    func forthTable_action(sender : NSInteger?)
}


class SideMenuView: UIView {

    @IBOutlet var contentView: UIView!
    
    @IBOutlet var menuView: UIView!
    
    @IBOutlet weak var userDisplayName: UILabel!
    
    @IBOutlet weak var homeView: UIView!
    
    @IBOutlet weak var homeLBL: UILabel!


    // MARK:- // Home Button
    @IBOutlet weak var homeBTN: UIButton!
    
    @IBAction func homeBTNTap(_ sender: UIButton) {
        
        Side_delegate?.sideMenuButtons(sender: sender.tag)
    }

    @IBOutlet weak var ForthTableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ForthTableView: UITableView!
    
    @IBOutlet weak var sideMenuCategoryTableHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var collapsibleTableHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var collapsibleTableview: UITableView!
    
    @IBOutlet var closeSideMenu: UIButton!

    @IBOutlet weak var sideMenuCategoryTable: UITableView!

    @IBOutlet weak var mainScroll: UIScrollView!
    
    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var insideCategoryView: UIView!
    
    @IBOutlet weak var backToMainMenuIMage: UIImageView!
    
    @IBOutlet weak var backToMainMenuLBL: UILabel!
    
    @IBOutlet weak var categoryBarImage: UIImageView!
    
    @IBOutlet weak var categoryNameLBL: UILabel!
    
    @IBOutlet weak var CategoryNameBtn: UIButton!
    
    @IBOutlet weak var insideCategoryTableview: UITableView!

    @IBOutlet weak var upperActivityView: UIView!
    
    @IBOutlet weak var loginImageView: UIImageView!
    
    @IBOutlet weak var loginLBL: UILabel!
    
    @IBOutlet weak var registerImageVIew: UIImageView!
    
    @IBOutlet weak var registerLBL: UILabel!
    
    @IBOutlet weak var snapchatImageView: UIImageView!
    
    @IBOutlet weak var snapchatLBL: UILabel!

    @IBOutlet weak var nearbyAndPostAdView: UIView!
    
    @IBOutlet weak var nearbyLBL: UILabel!

    @IBOutlet weak var userView: UIView!
    
    @IBOutlet weak var userImageview: UIImageView!
    
    @IBOutlet weak var guestView: UIView!
    
    @IBOutlet weak var guestImageview: UIImageView!
    
    @IBOutlet weak var tapOnProfile: UIButton!

    @IBOutlet weak var loginAndRegisterView: UIView!

    @IBOutlet weak var snapchatAndQrView: UIView!

    @IBOutlet weak var upperActivityViewSeparator: UIView!

    @IBOutlet weak var scanQRCodeLBL: UILabel!



    weak var SideBtnDelegate : SideButtonDelegate?

    weak var Side_delegate: Side_menu_delegate?

    let dispatchGroup = DispatchGroup()

    var allCategoriesArray = NSArray()

    var allSubcategoriesArray = [AnyObject]()

    var FullWidth = UIScreen.main.bounds.size.width
    
    var FullHeight = UIScreen.main.bounds.size.height

    var FoodCourtArray : NSMutableArray! = NSMutableArray()

    var categoryID : String! = ""

    var categoryType : String! = ""


    let langID = UserDefaults.standard.string(forKey: "langID")

    let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")

    let languageName = UserDefaults.standard.string(forKey: "myLanguage")

    var ForthTableArray = [LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_the_language", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "choose_country", comment: "")]

    var data = [
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_account", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit_account", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "edit_account_info", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "verification_file", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "User_information", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "Text_advertisement", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_advertisement", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "Restaurant_feature", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "service_feature", comment: ""),
                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "category_section", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "holiday_features", comment: "")], isExpandable: false),
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_interests", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "watchlist", comment: ""),
                                                    
                                                                                                                           LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_favourites", comment: ""),
                                                                                                                           LocalizationSystem.sharedInstance.localizedStringForKey(key: "recently_viewed", comment: "")
            ], isExpandable: false),
        
        //MARK:- My Account Marketplace Section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Marketplace", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_i_am_selling", comment: ""),
                                                                                                                    LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_I_won", comment: ""),
                                                                                                                    LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_i_lost", comment: ""),
                                                                                                                    LocalizationSystem.sharedInstance.localizedStringForKey(key: "sold_items", comment: ""),
                                                                                                                    LocalizationSystem.sharedInstance.localizedStringForKey(key: "unsold_items", comment: ""),
                                                                                                                    LocalizationSystem.sharedInstance.localizedStringForKey(key: "buy_now_invoices", comment: "")
            ], isExpandable: false),
        
        //End
        
        //MARK:- My Account Happening Section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "happening", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed_happening", comment: ""),
                                                                                                                                LocalizationSystem.sharedInstance.localizedStringForKey(key: "event_booking", comment: ""),
                                                                                                                                LocalizationSystem.sharedInstance.localizedStringForKey(key: "event_item_booking", comment: "")
            ], isExpandable: false),
        
        //End
        
        
        //MARK:- //My Account Daily Rental section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Daily Rental", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed_daily_rental", comment: ""),
                                                                                                                              LocalizationSystem.sharedInstance.localizedStringForKey(key: "Booking Information", comment: ""),
                                                                                                                              LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingItem", comment: "")
            ], isExpandable: false),
        

        //End
        
        //MARK:- //My Account Holiday Accommodation section
        
        //start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Holiday Accomodation", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed_accommodation", comment: ""),
                                                                                                                                 LocalizationSystem.sharedInstance.localizedStringForKey(key: "holidayBooking", comment: ""),
                                                                                                                                 LocalizationSystem.sharedInstance.localizedStringForKey(key: "holidaybookinginformation", comment: ""),
                                                                                                                                 LocalizationSystem.sharedInstance.localizedStringForKey(key: "Users Holiday Features", comment: "")
            ], isExpandable: false),
        
        //End
        
        //MARK:- //My Account Service Section
        
        //Start
        
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Services", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed_services", comment: ""),
                                                                                                                                         LocalizationSystem.sharedInstance.localizedStringForKey(key: "booked_services", comment: ""),
                                                                                                                                         LocalizationSystem.sharedInstance.localizedStringForKey(key: "service_invoices", comment: ""),
                                                                                                                                         LocalizationSystem.sharedInstance.localizedStringForKey(key: "users_holiday_services", comment: "")
            ], isExpandable: false),
        
        //End
        
        //MARK :- //My Account Food Court Section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Food Court", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "food-list", comment: ""),
                                                                                                                               LocalizationSystem.sharedInstance.localizedStringForKey(key: "food_court_orders", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "food_order_items", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "users_restaurent_features", comment: "")], isExpandable: false),
        
        //End
        
        
        //MARK:- //My Account Real Estate Section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "Real Estate", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "listed_real_estate", comment: "")], isExpandable: false),
        
        //End
        
        
        //MARK:- //My Account Latest News Section
        
        //Start
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "latest_news", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "latest_news", comment: "")], isExpandable: false),
        
        //End
        
        

        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "selling", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "List an Item", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "items_i_am_selling", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "sold_items", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "unsold_items", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "bookingItem", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "serviceItem", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "bonusRequest", comment: ""),
                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "foodList", comment: ""),

                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "eventBooking", comment: ""),

                                                                                                                            LocalizationSystem.sharedInstance.localizedStringForKey(key: "holidayBooking", comment: "")], isExpandable: false),
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "account_wallet", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "credit_charging", comment: ""),
                                                                                                                                   LocalizationSystem.sharedInstance.localizedStringForKey(key: "kaafooBonusSection", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "wallet_refund", comment: "")], isExpandable: false),
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "jobSection", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "jobPost", comment: ""),
                                                                                                                                   LocalizationSystem.sharedInstance.localizedStringForKey(key: "jobPostList", comment: "")], isExpandable: false),
        
        //Commercial Advertisement List
        
//        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "commercial_advertisement_list", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "commercial_advertisement", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "commercial_advertisement_list", comment: "")], isExpandable: false),
        
        //Text Advertisement Section
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "text_advertisement", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "text_advertisement", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "add_advertisement", comment: "")], isExpandable: false),
        
        
        //MARK:- //Query Section My Account Section
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "query", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "product_query", comment: ""),LocalizationSystem.sharedInstance.localizedStringForKey(key: "job_query", comment: "")], isExpandable: false),
        
        
        //MY Account Notification Section
        
         dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "notification", comment: ""), subType: [LocalizationSystem.sharedInstance.localizedStringForKey(key: "notification", comment: "")], isExpandable: false),
        
        
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "chat", comment: ""), subType: [], isExpandable: false),
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_the_language", comment: ""), subType: [], isExpandable: false),
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "choose_country", comment: ""), subType: [], isExpandable: false),
        dataSet(headerName: LocalizationSystem.sharedInstance.localizedStringForKey(key: "logout", comment: ""), subType: [], isExpandable: false)
    ]

    // MARK:- // Override Init

    override init(frame: CGRect) {
        super.init(frame: frame)

    }


    // MARK:- // Required Init

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        Bundle.main.loadNibNamed("SideMenuView", owner: self, options: nil)
        
        addSubview(contentView)
        
        contentView.frame = self.bounds
        
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        let cellIdentifier = "SideViewCell"
        
        let nibName = UINib(nibName: "SideViewCell", bundle:nil)
        
        self.collapsibleTableview.register(nibName, forCellReuseIdentifier: cellIdentifier)

        let categoryCellIdentifier = "SideViewCategoryCell"
        
        let categoryNibName = UINib(nibName: "SideViewCategoryCell", bundle:nil)
        
        self.sideMenuCategoryTable.register(categoryNibName, forCellReuseIdentifier: categoryCellIdentifier)

        let identifier = "CategoryCell"
        
        let detailsNibName = UINib(nibName: "CategoryCell", bundle: nil)
        
        self.insideCategoryTableview.register(detailsNibName, forCellReuseIdentifier: identifier)
        
        self.ForthTableView.register(detailsNibName, forCellReuseIdentifier: identifier)

        self.fetchData()
        //
        self.changeLanguageStrings()

//        self.SetCountryChecking()

        self.postAdButtonOutlet.layer.cornerRadius = self.postAdButtonOutlet.frame.size.height / 2

        self.sideMenuCategoryTable.estimatedRowHeight = 70
        
        self.sideMenuCategoryTable.rowHeight = UITableView.automaticDimension

        self.insideCategoryTableview.estimatedRowHeight = 70
        
        self.insideCategoryTableview.rowHeight = UITableView.automaticDimension

        self.collapsibleTableview.estimatedRowHeight = 70
        
        self.collapsibleTableview.rowHeight = UITableView.automaticDimension
        
        self.ForthTableView.estimatedRowHeight = 70
        
        self.ForthTableView.rowHeight = UITableView.automaticDimension
        
    }
    
    // MARK:- // Change Language Strings
    
    func changeLanguageStrings() {
        self.homeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Home", comment: "")
        
        self.snapchatLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "snapChat", comment: "")
        
        self.scanQRCodeLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "scanqrcode", comment: "")
        
        self.nearbyLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "nearby", comment: "")
        
        self.postAdButtonOutlet.setTitle(LocalizationSystem.sharedInstance.localizedStringForKey(key: "post_ad", comment: ""), for: .normal)
        
    }


    func SetCountryChecking()
    {
        if langID == nil
        {
          data[8].headerName = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_the_language", comment: "")
        }
        else
        {
          data[8].headerName = langID
        }
    }



    // MARK:- // Buttons


    // MARK:- // Back to Main Menu Button

    @IBAction func tapOnBackToMainMenu(_ sender: UIButton) {

        self.insideCategoryView.isHidden = true
        self.menuView.sendSubviewToBack(self.insideCategoryView)


    }



    // MARK:- // Login Button

    @IBAction func tapOnLogin(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }


    // MARK:- // Register Button

    @IBAction func tapOnRegister(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }



    // MARK:- // Snapchat Button

    @IBAction func tapOnSnapchat(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }


    // MARK:- // Scan QR Button

    @IBAction func tapOnScanQR(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }


    // MARK:- // Nearby Button

    @IBAction func tapOnNearby(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }


    // MARK:- // Post Ad Button

    @IBOutlet weak var postAdButtonOutlet: UIButton!
    @IBAction func tapOnPostAd(_ sender: UIButton) {

        Side_delegate?.sideMenuButtons(sender: sender.tag)

    }






    // MARK: - // JSON Get Method to get Category Data from Web


    func fetchData()

    {
        self.dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }

        let CountryId = UserDefaults.standard.string(forKey: "countryID")
        
        let myCountryName = UserDefaults.standard.string(forKey: "myCountryName")

//        let langID = UserDefaults.standard.string(forKey: "langID")

        var str_url : String! = ""

//        if CountryId == nil
//        {
//             str_url = "http://esolz.co.in/lab6/kaafoo/appcontrol/app_category_list?mycountry_id="
//        }
//        else
//        {
//             str_url = "http://esolz.co.in/lab6/kaafoo/appcontrol/app_category_list?mycountry_id=\(myCountryName!)"
//        }
        
        print("MyCountryName",myCountryName ?? "")
        
        //str_url = "https://staging.esolzbackoffice.com/kaafoo/web/appcontrol/app_category_list?mycountry_id=\(myCountryName!)"
        let encodedString = myCountryName?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        str_url = "https://staging.esolzbackoffice.com/kaafoo/web/appcontrol/app_category_list?mycountry_id=\(encodedString!)"
        
        print("String_URL",str_url!)

        if let url = NSURL(string: str_url)

        {

            if let data = try? Data(contentsOf:url as URL)

            {

                do{

                    let dict = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! NSDictionary

                    print("Let URL",str_url!)
                    //print("Category fetch Data Response : " , dict)

                    self.allCategoriesArray = dict["info_categories"] as! NSArray
                    
                    print("AllCategories Count",self.allCategoriesArray.count)

                    DispatchQueue.main.async {

                        self.dispatchGroup.leave()

                        self.sideMenuCategoryTable.delegate = self
                        
                        self.sideMenuCategoryTable.dataSource = self
                        
                        self.sideMenuCategoryTable.reloadData()

                        SVProgressHUD.dismiss()
                    }

                }

                catch

                {

                    print("Error")

                }

            }

        }

    }


//    // MARK: - // JSON Get Method to get inside Category Data from Web
//
//
//    func insideCategoryFetchData()
//
//    {
//
//        dispatchGroup.enter()
//
//        DispatchQueue.main.async {
//            SVProgressHUD.show()
//        }
//
//        let langID = UserDefaults.standard.string(forKey: "langID")
//
//        let str_url = "http://esolz.co.in/lab6/kaafoo/appcontrol/app_product_list?" + "cat_id=" + "\(categoryID!)" + "&lang_id=" + "\(langID!)"
//
//        print("Url is : ", str_url)
//
//        if let url = NSURL(string: str_url)
//
//        {
//
//            if let data = try? Data(contentsOf:url as URL)
//
//            {
//
//                do{
//
//                    let dict = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! NSDictionary
//
//
//                    //print("Inside Category fetch Data Response : " , dict)
//
//                    //making a deffirent dictionary for JOb in requirement
//                    if categoryType.elementsEqual("Jobs")
//                    {
//
//                        for i in 0..<allCategoriesArray.count
//                        {
//                            if "\((allSubcategoriesArray[i] as! NSDictionary)["id"]!)".elementsEqual(categoryID)
//                            {
//                                allSubcategoriesArray  = (allCategoriesArray[i] as! NSDictionary)["child_cat"] as! [AnyObject]
//                            }
//                        }
//
//                    }
//                    else
//                    {
//
//                        self.allSubcategoriesArray = (dict["product_info"] as! NSDictionary)["child_category_listing"] as! [AnyObject]
//
//                    }
//
//
//
//
//                    DispatchQueue.main.async {
//
//                        self.dispatchGroup.leave()
//
//                        self.insideCategoryTableview.delegate = self
//                        self.insideCategoryTableview.dataSource = self
//                        self.insideCategoryTableview.reloadData()
//
//                        SVProgressHUD.dismiss()
//                    }
//
//                }
//
//                catch
//
//                {
//
//                    print("Error")
//
//                }
//
//            }
//
//        }
//
//    }

    // MARK: - // JSON Get Method to get inside Category Data from Web


    func InsideCategoryFetchData()
    {

        dispatchGroup.enter()

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
       let CountryId = UserDefaults.standard.string(forKey: "countryID")

//       let langID = UserDefaults.standard.string(forKey: "langID")
        
        let myCountryNameOne = UserDefaults.standard.string(forKey: "myCountryName")
        
        let encodedCountry = myCountryNameOne?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)

        var str_url : String! = ""
        
        str_url = "https://staging.esolzbackoffice.com/kaafoo/web/appcontrol/app_category_list?mycountry_id=\(encodedCountry ?? "")"

        print("Url is : ", str_url)

        if let url = NSURL(string: str_url)

        {

            if let data = try? Data(contentsOf:url as URL)

            {

                do{

                    let dict = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! NSDictionary

                    //print("Inside Category fetch Data Response : " , dict)

                    //making a deffirent dictionary for JOb in requirement

                    let infoArray = dict["info_categories"] as! NSArray
                    //print("InfoArray=\\==",infoArray)

                    for i in 0..<infoArray.count
                    {
                      if "\((infoArray[i] as! NSDictionary)["id"]!)".elementsEqual(categoryID)
                      {
                        let DemoArray = ((infoArray[i] as! NSDictionary)["child_cat"]! as! NSArray)
                        //print("demoArray==",DemoArray)
                        self.allSubcategoriesArray = DemoArray as [AnyObject]
                        //print("AllsubcategoriesArray",self.allSubcategoriesArray)
                      }
                        else
                      {
                        print("Do Nothing")
                      }
                    }
                    
                    
//                    if categoryType.elementsEqual("Jobs")
//                    {
//
//                        for i in 0..<allCategoriesArray.count
//                        {
//                            if "\((allSubcategoriesArray[i] as! NSDictionary)["id"]!)".elementsEqual(categoryID)
//                            {
//                                allSubcategoriesArray  = (allCategoriesArray[i] as! NSDictionary)["child_cat"] as! [AnyObject]
//                            }
//                        }
//
//                    }
//                    else
//                    {
//
//                        self.allSubcategoriesArray = (dict["product_info"] as! NSDictionary)["child_category_listing"] as! [AnyObject]
//
//                    }




                    DispatchQueue.main.async {

                        self.dispatchGroup.leave()

                        self.insideCategoryTableview.delegate = self
                        self.insideCategoryTableview.dataSource = self
                        self.insideCategoryTableview.reloadData()
                        
                        
                        SVProgressHUD.dismiss()
                    }

                }

                catch

                {
                    print("Error")

                }

            }

        }

    }



    // MARK:- // function for Set Font

    func Get_fontSize(size: Float) -> Float
    {
        var size1 = size

        if(GlobalViewController.Isiphone6)
        {
            size1 += 1.0
        }
        else if(GlobalViewController.Isiphone6Plus)
        {
            size1 += 2.0
        }

        return size1
    }
    
    @objc func viewTapped(sender : UITapGestureRecognizer)
    {
        Side_delegate?.SelectAllCategory()
        print("Marketplace is tapped")
    }
    
}



// MARK:- // SEction Delegate

extension SideMenuView : sectionDelegate {


    func callSection(idx: Int) {

        for i in 0..<self.data.count {

            if i == idx {
                self.data[i].isExpandable = !self.data[i].isExpandable
            }
            else {
                self.data[i].isExpandable = false
            }

            self.collapsibleTableview.reloadSections([i], with: .automatic)
        }

        if self.data[idx].isExpandable == true {
            if self.data[idx].subType.count > 0 {
                self.collapsibleTableHeightConstraint.constant = ((30/568)*self.FullHeight * CGFloat(self.data.count - 2)) + (self.collapsibleTableview.visibleCells[0].bounds.height * CGFloat(self.data[idx].subType.count))
            }
            else {
                self.collapsibleTableHeightConstraint.constant = ((30/568)*self.FullHeight * CGFloat(self.data.count - 2))
            }

        }
        else {
            self.collapsibleTableHeightConstraint.constant = ((30/568)*self.FullHeight * CGFloat(self.data.count - 2))
        }

        Side_delegate?.action_method(sender: idx)

    }


}



// MARK:- // Tableview Delegates

extension SideMenuView : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == collapsibleTableview
        {
            return data.count
        }
        else
        {
            return 1
        }

    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == collapsibleTableview  // Home / My Account / Buying etc.
        {
            if data[section].isExpandable {
                return data[section].subType.count
            } else {
                return 0
            }
        }
        else if tableView == sideMenuCategoryTable   // Sidemenu Category Table
        {
            return self.allCategoriesArray.count
        }
        else if tableView == ForthTableView
        {
            return self.ForthTableArray.count
        }
        else   // InsideCategory Table
        {
            return allSubcategoriesArray.count
        }

    }


    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        if tableView == collapsibleTableview
        {

            let viewOfHeader = UIView()

            let sectionview : SectionView = {
                let sectionView = SectionView()
                sectionView.translatesAutoresizingMaskIntoConstraints = false
                return sectionView
            }()

            viewOfHeader.addSubview(sectionview)
//            viewOfHeader.backgroundColor = .red

            sectionview.anchor(top: viewOfHeader.topAnchor, leading: viewOfHeader.leadingAnchor, bottom: viewOfHeader.bottomAnchor, trailing: viewOfHeader.trailingAnchor)

            sectionview.delegate = self
            sectionview.secIndex = section
            sectionview.titleLBL.text = data[section].headerName
            print("HeaderName",data[section].headerName!)
//            sectionview.titleLBL.textColor = .red

            sectionview.btn.setTitleColor(UIColor.black, for: .normal)
            sectionview.btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
            
            
            
//            //MarketPlace Header Add Target Method
//
//            if sectionview.titleLBL.text == LocalizationSystem.sharedInstance.localizedStringForKey(key: "Marketplace", comment: "")
//            {
//                let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
//                gesture.numberOfTapsRequired = 1
//                sectionview.isUserInteractionEnabled = true
//                sectionview.addGestureRecognizer(gesture)
//            }
//            else
//            {
//               //do nothing
//            }
//
            

            // Check if Guest
            
            if section == 17
            {
                sectionview.isHidden = true
            }
            
            if section == 18
            {
                sectionview.isHidden = true
            }

            let isGuest = UserDefaults.standard.string(forKey: "isGuest")

            if (isGuest?.elementsEqual("1"))!  // check if the user entered as guest
            {
                if section == (data.count - 1) {
                    sectionview.titleLBL.text = ""
                }
            }
            else
            {
                sectionview.titleLBL.text = data[section].headerName
            }


            if (data[section].headerName!.elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "choose_country", comment: "")))
            {
                if myCountryName == nil
                {
                    sectionview.titleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "choose_country", comment: "")
                }
                else
                {
                    sectionview.titleLBL.text = myCountryName
                }
            }
            else
            {
                //do Nothing
            }


            if (data[section].headerName!.elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_the_language", comment: "")))
            {
                if languageName == nil
                {
                    sectionview.titleLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "select_the_language", comment: "")
                }
                else
                {
                    sectionview.titleLBL.text = languageName
                }
            }
            else
            {
                //do Nothing
            }



            if self.data[section].isExpandable == true {
                if self.data[section].subType.count > 0 {
                    sectionview.barImage.isHidden = false
                    sectionview.plusImage.isHidden = false
                    sectionview.plusImage.image = UIImage(named: "minus")
                }
                else {
                    sectionview.barImage.isHidden = true
                    sectionview.plusImage.isHidden = true
                }

            }
            else {
                if self.data[section].subType.count > 0 {
                    sectionview.plusImage.isHidden = false
                    sectionview.plusImage.image = UIImage(named: "plus")
                }
                else {
                    sectionview.plusImage.isHidden = true
                }
                sectionview.barImage.isHidden = true
            }
            
            //sectionview.backgroundColor = .red
            
            sectionview.titleLBL.font = UIFont(name: (self.homeLBL.font.fontName), size: CGFloat(Get_fontSize(size: 17)))    // Setting Font of Sections

            //sectionview.backgroundColor = UIColor.red

            return viewOfHeader
        }
        else
        {
            return nil
        }

    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == collapsibleTableview
        {
            if section == 17
            {
                return 0
            }
            if section == 18
            {
                return 0
            }
            
            return (30/568)*FullHeight
        }
        else
        {
            return 0
        }
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tableView == collapsibleTableview
        {

            let cellIdentifier = "SideViewCell"

            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SideViewCell

            //cell.backgroundColor=UIColor.clear

            cell.cellLabel.text = data[indexPath.section].subType[indexPath.row]

            cell.cellLabel.textAlignment = .natural
            
            cell.cellLabel.textColor = .black


            // Setting Font size of cells
            cell.cellLabel.font = UIFont(name: (cell.cellLabel.font.fontName), size: CGFloat(Get_fontSize(size: 14)))

            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell

        }
        else if tableView == sideMenuCategoryTable
        {
            let categoryCellIdentifier = "SideViewCategoryCell"

            let cell = tableView.dequeueReusableCell(withIdentifier: categoryCellIdentifier, for: indexPath) as! SideViewCategoryCell


            let langID = UserDefaults.standard.string(forKey: "langID")

            //AR or
            if (langID?.elementsEqual("EN"))!
            {
               // let categoryName = UserDefaults.standard.string(forKey: "categoryName")
                cell.cellTitleLBL.text = "\((allCategoriesArray[indexPath.row] as! NSDictionary)["categoryname_en"] ?? "")" + "(" +  "\((allCategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")" + ")"
            }
            else if (langID?.elementsEqual("AR"))!
            {
                cell.cellTitleLBL.text = "\((allCategoriesArray[indexPath.row] as! NSDictionary)["categoryname_ar"]!)" + "(" +  "\((allCategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")" + ")"
            }

            cell.backgroundColor = UIColor.clear

            // Setting Font size of cells
            cell.cellTitleLBL.font = UIFont(name: (cell.cellTitleLBL.font.fontName), size: CGFloat(Get_fontSize(size: 15)))

            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }
        else if tableView == self.ForthTableView
        {
            let identifier = "CategoryCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CategoryCell
            
            cell.cellTitleLBL.font = UIFont(name: (self.homeLBL.font.fontName), size: CGFloat(Get_fontSize(size: 17)))
            
            if indexPath.row == 0
            {
                if (languageName?.elementsEqual(""))!
                {
                   cell.cellTitleLBL.text = "\(self.ForthTableArray[indexPath.row])"
                }
                else
                {
                    cell.cellTitleLBL.text = "\(languageName ?? "")"
                }
            }
            else
            {
                if (myCountryName?.elementsEqual(""))!
                {
                   cell.cellTitleLBL.text = "\(self.ForthTableArray[indexPath.row])"
                }
                else
                {
                    cell.cellTitleLBL.text = "\(myCountryName ?? "")"
                }
            }
            
            return cell

        }
        else
        {


            let identifier = "CategoryCell"

            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CategoryCell

            // AR or EN

            let langID = UserDefaults.standard.string(forKey: "langID")

            //  print("All Categories Array----",self.allCategoriesArray)
            
            //print("totalCount","\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")")

            if (langID?.elementsEqual("EN"))!
            {
                let categoryName = UserDefaults.standard.string(forKey: "categoryName")
                
                print("Subcategory Count","\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")")
                
                cell.cellTitleLBL.text = "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["categoryname_en"] ?? "")" + "(" +  "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")" + ")"
                
            }
            else
            {
                cell.cellTitleLBL.text = "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["categoryname_ar"]!)" + "(" +  "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["total_count"] ?? "")" + ")"
            }

            cell.backgroundColor = UIColor.clear

            // Setting Font size of cells
            cell.cellTitleLBL.font = UIFont(name: (cell.cellTitleLBL.font.fontName), size: CGFloat(Get_fontSize(size: 16)))

            cell.selectionStyle = UITableViewCell.SelectionStyle.none

            return cell
        }



    }
    
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == collapsibleTableview
        {

            if data[indexPath.section].isExpandable
            {
                print("headername : ",((data[indexPath.section].headerName)!) )
                if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_account", comment: "")))
                {
                    Side_delegate?.myAccount_section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "my_interests", comment: "")))
                {
                    Side_delegate?.buying_section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "selling", comment: "")))
                {
                    Side_delegate?.selling_section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual("Job Section"))
                {
                    Side_delegate?.job_section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Marketplace", comment: "")))
                {
                    Side_delegate?.myAccount_marketplace_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Daily Rental", comment: "")))
                {
                    Side_delegate?.myAccount_dailyRental_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "happening", comment: "")))
                {
                    Side_delegate?.myAccount_Happening_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Holiday Accomodation", comment: "")))
                {
                    Side_delegate?.myAccount_HolidayAccommodation_Section(sender: indexPath.row)
                }
                else if       (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Food Court", comment: "")))
                {
                    Side_delegate?.myAccount_FoodCourt_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Services", comment: "")))
                {
                    Side_delegate?.myAccount_Service_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "Real Estate", comment: "")))
                {
                    Side_delegate?.myAccount_Real_Estate_Section(sender: indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "text_advertisement", comment: "")))
                {
                    Side_delegate?.myAccount_Advertsiement_Section(sender: indexPath.row)
                }
                
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "latest_news", comment: "")))
                {
                    Side_delegate?.myAccount_Latest_news()
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "query", comment: "")))
                {
                    Side_delegate?.myAccount_Query_Section(sender : indexPath.row)
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "notification", comment: "")))
                {
                    Side_delegate?.myAccount_notification()
                }
                else if (((data[indexPath.section].headerName)!).elementsEqual(LocalizationSystem.sharedInstance.localizedStringForKey(key: "account_wallet", comment: "")))
                {
                    Side_delegate?.myAccount_account_wallet_Section(sender: indexPath.row)
                }

            }
        }
        else if tableView == sideMenuCategoryTable
        {

            categoryID = "\((allCategoriesArray[indexPath.row] as! NSDictionary)["id"] ?? "")"

//            print("Category ID --- ", categoryID)

            if categoryID.elementsEqual("11")
            {
                categoryType = "Services"
            }
            else if categoryID.elementsEqual("9")
            {
                categoryNameLBL.textColor = .black
//                CategoryNameBtn.isHidden = false
//                CategoryNameBtn.addTarget(self, action: #selector(SideBtnDelegate?.MarketPlace(sender:)), for: .touchUpInside)
                let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
                gesture.numberOfTapsRequired = 1
                categoryNameLBL.isUserInteractionEnabled = true
                categoryNameLBL.addGestureRecognizer(gesture)
                
            }
            else if categoryID.elementsEqual("10")
            {
                categoryType = "None"
                Side_delegate?.dailyDealsDidSelect()
            }
            else if categoryID.elementsEqual("13")
            {
                categoryType = "Food Court"
                //                Side_delegate?.foodCourtDidSelect(typeID: "\(self.FoodCourtArray[indexPath.row])")
                //                Side_delegate?.foodCourtDidSelect()
            }
            else if categoryID.elementsEqual("16")
            {
                categoryType = "None"
                Side_delegate?.holidayAccomodationDidSelect()
            }
            else if categoryID.elementsEqual("999")
            {
                categoryType = "None"
                Side_delegate?.RealEstateDidSelect()
            }
            else if categoryID.elementsEqual("15")
            {
                categoryType = "None"
                Side_delegate?.dailyRentalDidSelect(productID: categoryID)
            }
            else if categoryID.elementsEqual("12")
            {
                categoryType = "None"
                Side_delegate?.jobDidSelect()
            }
            else if categoryID.elementsEqual("14")
            {
                categoryType = "None"
                Side_delegate?.HappeningDidSelect()
            }
            else if categoryID.elementsEqual("1122")
            {
                categoryType = "None"
                Side_delegate?.ClassifiedDidSelect()

            }
            else
            {
                categoryType = "None"
            }

            let langID = UserDefaults.standard.string(forKey: "langID")

            if (langID?.elementsEqual("EN"))!
            {
                let categoryName = UserDefaults.standard.string(forKey: "categoryName")
                categoryNameLBL.text = "\((allCategoriesArray[indexPath.row] as! NSDictionary)[categoryName ?? ""] ?? "")"
            }
            else
            {
                categoryNameLBL.text = "\((allCategoriesArray[indexPath.row] as! NSDictionary)["categoryname_ar"]!)"
            }

            //back to main menu lbl
            backToMainMenuLBL.text = LocalizationSystem.sharedInstance.localizedStringForKey(key: "Back to Main Menu", comment: "")

            

            self.InsideCategoryFetchData()

            dispatchGroup.notify(queue: .main) {

                self.insideCategoryView.isHidden = false
                self.menuView.bringSubviewToFront(self.insideCategoryView)

            }

        }
        else if tableView == insideCategoryTableview
        {

            if categoryType.elementsEqual("Services")
            {
                Side_delegate?.servicesDidselect(typeID: "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["id"]!)" , categoryName : "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["categoryname_en"]!)")
            }
            else if categoryType.elementsEqual("Food Court")
            {

                Side_delegate?.foodCourtDidSelect(typeID: "\((self.allSubcategoriesArray[indexPath.row] as! NSDictionary)["id"] ?? "")")

            }
                //            else if categoryType.elementsEqual("Jobs")
                //            {
                //                Side_delegate?.jobDidSelect(typeID: "\((allCategoriesArray[indexPath.row] as! NSDictionary)["id"]!)")
                //            }
            else
            {
                let categoryName = UserDefaults.standard.string(forKey: "categoryName")
                
                MarketPlaceParameters.shared.setMarketPlaceCategory(selectcategory: "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)[categoryName!]!)")
                
                Side_delegate?.categoryDidSelect(typeID : "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)["id"]!)" , categoryID : categoryID! , categoryName : "\((allSubcategoriesArray[indexPath.row] as! NSDictionary)[categoryName!]!)" )

            }
        }
        
        else if tableView == self.ForthTableView
        {
            Side_delegate?.forthTable_action(sender: indexPath.row)
        }
    }
}

//MARK:- //Add Target To MarketPlace Heading

@objc public protocol SideButtonDelegate : class
{
    @objc func MarketPlace(sender : UIButton)
    
}




