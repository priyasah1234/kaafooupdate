//
//  SideViewCategoryCell.swift
//  Kaafoo
//
//  Created by admin on 17/09/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import UIKit

class SideViewCategoryCell: UITableViewCell {
    @IBOutlet weak var cellTitleLBL: UILabel!
    @IBOutlet weak var cellImageview: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
