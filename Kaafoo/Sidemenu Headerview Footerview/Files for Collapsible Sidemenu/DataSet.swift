//
//  DataSet.swift
//  Kaafoo
//
//  Created by admin on 18/07/18.
//  Copyright © 2018 ESOLZ. All rights reserved.
//

import Foundation

class dataSet
{
    
    var headerName : String?
    var subType = [String]()
    var isExpandable : Bool = false
    
    init(headerName: String, subType: [String], isExpandable: Bool) {
        
        self.headerName = headerName
        self.subType = subType
        self.isExpandable = isExpandable
        
    }
}
